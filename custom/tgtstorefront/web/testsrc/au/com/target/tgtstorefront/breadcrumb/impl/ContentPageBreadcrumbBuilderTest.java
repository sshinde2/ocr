/**
 * 
 */
package au.com.target.tgtstorefront.breadcrumb.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;


/**
 * @author rmcalave
 * 
 */
public class ContentPageBreadcrumbBuilderTest {
    @InjectMocks
    private final ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder = new ContentPageBreadcrumbBuilder();

    @Mock
    private Converter<CategoryModel, CategoryData> mockCategoryConverter;
    @Mock
    private UrlResolver<CategoryData> mockCategoryDataUrlResolver;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetBreadcrumbsWithOnePage() {
        final String pageTitle = "Awesome Page";
        final String pageLabel = "/help/faqs";

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getTitle()).willReturn(pageTitle);
        BDDMockito.given(mockContentPage.getLabel()).willReturn(pageLabel);

        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockContentPage);

        Assert.assertTrue(breadcrumbs.size() == 1);

        final Breadcrumb breadcrumb = breadcrumbs.get(0);

        Assert.assertEquals(pageTitle, breadcrumb.getName());
        Assert.assertEquals(pageLabel, breadcrumb.getUrl());
        Assert.assertEquals("active", breadcrumb.getLinkClass());
        Assert.assertNull(breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithTwoPages() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent page
        final String parentPageTitle = "Awesome Parent Page";
        final String parentPageLabel = "/help";

        final ContentPageModel mockParentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockParentContentPage.getTitle()).willReturn(parentPageTitle);
        BDDMockito.given(mockParentContentPage.getLabel()).willReturn(parentPageLabel);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentContentPage);

        // Parent node
        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 2);

        // First level
        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(parentPageTitle, level1Breadcrumb.getName());
        Assert.assertEquals(parentPageLabel, level1Breadcrumb.getUrl());
        Assert.assertEquals("", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());

        // Second level
        final Breadcrumb level2Breadcrumb = breadcrumbs.get(1);
        Assert.assertEquals(currentPageTitle, level2Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("active", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithThreePages() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Root page
        final String rootPageTitle = "Awesome Root Page";
        final String rootPageLabel = "/help";

        final ContentPageModel mockRootContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockRootContentPage.getTitle()).willReturn(rootPageTitle);
        BDDMockito.given(mockRootContentPage.getLabel()).willReturn(rootPageLabel);

        // Root entry
        final CMSNavigationEntryModel mockRootNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockRootNavEntry.getItem()).willReturn(mockRootContentPage);

        // Root node
        final CMSNavigationNodeModel mockRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockRootNavNode.getEntries()).willReturn(Collections.singletonList(mockRootNavEntry));
        BDDMockito.given(mockRootNavNode.getParent()).willReturn(mockContentRootNavNode);


        // Parent page
        final String parentPageTitle = "Awesome Parent Page";
        final String parentPageLabel = "/help/faqs";

        final ContentPageModel mockParentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockParentContentPage.getTitle()).willReturn(parentPageTitle);
        BDDMockito.given(mockParentContentPage.getLabel()).willReturn(parentPageLabel);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentContentPage);

        // Parent node
        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockRootNavNode);


        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs/delivery-faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 3);

        // First level
        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(rootPageTitle, level1Breadcrumb.getName());
        Assert.assertEquals(rootPageLabel, level1Breadcrumb.getUrl());
        Assert.assertEquals("", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());

        // Second level
        final Breadcrumb level2Breadcrumb = breadcrumbs.get(1);
        Assert.assertEquals(parentPageTitle, level2Breadcrumb.getName());
        Assert.assertEquals(parentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());

        // Third level
        final Breadcrumb level3Breadcrumb = breadcrumbs.get(2);
        Assert.assertEquals(currentPageTitle, level3Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level3Breadcrumb.getUrl());
        Assert.assertEquals("active", level3Breadcrumb.getLinkClass());
        Assert.assertNull(level3Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithOnePageOneLink() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent link
        final String parentLinkText = "Awesome Parent Page";
        final String parentLinkUrl = "/help";

        final CMSLinkComponentModel mockParentLink = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockParentLink.getName()).willReturn(parentLinkText);
        BDDMockito.given(mockParentLink.getUrl()).willReturn(parentLinkUrl);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentLink);

        // Parent node
        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 2);

        // First level
        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(parentLinkText, level1Breadcrumb.getName());
        Assert.assertEquals(parentLinkUrl, level1Breadcrumb.getUrl());
        Assert.assertEquals("", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());

        // Second level
        final Breadcrumb level2Breadcrumb = breadcrumbs.get(1);
        Assert.assertEquals(currentPageTitle, level2Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("active", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithOnePageOneLinkNoUrl() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent link
        final String parentLinkText = "Awesome Parent Page";

        final CMSLinkComponentModel mockParentLink = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockParentLink.getName()).willReturn(parentLinkText);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentLink);

        // Parent node
        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 2);

        // First level
        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(parentLinkText, level1Breadcrumb.getName());
        Assert.assertEquals("", level1Breadcrumb.getUrl());
        Assert.assertEquals("", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());

        // Second level
        final Breadcrumb level2Breadcrumb = breadcrumbs.get(1);
        Assert.assertEquals(currentPageTitle, level2Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("active", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithOnePageOneCategory() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent category
        final String parentCategoryName = "Awesome Parent Page";
        final String parentLinkUrl = "/help";

        final CategoryModel mockParentCategory = Mockito.mock(CategoryModel.class);
        BDDMockito.given(mockParentCategory.getName()).willReturn(parentCategoryName);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentCategory);

        final CategoryData mockParentCategoryData = Mockito.mock(CategoryData.class);
        BDDMockito.given(mockCategoryConverter.convert(mockParentCategory)).willReturn(mockParentCategoryData);
        BDDMockito.given(mockCategoryDataUrlResolver.resolve(mockParentCategoryData)).willReturn(parentLinkUrl);

        // Parent node
        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 2);

        // First level
        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(parentCategoryName, level1Breadcrumb.getName());
        Assert.assertEquals(parentLinkUrl, level1Breadcrumb.getUrl());
        Assert.assertEquals("", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());

        // Second level
        final Breadcrumb level2Breadcrumb = breadcrumbs.get(1);
        Assert.assertEquals(currentPageTitle, level2Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("active", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithTwoPagesWithNodeTitle() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent page
        final String parentPageTitle = "Awesome Parent Page";
        final String parentPageLabel = "/help";

        final ContentPageModel mockParentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockParentContentPage.getTitle()).willReturn(parentPageTitle);
        BDDMockito.given(mockParentContentPage.getLabel()).willReturn(parentPageLabel);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentContentPage);

        // Parent node
        final String parentNavNodeTitle = "Parent Nav Node";

        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getTitle()).willReturn(parentNavNodeTitle);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final String currentNavNodeTitle = "Current Nav Node";

        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getTitle()).willReturn(currentNavNodeTitle);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 2);

        // First level
        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(parentNavNodeTitle, level1Breadcrumb.getName());
        Assert.assertEquals(parentPageLabel, level1Breadcrumb.getUrl());
        Assert.assertEquals("", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());

        // Second level
        final Breadcrumb level2Breadcrumb = breadcrumbs.get(1);
        Assert.assertEquals(currentNavNodeTitle, level2Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("active", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithOnePageOneLinkWithNodeTitle() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent link
        final String parentLinkText = "Awesome Parent Page";
        final String parentLinkUrl = "/help";

        final CMSLinkComponentModel mockParentLink = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockParentLink.getName()).willReturn(parentLinkText);
        BDDMockito.given(mockParentLink.getUrl()).willReturn(parentLinkUrl);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentLink);

        // Parent node
        final String parentNavNodeTitle = "Parent Nav Node";

        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getTitle()).willReturn(parentNavNodeTitle);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final String currentNavNodeTitle = "Current Nav Node";

        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getTitle()).willReturn(currentNavNodeTitle);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 2);

        // First level
        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(parentNavNodeTitle, level1Breadcrumb.getName());
        Assert.assertEquals(parentLinkUrl, level1Breadcrumb.getUrl());
        Assert.assertEquals("", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());

        // Second level
        final Breadcrumb level2Breadcrumb = breadcrumbs.get(1);
        Assert.assertEquals(currentNavNodeTitle, level2Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("active", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithOnePageOneCategoryWithNodeTitle() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent link
        final String parentCategoryName = "Awesome Parent Page";
        final String parentLinkUrl = "/help";

        final CategoryModel mockParentCategory = Mockito.mock(CategoryModel.class);
        BDDMockito.given(mockParentCategory.getName()).willReturn(parentCategoryName);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentCategory);

        final CategoryData mockParentCategoryData = Mockito.mock(CategoryData.class);
        BDDMockito.given(mockCategoryConverter.convert(mockParentCategory)).willReturn(mockParentCategoryData);
        BDDMockito.given(mockCategoryDataUrlResolver.resolve(mockParentCategoryData)).willReturn(parentLinkUrl);

        // Parent node
        final String parentNavNodeTitle = "Parent Nav Node";

        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getTitle()).willReturn(parentNavNodeTitle);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final String currentNavNodeTitle = "Current Nav Node";

        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getTitle()).willReturn(currentNavNodeTitle);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 2);

        // First level
        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(parentNavNodeTitle, level1Breadcrumb.getName());
        Assert.assertEquals(parentLinkUrl, level1Breadcrumb.getUrl());
        Assert.assertEquals("", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());

        // Second level
        final Breadcrumb level2Breadcrumb = breadcrumbs.get(1);
        Assert.assertEquals(currentNavNodeTitle, level2Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("active", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithOnePageOneNotSupportedType() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent Item
        final ItemModel mockItem = Mockito.mock(ItemModel.class);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockItem);

        // Parent node
        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 1);

        final Breadcrumb level2Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(currentPageTitle, level2Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level2Breadcrumb.getUrl());
        Assert.assertEquals("active", level2Breadcrumb.getLinkClass());
        Assert.assertNull(level2Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithNotEnoughLevels() {
        // Parent page
        final String parentPageTitle = "Awesome Parent Page";
        final String parentPageLabel = "/help";

        final ContentPageModel mockParentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockParentContentPage.getTitle()).willReturn(parentPageTitle);
        BDDMockito.given(mockParentContentPage.getLabel()).willReturn(parentPageLabel);

        // Parent entry
        final CMSNavigationEntryModel mockParentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockParentNavEntry.getItem()).willReturn(mockParentContentPage);

        // Parent node
        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getEntries()).willReturn(Collections.singletonList(mockParentNavEntry));

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 1);

        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(currentPageTitle, level1Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level1Breadcrumb.getUrl());
        Assert.assertEquals("active", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithNoParentNavNodeEntry() {
        // Site Root
        final CMSNavigationNodeModel mockSiteRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Content Root
        final CMSNavigationNodeModel mockContentRootNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentRootNavNode.getParent()).willReturn(mockSiteRootNavNode);

        // Parent node
        final CMSNavigationNodeModel mockParentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockParentNavNode.getParent()).willReturn(mockContentRootNavNode);

        // Current page
        final String currentPageTitle = "Awesome Page";
        final String currentPageLabel = "/help/faqs";

        final ContentPageModel mockCurrentContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCurrentContentPage.getTitle()).willReturn(currentPageTitle);
        BDDMockito.given(mockCurrentContentPage.getLabel()).willReturn(currentPageLabel);

        // Current entry
        final CMSNavigationEntryModel mockCurrentNavEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCurrentNavEntry.getItem()).willReturn(mockCurrentContentPage);

        // Current node
        final CMSNavigationNodeModel mockCurrentNavNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCurrentNavNode.getEntries()).willReturn(Collections.singletonList(mockCurrentNavEntry));
        BDDMockito.given(mockCurrentNavNode.getParent()).willReturn(mockParentNavNode);

        BDDMockito.given(mockCurrentContentPage.getNavigationNodeList()).willReturn(
                Collections.singletonList(mockCurrentNavNode));


        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockCurrentContentPage);

        Assert.assertTrue(breadcrumbs.size() == 1);

        final Breadcrumb level1Breadcrumb = breadcrumbs.get(0);
        Assert.assertEquals(currentPageTitle, level1Breadcrumb.getName());
        Assert.assertEquals(currentPageLabel, level1Breadcrumb.getUrl());
        Assert.assertEquals("active", level1Breadcrumb.getLinkClass());
        Assert.assertNull(level1Breadcrumb.getCategoryCode());
    }

    @Test
    public void testGetBreadcrumbsWithOnePageNoTitleNoLabel() {
        final String pageName = "Awesome Page";

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getName()).willReturn(pageName);

        final List<Breadcrumb> breadcrumbs = contentPageBreadcrumbBuilder.getBreadcrumbs(mockContentPage);

        Assert.assertTrue(breadcrumbs.size() == 1);

        final Breadcrumb breadcrumb = breadcrumbs.get(0);

        Assert.assertEquals(pageName, breadcrumb.getName());
        Assert.assertEquals("", breadcrumb.getUrl());
        Assert.assertEquals("active", breadcrumb.getLinkClass());
        Assert.assertNull(breadcrumb.getCategoryCode());
    }
}

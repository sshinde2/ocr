/**
 * 
 */
package au.com.target.tgtstorefront.breadcrumb.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.commerceservices.url.UrlResolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaAncestor;
import au.com.target.endeca.infront.data.EndecaBreadCrumb;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;


/**
 * @author bhuang3
 * 
 */
@UnitTest
public class SearchBreadcrumbBuilderTest {

    @Spy
    @InjectMocks
    private final SearchBreadcrumbBuilder searchBreadcrumbBuilder = new SearchBreadcrumbBuilder();

    @Mock
    private CategoryModel categoryModel;

    @Mock
    private UrlResolver<CategoryModel> categoryUrlResolver;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        searchBreadcrumbBuilder.setCategoryModelUrlResolver(categoryUrlResolver);
    }

    @Test
    public void testConvertDealBreadCrumbData() {
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb breadCrumb1 = new EndecaBreadCrumb();
        breadCrumb1.setElement("Jan  1 2014 12:00AM@@test1@@Jan  1 2099 12:00AM");
        final EndecaBreadCrumb breadCrumb2 = new EndecaBreadCrumb();
        breadCrumb2.setElement("Jan  1 2014 12:00AM@@test2@@Jan  1 2099 12:00AM");
        endecaCrumbs.add(breadCrumb1);
        endecaCrumbs.add(breadCrumb2);
        searchBreadcrumbBuilder.convertDealBreadCrumbData(endecaCrumbs);
        assertThat(endecaCrumbs.get(0).getElement()).isEqualTo("test1");
        assertThat(endecaCrumbs.get(1).getElement()).isEqualTo("test2");
    }

    @Test
    public void testConvertDealBreadCrumbDataWithEmptyCollection() {
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        searchBreadcrumbBuilder.convertDealBreadCrumbData(endecaCrumbs);
        assertThat(endecaCrumbs.size()).isEqualTo(0);
    }

    @Test
    public void testConvertDealBreadCrumbDataWithWrongFormat() {
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb breadCrumb1 = new EndecaBreadCrumb();
        breadCrumb1.setElement("Jan  1 2014 12:00AM");
        endecaCrumbs.add(breadCrumb1);
        searchBreadcrumbBuilder.convertDealBreadCrumbData(endecaCrumbs);
        assertThat(endecaCrumbs.get(0).getElement()).isEqualTo("Jan  1 2014 12:00AM");
    }

    @Test
    public void testGetBreadcrumbsWithNullCategoryDimensionTypeCategory() {
        final CategoryModel category = null;
        final BrandModel brand = new BrandModel();
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY);
        endecaCrumbs.add(endecaBreadCrumb);
        searchResults.setBreadCrumb(endecaCrumbs);

        doNothing().when(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        doNothing().when(searchBreadcrumbBuilder)
                .createCategoryBreadCrumbForSearch(Mockito.anyList(), Mockito.any(EndecaBreadCrumb.class),
                        Mockito.anyString());

        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        verify(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        verify(searchBreadcrumbBuilder)
                .createCategoryBreadCrumbForSearch(Mockito.anyList(), Mockito.any(EndecaBreadCrumb.class),
                        Mockito.anyString());
        assertThat(breadcrumbs.size()).isEqualTo(1);
        assertThat(breadcrumbs.get(0).getName()).isEqualTo("testFreeTextSearch");

    }

    @Test
    public void testGetBreadcrumbsWithNullCategoryDimensionTypeNotCategoryNotBulkyBoard() {
        final CategoryModel category = null;
        final BrandModel brand = new BrandModel();
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND);
        endecaCrumbs.add(endecaBreadCrumb);
        searchResults.setBreadCrumb(endecaCrumbs);

        doNothing().when(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        doNothing().when(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);

        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        verify(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        verify(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);
        assertThat(breadcrumbs.size()).isEqualTo(1);
        assertThat(breadcrumbs.get(0).getName()).isEqualTo("testFreeTextSearch");

    }

    @Test
    public void testGetBreadcrumbsWithNullCategoryEmptyEndecaCrumbs() {
        final CategoryModel category = null;
        final BrandModel brand = new BrandModel();
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        searchResults.setBreadCrumb(endecaCrumbs);


        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        assertThat(breadcrumbs.size()).isEqualTo(1);
        assertThat(breadcrumbs.get(0).getName()).isEqualTo("testFreeTextSearch");
    }

    @Test
    public void testGetCategoryBreadcrumb() {
        when(categoryUrlResolver.resolve(categoryModel)).thenReturn("testUrl");
        final Breadcrumb breadcrumb = searchBreadcrumbBuilder.getCategoryBreadcrumb(categoryModel, "testLink");
        assertThat(breadcrumb).isNotNull();
    }

    @Test
    public void testGetBreadcrumbsWithNullBrandDimensionTypeCategory() {
        final CategoryModel category = new CategoryModel();
        final BrandModel brand = null;
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY);
        endecaCrumbs.add(endecaBreadCrumb);
        searchResults.setBreadCrumb(endecaCrumbs);

        doNothing().when(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        doNothing().when(searchBreadcrumbBuilder)
                .createCategoryBreadCrumb(Mockito.anyList(), Mockito.any(EndecaBreadCrumb.class), Mockito.anyString());

        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        verify(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        verify(searchBreadcrumbBuilder)
                .createCategoryBreadCrumb(Mockito.anyList(), Mockito.any(EndecaBreadCrumb.class), Mockito.anyString());
        assertThat(breadcrumbs.size()).isEqualTo(0);

    }

    @Test
    public void testGetBreadcrumbsWithNullBrandDimensionTypeNotCategoryNotBulkyBoard() {
        final CategoryModel category = new CategoryModel();
        final BrandModel brand = null;
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_BRAND);
        endecaCrumbs.add(endecaBreadCrumb);
        searchResults.setBreadCrumb(endecaCrumbs);

        doNothing().when(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        doNothing().when(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);

        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        verify(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        verify(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);
        assertThat(breadcrumbs.size()).isEqualTo(0);
    }

    @Test
    public void testGetBreadcrumbsWithNullBrandTargetDealCategory() {
        final CategoryModel category = new TargetDealCategoryModel();
        final BrandModel brand = null;
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY);
        endecaCrumbs.add(endecaBreadCrumb);
        searchResults.setBreadCrumb(endecaCrumbs);
        final Breadcrumb breadCrumb = new Breadcrumb("testUrl", "testName", "testLinkClass");

        doNothing().when(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        doReturn(breadCrumb).when(searchBreadcrumbBuilder)
                .getDealCategoryBreadcrumb(Mockito.any(TargetDealCategoryModel.class), Mockito.anyString());
        doNothing().when(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);

        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        verify(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        verify(searchBreadcrumbBuilder).getDealCategoryBreadcrumb(Mockito.any(TargetDealCategoryModel.class),
                Mockito.anyString());
        verify(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);
        assertThat(breadcrumbs.size()).isEqualTo(1);

    }

    @Test
    public void testGetBreadcrumbsWithNullBrandTargetDealCategoryWithDimensionDealQualifierCategory() {
        final CategoryModel category = new TargetDealCategoryModel();
        final BrandModel brand = null;
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_QUALIFIER_CATEGORY);
        endecaCrumbs.add(endecaBreadCrumb);
        searchResults.setBreadCrumb(endecaCrumbs);
        final Breadcrumb breadCrumb = new Breadcrumb("testUrl", "testName", "testLinkClass");

        doNothing().when(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        doReturn(breadCrumb).when(searchBreadcrumbBuilder)
                .getDealCategoryBreadcrumb(Mockito.any(TargetDealCategoryModel.class), Mockito.anyString());
        doNothing().when(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);

        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        verify(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        verify(searchBreadcrumbBuilder).getDealCategoryBreadcrumb(Mockito.any(TargetDealCategoryModel.class),
                Mockito.anyString());
        verify(searchBreadcrumbBuilder, Mockito.times(0))
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);
        assertThat(breadcrumbs.size()).isEqualTo(1);

    }

    @Test
    public void testGetBreadcrumbsWithNullBrandTargetDealCategoryWithDimensionDealRewardCategory() {
        final CategoryModel category = new TargetDealCategoryModel();
        final BrandModel brand = null;
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY);
        endecaCrumbs.add(endecaBreadCrumb);
        searchResults.setBreadCrumb(endecaCrumbs);
        final Breadcrumb breadCrumb = new Breadcrumb("testUrl", "testName", "testLinkClass");

        doNothing().when(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        doReturn(breadCrumb).when(searchBreadcrumbBuilder)
                .getDealCategoryBreadcrumb(Mockito.any(TargetDealCategoryModel.class), Mockito.anyString());
        doNothing().when(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);

        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        verify(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        verify(searchBreadcrumbBuilder).getDealCategoryBreadcrumb(Mockito.any(TargetDealCategoryModel.class),
                Mockito.anyString());
        verify(searchBreadcrumbBuilder, Mockito.times(0))
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);
        assertThat(breadcrumbs.size()).isEqualTo(1);

    }

    @Test
    public void testGetBreadcrumbsWithBrandAndCategory() {
        final CategoryModel category = new TargetDealCategoryModel();
        final BrandModel brand = new BrandModel();
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<BreadcrumbData<EndecaSearchStateData>>();
        final String freeTextSearch = "testFreeTextSearch";
        final EndecaSearch searchResults = new EndecaSearch();
        final List<EndecaBreadCrumb> endecaCrumbs = new ArrayList<>();
        final EndecaBreadCrumb endecaBreadCrumb = new EndecaBreadCrumb();
        endecaBreadCrumb.setDimensionType(EndecaConstants.EndecaRecordSpecificFields.ENDECA_DEAL_REWARD_CATEGORY);
        endecaCrumbs.add(endecaBreadCrumb);
        searchResults.setBreadCrumb(endecaCrumbs);
        final Breadcrumb breadCrumb = new Breadcrumb("testUrl", "testName", "testLinkClass");

        doNothing().when(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        doReturn(breadCrumb).when(searchBreadcrumbBuilder)
                .getBrandBreadcrumb(Mockito.any(BrandModel.class), Mockito.anyString());
        doNothing().when(searchBreadcrumbBuilder)
                .populateRefinements(category, brand, endecaBreadCrumb, refinements);
        doNothing().when(searchBreadcrumbBuilder).createCategoryBreadCrumbForBrand(Mockito.any(BrandModel.class),
                Mockito.anyString(),
                Mockito.anyList(), Mockito.any(EndecaBreadCrumb.class));

        final List<Breadcrumb> breadcrumbs = searchBreadcrumbBuilder.getBreadcrumbs(category, brand, refinements,
                freeTextSearch, searchResults);
        verify(searchBreadcrumbBuilder).convertDealBreadCrumbData(endecaCrumbs);
        verify(searchBreadcrumbBuilder).getBrandBreadcrumb(Mockito.any(BrandModel.class), Mockito.anyString());
        verify(searchBreadcrumbBuilder)
                .createCategoryBreadCrumbForBrand(brand, "testUrl", Arrays.asList(breadCrumb), endecaBreadCrumb);
        assertThat(breadcrumbs.size()).isEqualTo(1);
    }

    @Test
    public void testCreateCategoryBreadCrumbForSearch() {
        final Breadcrumb breadcrumb = mock(Breadcrumb.class);
        given(searchBreadcrumbBuilder.getCategoryBreadcrumbForSearch(Mockito.anyString(), Mockito.anyString(),
                Mockito.anyString(), Mockito.anyString())).willReturn(breadcrumb);
        final List<Breadcrumb> categoryBreadcrumbs = new ArrayList<>();
        final EndecaBreadCrumb crumbs = mock(EndecaBreadCrumb.class);
        given(crumbs.getCode()).willReturn("crumbCode");
        given(crumbs.getElement()).willReturn("crumbElement");
        searchBreadcrumbBuilder.createCategoryBreadCrumbForSearch(categoryBreadcrumbs, crumbs, "searchText");
        verify(searchBreadcrumbBuilder).getCategoryBreadcrumbForSearch("crumbCode", "crumbElement",
                "searchText", "active");
        assertThat(categoryBreadcrumbs).containsExactly(breadcrumb);
    }

    @Test
    public void testCreateCategoryBreadCrumbForBrand() {
        final BrandModel brand = mock(BrandModel.class);
        given(brand.getName()).willReturn("Piping Hot");
        final String brandUrl = "/b/target";
        final List<Breadcrumb> categoryBreadcrumbs = new ArrayList<>();
        final EndecaBreadCrumb crumb = mock(EndecaBreadCrumb.class);
        final List<EndecaAncestor> ancestors = new ArrayList<>();
        final EndecaAncestor ancestor1 = mock(EndecaAncestor.class);
        given(ancestor1.getCode()).willReturn("W1");
        given(ancestor1.getElement()).willReturn("C1");
        ancestors.add(ancestor1);
        final EndecaAncestor ancestor2 = mock(EndecaAncestor.class);
        given(ancestor2.getCode()).willReturn("W2");
        given(ancestor2.getElement()).willReturn("C2");
        ancestors.add(ancestor2);
        given(crumb.getAncestor()).willReturn(ancestors);
        given(crumb.getCode()).willReturn("W3");
        given(crumb.getElement()).willReturn("C3");
        doReturn("?N=W1").when(searchBreadcrumbBuilder).getNavState("W1", "Piping Hot");
        doReturn("?N=W2").when(searchBreadcrumbBuilder).getNavState("W2", "Piping Hot");
        doReturn("?N=W3").when(searchBreadcrumbBuilder).getNavState("W3", "Piping Hot");
        searchBreadcrumbBuilder.createCategoryBreadCrumbForBrand(brand, brandUrl, categoryBreadcrumbs, crumb);
        assertThat(categoryBreadcrumbs).isNotEmpty().hasSize(3);
        assertThat(categoryBreadcrumbs).onProperty("url").containsExactly("/b/target?N=W1", "/b/target?N=W2",
                "/b/target?N=W3");
        assertThat(categoryBreadcrumbs).onProperty("name").containsExactly("C1", "C2", "C3");
        assertThat(categoryBreadcrumbs).onProperty("linkClass").containsExactly(null, null, "active");
    }
}

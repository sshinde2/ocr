/**
 * 
 */
package au.com.target.tgtstorefront.comparators;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.product.data.TargetProductSizeData;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductSizeAttributeComparatorTest {

    private final ProductSizeAttributeComparator comparator = new ProductSizeAttributeComparator();

    @Test
    public void testCompareWithBothProductSizeNull() {
        final int compared = comparator.compare(null, null);
        Assertions.assertThat(compared).isEqualTo(0);
    }

    @Test
    public void testCompareWithOneProductSizeNull() {
        final int compared = comparator.compare(new TargetProductSizeData(), null);
        Assertions.assertThat(compared).isEqualTo(-1);
    }

    @Test
    public void testCompareWithOtherOneProductSizeNull() {
        final int compared = comparator.compare(null, new TargetProductSizeData());
        Assertions.assertThat(compared).isEqualTo(1);
    }

    @Test
    public void testCompareWithProductSizeWithBothPositionNull() {
        final TargetProductSizeData targetProductSizeData1 = new TargetProductSizeData();
        final TargetProductSizeData targetProductSizeData2 = new TargetProductSizeData();
        final int compared = comparator.compare(targetProductSizeData1, targetProductSizeData2);
        Assertions.assertThat(compared).isEqualTo(0);
    }

    @Test
    public void testCompareWithOneProductSizeWithPositionNull() {
        final TargetProductSizeData targetProductSizeData1 = new TargetProductSizeData();
        final TargetProductSizeData targetProductSizeData2 = new TargetProductSizeData();
        targetProductSizeData2.setPosition(Integer.valueOf(1));
        final int compared = comparator.compare(targetProductSizeData1, targetProductSizeData2);
        Assertions.assertThat(compared).isEqualTo(1);
    }

    @Test
    public void testCompareWithOtherOneProductSizeWithPositionNull() {
        final TargetProductSizeData targetProductSizeData1 = new TargetProductSizeData();
        targetProductSizeData1.setPosition(Integer.valueOf(1));
        final TargetProductSizeData targetProductSizeData2 = new TargetProductSizeData();
        final int compared = comparator.compare(targetProductSizeData1, targetProductSizeData2);
        Assertions.assertThat(compared).isEqualTo(-1);
    }

    @Test
    public void testCompareWithOtherOneProductSizeWithValidPositionsFirstBeforeSecond() {
        final TargetProductSizeData targetProductSizeData1 = new TargetProductSizeData();
        targetProductSizeData1.setPosition(Integer.valueOf(1));
        final TargetProductSizeData targetProductSizeData2 = new TargetProductSizeData();
        targetProductSizeData2.setPosition(Integer.valueOf(5));
        final int compared = comparator.compare(targetProductSizeData1, targetProductSizeData2);
        Assertions.assertThat(compared).isEqualTo(-1);
    }

    @Test
    public void testCompareWithOtherOneProductSizeWithValidPositionsFirstAfterSecond() {
        final TargetProductSizeData targetProductSizeData1 = new TargetProductSizeData();
        targetProductSizeData1.setPosition(Integer.valueOf(4));
        final TargetProductSizeData targetProductSizeData2 = new TargetProductSizeData();
        targetProductSizeData2.setPosition(Integer.valueOf(3));
        final int compared = comparator.compare(targetProductSizeData1, targetProductSizeData2);
        Assertions.assertThat(compared).isEqualTo(1);
    }

    @Test
    public void testCompareWithOtherOneProductSizeWithValidPositionsBothAtSamePosition() {
        final TargetProductSizeData targetProductSizeData1 = new TargetProductSizeData();
        targetProductSizeData1.setPosition(Integer.valueOf(4));
        final TargetProductSizeData targetProductSizeData2 = new TargetProductSizeData();
        targetProductSizeData2.setPosition(Integer.valueOf(4));
        final int compared = comparator.compare(targetProductSizeData1, targetProductSizeData2);
        Assertions.assertThat(compared).isEqualTo(0);
    }
}

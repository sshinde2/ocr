/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import static org.mockito.Mockito.mock;

import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.preview.CMSPreviewTicketModel;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cms2.model.restrictions.AbstractRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.CMSDataFactory;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.CMSContentSlotService;
import de.hybris.platform.cms2.servicelayer.services.CMSPreviewService;
import de.hybris.platform.cms2.servicelayer.services.CMSRestrictionService;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtwebcore.model.cms2.components.ArticleItemComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.ArticleListComponentModel;



/**
 * @author asingh78
 * 
 */
public class ArticleListComponentControllerTest {


    private static final String CATALOG = "catalogId";
    private static final String CURRENT_CATEGORY_CODE = "currentCategoryCode";
    private static final String CURRENT_PRODUCT_CODE = "currentProductCode";
    private static final String CMS_TICKET_ID = "cmsTicketId";
    @Mock
    private CMSRestrictionService cmsRestrictionService;

    @Mock
    private CMSContentSlotService cmsContentSlotService;
    @Mock
    private CMSDataFactory cmsDataFactory;

    @Mock
    private CMSPreviewService cmsPreviewService;

    @Mock
    private HttpServletRequest request;

    @InjectMocks
    private final ArticleListComponentController articleListComponentController = new ArticleListComponentController();

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

    }



    @Test
    public void simpleCMSComponentsPreviewModeOffVisibleNoRestrictionTest() {

        final ArticleListComponentModel articleListComponentModel = Mockito.mock(ArticleListComponentModel.class);
        final ArticleItemComponentModel articleItemComponentModel1 = Mockito.mock(ArticleItemComponentModel.class);
        BDDMockito.given(articleItemComponentModel1.getVisible()).willReturn(
                Boolean.TRUE);
        final List<ArticleItemComponentModel> articleItemComponentList = new ArrayList<>();
        // final List<AbstractRestrictionModel> restrictionList = new ArrayList<>();
        articleItemComponentList.add(articleItemComponentModel1);

        final ArticleListComponentController componentController = Mockito.spy(articleListComponentController);
        Mockito.doReturn(Boolean.FALSE).when(componentController).isPreviewEnabled((BDDMockito
                .any(HttpServletRequest.class)));
        Mockito.doReturn(Boolean.TRUE).when(componentController)
                .isValidComponent(BDDMockito.any(AbstractCMSComponentModel.class));
        articleListComponentModel.setArticle(articleItemComponentList);
        BDDMockito.given(articleListComponentModel.getArticle()).willReturn(articleItemComponentList);
        BDDMockito.given(articleItemComponentModel1.getRestrictions()).willReturn(null);
        final List ret = componentController.getSimpleCMSComponents(articleListComponentModel, request);
        Assert.assertEquals(ret.size(), 1);

    }


    @Test
    public void simpleCMSComponentsPreviewModeOffVisibleRestrictionTest() {

        final ArticleListComponentModel articleListComponentModel = Mockito.mock(ArticleListComponentModel.class);
        final ArticleItemComponentModel articleItemComponentModel1 = Mockito.mock(ArticleItemComponentModel.class);
        final AbstractRestrictionModel abstractRestrictionModel = Mockito.mock(AbstractRestrictionModel.class);
        BDDMockito.given(articleItemComponentModel1.getVisible()).willReturn(
                Boolean.TRUE);
        final List<ArticleItemComponentModel> articleItemComponentList = new ArrayList<>();
        final List<AbstractRestrictionModel> restrictionList = new ArrayList<>();
        restrictionList.add(abstractRestrictionModel);

        articleItemComponentList.add(articleItemComponentModel1);

        final ArticleListComponentController componentController = Mockito.spy(articleListComponentController);
        Mockito.doReturn(Boolean.FALSE).when(componentController).isPreviewEnabled((BDDMockito
                .any(HttpServletRequest.class)));
        Mockito.doReturn(Boolean.TRUE).when(componentController)
                .isValidComponent(BDDMockito.any(AbstractCMSComponentModel.class));
        articleListComponentModel.setArticle(articleItemComponentList);
        BDDMockito.given(articleListComponentModel.getArticle()).willReturn(articleItemComponentList);
        BDDMockito.given(articleItemComponentModel1.getRestrictions()).willReturn(restrictionList);
        final List ret = componentController.getSimpleCMSComponents(articleListComponentModel, request);
        Assert.assertEquals(ret.size(), 0);

    }


    @Test
    public void simpleCMSComponentsPreviewModeOnVisibleRestrictionTest() {

        final ArticleListComponentModel articleListComponentModel = Mockito.mock(ArticleListComponentModel.class);
        final ArticleItemComponentModel articleItemComponentModel1 = Mockito.mock(ArticleItemComponentModel.class);
        final AbstractRestrictionModel abstractRestrictionModel = Mockito.mock(AbstractRestrictionModel.class);
        BDDMockito.given(articleItemComponentModel1.getVisible()).willReturn(
                Boolean.TRUE);
        final List<ArticleItemComponentModel> articleItemComponentList = new ArrayList<>();
        final List<AbstractRestrictionModel> restrictionList = new ArrayList<>();
        restrictionList.add(abstractRestrictionModel);

        articleItemComponentList.add(articleItemComponentModel1);

        final ArticleListComponentController componentController = Mockito.spy(articleListComponentController);
        Mockito.doReturn(Boolean.TRUE).when(componentController).isPreviewEnabled((BDDMockito
                .any(HttpServletRequest.class)));
        Mockito.doReturn(Boolean.TRUE).when(componentController)
                .isValidComponent(BDDMockito.any(AbstractCMSComponentModel.class));
        articleListComponentModel.setArticle(articleItemComponentList);
        BDDMockito.given(articleListComponentModel.getArticle()).willReturn(articleItemComponentList);
        BDDMockito.given(articleItemComponentModel1.getRestrictions()).willReturn(restrictionList);
        final List ret = componentController.getSimpleCMSComponents(articleListComponentModel, request);
        Assert.assertEquals(ret.size(), 1);

    }



    @Test
    public void simpleCMSComponentsPreviewModeOnVisibleOffRestrictionTest() {

        final ArticleListComponentModel articleListComponentModel = Mockito.mock(ArticleListComponentModel.class);
        final ArticleItemComponentModel articleItemComponentModel1 = Mockito.mock(ArticleItemComponentModel.class);
        final AbstractRestrictionModel abstractRestrictionModel = Mockito.mock(AbstractRestrictionModel.class);
        BDDMockito.given(articleItemComponentModel1.getVisible()).willReturn(
                Boolean.FALSE);
        final List<ArticleItemComponentModel> articleItemComponentList = new ArrayList<>();
        final List<AbstractRestrictionModel> restrictionList = new ArrayList<>();
        restrictionList.add(abstractRestrictionModel);

        articleItemComponentList.add(articleItemComponentModel1);

        final ArticleListComponentController componentController = Mockito.spy(articleListComponentController);
        Mockito.doReturn(Boolean.TRUE).when(componentController).isPreviewEnabled((BDDMockito
                .any(HttpServletRequest.class)));
        Mockito.doReturn(Boolean.TRUE).when(componentController)
                .isValidComponent(BDDMockito.any(AbstractCMSComponentModel.class));
        articleListComponentModel.setArticle(articleItemComponentList);
        BDDMockito.given(articleListComponentModel.getArticle()).willReturn(articleItemComponentList);
        BDDMockito.given(articleItemComponentModel1.getRestrictions()).willReturn(restrictionList);
        final List ret = componentController.getSimpleCMSComponents(articleListComponentModel, request);
        Assert.assertEquals(ret.size(), 0);

    }


    @Test
    public void simpleCMSComponentsPreviewModeOnVisibleOnNotValidRestrictionTest() {

        final ArticleListComponentModel articleListComponentModel = Mockito.mock(ArticleListComponentModel.class);
        final ArticleItemComponentModel articleItemComponentModel1 = mock(ArticleItemComponentModel.class);
        final AbstractRestrictionModel abstractRestrictionModel = mock(AbstractRestrictionModel.class);
        BDDMockito.given(articleItemComponentModel1.getVisible()).willReturn(
                Boolean.FALSE);
        final List<ArticleItemComponentModel> articleItemComponentList = new ArrayList<>();
        final List<AbstractRestrictionModel> restrictionList = new ArrayList<>();
        restrictionList.add(abstractRestrictionModel);

        articleItemComponentList.add(articleItemComponentModel1);

        final ArticleListComponentController componentController = Mockito.spy(articleListComponentController);
        Mockito.doReturn(Boolean.TRUE).when(componentController).isPreviewEnabled((BDDMockito
                .any(HttpServletRequest.class)));
        Mockito.doReturn(Boolean.FALSE).when(componentController)
                .isValidComponent(BDDMockito.any(AbstractCMSComponentModel.class));
        articleListComponentModel.setArticle(articleItemComponentList);
        BDDMockito.given(articleListComponentModel.getArticle()).willReturn(articleItemComponentList);
        BDDMockito.given(articleItemComponentModel1.getRestrictions()).willReturn(restrictionList);
        final List ret = componentController.getSimpleCMSComponents(articleListComponentModel, request);
        Assert.assertEquals(ret.size(), 0);

    }



    @Test
    public void previewEnabledTest() {
        final CMSPreviewTicketModel previewTicket = mock(CMSPreviewTicketModel.class);
        final PreviewDataModel previewDataModel = mock(PreviewDataModel.class);
        BDDMockito.given(request.getParameter(CMS_TICKET_ID)).willReturn(CMS_TICKET_ID);
        BDDMockito.given(cmsPreviewService.getPreviewTicket(BDDMockito.any(String.class))).willReturn(previewTicket);
        BDDMockito.given(previewTicket.getPreviewData()).willReturn(previewDataModel);
        BDDMockito.given(previewDataModel.getEditMode()).willReturn(Boolean.FALSE);
        Assert.assertTrue(articleListComponentController.isPreviewEnabled(request));
    }

    @Test
    public void previewEnabledWithOutTicketTest() {
        final CMSPreviewTicketModel previewTicket = mock(CMSPreviewTicketModel.class);
        final PreviewDataModel previewDataModel = mock(PreviewDataModel.class);
        BDDMockito.given(request.getParameter(CMS_TICKET_ID)).willReturn(null);
        BDDMockito.given(cmsPreviewService.getPreviewTicket(BDDMockito.any(String.class))).willReturn(previewTicket);
        BDDMockito.given(previewTicket.getPreviewData()).willReturn(previewDataModel);
        BDDMockito.given(previewDataModel.getEditMode()).willReturn(Boolean.FALSE);
        Assert.assertFalse(articleListComponentController.isPreviewEnabled(request));
    }

    @Test
    public void previewEnabledWithNoPreviewTicketTest() {
        final CMSPreviewTicketModel previewTicket = mock(CMSPreviewTicketModel.class);
        final PreviewDataModel previewDataModel = mock(PreviewDataModel.class);
        BDDMockito.given(request.getParameter(CMS_TICKET_ID)).willReturn(CMS_TICKET_ID);
        BDDMockito.given(cmsPreviewService.getPreviewTicket(BDDMockito.any(String.class))).willReturn(null);
        BDDMockito.given(previewTicket.getPreviewData()).willReturn(previewDataModel);
        BDDMockito.given(previewDataModel.getEditMode()).willReturn(Boolean.FALSE);
        Assert.assertFalse(articleListComponentController.isPreviewEnabled(request));
    }


    @Test
    public void previewEnabledWithEditModeTest() {
        final CMSPreviewTicketModel previewTicket = mock(CMSPreviewTicketModel.class);
        final PreviewDataModel previewDataModel = mock(PreviewDataModel.class);
        BDDMockito.given(request.getParameter(CMS_TICKET_ID)).willReturn(null);
        BDDMockito.given(cmsPreviewService.getPreviewTicket(BDDMockito.any(String.class))).willReturn(previewTicket);
        BDDMockito.given(previewTicket.getPreviewData()).willReturn(previewDataModel);
        BDDMockito.given(previewDataModel.getEditMode()).willReturn(Boolean.TRUE);
        Assert.assertFalse(articleListComponentController.isPreviewEnabled(request));
    }



    @Test
    public void validComponentWhenArticleItemHavingAllDataTest() {

        final ArticleItemComponentModel articleItemComponentModel = mock(ArticleItemComponentModel.class);
        final MediaModel mediaModel = mock(MediaModel.class);
        BDDMockito.given(articleItemComponentModel.getContent()).willReturn("testContent");
        BDDMockito.given(articleItemComponentModel.getHeadline()).willReturn("testHeadline");
        BDDMockito.given(articleItemComponentModel.getUrlLink()).willReturn("testUrl");
        BDDMockito.given(articleItemComponentModel.getMedia()).willReturn(mediaModel);
        BDDMockito.given(mediaModel.getURL()).willReturn("testURL");
        Assert.assertTrue(articleListComponentController.isValidComponent(articleItemComponentModel));
    }


    @Test
    public void validComponentWhenArticleItemWithOutContentDataTest() {

        final ArticleItemComponentModel articleItemComponentModel = mock(ArticleItemComponentModel.class);
        final MediaModel mediaModel = mock(MediaModel.class);
        BDDMockito.given(articleItemComponentModel.getContent()).willReturn(null);
        BDDMockito.given(articleItemComponentModel.getHeadline()).willReturn("testHeadline");
        BDDMockito.given(articleItemComponentModel.getUrlLink()).willReturn("testUrl");
        BDDMockito.given(articleItemComponentModel.getMedia()).willReturn(mediaModel);
        BDDMockito.given(mediaModel.getURL()).willReturn("testURL");
        Assert.assertFalse(articleListComponentController.isValidComponent(articleItemComponentModel));
    }

    @Test
    public void validComponentWhenArticleItemWithOutHeadLineTest() {

        final ArticleItemComponentModel articleItemComponentModel = mock(ArticleItemComponentModel.class);
        final MediaModel mediaModel = mock(MediaModel.class);
        BDDMockito.given(articleItemComponentModel.getContent()).willReturn("testContent");
        BDDMockito.given(articleItemComponentModel.getHeadline()).willReturn(null);
        BDDMockito.given(articleItemComponentModel.getUrlLink()).willReturn("testUrl");
        BDDMockito.given(articleItemComponentModel.getMedia()).willReturn(mediaModel);
        BDDMockito.given(mediaModel.getURL()).willReturn("testURL");
        Assert.assertFalse(articleListComponentController.isValidComponent(articleItemComponentModel));
    }

    @Test
    public void validComponentWhenArticleItemWithOutURLHavingContentPageLineTest() {

        final ArticleItemComponentModel articleItemComponentModel = mock(ArticleItemComponentModel.class);
        final MediaModel mediaModel = mock(MediaModel.class);
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        BDDMockito.given(articleItemComponentModel.getContent()).willReturn("testContent");
        BDDMockito.given(articleItemComponentModel.getHeadline()).willReturn("testHeadline");
        BDDMockito.given(articleItemComponentModel.getUrlLink()).willReturn(null);
        BDDMockito.given(articleItemComponentModel.getPage()).willReturn(contentPageModel);
        BDDMockito.given(articleItemComponentModel.getMedia()).willReturn(mediaModel);
        BDDMockito.given(mediaModel.getURL()).willReturn("testURL");
        Assert.assertTrue(articleListComponentController.isValidComponent(articleItemComponentModel));
    }


    @Test
    public void validComponentWhenArticleItemWithOutURLWithOutContentPageLineTest() {

        final ArticleItemComponentModel articleItemComponentModel = mock(ArticleItemComponentModel.class);
        final MediaModel mediaModel = mock(MediaModel.class);

        BDDMockito.given(articleItemComponentModel.getContent()).willReturn("testContent");
        BDDMockito.given(articleItemComponentModel.getHeadline()).willReturn("testHeadline");
        BDDMockito.given(articleItemComponentModel.getUrlLink()).willReturn(null);
        BDDMockito.given(articleItemComponentModel.getPage()).willReturn(null);
        BDDMockito.given(articleItemComponentModel.getMedia()).willReturn(mediaModel);
        BDDMockito.given(mediaModel.getURL()).willReturn("testURL");
        Assert.assertFalse(articleListComponentController.isValidComponent(articleItemComponentModel));
    }




    @Test
    public void populateTest() {
        final RestrictionData restrictionData = mock(RestrictionData.class);
        BDDMockito.given(request.getAttribute(CATALOG)).willReturn("");
        BDDMockito.given(request.getAttribute(CURRENT_CATEGORY_CODE)).willReturn("");
        BDDMockito.given(request.getAttribute(CURRENT_PRODUCT_CODE)).willReturn("");

        BDDMockito.given(
                cmsDataFactory.createRestrictionData(BDDMockito.any(String.class), BDDMockito.any(String.class),
                        BDDMockito.any(String.class)))
                .willReturn(restrictionData);
        articleListComponentController.populate(request);
        BDDMockito.verify(cmsDataFactory).createRestrictionData(BDDMockito.any(String.class),
                BDDMockito.any(String.class),
                BDDMockito.any(String.class));
    }

    @Test
    public void populateRequestAttributContainingDiffrentObjectTest() {
        final RestrictionData restrictionData = mock(RestrictionData.class);
        BDDMockito.given(request.getAttribute(CATALOG)).willReturn(null);
        BDDMockito.given(request.getAttribute(CURRENT_CATEGORY_CODE)).willReturn(null);
        BDDMockito.given(request.getAttribute(CURRENT_PRODUCT_CODE)).willReturn(null);

        BDDMockito.given(
                cmsDataFactory.createRestrictionData(BDDMockito.any(String.class), BDDMockito.any(String.class),
                        BDDMockito.any(String.class)))
                .willReturn(restrictionData);
        articleListComponentController.populate(request);
        BDDMockito.verify(cmsDataFactory).createRestrictionData(BDDMockito.any(String.class),
                BDDMockito.any(String.class),
                BDDMockito.any(String.class));
    }


}

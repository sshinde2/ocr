/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.acceleratorcms.enums.NavigationBarMenuLayout;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSComponentService;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtstorefront.controllers.util.NavigationNodeHelper;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuColumn;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuDepartment;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuSection;
import au.com.target.tgtwebcore.enums.NavigationNodeTypeEnum;


/**
 * Unit test for {@link NavigationBarComponentController}
 */
public class NavigationBarComponentControllerTest
{
    @InjectMocks
    private final NavigationBarComponentController navigationBarComponentController = new NavigationBarComponentController();

    @Mock
    private Converter<CategoryModel, CategoryData> mockCategoryConverter;
    @Mock
    private UrlResolver<CategoryData> mockCategoryDataUrlResolver;
    @Mock
    private DefaultCMSComponentService cmsComponentService;
    @Mock
    private Model mockModel;
    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpServletResponse mockResponse;
    @Mock
    private NavigationBarComponentModel mockComponent;
    @Mock
    private Configuration mockConfiguration;
    @Mock
    private ConfigurationService mockConfigurationService;
    @Mock
    private NavigationNodeHelper navigationNodeHelper;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @SuppressWarnings("boxing")
    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

        BDDMockito.given(mockConfiguration.getInt(Mockito.eq("megamenu.max.column"), Mockito.anyInt())).willReturn(4);
        BDDMockito.given(mockConfigurationService.getConfiguration()).willReturn(mockConfiguration);
    }

    @Test
    public void testFillModelCheckDropDownLayoutValue() {
        BDDMockito.given(mockComponent.getDropDownLayout()).willReturn(NavigationBarMenuLayout.AUTO);

        final Model model = new ExtendedModelMap();

        navigationBarComponentController.fillModel(mockRequest, model, mockComponent);

        Assert.assertTrue(model.containsAttribute("dropDownLayout"));
        Assert.assertEquals(NavigationBarMenuLayout.AUTO.getCode().toLowerCase(), model.asMap().get("dropDownLayout"));
    }

    @Test
    public void testFillModelCheckNoNavigationNode() {
        final Model model = new ExtendedModelMap();

        navigationBarComponentController.fillModel(mockRequest, model, mockComponent);

        Assert.assertTrue(model.containsAttribute("departments"));
        Assert.assertEquals(new ArrayList<MegaMenuDepartment>(), model.asMap().get("departments"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testFillModelCheckNavigationNodeWithNoChildren() {
        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNode);

        final Model model = new ExtendedModelMap();

        navigationBarComponentController.fillModel(mockRequest, model, mockComponent);

        Assert.assertTrue(model.containsAttribute("departments"));
        Assert.assertEquals(new ArrayList<MegaMenuDepartment>(), model.asMap().get("departments"));
    }

    @Ignore
    @SuppressWarnings("boxing")
    @Test
    public void testFillModelCheckNavigationNodeWithOneChild() {
        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNode);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final Model model = new ExtendedModelMap();

        navigationBarComponentController.fillModel(mockRequest, model, mockComponent);

        Assert.assertTrue(model.containsAttribute("departments"));
        final List<MegaMenuDepartment> departmentList = (List<MegaMenuDepartment>)model.asMap().get("departments");

        Assert.assertNotNull(departmentList);
        Assert.assertTrue(departmentList.size() == 1);
    }

    @Ignore
    @SuppressWarnings("boxing")
    @Test
    public void testFillModelCheckNavigationNodeWithTwoChildren() {
        final CMSNavigationNodeModel mockChildNavigationNode1 = Mockito.mock(CMSNavigationNodeModel.class);
        final CMSNavigationNodeModel mockChildNavigationNode2 = Mockito.mock(CMSNavigationNodeModel.class);

        final List<CMSNavigationNodeModel> departmentList = new ArrayList<>();
        departmentList.add(mockChildNavigationNode1);
        departmentList.add(mockChildNavigationNode2);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNode);

        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(departmentList);

        final Model model = new ExtendedModelMap();

        navigationBarComponentController.fillModel(mockRequest, model, mockComponent);

        Assert.assertTrue(model.containsAttribute("departments"));
        final List<MegaMenuDepartment> returnedDepartmentList = (List<MegaMenuDepartment>)model.asMap().get(
                "departments");

        Assert.assertNotNull(returnedDepartmentList);
        Assert.assertTrue(returnedDepartmentList.size() == 2);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testFillModelWithNotVisibleNavigationNode() {
        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.isVisible()).willReturn(false);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNode);

        final Model model = new ExtendedModelMap();

        navigationBarComponentController.fillModel(mockRequest, model, mockComponent);

        Assert.assertTrue(model.containsAttribute("departments"));
        Assert.assertEquals(new ArrayList<MegaMenuDepartment>(), model.asMap().get("departments"));
    }

    /* Departments */

    @Test
    public void testBuildDepartmentWithNoColumns() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getTitle()).willReturn(departmentTitle);

        final NavigationBarComponentController controller = Mockito.spy(navigationBarComponentController);
        Mockito.doReturn(departmentUrl).when(controller).getNavigationNodeLink(mockNavigationNode);

        final MegaMenuDepartment megaMenuDepartment = Mockito.mock(MegaMenuDepartment.class);
        BDDMockito.given(megaMenuDepartment.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(megaMenuDepartment.getLink()).willReturn(departmentUrl);
        BDDMockito.given(navigationNodeHelper.buildDepartment(mockNavigationNode, true)).willReturn(megaMenuDepartment);

        final MegaMenuDepartment department = controller.buildDepartment(mockNavigationNode);

        Assert.assertNotNull(department);
        Assert.assertEquals(departmentTitle, department.getTitle());
        Assert.assertEquals(departmentUrl, department.getLink());

        Assert.assertNotNull(department.getColumns());
        Assert.assertTrue(CollectionUtils.isEmpty(department.getColumns()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBuildDepartmentWithNotVisibleColumn() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode.isVisible()).willReturn(false);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final NavigationBarComponentController controller = Mockito.spy(navigationBarComponentController);
        Mockito.doReturn(departmentUrl).when(controller).getNavigationNodeLink(mockNavigationNode);

        final MegaMenuDepartment megaMenuDepartment = Mockito.mock(MegaMenuDepartment.class);
        BDDMockito.given(megaMenuDepartment.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(megaMenuDepartment.getLink()).willReturn(departmentUrl);
        BDDMockito.given(navigationNodeHelper.buildDepartment(mockNavigationNode, true)).willReturn(megaMenuDepartment);

        final MegaMenuDepartment department = controller.buildDepartment(mockNavigationNode);

        Assert.assertNotNull(department);
        Assert.assertEquals(departmentTitle, department.getTitle());
        Assert.assertEquals(departmentUrl, department.getLink());

        Assert.assertNotNull(department.getColumns());
        Assert.assertTrue(CollectionUtils.isEmpty(department.getColumns()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBuildDepartmentWithVisibleNotColumn() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        BDDMockito.given(mockChildNavigationNode.isVisible()).willReturn(true);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final NavigationBarComponentController controller = Mockito.spy(navigationBarComponentController);
        Mockito.doReturn(departmentUrl).when(controller).getNavigationNodeLink(mockNavigationNode);

        final MegaMenuDepartment megaMenuDepartment = Mockito.mock(MegaMenuDepartment.class);
        BDDMockito.given(megaMenuDepartment.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(megaMenuDepartment.getLink()).willReturn(departmentUrl);
        BDDMockito.given(navigationNodeHelper.buildDepartment(mockNavigationNode, true)).willReturn(megaMenuDepartment);

        final MegaMenuDepartment department = controller.buildDepartment(mockNavigationNode);

        Assert.assertNotNull(department);
        Assert.assertEquals(departmentTitle, department.getTitle());
        Assert.assertEquals(departmentUrl, department.getLink());

        Assert.assertNotNull(department.getColumns());
        Assert.assertTrue(CollectionUtils.isEmpty(department.getColumns()));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBuildDepartmentWithSingleColumn() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode.isVisible()).willReturn(true);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final MegaMenuColumn megaMenuColumn = Mockito.mock(MegaMenuColumn.class);
        final MegaMenuSection megaMenuSection = Mockito.mock(MegaMenuSection.class);
        final List<MegaMenuSection> sections = new ArrayList<>();
        sections.add(megaMenuSection);
        BDDMockito.given(megaMenuColumn.getSections()).willReturn(sections);
        final NavigationBarComponentController controller = Mockito.spy(navigationBarComponentController);
        Mockito.doReturn(departmentUrl).when(controller).getNavigationNodeLink(mockNavigationNode);
        Mockito.doReturn(megaMenuColumn).when(navigationNodeHelper).buildColumn(mockChildNavigationNode);

        final List<MegaMenuColumn> megaMenuColumns = new ArrayList<>();
        megaMenuColumns.add(megaMenuColumn);

        final MegaMenuDepartment megaMenuDepartment = Mockito.mock(MegaMenuDepartment.class);
        BDDMockito.given(megaMenuDepartment.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(megaMenuDepartment.getLink()).willReturn(departmentUrl);
        BDDMockito.given(megaMenuDepartment.getColumns()).willReturn(megaMenuColumns);
        BDDMockito.given(navigationNodeHelper.buildDepartment(mockNavigationNode, true)).willReturn(megaMenuDepartment);

        final MegaMenuDepartment department = controller.buildDepartment(mockNavigationNode);

        Assert.assertNotNull(department);
        Assert.assertEquals(departmentTitle, department.getTitle());
        Assert.assertEquals(departmentUrl, department.getLink());

        Assert.assertNotNull(department.getColumns());
        Assert.assertTrue(department.getColumns().size() == 1);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBuildDepartmentWithTwoColumns() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockChildNavigationNode1 = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode1.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode1.isVisible()).willReturn(true);

        final CMSNavigationNodeModel mockChildNavigationNode2 = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode2.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode2.isVisible()).willReturn(true);

        final List<CMSNavigationNodeModel> childList = new ArrayList<>();
        childList.add(mockChildNavigationNode1);
        childList.add(mockChildNavigationNode2);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(childList);
        final MegaMenuColumn megaMenuColumn = Mockito.mock(MegaMenuColumn.class);
        final MegaMenuSection megaMenuSection = Mockito.mock(MegaMenuSection.class);
        final List<MegaMenuSection> sections = new ArrayList<>();
        sections.add(megaMenuSection);
        BDDMockito.given(megaMenuColumn.getSections()).willReturn(sections);
        final NavigationBarComponentController controller = Mockito.spy(navigationBarComponentController);
        Mockito.doReturn(departmentUrl).when(controller).getNavigationNodeLink(mockNavigationNode);
        Mockito.doReturn(megaMenuColumn).when(navigationNodeHelper)
                .buildColumn(BDDMockito.any(CMSNavigationNodeModel.class));

        final MegaMenuColumn megaMenuColumn2 = Mockito.mock(MegaMenuColumn.class);
        Mockito.doReturn(megaMenuColumn2).when(navigationNodeHelper)
                .buildColumn(BDDMockito.any(CMSNavigationNodeModel.class));

        final List<MegaMenuColumn> megaMenuColumns = new ArrayList<>();
        megaMenuColumns.add(megaMenuColumn);
        megaMenuColumns.add(megaMenuColumn2);

        final MegaMenuDepartment megaMenuDepartment = Mockito.mock(MegaMenuDepartment.class);
        BDDMockito.given(megaMenuDepartment.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(megaMenuDepartment.getLink()).willReturn(departmentUrl);
        BDDMockito.given(megaMenuDepartment.getColumns()).willReturn(megaMenuColumns);
        BDDMockito.given(navigationNodeHelper.buildDepartment(mockNavigationNode, true)).willReturn(megaMenuDepartment);

        final MegaMenuDepartment department = controller.buildDepartment(mockNavigationNode);

        Assert.assertNotNull(department);
        Assert.assertEquals(departmentTitle, department.getTitle());
        Assert.assertEquals(departmentUrl, department.getLink());

        Assert.assertNotNull(department.getColumns());
        Assert.assertTrue(department.getColumns().size() == 2);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testBuildDepartmentWithFiveColumns() {
        final String departmentTitle = "Department Title";
        final String departmentUrl = "/department/url";

        final CMSNavigationNodeModel mockChildNavigationNode1 = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode1.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode1.isVisible()).willReturn(true);

        final CMSNavigationNodeModel mockChildNavigationNode2 = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode2.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode2.isVisible()).willReturn(true);

        final CMSNavigationNodeModel mockChildNavigationNode3 = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode3.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode3.isVisible()).willReturn(true);

        final CMSNavigationNodeModel mockChildNavigationNode4 = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode4.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode4.isVisible()).willReturn(true);

        final CMSNavigationNodeModel mockChildNavigationNode5 = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode5.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode5.isVisible()).willReturn(true);

        final List<CMSNavigationNodeModel> childList = new ArrayList<>();
        childList.add(mockChildNavigationNode1);
        childList.add(mockChildNavigationNode2);
        childList.add(mockChildNavigationNode3);
        childList.add(mockChildNavigationNode4);
        childList.add(mockChildNavigationNode5);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(childList);

        final MegaMenuColumn megaMenuColumn = Mockito.mock(MegaMenuColumn.class);

        final MegaMenuSection megaMenuSection = Mockito.mock(MegaMenuSection.class);

        final List<MegaMenuSection> sections = new ArrayList<>();
        sections.add(megaMenuSection);
        BDDMockito.given(megaMenuColumn.getSections()).willReturn(sections);

        final NavigationBarComponentController controller = Mockito.spy(navigationBarComponentController);
        Mockito.doReturn(departmentUrl).when(controller).getNavigationNodeLink(mockNavigationNode);
        Mockito.doReturn(megaMenuColumn).when(navigationNodeHelper)
                .buildColumn(BDDMockito.any(CMSNavigationNodeModel.class));

        final List<MegaMenuColumn> megaMenuColumns = new ArrayList<>();
        megaMenuColumns.add(megaMenuColumn);

        final MegaMenuColumn megaMenuColumn2 = Mockito.mock(MegaMenuColumn.class);
        Mockito.doReturn(megaMenuColumn2).when(navigationNodeHelper)
                .buildColumn(BDDMockito.any(CMSNavigationNodeModel.class));
        megaMenuColumns.add(megaMenuColumn2);

        final MegaMenuColumn megaMenuColumn3 = Mockito.mock(MegaMenuColumn.class);
        Mockito.doReturn(megaMenuColumn2).when(navigationNodeHelper)
                .buildColumn(BDDMockito.any(CMSNavigationNodeModel.class));
        megaMenuColumns.add(megaMenuColumn3);

        final MegaMenuColumn megaMenuColumn4 = Mockito.mock(MegaMenuColumn.class);
        Mockito.doReturn(megaMenuColumn2).when(navigationNodeHelper)
                .buildColumn(BDDMockito.any(CMSNavigationNodeModel.class));
        megaMenuColumns.add(megaMenuColumn4);

        final MegaMenuDepartment megaMenuDepartment = Mockito.mock(MegaMenuDepartment.class);
        BDDMockito.given(megaMenuDepartment.getTitle()).willReturn(departmentTitle);
        BDDMockito.given(megaMenuDepartment.getLink()).willReturn(departmentUrl);
        BDDMockito.given(megaMenuDepartment.getColumns()).willReturn(megaMenuColumns);
        BDDMockito.given(navigationNodeHelper.buildDepartment(mockNavigationNode, true)).willReturn(megaMenuDepartment);

        final MegaMenuDepartment department = controller.buildDepartment(mockNavigationNode);

        Assert.assertNotNull(department);
        Assert.assertEquals(departmentTitle, department.getTitle());
        Assert.assertEquals(departmentUrl, department.getLink());

        Assert.assertNotNull(department.getColumns());
        Assert.assertTrue(department.getColumns().size() == 4);
    }

    /* Columns */




    /* Sections */


    @Test
    public void testGetNavigationNodeLinkWithNoEntries() {
        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        final String link = navigationBarComponentController.getNavigationNodeLink(mockNavigationNode);

        Assert.assertNull(link);
    }

    @Test
    public void testGetNavigationNodeLinkWithCategoryEntry() {
        final String categoryUrl = "/category/url";

        final CategoryModel mockCategoryModel = Mockito.mock(CategoryModel.class);
        final CategoryData mockCategoryData = Mockito.mock(CategoryData.class);

        BDDMockito.given(mockCategoryConverter.convert(mockCategoryModel)).willReturn(mockCategoryData);
        BDDMockito.given(mockCategoryDataUrlResolver.resolve(mockCategoryData)).willReturn(categoryUrl);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockCategoryModel);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));

        BDDMockito.given(navigationNodeHelper.getNavigationNodeLink(mockNavigationNode)).willReturn(categoryUrl);

        final String returnedCategoryUrl = navigationBarComponentController.getNavigationNodeLink(mockNavigationNode);

        Assert.assertEquals(categoryUrl, returnedCategoryUrl);
    }

    @Test
    public void testGetNavigationNodeLinkWithContentPageEntry() {
        final String contentPageLabel = "/contentPage/url";

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getLabel()).willReturn(contentPageLabel);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockContentPage);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));

        BDDMockito.given(navigationNodeHelper.getNavigationNodeLink(mockNavigationNode)).willReturn(contentPageLabel);

        final String returnedContentPageUrl = navigationBarComponentController
                .getNavigationNodeLink(mockNavigationNode);

        Assert.assertEquals(contentPageLabel, returnedContentPageUrl);
    }

    @Test
    public void testGetNavigationNodeLinkWithLinkEntry() {
        final String linkUrl = "/link/url";

        final CMSLinkComponentModel mockCmsLinkComponent = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockCmsLinkComponent.getUrl()).willReturn(linkUrl);
        BDDMockito.given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.TRUE);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));

        BDDMockito.given(navigationNodeHelper.getNavigationNodeLink(mockNavigationNode)).willReturn(linkUrl);

        final String returnedLinkUrl = navigationBarComponentController.getNavigationNodeLink(mockNavigationNode);

        Assert.assertEquals(linkUrl, returnedLinkUrl);
    }

    @Test
    public void testGetNavigationNodeLinkWithNotVisibleLinkEntry() {
        final String linkUrl = "/link/url";

        final CMSLinkComponentModel mockCmsLinkComponent = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockCmsLinkComponent.getUrl()).willReturn(linkUrl);
        BDDMockito.given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.FALSE);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));

        final String returnedLinkUrl = navigationBarComponentController.getNavigationNodeLink(mockNavigationNode);

        Assert.assertTrue(StringUtils.isBlank(returnedLinkUrl));
    }

    @Test
    public void testGetNavigationNodeLinkWithNotSupportedEntry() {
        final ItemModel mockItemModel = Mockito.mock(ItemModel.class);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockItemModel);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));

        final String returnedUrl = navigationBarComponentController.getNavigationNodeLink(mockNavigationNode);

        Assert.assertNull(returnedUrl);
    }


}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.navigation.NavigationComponentMenuBuilder;
import au.com.target.tgtwebcore.model.cms2.components.NavigationComponentModel;


/**
 * @author rmcalave
 * 
 */
public class NavigationComponentControllerTest {
    @InjectMocks
    private final NavigationComponentController navigationComponentController = new NavigationComponentController();

    @Mock
    private NavigationComponentMenuBuilder mockNavigationComponentMenuBuilder;

    @Mock
    private Model mockModel;

    @Mock
    private NavigationComponentModel mockComponent;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Ignore
    @Test
    public void testFillModelWithNoMenuItem() {
        BDDMockito.given(mockNavigationComponentMenuBuilder.createNavigationComponentMenu(mockComponent)).willReturn(
                null);

        navigationComponentController.fillModel(null, mockModel, mockComponent);
        Mockito.verifyZeroInteractions(mockModel);
    }

    @Test
    public void testFillModelWithMenuItem() {
        final NavigationMenuItem mockNavigationMenuItem = Mockito.mock(NavigationMenuItem.class);
        BDDMockito.given(mockNavigationComponentMenuBuilder.createNavigationComponentMenu(mockComponent)).willReturn(
                mockNavigationMenuItem);

        navigationComponentController.fillModel(null, mockModel, mockComponent);
        Mockito.verify(mockModel).addAttribute(WebConstants.NAVIGATION_COMPONENT_MENU_ITEM, mockNavigationMenuItem);
    }
}

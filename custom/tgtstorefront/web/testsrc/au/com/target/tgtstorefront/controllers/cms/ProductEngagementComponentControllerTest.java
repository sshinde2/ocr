/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2lib.model.components.BannerComponentModel;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.configuration.Configuration;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.endeca.navigation.ENEQueryException;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtwebcore.enums.RegionLayout;
import au.com.target.tgtwebcore.model.cms2.components.ProductEngagementComponentModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductEngagementComponentControllerTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration mockConfiguration;

    private Model model;

    @Mock
    private ProductEngagementComponentModel component;

    @Mock
    private EndecaDimensionCacheService endecaDimensionCacheService;

    @Mock
    private EndecaSearchStateData endecaSearchStateDataForBackFill;

    private List<TargetProductListerData> backFillProducts;

    @Mock
    private EndecaSearchStateData endecaSearchStateDataForProductsForLeftRegion;

    @Mock
    private EndecaSearchStateData endecaSearchStateDataForProductsForRightRegion;

    private List<TargetProductListerData> productsForLeftRegion;

    private List<TargetProductListerData> productsForRightRegion;

    private List<ProductModel> pdtModelsForLeftRegion;

    private List<ProductModel> pdtModelsForRightRegion;

    private CMSLinkComponentModel linkModel;

    @InjectMocks
    private final ProductEngagementComponentControllerMock productEngagementController = new ProductEngagementComponentControllerMock();

    private class ProductEngagementComponentControllerMock extends ProductEngagementComponentController {

        @Override
        protected EndecaSearchStateData collectCategoryAndBrandProducts(final ProductCarouselComponentModel componentModel,
                final int maxProducts, final EndecaSearchStateData searchStateData) {
            return endecaSearchStateDataForBackFill;
        }

        @Override
        protected EndecaSearchStateData getLinkedProducts(final List<ProductModel> productModels,
                final boolean isFeatuedComponent) {

            if (productModels.equals(pdtModelsForLeftRegion)) {
                return endecaSearchStateDataForProductsForLeftRegion;
            }
            else if (productModels.equals(pdtModelsForRightRegion)) {
                return endecaSearchStateDataForProductsForRightRegion;
            }
            return null;
        }

        @Override
        protected List<TargetProductListerData> getTargetProductsList(
                final EndecaSearchStateData searchStateData, final List<ProductModel> productModels,
                final boolean isFeatuedComponent, final boolean alwaysHideOutOfStock) {

            if (searchStateData.equals(endecaSearchStateDataForBackFill)) {
                return backFillProducts;
            }
            else if (searchStateData.equals(endecaSearchStateDataForProductsForLeftRegion)) {
                return productsForLeftRegion;
            }
            else if (searchStateData.equals(endecaSearchStateDataForProductsForRightRegion)) {
                return productsForRightRegion;
            }
            return null;
        }
    }



    @Before
    public void setup() throws ENEQueryException, TargetEndecaException {
        model = new ExtendedModelMap();
        given(configurationService.getConfiguration()).willReturn(mockConfiguration);

        BDDMockito.given(component.getTitle()).willReturn("title");
        linkModel = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(component.getLink()).willReturn(linkModel);


        final BannerComponentModel leftbanner1 = createBanner();
        BDDMockito.given(component.getLeftBanner1()).willReturn(leftbanner1);
        final BannerComponentModel leftbanner2 = createBanner();
        BDDMockito.given(component.getLeftBanner2()).willReturn(leftbanner2);
        final BannerComponentModel leftbanner3 = createBanner();
        BDDMockito.given(component.getLeftBanner3()).willReturn(leftbanner3);
        final BannerComponentModel leftbanner4 = createBanner();
        BDDMockito.given(component.getLeftBanner4()).willReturn(leftbanner4);

        final BannerComponentModel rightbanner1 = createBanner();
        BDDMockito.given(component.getRightBanner1()).willReturn(rightbanner1);
        final BannerComponentModel rightbanner2 = createBanner();
        BDDMockito.given(component.getRightBanner2()).willReturn(rightbanner2);
        final BannerComponentModel rightbanner3 = createBanner();
        BDDMockito.given(component.getRightBanner3()).willReturn(rightbanner3);
        final BannerComponentModel rightbanner4 = createBanner();
        BDDMockito.given(component.getRightBanner4()).willReturn(rightbanner4);

        final CategoryModel backFillCategory = Mockito.mock(CategoryModel.class);
        BDDMockito.given(backFillCategory.getCode()).willReturn("W2222");
        BDDMockito.given(endecaDimensionCacheService.getDimension(
                EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY, "W2222")).willReturn("N124");

        backFillProducts = null;
        productsForLeftRegion = null;
        productsForRightRegion = null;
    }


    private void setupBackFillProducts(final int numberofProducts) {
        backFillProducts = new ArrayList<>();
        for (int i = 0; i < numberofProducts; i++) {
            final TargetProductListerData pdt = Mockito.mock(TargetProductListerData.class);
            backFillProducts.add(pdt);
        }
    }

    private void setupProductsForLeftRegion(final int numberOfProducts) {
        //Products returned from  component and Endeca
        productsForLeftRegion = new ArrayList<>();
        pdtModelsForLeftRegion = new ArrayList<>();
        for (int i = 0; i < numberOfProducts; i++) {
            final ProductModel pdtModel = Mockito.mock(ProductModel.class);
            pdtModelsForLeftRegion.add(pdtModel);
            final TargetProductListerData pdt = Mockito.mock(TargetProductListerData.class);
            productsForLeftRegion.add(pdt);
        }
        BDDMockito.given(component.getProducts()).willReturn(pdtModelsForLeftRegion);
    }

    private void setupProductsForRightRegion(final int numberOfProducts) {
        //Products returned from  component and Endeca
        productsForRightRegion = new ArrayList<>();
        pdtModelsForRightRegion = new ArrayList<>();
        for (int i = 0; i < numberOfProducts; i++) {
            final ProductModel pdtModel = Mockito.mock(ProductModel.class);
            pdtModelsForRightRegion.add(pdtModel);
            final TargetProductListerData pdt = Mockito.mock(TargetProductListerData.class);
            productsForRightRegion.add(pdt);
        }
        BDDMockito.given(component.getProductsForRightRegion()).willReturn(pdtModelsForRightRegion);
    }

    private BannerComponentModel createBanner() {
        final BannerComponentModel banner = Mockito.mock(BannerComponentModel.class);
        BDDMockito.given(banner.getVisible()).willReturn(Boolean.TRUE);
        return banner;
    }

    private BannerComponentModel createNotAllowedBanner() {
        final BannerComponentModel banner = Mockito.mock(BannerComponentModel.class);
        BDDMockito.given(banner.getVisible()).willReturn(Boolean.FALSE);
        return banner;
    }

    @Test
    public void testLeftAndRightRegionHasOneRegionWithRightRegionHavingBannerOneNotVisible() {
        BDDMockito.given(component.getLeftRegionLayout()).willReturn(RegionLayout.ONE);
        BDDMockito.given(component.getRightRegionLayout()).willReturn(RegionLayout.ONE);
        final BannerComponentModel rightBannerNotVisible = createNotAllowedBanner();
        BDDMockito.given(component.getRightBanner1()).willReturn(rightBannerNotVisible);
        productEngagementController.fillModelInternal(request, model, component);

        final Map<String, Object> modelMap = model.asMap();
        Assertions.assertThat(modelMap.get("title")).isEqualTo("title");
        Assertions.assertThat(modelMap.get("link")).isEqualTo(linkModel);
        Assertions.assertThat(modelMap.get("rightRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> rightRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("rightRegionSlots");
        Assertions.assertThat(rightRegionSlots).isNotEmpty().hasSize(1);
        Assertions.assertThat(rightRegionSlots.get(0).getBanner()).isNotNull().isEqualTo(component.getRightBanner2());

        Assertions.assertThat(modelMap.get("leftRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> leftRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("leftRegionSlots");
        Assertions.assertThat(leftRegionSlots).isNotEmpty().hasSize(1);
        Assertions.assertThat(leftRegionSlots.get(0).getBanner()).isNotNull().isEqualTo(component.getLeftBanner1());

    }

    @Test
    public void testLeftRegionHas4SlotsWith3BannersNoProductsAndBackFilledByProductsAndRightRegionHasOneSlot() {
        BDDMockito.given(component.getLeftRegionLayout()).willReturn(RegionLayout.FOUR);
        BDDMockito.given(component.getRightRegionLayout()).willReturn(RegionLayout.ONE);
        BDDMockito.given(component.getLeftBanner4()).willReturn(null);
        setupBackFillProducts(4);

        productEngagementController.fillModelInternal(request, model, component);

        final Map<String, Object> modelMap = model.asMap();
        Assertions.assertThat(modelMap.get("title")).isEqualTo("title");
        Assertions.assertThat(modelMap.get("link")).isEqualTo(linkModel);
        Assertions.assertThat(modelMap.get("rightRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> rightRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("rightRegionSlots");

        Assertions.assertThat(rightRegionSlots).isNotEmpty().hasSize(1);
        Assertions.assertThat(rightRegionSlots.get(0).getBanner()).isNotNull().isEqualTo(component.getRightBanner1());

        Assertions.assertThat(modelMap.get("leftRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> leftRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("leftRegionSlots");
        Assertions.assertThat(leftRegionSlots).isNotEmpty().hasSize(4);
        Assertions.assertThat((leftRegionSlots.get(0).getBanner())).isEqualTo(component.getLeftBanner1());
        Assertions.assertThat((leftRegionSlots.get(1).getBanner())).isEqualTo(component.getLeftBanner2());
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isEqualTo(component.getLeftBanner3());
        Assertions.assertThat((leftRegionSlots.get(3).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(3).getProductData())).isEqualTo(backFillProducts.get(0));

    }

    @Test
    public void testLeftRegionHas4SlotsWith2Banners1ProductAndBackFilledBy1ProductsAndRightRegionHasOneSlot() {
        BDDMockito.given(component.getLeftRegionLayout()).willReturn(RegionLayout.FOUR);
        BDDMockito.given(component.getRightRegionLayout()).willReturn(RegionLayout.ONE);
        BDDMockito.given(component.getLeftBanner4()).willReturn(null);
        BDDMockito.given(component.getLeftBanner3()).willReturn(null);
        setupBackFillProducts(4);
        setupProductsForLeftRegion(1);

        productEngagementController.fillModelInternal(request, model, component);


        final Map<String, Object> modelMap = model.asMap();
        Assertions.assertThat(modelMap.get("title")).isEqualTo("title");
        Assertions.assertThat(modelMap.get("link")).isEqualTo(linkModel);
        Assertions.assertThat(modelMap.get("rightRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> rightRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("rightRegionSlots");

        Assertions.assertThat(rightRegionSlots).isNotEmpty().hasSize(1);
        Assertions.assertThat(rightRegionSlots.get(0).getBanner()).isNotNull().isEqualTo(component.getRightBanner1());

        Assertions.assertThat(modelMap.get("leftRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> leftRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("leftRegionSlots");
        Assertions.assertThat(leftRegionSlots).isNotEmpty().hasSize(4);
        Assertions.assertThat((leftRegionSlots.get(0).getBanner())).isEqualTo(component.getLeftBanner1());
        Assertions.assertThat((leftRegionSlots.get(1).getBanner())).isEqualTo(component.getLeftBanner2());
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(2).getProductData())).isEqualTo(productsForLeftRegion.get(0));
        Assertions.assertThat((leftRegionSlots.get(3).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(3).getProductData())).isEqualTo(backFillProducts.get(0));

    }

    @Test
    public void testLeftRegionHas4SlotsWith2Banners1ProductAndNoBackFillProductsAndRightRegionHasOneSlot() {
        BDDMockito.given(component.getLeftRegionLayout()).willReturn(RegionLayout.FOUR);
        BDDMockito.given(component.getRightRegionLayout()).willReturn(RegionLayout.ONE);
        BDDMockito.given(component.getLeftBanner4()).willReturn(null);
        BDDMockito.given(component.getLeftBanner3()).willReturn(null);
        setupProductsForLeftRegion(1);

        productEngagementController.fillModelInternal(request, model, component);


        final Map<String, Object> modelMap = model.asMap();
        Assertions.assertThat(modelMap.get("title")).isEqualTo("title");
        Assertions.assertThat(modelMap.get("link")).isEqualTo(linkModel);
        Assertions.assertThat(modelMap.get("rightRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> rightRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("rightRegionSlots");

        Assertions.assertThat(rightRegionSlots).isNotEmpty().hasSize(1);
        Assertions.assertThat(rightRegionSlots.get(0).getBanner()).isNotNull().isEqualTo(component.getRightBanner1());

        Assertions.assertThat(modelMap.get("leftRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> leftRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("leftRegionSlots");
        Assertions.assertThat(leftRegionSlots).isNotEmpty().hasSize(4);
        Assertions.assertThat((leftRegionSlots.get(0).getBanner())).isEqualTo(component.getLeftBanner1());
        Assertions.assertThat((leftRegionSlots.get(1).getBanner())).isEqualTo(component.getLeftBanner2());
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(2).getProductData())).isEqualTo(productsForLeftRegion.get(0));
        Assertions.assertThat((leftRegionSlots.get(3).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(3).getProductData())).isNull();

    }

    @Test
    public void testLeftRegionHas4SlotsWith2Banners1ProductAndOneBackFillProductsAndRightRegionHasFourSlotsWithNoBanners1ProductRestToBeBackFilled() {
        BDDMockito.given(component.getLeftRegionLayout()).willReturn(RegionLayout.FOUR);
        BDDMockito.given(component.getRightRegionLayout()).willReturn(RegionLayout.FOUR);
        BDDMockito.given(component.getLeftBanner4()).willReturn(null);
        BDDMockito.given(component.getLeftBanner3()).willReturn(null);

        BDDMockito.given(component.getRightBanner1()).willReturn(null);
        BDDMockito.given(component.getRightBanner2()).willReturn(null);
        BDDMockito.given(component.getRightBanner3()).willReturn(null);
        BDDMockito.given(component.getRightBanner4()).willReturn(null);
        setupBackFillProducts(20);
        setupProductsForLeftRegion(1);
        setupProductsForRightRegion(1);

        productEngagementController.fillModelInternal(request, model, component);

        final Map<String, Object> modelMap = model.asMap();
        Assertions.assertThat(modelMap.get("title")).isEqualTo("title");
        Assertions.assertThat(modelMap.get("link")).isEqualTo(linkModel);
        Assertions.assertThat(modelMap.get("rightRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> rightRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("rightRegionSlots");


        Assertions.assertThat(modelMap.get("leftRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> leftRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("leftRegionSlots");
        Assertions.assertThat(leftRegionSlots).isNotEmpty().hasSize(4);
        Assertions.assertThat((leftRegionSlots.get(0).getBanner())).isEqualTo(component.getLeftBanner1());
        Assertions.assertThat((leftRegionSlots.get(1).getBanner())).isEqualTo(component.getLeftBanner2());
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(2).getProductData())).isEqualTo(productsForLeftRegion.get(0));
        Assertions.assertThat((leftRegionSlots.get(3).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(3).getProductData())).isEqualTo(backFillProducts.get(0));

        Assertions.assertThat(rightRegionSlots).isNotEmpty().hasSize(4);
        Assertions.assertThat((rightRegionSlots.get(0).getProductData())).isEqualTo(productsForRightRegion.get(0));
        Assertions.assertThat(rightRegionSlots.get(0).getBanner()).isNull();
        Assertions.assertThat((rightRegionSlots.get(1).getProductData())).isEqualTo(backFillProducts.get(1));
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isNull();
        Assertions.assertThat((rightRegionSlots.get(2).getProductData())).isEqualTo(backFillProducts.get(2));
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isNull();
        Assertions.assertThat((rightRegionSlots.get(3).getProductData())).isEqualTo(backFillProducts.get(3));
        Assertions.assertThat((leftRegionSlots.get(3).getBanner())).isNull();
    }

    @Test
    public void testLeftRegionHas4SlotsWithNoBannersNoProductAndBackFillProductsAllAndRightRegionHasFourSlotsWithNoBannersNoProductRestToBeBackFilled() {
        BDDMockito.given(component.getLeftRegionLayout()).willReturn(RegionLayout.FOUR);
        BDDMockito.given(component.getRightRegionLayout()).willReturn(RegionLayout.FOUR);
        BDDMockito.given(component.getLeftBanner4()).willReturn(null);
        BDDMockito.given(component.getLeftBanner3()).willReturn(null);
        BDDMockito.given(component.getLeftBanner2()).willReturn(null);
        BDDMockito.given(component.getLeftBanner1()).willReturn(null);

        BDDMockito.given(component.getRightBanner1()).willReturn(null);
        BDDMockito.given(component.getRightBanner2()).willReturn(null);
        BDDMockito.given(component.getRightBanner3()).willReturn(null);
        BDDMockito.given(component.getRightBanner4()).willReturn(null);
        setupBackFillProducts(20);

        productEngagementController.fillModelInternal(request, model, component);

        final Map<String, Object> modelMap = model.asMap();
        Assertions.assertThat(modelMap.get("title")).isEqualTo("title");
        Assertions.assertThat(modelMap.get("link")).isEqualTo(linkModel);
        Assertions.assertThat(modelMap.get("rightRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> rightRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("rightRegionSlots");


        Assertions.assertThat(modelMap.get("leftRegionSlots")).isInstanceOfAny(ArrayList.class);
        final List<EngagementContainerSlotData> leftRegionSlots = (List<EngagementContainerSlotData>)modelMap
                .get("leftRegionSlots");
        Assertions.assertThat(leftRegionSlots).isNotEmpty().hasSize(4);
        Assertions.assertThat((leftRegionSlots.get(0).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(0).getProductData())).isEqualTo(backFillProducts.get(0));
        Assertions.assertThat((leftRegionSlots.get(1).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(1).getProductData())).isEqualTo(backFillProducts.get(1));
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(2).getProductData())).isEqualTo(backFillProducts.get(2));
        Assertions.assertThat((leftRegionSlots.get(3).getBanner())).isNull();
        Assertions.assertThat((leftRegionSlots.get(3).getProductData())).isEqualTo(backFillProducts.get(3));

        Assertions.assertThat(rightRegionSlots).isNotEmpty().hasSize(4);
        Assertions.assertThat((rightRegionSlots.get(0).getProductData())).isEqualTo(backFillProducts.get(4));
        Assertions.assertThat(rightRegionSlots.get(0).getBanner()).isNull();
        Assertions.assertThat((rightRegionSlots.get(1).getProductData())).isEqualTo(backFillProducts.get(5));
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isNull();
        Assertions.assertThat((rightRegionSlots.get(2).getProductData())).isEqualTo(backFillProducts.get(6));
        Assertions.assertThat((leftRegionSlots.get(2).getBanner())).isNull();
        Assertions.assertThat((rightRegionSlots.get(3).getProductData())).isEqualTo(backFillProducts.get(7));
        Assertions.assertThat((leftRegionSlots.get(3).getBanner())).isNull();
    }





}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtstorefront.controllers.cms;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.ProductReferencesComponentModel;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSComponentService;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.product.ProductService;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.Model;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.ENEQueryResults;

import au.com.target.endeca.infront.cache.EndecaDimensionCacheService;
import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.util.TargetProductDataHelper;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.pages.AbstractPageController;
import au.com.target.tgtstorefront.controllers.util.ProductDataHelper;
import junit.framework.Assert;


/**
 * Unit test for {@link ProductReferencesComponentController}
 */
@UnitTest
public class ProductReferencesComponentControllerTest {
    private static final String COMPONENT_UID = "componentUid";
    private static final String TEST_COMPONENT_UID = "componentUID";
    private static final String TEST_TYPE_CODE = "myTypeCode";
    private static final String TEST_TYPE_VIEW = ControllerConstants.Views.Cms.COMPONENT_PREFIX
            + StringUtils.lowerCase(TEST_TYPE_CODE);
    private static final String TEST_PRODUCT_CODE = "productCode";
    private static final String TITLE = "title";
    private static final String TITLE_VALUE = "Accessories";
    private static final String PRODUCT_REFERENCES = "productReferences";
    private static final String PRODUCT_LISTER_DATA = "productListerData";
    private static final String COMPONENT = "component";

    private ProductReferencesComponentController productReferencesComponentController;

    @Mock
    private ProductReferencesComponentModel productReferencesComponentModel;
    @Mock
    private Model model;
    @Mock
    private DefaultCMSComponentService cmsComponentService;
    @Mock
    private ProductFacade productFacade;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private ComposedTypeModel composedTypeModel;
    @Mock
    private ProductReferenceData productReferenceData;
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;
    @Mock
    private ProductService productService;
    @Mock
    private TargetSharedConfigFacade targetSharedConfigFacade;
    @Mock
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;
    @Mock
    private EndecaProductQueryBuilder endecaProductQueryBuilder;
    @Mock
    private TargetProductListerData targetProductListerData;
    @Mock
    private EndecaSearchStateData searchStateData;
    @Mock
    private TargetProductModel baseProductModel;
    @Mock
    private ProductModel productModel;
    @Mock
    private TargetColourVariantProductModel colourVariantModel;
    @Mock
    private TargetProductCategoryModel originalCategoryModel;
    @Mock
    private EndecaDimensionCacheService endecaDimensionCacheService;

    private final List<ProductReferenceData> productReferenceDataList = new ArrayList();
    private final List<TargetProductListerData> productListerDataList = new ArrayList();



    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        productReferenceDataList.add(productReferenceData);
        productListerDataList.add(targetProductListerData);
        productReferencesComponentController = new ProductReferencesComponentController();
        productReferencesComponentController.setCmsComponentService(cmsComponentService);
        ReflectionTestUtils.setField(productReferencesComponentController, "productFacade", productFacade);
        ReflectionTestUtils.setField(productReferencesComponentController, "targetFeatureSwitchFacade",
                targetFeatureSwitchFacade);
        ReflectionTestUtils.setField(productReferencesComponentController, "productService", productService);
        ReflectionTestUtils.setField(productReferencesComponentController, "targetSharedConfigFacade",
                targetSharedConfigFacade);
        ReflectionTestUtils.setField(productReferencesComponentController, "aggrERecToProductConverter",
                aggrERecToProductConverter);
        ReflectionTestUtils.setField(productReferencesComponentController, "endecaProductQueryBuilder",
                endecaProductQueryBuilder);

    }

    @Test
    public void testRenderComponent() throws Exception {
        final List<ProductReferenceTypeEnum> productReferenceTypeEnums = new ArrayList();
        productReferenceTypeEnums.add(ProductReferenceTypeEnum.ACCESSORIES);
        given(productReferencesComponentModel.getMaximumNumberProducts()).willReturn(Integer.valueOf(1));
        given(productReferencesComponentModel.getTitle()).willReturn(TITLE_VALUE);
        given(productReferencesComponentModel.getProductReferenceTypes()).willReturn(productReferenceTypeEnums);
        given(productReferencesComponentModel.getItemtype()).willReturn(TEST_TYPE_CODE);
        given(request.getAttribute(ProductDataHelper.CURRENT_PRODUCT)).willReturn(TEST_PRODUCT_CODE);
        given(
                productFacade.getProductReferencesForCode(Mockito.anyString(),
                        Mockito.anyListOf(ProductReferenceTypeEnum.class),
                        Mockito.any(List.class), Mockito.<Integer> any())).willReturn(productReferenceDataList);

        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isProductEndOfLifeCrossSellEnabled();
        given(request.getAttribute(ControllerConstants.REQUEST_PARAM_NOTAVAILABLE_ONLINE_AND_INSTORE))
                .willReturn(Boolean.FALSE);

        final String viewName = productReferencesComponentController.handleComponent(request, model,
                productReferencesComponentModel);
        verify(model, Mockito.times(1)).addAttribute(TITLE, TITLE_VALUE);
        verify(model, Mockito.times(1)).addAttribute(PRODUCT_REFERENCES, productReferenceDataList);
        Assert.assertEquals(TEST_TYPE_VIEW, viewName);
    }

    @Test
    public void testRenderComponentUid() throws Exception {
        final List<ProductReferenceTypeEnum> productReferenceTypeEnums = new ArrayList();
        productReferenceTypeEnums.add(ProductReferenceTypeEnum.ACCESSORIES);
        given(request.getAttribute(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
        given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID))
                .willReturn(productReferencesComponentModel);
        given(productReferencesComponentModel.getMaximumNumberProducts()).willReturn(Integer.valueOf(1));
        given(productReferencesComponentModel.getTitle()).willReturn(TITLE_VALUE);
        given(productReferencesComponentModel.getProductReferenceTypes()).willReturn(productReferenceTypeEnums);
        given(productReferencesComponentModel.getItemtype()).willReturn(TEST_TYPE_CODE);

        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isProductEndOfLifeCrossSellEnabled();
        given(request.getAttribute(ControllerConstants.REQUEST_PARAM_NOTAVAILABLE_ONLINE_AND_INSTORE))
                .willReturn(Boolean.FALSE);

        given(request.getAttribute(ProductDataHelper.CURRENT_PRODUCT)).willReturn(TEST_PRODUCT_CODE);
        given(
                productFacade.getProductReferencesForCode(Mockito.anyString(),
                        Mockito.anyListOf(ProductReferenceTypeEnum.class),
                        Mockito.any(List.class), Mockito.<Integer> any())).willReturn(productReferenceDataList);

        final String viewName = productReferencesComponentController.handleGet(request, model);
        verify(model, Mockito.times(1)).addAttribute(COMPONENT, productReferencesComponentModel);
        verify(model, Mockito.times(1)).addAttribute(TITLE, TITLE_VALUE);
        verify(model, Mockito.times(1)).addAttribute(PRODUCT_REFERENCES, productReferenceDataList);
        Assert.assertEquals(TEST_TYPE_VIEW, viewName);
    }

    @Test(expected = AbstractPageController.HttpNotFoundException.class)
    public void testRenderComponentNotFound() throws Exception {
        given(request.getAttribute(COMPONENT_UID)).willReturn(null);
        given(request.getParameter(COMPONENT_UID)).willReturn(null);
        productReferencesComponentController.handleGet(request, model);
    }

    @Test(expected = AbstractPageController.HttpNotFoundException.class)
    public void testRenderComponentNotFound2() throws Exception {
        given(request.getAttribute(COMPONENT_UID)).willReturn(null);
        given(request.getParameter(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
        given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID)).willReturn(null);
        productReferencesComponentController.handleGet(request, model);
    }

    @Test(expected = AbstractPageController.HttpNotFoundException.class)
    public void testRenderComponentNotFound3() throws Exception {
        given(request.getAttribute(COMPONENT_UID)).willReturn(TEST_COMPONENT_UID);
        given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID)).willReturn(null);
        given(cmsComponentService.getSimpleCMSComponent(TEST_COMPONENT_UID))
                .willThrow(new CMSItemNotFoundException(""));
        productReferencesComponentController.handleGet(request, model);
    }

    @Test
    public void testRenderComponentForYouMayAlsoLike() throws Exception {
        final ENEQueryResults results = mock(ENEQueryResults.class);

        given(productReferencesComponentModel.getMaximumNumberProducts()).willReturn(Integer.valueOf(1));
        given(productReferencesComponentModel.getTitle()).willReturn(TITLE_VALUE);

        given(productReferencesComponentModel.getItemtype()).willReturn(TEST_TYPE_CODE);
        given(request.getAttribute(ProductDataHelper.CURRENT_PRODUCT)).willReturn(TEST_PRODUCT_CODE);

        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isProductEndOfLifeCrossSellEnabled();
        given(request.getAttribute(ControllerConstants.REQUEST_PARAM_NOTAVAILABLE_ONLINE_AND_INSTORE))
                .willReturn(Boolean.TRUE);
        given(productService.getProductForCode(TEST_PRODUCT_CODE)).willReturn(colourVariantModel);

        populateOriginalCategory();

        given(baseProductModel.getOriginalCategory()).willReturn(originalCategoryModel);
        given(endecaDimensionCacheService.getDimension(EndecaConstants.EndecaRecordSpecificFields.ENDECA_CATEGORY,
                baseProductModel.getOriginalCategory().getCode())).willReturn("12345");

        given(endecaProductQueryBuilder.getYouMayAlsoLikeQueryResults(searchStateData, baseProductModel))
                .willReturn(results);

        willReturn(Integer.valueOf(12)).given(targetSharedConfigFacade).getInt("productEndOfLife.crossSellItemsPerPage",
                12);
        when(TargetProductDataHelper.getBaseProduct(colourVariantModel)).thenReturn(baseProductModel);

        final String viewName = productReferencesComponentController.handleComponent(request, model,
                productReferencesComponentModel);
        verify(model, Mockito.times(1)).addAttribute(TITLE, TITLE_VALUE);
        verify(model, Mockito.times(0)).addAttribute(PRODUCT_LISTER_DATA, productListerDataList);
        assertEquals(TEST_TYPE_VIEW, viewName);
    }

    private void populateOriginalCategory() {
        given(originalCategoryModel.getCode()).willReturn("W1234");
        given(originalCategoryModel.getName()).willReturn("tops");
    }


}

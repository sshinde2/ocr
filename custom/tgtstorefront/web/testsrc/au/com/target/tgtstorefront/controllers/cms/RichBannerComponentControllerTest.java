/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtwebcore.model.cms2.components.CoordinateLinkComponentModel;
import au.com.target.tgtwebcore.model.cms2.components.RichBannerComponentModel;


/**
 * @author asingh78
 * 
 */
public class RichBannerComponentControllerTest {

    private RichBannerComponentModel richBannerComponentModel;
    private final List<CMSLinkComponentModel> cmsLinkComponents = new ArrayList<>();
    @InjectMocks
    private final RichBannerComponentController richBannerComponentController = new RichBannerComponentController();

    @Mock
    private Model mockModel;

    @Mock
    private HttpServletRequest mockRequest;


    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        richBannerComponentModel = BDDMockito.mock(RichBannerComponentModel.class);
        final CMSLinkComponentModel cmsLinkComponentModel1 = BDDMockito.mock(CMSLinkComponentModel.class);
        final CMSLinkComponentModel cmsLinkComponentModel2 = BDDMockito.mock(CMSLinkComponentModel.class);
        final CoordinateLinkComponentModel coordinateLinkComponentModel = BDDMockito
                .mock(CoordinateLinkComponentModel.class);

        BDDMockito.given(cmsLinkComponentModel1.getVisible()).willReturn(Boolean.TRUE);
        BDDMockito.given(cmsLinkComponentModel2.getVisible()).willReturn(Boolean.TRUE);
        BDDMockito.given(coordinateLinkComponentModel.getVisible()).willReturn(Boolean.TRUE);

        cmsLinkComponents.add(cmsLinkComponentModel1);
        cmsLinkComponents.add(cmsLinkComponentModel2);
        cmsLinkComponents.add(coordinateLinkComponentModel);


    }


    @Test
    public void testfillModel() {
        final CMSLinkComponentModel mobileCmsLinkComponentModel = BDDMockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(richBannerComponentModel.getCmsLinkComponent()).willReturn(mobileCmsLinkComponentModel);
        BDDMockito.given(mobileCmsLinkComponentModel.getVisible()).willReturn(Boolean.TRUE);

        final Model model = new ExtendedModelMap();
        richBannerComponentController.fillModel(mockRequest, model, richBannerComponentModel);
        Assert.assertEquals(richBannerComponentModel, model.asMap().get((WebConstants.RICHBANNER)));
        Assert.assertEquals(mobileCmsLinkComponentModel, model.asMap().get(WebConstants.MOBILE_LINK));
        Assert.assertTrue(model.containsAttribute(WebConstants.COORDINATE_LINK_COMPONENTS));
        Assert.assertTrue(model.containsAttribute(WebConstants.MOBILE_LINK));

    }

    @Test
    public void populateLinkComponentsTest() {
        final List<CoordinateLinkComponentModel> coordinateLinkComponents = new ArrayList<>();
        final List<CMSLinkComponentModel> linkComponents = new ArrayList<>();
        BDDMockito.given(richBannerComponentModel.getCmsLinkComponents()).willReturn(cmsLinkComponents);
        richBannerComponentController.populateLinkComponents(richBannerComponentModel, coordinateLinkComponents,
                linkComponents, mockRequest);
        Assert.assertEquals(1, coordinateLinkComponents.size());
        Assert.assertEquals(2, linkComponents.size());
    }


}

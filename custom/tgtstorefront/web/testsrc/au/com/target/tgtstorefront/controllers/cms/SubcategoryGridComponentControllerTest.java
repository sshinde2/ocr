/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.servicelayer.services.CMSNavigationService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtstorefront.controllers.util.CMSNavigationComponentHelper;
import au.com.target.tgtstorefront.navigation.NavNodeMenuBuilder;
import au.com.target.tgtstorefront.navigation.subcategorygrid.SubcategoryGridItem;
import au.com.target.tgtwebcore.enums.NavigationNodeTypeEnum;
import au.com.target.tgtwebcore.model.cms2.components.SubcategoryGridComponentModel;


/**
 * @author rmcalave
 * 
 */
public class SubcategoryGridComponentControllerTest {
    //CHECKSTYLE:OFF needed to have visibility for mockito to inject it's mocks
    @InjectMocks
    SubcategoryGridComponentController subcategoryGridComponentController = new SubcategoryGridComponentController();
    //CHECKSTYLE_ON

    @Mock
    private CMSNavigationService mockCmsNavigationService;

    @Mock
    private NavNodeMenuBuilder mockNavNodeMenuBuilder;

    @Mock
    private CMSNavigationComponentHelper cmsNavigationComponentHelper;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testFillModelWithNoNavigationNode() {
        final String categoryCodeKey = "categoryCode";
        final String categoryCode = "Category Code";

        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        BDDMockito.given(mockRequest.getAttribute(categoryCodeKey)).willReturn(categoryCode);

        final Model model = new ExtendedModelMap();

        final SubcategoryGridComponentModel mockComponent = Mockito.mock(SubcategoryGridComponentModel.class);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(null);

        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        Mockito.doReturn(null).when(cmsNavigationComponentHelper).getCategoryNavigationNode(categoryCode);

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verify(mockRequest, Mockito.times(1)).getAttribute(categoryCodeKey);
        Mockito.verify(controller, Mockito.never()).createSubcategoryGridItems(
                Mockito.any(CMSNavigationNodeModel.class));

        Assert.assertEquals(0, model.asMap().size());
    }

    @Test
    public void testFillModelWithComponentNavigationNode() {
        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);

        final Model model = new ExtendedModelMap();

        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);

        final SubcategoryGridComponentModel mockComponent = Mockito.mock(SubcategoryGridComponentModel.class);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNodeModel);

        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verifyZeroInteractions(mockRequest);
        Mockito.verify(controller, Mockito.times(1)).createSubcategoryGridItems(mockNavigationNodeModel);

        Assert.assertEquals(0, model.asMap().size());
    }

    @Test
    public void testFillModelWithServiceNavigationNode() {
        final String categoryCodeKey = "categoryCode";
        final String categoryCode = "Category Code";

        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        BDDMockito.given(mockRequest.getAttribute(categoryCodeKey)).willReturn(categoryCode);

        final Model model = new ExtendedModelMap();

        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);

        final SubcategoryGridComponentModel mockComponent = Mockito.mock(SubcategoryGridComponentModel.class);

        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        Mockito.doReturn(mockNavigationNodeModel).when(cmsNavigationComponentHelper)
                .getCategoryNavigationNode(categoryCode);

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verify(mockRequest, Mockito.times(1)).getAttribute(categoryCodeKey);
        Mockito.verify(controller, Mockito.times(1)).createSubcategoryGridItems(mockNavigationNodeModel);

        Assert.assertEquals(0, model.asMap().size());
    }

    @Test
    public void testFillModelWithComponentNavigationNodeAndResults() {
        final List<SubcategoryGridItem> subcategoryGridItems = new ArrayList<>();
        subcategoryGridItems.add(new SubcategoryGridItem());

        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);

        final Model model = new ExtendedModelMap();

        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);

        final SubcategoryGridComponentModel mockComponent = Mockito.mock(SubcategoryGridComponentModel.class);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNodeModel);

        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        Mockito.doReturn(subcategoryGridItems).when(controller).createSubcategoryGridItems(mockNavigationNodeModel);

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verifyZeroInteractions(mockRequest);
        Mockito.verify(controller, Mockito.times(1)).createSubcategoryGridItems(mockNavigationNodeModel);

        Assert.assertEquals(1, model.asMap().size());
        Assert.assertTrue(model.containsAttribute("subcategoryGridItems"));

        final List<SubcategoryGridItem> modelList = (List<SubcategoryGridItem>)model.asMap()
                .get("subcategoryGridItems");
        Assert.assertTrue(CollectionUtils.isNotEmpty(modelList));
        Assert.assertEquals(subcategoryGridItems, modelList);
    }


    @Test
    public void testCreateSubcategoryGridItemsWithNoChildren() {
        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        final List<SubcategoryGridItem> result = subcategoryGridComponentController
                .createSubcategoryGridItems(mockNavigationNode);

        Assert.assertNull(result);
    }

    @Test
    public void testCreateSubcategoryGridItemsWithNoReturnedItem() {
        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        Mockito.doReturn(null).when(controller).createSubcategoryGridItems(mockChildNavigationNode);

        final List<SubcategoryGridItem> result = controller.createSubcategoryGridItems(mockNavigationNode);

        Assert.assertNotNull(result);
        Assert.assertTrue(CollectionUtils.isEmpty(result));
    }

    @Test
    public void testCreateSubcategoryGridItemsWithoutColumn() {
        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getType()).willReturn(null);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        final List<SubcategoryGridItem> result = controller.createSubcategoryGridItems(mockNavigationNode);
        Assert.assertNotNull(result);
        Mockito.verify(mockNavigationNode, Mockito.times(1)).getChildren();
        Mockito.verify(controller, Mockito.times(0)).addSubcategoryGridItemsForSections(
                Collections.singletonList(mockChildNavigationNode), result);



    }

    @Test
    public void testCreateSubcategoryGridItemsWithReturnedItemAndColumn() {
        final List<CMSNavigationNodeModel> childrenOfChildList = new ArrayList<>();

        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.COLUMN);
        BDDMockito.given(mockChildNavigationNode.getChildren()).willReturn(childrenOfChildList);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));
        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        controller.createSubcategoryGridItems(mockNavigationNode);
        Mockito.verify(controller, Mockito.times(1)).addSubcategoryGridItemsForSections(
                Mockito.eq(childrenOfChildList),
                Mockito.anyList());
    }

    @Test
    public void testAddSubcategoryGridItemsForSectionsNotClickable() {

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        final NavNodeMenuBuilder navNodeMenuBuilder = Mockito.mock(NavNodeMenuBuilder.class);
        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        BDDMockito.given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        BDDMockito.given(mockChildNavigationNode.getClickable()).willReturn(Boolean.FALSE);

        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));
        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        controller.createSubcategoryGridItems(mockNavigationNode);
        Mockito.verify(navNodeMenuBuilder, Mockito.never()).createMenuItem(mockNavigationNode);

    }

    @Ignore
    @Test
    public void testAddSubcategoryGridItemsForSectionsClickable() {
        final String navigationNodeTitle = "Nav Node Title";
        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        final List<SubcategoryGridItem> subcategoryList = new ArrayList<>();

        BDDMockito.given(mockChildNavigationNode.getType()).willReturn(NavigationNodeTypeEnum.SECTION);
        BDDMockito.given(mockChildNavigationNode.getTitle()).willReturn(navigationNodeTitle);
        BDDMockito.given(mockChildNavigationNode.getClickable()).willReturn(Boolean.TRUE);
        final SubcategoryGridComponentController controller = Mockito.spy(subcategoryGridComponentController);
        controller.addSubcategoryGridItemsForSections(
                Collections.singletonList(mockChildNavigationNode), subcategoryList);
        Assert.assertEquals(navigationNodeTitle, subcategoryList.get(0).getTitle());


    }

}

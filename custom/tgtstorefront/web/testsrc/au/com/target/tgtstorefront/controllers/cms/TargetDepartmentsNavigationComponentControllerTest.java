/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtstorefront.controllers.util.NavigationNodeHelper;
import au.com.target.tgtstorefront.navigation.megamenu.MegaMenuDepartment;
import au.com.target.tgtwebcore.model.cms2.components.TargetDepartmentsNavigationComponentModel;


public class TargetDepartmentsNavigationComponentControllerTest {

    private HttpServletRequest mockRequest;
    private Model model;
    private TargetDepartmentsNavigationComponentModel mockComponent;
    private TargetDepartmentsNavigationComponentController controller;

    @Mock
    private NavigationNodeHelper navigationNodeHelper;

    @InjectMocks
    private final TargetDepartmentsNavigationComponentController targetDepartmentsNavigationComponentController = new TargetDepartmentsNavigationComponentController();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mockRequest = Mockito.mock(HttpServletRequest.class);
        model = new ExtendedModelMap();
        mockComponent = Mockito.mock(TargetDepartmentsNavigationComponentModel.class);
        controller = Mockito.spy(targetDepartmentsNavigationComponentController);
    }

    @Test
    public void testFillModelWithNoNavigationNode() {
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(null);

        controller.fillModel(mockRequest, model, mockComponent);

        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);

        Mockito.verify(controller, Mockito.times(0)).getDepartments(mockNavigationNodeModel);
        Assert.assertEquals(0, model.asMap().size());
    }

    @Test
    public void testFillModelWithComponentNavigationNodeWithoutChildren() {
        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);

        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNodeModel);
        BDDMockito.given(mockComponent.getNavigationNode().getChildren()).willReturn(null);

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verify(controller, Mockito.times(0)).getDepartments(mockNavigationNodeModel);
        Assert.assertEquals(0, model.asMap().size());
    }

    @Test
    public void testFillModelWithComponentNavigationNodeWithEmptyChildren() {
        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);

        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNodeModel);
        BDDMockito.given(mockComponent.getNavigationNode().getChildren()).willReturn(new ArrayList());

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verify(controller, Mockito.times(0)).getDepartments(mockNavigationNodeModel);
        Assert.assertEquals(0, model.asMap().size());
    }

    @Test
    public void testFillModelWithComponentNavigationWithChildren() {
        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNodeModel);

        final CMSNavigationNodeModel mockNavigationNodeModelChild = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> navigationNodeModelChildren = new ArrayList<>();
        navigationNodeModelChildren.add(mockNavigationNodeModelChild);

        BDDMockito.given(mockComponent.getNavigationNode().getChildren()).willReturn(navigationNodeModelChildren);

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verify(controller, Mockito.times(1)).getDepartments(mockNavigationNodeModel);
        Assert.assertEquals(3, model.asMap().size());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSvgMenuInfoInModel() {
        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNodeModel);

        final CMSNavigationNodeModel mockNavigationNodeModelChild = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> navigationNodeModelChildren = new ArrayList<>();
        navigationNodeModelChildren.add(mockNavigationNodeModelChild);

        BDDMockito.given(mockComponent.getNavigationNode().getChildren()).willReturn(navigationNodeModelChildren);

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verify(controller, Mockito.times(1)).getDepartments(mockNavigationNodeModel);

        Assert.assertEquals(3, model.asMap().size());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testSvgUrlInfoInModel() {
        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNodeModel);

        final CMSNavigationNodeModel mockNavigationNodeModelChild = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> navigationNodeModelChildren = new ArrayList<>();
        navigationNodeModelChildren.add(mockNavigationNodeModelChild);

        BDDMockito.given(mockComponent.getNavigationNode().getChildren()).willReturn(navigationNodeModelChildren);
        BDDMockito.given(controller.getSVGSpriteUrl(mockNavigationNodeModel)).willReturn("svgUrl");

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verify(controller, Mockito.times(1)).getDepartments(mockNavigationNodeModel);
        Mockito.verify(controller, Mockito.atLeastOnce()).getSVGSpriteUrl(mockNavigationNodeModel);

        Assert.assertEquals(3, model.asMap().size());
        Assert.assertNotNull(model.asMap().get("departmentsSVGUrl"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testNonEmptyDepartmentsInfoInModel() {
        final CMSNavigationNodeModel mockNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockComponent.getNavigationNode()).willReturn(mockNavigationNodeModel);

        final CMSNavigationNodeModel mockNavigationNodeModelChild = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationNodeModel> navigationNodeModelChildren = new ArrayList<>();
        navigationNodeModelChildren.add(mockNavigationNodeModelChild);

        BDDMockito.given(mockComponent.getNavigationNode().getChildren()).willReturn(navigationNodeModelChildren);

        final MegaMenuDepartment department = new MegaMenuDepartment();
        final List<MegaMenuDepartment> departments = new ArrayList<>();
        departments.add(department);

        BDDMockito.given(controller.getDepartments(mockNavigationNodeModel)).willReturn(departments);

        controller.fillModel(mockRequest, model, mockComponent);

        Mockito.verify(controller, Mockito.atLeastOnce()).getDepartments(mockNavigationNodeModel);

        Assert.assertEquals(3, model.asMap().size());
        Assert.assertEquals(1, ((List)model.asMap().get("departments")).size());
    }
}
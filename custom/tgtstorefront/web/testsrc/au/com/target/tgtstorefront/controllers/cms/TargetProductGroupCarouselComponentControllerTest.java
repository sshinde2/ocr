/**
 * 
 */
package au.com.target.tgtstorefront.controllers.cms;

import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.featureswitch.service.impl.TargetFeatureSwitchServiceImpl;
import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtwebcore.model.cms2.components.TargetProductGroupCarouselComponentModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductCollectionPageModel;
import au.com.target.tgtwebcore.model.cms2.pages.TargetProductGroupPageModel;


/**
 * @author niannel1
 *
 */
public class TargetProductGroupCarouselComponentControllerTest {

    private Model model;

    @Mock
    private HttpServletRequest request;

    @Mock
    private final TargetProductGroupCarouselComponentModel component = Mockito
            .mock(TargetProductGroupCarouselComponentModel.class);

    @Mock
    private final TargetFeatureSwitchService targetFeatureSwitchService = Mockito
            .mock(TargetFeatureSwitchServiceImpl.class);

    @Mock
    private TargetShopTheLookFacade targetShopTheLookFacade;

    @InjectMocks
    private final TargetProductGroupCarouselComponentController controller = new TargetProductGroupCarouselComponentController();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        model = new ExtendedModelMap();
        controller.setPreviousLookCount(2);
    }

    @Test
    public void testFillModelWithNoNavigationNode() {
        controller.fillModel(request, model, component);
    }

    @Test
    public void testFillModelWithNoNavigationNodeChildren() {
        final CMSNavigationNodeModel navigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        Mockito.when(component.getNavigationNode()).thenReturn(navigationNodeModel);
        controller.fillModel(request, model, component);
    }

    @Test
    public void testWillNotFailIfNoLookUrlProvided() {
        final String[] pages = { "page1", "page2" };
        final CMSNavigationNodeModel mockNavigationModel = createNavNodeWithVisibleGroupPages(pages);
        Mockito.when(component.getNavigationNode()).thenReturn(mockNavigationModel);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn(null);
        controller.fillModel(request, model, component);
    }

    @Test
    public void testCollectionIsSameWhenNotRelevantLookUrl() {
        final String[] pages = { "page1", "page2" };
        final CMSNavigationNodeModel mockNavigationModel = createNavNodeWithVisibleGroupPages(pages);
        Mockito.when(component.getNavigationNode()).thenReturn(mockNavigationModel);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn("testUrl");
        controller.fillModel(request, model, component);
        final Object result = model.asMap().get("groupPageModels");
        final List<TargetProductGroupPageModel> groupPageModels = (List<TargetProductGroupPageModel>)result;
        for (int i = 0; i < groupPageModels.size(); i++) {
            Assert.assertEquals(pages[i], groupPageModels.get(i).getLabel());
        }
    }

    @Test
    public void testCollectionRotatesWhenOnOwnPage() {
        final String[] pages = { "page1", "page2", "page3", "page4", "page5", "page6" };
        final String[] expected = { "page3", "page4", "page6", "page1", "page2" };
        final CMSNavigationNodeModel mockNavigationModel = createNavNodeWithVisibleGroupPages(pages);
        Mockito.when(component.getNavigationNode()).thenReturn(mockNavigationModel);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn("page5");
        controller.fillModel(request, model, component);
        final Object result = model.asMap().get("groupPageModels");
        final List<TargetProductGroupPageModel> groupPageModels = (List<TargetProductGroupPageModel>)result;
        for (int i = 0; i < groupPageModels.size(); i++) {
            Assert.assertEquals(expected[i], groupPageModels.get(i).getLabel());
        }
    }

    @Test
    public void testCollectionDoesNotRotateWhenFirstLook() {
        final String[] pages = { "page1", "page2", "page3", "page4", "page5", "page6" };
        final String[] expected = { "page2", "page3", "page4", "page5", "page6" };
        final CMSNavigationNodeModel mockNavigationModel = createNavNodeWithVisibleGroupPages(pages);
        Mockito.when(component.getNavigationNode()).thenReturn(mockNavigationModel);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn("page1");
        controller.fillModel(request, model, component);
        final Object result = model.asMap().get("groupPageModels");
        final List<TargetProductGroupPageModel> groupPageModels = (List<TargetProductGroupPageModel>)result;
        for (int i = 0; i < groupPageModels.size(); i++) {
            Assert.assertEquals(expected[i], groupPageModels.get(i).getLabel());
        }
    }

    @Test
    public void testCollectionDoesNotRotateWhenSecondLook() {
        final String[] pages = { "page1", "page2", "page3", "page4", "page5", "page6" };
        final String[] expected = { "page1", "page3", "page4", "page5", "page6" };
        final CMSNavigationNodeModel mockNavigationModel = createNavNodeWithVisibleGroupPages(pages);
        Mockito.when(component.getNavigationNode()).thenReturn(mockNavigationModel);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn("page2");
        controller.fillModel(request, model, component);
        final Object result = model.asMap().get("groupPageModels");
        final List<TargetProductGroupPageModel> groupPageModels = (List<TargetProductGroupPageModel>)result;
        for (int i = 0; i < groupPageModels.size(); i++) {
            Assert.assertEquals(expected[i], groupPageModels.get(i).getLabel());
        }
    }

    @Test
    public void testCollectionDoesNotRotateWhenThirdLook() {
        final String[] pages = { "page1", "page2", "page3", "page4", "page5", "page6" };
        final String[] expected = { "page1", "page2", "page4", "page5", "page6" };
        final CMSNavigationNodeModel mockNavigationModel = createNavNodeWithVisibleGroupPages(pages);
        Mockito.when(component.getNavigationNode()).thenReturn(mockNavigationModel);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn("page3");
        controller.fillModel(request, model, component);
        final Object result = model.asMap().get("groupPageModels");
        final List<TargetProductGroupPageModel> groupPageModels = (List<TargetProductGroupPageModel>)result;
        for (int i = 0; i < groupPageModels.size(); i++) {
            Assert.assertEquals(expected[i], groupPageModels.get(i).getLabel());
        }
    }

    @Test
    public void testCollectionDoesNotAddSelfWhenOnOwnPage() {
        final String[] pages = { "page1", "page2", "page3" };
        final String[] expected = { "page1", "page2" };
        final CMSNavigationNodeModel mockNavigationModel = createNavNodeWithVisibleGroupPages(pages);
        Mockito.when(component.getNavigationNode()).thenReturn(mockNavigationModel);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn("page3");
        controller.fillModel(request, model, component);
        final Object result = model.asMap().get("groupPageModels");
        final List<TargetProductGroupPageModel> groupPageModels = (List<TargetProductGroupPageModel>)result;
        for (int i = 0; i < groupPageModels.size(); i++) {
            Assert.assertEquals(expected[i], groupPageModels.get(i).getLabel());
        }
    }

    @Test
    public void testCollectionReturnsEmptyIfNoVisiblePages() {
        final String[] pages = { "page1", "page2", "page3" };
        final CMSNavigationNodeModel mockNavigationModel = createNavNodeWithNoVisibleGroupPages(pages);
        Mockito.when(component.getNavigationNode()).thenReturn(mockNavigationModel);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn("testUrl");
        controller.fillModel(request, model, component);
        final Object result = model.asMap().get("groupPageModels");
        final List<TargetProductGroupPageModel> groupPageModels = (List<TargetProductGroupPageModel>)result;
        Assert.assertEquals(groupPageModels.size(), 0);
    }

    @Test
    public void testStepStlWithEmptyCollection() {
        Mockito.when(Boolean.valueOf(
                targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(component.getLookCollection()).thenReturn(null);
        controller.fillModel(request, model, component);
        Assert.assertTrue(CollectionUtils.isEmpty(model.asMap().keySet()));
    }

    @Test
    public void testStepStlWithEmptyLooks() {
        Mockito.when(Boolean.valueOf(
                targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)))
                .thenReturn(Boolean.TRUE);
        Mockito.when(component.getLookCollection()).thenReturn(new TargetLookCollectionModel());
        controller.fillModel(request, model, component);
        Assert.assertTrue(CollectionUtils.isEmpty(model.asMap().keySet()));
    }

    @Test
    public void testStepStl() {
        Mockito.when(Boolean.valueOf(
                targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)))
                .thenReturn(Boolean.TRUE);
        final String code = "LC01";
        final TargetLookCollectionModel collection = Mockito.mock(TargetLookCollectionModel.class);
        Mockito.when(collection.getId()).thenReturn(code);
        final List<TargetLookModel> looks = new ArrayList<>();
        final TargetLookModel look1 = Mockito.mock(TargetLookModel.class);
        Mockito.when(look1.getId()).thenReturn("L1");
        looks.add(look1);

        final TargetLookModel look2 = Mockito.mock(TargetLookModel.class);
        Mockito.when(look2.getId()).thenReturn("L2");
        Mockito.when(look2.getUrl()).thenReturn("testUrl");
        looks.add(look2);

        final TargetLookModel look3 = Mockito.mock(TargetLookModel.class);
        Mockito.when(look3.getId()).thenReturn("L3");
        looks.add(look3);
        final List<LookDetailsData> lookDetailsDatas = new ArrayList<>();
        final LookDetailsData lookDetailsData1 = Mockito.mock(LookDetailsData.class);
        lookDetailsDatas.add(lookDetailsData1);
        final LookDetailsData lookDetailsData2 = Mockito.mock(LookDetailsData.class);
        lookDetailsDatas.add(lookDetailsData2);
        final LookDetailsData lookDetailsData3 = Mockito.mock(LookDetailsData.class);
        lookDetailsDatas.add(lookDetailsData3);

        Mockito.when(component.getLookCollection()).thenReturn(collection);
        Mockito.when(targetShopTheLookFacade.getVisibleLooksForCollection(code)).thenReturn(looks);
        Mockito.when(targetShopTheLookFacade.populateLookDetailsDataList(looks)).thenReturn(lookDetailsDatas);
        controller.fillModel(request, model, component);
        Assert.assertNotNull(model.asMap().get("lookCollection"));
        Assert.assertNotNull(model.asMap().get("looks"));
        Assert.assertEquals(3, ((Collection<?>)model.asMap().get("looks")).size());
        Assert.assertNull(model.asMap().get("groupPageModels"));
        Assert.assertNull(model.asMap().get("collectionPageModel"));
    }

    @Test
    public void testStepStlSkipLook() throws ParseException {
        Mockito.when(Boolean.valueOf(
                targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.STEP_SHOP_THE_LOOK)))
                .thenReturn(Boolean.TRUE);
        final String code = "LC01";
        final TargetLookCollectionModel collection = Mockito.mock(TargetLookCollectionModel.class);
        Mockito.when(collection.getId()).thenReturn(code);
        final List<TargetLookModel> looks = new ArrayList<>();

        final TargetLookModel look1 = Mockito.mock(TargetLookModel.class);
        Mockito.when(look1.getId()).thenReturn("L1");
        looks.add(look1);

        final TargetLookModel look2 = Mockito.mock(TargetLookModel.class);
        Mockito.when(look2.getId()).thenReturn("L2");
        Mockito.when(look2.getUrl()).thenReturn("testUrl");
        looks.add(look2);

        final TargetLookModel look3 = Mockito.mock(TargetLookModel.class);
        Mockito.when(look3.getId()).thenReturn("L3");
        looks.add(look3);

        final List<LookDetailsData> lookDetailsDatas = new ArrayList<>();
        final LookDetailsData lookDetailsData1 = Mockito.mock(LookDetailsData.class);
        lookDetailsDatas.add(lookDetailsData1);
        final LookDetailsData lookDetailsData2 = Mockito.mock(LookDetailsData.class);
        lookDetailsDatas.add(lookDetailsData2);
        final LookDetailsData lookDetailsData3 = Mockito.mock(LookDetailsData.class);
        lookDetailsDatas.add(lookDetailsData3);

        Mockito.when(targetShopTheLookFacade.populateLookDetailsDataList(Mockito.anyList())).thenReturn(
                lookDetailsDatas);
        Mockito.when(targetShopTheLookFacade.getVisibleLooksForCollection(code)).thenReturn(looks);
        Mockito.when(component.getLookCollection()).thenReturn(collection);
        Mockito.when(request.getAttribute("lookUrl")).thenReturn("testUrl");

        controller.fillModel(request, model, component);
        Assert.assertNull(model.asMap().get("groupPageModels"));
        Assert.assertNull(model.asMap().get("collectionPageModel"));
        Assert.assertNotNull(model.asMap().get("lookCollection"));
        Assert.assertNotNull(model.asMap().get("looks"));
        Assert.assertEquals(3, ((List<?>)model.asMap().get("looks")).size());

        final ArgumentCaptor<List<TargetLookModel>> argument = ArgumentCaptor.forClass((Class)List.class);
        Mockito.verify(targetShopTheLookFacade).populateLookDetailsDataList(argument.capture());
        final Matcher<String> matcher = Matchers.anyOf(Matchers.equalTo("L1"), Matchers.equalTo("L3"));
        for (final TargetLookModel targetLook : argument.getValue()) {
            Assert.assertNotSame("testUrl", targetLook.getUrl());
            Assert.assertThat(targetLook.getId(), matcher);
        }

    }

    @SuppressWarnings("boxing")
    private CMSNavigationNodeModel createNavigationNodeWithLook(final String label, final Boolean isVisible) {

        final CMSNavigationNodeModel child = Mockito.mock(CMSNavigationNodeModel.class);

        final TargetProductGroupPageModel groupPageModel = Mockito.mock(TargetProductGroupPageModel.class);
        Mockito.when(groupPageModel.getLabel()).thenReturn(label);
        final CMSNavigationEntryModel entryModel = new CMSNavigationEntryModel();
        entryModel.setItem(groupPageModel);
        Mockito.when(child.isVisible()).thenReturn(isVisible.booleanValue());
        Mockito.when(child.getEntries()).thenReturn(Collections.singletonList(entryModel));
        return child;
    }

    private CMSNavigationNodeModel createNavNodeWithNoVisibleGroupPages(final String[] labels) {

        final List<CMSNavigationNodeModel> children = new ArrayList<>();

        for (final String label : labels) {
            final CMSNavigationNodeModel child = createNavigationNodeWithLook(label, Boolean.FALSE);
            children.add(child);
        }
        final CMSNavigationNodeModel parent = new CMSNavigationNodeModel();
        parent.setVisible(true);
        parent.setChildren(children);
        final TargetProductCollectionPageModel collection = Mockito.mock(TargetProductCollectionPageModel.class);
        final CMSNavigationEntryModel parentEntryModel = new CMSNavigationEntryModel();
        parentEntryModel.setItem(collection);
        parent.setEntries(Collections.singletonList(parentEntryModel));
        return parent;
    }

    private CMSNavigationNodeModel createNavNodeWithVisibleGroupPages(final String[] labels) {

        final List<CMSNavigationNodeModel> children = new ArrayList<>();

        for (final String label : labels) {
            final CMSNavigationNodeModel child = createNavigationNodeWithLook(label, Boolean.TRUE);
            children.add(child);
        }
        final CMSNavigationNodeModel parent = new CMSNavigationNodeModel();
        parent.setVisible(true);
        parent.setChildren(children);
        final TargetProductCollectionPageModel collection = Mockito.mock(TargetProductCollectionPageModel.class);
        final CMSNavigationEntryModel parentEntryModel = new CMSNavigationEntryModel();
        parentEntryModel.setItem(collection);
        parent.setEntries(Collections.singletonList(parentEntryModel));
        return parent;
    }
}

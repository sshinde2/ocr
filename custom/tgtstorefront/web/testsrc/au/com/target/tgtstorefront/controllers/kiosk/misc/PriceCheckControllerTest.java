/**
 * 
 */
package au.com.target.tgtstorefront.controllers.kiosk.misc;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetPOSProductData;


/**
 * Test for PriceCheckController
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PriceCheckControllerTest {

    @Mock
    private Model model;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private TargetProductFacade productFacade;

    @Mock
    private ProductService productService;

    @Mock
    private ProductModel productModel;

    @Mock
    private TargetPOSProductData posProduct;

    @InjectMocks
    private final PriceCheckController controller = Mockito.spy(new PriceCheckController());

    @Test
    public void testControllerEmptyBarcode() {

        controller.priceCheckBarcode("", "7001", model, request, response);
        Mockito.verifyZeroInteractions(model);
    }

    @Test
    public void testControllerEmptyStore() {

        controller.priceCheckBarcode("1234", "", model, request, response);
        Mockito.verifyZeroInteractions(model);
    }

    @Test
    public void testControllerValidParametersNullPosProduct() {

        Mockito.when(productFacade.getPOSProductDetails(Mockito.anyString(), Mockito.anyString())).thenReturn(null);

        controller.priceCheckBarcode("1234", "7001", model, request, response);
        Mockito.verify(model).addAttribute("posProduct", null);
        Mockito.verify(model, Mockito.times(1)).addAttribute(Mockito.anyString(), Mockito.anyObject());
    }

    @Ignore
    @Test
    public void testControllerValidParametersNullPosProductCode() {

        Mockito.when(productFacade.getPOSProductDetails(Mockito.anyString(), Mockito.anyString())).thenReturn(
                posProduct);

        controller.priceCheckBarcode("1234", "7001", model, request, response);
        Mockito.verify(model).addAttribute("posProduct", posProduct);
        Mockito.verify(model, Mockito.times(1)).addAttribute(Mockito.anyString(), Mockito.anyObject());
    }

    @Ignore
    @Test
    public void testControllerValidParametersValidProductCode() {

        Mockito.when(productFacade.getPOSProductDetails(Mockito.anyString(), Mockito.anyString())).thenReturn(
                posProduct);
        Mockito.when(posProduct.getItemCode()).thenReturn("P1000");
        Mockito.when(productService.getProductForCode("P1000")).thenReturn(productModel);

        Mockito.doNothing().when(controller).populateHybrisProductDetail(productModel, model, request);

        controller.priceCheckBarcode("1234", "7001", model, request, response);

        Mockito.verify(model).addAttribute("posProduct", posProduct);
        Mockito.verify(controller).populateHybrisProductDetail(productModel, model, request);
    }



}

package au.com.target.tgtstorefront.controllers.misc;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.enums.CmsApprovalStatus;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.endeca.navigation.ENEQueryException;
import com.google.common.collect.ImmutableList;

import au.com.target.endeca.infront.exception.TargetEndecaException;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtfacades.category.TargetCategoryFacade;
import au.com.target.tgtfacades.category.data.TargetCategoryData;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.data.TargetProductCountDepartmentData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.sitemap.TargetSiteMapFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.controllers.AbstractController.HttpNotFoundException;
import au.com.target.tgtstorefront.data.sitemap.SiteMapData;
import au.com.target.tgtstorefront.data.sitemap.SiteMapIndexData;
import au.com.target.tgtstorefront.data.sitemap.SiteMapUrlData;
import au.com.target.tgtstorefront.data.sitemap.SiteMapUrlData.ChangeFrequency;


/**
 * Test suite for {@link SiteMapController}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SiteMapControllerTest {

    private static final String BASE_URL = "http://www.example.com:8080";
    private static final String CONTENT_SITEMAP_CHANGE_FREQUENCY = "YEARLY";
    private static final BigDecimal CONTENT_SITEMAP_PRIORITY = BigDecimal.valueOf(0.5d);
    private static final String CATEGORY_SITEMAP_CHANGE_FREQUENCY = "WEEKLY";
    private static final BigDecimal CATEGORY_SITEMAP_PRIORITY = BigDecimal.valueOf(0.7d);
    private static final String STORE_SITEMAP_CHANGE_FREQUENCY = "MONTHLY";
    private static final BigDecimal STORE_SITEMAP_PRIORITY = BigDecimal.valueOf(0.6d);
    private static final String CAT1_URL = "/boys/toys/c/WS123";
    private static final String PRODUCT_SITEMAP_CHANGE_FREQUENCY = "MONTHLY";
    private static final BigDecimal PRODUCT_SITEMAP_PRIORITY = BigDecimal.valueOf(0.6d);

    @Mock
    private TargetCategoryFacade categoryFacade;
    @Mock
    private TargetStoreLocatorFacade storeLocator;
    @Mock
    private TargetProductFacade productFacade;
    @Mock
    private CMSPageService cmsPageService;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ConfigurationService configurationService;
    @Mock
    private List<String> allowedContentPages;
    @Mock
    private TargetCategoryData category;
    @Mock
    private TargetProductData product;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ContentPageModel contentPage;
    @Mock
    private SiteMapUrlData siteMap;
    @Mock
    private TargetSiteMapFacade targetSiteMapFacade;
    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;
    @InjectMocks
    private final SiteMapController siteMapController = new SiteMapController();


    /**
     * Initializes test cases before run.
     */
    @SuppressWarnings("boxing")
    @Before
    public void setUp() {
        given(configurationService.getConfiguration().getString("tgtstorefront.host.fqdn")).willReturn(BASE_URL);
        given(category.getUrl()).willReturn(CAT1_URL);
        given(category.getLastModified()).willReturn(DateTime.now());
        given(new Integer(configurationService.getConfiguration().getInt("tgtstorefront.sitemap.productPage.pageSize")))
                .willReturn(
                        new Integer(10));
    }

    /**
     * Verifies that {@code sitemap-index.xml} file lists first two elements as references to content & category files,
     * and then enumerates all leaf categories.
     */
    @SuppressWarnings("boxing")
    @Test
    public void verifySiteMapIndexElements() {
        given(configurationService.getConfiguration().getInt("tgtstorefront.sitemap.productPage.pageSize"))
                .willReturn(200);

        final TargetProductCountDepartmentData productDepartemntData = new TargetProductCountDepartmentData();
        productDepartemntData.setTargetMerchDepartmentCode("100");
        productDepartemntData.setPageCount(3);
        final List<TargetProductCountDepartmentData> productDepartmentDataList = new ArrayList<>();
        productDepartmentDataList.add(productDepartemntData);
        given(productFacade.getProductPageCountForDepartment(200)).willReturn(productDepartmentDataList);
        final SiteMapIndexData result = siteMapController.getSiteMapIndex();

        assertThat(result.getElements().get(0).getLocation()).isEqualTo(buildAbsoluteUrl("/content-sitemap.xml"));
        assertThat(result.getElements().get(1).getLocation()).isEqualTo(buildAbsoluteUrl("/category-sitemap.xml"));
        assertThat(result.getElements().get(2).getLocation()).isEqualTo(buildAbsoluteUrl("/stores-sitemap.xml"));
        assertThat(result.getElements().get(2).getLastModified()).isNull();
        assertThat(result.getElements().get(3).getLocation()).isEqualTo(buildAbsoluteUrl("/psm/100/0/sitemap.xml"));
        assertThat(result.getElements().get(4).getLocation()).isEqualTo(buildAbsoluteUrl("/psm/100/1/sitemap.xml"));
        assertThat(result.getElements().get(5).getLocation()).isEqualTo(buildAbsoluteUrl("/psm/100/2/sitemap.xml"));
        assertThat(result.getElements()).hasSize(6);
    }

    /**
     * Verifies that {@code category-sitemap.xml} file lists all categories in the system.
     */

    @Test
    public void verifyCategorySiteMapElements() {

        willReturn(Boolean.FALSE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CATEGORY_SITEMAP_OPTIMIZATION);
        given(categoryFacade.getAllCategoryData()).willReturn(ImmutableList.of(category));
        given(siteMap.getChangeFrequency()).willReturn(ChangeFrequency.WEEKLY);
        given(siteMap.getPriority()).willReturn(CATEGORY_SITEMAP_PRIORITY);
        given(configurationService.getConfiguration().getString("tgtstorefront.sitemap.categoryPage.changeFreq"))
                .willReturn(CATEGORY_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal("tgtstorefront.sitemap.categoryPage.priority"))
                .willReturn(CATEGORY_SITEMAP_PRIORITY);
        final SiteMapData result = siteMapController.getCategorySiteMap();
        final SiteMapUrlData firstCategory = result.getUrls().get(0);
        assertThat(firstCategory.getLocation()).isEqualTo(buildAbsoluteUrl(category.getUrl()));
        assertThat(siteMap.getChangeFrequency()).isEqualTo(SiteMapUrlData.ChangeFrequency.WEEKLY);
        assertThat(firstCategory.getLastModified()).isNull();
        assertThat(firstCategory.getPriority()).isEqualTo(BigDecimal.valueOf(0.7d));
    }

    @SuppressWarnings("boxing")
    @Test
    public void verifyCategorySiteMapElementsWithDepartmentAndCategoryAndSubcategory() {
        final String topLevelChangeFrequency = "WEEKLY";
        final BigDecimal topLevelPriority = BigDecimal.valueOf(0.8d);

        final String allProductsCategoryCode = "AP01";

        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CATEGORY_SITEMAP_OPTIMIZATION);
        given(categoryFacade.isRootCategory(allProductsCategoryCode)).willReturn(Boolean.TRUE);

        final TargetCategoryData allProducts = new TargetCategoryData();
        allProducts.setCode(allProductsCategoryCode);

        final TargetCategoryData department = new TargetCategoryData();
        department.setCode("topCategory");
        final List<CategoryData> departmentSupercategories = new ArrayList<>();
        departmentSupercategories.add(allProducts);

        final TargetCategoryData testCategory = new TargetCategoryData();

        final List<CategoryData> categorySupercategories = new ArrayList<>();
        categorySupercategories.add(department);

        final TargetCategoryData subcategory = new TargetCategoryData();

        final List<CategoryData> subcategorySupercategories = new ArrayList<>();
        subcategorySupercategories.add(testCategory);

        final List<TargetCategoryData> allCategories = new ArrayList<>();
        allCategories.add(allProducts);
        allCategories.add(department);
        allCategories.add(testCategory);
        allCategories.add(subcategory);

        given(categoryFacade.isTopCategory(department.getCode())).willReturn(Boolean.TRUE);
        given(categoryFacade.getCategoriesWithProducts()).willReturn(allCategories);
        given(siteMap.getChangeFrequency()).willReturn(ChangeFrequency.WEEKLY);
        given(siteMap.getPriority()).willReturn(CATEGORY_SITEMAP_PRIORITY);
        given(configurationService.getConfiguration().getString("tgtstorefront.sitemap.categoryPage.changeFreq"))
                .willReturn(CATEGORY_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal("tgtstorefront.sitemap.categoryPage.priority"))
                .willReturn(CATEGORY_SITEMAP_PRIORITY);
        given(
                configurationService.getConfiguration().getString(
                        "tgtstorefront.sitemap.categoryPage.toplevel.changeFreq")).willReturn(topLevelChangeFrequency);
        given(
                configurationService.getConfiguration().getBigDecimal(
                        "tgtstorefront.sitemap.categoryPage.toplevel.priority")).willReturn(topLevelPriority);
        final SiteMapData result = siteMapController.getCategorySiteMap();
        assertThat(result.getUrls()).hasSize(3);

        final SiteMapUrlData firstCategory = result.getUrls().get(0);
        assertThat(firstCategory.getChangeFrequency()).isEqualTo(SiteMapUrlData.ChangeFrequency.WEEKLY);
        assertThat(firstCategory.getPriority()).isEqualTo(BigDecimal.valueOf(0.8d));

        final SiteMapUrlData secondCategory = result.getUrls().get(1);
        assertThat(secondCategory.getChangeFrequency()).isEqualTo(SiteMapUrlData.ChangeFrequency.WEEKLY);
        assertThat(secondCategory.getPriority()).isEqualTo(BigDecimal.valueOf(0.7d));

        final SiteMapUrlData thirdCategory = result.getUrls().get(2);
        assertThat(thirdCategory.getChangeFrequency()).isEqualTo(SiteMapUrlData.ChangeFrequency.WEEKLY);
        assertThat(thirdCategory.getPriority()).isEqualTo(BigDecimal.valueOf(0.7d));
    }

    /**
     * Verify content site map elements.
     */
    @SuppressWarnings("boxing")
    @Test
    public void verifyContentSiteMapElements() {
        given(contentPage.getLabel()).willReturn("homepage");
        given(allowedContentPages.contains("homepage")).willReturn(true);
        given(cmsPageService.getAllContentPages()).willReturn(ImmutableList.of(contentPage));
        given(siteMap.getChangeFrequency()).willReturn(ChangeFrequency.YEARLY);
        given(siteMap.getPriority()).willReturn(CONTENT_SITEMAP_PRIORITY);

        given(configurationService.getConfiguration().getString("tgtstorefront.sitemap.contentPage.changeFreq"))
                .willReturn(CONTENT_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal("tgtstorefront.sitemap.contentPage.priority"))
                .willReturn(CONTENT_SITEMAP_PRIORITY);

        final SiteMapData result = siteMapController.getContentSiteMap();

        final SiteMapUrlData firstCategory = result.getUrls().get(0);
        assertThat(firstCategory.getChangeFrequency()).isEqualTo(SiteMapUrlData.ChangeFrequency.YEARLY);
        assertThat(firstCategory.getLastModified()).isNull();
        assertThat(firstCategory.getPriority()).isEqualTo(BigDecimal.valueOf(0.5d));
    }

    /**
     * Verifies that modal content pages are excluded from site map.
     */
    @Test
    public void verifyContentSiteMapExcludesModalPages() {
        given(contentPage.getApprovalStatus()).willReturn(CmsApprovalStatus.APPROVED);
        given(contentPage.getMasterTemplate().getUid()).willReturn("ModalPageTemplate");
        given(cmsPageService.getAllContentPages()).willReturn(ImmutableList.of(contentPage));

        final SiteMapData result = siteMapController.getContentSiteMap();
        assertThat(result.getUrls()).isEmpty();
    }

    /**
     * Verifies that content pages having label that does not start with forward slash will be excluded.
     */
    @Test
    public void verifyContentPageLabelPatternHonoredWithNoForwardSlash() {
        given(contentPage.getApprovalStatus()).willReturn(CmsApprovalStatus.APPROVED);
        given(contentPage.getLabel()).willReturn("something");
        given(cmsPageService.getAllContentPages()).willReturn(ImmutableList.of(contentPage));

        final SiteMapData result = siteMapController.getContentSiteMap();
        assertThat(result.getUrls()).isEmpty();
    }

    /**
     * Verifies that content pages having label that does not start with forward slash will be excluded.
     */
    @Test
    public void verifyContentPageLabelPatternHonoredWithForwardSlashAndTilda() {
        given(contentPage.getApprovalStatus()).willReturn(CmsApprovalStatus.APPROVED);
        given(contentPage.getLabel()).willReturn("/~something");
        given(cmsPageService.getAllContentPages()).willReturn(ImmutableList.of(contentPage));

        final SiteMapData result = siteMapController.getContentSiteMap();

        assertThat(result.getUrls()).isEmpty();
    }

    @Test
    public void verifyContentPageLabelPatternHonoredWithForwardSlash() {
        given(contentPage.getApprovalStatus()).willReturn(CmsApprovalStatus.APPROVED);
        given(contentPage.getLabel()).willReturn("/something");
        given(cmsPageService.getAllContentPages()).willReturn(ImmutableList.of(contentPage));
        given(configurationService.getConfiguration().getString("tgtstorefront.sitemap.contentPage.changeFreq"))
                .willReturn(CONTENT_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal("tgtstorefront.sitemap.contentPage.priority"))
                .willReturn(CONTENT_SITEMAP_PRIORITY);
        final SiteMapData result = siteMapController.getContentSiteMap();

        assertThat(result.getUrls()).hasSize(1);
    }

    @Test
    public void verifyContentPageNotApprovedNotIncluded() {
        given(contentPage.getApprovalStatus()).willReturn(CmsApprovalStatus.UNAPPROVED);
        given(contentPage.getLabel()).willReturn("/something");
        given(cmsPageService.getAllContentPages()).willReturn(ImmutableList.of(contentPage));
        final SiteMapData result = siteMapController.getContentSiteMap();
        assertThat(result.getUrls()).isEmpty();
    }

    /**
     * Verifies that configured list of content pages will always be included.
     */
    @SuppressWarnings("boxing")
    @Test
    public void verifyAllowedContentPagesAlwaysIncluded() {
        given(contentPage.getLabel()).willReturn("homepage");
        given(allowedContentPages.contains("homepage")).willReturn(true);
        given(cmsPageService.getAllContentPages()).willReturn(ImmutableList.of(contentPage));

        final SiteMapData result = siteMapController.getContentSiteMap();

        assertThat(result.getUrls()).hasSize(1);
    }

    @Test
    public void verifyStoreSiteMapElements() {
        final Map<String, List<TargetPointOfServiceData>> storeLoc = new HashMap<String, List<TargetPointOfServiceData>>();
        final TargetPointOfServiceData data = new TargetPointOfServiceData();
        data.setUrl("/stores/nsw/paramatta/2145");
        storeLoc.put("State", ImmutableList.of(data));
        given(storeLocator.getAllStateAndStores()).willReturn(storeLoc);
        given(siteMap.getChangeFrequency()).willReturn(ChangeFrequency.MONTHLY);
        given(siteMap.getPriority()).willReturn(STORE_SITEMAP_PRIORITY);
        given(configurationService.getConfiguration().getString("tgtstorefront.sitemap.storePage.changeFreq"))
                .willReturn(STORE_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal("tgtstorefront.sitemap.storePage.priority"))
                .willReturn(STORE_SITEMAP_PRIORITY);
        final SiteMapData result = siteMapController.getStoreSiteMap();
        final SiteMapUrlData storeurl = result.getUrls().get(0);
        assertThat(storeurl.getLocation()).isEqualTo(buildAbsoluteUrl(data.getUrl()));
        assertThat(storeurl.getChangeFrequency()).isEqualTo(SiteMapUrlData.ChangeFrequency.MONTHLY);
        assertThat(storeurl.getLastModified()).isNull();
        assertThat(storeurl.getPriority()).isEqualTo(BigDecimal.valueOf(0.6d));
    }



    @Test
    public void verifyStoreSiteMapElementsWithNullList() {
        final Map<String, List<TargetPointOfServiceData>> storeLoc = new HashMap<String, List<TargetPointOfServiceData>>();
        storeLoc.put("State", null);
        given(storeLocator.getAllStateAndStores()).willReturn(storeLoc);
        given(siteMap.getChangeFrequency()).willReturn(ChangeFrequency.MONTHLY);
        given(siteMap.getPriority()).willReturn(STORE_SITEMAP_PRIORITY);
        given(configurationService.getConfiguration().getString("tgtstorefront.sitemap.storePage.changeFreq"))
                .willReturn(STORE_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal("tgtstorefront.sitemap.storePage.priority"))
                .willReturn(STORE_SITEMAP_PRIORITY);
        final SiteMapData result = siteMapController.getStoreSiteMap();
        assertThat(result.getUrls()).isEmpty();
    }

    @Test
    public void verifyStoreSiteMapElementsWithNullAndValidList() {
        final Map<String, List<TargetPointOfServiceData>> storeLoc = new HashMap<String, List<TargetPointOfServiceData>>();
        final TargetPointOfServiceData storeParramatta = new TargetPointOfServiceData();
        storeParramatta.setUrl("/nsw/stores/paramatta");
        final TargetPointOfServiceData storePenrith = new TargetPointOfServiceData();
        storePenrith.setUrl("/nsw/stores/penrith");
        final List<TargetPointOfServiceData> storeList = new ArrayList<>();
        storeList.add(storeParramatta);
        storeList.add(storePenrith);
        storeLoc.put("NSW", storeList);
        storeLoc.put("VIC", null);
        given(storeLocator.getAllStateAndStores()).willReturn(storeLoc);
        given(siteMap.getChangeFrequency()).willReturn(ChangeFrequency.MONTHLY);
        given(siteMap.getPriority()).willReturn(STORE_SITEMAP_PRIORITY);
        given(configurationService.getConfiguration().getString("tgtstorefront.sitemap.storePage.changeFreq"))
                .willReturn(STORE_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal("tgtstorefront.sitemap.storePage.priority"))
                .willReturn(STORE_SITEMAP_PRIORITY);
        final SiteMapData result = siteMapController.getStoreSiteMap();
        final SiteMapUrlData storeurl = result.getUrls().get(0);
        final SiteMapUrlData storeurl1 = result.getUrls().get(1);
        assertThat(result.getUrls()).hasSize(2);
        assertThat(storeurl.getLocation()).isEqualTo(buildAbsoluteUrl(storeParramatta.getUrl()));
        assertThat(storeurl.getChangeFrequency()).isEqualTo(SiteMapUrlData.ChangeFrequency.MONTHLY);
        assertThat(storeurl.getLastModified()).isNull();
        assertThat(storeurl.getPriority()).isEqualTo(BigDecimal.valueOf(0.6d));
        assertThat(storeurl1.getLocation()).isEqualTo(buildAbsoluteUrl(storePenrith.getUrl()));
        assertThat(storeurl1.getChangeFrequency()).isEqualTo(SiteMapUrlData.ChangeFrequency.MONTHLY);
        assertThat(storeurl1.getLastModified()).isNull();
        assertThat(storeurl1.getPriority()).isEqualTo(BigDecimal.valueOf(0.6d));
    }


    @Test
    public void verifyStoreSiteMapElementsWithEmptyMap() {
        final Map<String, List<TargetPointOfServiceData>> storeLoc = new HashMap<String, List<TargetPointOfServiceData>>();
        given(storeLocator.getAllStateAndStores()).willReturn(storeLoc);
        given(siteMap.getChangeFrequency()).willReturn(ChangeFrequency.MONTHLY);
        given(siteMap.getPriority()).willReturn(STORE_SITEMAP_PRIORITY);
        given(configurationService.getConfiguration().getString("tgtstorefront.sitemap.storePage.changeFreq"))
                .willReturn(STORE_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal("tgtstorefront.sitemap.storePage.priority"))
                .willReturn(STORE_SITEMAP_PRIORITY);
        final SiteMapData result = siteMapController.getStoreSiteMap();
        assertThat(result.getUrls()).isEmpty();
    }

    @Test(expected = HttpNotFoundException.class)
    public void verifyMerchProductSiteMapWithInvalidValues() throws TargetEndecaException, ENEQueryException {
        final String merchCode = "aaaa";
        final String pageNumber = "bbbb";
        siteMapController.getMerchDeptSiteMap(merchCode, pageNumber);
    }


    @Test
    public void verifyMerchProductSiteMapWithDeptNotExists() throws TargetEndecaException, ENEQueryException {
        final int merchCode = 102;
        final int pageNumber = 1;
        final String merchCodeStr = "102";
        final String pageNumberStr = "1";
        given(targetSiteMapFacade.getSiteMapColourVariantUrl(merchCode, pageNumber, 10)).willReturn(null);
        final SiteMapData siteMapData = siteMapController.getMerchDeptSiteMap(merchCodeStr, pageNumberStr);
        assertThat(siteMapData.getUrls()).isEmpty();
    }

    @Test(expected = HttpNotFoundException.class)
    public void verifyMerchProductSiteMapWithNegativeValues() throws TargetEndecaException, ENEQueryException {
        final String merchCodeStr = "102";
        final String pageNumberStr = "-1";
        siteMapController.getMerchDeptSiteMap(merchCodeStr, pageNumberStr);
    }

    @Test
    public void verifyMerchProductSiteMapDeptWithNoProducts() throws TargetEndecaException, ENEQueryException {
        final int merchCode = 102;
        final int pageNumber = 1;
        final String merchCodeStr = "102";
        final String pageNumberStr = "1";
        final List<String> productData = new ArrayList<>();
        given(targetSiteMapFacade.getSiteMapColourVariantUrl(merchCode, pageNumber, 10)).willReturn(productData);
        final SiteMapData siteMapData = siteMapController.getMerchDeptSiteMap(merchCodeStr, pageNumberStr);
        assertThat(siteMapData.getUrls()).isEmpty();
    }

    @Test
    public void verifyMerchProductSiteMapDeptWithProducts() throws TargetEndecaException, ENEQueryException {
        final int merchCode = 102;
        final int pageNumber = 1;
        final String merchCodeStr = "102";
        final String pageNumberStr = "1";
        final List<String> productDataList = new ArrayList<>();
        productDataList.add("/AFL/CP2033/");
        given(configurationService.getConfiguration().getString(
                "tgtstorefront.sitemap.productPage.changeFreq")).willReturn(PRODUCT_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal(
                "tgtstorefront.sitemap.productPage.priority")).willReturn(PRODUCT_SITEMAP_PRIORITY);
        given(targetSiteMapFacade.getSiteMapColourVariantUrl(merchCode, pageNumber, 10)).willReturn(productDataList);
        final SiteMapData siteMapData = siteMapController.getMerchDeptSiteMap(merchCodeStr, pageNumberStr);
        final SiteMapUrlData siteMapUrl = siteMapData.getUrls().get(0);
        assertThat(siteMapData.getUrls()).hasSize(1);
        assertThat(siteMapUrl.getLocation()).isEqualTo(buildAbsoluteUrl("/AFL/CP2033/"));
        assertThat(siteMapUrl.getChangeFrequency().toString()).isEqualTo(PRODUCT_SITEMAP_CHANGE_FREQUENCY);
        assertThat(siteMapUrl.getPriority().abs()).isEqualTo(PRODUCT_SITEMAP_PRIORITY);

    }

    @Test
    public void verifyMerchProductSiteMapDeptWithMoreProducts() throws TargetEndecaException, ENEQueryException {
        final int merchCode = 102;
        final int pageNumber = 1;
        final String merchCodeStr = "102";
        final String pageNumberStr = "1";
        final List<String> productDataList = new ArrayList<>();
        productDataList.add("/AFL/CP2033/");
        productDataList.add("/AFL/CP2013/");
        given(configurationService.getConfiguration().getString(
                "tgtstorefront.sitemap.productPage.changeFreq")).willReturn(PRODUCT_SITEMAP_CHANGE_FREQUENCY);
        given(configurationService.getConfiguration().getBigDecimal(
                "tgtstorefront.sitemap.productPage.priority")).willReturn(PRODUCT_SITEMAP_PRIORITY);
        given(targetSiteMapFacade.getSiteMapColourVariantUrl(merchCode, pageNumber, 10)).willReturn(productDataList);
        final SiteMapData siteMapData = siteMapController.getMerchDeptSiteMap(merchCodeStr, pageNumberStr);
        final SiteMapUrlData siteMapUrl = siteMapData.getUrls().get(0);
        final SiteMapUrlData siteMapUrl1 = siteMapData.getUrls().get(1);
        assertThat(siteMapData.getUrls()).hasSize(2);
        assertThat(siteMapUrl.getLocation()).isEqualTo(buildAbsoluteUrl("/AFL/CP2033/"));
        assertThat(siteMapUrl.getChangeFrequency().toString()).isEqualTo(PRODUCT_SITEMAP_CHANGE_FREQUENCY);
        assertThat(siteMapUrl.getPriority().abs()).isEqualTo(PRODUCT_SITEMAP_PRIORITY);
        assertThat(siteMapUrl1.getLocation()).isEqualTo(buildAbsoluteUrl("/AFL/CP2013/"));
        assertThat(siteMapUrl1.getChangeFrequency().toString()).isEqualTo(PRODUCT_SITEMAP_CHANGE_FREQUENCY);
        assertThat(siteMapUrl1.getPriority().abs()).isEqualTo(PRODUCT_SITEMAP_PRIORITY);

    }

    @Test
    public void verifyGetsOnlyCategoriesWithProductsWhenFeatureSwitchEnabled() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CATEGORY_SITEMAP_OPTIMIZATION);
        given(categoryFacade.getCategoriesWithProducts()).willReturn(Collections.EMPTY_LIST);
        final SiteMapData result = siteMapController.getCategorySiteMap();
        assertThat(result.getUrls()).hasSize(0);
    }



    /**
     * Returns absolute URL for given {@code relativeUrl}.
     * 
     * @param relativeUrl
     *            the relative url to be made absolute
     * @return the absolute url
     */
    private String buildAbsoluteUrl(final String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
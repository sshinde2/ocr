package au.com.target.tgtstorefront.controllers.mobileapp;

import de.hybris.bootstrap.annotations.UnitTest;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.user.data.TargetCustomerInfoData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MobileAppCustomerInfoControllerTest {


    @Mock
    private TargetCustomerFacade targetCustomerFacade;

    @InjectMocks
    private final MobileAppCustomerInfoController mobileAppCustomerController = new MobileAppCustomerInfoController();

    @Test
    public void testGetCustomerInfo() {
        final TargetCustomerInfoData targetCustomerInfoData = Mockito.mock(TargetCustomerInfoData.class);
        BDDMockito.given(targetCustomerFacade.getCustomerInfo()).willReturn(targetCustomerInfoData);
        Assertions.assertThat(mobileAppCustomerController.getCustomerInfo()).isEqualTo(targetCustomerInfoData);
    }

}

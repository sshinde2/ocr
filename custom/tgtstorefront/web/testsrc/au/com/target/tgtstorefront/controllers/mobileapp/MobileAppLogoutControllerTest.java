package au.com.target.tgtstorefront.controllers.mobileapp;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtstorefront.security.TargetRememberMeServices;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MobileAppLogoutControllerTest {

    @Mock
    private SecurityContextLogoutHandler mockSecurityContextLogoutHandler;

    @Mock
    private TargetRememberMeServices mockTargetRememberMeServices;

    @InjectMocks
    private final MobileAppLogoutController mobileAppLogoutController = new MobileAppLogoutController();

    @Test
    public void testLogout() throws Exception {
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        final HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        final Response response = mobileAppLogoutController.logout(mockRequest, mockResponse);

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNull();

        final InOrder order = inOrder(mockSecurityContextLogoutHandler, mockTargetRememberMeServices);

        final ArgumentCaptor<Authentication> authenticationCaptor = ArgumentCaptor.forClass(Authentication.class);
        order.verify(mockSecurityContextLogoutHandler).logout(eq(mockRequest), eq(mockResponse),
                authenticationCaptor.capture());
        order.verify(mockTargetRememberMeServices).logout(mockRequest, mockResponse,
                authenticationCaptor.getValue());
    }

}

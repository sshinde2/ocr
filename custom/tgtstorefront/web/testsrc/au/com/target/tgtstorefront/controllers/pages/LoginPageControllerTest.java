/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.ui.Model;

import au.com.target.tgtstorefront.util.PageTitleResolver;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class LoginPageControllerTest {

    @Mock
    private HttpSessionRequestCache httpSessionRequestCache;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HttpSession session;

    @Mock
    private Model model;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private I18NService i18nService;

    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private final LoginPageController loginPageController = new LoginPageController();

    @Test
    public void testLoginSetForwardPageIfComingFromFavourites() throws CMSItemNotFoundException {
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        given(cmsPageService.getPageForLabelOrId("login")).willReturn(contentPageModel);
        loginPageController.doLogin("http:localhost/favourites", false, model, request, response, session);
        verify(model).addAttribute("forwardPage", "favourites");
    }

    @Test
    public void testLoginDontSetForwardPageIfNotComingFromFavourites() throws CMSItemNotFoundException {
        final ContentPageModel contentPageModel = mock(ContentPageModel.class);
        given(cmsPageService.getPageForLabelOrId("login")).willReturn(contentPageModel);
        loginPageController.doLogin("http:localhost", false, model, request, response, session);
        verify(model, never()).addAttribute("forwardPage", "favourites");
    }

}

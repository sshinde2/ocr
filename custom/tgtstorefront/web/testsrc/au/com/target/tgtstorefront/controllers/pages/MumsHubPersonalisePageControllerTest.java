/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionSource;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.forms.MumsHubChildForm;
import au.com.target.tgtstorefront.forms.MumsHubPersonaliseForm;
import au.com.target.tgtstorefront.forms.ThreePartDateForm;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MumsHubPersonalisePageControllerTest {

    @Mock
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Spy
    @InjectMocks
    private final MumsHubPersonalisePageController mumsHubPersonalisePageController = new MumsHubPersonalisePageController();


    @Test
    public void testTransformFormToDto() {
        final MumsHubPersonaliseForm form = new MumsHubPersonaliseForm();
        form.setEmail("testEmail");
        form.setTitleCode("testTitle");
        form.setLastName("testLastName");
        form.setGender("testGender");
        form.setFirstName("testFirstName");
        form.setExpectedGender("testExpectedGender");
        final ThreePartDateForm dueDate = new ThreePartDateForm();
        dueDate.setDay("11");
        dueDate.setMonth("11");
        dueDate.setYear("2015");
        form.setDueDate(dueDate);
        final ThreePartDateForm dateOfBirth = new ThreePartDateForm();
        dateOfBirth.setDay("01");
        dateOfBirth.setMonth("01");
        dateOfBirth.setYear("1970");
        form.setDateOfBirth(dateOfBirth);
        final MumsHubChildForm childForm1 = new MumsHubChildForm();
        childForm1.setFirstName("testChildName1");
        childForm1.setGender("testChildGender1");
        final ThreePartDateForm childDateOfBirth1 = new ThreePartDateForm();
        childDateOfBirth1.setDay("01");
        childDateOfBirth1.setMonth("01");
        childDateOfBirth1.setYear("2000");
        childForm1.setDateOfBirth(childDateOfBirth1);
        final List<MumsHubChildForm> childFormList = new ArrayList<>();
        childFormList.add(childForm1);
        form.setChildren(childFormList);

        final TargetCustomerSubscriptionRequestDto resultDto = mumsHubPersonalisePageController
                .transformFormToDto(form);

        final SimpleDateFormat sdf = new SimpleDateFormat("YYYY/MM/dd");
        Assert.assertEquals("testEmail", resultDto.getCustomerEmail());
        Assert.assertEquals("testExpectedGender", resultDto.getBabyGender());
        Assert.assertEquals("testFirstName", resultDto.getFirstName());
        Assert.assertEquals("testGender", resultDto.getGender());
        Assert.assertEquals("testLastName", resultDto.getLastName());
        Assert.assertEquals("testTitle", resultDto.getTitle());
        Assert.assertEquals("1970/01/01", sdf.format(resultDto.getBirthday()));
        Assert.assertEquals(TargetCustomerSubscriptionSource.MUMSHUB, resultDto.getCustomerSubscriptionSource());
        Assert.assertEquals("2015/11/11", sdf.format(resultDto.getDueDate()));
        Assert.assertTrue(resultDto.isUpdateCustomerPersonalDetails());
        Assert.assertEquals("testChildName1", resultDto.getChildrenDetails().get(0).getFirstName());
        Assert.assertEquals("testChildGender1", resultDto.getChildrenDetails().get(0).getGender());
        Assert.assertEquals("2000/01/01", sdf.format(resultDto.getChildrenDetails().get(0).getBirthday()));
    }

    @Test
    public void testTransformFormToDtoWithNullForm() {
        final MumsHubPersonaliseForm form = null;

        final TargetCustomerSubscriptionRequestDto resultDto = mumsHubPersonalisePageController
                .transformFormToDto(form);

        Assert.assertNull(resultDto);
    }

    @Test
    public void testTransformFormToDtoWithMoreThanFiveChildInfo() {
        final MumsHubPersonaliseForm form = new MumsHubPersonaliseForm();
        form.setEmail("testEmail");
        form.setTitleCode("testTitle");
        form.setLastName("testLastName");
        form.setGender("testGender");
        form.setFirstName("testFirstName");
        form.setExpectedGender("testExpectedGender");
        final ThreePartDateForm dueDate = new ThreePartDateForm();
        dueDate.setDay("11");
        dueDate.setMonth("11");
        dueDate.setYear("2015");
        form.setDueDate(dueDate);
        final ThreePartDateForm dateOfBirth = new ThreePartDateForm();
        dateOfBirth.setDay("01");
        dateOfBirth.setMonth("01");
        dateOfBirth.setYear("1970");
        form.setDateOfBirth(dateOfBirth);
        final MumsHubChildForm childForm1 = new MumsHubChildForm();
        childForm1.setFirstName("testChildName1");
        final MumsHubChildForm childForm2 = new MumsHubChildForm();
        childForm2.setFirstName("testChildName2");
        final MumsHubChildForm childForm3 = new MumsHubChildForm();
        childForm3.setFirstName("testChildName3");
        final MumsHubChildForm childForm4 = new MumsHubChildForm();
        childForm4.setFirstName("testChildName4");
        final MumsHubChildForm childForm5 = new MumsHubChildForm();
        childForm5.setFirstName("testChildName5");
        final MumsHubChildForm childForm6 = new MumsHubChildForm();
        childForm6.setFirstName("testChildName6");

        final List<MumsHubChildForm> childFormList = new ArrayList<>();
        childFormList.add(childForm1);
        childFormList.add(childForm2);
        childFormList.add(childForm3);
        childFormList.add(childForm4);
        childFormList.add(childForm5);
        childFormList.add(childForm6);
        form.setChildren(childFormList);

        final TargetCustomerSubscriptionRequestDto resultDto = mumsHubPersonalisePageController
                .transformFormToDto(form);

        final SimpleDateFormat sdf = new SimpleDateFormat("YYYY/MM/dd");
        Assert.assertEquals("testEmail", resultDto.getCustomerEmail());
        Assert.assertEquals("testExpectedGender", resultDto.getBabyGender());
        Assert.assertEquals("testFirstName", resultDto.getFirstName());
        Assert.assertEquals("testGender", resultDto.getGender());
        Assert.assertEquals("testLastName", resultDto.getLastName());
        Assert.assertEquals("testTitle", resultDto.getTitle());
        Assert.assertEquals("1970/01/01", sdf.format(resultDto.getBirthday()));
        Assert.assertEquals(TargetCustomerSubscriptionSource.MUMSHUB, resultDto.getCustomerSubscriptionSource());
        Assert.assertEquals("2015/11/11", sdf.format(resultDto.getDueDate()));
        Assert.assertTrue(resultDto.isUpdateCustomerPersonalDetails());
        Assert.assertEquals(5, resultDto.getChildrenDetails().size());
    }

    @Test
    public void testGetEnewsSubscribeWithNullEmailInSession() throws CMSItemNotFoundException {
        final Model model = new ExtendedModelMap();
        final HttpServletRequest request = new MockHttpServletRequest();
        final RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        BDDMockito.doReturn(ControllerConstants.Redirection.MUMS_HUB_LANDING).when(mumsHubPersonalisePageController)
                .redirectToMumsHubLandingPage();
        final String result = mumsHubPersonalisePageController
                .getEnewsSubscribe(model, request, redirectAttributes);
        Assert.assertEquals(ControllerConstants.Redirection.MUMS_HUB_LANDING, result);
    }

    @Test
    public void testGetEnewsSubscribeWithEmailInSession() throws CMSItemNotFoundException {
        final Model model = new ExtendedModelMap();
        final HttpServletRequest request = new MockHttpServletRequest();
        request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL,
                "testEmail@test.com.au");
        final RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        final ContentPageModel contentPageModel = Mockito.mock(ContentPageModel.class);
        BDDMockito.doReturn(contentPageModel).when(mumsHubPersonalisePageController)
                .getContentPageForLabelOrId("mums-hub-personalise");
        BDDMockito.doNothing().when(mumsHubPersonalisePageController)
                .storeCmsPageInModel(model, contentPageModel);
        final String result = mumsHubPersonalisePageController
                .getEnewsSubscribe(model, request, redirectAttributes);
        Assert.assertEquals(ControllerConstants.Views.Pages.Account.ACCOUNT_MUMS_HUB_PERSONALISE_PAGE, result);
    }

    @Test
    public void testdoSubscribeWithNullEmailInSession() throws CMSItemNotFoundException {
        final Model model = new ExtendedModelMap();
        final HttpServletRequest request = new MockHttpServletRequest();
        final MumsHubPersonaliseForm mumsHubPersonaliseForm = new MumsHubPersonaliseForm();
        final BindingResult bindingResult = Mockito.mock(BindingResult.class);
        final RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        BDDMockito.doReturn(ControllerConstants.Redirection.MUMS_HUB_LANDING).when(mumsHubPersonalisePageController)
                .redirectToMumsHubLandingPage();
        final String result = mumsHubPersonalisePageController
                .doSubscribe(mumsHubPersonaliseForm, bindingResult, model, request, redirectAttributes);
        Assert.assertEquals(ControllerConstants.Redirection.MUMS_HUB_LANDING, result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testdoSubscribeWithEmailInSessionWithNoValidationError() throws CMSItemNotFoundException {
        final Model model = new ExtendedModelMap();
        final HttpServletRequest request = new MockHttpServletRequest();
        request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL,
                "testEmail@test.com.au");
        final MumsHubPersonaliseForm mumsHubPersonaliseForm = new MumsHubPersonaliseForm();
        final BindingResult bindingResult = Mockito.mock(BindingResult.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(false);
        final RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        BDDMockito.doReturn(ControllerConstants.Redirection.MUMS_HUB_LANDING).when(mumsHubPersonalisePageController)
                .redirectToMumsHubLandingPage();
        final String result = mumsHubPersonalisePageController
                .doSubscribe(mumsHubPersonaliseForm, bindingResult, model, request, redirectAttributes);
        Assert.assertEquals(ControllerConstants.Redirection.MUMS_HUB_LANDING, result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testdoSubscribeWithEmailInSessionWithValidationError() throws CMSItemNotFoundException {
        final Model model = new ExtendedModelMap();
        final HttpServletRequest request = new MockHttpServletRequest();
        request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL,
                "testEmail@test.com.au");
        final MumsHubPersonaliseForm mumsHubPersonaliseForm = new MumsHubPersonaliseForm();
        final BindingResult bindingResult = Mockito.mock(BindingResult.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(true);
        final RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        final ContentPageModel contentPageModel = Mockito.mock(ContentPageModel.class);
        BDDMockito.doReturn(contentPageModel).when(mumsHubPersonalisePageController)
                .getContentPageForLabelOrId("mums-hub-personalise");
        BDDMockito.doNothing().when(mumsHubPersonalisePageController)
                .storeCmsPageInModel(model, contentPageModel);
        final String result = mumsHubPersonalisePageController
                .doSubscribe(mumsHubPersonaliseForm, bindingResult, model, request, redirectAttributes);
        Assert.assertEquals(ControllerConstants.Views.Pages.Account.ACCOUNT_MUMS_HUB_PERSONALISE_PAGE, result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testdoSubscribeWithEmailInSessionWithNoValidationErrorWithMumsHubLandingPageMissing()
            throws CMSItemNotFoundException {
        final Model model = new ExtendedModelMap();
        final HttpServletRequest request = new MockHttpServletRequest();
        request.getSession().setAttribute(ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL,
                "testEmail@test.com.au");
        final MumsHubPersonaliseForm mumsHubPersonaliseForm = new MumsHubPersonaliseForm();
        final BindingResult bindingResult = Mockito.mock(BindingResult.class);
        BDDMockito.when(bindingResult.hasErrors()).thenReturn(false);
        final RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();
        BDDMockito.doReturn(ControllerConstants.Redirection.HOME).when(mumsHubPersonalisePageController)
                .redirectToMumsHubLandingPage();
        final String result = mumsHubPersonalisePageController
                .doSubscribe(mumsHubPersonaliseForm, bindingResult, model, request, redirectAttributes);
        Assert.assertEquals(ControllerConstants.Redirection.HOME, result);
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.endeca.infront.dto.CustomerSubscriptionInfo;
import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MumsHubSubscriptionControllerTest {


    private HttpServletRequest request = new MockHttpServletRequest();

    @Mock
    private HttpServletResponse response;

    @Mock
    private MessageSource messageSource;

    @Mock
    private I18NService i18nService;

    @Mock
    private Model model;

    @Mock
    private TargetCustomerSubscriptionResponseDto result;

    @Mock
    private BindingResult bindingResult;

    @Mock
    private RedirectAttributes redirectAttributes;

    @Mock
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;
    private final CustomerSubscriptionInfo customerSubscriptionInfo = new CustomerSubscriptionInfo();

    @InjectMocks
    private final MumsHubSubscriptionPageController mumbsHubSubscriptionController = new MumsHubSubscriptionPageController();

    @Test
    public void testDoMumsHubSubscribeWithEmptyEmail() throws CMSItemNotFoundException {
        customerSubscriptionInfo.setEmail("");
        customerSubscriptionInfo.setSubscriptionSource("MumsHub");
        final String url = mumbsHubSubscriptionController.doMumsHubSubscribe(model, customerSubscriptionInfo, request,
                response);
        verify(model).addAttribute("subscriptionType", "MumsHub");
        verify(model).addAttribute("messageType", "error");
        assertEquals(url, ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT);
    }

    @Test
    public void testDoMumsHubSubscribeWithNullEmail() throws CMSItemNotFoundException {
        customerSubscriptionInfo.setEmail(null);
        customerSubscriptionInfo.setSubscriptionSource("MumsHub");
        final String url = mumbsHubSubscriptionController.doMumsHubSubscribe(model, customerSubscriptionInfo, request,
                response);
        verify(model).addAttribute("subscriptionType", "MumsHub");
        verify(model).addAttribute("messageType", "error");
        assertEquals(url, ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT);
    }

    @Test
    public void testDoMumsHubSubscribeWithInvalidEmail() throws CMSItemNotFoundException {
        customerSubscriptionInfo.setEmail("abcdefgh");
        customerSubscriptionInfo.setSubscriptionSource("MumsHub");
        final String url = mumbsHubSubscriptionController.doMumsHubSubscribe(model, customerSubscriptionInfo, request,
                response);
        verify(model).addAttribute("subscriptionType", "MumsHub");
        verify(model).addAttribute("messageType", "error");
        assertEquals(url, ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT);
    }

    @Test
    public void testDoMumsHubSubscribeWithValidEmail() throws CMSItemNotFoundException {
        customerSubscriptionInfo.setEmail("test@target.com.au");
        customerSubscriptionInfo.setSubscriptionSource("MumsHub");
        final String url = mumbsHubSubscriptionController.doMumsHubSubscribe(model, customerSubscriptionInfo, request,
                response);
        verify(model).addAttribute("subscriptionType", "MumsHub");
        verify(model).addAttribute("messageType", "success");
        assertEquals(url, ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT);
    }

    @Test
    public void testDoMumsHubSubscribeWithEmptySource() throws CMSItemNotFoundException {
        customerSubscriptionInfo.setEmail("test@target.com.au");
        customerSubscriptionInfo.setSubscriptionSource("");
        final String url = mumbsHubSubscriptionController.doMumsHubSubscribe(model, customerSubscriptionInfo, request,
                response);
        verify(model).addAttribute("subscriptionType", "MumsHub");
        verify(model).addAttribute("messageType", "success");
        assertEquals(url, ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT);
    }

    @Test
    public void testDoMumsHubSubscribeWithValidEmailEqualsEmailInSession() throws CMSItemNotFoundException {
        request = new MockHttpServletRequest();
        request.getSession().setAttribute(
                ControllerConstants.SESSION_PARAM_MUMS_HUB_SUBSCRIPTION_EMAIL, "test@target.com.au");
        customerSubscriptionInfo.setEmail("test@target.com.au");
        customerSubscriptionInfo.setSubscriptionSource("MumsHub");
        final String url = mumbsHubSubscriptionController.doMumsHubSubscribe(model, customerSubscriptionInfo, request,
                response);
        verify(model).addAttribute("subscriptionType", "MumsHub");
        verify(model).addAttribute("messageType", "success");
        assertEquals(url, ControllerConstants.Views.Fragments.Enews.ENEWS_JSON_FRAGMENT);
    }
}

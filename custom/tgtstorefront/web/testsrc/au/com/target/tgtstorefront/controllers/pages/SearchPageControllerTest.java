/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtutility.util.JsonConversionUtil;
import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerException;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import au.com.target.endeca.infront.converter.EndecaToSortConverter;
import au.com.target.endeca.infront.data.EndecaRecords;
import au.com.target.endeca.infront.data.EndecaSearch;
import au.com.target.endeca.infront.data.EndecaSearchStateData;
import au.com.target.endeca.infront.exception.TargetEndecaWrapperException;
import au.com.target.endeca.infront.transform.TargetEndecaURLHelper;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData;
import au.com.target.tgtfacades.url.impl.TargetProductModelUrlResolver;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.breadcrumb.impl.SearchBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.pages.data.SeachEvaluatorData;
import au.com.target.tgtstorefront.controllers.pages.data.ViewPreferencesInitialData;
import au.com.target.tgtstorefront.controllers.pages.preferences.StoreViewPreferencesHandler;
import au.com.target.tgtstorefront.controllers.util.TargetEndecaSearchHelper;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;



/**
 * @author asingh78
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SearchPageControllerTest {

    /**
     * 
     */
    private static final String SEARCH_WHITE_LIST_CHAR = "[^a-zA-Z0-9\"'+-]";

    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Configuration mockConfiguration;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private EndecaSearchStateData endecaSearchStateData;

    @Mock
    private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

    @Mock
    private I18NService i18NService;

    @Mock
    private CMSSiteService cmsSiteService;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private MessageSource messageSource;

    @Mock
    private SessionService sessionService;

    @Mock
    private SiteConfigService siteConfigService;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private StoreViewPreferencesHandler storeViewPreferencesHandler;

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private TargetProductModelUrlResolver targetProductModelUrlResolver;

    @Mock
    private TargetEndecaURLHelper targetEndecaURLHelper;

    @Mock
    private EndecaToSortConverter endecaToSortConverter;

    @Mock
    private TargetEndecaSearchHelper targetEndecaSearchHelper;

    @Mock
    private SeachEvaluatorData seachEvaluatorData;

    @Mock
    private TargetFeatureSwitchFacade mockTargetFeatureSwitchFacade;

    @Mock
    private Model mockModel;

    private Model model;

    @InjectMocks
    @Spy
    private final SearchPageController searchPageController = new SearchPageController();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        model = new BindingAwareModelMap();
        given(configurationService.getConfiguration()).willReturn(mockConfiguration);
        given(mockConfiguration.getString("search.white.list.char",
                SEARCH_WHITE_LIST_CHAR))
                .willReturn("[^a-zA-Z0-9\"'$+-]");
        given(mockConfiguration.getInteger(anyString(),
                any(Integer.class)))
                .willReturn(Integer.valueOf(10));
        searchPageController.setSearchQuerySanitiser(new SearchQuerySanitiser(SEARCH_WHITE_LIST_CHAR));
        given(targetEndecaSearchHelper.handlePreferencesInitialData(any(HttpServletRequest.class),
                any(HttpServletResponse.class), anyString(), anyString(), anyString(),
                anyString(), anyString(), anyString()))
                .willReturn(seachEvaluatorData);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEndecaSearch() throws CMSItemNotFoundException, JAXBException, TransformerException, IOException,
            TargetEndecaWrapperException {
        final String searchText = "searchText";
        final String page = "1";
        final String navigationState = "&Ns=22233";
        final String sortCode = "productName";
        final String viewAs = "list";
        final String pageSize = "30";
        final EndecaSearch search = new EndecaSearch();
        search.setRecords(new EndecaRecords());
        final List<String> navStateList = new ArrayList<>();
        navStateList.add(navigationState);
        endecaSearchStateData.setNavigationState(navStateList);
        endecaSearchStateData.setItemsPerPage(30);
        endecaSearchStateData.setPageNumber(1);
        endecaSearchStateData.setPersistAvailableOnlineFacet(false);

        given(targetEndecaSearchHelper.sanitizeSearchText(searchText, mockModel)).willReturn(searchText);
        given(targetEndecaSearchHelper.getEndecaSearchResult(request, endecaSearchStateData)).willReturn(
                search);
        given(searchBreadcrumbBuilder.getBreadcrumbs(any(CategoryModel.class),
                any(BrandModel.class), anyList(), anyString(),
                any(EndecaSearch.class)))
                .willReturn(new ArrayList<Breadcrumb>());
        given(pageTitleResolver.resolveContentPageTitle(anyString())).willReturn("test Page");
        given(targetEndecaSearchHelper.handlePreferencesInitialData(request, response, StringUtils.EMPTY,
                navigationState, sortCode, viewAs, pageSize, StringUtils.EMPTY))
                .willReturn(
                        seachEvaluatorData);
        given(targetEndecaURLHelper.buildEndecaSearchStateData(anyString(), anyList(),
                anyString(), anyInt(), anyInt(),
                anyString(), anyString(), (String)Matchers.isNull(), (String)Matchers.isNull())).willReturn(
                endecaSearchStateData);
        searchPageController.textSearch(searchText, page, sortCode, viewAs, pageSize, navigationState, "false",
                request,
                response, mockModel);
        verify(targetEndecaSearchHelper).getEndecaSearchResult(request, endecaSearchStateData);
        verify(targetEndecaSearchHelper).populateSearchPageData(
                any(TargetProductCategorySearchPageData.class),
                any(EndecaSearch.class), any(EndecaSearchStateData.class));
        verify(searchBreadcrumbBuilder).getBreadcrumbs(any(CategoryModel.class),
                any(BrandModel.class), anyList(), anyString(),
                any(EndecaSearch.class));
        verify(mockModel).addAttribute(eq("freeTextSearch"), anyString());
        verifyNoMoreInteractions(searchBreadcrumbBuilder);
        verifyNoMoreInteractions(targetProductService);
        verifyNoMoreInteractions(targetProductModelUrlResolver);
    }

    @Test
    public void testProductSearchReturnsNull() throws CMSItemNotFoundException, JAXBException, TransformerException,
            IOException, TargetEndecaWrapperException {
        final String searchText = "searchText";
        final String page = "1";
        final String navigationState = "&Ns=22233";
        final String sortCode = "productName";
        final String viewAs = "list";
        final String pageSize = "30";
        final EndecaSearch search = new EndecaSearch();
        search.setRecords(null);
        final List<String> navStateList = new ArrayList<>();
        navStateList.add(navigationState);
        endecaSearchStateData.setNavigationState(navStateList);
        endecaSearchStateData.setItemsPerPage(30);
        endecaSearchStateData.setPageNumber(1);
        endecaSearchStateData.setPersistAvailableOnlineFacet(false);

        given(targetEndecaSearchHelper.sanitizeSearchText(searchText, mockModel)).willReturn(searchText);
        given(targetEndecaSearchHelper.getEndecaSearchResult(request, endecaSearchStateData)).willReturn(
                search);
        given(searchBreadcrumbBuilder.getBreadcrumbs(any(CategoryModel.class),
                any(BrandModel.class), anyList(), anyString(),
                any(EndecaSearch.class)))
                .willReturn(new ArrayList<Breadcrumb>());
        given(pageTitleResolver.resolveContentPageTitle(anyString())).willReturn("test Page");
        given(targetEndecaSearchHelper.handlePreferencesInitialData(request, response, StringUtils.EMPTY,
                navigationState, sortCode, viewAs, pageSize, StringUtils.EMPTY)).willReturn(seachEvaluatorData);
        given(targetEndecaURLHelper.buildEndecaSearchStateData(anyString(), anyList(),
                anyString(), anyInt(), anyInt(), anyString(), anyString(), (String)Matchers.isNull(),
                (String)Matchers.isNull())).willReturn(endecaSearchStateData);
        searchPageController.textSearch(searchText, page, sortCode, viewAs, pageSize, navigationState, "false",
                request, response, mockModel);
        verify(targetEndecaSearchHelper).getEndecaSearchResult(request, endecaSearchStateData);
        verify(targetEndecaSearchHelper).populateSearchPageData(
                any(TargetProductCategorySearchPageData.class),
                any(EndecaSearch.class), any(EndecaSearchStateData.class));
        verify(mockModel).addAttribute(eq("freeTextSearch"), anyString());
        verifyNoMoreInteractions(searchBreadcrumbBuilder);
        verifyNoMoreInteractions(searchBreadcrumbBuilder);
        verifyNoMoreInteractions(targetProductService);
        verifyNoMoreInteractions(targetProductModelUrlResolver);
    }

    @Test
    public void testProductSearchThrowsUnknownIdentifierException() throws CMSItemNotFoundException, JAXBException,
            TransformerException,
            IOException, TargetEndecaWrapperException {
        given(targetProductService.getProductForCode(anyString())).willThrow(
                new UnknownIdentifierException("Do not know of this product"));

        final String searchText = "P123445";
        final String page = "1";
        final String navigationState = "&Ns=22233";
        final String sortCode = "productName";
        final String viewAs = "list";
        final String pageSize = "30";
        final EndecaSearch search = new EndecaSearch();
        search.setRecords(new EndecaRecords());
        final List<String> navStateList = new ArrayList<>();
        navStateList.add(navigationState);
        endecaSearchStateData.setNavigationState(navStateList);
        endecaSearchStateData.setItemsPerPage(30);
        endecaSearchStateData.setPageNumber(1);
        endecaSearchStateData.setPersistAvailableOnlineFacet(false);
        given(targetEndecaSearchHelper.sanitizeSearchText(searchText, model)).willReturn(searchText);
        given(targetEndecaSearchHelper.getEndecaSearchResult(request, endecaSearchStateData)).willReturn(
                search);
        given(searchBreadcrumbBuilder.getBreadcrumbs(any(CategoryModel.class),
                any(BrandModel.class), anyList(), anyString(),
                any(EndecaSearch.class)))
                .willReturn(new ArrayList<Breadcrumb>());
        given(pageTitleResolver.resolveContentPageTitle(anyString())).willReturn("test Page");
        given(targetEndecaSearchHelper.handlePreferencesInitialData(request, response, StringUtils.EMPTY,
                navigationState, sortCode, viewAs, pageSize, StringUtils.EMPTY))
                .willReturn(
                        seachEvaluatorData);
        given(targetEndecaURLHelper.buildEndecaSearchStateData(anyString(), anyList(),
                anyString(), anyInt(), anyInt(),
                anyString(), anyString(), (String)Matchers.isNull(), (String)Matchers.isNull())).willReturn(
                endecaSearchStateData);
        searchPageController.textSearch(searchText, page, sortCode, viewAs, pageSize, navigationState, "false",
                request,
                response, model);
        verify(targetEndecaSearchHelper).getEndecaSearchResult(request, endecaSearchStateData);
        verify(targetEndecaSearchHelper).populateSearchPageData(
                any(TargetProductCategorySearchPageData.class),
                any(EndecaSearch.class), any(EndecaSearchStateData.class));
        verify(searchBreadcrumbBuilder).getBreadcrumbs(any(CategoryModel.class),
                any(BrandModel.class), anyList(), anyString(),
                any(EndecaSearch.class));
        verifyNoMoreInteractions(searchBreadcrumbBuilder);
        verifyNoMoreInteractions(targetProductModelUrlResolver);

    }

    @Test
    public void testProductSearch() throws CMSItemNotFoundException, JAXBException, TransformerException, IOException {
        given(pageTitleResolver.resolveContentPageTitle(anyString())).willReturn("test Page");
        given(storeViewPreferencesHandler.getViewAsPreference(any(HttpServletResponse.class),
                any(ViewPreferencesInitialData.class), anyString())).willReturn("preferences page");

        given(targetProductService.getProductForCode(anyString())).willReturn(
                new TargetProductModel());
        given(targetProductModelUrlResolver.resolve(any(ProductModel.class))).willReturn(
                "/p/P123445");
        given(targetEndecaSearchHelper.sanitizeSearchText("P123445", model)).willReturn("P123445");
        searchPageController.textSearch("P123445", "1", "productName", "list", "30", "&Ns=22233", "false", request,
                response,
                model);

        verify(targetProductService).getProductForCode(anyString());
        verify(targetProductModelUrlResolver).resolve(any(ProductModel.class));
        verifyNoMoreInteractions(searchBreadcrumbBuilder);
        verifyNoMoreInteractions(targetProductService);
        verifyNoMoreInteractions(targetProductModelUrlResolver);
    }

    @Test
    public void testProductSearchForBreadCrumb() throws CMSItemNotFoundException, JAXBException, TransformerException,
            IOException, TargetEndecaWrapperException {
        given(pageTitleResolver.resolveContentPageTitle(anyString())).willReturn("test Page");
        given(storeViewPreferencesHandler.getViewAsPreference(any(HttpServletResponse.class),
                any(ViewPreferencesInitialData.class), anyString())).willReturn("preferences page");

        given(targetProductService.getProductForCode(anyString())).willReturn(
                null);

        final List<String> navStateList = new ArrayList<>();
        navStateList.add("&Ns=22233");
        given(endecaSearchStateData.getNavigationState()).willReturn(navStateList);
        given(Integer.valueOf(endecaSearchStateData.getItemsPerPage())).willReturn(Integer.valueOf(30));
        given(Integer.valueOf(endecaSearchStateData.getPageNumber())).willReturn(Integer.valueOf(1));

        given(targetEndecaURLHelper.buildEndecaSearchStateData(anyString(), anyList(),
                anyString(), anyInt(), anyInt(), anyString(), anyString(), anyString(), anyString()))
                .willReturn(endecaSearchStateData);
        given(targetEndecaSearchHelper.sanitizeSearchText("P123445", mockModel)).willReturn("P123445");

        searchPageController.textSearch("P123445", "1", "productName", "list", "30", "&Ns=22233", "false", request,
                response, mockModel);

        verify(targetProductService).getProductForCode(anyString());
        verify(targetEndecaSearchHelper).getEndecaSearchResult(request, endecaSearchStateData);
        verify(endecaSearchStateData).setSkipRedirect(false);
        verify(mockModel).addAttribute(eq("freeTextSearch"), anyString());
        verifyNoMoreInteractions(searchBreadcrumbBuilder);
        verifyNoMoreInteractions(targetProductService);
        verifyNoMoreInteractions(targetProductModelUrlResolver);
    }

    @Test
    public void testPopulateSearchTermForBreadcrumb() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(
                TargetProductCategorySearchPageData.class);
        final EndecaSearch searchResults = mock(EndecaSearch.class);
        final List<BreadcrumbData<EndecaSearchStateData>> refinements = new ArrayList<>();
        final List<Breadcrumb> breadcrumbs = new ArrayList<>();
        breadcrumbs.add(mock(Breadcrumb.class));
        doNothing().when(searchPageController).populateModel(model, searchPageData);
        given(searchPageData.getFreeTextSearch()).willReturn("freeTextSearch");
        given(searchPageData.getBreadcrumbs()).willReturn(refinements);
        given(searchBreadcrumbBuilder.getBreadcrumbs(null, null,
                refinements, "cleanSearchText", searchResults)).willReturn(breadcrumbs);
        searchPageController.populateSearchTermForBreadcrumb(endecaSearchStateData, searchPageData, refinements,
                searchResults, "cleanSearchText", model);
        verify(targetEndecaSearchHelper).populateClearAllUrl(searchPageData, searchResults, breadcrumbs);
    }

    @Test
    public void testPopulateReactDataReactListingFeatureSwitchOff() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(TargetProductCategorySearchPageData.class);
        List<ProductData> products = new ArrayList<>();
        given(mockTargetFeatureSwitchFacade.isReactListingEnabled()).willReturn(Boolean.FALSE);
        given(searchPageData.getResults()).willReturn(products);

        searchPageController.populateReactData(searchPageData, mockModel);

        verify(mockModel, never()).addAttribute(eq("productList"), any());
    }

    @Test
    public void testPopulateReactDataReactListingFeatureSwitchOn() {
        final TargetProductCategorySearchPageData<EndecaSearchStateData, ProductData, CategoryData> searchPageData = mock(TargetProductCategorySearchPageData.class);
        ProductData productData = new ProductData();
        productData.setCode("PRODUCT_CODE123");
        List<ProductData> products = new ArrayList<>();
        products.add(productData);
        Map<String, ProductData> productsDataMapExpected = new HashMap<>();
        for (ProductData product : products) {
            productsDataMapExpected.put(productData.getCode(), product);
        }
        final String productsDataJsonExpected = JsonConversionUtil.convertToJsonString(productsDataMapExpected);
        ArgumentCaptor<String> productsDataJsonActual = ArgumentCaptor.forClass(String.class);
        given(mockTargetFeatureSwitchFacade.isReactListingEnabled()).willReturn(Boolean.TRUE);
        given(searchPageData.getResults()).willReturn(products);

        searchPageController.populateReactData(searchPageData, mockModel);

        verify(mockModel).addAttribute(eq("productList"), productsDataJsonActual.capture());
        assertThat(productsDataJsonActual.getValue()).isEqualTo(productsDataJsonExpected);
    }
}

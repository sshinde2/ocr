/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSNavigationService;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtstorefront.breadcrumb.Breadcrumb;
import au.com.target.tgtstorefront.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import au.com.target.tgtstorefront.navigation.SiteMapMenuBuilder;
import au.com.target.tgtstorefront.util.PageTitleResolver;
import au.com.target.tgtwebcore.enums.CachedPageType;


/**
 * @author rmcalave
 * 
 */
public class SiteMapPageControllerTest {
    @InjectMocks
    private final SiteMapPageController siteMapPageControllerReal = new SiteMapPageController();
    private SiteMapPageController siteMapPageController;

    @Mock
    private HttpServletResponse response;

    @Mock
    private CMSPageService mockCmsPageService;

    @Mock
    private Model mockModel;

    @Mock
    private CMSNavigationService mockCmsNavigationService;

    @Mock
    private SiteMapMenuBuilder mockSiteMapMenuBuilder;

    @Mock
    private ContentPageBreadcrumbBuilder mockContentPageBreadcrumbBuilder;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        siteMapPageController = Mockito.spy(siteMapPageControllerReal);
        BDDMockito.given(pageTitleResolver.resolveContentPageTitle(Mockito.anyString())).willReturn("Page Title");
        BDDMockito.doNothing().when(siteMapPageController)
                .addAkamaiCacheAttributes(Mockito.eq(CachedPageType.SITEMAP), Mockito.eq(response),
                        Mockito.any(Model.class));
    }

    @Test
    public void testGetSiteMapPageWithInvalidPageId() throws CMSItemNotFoundException {
        final CMSItemNotFoundException expectedException = new CMSItemNotFoundException("Page not found");
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId(Mockito.anyString())).willThrow(expectedException);

        try {
            siteMapPageController.getSiteMapPage(mockModel, response);
        }
        catch (final CMSItemNotFoundException ex) {
            Assert.assertEquals(expectedException, ex);
            return;
        }

        Assert.fail("Expected exception to be thrown");
    }

    @Test
    public void testGetSiteMapPageWithInvalidNavigationNodes() throws CMSItemNotFoundException {
        final ContentPageModel mockSiteMapContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId(Mockito.anyString()))
                .willReturn(mockSiteMapContentPage);

        final CMSItemNotFoundException expectedException = new CMSItemNotFoundException("Navigation node not found");
        BDDMockito.given(mockCmsNavigationService.getNavigationNodeForId(Mockito.anyString())).willThrow(
                expectedException);

        final Model model = new ExtendedModelMap();
        siteMapPageController.getSiteMapPage(model, response);

        final Map<String, Object> modelMap = model.asMap();

        Assert.assertTrue(modelMap.containsKey("siteMapStructureMap"));
        final Map<String, List<NavigationMenuItem>> siteMapStructureMap = (Map<String, List<NavigationMenuItem>>)modelMap
                .get("siteMapStructureMap");

        Assert.assertTrue(siteMapStructureMap.containsKey("categories"));
        Assert.assertNull(siteMapStructureMap.get("categories"));

        Assert.assertTrue(siteMapStructureMap.containsKey("content"));
        Assert.assertNull(siteMapStructureMap.get("content"));
    }

    @Test
    public void testGetSiteMapPageWithValidNavigationNodes() throws CMSItemNotFoundException {
        // Site Map Content Page
        final ContentPageModel mockSiteMapContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId(Mockito.anyString()))
                .willReturn(mockSiteMapContentPage);

        // Category Navigation Node
        final List<CMSNavigationNodeModel> categoryNavigationNodeChildren = new ArrayList<>();
        final CMSNavigationNodeModel mockCategoryNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCategoryNavigationNode.getChildren()).willReturn(categoryNavigationNodeChildren);

        // Content Navigation Node
        final List<CMSNavigationNodeModel> contentNavigationNodeChildren = new ArrayList<>();
        final CMSNavigationNodeModel mockContentNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentNavigationNode.getChildren()).willReturn(contentNavigationNodeChildren);

        // CMS Navigation Service
        BDDMockito.given(mockCmsNavigationService.getNavigationNodeForId("SiteHomeNavNode")).willReturn(
                mockContentNavigationNode);
        BDDMockito.given(mockCmsNavigationService.getNavigationNodeForId("TargetSiteCategoriesNavNode")).willReturn(
                mockCategoryNavigationNode);


        final Set<NavigationMenuItem> categoryMenuItemSet = new LinkedHashSet<>();
        final Set<NavigationMenuItem> contentMenuItemSet = new LinkedHashSet<>();
        BDDMockito.given(mockSiteMapMenuBuilder.createMenuItemsForChildren(categoryNavigationNodeChildren)).willReturn(
                categoryMenuItemSet);
        BDDMockito.given(mockSiteMapMenuBuilder.createMenuItemsForChildren(contentNavigationNodeChildren)).willReturn(
                contentMenuItemSet);


        final Model model = new ExtendedModelMap();
        siteMapPageController.getSiteMapPage(model, response);

        final Map<String, Object> modelMap = model.asMap();

        Assert.assertTrue(modelMap.containsKey("siteMapStructureMap"));
        final Map<String, Set<NavigationMenuItem>> siteMapStructureMap = (Map<String, Set<NavigationMenuItem>>)modelMap
                .get("siteMapStructureMap");

        Assert.assertTrue(siteMapStructureMap.containsKey("categories"));
        Assert.assertEquals(categoryMenuItemSet, siteMapStructureMap.get("categories"));

        Assert.assertTrue(siteMapStructureMap.containsKey("content"));
        Assert.assertEquals(contentMenuItemSet, siteMapStructureMap.get("content"));
    }

    @Test
    public void testGetSiteMapPageCheckOtherModelAttributes() throws CMSItemNotFoundException {
        final ContentPageModel mockSiteMapContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockCmsPageService.getPageForLabelOrId(Mockito.anyString()))
                .willReturn(mockSiteMapContentPage);

        final List<Breadcrumb> breadcrumbList = new ArrayList<>();
        BDDMockito.given(mockContentPageBreadcrumbBuilder.getBreadcrumbs(mockSiteMapContentPage)).willReturn(
                breadcrumbList);

        final CMSItemNotFoundException expectedException = new CMSItemNotFoundException("Navigation node not found");
        BDDMockito.given(mockCmsNavigationService.getNavigationNodeForId(Mockito.anyString())).willThrow(
                expectedException);

        final Model model = new ExtendedModelMap();
        siteMapPageController.getSiteMapPage(model, response);

        final Map<String, Object> modelMap = model.asMap();

        Assert.assertTrue(modelMap.containsKey("cmsPage"));
        Assert.assertEquals(mockSiteMapContentPage, modelMap.get("cmsPage"));

        Assert.assertTrue(modelMap.containsKey("breadcrumbs"));
        Assert.assertEquals(breadcrumbList, modelMap.get("breadcrumbs"));
    }
}

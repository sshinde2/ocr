/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.breadcrumb.impl.StoreBreadcrumbBuilder;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.forms.StoreFinderForm;
import au.com.target.tgtstorefront.forms.StorePositionForm;
import au.com.target.tgtstorefront.util.PageTitleResolver;


/**
 * @author mjanarth
 *
 */
@UnitTest
public class StorePageControllerTest {

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private StoreBreadcrumbBuilder storeBreadcrumbBuilder;

    @Mock
    private TargetStoreLocatorFacade storeLocatorFacade;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private Model model;

    @InjectMocks
    private final StorePageController controller = new StorePageController();

    @Mock
    private TargetPointOfServiceData pointOfServiceData;

    @Mock
    private StoreBreadcrumbBuilder breadCrumbBuilder;

    @Mock
    private ContentPageModel homepage;

    @Mock
    private ContentPageModel cmsPage;

    @Mock
    private PageTitleResolver pageTitleResolver;

    private final AddressData address = new AddressData();

    private final CountryData country = new CountryData();


    private String viewJsp = null;

    private String storeNumber = null;

    private StorePageController spy;




    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        spy = Mockito.spy(controller);
        country.setName("Australia");
        address.setCountry(country);
        address.setPostalCode("3215");
        address.setTown("Geelong");

    }

    @Test
    public void testGetStoreHoursWithEmptyStoreNumber() {
        viewJsp = controller.getStoreHours(null, model);
        given(storeLocatorFacade.getPointOfService(null)).willReturn(null);
        verify(model).addAttribute("store", null);
        assertThat(viewJsp).isEqualTo(ControllerConstants.Views.Fragments.STORE.STORE_HOURS);
    }

    @Test
    public void testGetStoreHoursWithStoreNumner() {
        storeNumber = "7001";
        given(storeLocatorFacade.getPointOfService(Integer.valueOf(7001))).willReturn(pointOfServiceData);
        viewJsp = controller.getStoreHours(storeNumber, model);
        verify(model).addAttribute("store", pointOfServiceData);
        assertThat(viewJsp).isEqualTo(ControllerConstants.Views.Fragments.STORE.STORE_HOURS);
    }

    @Test
    public void testGetStoreDetailsWithEmptyStoreNumber() throws CMSItemNotFoundException, UnsupportedEncodingException {
        given(storeLocatorFacade.getPointOfService(null)).willReturn(null);
        viewJsp = controller.storeFinderDetails(storeNumber, model, request, response);
        verifyNoMoreInteractions(model);
        assertThat(viewJsp).isEqualTo(ControllerConstants.Forward.ERROR_404);
    }

    @Test
    public void testGetGetStoreDetailsWithStoreNumberWithRedirection() throws CMSItemNotFoundException,
            UnsupportedEncodingException {
        storeNumber = "7001";
        given(storeLocatorFacade.getPointOfService(Integer.valueOf(7001))).willReturn(pointOfServiceData);
        given(pointOfServiceData.getUrl()).willReturn("url");
        doReturn("redirectionurl").when(spy).checkRequestUrl(request, response, "url");
        viewJsp = spy.storeFinderDetails(storeNumber, model, request, response);
        verifyNoMoreInteractions(model);
        assertThat(viewJsp).isEqualTo("redirectionurl");
    }

    @Test
    public void testGetGetStoreDetailsWithStoreNumberWithOutRedirection() throws CMSItemNotFoundException,
            UnsupportedEncodingException {
        storeNumber = "7001";
        given(storeLocatorFacade.getPointOfService(Integer.valueOf(7001))).willReturn(pointOfServiceData);
        given(storeBreadcrumbBuilder.getBreadcrumbs(pointOfServiceData)).willReturn(null);
        given(pointOfServiceData.getAddress()).willReturn(address);
        given(pointOfServiceData.getDescription()).willReturn("Geelong Store");
        given(pointOfServiceData.getUrl()).willReturn("url");
        given(pageTitleResolver.resolveStorePageTitle("Geelong")).willReturn("Geelong");
        given(pointOfServiceData.getName()).willReturn("Geelong");
        given(cmsPageService.getHomepage()).willReturn(homepage);
        given(cmsPageService.getLabelOrId(homepage)).willReturn("store");
        given(cmsPageService.getPageForLabelOrId("storefinder")).willReturn(cmsPage);
        doReturn("").when(spy).checkRequestUrl(request, response, "url");
        final ArgumentCaptor<StoreFinderForm> storeFinderFormCaptor = ArgumentCaptor.forClass(StoreFinderForm.class);
        final ArgumentCaptor<StorePositionForm> storePositionForm = ArgumentCaptor.forClass(StorePositionForm.class);
        viewJsp = spy.storeFinderDetails(storeNumber, model, request, response);
        verify(model).addAttribute(WebConstants.CMS_PAGE_MODEL, cmsPage);
        verify(model).addAttribute(eq("storeFinderForm"), storeFinderFormCaptor.capture());
        verify(model).addAttribute(eq("storePositionForm"), storePositionForm.capture());
        verify(model).addAttribute("metaKeywords", "Geelong,3215,Australia");
        verify(model).addAttribute("metaDescription", "Geelong Store");
        verify(model).addAttribute("store", pointOfServiceData);
        verify(model).addAttribute(WebConstants.BREADCRUMBS_KEY, null);
        verify(model).addAttribute("pageTitle", "Geelong");
        assertThat(viewJsp).isEqualTo(ControllerConstants.Views.Pages.StoreFinder.STORE_FINDER_DETAILS_PAGE);
    }

    @Test(expected = UnsupportedEncodingException.class)
    public void testGetGetStoreDetailsWithStoreNumberUException() throws CMSItemNotFoundException,
            UnsupportedEncodingException {
        storeNumber = "7001";
        given(storeLocatorFacade.getPointOfService(Integer.valueOf(7001))).willReturn(pointOfServiceData);
        given(storeBreadcrumbBuilder.getBreadcrumbs(pointOfServiceData)).willReturn(null);
        given(pointOfServiceData.getAddress()).willReturn(address);
        given(pointOfServiceData.getDescription()).willReturn("Geelong Store");
        given(pointOfServiceData.getUrl()).willReturn("url");
        given(pageTitleResolver.resolveStorePageTitle("Geelong")).willReturn("Geelong");
        given(pointOfServiceData.getName()).willReturn("Geelong");
        doThrow(new UnsupportedEncodingException()).when(spy).checkRequestUrl(request, response, "url");
        viewJsp = spy.storeFinderDetails(storeNumber, model, request, response);
    }

    @Test(expected = CMSItemNotFoundException.class)
    public void testGetGetStoreDetailsWithStoreNumberCMSException() throws CMSItemNotFoundException,
            UnsupportedEncodingException {
        storeNumber = "7001";
        given(storeLocatorFacade.getPointOfService(Integer.valueOf(7001))).willReturn(pointOfServiceData);
        given(storeBreadcrumbBuilder.getBreadcrumbs(pointOfServiceData)).willReturn(null);
        given(pointOfServiceData.getAddress()).willReturn(address);
        given(pointOfServiceData.getDescription()).willReturn("Geelong Store");
        given(pointOfServiceData.getUrl()).willReturn("url");
        given(pageTitleResolver.resolveStorePageTitle("Geelong")).willReturn("Geelong");
        given(pointOfServiceData.getName()).willReturn("Geelong");
        given(cmsPageService.getHomepage()).willReturn(homepage);
        given(cmsPageService.getLabelOrId(homepage)).willReturn("store");
        given(cmsPageService.getPageForLabelOrId("storefinder")).willThrow(new CMSItemNotFoundException("test"));
        doReturn("").when(spy).checkRequestUrl(request, response, "url");
        viewJsp = spy.storeFinderDetails(storeNumber, model, request, response);
    }
}

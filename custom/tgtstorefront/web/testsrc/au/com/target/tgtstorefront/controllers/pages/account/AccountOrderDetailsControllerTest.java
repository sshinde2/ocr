/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgtfacades.order.enums.TargetUpdateCardErrorType;
import au.com.target.tgtfacades.order.exception.TargetUpdateCardException;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtstorefront.breadcrumb.ResourceBreadcrumbBuilder;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.url.TargetUrlsMapping;
import au.com.target.tgtstorefront.util.PageTitleResolver;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@UnitTest
public class AccountOrderDetailsControllerTest {

    @Mock
    protected UserFacade userFacade;

    @Mock
    protected CustomerFacade customerFacade;

    @Mock
    protected TargetOrderFacade targetOrderFacade;

    @Mock
    protected CheckoutFacade checkoutFacade;

    @Mock
    protected ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

    @Mock
    private CommonI18NService commonI18NService;

    @Mock
    private I18NService i18NService;

    @Mock
    private PriceDataFactory priceDataFactory;

    @Mock
    private CurrencyModel currencyModel;

    @Mock
    private MessageSource messageSource;

    @Mock
    private CMSPageService cmsPageService;

    @Mock
    private PageTitleResolver pageTitleResolver;

    @Mock
    private ContentPageModel contentPageModel;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private Model mockedModel;

    @Mock
    private RedirectAttributes redirectModel;

    @Mock
    private RedirectAttributes redirectAttributes;

    @Mock
    private HttpServletRequest request;

    @Mock
    private TargetOrderData targetOrderData;

    @Mock
    private PriceData priceData;

    @Mock
    private PriceData priceDataOutstandingAmount;

    @Mock
    private TargetUrlsMapping targetUrlsMapping;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    private final String orderCode = "o1234";
    private final String requestURL = "https://localhost:9002";
    private final String requestURI = "https://localhost:9003";
    private final String contextPath = "/context-path";


    @InjectMocks
    private final AccountOrderDetailsController accountOrderDetailsController = new AccountOrderDetailsController();

    @Before
    public void setUp() throws CMSItemNotFoundException {
        MockitoAnnotations.initMocks(this);
        given(cmsPageService.getPageForLabelOrId(BDDMockito.anyString())).willReturn(
                contentPageModel);
        given(targetOrderData.getOutstandingAmount()).willReturn(priceDataOutstandingAmount);
        given(targetOrderFacade.getOrderDetailsForCode(anyString())).willReturn(targetOrderData);
        given(commonI18NService.getCurrentCurrency()).willReturn(currencyModel);
        given(currencyModel.getIsocode()).willReturn("$");
    }

    @Test
    public void testOrderDetailEmptyCode() throws CMSItemNotFoundException {
        given(targetOrderFacade.getOrderDetailsForCodeUnderCurrentCustomer(anyString()))
                .willThrow(
                        new UnknownIdentifierException("no order"));
        assertThat(accountOrderDetailsController.order(StringUtils.EMPTY, mockedModel, redirectModel))
                .isEqualTo(ControllerConstants.Redirection.MY_ACCOUNT_ORDERS);
    }

    @Ignore
    @Test
    public void testOrderDetail() throws CMSItemNotFoundException {
        given(targetOrderFacade.getOrderDetailsForCodeUnderCurrentCustomer(anyString()))
                .willReturn(targetOrderData);
        assertThat(accountOrderDetailsController.order(StringUtils.EMPTY, mockedModel, redirectModel))
                .isEqualTo(ControllerConstants.Views.Pages.Account.ACCOUNT_ORDER_PAGE);
    }

    @Test
    public void testUpdateCard() throws CMSItemNotFoundException, TargetUpdateCardException {

        final String cancelUrl = requestURL + ControllerConstants.MY_ACCOUNT + ControllerConstants.MY_ACCOUNT_ORDER
                + orderCode;
        final String returnUrl = requestURL + ControllerConstants.MY_ACCOUNT_IPG + orderCode
                + ControllerConstants.MY_ACCOUNT_ORDER_UPDATE_CARD_IPG;

        given(request.getRequestURL()).willReturn(new StringBuffer(requestURL));
        given(request.getRequestURI()).willReturn(requestURI);
        given(request.getContextPath()).willReturn(contextPath);
        given(targetUrlsMapping.getMyAccountOrders()).willReturn("my-account");
        given(targetUrlsMapping.getMyAccountOrderDetails()).willReturn("/order-details/");
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPreOrderUpdateCardEnabled();

        final String view = accountOrderDetailsController.updateCard(orderCode, mockedModel, redirectAttributes,
                request);
        verify(targetOrderFacade).createIpgUrl(orderCode, cancelUrl, returnUrl);
        assertThat(view).isEqualTo(ControllerConstants.Views.Pages.Account.ACCOUNT_ORDER_UPDATE_CARD_PAGE);

    }

    @Test
    public void testUpdateCardWithTargetUpdateCardExceptionInvalidOrder()
            throws CMSItemNotFoundException, TargetUpdateCardException {

        final String cancelUrl = requestURL + ControllerConstants.MY_ACCOUNT + ControllerConstants.MY_ACCOUNT_ORDER
                + orderCode;
        final String returnUrl = requestURL + ControllerConstants.MY_ACCOUNT_IPG + orderCode
                + ControllerConstants.MY_ACCOUNT_ORDER_UPDATE_CARD_IPG;

        given(request.getRequestURL()).willReturn(new StringBuffer(requestURL));
        given(request.getRequestURI()).willReturn(requestURI);
        given(request.getContextPath()).willReturn(contextPath);
        given(targetUrlsMapping.getMyAccountOrders()).willReturn("my-account");
        given(targetUrlsMapping.getMyAccountOrderDetails()).willReturn("/order-details/");
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPreOrderUpdateCardEnabled();

        given(targetOrderFacade.createIpgUrl(orderCode, cancelUrl, returnUrl))
                .willThrow(new TargetUpdateCardException("some error", TargetUpdateCardErrorType.INVALID_ORDER));

        final String view = accountOrderDetailsController.updateCard(orderCode, mockedModel, redirectAttributes,
                request);
        assertThat(view).isEqualTo(ControllerConstants.Redirection.MY_ACCOUNT_ORDERS);
    }

    @Test
    public void testUpdateCardWithAdapterException() throws CMSItemNotFoundException, TargetUpdateCardException {

        final String cancelUrl = requestURL + ControllerConstants.MY_ACCOUNT + ControllerConstants.MY_ACCOUNT_ORDER
                + orderCode;
        final String returnUrl = requestURL + ControllerConstants.MY_ACCOUNT_IPG + orderCode
                + ControllerConstants.MY_ACCOUNT_ORDER_UPDATE_CARD_IPG;

        given(request.getRequestURL()).willReturn(new StringBuffer(requestURL));
        given(request.getRequestURI()).willReturn(requestURI);
        given(request.getContextPath()).willReturn(contextPath);
        given(targetUrlsMapping.getMyAccountOrders()).willReturn("my-account");
        given(targetUrlsMapping.getMyAccountOrderDetails()).willReturn("/order-details/");
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPreOrderUpdateCardEnabled();

        given(targetOrderFacade.createIpgUrl(orderCode, cancelUrl, returnUrl))
                .willThrow(new AdapterException("some error"));
        final String view = accountOrderDetailsController.updateCard(orderCode, mockedModel, redirectAttributes,
                request);
        assertThat(view).isEqualTo(ControllerConstants.Redirection.MY_ACCOUNT_ORDER_DETAILS + orderCode);
    }

    @Test
    public void testUpdateCardWithTargetUpdateCardInvalidUpdateCard()
            throws CMSItemNotFoundException, TargetUpdateCardException {

        final String cancelUrl = requestURL + ControllerConstants.MY_ACCOUNT + ControllerConstants.MY_ACCOUNT_ORDER
                + orderCode;
        final String returnUrl = requestURL + ControllerConstants.MY_ACCOUNT_IPG + orderCode
                + ControllerConstants.MY_ACCOUNT_ORDER_UPDATE_CARD_IPG;

        given(request.getRequestURL()).willReturn(new StringBuffer(requestURL));
        given(request.getRequestURI()).willReturn(requestURI);
        given(request.getContextPath()).willReturn(contextPath);
        given(targetUrlsMapping.getMyAccountOrders()).willReturn("my-account");
        given(targetUrlsMapping.getMyAccountOrderDetails()).willReturn("/order-details/");
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isPreOrderUpdateCardEnabled();

        given(targetOrderFacade.createIpgUrl(orderCode, cancelUrl, returnUrl))
                .willThrow(new TargetUpdateCardException("some error", TargetUpdateCardErrorType.INVALID_ORDER_STATE));
        final String view = accountOrderDetailsController.updateCard(orderCode, mockedModel, redirectAttributes,
                request);
        assertThat(view).isEqualTo(ControllerConstants.Redirection.MY_ACCOUNT_ORDER_DETAILS + orderCode);
    }

    @Test
    public void testUpdateCardWithFeatureSwitchOff() throws CMSItemNotFoundException {
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isPreOrderUpdateCardEnabled();
        final String view = accountOrderDetailsController.updateCard(orderCode, mockedModel, redirectAttributes,
                request);
        assertThat(view).isEqualTo(ControllerConstants.Redirection.ERROR_404);
    }
}

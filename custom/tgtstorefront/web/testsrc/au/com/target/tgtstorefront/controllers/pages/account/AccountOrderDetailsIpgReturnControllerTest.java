/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.account;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author gsing236
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AccountOrderDetailsIpgReturnControllerTest {

    @InjectMocks
    private final AccountOrderDetailsIpgReturnController controller = new AccountOrderDetailsIpgReturnController();
    @Mock
    private Model model;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private TargetOrderFacade targetOrderFacade;

    @Before
    public void setUp() {
        when(request.getSession()).thenReturn(session);
    }

    @Test
    public void testUpdateCardSuccessful() {
        final String orderId = "OrderId";
        final String token = "token";
        willReturn(Boolean.TRUE).given(targetOrderFacade).isUpdateCreditCardDetailsSuccessful(orderId, token);
        final String redirectPage = controller.fromIpg(token, null, orderId, model);
        verify(model).addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_COMPLETE);
        assertThat(redirectPage).isEqualTo(ControllerConstants.Views.Pages.Checkout.CHECKOUT_IPG_RETURN_PAGE);
    }

    @Test
    public void testReceiveAbortFromIpg() {
        final String orderId = "OrderId";
        final String token = "token";
        final String redirectPage = controller.fromIpg(token, ControllerConstants.Ipg.IPG_ABORT_ACTION, orderId, model);
        verify(model).addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_ABORT);
        assertThat(redirectPage).isEqualTo(ControllerConstants.Views.Pages.Checkout.CHECKOUT_IPG_RETURN_PAGE);
        verifyZeroInteractions(targetOrderFacade);
    }

    @Test
    public void testUpdateCardNotSuccessful() {
        final String orderId = "OrderId";
        final String token = "token";
        willReturn(Boolean.FALSE).given(targetOrderFacade).isUpdateCreditCardDetailsSuccessful(orderId, token);
        final String redirectPage = controller.fromIpg(token, null, orderId, model);
        verify(model).addAttribute("postMessageActionType", ControllerConstants.Ipg.IPG_PAYMENT_TYPE_CANCEL);
        assertThat(redirectPage).isEqualTo(ControllerConstants.Views.Pages.Checkout.CHECKOUT_IPG_RETURN_PAGE);
    }
}

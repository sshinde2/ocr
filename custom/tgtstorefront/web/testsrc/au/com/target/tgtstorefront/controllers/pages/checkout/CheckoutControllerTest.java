/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.prepopulatecart.TargetPrepopulateCheckoutCartFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutControllerTest {

    @InjectMocks
    @Spy
    private final CheckoutController checkoutController = new CheckoutController();
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;
    @Mock
    private SalesApplicationFacade salesApplicationFacade;
    @Mock
    private TargetCheckoutFacade checkoutFacade;
    @Mock
    private HttpServletRequest request;
    @Mock
    private Model model;
    @Mock
    private RedirectAttributes redirectAttributes;
    @Mock
    private TargetCartData cartData;
    @Mock
    private UserService userService;
    @Mock
    private SessionService sessionService;
    @Mock
    private TargetPrepopulateCheckoutCartFacade targetPrepopulateCheckoutCartFacade;

    @Before
    public void setUp() {
        when(Boolean.valueOf(salesApplicationFacade.isKioskApplication())).thenReturn(Boolean.FALSE);
        when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
    }

    @Test
    public void itShouldGoToSPCIfSPCIsTurnedOnAndIsNotKiosk() {
        doReturn(StringUtils.EMPTY).when(checkoutController).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY, cartData);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isSpcLoginAndCheckout();
        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(any(UserModel.class));
        final String redirectUrl = checkoutController.getCheckoutRedirectUrl(request, model, redirectAttributes);
        verify(checkoutFacade).hasIncompleteDeliveryDetails();
        verify(targetPrepopulateCheckoutCartFacade).isCartPrePopulatedForCheckout();
        Assertions.assertThat(redirectUrl).isEqualTo(ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN);
    }

    @Test
    public void itShouldGoToYourAddressForMobileWithoutSPCInSession() {
        doReturn(StringUtils.EMPTY).when(checkoutController).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY, cartData);
        when(sessionService.getAttribute("spc")).thenReturn(null);
        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(any(UserModel.class));
        final String redirectUrl = checkoutController.getCheckoutRedirectUrl(request, model, redirectAttributes);
        verify(checkoutFacade).hasIncompleteDeliveryDetails();
        Assertions.assertThat(redirectUrl).isEqualTo(ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS);
    }

    @Test
    public void itShouldGoToYourAddressForMobileIfSPCIsDisabled() {
        doReturn(StringUtils.EMPTY).when(checkoutController).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY, cartData);
        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(any(UserModel.class));
        final String redirectUrl = checkoutController.getCheckoutRedirectUrl(request, model, redirectAttributes);
        verify(checkoutFacade).hasIncompleteDeliveryDetails();
        Assertions.assertThat(redirectUrl).isEqualTo(ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS);
    }

    @Test
    public void itShouldGoToYourAddressForKiosk() {
        doReturn(StringUtils.EMPTY).when(checkoutController).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY, cartData);
        when(sessionService.getAttribute("spc")).thenReturn("on");
        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(any(UserModel.class));
        when(Boolean.valueOf(salesApplicationFacade.isKioskApplication())).thenReturn(Boolean.TRUE);
        final String redirectUrl = checkoutController.getCheckoutRedirectUrl(request, model, redirectAttributes);
        verify(checkoutFacade).hasIncompleteDeliveryDetails();
        Assertions.assertThat(redirectUrl).isEqualTo(ControllerConstants.Redirection.CHECKOUT_YOUR_ADDRESS);
    }

    @Test
    public void itShouldGoToSPCForGuestUserOnMobileWithSPCEnabled() {
        doReturn(StringUtils.EMPTY).when(checkoutController).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY, cartData);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isSpcLoginAndCheckout();
        doReturn(Boolean.TRUE).when(userService).isAnonymousUser(any(UserModel.class));
        doReturn(Boolean.TRUE).when(checkoutFacade).hasIncompleteDeliveryDetails();
        doReturn(Boolean.TRUE).when(sessionService).getAttribute(WebConstants.ANONYMOUS_CHECKOUT);
        final String redirectUrl = checkoutController.getCheckoutRedirectUrl(request, model, redirectAttributes);
        verify(checkoutFacade).hasIncompleteDeliveryDetails();
        verify(checkoutFacade).removeDeliveryInfo();
        Assertions.assertThat(redirectUrl).isEqualTo(ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN);
    }

    @Test
    public void itShouldGoToLoginForAnonymousUser() {
        doReturn(StringUtils.EMPTY).when(checkoutController).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY, cartData);
        doReturn(Boolean.TRUE).when(userService).isAnonymousUser(any(UserModel.class));
        doReturn(null).when(sessionService).getAttribute(WebConstants.ANONYMOUS_CHECKOUT);
        final String redirectUrl = checkoutController.getCheckoutRedirectUrl(request, model, redirectAttributes);
        Assertions.assertThat(redirectUrl).isEqualTo(ControllerConstants.Redirection.CHECKOUT_LOGIN);
    }

    @Test
    public void itShouldGoToSPCLoginAndCheckoutPageWithFeatureSwitchOn() {
        doReturn(StringUtils.EMPTY).when(checkoutController).getRedirectionAndCheckSOH(model, redirectAttributes,
                StringUtils.EMPTY, cartData);
        when(sessionService.getAttribute("spc")).thenReturn("on");
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isSpcLoginAndCheckout();
        doReturn(Boolean.FALSE).when(userService).isAnonymousUser(any(UserModel.class));
        final String redirectUrl = checkoutController.getCheckoutRedirectUrl(request, model, redirectAttributes);
        verify(checkoutFacade).hasIncompleteDeliveryDetails();
        verify(targetPrepopulateCheckoutCartFacade).isCartPrePopulatedForCheckout();
        Assertions.assertThat(redirectUrl).isEqualTo(ControllerConstants.Redirection.SINGLE_PAGE_CHECKOUT_AND_LOGIN);
    }
}

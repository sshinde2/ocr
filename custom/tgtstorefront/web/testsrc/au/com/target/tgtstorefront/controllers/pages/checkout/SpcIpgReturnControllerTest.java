/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.checkout;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.ui.Model;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FlashMap;

import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.TargetPlaceOrderFacade;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SpcIpgReturnControllerTest {

    @InjectMocks
    private final SpcIpgReturnController controller = new SpcIpgReturnController();
    @Mock
    private Model model;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private TargetCheckoutFacade checkoutFacade;
    @Mock
    private TargetPlaceOrderFacade targetPlaceOrderFacade;

    @Before
    public void setUp() {
        when(request.getSession()).thenReturn(session);
    }

    @Test
    public void itShouldRedirectToSpcIpgReturnPage() {
        final String redirectPage = controller.fromIpg("token", null, model, request);
        verify(session).setAttribute("token", "token");
        verify(model).addAttribute("postMessageActionType", "IPG_PAYMENT_COMPLETE");
        assertEquals(ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE, redirectPage);
    }

    @Test
    public void itShouldSetAbortFlagIfIpgReturnsAborted() {
        final String redirectPage = controller.fromIpg("token", "ipg_abort_session", model, request);
        verify(session).setAttribute("token", "token");
        verify(model).addAttribute("postMessageActionType", "IPG_PAYMENT_ABORT");
        assertEquals(ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE, redirectPage);
    }

    @Test
    public void itShouldSetCancelFlagIfIpgReturnsSuccessWithNullTxResult() {
        when(targetPlaceOrderFacade.getQueryTransactionDetailResults()).thenReturn(null);
        final String redirectPage = controller.fromIpg("token", null, model, request);
        verify(session).setAttribute("token", "token");
        verify(model).addAttribute("postMessageActionType", "IPG_PAYMENT_COMPLETE");
        assertEquals(ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE, redirectPage);
    }

    @Test
    public void itShouldSetCancelFlagIfIpgReturnsSuccessWithCancelledPayment() {
        final TargetQueryTransactionDetailsResult result = mock(TargetQueryTransactionDetailsResult.class);
        when(targetPlaceOrderFacade.getQueryTransactionDetailResults()).thenReturn(result);
        when(Boolean.valueOf(result.isCancel())).thenReturn(Boolean.TRUE);
        final String redirectPage = controller.fromIpg("token", null, model, request);
        verify(session).setAttribute("token", "token");
        verify(model).addAttribute("postMessageActionType", "IPG_PAYMENT_CANCEL");
        assertEquals(ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE, redirectPage);
    }

    @Test
    public void handleExceptions() {
        final HttpServletRequest mockRequest = new MockHttpServletRequest();
        mockRequest.setAttribute(DispatcherServlet.OUTPUT_FLASH_MAP_ATTRIBUTE, new FlashMap());
        final String returnURL = controller.handleException(new Exception(), mockRequest);
        assertEquals(ControllerConstants.Views.Pages.Checkout.CHECKOUT_SPC_IPG_RETURN_PAGE, returnURL);

    }
}
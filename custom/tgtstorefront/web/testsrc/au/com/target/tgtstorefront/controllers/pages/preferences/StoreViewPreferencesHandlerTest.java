/**
 * 
 */
package au.com.target.tgtstorefront.controllers.pages.preferences;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

//import au.com.target.tgtsearch.service.impl.TargetCommerceFacetSearchConfigServiceImpl;
import au.com.target.tgtstorefront.controllers.pages.data.ViewPreferencesInitialData;


/**
 * @author quan.le
 * 
 */
public class StoreViewPreferencesHandlerTest {

    @InjectMocks
    private final StoreViewPreferencesHandler storeViewPreferencesHandler = new StoreViewPreferencesHandler();
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private ConfigurationService mockConfigurationService;

    private ViewPreferencesInitialData viewData;


    @SuppressWarnings("boxing")
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        storeViewPreferencesHandler.setItemPerPageDefaultValue("30");
        storeViewPreferencesHandler.setViewAsDefaultValue("grid");
        request = Mockito.mock(HttpServletRequest.class);
        response = Mockito.mock(HttpServletResponse.class);
    }

    @Test
    public void testViewAsWithDefaultValueNotSet() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String viewAsResult = storeViewPreferencesHandler.getViewAsPreference(response, viewData, "");
        Assert.assertEquals("grid", viewAsResult);
    }

    @Test
    public void testViewAsWithInalidValue() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String viewAsResult = storeViewPreferencesHandler.getViewAsPreference(response, viewData, "fegae");
        Assert.assertEquals("grid", viewAsResult);
    }

    @Test
    public void testViewAsWithGridValue() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String viewAsResult = storeViewPreferencesHandler.getViewAsPreference(response, viewData, "grid");
        Assert.assertEquals("grid", viewAsResult);
    }

    @Ignore
    @Test
    public void testViewAsWithListValue() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String viewAsResult = storeViewPreferencesHandler.getViewAsPreference(response, viewData, "list");
        Assert.assertEquals("list", viewAsResult);
    }

    @Test
    public void testViewAsWithGridValueFromRootCategoryWithQueryString() {
        viewData = new ViewPreferencesInitialData(request, "W93743", ":latest:category:W127748");
        final String viewAsResult = storeViewPreferencesHandler.getViewAsPreference(response, viewData, "grid");
        Assert.assertEquals("grid", viewAsResult);
    }

    @Ignore
    @Test
    public void testViewAsWithListValueFromRootCategoryWithQueryString() {
        viewData = new ViewPreferencesInitialData(request, "W93743", ":latest:category:W127748");
        final String viewAsResult = storeViewPreferencesHandler.getViewAsPreference(response, viewData, "list");
        Assert.assertEquals("list", viewAsResult);
    }

    @Test
    public void testSortCodeDefaultFromRootCategory() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String sortCodeResult = storeViewPreferencesHandler.getSortCodePreference(response, viewData, "");
        Assert.assertEquals("", sortCodeResult);
    }

    @Test
    public void testSortCodeChangeBrandFromRootCategory() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String sortCodeResult = storeViewPreferencesHandler.getSortCodePreference(response, viewData, "brand");
        Assert.assertEquals("brand", sortCodeResult);
    }

    @Test
    public void testSortCodeChangePriceASCFromRootCategory() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String sortCodeResult = storeViewPreferencesHandler
                .getSortCodePreference(response, viewData, "price-asc");
        Assert.assertEquals("price-asc", sortCodeResult);
    }

    @Test
    public void testSortCodeChangePriceDESCFromRootCategory() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String sortCodeResult = storeViewPreferencesHandler.getSortCodePreference(response, viewData,
                "price-desc");
        Assert.assertEquals("price-desc", sortCodeResult);
    }

    @Test
    public void testSortCodeChangeNameASCFromRootCategory() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String sortCodeResult = storeViewPreferencesHandler.getSortCodePreference(response, viewData, "name-asc");
        Assert.assertEquals("name-asc", sortCodeResult);
    }


    @Test
    public void testSortCodeChangeNameDESCFromRootCategory() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String sortCodeResult = storeViewPreferencesHandler
                .getSortCodePreference(response, viewData, "name-desc");
        Assert.assertEquals("name-desc", sortCodeResult);
    }

    @Test
    public void testSortCodeChangeLatestFromRootCategory() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String sortCodeResult = storeViewPreferencesHandler.getSortCodePreference(response, viewData, "latest");
        Assert.assertEquals("latest", sortCodeResult);
    }

    @Test
    public void testSortCodeUnChangeLatestFromRootCategory() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final String sortCodeResult = storeViewPreferencesHandler.getSortCodePreference(response, viewData, "latest");
        Assert.assertEquals("latest", sortCodeResult);
    }

    @Test
    public void testItemsPerPageDefault() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final int itemsPerPageResult = storeViewPreferencesHandler.getItemPerPagePreference(response, viewData, 0);
        Assert.assertEquals(30, itemsPerPageResult);
    }

    @Test
    public void testItemsPerPageWithInvalidValue() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final int itemsPerPageResult = storeViewPreferencesHandler.getItemPerPagePreference(response, viewData, 10);
        Assert.assertEquals(30, itemsPerPageResult);
    }

    @Test
    public void testItemsPerPageChange30() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final int itemsPerPageResult = storeViewPreferencesHandler.getItemPerPagePreference(response, viewData, 30);
        Assert.assertEquals(30, itemsPerPageResult);
    }


    @Test
    public void testItemsPerPageChange90() {
        viewData = new ViewPreferencesInitialData(request, "W93743", "");
        final int itemsPerPageResult = storeViewPreferencesHandler.getItemPerPagePreference(response, viewData, 90);
        Assert.assertEquals(90, itemsPerPageResult);
    }


    @Test
    public void testGenerateCookie() {
        final Cookie cookie = storeViewPreferencesHandler.generateCookie("pref_v", "AX100TYOP");
        Assert.assertTrue(cookie.getSecure());
        Assert.assertEquals(cookie.getMaxAge(), 0);
        Assert.assertEquals(cookie.getName(), "pref_v");
        Assert.assertEquals(cookie.getValue(), "AX100TYOP");
    }


    @Test
    public void testGenerateCookieWithEmptyValue() {
        final Cookie cookie = storeViewPreferencesHandler.generateCookie("pref_v", "");
        Assert.assertTrue(cookie.getSecure());
        Assert.assertEquals(cookie.getMaxAge(), 0);
        Assert.assertEquals(cookie.getName(), "pref_v");
        Assert.assertEquals(cookie.getValue(), "");
    }

}

package au.com.target.tgtstorefront.controllers.services;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import au.com.target.tgtfacades.customer.TargetCustomerFacade;
import au.com.target.tgtfacades.customer.TargetCustomerSubscriptionFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade;
import au.com.target.tgtfacades.prepopulatecart.TargetPrepopulateCheckoutCartFacade;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtstorefront.checkout.login.request.dto.CheckoutLoginEmailAddressDTO;
import au.com.target.tgtstorefront.checkout.login.request.dto.CheckoutLoginRegistrationDTO;
import au.com.target.tgtstorefront.checkout.login.request.dto.CheckoutLoginResetPasswordDTO;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.cookie.TargetCookieStrategy;
import au.com.target.tgtstorefront.security.AutoLoginStrategy;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CheckoutLoginServiceControllerTest {
    @Mock
    private HttpSession mockHttpSession;

    @Mock
    private Response mockResponse;

    @Mock
    private TargetCheckoutLoginResponseFacade mockTargetCheckoutLoginResponseFacade;

    @Mock
    private WebServiceExceptionHelper mockWebServiceExceptionHelper;

    @Mock
    private SessionService mockSessionService;

    @Mock
    private SalesApplicationFacade mockSalesApplicationFacade;

    @Mock
    private TargetPrepopulateCheckoutCartFacade targetPrepopulateCheckoutCartFacade;

    @InjectMocks
    private final CheckoutLoginServiceController checkoutLoginServiceController = new CheckoutLoginServiceController();

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private AutoLoginStrategy autoLoginStrategy;

    @Mock
    private TargetCustomerSubscriptionFacade targetCustomerSubscriptionFacade;

    @Mock
    private TargetCookieStrategy guidCookieStrategy;

    @Mock
    private TargetCustomerFacade<?> targetCustomerFacade;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Before
    public void setUp() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isSpcLoginAndCheckout();
        doReturn(Boolean.FALSE).when(mockSalesApplicationFacade).isKioskApplication();
    }

    @Test
    public void testLoginRequired() {
        given(mockTargetCheckoutLoginResponseFacade.createLoginRequiredResponse()).willReturn(mockResponse);

        final Response response = checkoutLoginServiceController.loginRequired();

        assertThat(response).isEqualTo(mockResponse);
    }

    @Test
    public void testLoginSuccess() throws Exception {
        given(mockTargetCheckoutLoginResponseFacade
                .createLoginSuccessResponse(ControllerConstants.SINGLE_PAGE_CHECKOUT))
                        .willReturn(mockResponse);

        final Response response = checkoutLoginServiceController.loginSuccess();
        verify(targetPrepopulateCheckoutCartFacade).isCartPrePopulatedForCheckout();
        assertThat(response).isEqualTo(mockResponse);
    }

    @Test
    public void testLoginFailureWithNoException() throws Exception {
        given(mockHttpSession.getAttribute(WebConstants.SPRING_SECURITY_LAST_EXCEPTION))
                .willReturn(null);
        given(mockTargetCheckoutLoginResponseFacade.createLoginFailedResponse(null))
                .willReturn(mockResponse);

        final Response response = checkoutLoginServiceController.loginFailure(mockHttpSession);

        assertThat(response).isEqualTo(mockResponse);
        verify(mockHttpSession).removeAttribute(WebConstants.SPRING_SECURITY_LAST_EXCEPTION);
    }

    @Test
    public void testLoginFailure() throws Exception {
        given(mockHttpSession.getAttribute(WebConstants.SPRING_SECURITY_LAST_EXCEPTION))
                .willReturn(new BadCredentialsException("Bad credentials!"));
        given(mockTargetCheckoutLoginResponseFacade.createLoginFailedResponse(BadCredentialsException.class))
                .willReturn(mockResponse);

        final Response response = checkoutLoginServiceController.loginFailure(mockHttpSession);

        assertThat(response).isEqualTo(mockResponse);
        verify(mockHttpSession).removeAttribute(WebConstants.SPRING_SECURITY_LAST_EXCEPTION);
    }

    @Test
    public void testRegistered() throws Exception {
        final CheckoutLoginEmailAddressDTO dto = new CheckoutLoginEmailAddressDTO();
        dto.setEmailAddress("therealdeal@target.com.au");

        given(mockTargetCheckoutLoginResponseFacade.isCustomerRegistered("therealdeal@target.com.au"))
                .willReturn(mockResponse);

        final Response response = checkoutLoginServiceController.registered(dto);

        assertThat(response).isEqualTo(mockResponse);
    }

    @Test
    public void testHandleValidationErrorWithMethodArgumentNotValidException() {
        final MethodArgumentNotValidException mockException = Mockito.mock(MethodArgumentNotValidException.class);
        given(mockWebServiceExceptionHelper.handleException(httpServletRequest, mockException))
                .willReturn(mockResponse);

        final Response response = checkoutLoginServiceController.handleException(httpServletRequest,
                mockException);

        assertThat(response).isEqualTo(mockResponse);
    }

    @Test
    public void testRegisterWithCartPrepopulatedAndAccountCreationSuccess() {
        final CheckoutLoginRegistrationDTO dto = new CheckoutLoginRegistrationDTO();
        final Response response = Mockito.mock(Response.class);
        given(Boolean.valueOf(response.isSuccess())).willReturn(Boolean.TRUE);
        given(mockTargetCheckoutLoginResponseFacade.processCustomerRegistrationRequest(any(TargetRegisterData.class),
                anyString())).willReturn(response);
        doNothing().when(autoLoginStrategy).login(anyString(), anyString(), any(HttpServletRequest.class),
                any(HttpServletResponse.class));
        checkoutLoginServiceController.register(dto, httpServletRequest, httpServletResponse);
        verify(targetPrepopulateCheckoutCartFacade).isCartPrePopulatedForCheckout();
        verify(autoLoginStrategy).login(anyString(), anyString(), any(HttpServletRequest.class),
                any(HttpServletResponse.class));
        verify(targetCustomerSubscriptionFacade).performCustomerSubscription(
                any(TargetCustomerSubscriptionRequestDto.class));
    }

    @Test
    public void testRegisterWithCartPrepopulatedAndAccountCreationNotSuccess() {
        final CheckoutLoginRegistrationDTO dto = new CheckoutLoginRegistrationDTO();
        final Response response = Mockito.mock(Response.class);
        given(Boolean.valueOf(response.isSuccess())).willReturn(Boolean.FALSE);
        given(mockTargetCheckoutLoginResponseFacade.processCustomerRegistrationRequest(any(TargetRegisterData.class),
                anyString())).willReturn(response);
        doNothing().when(autoLoginStrategy).login(anyString(), anyString(), any(HttpServletRequest.class),
                any(HttpServletResponse.class));
        checkoutLoginServiceController.register(dto, httpServletRequest, httpServletResponse);
        verify(targetPrepopulateCheckoutCartFacade).isCartPrePopulatedForCheckout();
        verify(autoLoginStrategy, Mockito.times(0)).login(anyString(), anyString(), any(HttpServletRequest.class),
                any(HttpServletResponse.class));
        verify(targetCustomerSubscriptionFacade, Mockito.times(0)).performCustomerSubscription(
                any(TargetCustomerSubscriptionRequestDto.class));
    }

    @Test
    public void testGuestWithSuccess() {
        final CheckoutLoginEmailAddressDTO customerUsername = new CheckoutLoginRegistrationDTO();
        final Response response = Mockito.mock(Response.class);
        given(Boolean.valueOf(response.isSuccess())).willReturn(Boolean.TRUE);
        given(mockTargetCheckoutLoginResponseFacade.processGuestCheckoutRequest(anyString(), anyString())).willReturn(
                response);
        doNothing().when(guidCookieStrategy).setCookie(httpServletRequest, httpServletResponse);
        checkoutLoginServiceController.guest(customerUsername, httpServletRequest, httpServletResponse);
        verify(guidCookieStrategy).setCookie(httpServletRequest, httpServletResponse);
        verify(targetPrepopulateCheckoutCartFacade, Mockito.times(0)).isCartPrePopulatedForCheckout();
    }

    @Test
    public void testGuestWithFailure() {
        final CheckoutLoginEmailAddressDTO customerUsername = new CheckoutLoginRegistrationDTO();
        final Response response = Mockito.mock(Response.class);
        given(Boolean.valueOf(response.isSuccess())).willReturn(Boolean.FALSE);
        given(mockTargetCheckoutLoginResponseFacade.processGuestCheckoutRequest(anyString(), anyString())).willReturn(
                response);
        doNothing().when(guidCookieStrategy).setCookie(httpServletRequest, httpServletResponse);
        checkoutLoginServiceController.guest(customerUsername, httpServletRequest, httpServletResponse);
        verify(guidCookieStrategy, Mockito.times(0)).setCookie(httpServletRequest, httpServletResponse);
        verify(targetPrepopulateCheckoutCartFacade, Mockito.times(0)).isCartPrePopulatedForCheckout();
    }

    @Test
    public void testResetPasswordWithRegisteredUser() {
        final CheckoutLoginEmailAddressDTO dto = new CheckoutLoginEmailAddressDTO();
        dto.setBlankRedirectPage(false);
        dto.setEmailAddress("registered@target.com.au");
        final Response response = checkoutLoginServiceController.resetPassword(dto);
        assertThat(response.isSuccess()).isTrue();
    }

    @Test
    public void testResetPasswordWithUnregisteredUser() {
        final CheckoutLoginEmailAddressDTO dto = new CheckoutLoginEmailAddressDTO();
        dto.setBlankRedirectPage(false);
        dto.setEmailAddress("unregistered@target.com.au");
        BDDMockito.doThrow(new UnknownIdentifierException("")).when(targetCustomerFacade)
                .forgottenPassword("unregistered@target.com.au", "/spc/order");
        final Response response = checkoutLoginServiceController.resetPassword(dto);
        assertThat(response.isSuccess()).isTrue();
        verify(targetCustomerFacade).forgottenPassword("unregistered@target.com.au", "/spc/order");
    }

    @Test
    public void testResetPasswordDoesNotSetRefererWhenTrue() {
        final CheckoutLoginEmailAddressDTO dto = new CheckoutLoginEmailAddressDTO();
        dto.setBlankRedirectPage(true);
        dto.setEmailAddress("registered@target.com.au");
        final Response response = checkoutLoginServiceController.resetPassword(dto);
        assertThat(response.isSuccess()).isTrue();
        verify(targetCustomerFacade).forgottenPassword("registered@target.com.au", "");
    }

    @Test
    public void testSetPasswordWithFailedResponse() {
        final String password = "password";
        final String token = "token";
        final CheckoutLoginResetPasswordDTO dto = Mockito.mock(CheckoutLoginResetPasswordDTO.class);
        given(dto.getPassword()).willReturn(password);
        given(dto.getToken()).willReturn(token);
        final Response response = new Response(false);
        given(mockTargetCheckoutLoginResponseFacade.processSetPasswordByToken(password, token)).willReturn(
                response);
        final Response result = checkoutLoginServiceController.setPassword(dto, httpServletRequest,
                httpServletResponse);
        assertThat(result).isEqualTo(response);
    }

    @Test
    public void testSetPasswordWithSuccessfulResponse() {
        final String password = "password";
        final String token = "token";
        final String uid = "test@test.com";
        final CheckoutLoginResetPasswordDTO dto = Mockito.mock(CheckoutLoginResetPasswordDTO.class);
        given(dto.getPassword()).willReturn(password);
        given(dto.getToken()).willReturn(token);
        final Response response = new Response(true);
        given(mockTargetCheckoutLoginResponseFacade.processSetPasswordByToken(password, token)).willReturn(
                response);
        given(targetCustomerFacade.getUidForPasswordResetToken(token)).willReturn(uid);
        final Response result = checkoutLoginServiceController.setPassword(dto, httpServletRequest,
                httpServletResponse);
        assertThat(result).isEqualTo(response);
        verify(targetCustomerFacade).getUidForPasswordResetToken(token);
        verify(autoLoginStrategy).login(uid, password, httpServletRequest, httpServletResponse);
        verify(targetPrepopulateCheckoutCartFacade).isCartPrePopulatedForCheckout();
    }
}

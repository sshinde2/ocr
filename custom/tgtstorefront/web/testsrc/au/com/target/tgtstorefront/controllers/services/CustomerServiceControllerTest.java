/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletResponse;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.StoreResponseData;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.security.cookie.EnhancedCookieGenerator;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceControllerTest {

    @InjectMocks
    private final CustomerServiceController customerServiceController = new CustomerServiceController();

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private WebServiceExceptionHelper webServiceExceptionHelper;

    @Mock
    private EnhancedCookieGenerator mockEnhancedCookieGenerator;

    private final MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

    @Test
    public void testSetPreferredStoreValid() {
        final Integer validStoreNumber = Integer.valueOf(7001);

        final TargetPointOfServiceData storeData = new TargetPointOfServiceData();
        storeData.setStoreNumber(validStoreNumber);

        given(targetStoreLocatorFacade.getPointOfService(validStoreNumber)).willReturn(storeData);

        final Response response = customerServiceController.setPreferredStore(validStoreNumber,
                mockHttpServletResponse);

        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(StoreResponseData.class);

        final StoreResponseData responseData = (StoreResponseData)response.getData();
        assertThat(responseData.getStoreNumber()).isEqualTo(validStoreNumber);
        assertThat(responseData.getStoreDetails()).isEqualTo(storeData);
        assertThat(responseData.getError()).isNull();

        verify(mockEnhancedCookieGenerator).addCookie(mockHttpServletResponse, validStoreNumber.toString());
    }

    @Test
    public void testSetPreferredStoreInvalid() {
        final Integer validStoreNumber = Integer.valueOf(7001);
        given(targetStoreLocatorFacade.getPointOfService(validStoreNumber)).willReturn(null);

        final Response response = customerServiceController.setPreferredStore(validStoreNumber,
                mockHttpServletResponse);

        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(StoreResponseData.class);

        final StoreResponseData responseData = (StoreResponseData)response.getData();
        assertThat(responseData.getStoreNumber()).isEqualTo(validStoreNumber);
        assertThat(responseData.getStoreDetails()).isNull();
        assertThat(responseData.getError().getCode()).isEqualTo("INVALID_STORE_NUMBER");

        verifyZeroInteractions(mockEnhancedCookieGenerator);
    }

    @Test
    public void handleException() {
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final Exception exception = mock(Exception.class);
        final Response errorResponse = mock(Response.class);

        given(webServiceExceptionHelper.handleException(request, exception)).willReturn(errorResponse);

        final Response response = customerServiceController.handleException(request, exception);

        assertThat(response).isEqualTo(errorResponse);
    }

}

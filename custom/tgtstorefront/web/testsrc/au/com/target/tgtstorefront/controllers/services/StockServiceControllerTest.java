/**
 * 
 */
package au.com.target.tgtstorefront.controllers.services;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.stock.TargetStockLookUpFacade;
import au.com.target.tgtwebcore.fluent.StockRequestDto;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StockServiceControllerTest {

    @Mock
    private TargetStockLookUpFacade targetStockLookUpFacade;

    @InjectMocks
    @Spy
    private final StockServiceController stockServiceController = new StockServiceController();

    @Test
    public void testLookupStock() {
        final Response response = mock(Response.class);
        willReturn(response).given(targetStockLookUpFacade).lookupStock(Mockito.anyList(), Mockito.anyList(),
                Mockito.anyList());
        final StockRequestDto stockRequestDto = mock(StockRequestDto.class);
        willReturn(Arrays.asList("P1000_red_L")).given(stockRequestDto).getVariants();
        willReturn(Arrays.asList("CC", "HD")).given(stockRequestDto).getDeliveryTypes();
        willReturn(Arrays.asList("5599")).given(stockRequestDto).getLocations();

        assertThat(stockServiceController.lookupStock(stockRequestDto)).isEqualTo(response);
        verify(targetStockLookUpFacade).lookupStock(Arrays.asList("P1000_red_L"), Arrays.asList("CC", "HD"),
                Arrays.asList("5599"));
    }

    @Test
    public void testLookupStockBaseProduct() {
        final Response response = mock(Response.class);
        willReturn(response).given(targetStockLookUpFacade).lookupStock(Mockito.anyString(), Mockito.anyList(),
                Mockito.anyList());
        final StockRequestDto stockRequestDto = mock(StockRequestDto.class);
        willReturn("P1000").given(stockRequestDto).getBaseProductCode();
        willReturn(Arrays.asList("CC", "HD")).given(stockRequestDto).getDeliveryTypes();
        willReturn(Arrays.asList("5599")).given(stockRequestDto).getLocations();

        assertThat(stockServiceController.lookupStock(stockRequestDto)).isEqualTo(response);
        verify(targetStockLookUpFacade).lookupStock("P1000", Arrays.asList("CC", "HD"),
                Arrays.asList("5599"));
    }
}

package au.com.target.tgtstorefront.controllers.services;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.util.CookieGenerator;

import au.com.target.tgtfacades.config.TargetSharedConfigFacade;
import au.com.target.tgtfacades.google.location.finder.TargetGoogleLocationFinderFacade;
import au.com.target.tgtfacades.response.data.HomeDeliveryLocation;
import au.com.target.tgtfacades.response.data.LocationResponseData;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;
import au.com.target.tgtstorefront.controllers.util.WebServiceExceptionHelper;
import au.com.target.tgtstorefront.util.SearchQuerySanitiser;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoresServiceControllerTest {

    private static final String DEFAULT_DELIVERY_LOCATION = "Chadstone$%%#@$^^";

    private static final String SANITISED_DEFAULT_DELIVERY_LOCATION = "Chadstone";

    private static final String DELIVERY_LOCATION = "&&&&####$%^&*Mackay*****&$^$#";

    private static final String SANITISED_DELIVERY_LOCATION = "Mackay";

    private static final Integer STORE_NUMBER_MACKAY = Integer.valueOf(7003);

    private static final Integer STORE_NUMBER = Integer.valueOf(7000);

    private static final String STORE_SELECTOR_PAGE_SIZE = "storefront.storelocator.pageSize";

    private static final String POSTAL_CODE = "3148";

    private static final String POSTAL_CODE_MACKAY = "4740";

    private static final String DEFAULT_DELIVERY_LOCATION_CODE = "default.location";

    private static final Object DEFAULT_DELIVERY_LOCATION_MELBOURNE = "Melbourne$$%^#^#^##%$%";

    private static final String POSTAL_CODE_MELBOURNE = "3000";

    private static final String SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE = "Melbourne";

    private static final String HOME_DELIVERY_LOCATION_DETAILS = "Home Delivery Location details";

    private static final String CLICK_AND_COLLECT_LOCATION_DETAILS = "Click and Collect store details";

    private static final String SUBURB = "Suburb";

    private static final String POSTAL_CODE_STRING = "Postal Code";

    private static final String CNC = "cnc";

    private static final String HD = "hd";

    private static final String SANITISED_INTERNATIONAL_LOCATION = "INT";

    private static final String INTERNATIONAL_LOCATION = "INT*%&^^*(^$****";


    @Mock
    private TargetStoreFinderStockFacade mockTargetStoreFinderStockFacade;

    @Mock(name = "inStoreStockSearchQuerySanitiser")
    private SearchQuerySanitiser mockInStoreStockSearchQuerySanitiser;

    @Mock(name = "inStoreStockSearchProductCodeSanitiser")
    private SearchQuerySanitiser mockInStoreStockSearchProductCodeSanitiser;

    @Mock
    private ConfigurationService mockConfigurationService;

    @Mock
    private Configuration mockConfiguration;

    @Mock
    private WebServiceExceptionHelper mockWebServiceExceptionHelper;

    @Mock
    private CookieGenerator mockStoreStockPostCodeCookieGenerator;

    @Mock
    private TargetSharedConfigFacade sharedConfigFacade;

    @Mock
    private TargetGoogleLocationFinderFacade targetGoogleLocationFinderFacade;

    private final HttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

    @InjectMocks
    private final StoresServiceController storesServiceController = new StoresServiceController();

    private final ArgumentCaptor<PageableData> pageableDataCaptor = ArgumentCaptor.forClass(PageableData.class);

    @Before
    public void setUp() {
        given(mockConfigurationService.getConfiguration()).willReturn(mockConfiguration);
    }

    @Test
    public void testGetNearestStoresWithLocation() throws Exception {
        final String location = "3215$^&";
        final String sanitisedLocation = "3215";
        final String selectedLocation = "Hamlyn Heights 3215  VIC**@@@(((";
        final String sanitisedSelectedLocation = "Hamlyn Heights 3215  VIC";
        given(mockInStoreStockSearchQuerySanitiser.sanitiseSearchText(location)).willReturn(sanitisedLocation);
        willReturn(Integer.valueOf(5)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 5);
        given(mockInStoreStockSearchQuerySanitiser.sanitiseSearchText(selectedLocation))
                .willReturn(sanitisedSelectedLocation);
        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        responseData.setSelectedLocation(selectedLocation);
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.doSearchProductStock((String)isNull(), eq(sanitisedLocation),
                (Double)isNull(), (Double)isNull(), (Integer)isNull(), pageableDataCaptor.capture(), eq(false)))
                        .willReturn(response);
        final Response actualResponse = storesServiceController.getNearestStores(null, null, location, null, null, 42,
                mockHttpServletResponse);
        assertThat(actualResponse).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(5);
        assertThat(capturedPageableData.getCurrentPage()).isEqualTo(42);
        verify(mockStoreStockPostCodeCookieGenerator).addCookie(mockHttpServletResponse,
                "Hamlyn%20Heights%203215%20%20VIC");
        verifyZeroInteractions(mockInStoreStockSearchProductCodeSanitiser);
    }

    @Test
    public void testGetNearestStoresWithCoordinates() throws Exception {
        final Double latitude = Double.valueOf(5);
        final Double longitude = Double.valueOf(10);
        willReturn(Integer.valueOf(5)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 5);
        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.doSearchProductStock((String)isNull(), (String)isNull(),
                eq(latitude), eq(longitude), (Integer)isNull(), pageableDataCaptor.capture(), eq(false)))
                        .willReturn(response);
        final Response actualResponse = storesServiceController.getNearestStores(longitude, latitude, null, null, null,
                42,
                mockHttpServletResponse);
        assertThat(actualResponse).isEqualTo(response);
        verifyZeroInteractions(mockInStoreStockSearchQuerySanitiser, mockStoreStockPostCodeCookieGenerator,
                mockInStoreStockSearchProductCodeSanitiser);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(5);
        assertThat(capturedPageableData.getCurrentPage()).isEqualTo(42);
    }

    @Test
    public void testGetNearestStoresWithStoreNumber() throws Exception {
        final Integer storeNumber = Integer.valueOf(5100);
        willReturn(Integer.valueOf(5)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 5);
        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.doSearchProductStock((String)isNull(), (String)isNull(),
                (Double)isNull(), (Double)isNull(), eq(storeNumber), pageableDataCaptor.capture(), eq(false)))
                        .willReturn(response);
        final Response actualResponse = storesServiceController.getNearestStores(null, null, null, null, storeNumber,
                42,
                mockHttpServletResponse);
        assertThat(actualResponse).isEqualTo(response);
        verifyZeroInteractions(mockInStoreStockSearchQuerySanitiser, mockStoreStockPostCodeCookieGenerator,
                mockInStoreStockSearchProductCodeSanitiser);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(5);
        assertThat(capturedPageableData.getCurrentPage()).isEqualTo(42);
    }

    @Test
    public void testGetNearestStoresWithLocationAndProductCode() throws Exception {
        final String productCode = "43560978**%%%^";
        final String sanitisedProductCode = "43560978";
        final String location = "3215$^&";
        final String sanitisedLocation = "3215";
        final String selectedLocation = "Hamlyn Heights 3215  VIC**@@@(((";
        final String sanitisedSelectedLocation = "Hamlyn Heights 3215  VIC";
        given(mockInStoreStockSearchQuerySanitiser.sanitiseSearchText(location)).willReturn(sanitisedLocation);
        given(mockInStoreStockSearchProductCodeSanitiser.sanitiseSearchText(productCode))
                .willReturn(sanitisedProductCode);
        willReturn(Integer.valueOf(5)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 5);
        given(mockInStoreStockSearchQuerySanitiser.sanitiseSearchText(selectedLocation))
                .willReturn(sanitisedSelectedLocation);
        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        responseData.setSelectedLocation(selectedLocation);
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.doSearchProductStock(eq(sanitisedProductCode), eq(sanitisedLocation),
                (Double)isNull(), (Double)isNull(), (Integer)isNull(), pageableDataCaptor.capture(), eq(false)))
                        .willReturn(response);
        final Response actualResponse = storesServiceController.getNearestStores(null, null, location, productCode,
                null, 42,
                mockHttpServletResponse);
        assertThat(actualResponse).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(5);
        assertThat(capturedPageableData.getCurrentPage()).isEqualTo(42);
        verify(mockStoreStockPostCodeCookieGenerator).addCookie(mockHttpServletResponse,
                "Hamlyn%20Heights%203215%20%20VIC");
    }

    @Test
    public void testGetNearestStoresWithCoordinatesAndProductCode() throws Exception {
        final String productCode = "43560978**%%%^";
        final String sanitisedProductCode = "43560978";
        final Double latitude = Double.valueOf(5);
        final Double longitude = Double.valueOf(10);
        given(mockInStoreStockSearchProductCodeSanitiser.sanitiseSearchText(productCode))
                .willReturn(sanitisedProductCode);
        willReturn(Integer.valueOf(5)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 5);
        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.doSearchProductStock(eq(sanitisedProductCode), (String)isNull(),
                eq(latitude), eq(longitude), (Integer)isNull(), pageableDataCaptor.capture(), eq(false)))
                        .willReturn(response);
        final Response actualResponse = storesServiceController.getNearestStores(longitude, latitude, null, productCode,
                null,
                42,
                mockHttpServletResponse);
        assertThat(actualResponse).isEqualTo(response);
        verifyZeroInteractions(mockInStoreStockSearchQuerySanitiser, mockStoreStockPostCodeCookieGenerator);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(5);
        assertThat(capturedPageableData.getCurrentPage()).isEqualTo(42);
    }

    @Test
    public void testGetNearestStoresWithStoreNumberAndProductCode() throws Exception {
        final String productCode = "43560978**%%%^";
        final String sanitisedProductCode = "43560978";
        final Integer storeNumber = Integer.valueOf(5100);
        given(mockInStoreStockSearchProductCodeSanitiser.sanitiseSearchText(productCode))
                .willReturn(sanitisedProductCode);
        willReturn(Integer.valueOf(5)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 5);
        final ProductStockSearchResultResponseData responseData = new ProductStockSearchResultResponseData();
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.doSearchProductStock(eq(sanitisedProductCode), (String)isNull(),
                (Double)isNull(), (Double)isNull(), eq(storeNumber), pageableDataCaptor.capture(), eq(false)))
                        .willReturn(response);
        final Response actualResponse = storesServiceController.getNearestStores(null, null, null, productCode,
                storeNumber,
                42,
                mockHttpServletResponse);
        assertThat(actualResponse).isEqualTo(response);
        verifyZeroInteractions(mockInStoreStockSearchQuerySanitiser, mockStoreStockPostCodeCookieGenerator);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(5);
        assertThat(capturedPageableData.getCurrentPage()).isEqualTo(42);
    }

    @Test
    public void testHandleException() {
        final HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        final Exception mockException = mock(Exception.class);
        storesServiceController.handleException(mockRequest, mockException);
        verify(mockWebServiceExceptionHelper).handleException(mockRequest, mockException);
    }

    /**
     * Method to verify nearest cnc store delivery location. Here the method accepting only one parameter(store
     * location) to fetch nearby store details available for cnc.
     *
     * @throws Exception
     */
    @Test
    public void testGetNearestCncDeliveryLocationWithStoreLocationParameter() throws Exception {
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 1);
        willReturn(SANITISED_DEFAULT_DELIVERY_LOCATION).given(mockInStoreStockSearchQuerySanitiser)
                .sanitiseSearchText(DEFAULT_DELIVERY_LOCATION);
        final LocationResponseData responseData = new LocationResponseData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = clickAndCollectDeliveryStoreData(
                responseData);
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.nearestStoreLocation(eq(CNC),
                eq(SANITISED_DEFAULT_DELIVERY_LOCATION),
                (Integer)isNull(), pageableDataCaptor.capture())).willReturn(response);
        final Response actualResponse = storesServiceController.getNearestCncHdDeliveryLocation(CNC,
                DEFAULT_DELIVERY_LOCATION,
                null, null);
        assertThat(actualResponse).as(CLICK_AND_COLLECT_LOCATION_DETAILS).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(1);
        final LocationResponseData locationResponseData = (LocationResponseData)actualResponse.getData();
        assertThat(locationResponseData.getClickAndCollectPos()).isNotNull();
        assertThat(locationResponseData.getClickAndCollectPos().getName()).isNotEmpty()
                .isEqualTo(SANITISED_DEFAULT_DELIVERY_LOCATION);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress().getPostalCode()).isNotEmpty()
                .isEqualTo(POSTAL_CODE);
        assertThat(locationResponseData.getClickAndCollectPos().getStoreNumber()).isNotNull().isEqualTo(STORE_NUMBER);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress()).isNotNull()
                .isEqualTo(targetPointOfServiceStockData.getAddress());
    }


    /**
     * Method to verify nearest cnc store delivery location. Here the method accepting only one parameter(store number)
     * to fetch nearby store details available for cnc.
     *
     * @throws Exception
     */
    @Test
    public void testGetNearestCncDeliveryLocationWithStoreNumber() throws Exception {
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 1);
        final LocationResponseData responseData = new LocationResponseData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = clickAndCollectDeliveryStoreData(
                responseData);
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.nearestStoreLocation(eq(CNC), (String)isNull(), eq(STORE_NUMBER),
                pageableDataCaptor.capture())).willReturn(response);
        final Response actualResponse = storesServiceController.getNearestCncHdDeliveryLocation(CNC, null,
                STORE_NUMBER, null);
        assertThat(actualResponse).as(CLICK_AND_COLLECT_LOCATION_DETAILS).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(1);
        final LocationResponseData locationResponseData = (LocationResponseData)actualResponse.getData();
        assertThat(locationResponseData.getClickAndCollectPos()).isNotNull();
        assertThat(locationResponseData.getClickAndCollectPos().getName()).isNotEmpty()
                .isEqualTo(SANITISED_DEFAULT_DELIVERY_LOCATION);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress().getPostalCode()).isNotEmpty()
                .isEqualTo(POSTAL_CODE);
        assertThat(locationResponseData.getClickAndCollectPos().getStoreNumber()).isNotNull().isEqualTo(STORE_NUMBER);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress()).isNotNull()
                .isEqualTo(targetPointOfServiceStockData.getAddress());
    }


    /**
     * Method to verify nearest cnc default store delivery location. Here the method does not accept any input parameter
     * and default store location 'Melbourne' is used to fetch nearby store details available for cnc.
     *
     * @throws Exception
     */
    @Test
    public void testGetNearestCncDeliveryLocationWithNoInputParameters() throws Exception {
        final String sanitisedLocation = "Melbourne";
        final String postalCode = "3000";
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 1);
        willReturn(sanitisedLocation).given(mockInStoreStockSearchQuerySanitiser)
                .sanitiseSearchText("Melbourne$$%^#^#^##%$%");
        final LocationResponseData responseData = new LocationResponseData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = new TargetPointOfServiceStockData();
        targetPointOfServiceStockData.setName(sanitisedLocation);
        final AddressData address = new AddressData();
        address.setPhone("(03) 5821 9266");
        final CountryData countryData = new CountryData();
        countryData.setName("Australia");
        countryData.setIsocode("AU");
        address.setCountry(countryData);
        address.setTown(sanitisedLocation);
        address.setPostalCode(postalCode);
        targetPointOfServiceStockData.setAddress(address);
        responseData.setClickAndCollectPos(targetPointOfServiceStockData);
        final Response response = new Response(true);
        response.setData(responseData);
        given(sharedConfigFacade.getConfigByCode(DEFAULT_DELIVERY_LOCATION_CODE)).willReturn(sanitisedLocation);
        given(mockTargetStoreFinderStockFacade.nearestStoreLocation(eq(CNC), eq(sanitisedLocation),
                (Integer)isNull(), pageableDataCaptor.capture())).willReturn(response);
        final Response actualResponse = storesServiceController.getNearestCncHdDeliveryLocation(CNC, null,
                null, null);
        assertThat(actualResponse).as(CLICK_AND_COLLECT_LOCATION_DETAILS).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(1);
        final LocationResponseData locationResponseData = (LocationResponseData)actualResponse.getData();
        assertThat(locationResponseData.getClickAndCollectPos()).isNotNull();
        assertThat(locationResponseData.getClickAndCollectPos().getName()).isNotEmpty().isEqualTo(sanitisedLocation);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress().getPostalCode()).isNotEmpty()
                .isEqualTo(postalCode);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress()).isNotNull()
                .isEqualTo(targetPointOfServiceStockData.getAddress());
    }

    /**
     * Method to verify nearest home delivery(HD) location. Here the method accepting only one parameter(store location)
     * to fetch nearby store details available for HD.
     *
     * @throws Exception
     */
    @Test
    public void testGetNearestHomeDeliveryLocationWithStoreLocationParameter() throws Exception {
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 1);
        willReturn(SANITISED_DELIVERY_LOCATION).given(mockInStoreStockSearchQuerySanitiser)
                .sanitiseSearchText(DELIVERY_LOCATION);
        final LocationResponseData responseData = new LocationResponseData();
        final HomeDeliveryLocation homeDeliveryLocation = homeDeliveryStoreData(responseData);
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.nearestStoreLocation(eq(HD), eq(SANITISED_DELIVERY_LOCATION),
                (Integer)isNull(), pageableDataCaptor.capture())).willReturn(response);
        final Response actualResponse = storesServiceController.getNearestCncHdDeliveryLocation(HD,
                DELIVERY_LOCATION, null, null);
        assertThat(actualResponse).as(HOME_DELIVERY_LOCATION_DETAILS).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(1);
        final LocationResponseData locationResponseData = (LocationResponseData)actualResponse.getData();
        assertThat(locationResponseData.getHomeDeliveryPos().getSuburb()).isNotEmpty().as(SUBURB)
                .isEqualTo(homeDeliveryLocation.getSuburb());
        assertThat(locationResponseData.getHomeDeliveryPos().getPostalCode()).isNotEmpty().as(POSTAL_CODE)
                .isEqualTo(homeDeliveryLocation.getPostalCode());
    }

    /**
     * Method to verify nearest home delivery(HD) location. Here the method accepting only one parameter(store number)
     * to fetch nearby store details available for HD.
     *
     * @throws Exception
     */
    @Test
    public void testGetNearestHomeDeliveryLocationWithStoreNumberParameter() throws Exception {
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 1);
        final LocationResponseData responseData = new LocationResponseData();
        final HomeDeliveryLocation homeDeliveryLocation = homeDeliveryStoreData(responseData);
        final Response response = new Response(true);
        response.setData(responseData);
        given(mockTargetStoreFinderStockFacade.nearestStoreLocation(eq(HD), (String)isNull(),
                eq(STORE_NUMBER_MACKAY),
                pageableDataCaptor.capture())).willReturn(response);
        final Response actualResponse = storesServiceController.getNearestCncHdDeliveryLocation(HD, null,
                STORE_NUMBER_MACKAY,
                null);
        assertThat(actualResponse).as(HOME_DELIVERY_LOCATION_DETAILS).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(1);
        final LocationResponseData locationResponseData = (LocationResponseData)actualResponse.getData();
        assertThat(locationResponseData.getHomeDeliveryPos().getSuburb()).isNotEmpty().as(SUBURB)
                .isEqualTo(homeDeliveryLocation.getSuburb());
        assertThat(locationResponseData.getHomeDeliveryPos().getPostalCode()).isNotEmpty().as(POSTAL_CODE)
                .isEqualTo(homeDeliveryLocation.getPostalCode());
    }


    /**
     * Method to verify nearest home delivery(HD) default store delivery location. Here the method does not accept any
     * input parameter and default store location is used to fetch nearby store details available for HD.
     *
     * @throws Exception
     */
    @Test
    public void testGetNearestHomeDeliveryLocationWithNoInputParameters() throws Exception {
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 1);
        willReturn(DEFAULT_DELIVERY_LOCATION_MELBOURNE).given(mockInStoreStockSearchQuerySanitiser)
                .sanitiseSearchText(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        final LocationResponseData responseData = new LocationResponseData();
        final HomeDeliveryLocation homeDeliveryLocation = new HomeDeliveryLocation();
        homeDeliveryLocation.setSuburb(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        homeDeliveryLocation.setPostalCode(POSTAL_CODE_MELBOURNE);
        responseData.setHomeDeliveryPos(homeDeliveryLocation);
        final Response response = new Response(true);
        response.setData(responseData);
        given(sharedConfigFacade.getConfigByCode(DEFAULT_DELIVERY_LOCATION_CODE))
                .willReturn(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        given(mockTargetStoreFinderStockFacade.nearestStoreLocation(eq(HD),
                eq(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE),
                (Integer)isNull(), pageableDataCaptor.capture())).willReturn(response);
        final Response actualResponse = storesServiceController.getNearestCncHdDeliveryLocation(HD, null,
                null, null);
        assertThat(actualResponse).as(HOME_DELIVERY_LOCATION_DETAILS).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(1);
        final LocationResponseData locationResponseData = (LocationResponseData)actualResponse.getData();
        assertThat(locationResponseData.getHomeDeliveryPos().getSuburb()).isNotEmpty().as(SUBURB)
                .isEqualTo(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        assertThat(locationResponseData.getHomeDeliveryPos().getPostalCode()).isNotEmpty().as(POSTAL_CODE_STRING)
                .isEqualTo(POSTAL_CODE_MELBOURNE);
    }

    @Test
    public void testGetNearestCncHdDeliveryLocationForCNCWithInternationalLocation() {
        final String postalCode = "3000";
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 1);
        willReturn(SANITISED_INTERNATIONAL_LOCATION).given(mockInStoreStockSearchQuerySanitiser)
                .sanitiseSearchText(INTERNATIONAL_LOCATION);
        final LocationResponseData responseData = new LocationResponseData();
        final TargetPointOfServiceStockData targetPointOfServiceStockData = new TargetPointOfServiceStockData();
        targetPointOfServiceStockData.setName(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        final AddressData address = new AddressData();
        address.setPhone("(03) 5821 9266");
        final CountryData countryData = new CountryData();
        countryData.setName("Australia");
        countryData.setIsocode("AU");
        address.setCountry(countryData);
        address.setTown(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        address.setPostalCode(postalCode);
        targetPointOfServiceStockData.setAddress(address);
        responseData.setClickAndCollectPos(targetPointOfServiceStockData);
        final Response response = new Response(true);
        response.setData(responseData);
        given(sharedConfigFacade.getConfigByCode(DEFAULT_DELIVERY_LOCATION_CODE))
                .willReturn(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        given(mockTargetStoreFinderStockFacade.nearestStoreLocation(eq(CNC),
                eq(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE),
                (Integer)isNull(), pageableDataCaptor.capture())).willReturn(response);
        final Response actualResponse = storesServiceController.getNearestCncHdDeliveryLocation(CNC,
                INTERNATIONAL_LOCATION,
                null, null);
        assertThat(actualResponse).as(CLICK_AND_COLLECT_LOCATION_DETAILS).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(1);
        final LocationResponseData locationResponseData = (LocationResponseData)actualResponse.getData();
        assertThat(locationResponseData.getClickAndCollectPos()).isNotNull();
        assertThat(locationResponseData.getClickAndCollectPos().getName()).isNotEmpty()
                .isEqualTo(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress().getPostalCode()).isNotEmpty()
                .isEqualTo(postalCode);
        assertThat(locationResponseData.getClickAndCollectPos().getAddress()).isNotNull()
                .isEqualTo(targetPointOfServiceStockData.getAddress());
    }

    @Test
    public void testGetNearestCncHdDeliveryLocationForHDWithInternationalLocation() throws Exception {
        willReturn(Integer.valueOf(1)).given(mockConfiguration).getInt(STORE_SELECTOR_PAGE_SIZE, 1);
        willReturn(SANITISED_INTERNATIONAL_LOCATION).given(mockInStoreStockSearchQuerySanitiser)
                .sanitiseSearchText(INTERNATIONAL_LOCATION);
        final LocationResponseData responseData = new LocationResponseData();
        final HomeDeliveryLocation homeDeliveryLocation = new HomeDeliveryLocation();
        homeDeliveryLocation.setSuburb("Melbourne$##$%#%$%%");
        homeDeliveryLocation.setPostalCode("3000");
        responseData.setHomeDeliveryPos(homeDeliveryLocation);
        final Response response = new Response(true);
        response.setData(responseData);
        given(sharedConfigFacade.getConfigByCode(DEFAULT_DELIVERY_LOCATION_CODE))
                .willReturn(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE);
        given(mockTargetStoreFinderStockFacade.nearestStoreLocation(eq(HD),
                eq(SANITISED_DEFAULT_DELIVERY_LOCATION_MELBOURNE),
                (Integer)isNull(), pageableDataCaptor.capture())).willReturn(response);
        final Response actualResponse = storesServiceController.getNearestCncHdDeliveryLocation(HD,
                INTERNATIONAL_LOCATION, null, null);
        assertThat(actualResponse).as(HOME_DELIVERY_LOCATION_DETAILS).isEqualTo(response);
        final PageableData capturedPageableData = pageableDataCaptor.getValue();
        assertThat(capturedPageableData.getPageSize()).isEqualTo(1);
        final LocationResponseData locationResponseData = (LocationResponseData)actualResponse.getData();
        assertThat(locationResponseData.getHomeDeliveryPos().getSuburb()).isNotEmpty().as(SUBURB)
                .isEqualTo(homeDeliveryLocation.getSuburb());
        assertThat(locationResponseData.getHomeDeliveryPos().getPostalCode()).isNotEmpty().as(POSTAL_CODE)
                .isEqualTo(homeDeliveryLocation.getPostalCode());
    }


    /**
     * This method will verify success scenario(geocode response received), Based on the current location latlng values
     * address details(location name and postcode) will get added to cookie(t_pc).
     */
    @Test
    public void testGetNearestStoresVerifyAddCookieForGivenLatLngSuccess() {
        final Double latitude = Double.valueOf(-38.1272064);
        final Double longitude = Double.valueOf(144.33648639999998);
        final String location = "Hamlyn Heights 3215**@@@(((";
        final String sanitisedSelectedLocation = "Hamlyn Heights 3215";
        given(targetGoogleLocationFinderFacade.searchUserCurrentAddressUsingLatLng(latitude, longitude))
                .willReturn(location);
        given(mockInStoreStockSearchQuerySanitiser.sanitiseSearchText(location))
                .willReturn(sanitisedSelectedLocation);
        final Response response = new Response(true);
        response.setData(mock(ProductStockSearchResultResponseData.class));
        given(mockTargetStoreFinderStockFacade.doSearchProductStock((String)isNull(), (String)isNull(),
                eq(latitude), eq(longitude), (Integer)isNull(), pageableDataCaptor.capture(), eq(false)))
                        .willReturn(response);
        final Response actualResponse = storesServiceController.getNearestStores(longitude, latitude, null, null, null,
                42, mockHttpServletResponse);
        assertThat(actualResponse).isEqualTo(response);
        verify(mockStoreStockPostCodeCookieGenerator).addCookie(mockHttpServletResponse,
                "Hamlyn%20Heights%203215");
    }

    /**
     * This method will verify failure(response-no geocode response received) scenario. Based on the current location
     * latlng values address details(location name and postcode) will not added to cookie(t_pc).
     */
    @Test
    public void testGetNearestStoresVerifyAddCookieForGivenLatLngFailure() {
        final Double latitude = Double.valueOf(-38.1272064);
        final Double longitude = Double.valueOf(144.33648639999998);
        final Response response = new Response(false);
        response.setData(mock(ProductStockSearchResultResponseData.class));
        given(mockTargetStoreFinderStockFacade.doSearchProductStock((String)isNull(), (String)isNull(),
                eq(latitude), eq(longitude), (Integer)isNull(), pageableDataCaptor.capture(), eq(false)))
                        .willReturn(response);
        final Response actualResponse = storesServiceController.getNearestStores(longitude, latitude, null, null, null,
                42, mockHttpServletResponse);
        assertThat(actualResponse).isEqualTo(response);
        verify(mockStoreStockPostCodeCookieGenerator, never()).addCookie(mockHttpServletResponse,
                "Hamlyn%20Heights%203215");
    }

    /**
     * @param responseData
     * @return TargetPointOfServiceStockData
     */
    private HomeDeliveryLocation homeDeliveryStoreData(final LocationResponseData responseData) {
        final HomeDeliveryLocation homeDeliveryLocation = new HomeDeliveryLocation();
        homeDeliveryLocation.setSuburb(DEFAULT_DELIVERY_LOCATION);
        homeDeliveryLocation.setPostalCode(POSTAL_CODE_MACKAY);
        responseData.setHomeDeliveryPos(homeDeliveryLocation);
        return homeDeliveryLocation;
    }

    /**
     * @param responseData
     * @return TargetPointOfServiceStockData
     */
    private TargetPointOfServiceStockData clickAndCollectDeliveryStoreData(final LocationResponseData responseData) {
        final TargetPointOfServiceStockData targetPointOfServiceStockData = new TargetPointOfServiceStockData();
        targetPointOfServiceStockData.setName(SANITISED_DEFAULT_DELIVERY_LOCATION);
        targetPointOfServiceStockData.setStoreNumber(STORE_NUMBER);
        final AddressData address = new AddressData();
        address.setPhone("(03) 5821 9255");
        final CountryData countryData = new CountryData();
        countryData.setName("Australia");
        countryData.setIsocode("AU");
        address.setCountry(countryData);
        address.setTown(SANITISED_DEFAULT_DELIVERY_LOCATION);
        address.setPostalCode("3148");
        targetPointOfServiceStockData.setAddress(address);
        responseData.setClickAndCollectPos(targetPointOfServiceStockData);
        return targetPointOfServiceStockData;
    }
}

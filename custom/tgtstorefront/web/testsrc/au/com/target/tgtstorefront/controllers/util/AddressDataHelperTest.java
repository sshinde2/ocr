/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.checkout.flow.TargetCheckoutCustomerStrategy;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtstorefront.data.AddressSuggestionResult;
import au.com.target.tgtstorefront.forms.AddressForm;
import au.com.target.tgtstorefront.forms.TargetAddressForm;
import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AddressDataHelperTest {

    @InjectMocks
    private final AddressDataHelper helper = new AddressDataHelper();

    @Mock
    private TargetDeliveryFacade targetDeliveryFacade;

    @Mock
    private TargetAddressVerificationService targetAddressVerificationService;

    @Mock
    private TargetUserFacade targetUserFacade;

    @Mock
    private TargetCheckoutCustomerStrategy targetCheckoutCustomerStrategy;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private UserService userService;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    @Mock
    private FormattedAddressData formattedAddressForm;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    @Mock
    private I18NFacade i18NFacade;

    @SuppressWarnings("boxing")
    @Before
    public void setUp() {
        when(configurationService.getConfiguration()).thenReturn(configuration);
        when(configuration.getInt("tgtstorefront.address.lookup.minimum.length")).thenReturn(3);
        when(configuration.getInt("tgtstorefront.address.lookup.size")).thenReturn(10);
    }

    @SuppressWarnings("boxing")
    @Test
    public void checkIfSingleLineAddressFeatureIsEnabled() {
        final Model model = mock(Model.class);
        when(targetFeatureSwitchFacade.isFeatureEnabled("single.line.address.lookup")).thenReturn(
                Boolean.TRUE);

        helper.checkIfSingleLineAddressFeatureIsEnabled(model);

        verify(targetFeatureSwitchFacade).isFeatureEnabled("single.line.address.lookup");
        verify(model).addAttribute("singleLineAddressLookup", Boolean.TRUE);
    }

    @SuppressWarnings("boxing")
    @Test
    public void shoudReturnFalseForKioskWhenSingleLineAddressFeatureIsEnabled() {
        final Model model = mock(Model.class);
        when(targetFeatureSwitchFacade.isFeatureEnabled("single.line.address.lookup")).thenReturn(
                Boolean.TRUE);
        when(salesApplicationFacade.isKioskApplication()).thenReturn(true);

        helper.checkIfSingleLineAddressFeatureIsEnabled(model);

        verify(targetFeatureSwitchFacade).isFeatureEnabled("single.line.address.lookup");
        verify(model).addAttribute("singleLineAddressLookup", Boolean.FALSE);
    }

    @SuppressWarnings("boxing")
    @Test
    public void checkIfSingleLineAddressFeatureIsDisabled() {
        final Model model = mock(Model.class);
        when(targetFeatureSwitchFacade.isFeatureEnabled("single.line.address.lookup")).thenReturn(
                Boolean.FALSE);

        helper.checkIfSingleLineAddressFeatureIsEnabled(model);

        verify(targetFeatureSwitchFacade).isFeatureEnabled("single.line.address.lookup");
        verify(model).addAttribute("singleLineAddressLookup", Boolean.FALSE);
    }

    @Test
    public void testFormatAddressWithNoResult() throws ServiceNotAvailableException {
        final ServiceNotAvailableException serviceNotAvailableException = mock(ServiceNotAvailableException.class);
        when(targetAddressVerificationService.formatAddress(any(AddressData.class))).thenThrow(
                serviceNotAvailableException);

        final FormattedAddressData formattedData = helper.formatAddress("Invalid Code", "123 Bourke Street");

        assertEquals(formattedData.getAddressLine1(), "123 Bourke Street");
    }

    @Test
    public void testFormatAddressWithResult() throws ServiceNotAvailableException {
        final ServiceNotAvailableException ex = mock(ServiceNotAvailableException.class);
        when(targetAddressVerificationService.formatAddress(any(AddressData.class))).thenThrow(ex);

        final FormattedAddressData formattedData = helper.formatAddress("Valid Code", "123 Bourke Street");

        assertEquals(formattedData.getAddressLine1(), "123 Bourke Street");
    }

    @Test
    public void testFormatAddressWhenException() throws ServiceNotAvailableException {
        final FormattedAddressData data = mock(FormattedAddressData.class);
        when(targetAddressVerificationService.formatAddress(any(AddressData.class))).thenReturn(data);
        final FormattedAddressData formattedData = helper.formatAddress("Valid Code", "123 Bourke Street");
        assertEquals(formattedData, data);
    }

    @Test
    public void testGetSuggestionWithTooManyResult() throws Exception {
        final TooManyMatchesFoundException ex = mock(TooManyMatchesFoundException.class);
        when(targetAddressVerificationService.searchAddress("TooManyMatchesFound", CountryEnum.AUSTRALIA))
                .thenThrow(ex);
        final AddressSuggestionResult suggestionResult = helper.getAddressSuggestions("TooManyMatchesFound");

        assertEquals("TooManyMatchesFound", suggestionResult.getStatus());
    }

    @Test
    public void testGetSuggestionWithNoResult() throws Exception {
        final NoMatchFoundException ex = mock(NoMatchFoundException.class);
        when(targetAddressVerificationService.searchAddress("NoMatchFound", CountryEnum.AUSTRALIA))
                .thenThrow(ex);

        final AddressSuggestionResult suggestionResult = helper.getAddressSuggestions("NoMatchFound");

        assertEquals("NoMatchFound", suggestionResult.getStatus());
    }

    @Test
    public void testGetSuggestionWithInvalidService() throws Exception {
        final ServiceNotAvailableException ex = mock(ServiceNotAvailableException.class);
        when(targetAddressVerificationService.searchAddress("ServiceNotAvailable", CountryEnum.AUSTRALIA))
                .thenThrow(ex);

        final AddressSuggestionResult suggestionResult = helper.getAddressSuggestions("ServiceNotAvailable");

        assertEquals("VerificationFailed", suggestionResult.getStatus());
    }

    @Test
    public void testGetSuggestionWithTimeout() throws Exception {
        final RequestTimeOutException ex = mock(RequestTimeOutException.class);
        when(targetAddressVerificationService.searchAddress("Timeout", CountryEnum.AUSTRALIA))
                .thenThrow(ex);

        final AddressSuggestionResult suggestionResult = helper.getAddressSuggestions("Timeout");

        assertEquals("VerificationFailed", suggestionResult.getStatus());
    }

    @Test
    public void testGetSuggestions() throws Exception {
        final List<AddressData> qasList = new ArrayList<>();
        qasList.add(new AddressData("code", "address", "postcode"));
        when(targetAddressVerificationService.searchAddress("Address", CountryEnum.AUSTRALIA))
                .thenReturn(qasList);

        final AddressSuggestionResult suggestionResult = helper.getAddressSuggestions("Address");

        assertEquals("OK", suggestionResult.getStatus());
        assertEquals(1, suggestionResult.getSuggestions().size());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetSuggestionsWithSuggestionsOverlimit() throws Exception {
        final List<AddressData> qasList = new ArrayList<>();
        when(configuration.getInt("tgtstorefront.address.lookup.size")).thenReturn(1);
        qasList.add(new AddressData("code1", "address1", "postcode1"));
        qasList.add(new AddressData("code2", "address2", "postcode2"));
        when(targetAddressVerificationService.searchAddress("Address", CountryEnum.AUSTRALIA))
                .thenReturn(qasList);

        final AddressSuggestionResult suggestionResult = helper.getAddressSuggestions("Address");

        assertEquals("OK", suggestionResult.getStatus());
        assertEquals(1, suggestionResult.getSuggestions().size());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testGetSuggestionsWithKeywordTooShort() throws Exception {
        final List<AddressData> qasList = new ArrayList<>();
        when(configuration.getInt("tgtstorefront.address.lookup.size")).thenReturn(1);
        qasList.add(new AddressData("code1", "address1", "postcode1"));
        qasList.add(new AddressData("code2", "address2", "postcode2"));
        when(targetAddressVerificationService.verifyAddress("add", CountryEnum.AUSTRALIA))
                .thenReturn(qasList);

        final AddressSuggestionResult suggestionResult = helper.getAddressSuggestions("add");

        assertEquals("InputIsTooShort", suggestionResult.getStatus());
        assertEquals(0, suggestionResult.getSuggestions().size());
        verifyZeroInteractions(targetAddressVerificationService);
    }

    @Test
    public void testVerifyAddress() {
        final AddressForm form1 = new AddressForm();
        final FormattedAddressData form2 = new FormattedAddressData();
        assertFalse(helper.isAddressFormVerified(null, null));
        assertFalse(helper.isAddressFormVerified(form1, null));
        assertTrue(helper.isAddressFormVerified(form1, form2));
        form1.setLine1("Line1");
        assertFalse(helper.isAddressFormVerified(form1, form2));
        form2.setAddressLine1("line1");
        assertTrue(helper.isAddressFormVerified(form1, form2));
        assertTrue(helper.isAddressFormVerified(form1, form2));
    }

    @Test
    public void testCreateAustralianShippingAddress() {
        final TargetAddressForm addressForm = new TargetAddressForm();
        addressForm.setFirstName("firstname");
        addressForm.setLastName("lastname");
        addressForm.setLine1("11 bourke st");
        addressForm.setLine2(null);
        addressForm.setPhone("0421521254");
        addressForm.setPostalCode("3125");
        addressForm.setState("vic");
        addressForm.setTitle("dr");
        addressForm.setTown("geelong");
        final TargetAddressData addressData = helper.createTargetAustralianShippingAddressData(addressForm);

        assertThat(addressData.isBillingAddress()).isFalse();
        assertThat(addressData.isShippingAddress()).isTrue();
        assertThat(addressData.getCountry().getIsocode()).isEqualTo(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        assertThat(addressData.getBuilding()).isNull();
        assertThat(addressData.getCompanyName()).isNull();
        assertThat(addressData.getEmail()).isNull();
        assertThat(addressData.getFirstName()).isEqualTo(addressForm.getFirstName());
        assertThat(addressData.getFormattedAddress()).isNull();
        assertThat(addressData.getId()).isNull();
        assertThat(addressData.getLastName()).isEqualTo(addressForm.getLastName());
        assertThat(addressData.getLine1()).isEqualTo(addressForm.getLine1());
        assertThat(addressData.getLine2()).isEqualTo(addressForm.getLine2());
        assertThat(addressData.getPhone()).isEqualTo(addressForm.getPhone());
        assertThat(addressData.getPostalCode()).isEqualTo(addressForm.getPostalCode());
        assertThat(addressData.getState()).isEqualTo(addressForm.getState());
        assertThat(addressData.getTitleCode()).isEqualTo(addressForm.getTitle());
        assertThat(addressData.getTown()).isEqualTo(addressForm.getTown());
        assertThat(addressData.isAddressValidated()).isFalse();

    }

    @Test
    public void testGetMaxAddressSearchSize() {
        doReturn(Integer.valueOf(10)).when(configuration).getInt("tgtstorefront.address.lookup.size");
        assertThat(helper.getMaxSearchSizeForAddressLookup()).isEqualTo(10);
    }

    @Test
    public void createValidatedAustralianShippingAddress() throws ServiceNotAvailableException {
        final FormattedAddressData formattedAddressData = new FormattedAddressData();
        formattedAddressData.setAddressLine1("10-12 Thompson Avenue");
        formattedAddressData.setPostcode("7253");
        formattedAddressData.setState("TAS");
        formattedAddressData.setCity("GEORGE TOWN");
        BDDMockito.given(
                targetAddressVerificationService.formatAddress(Mockito
                        .any(au.com.target.tgtverifyaddr.data.AddressData.class))).willReturn(formattedAddressData);
        final TargetAddressForm addressForm = new TargetAddressForm();
        addressForm.setFirstName("firstname");
        addressForm.setLastName("lastname");
        addressForm.setLine1(null);
        addressForm.setLine2(null);
        addressForm.setPhone("0421521254");
        addressForm.setPostalCode(null);
        addressForm.setState("vic");
        addressForm.setTitle("dr");
        addressForm.setTown(null);
        addressForm
                .setSingleLineId("SingleLineID");
        addressForm
                .setSingleLineLabel("SingleLineLabel");
        final TargetAddressData returnedAddressData = helper
                .createVerifiedTargetAustralianShippingAddressData(addressForm);
        assertThat(returnedAddressData.isBillingAddress()).isFalse();
        assertThat(returnedAddressData.isShippingAddress()).isTrue();
        assertThat(returnedAddressData.getCountry().getIsocode())
                .isEqualTo(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        assertThat(returnedAddressData.getBuilding()).isNull();
        assertThat(returnedAddressData.getCompanyName()).isNull();
        assertThat(returnedAddressData.getEmail()).isNull();
        assertThat(returnedAddressData.getFirstName()).isEqualTo(addressForm.getFirstName());
        assertThat(returnedAddressData.getFormattedAddress()).isNull();
        assertThat(returnedAddressData.getId()).isNull();
        assertThat(returnedAddressData.getLastName()).isEqualTo(addressForm.getLastName());
        assertThat(returnedAddressData.getLine1()).isEqualTo("10-12 Thompson Avenue");
        assertThat(returnedAddressData.getLine2()).isEqualTo(addressForm.getLine2());
        assertThat(returnedAddressData.getPhone()).isEqualTo(addressForm.getPhone());
        assertThat(returnedAddressData.getPostalCode()).isEqualTo("7253");
        assertThat(returnedAddressData.getState()).isEqualTo("TAS");
        assertThat(returnedAddressData.getTitleCode()).isEqualTo(addressForm.getTitle());
        assertThat(returnedAddressData.getTown()).isEqualTo("GEORGE TOWN");
        assertThat(returnedAddressData.isAddressValidated()).isTrue();
    }

    @Test
    public void createValidatedAustralianShippingAddressWhenServiceErrorOrAddressNotValid() {
        final TargetAddressForm addressForm = new TargetAddressForm();
        addressForm.setFirstName("firstname");
        addressForm.setLastName("lastname");
        addressForm.setLine1(null);
        addressForm.setLine2(null);
        addressForm.setPhone("0421521254");
        addressForm.setPostalCode(null);
        addressForm.setState("vic");
        addressForm.setTitle("dr");
        addressForm.setTown(null);
        addressForm
                .setSingleLineId("SingleLineID");
        addressForm
                .setSingleLineLabel("SingleLineLabel");
        final TargetAddressData returnedAddressData = helper
                .createVerifiedTargetAustralianShippingAddressData(addressForm);
        assertThat(returnedAddressData).isNull();
    }

    @Test
    public void testCreateValidatedAustralianBillingAddress() throws ServiceNotAvailableException {
        final FormattedAddressData formattedAddressData = new FormattedAddressData();
        formattedAddressData.setAddressLine1("10-12 Thompson Avenue");
        formattedAddressData.setPostcode("7253");
        formattedAddressData.setState("TAS");
        formattedAddressData.setCity("GEORGE TOWN");
        BDDMockito.given(targetAddressVerificationService.formatAddress(Mockito
                .any(au.com.target.tgtverifyaddr.data.AddressData.class))).willReturn(formattedAddressData);

        final CountryData australia = new CountryData();
        australia.setIsocode("AU");
        australia.setName("Australia");
        BDDMockito.given(i18NFacade.getCountryForIsocode("AU")).willReturn(australia);

        final TargetAddressForm addressForm = new TargetAddressForm();
        addressForm.setLine1(null);
        addressForm.setLine2(null);
        addressForm.setPostalCode(null);
        addressForm.setState("vic");
        addressForm.setTown(null);
        addressForm.setSingleLineId("SingleLineID");
        addressForm.setSingleLineLabel("SingleLineLabel");

        final TargetAddressData returnedAddressData = helper
                .createVerifiedTargetAustralianBillingAddressData(addressForm);

        assertThat(returnedAddressData.isBillingAddress()).isTrue();
        assertThat(returnedAddressData.isShippingAddress()).isFalse();
        assertThat(returnedAddressData.getCountry().getIsocode())
                .isEqualTo(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        assertThat(returnedAddressData.getBuilding()).isNull();
        assertThat(returnedAddressData.getCompanyName()).isNull();
        assertThat(returnedAddressData.getEmail()).isNull();
        assertThat(returnedAddressData.getFirstName()).isNull();
        assertThat(returnedAddressData.getFormattedAddress()).isNull();
        assertThat(returnedAddressData.getId()).isNull();
        assertThat(returnedAddressData.getLastName()).isNull();
        assertThat(returnedAddressData.getLine1()).isEqualTo("10-12 Thompson Avenue");
        assertThat(returnedAddressData.getLine2()).isEqualTo(addressForm.getLine2());
        assertThat(returnedAddressData.getPhone()).isNull();
        assertThat(returnedAddressData.getPostalCode()).isEqualTo("7253");
        assertThat(returnedAddressData.getState()).isEqualTo("TAS");
        assertThat(returnedAddressData.getTitleCode()).isNull();
        assertThat(returnedAddressData.getTown()).isEqualTo("GEORGE TOWN");
        assertThat(returnedAddressData.isAddressValidated()).isTrue();
        assertThat(returnedAddressData.getCountry()).isNotNull();
        assertThat(returnedAddressData.getCountry().getIsocode()).isEqualTo("AU");
        assertThat(returnedAddressData.getCountry().getName()).isEqualTo("Australia");
    }
}
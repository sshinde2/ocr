/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.servicelayer.services.CMSNavigationService;

import java.util.Arrays;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CMSNavigationComponentHelperTest {

    @Mock
    private CMSNavigationService cmsNavigationService;

    @InjectMocks
    private final CMSNavigationComponentHelper cmsNavigationComponentHelper = new CMSNavigationComponentHelper();

    @Test
    public void testGetCategoryNavigationNodeWhenNoCategoryFound() throws CMSItemNotFoundException {
        BDDMockito.given(
                cmsNavigationService.getNavigationNodeForId("TargetSiteCategoriesNavNode"))
                .willThrow(new CMSItemNotFoundException("error"));

        Assertions.assertThat(cmsNavigationComponentHelper.getCategoryNavigationNode("W2222")).isNull();
    }

    @Test
    public void testGetCategoryNavigationNodeWhenNoChildrenForSiteCategoriesNode() throws CMSItemNotFoundException {
        final CMSNavigationNodeModel mockCMSNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCMSNavigationNodeModel.getChildren()).willReturn(null);
        BDDMockito.given(
                cmsNavigationService.getNavigationNodeForId("TargetSiteCategoriesNavNode"))
                .willReturn(mockCMSNavigationNodeModel);

        Assertions.assertThat(cmsNavigationComponentHelper.getCategoryNavigationNode("W2222")).isNull();
    }

    @Test
    public void testGetCategoryNavigationNodeWhenMatchingChildrenForSiteCategoriesNodeFound()
            throws CMSItemNotFoundException {
        final CMSNavigationNodeModel mockCMSNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final CMSNavigationNodeModel mockChildCMSNavigationNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCMSNavigationNodeModel.getChildren()).willReturn(
                Arrays.asList(mockChildCMSNavigationNodeModel));

        final CMSNavigationEntryModel mockCMSNavigationEntryModel = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockChildCMSNavigationNodeModel.getEntries()).willReturn(
                Arrays.asList(mockCMSNavigationEntryModel));

        final CategoryModel mockCategoryModel = Mockito.mock(CategoryModel.class);
        BDDMockito.given(mockCMSNavigationEntryModel.getItem()).willReturn(
                mockCategoryModel);
        BDDMockito.given(mockCategoryModel.getCode()).willReturn(
                "W2222");

        BDDMockito.given(
                cmsNavigationService.getNavigationNodeForId("TargetSiteCategoriesNavNode"))
                .willReturn(mockCMSNavigationNodeModel);

        Assertions.assertThat(cmsNavigationComponentHelper.getCategoryNavigationNode("W2222")).isEqualTo(
                mockChildCMSNavigationNodeModel);
    }
}

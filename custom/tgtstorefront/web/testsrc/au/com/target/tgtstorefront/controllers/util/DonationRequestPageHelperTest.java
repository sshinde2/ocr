/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;
import au.com.target.tgtstorefront.forms.DonationRequestForm;


/**
 * @author bhuang3
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class DonationRequestPageHelperTest {

    @InjectMocks
    private final DonationRequestPageHelper donationRequestPageHelper = new DonationRequestPageHelper();

    private final DonationRequestForm form = new DonationRequestForm();
    private final DonationRequestForm formWithTag = new DonationRequestForm();

    @Before
    public void setup() {
        form.setAbnOrAcn("65498732112");
        form.setOrganisationName("Ruby Org");
        form.setEventName("Rugby Event");
        form.setContactName("Jay");
        form.setRole("Leader");
        form.setOrganisationStreet("115 Swanston St");
        form.setOrganisationCity("Melbourne");
        form.setOrganisationState("VIC");
        form.setOrganisationPostCode("3000");
        form.setEmail("test@target.com.au");
        form.setPhoneNumber("95369875");
        form.setReason("reason test");
        form.setNotes("notes test");
        form.setPreferredMethodofContact("Email");
        form.setStoreNumber("5128");

        formWithTag.setAbnOrAcn("<html>65498732112");
        formWithTag.setOrganisationName("<html>Ruby Org");
        formWithTag.setEventName("<html>Rugby Event");
        formWithTag.setContactName("<html>Jay");
        formWithTag.setRole("<html>Leader");
        formWithTag.setOrganisationStreet("<html>115 Swanston St");
        formWithTag.setOrganisationCity("<html>Melbourne");
        formWithTag.setOrganisationState("<html>VIC");
        formWithTag.setOrganisationPostCode("3000");
        formWithTag.setEmail("test@target.com.au");
        formWithTag.setPhoneNumber("95369875");
        formWithTag.setReason("<html>reason test");
        formWithTag.setNotes("<html>notes test");
        formWithTag.setPreferredMethodofContact("Email");
        formWithTag.setStoreNumber("5128");
    }

    @Test
    public void testPopulateDonationRequestDtoFromForm() {

        final TargetCommunityDonationRequestDto dto = donationRequestPageHelper
                .populateDonationRequestDtoFromForm(form);
        Assert.assertEquals("65498732112", dto.getAbnOrAcn());
        Assert.assertEquals("Ruby Org", dto.getOrgName());
        Assert.assertEquals("Rugby Event", dto.getEventName());
        Assert.assertEquals("Jay", dto.getContactName());
        Assert.assertEquals("Leader", dto.getRole());
        Assert.assertEquals("115 Swanston St", dto.getOrgStreet());
        Assert.assertEquals("Melbourne", dto.getOrgCity());
        Assert.assertEquals("VIC", dto.getOrgState());
        Assert.assertEquals("3000", dto.getOrgPostcode());
        Assert.assertEquals("test@target.com.au", dto.getEmail());
        Assert.assertEquals("95369875", dto.getPhone());
        Assert.assertEquals("reason test", dto.getReason());
        Assert.assertEquals("notes test", dto.getRequestNotes());
        Assert.assertEquals("Email", dto.getPreferredMethodOfContact());
        Assert.assertEquals("5128", dto.getStoreNumber());
        Assert.assertEquals("Local", dto.getScope());
        Assert.assertEquals("", dto.getOrgCountry());
        Assert.assertEquals("", dto.getOrgFocus());
    }

    @Ignore
    @Test
    public void testPopulateDonationRequestDtoFromFormWithTag() {

        final TargetCommunityDonationRequestDto dto = donationRequestPageHelper
                .populateDonationRequestDtoFromForm(formWithTag);
        Assert.assertEquals("&lt;html&gt;65498732112", dto.getAbnOrAcn());
        Assert.assertEquals("&lt;html&gt;Ruby Org", dto.getOrgName());
        Assert.assertEquals("&lt;html&gt;Rugby Event", dto.getEventName());
        Assert.assertEquals("&lt;html&gt;Jay", dto.getContactName());
        Assert.assertEquals("&lt;html&gt;Leader", dto.getRole());
        Assert.assertEquals("&lt;html&gt;115 Swanston St", dto.getOrgStreet());
        Assert.assertEquals("&lt;html&gt;Melbourne", dto.getOrgCity());
        Assert.assertEquals("&lt;html&gt;VIC", dto.getOrgState());
        Assert.assertEquals("3000", dto.getOrgPostcode());
        Assert.assertEquals("test@target.com.au", dto.getEmail());
        Assert.assertEquals("95369875", dto.getPhone());
        Assert.assertEquals("&lt;html&gt;reason test", dto.getReason());
        Assert.assertEquals("&lt;html&gt;notes test", dto.getRequestNotes());
        Assert.assertEquals("Email", dto.getPreferredMethodOfContact());
        Assert.assertEquals("5128", dto.getStoreNumber());
        Assert.assertEquals("Local", dto.getScope());
        Assert.assertEquals("", dto.getOrgCountry());
        Assert.assertEquals("", dto.getOrgFocus());
    }

}

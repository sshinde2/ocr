/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.endeca.infront.constants.EndecaConstants;
import au.com.target.endeca.infront.converter.AggregatedEndecaRecordToTargetProductListerConverter;
import au.com.target.endeca.infront.querybuilder.EndecaProductQueryBuilder;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.TargetProductListerData;

import com.endeca.navigation.AggrERec;
import com.endeca.navigation.AggrERecList;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.MAggrERecList;
import com.endeca.navigation.Navigation;
import com.endeca.navigation.PropertyMap;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EndecaQueryResultsHelperTest {

    @Mock
    private EndecaProductQueryBuilder endecaProductQueryBuilder;

    @Mock
    private AggregatedEndecaRecordToTargetProductListerConverter<AggrERec, TargetProductListerData> aggrERecToProductConverter;


    @Mock
    private ENEQueryResults queryResults;

    @Mock
    private AggrERec aggrERec;

    @InjectMocks
    @Spy
    private final EndecaQueryResultsHelper helper = new EndecaQueryResultsHelper();




    @Test
    public void testGetTargetProductsList() {
        final Navigation navigation = mock(Navigation.class);
        final AggrERecList aggregateERecs = mock(AggrERecList.class);
        Mockito.when(queryResults.getNavigation()).thenReturn(navigation);
        Mockito.when(navigation.getAggrERecs()).thenReturn(aggregateERecs);
        final ERec rec = mock(ERec.class);
        final PropertyMap properties = mock(PropertyMap.class);
        Mockito.when(aggregateERecs.iterator()).thenReturn(createIterator(aggrERec));
        final ERecList eRecList = mock(ERecList.class);
        Mockito.when(eRecList.iterator()).thenReturn(createIterator(rec));
        Mockito.when(aggrERec.getERecs()).thenReturn(eRecList);
        Mockito.when(rec.getProperties()).thenReturn(properties);
        Mockito.when(
                Boolean.valueOf(properties
                        .containsKey(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)))
                .thenReturn(
                        Boolean.TRUE);
        Mockito.when(properties.get(EndecaConstants.EndecaRecordSpecificFields.ENDECA_COLOUR_VARIANT_CODE)).thenReturn(
                "CP2010");

        final List<TargetProductListerData> results = helper.getTargetProductsListForColorVariant(queryResults);

        verify(queryResults, Mockito.times(2)).getNavigation();
        verify(aggrERecToProductConverter).convert(any(AggrERec.class), any(String.class));
        assertEquals(1, results.size());
    }

    @Test
    public void testGetTargetProductsListWithNull() {
        final List<TargetProductListerData> results = helper.getTargetProductList(queryResults);
        assertTrue(results.isEmpty());
    }

    @Test
    public void testConvertQueryResults() {
        final Navigation navigation = mock(Navigation.class);
        final AggrERecList aggregateERecs = new MAggrERecList();
        aggregateERecs.add(aggrERec);
        final TargetProductListerData productLister = mock(TargetProductListerData.class);
        Mockito.when(queryResults.getNavigation()).thenReturn(navigation);
        Mockito.when(navigation.getAggrERecs()).thenReturn(aggregateERecs);
        Mockito.when(aggrERecToProductConverter.convert(any(AggrERec.class))).thenReturn(productLister);
        final List<TargetProductListerData> results = helper.getTargetProductList(queryResults);
        verify(queryResults, Mockito.times(2)).getNavigation();
        verify(aggrERecToProductConverter).convert(any(AggrERec.class));
        assertEquals(1, results.size());
    }

    @Test
    public void testConvertQueryResultsWithCustomerInfos() {
        final Navigation navigation = mock(Navigation.class);
        Mockito.doReturn("P1000").when(helper).getProductCodeFromAggrERec(aggrERec);
        final Map<String, String> map = new HashMap<String, String>();
        map.put("P1000", "P1000_black");
        map.put("P1001", "P1000_black_S");
        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = new CustomerProductInfo();
        customerProductInfos.add(info);
        final AggrERecList aggregateERecs = new MAggrERecList();
        aggregateERecs.add(aggrERec);
        final TargetProductListerData productLister = mock(TargetProductListerData.class);
        Mockito.when(queryResults.getNavigation()).thenReturn(navigation);
        Mockito.when(navigation.getAggrERecs()).thenReturn(aggregateERecs);
        Mockito.when(aggrERecToProductConverter.convert(aggrERec)).thenReturn(productLister);
        final List<TargetProductListerData> results = helper.getTargetProductList(queryResults, map);
        verify(queryResults, Mockito.times(2)).getNavigation();
        verify(aggrERecToProductConverter).convert(aggrERec, "P1000_black");
        assertEquals(1, results.size());
    }

    @Test
    public void testConvertQueryResultsWithCustomerInfosSelectedVariantNull() {
        final Navigation navigation = mock(Navigation.class);
        Mockito.doReturn("P1000").when(helper).getProductCodeFromAggrERec(aggrERec);
        final Map<String, String> map = new HashMap<String, String>();
        map.put("P1000", null);
        final List<CustomerProductInfo> customerProductInfos = new ArrayList<>();
        final CustomerProductInfo info = new CustomerProductInfo();
        customerProductInfos.add(info);
        final AggrERecList aggregateERecs = new MAggrERecList();
        aggregateERecs.add(aggrERec);
        final TargetProductListerData productLister = mock(TargetProductListerData.class);
        Mockito.when(queryResults.getNavigation()).thenReturn(navigation);
        Mockito.when(navigation.getAggrERecs()).thenReturn(aggregateERecs);
        Mockito.when(aggrERecToProductConverter.convert(aggrERec, "P1000_black")).thenReturn(productLister);
        final List<TargetProductListerData> results = helper.getTargetProductList(queryResults, map);
        verify(queryResults, Mockito.times(2)).getNavigation();
        verify(aggrERecToProductConverter).convert(aggrERec);
        assertEquals(1, results.size());
    }



    @Test
    public void testConvertAggrERecToListerData() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("P1000", "P1000_black");
        map.put("P1001", "P1000_black_S");
        Mockito.doReturn("P1000").when(helper).getProductCodeFromAggrERec(aggrERec);
        helper.convertAggrERecToListerData(aggrERec, map);
        Mockito.verify(aggrERecToProductConverter).convert(aggrERec, "P1000_black");
        Mockito.verifyNoMoreInteractions(aggrERecToProductConverter);

    }

    @Test
    public void testConvertAggrERecToListerDataWithoutSelectedVariant() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("P1000", "P1000_black");
        map.put("P1001", null);
        Mockito.doReturn("P1001").when(helper).getProductCodeFromAggrERec(aggrERec);
        helper.convertAggrERecToListerData(aggrERec, map);
        Mockito.verify(aggrERecToProductConverter).convert(aggrERec);
        Mockito.verifyNoMoreInteractions(aggrERecToProductConverter);

    }

    @Test
    public void testConvertAggrERecToListerDataWithNullMap() {
        final Map<String, String> map = null;
        helper.convertAggrERecToListerData(aggrERec, map);
        Mockito.verify(aggrERecToProductConverter).convert(aggrERec);
        Mockito.verifyNoMoreInteractions(aggrERecToProductConverter);

    }


    private Iterator createIterator(final Object object) {
        final List list = new ArrayList();
        list.add(object);
        return list.iterator();
    }

}

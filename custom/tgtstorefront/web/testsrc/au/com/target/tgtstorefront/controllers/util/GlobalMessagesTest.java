/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import au.com.target.tgtstorefront.data.MessageKeyArguments;


/**
 * @author Benoit VanalderWeireldt
 * 
 */
@UnitTest
public class GlobalMessagesTest {

    @Mock
    private Model mockModel;

    private final Map<String, Object> modelMap = new HashMap<String, Object>();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(mockModel.asMap()).willReturn(modelMap);
    }

    @Test
    public void addMessageWithoutArgs() {
        GlobalMessages.addConfMessage(mockModel, "update");
    }

    @Test
    public void addMessageWithArgs() {
        GlobalMessages.addConfMessage(mockModel, "update", new Object[] { "args" });
    }

    @Test
    public void testHasConfMessages() {
        modelMap.put("accConfMsgs", Collections.singletonList(new MessageKeyArguments()));

        Assert.assertTrue(GlobalMessages.hasConfMessages(mockModel));
    }

    @Test
    public void testHasConfMessagesNoMessageContainer() {
        Assert.assertFalse(GlobalMessages.hasConfMessages(mockModel));
    }

    @Test
    public void testHasConfMessagesEmptyMessageContainer() {
        modelMap.put("accConfMsgs", new ArrayList());

        Assert.assertFalse(GlobalMessages.hasConfMessages(mockModel));
    }

    @Test
    public void testHasInfoMessages() {
        modelMap.put("accInfoMsgs", Collections.singletonList(new MessageKeyArguments()));

        Assert.assertTrue(GlobalMessages.hasInfoMessages(mockModel));
    }

    @Test
    public void testHasInfoMessagesNoMessageContainer() {
        Assert.assertFalse(GlobalMessages.hasInfoMessages(mockModel));
    }

    @Test
    public void testHasInfoMessagesEmptyMessageContainer() {
        modelMap.put("accInfoMsgs", new ArrayList());

        Assert.assertFalse(GlobalMessages.hasInfoMessages(mockModel));
    }

    @Test
    public void testHasErrorMessages() {
        modelMap.put("accErrorMsgs", Collections.singletonList(new MessageKeyArguments()));

        Assert.assertTrue(GlobalMessages.hasErrorMessages(mockModel));
    }

    @Test
    public void testHasErrorMessagesNoMessageContainer() {
        Assert.assertFalse(GlobalMessages.hasErrorMessages(mockModel));
    }

    @Test
    public void testHasErrorMessagesEmptyMessageContainer() {
        modelMap.put("accErrorMsgs", new ArrayList());

        Assert.assertFalse(GlobalMessages.hasErrorMessages(mockModel));
    }

    @Test
    public void testHasMessagesWithConfMessages() {
        modelMap.put("accConfMsgs", Collections.singletonList(new MessageKeyArguments()));

        Assert.assertTrue(GlobalMessages.hasMessages(mockModel));
    }

    @Test
    public void testHasMessagesWithInfoMessages() {
        modelMap.put("accInfoMsgs", Collections.singletonList(new MessageKeyArguments()));

        Assert.assertTrue(GlobalMessages.hasMessages(mockModel));
    }

    @Test
    public void testHasMessagesWithErrorMessages() {
        modelMap.put("accErrorMsgs", Collections.singletonList(new MessageKeyArguments()));

        Assert.assertTrue(GlobalMessages.hasMessages(mockModel));
    }

    @Test
    public void testHasMessagesWithNoMessages() {
        Assert.assertFalse(GlobalMessages.hasMessages(mockModel));
    }
}

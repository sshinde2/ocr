package au.com.target.tgtstorefront.controllers.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.product.data.ProductDisplayType;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgtstorefront.controllers.pages.data.SchemaData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductDataHelperTest {


    private static final String PRD_CODE = "P1000_red";
    @Mock
    private ProductFacade productFacade;
    @Mock
    private ProductModel productModel;
    @Mock
    private VariantProductModel variantModel1;
    @Mock
    private VariantProductModel variantModel2;
    @Mock
    private TargetProductData productData1;
    @Mock
    private TargetProductData productData2;
    @Mock
    private StockData inStockData;
    @Mock
    private StockData lowStockData;
    @Mock
    private StockData noStockData;
    @Mock
    private VariantOptionData variantOptionData1;
    @Mock
    private VariantOptionData variantOptionData2;

    @Before
    public void setUp() {
        given(productModel.getCode()).willReturn("P1000");
        given(variantModel1.getCode()).willReturn("P1000_Red");
        given(variantModel2.getCode()).willReturn("P1000_Blue");
        given(productFacade.getProductForCodeAndOptions(
                "P1000_Red",
                Arrays.asList(ProductOption.STOCK, ProductOption.VARIANT_FULL))).willReturn(productData1);
        given(productFacade.getProductForCodeAndOptions(
                "P1000_Blue",
                Arrays.asList(ProductOption.STOCK, ProductOption.VARIANT_FULL))).willReturn(productData2);
        given(productData1.getCode()).willReturn("P1000_Red");
        given(productData2.getCode()).willReturn("P1000_Blue");
        given(inStockData.getStockLevelStatus()).willReturn(StockLevelStatus.INSTOCK);
        given(lowStockData.getStockLevelStatus()).willReturn(StockLevelStatus.LOWSTOCK);
        given(noStockData.getStockLevelStatus()).willReturn(StockLevelStatus.OUTOFSTOCK);
        given(variantOptionData1.getCode()).willReturn("P1000_Red_L");
        given(variantOptionData2.getCode()).willReturn("P1000_Red_M");
        given(variantOptionData1.getStock()).willReturn(inStockData);
    }

    @Test
    public void testGetRedirectProductCode() {
        final String productCodeToRedirect = ProductDataHelper.getProductCodeToRedirect(productFacade, productModel);
        assertThat(productCodeToRedirect).isEqualTo("P1000");
    }

    @Test
    public void testGetRedirectProductCodeWithVariants() {
        given(productModel.getVariants()).willReturn(Arrays.asList(variantModel1, variantModel2));
        given(productData1.getStock()).willReturn(noStockData);
        given(productData2.getStock()).willReturn(noStockData);
        final String productCodeToRedirect = ProductDataHelper.getProductCodeToRedirect(productFacade, productModel);
        assertThat(productCodeToRedirect).isEqualTo("P1000_Red");
    }

    @Test
    public void testGetRedirectProductCodeWithVariantInStock() {
        given(productModel.getVariants()).willReturn(Arrays.asList(variantModel1, variantModel2));
        given(productData1.getStock()).willReturn(noStockData);
        given(productData2.getStock()).willReturn(inStockData);
        final String productCodeToRedirect = ProductDataHelper.getProductCodeToRedirect(productFacade, productModel);
        assertThat(productCodeToRedirect).isEqualTo("P1000_Blue");
    }

    @Test
    public void testGetRedirectProductCodeWithVariantLowStock() {
        given(productModel.getVariants()).willReturn(Arrays.asList(variantModel1, variantModel2));
        given(productData1.getStock()).willReturn(noStockData);
        given(productData2.getStock()).willReturn(lowStockData);
        final String productCodeToRedirect = ProductDataHelper.getProductCodeToRedirect(productFacade, productModel);
        assertThat(productCodeToRedirect).isEqualTo("P1000_Blue");
    }

    @Test
    public void testGetRedirectProductCodeWithSizeVariant1InStock() {
        given(productModel.getVariants()).willReturn(Arrays.asList(variantModel1, variantModel2));
        given(productData1.getVariantOptions()).willReturn(Arrays.asList(variantOptionData1, variantOptionData2));
        given(variantOptionData1.getStock()).willReturn(noStockData);
        given(variantOptionData2.getStock()).willReturn(lowStockData);
        final String productCodeToRedirect = ProductDataHelper.getProductCodeToRedirect(productFacade, productModel);
        assertThat(productCodeToRedirect).isEqualTo("P1000_Red");
    }

    @Test
    public void testGetRedirectProductCodeWithSizeVariant2LowStock() {
        given(productModel.getVariants()).willReturn(Arrays.asList(variantModel1, variantModel2));
        given(productData2.getVariantOptions()).willReturn(Arrays.asList(variantOptionData1, variantOptionData2));
        given(productData1.getStock()).willReturn(noStockData);
        given(variantOptionData1.getStock()).willReturn(null);
        given(variantOptionData2.getStock()).willReturn(lowStockData);
        final String productCodeToRedirect = ProductDataHelper.getProductCodeToRedirect(productFacade, productModel);
        assertThat(productCodeToRedirect).isEqualTo("P1000_Blue");
    }


    @Test
    public void testGetSchemaDataWithNullListerData() {
        final SchemaData schemaData = ProductDataHelper.getSchemaData(null, PRD_CODE);
        assertThat(schemaData).isNotNull();
        assertThat(schemaData.getCategoryName()).isNull();
        assertThat(schemaData.getMaxAvailQtyInStore()).isNull();
        assertThat(schemaData.getMaxAvailQtyOnline()).isNull();
        assertThat(schemaData.getPrimaryImage()).isNull();
        assertThat(schemaData.getProductCode()).isNull();



    }

    @Test
    public void testGetSchemaDataListerDataNotNull() {
        final SchemaData schemaData = ProductDataHelper.getSchemaData(populateTargetProductListerData(false, true),
                PRD_CODE);
        assertThat(schemaData).isNotNull();
        assertThat(schemaData.getPriceRange()).isNotNull();
        assertThat(schemaData.getPriceRange().getMaxPrice()).isNotNull();
        assertThat(schemaData.getPriceRange().getMinPrice()).isNotNull();
        assertThat(schemaData.getProductCode()).isEqualTo(PRD_CODE);
        assertThat(schemaData.getPriceRange().getMaxPrice().getCurrencyIso()).isEqualTo("AUD");
        assertThat(schemaData.getPriceRange().getMaxPrice().getValue()).isEqualTo(BigDecimal.valueOf(100));
        assertThat(schemaData.getPriceRange().getMinPrice().getCurrencyIso()).isEqualTo("AUD");
        assertThat(schemaData.getPriceRange().getMinPrice().getValue()).isEqualTo(BigDecimal.valueOf(10));
        assertThat(schemaData.getMaxAvailQtyInStore()).isEqualTo(199);
        assertThat(schemaData.getMaxAvailQtyOnline()).isEqualTo(10);
        assertThat(schemaData.getCategoryName()).isEqualTo("Kids Toys");
        assertThat(schemaData.getProductDisplayType()).isNull();
        assertThat(schemaData.getNormalSaleDate()).isNull();
        assertThat(schemaData.getName()).isEqualTo("Peppa Pig Scooter");


    }

    @Test
    public void testGetSchemaDataListerDataWithVariantDataAndPrice() {
        final SchemaData schemaData = ProductDataHelper.getSchemaData(populateTargetProductListerData(true, false),
                PRD_CODE);
        assertThat(schemaData).isNotNull();
        assertThat(schemaData.getPriceRange()).isNull();
        assertThat(schemaData.getProductCode()).isEqualTo(PRD_CODE);
        assertThat(schemaData.getPrice().getCurrencyIso()).isEqualTo("AUD");
        assertThat(schemaData.getPrice().getValue()).isEqualTo(BigDecimal.valueOf(10));
        assertThat(schemaData.getMaxAvailQtyInStore()).isEqualTo(199);
        assertThat(schemaData.getMaxAvailQtyOnline()).isEqualTo(10);
        assertThat(schemaData.getCategoryName()).isEqualTo("Kids Toys");
        assertThat(schemaData.getProductDisplayType()).isEqualTo(ProductDisplayType.AVAILABLE_FOR_SALE);
        assertThat(schemaData.getNormalSaleDate()).isNull();
        assertThat(schemaData.getPrimaryImage()).isEqualTo("/img/large");
        assertThat(schemaData.getName()).isEqualTo("Peppa Pig Scooter");
    }

    private TargetProductListerData populateTargetProductListerData(final boolean variants, final boolean priceRange
            ) {
        final TargetProductListerData listerData = new TargetProductListerData();
        listerData.setTopLevelCategory("Kids Toys");
        listerData.setName("Peppa Pig Scooter");
        listerData.setMaxAvailStoreQty(Integer.valueOf(199));
        listerData.setMaxAvailOnlineQty(Integer.valueOf(10));
        final PriceData price = new PriceData();
        price.setCurrencyIso("AUD");
        price.setValue(BigDecimal.TEN);
        if (priceRange) {
            final PriceRangeData range = new PriceRangeData();
            range.setMinPrice(price);
            final PriceData maxPrice = new PriceData();
            maxPrice.setCurrencyIso("AUD");
            maxPrice.setValue(BigDecimal.valueOf(100));
            range.setMaxPrice(maxPrice);
            listerData.setPriceRange(range);
        }
        else {
            listerData.setPrice(price);
        }
        if (variants) {
            final List<String> imgUrls = new ArrayList<String>();
            imgUrls.add("/img/large");
            imgUrls.add("/img/large2");
            final TargetVariantProductListerData variantData = new TargetVariantProductListerData();
            variantData.setProductDisplayType(ProductDisplayType.AVAILABLE_FOR_SALE);
            variantData.setNormalSaleStartDate(null);
            variantData.setLargeImageUrls(imgUrls);
            listerData.setTargetVariantProductListerData(Collections.singletonList(variantData));
        }

        return listerData;
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.servicelayer.session.SessionService;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import au.com.target.tgtcore.delivery.TargetDeliveryModeHelper;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.order.TargetCheckoutFacade;
import au.com.target.tgtfacades.order.data.TargetCCPaymentInfoData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.user.data.IpgPaymentInfoData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.forms.BillingAddressForm;
import au.com.target.tgtstorefront.forms.checkout.BillingAddressOnlyForm;
import au.com.target.tgtstorefront.forms.checkout.ExistingCardPaymentForm;
import au.com.target.tgtstorefront.forms.checkout.NewCardPaymentDetailsForm;
import au.com.target.tgtstorefront.forms.checkout.VendorPaymentForm;


/**
 * Test class to test TargetPaymentHelper functionality
 * 
 * @author jjayawa1
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class TargetPaymentHelperTest {

    @Mock
    private TargetCheckoutFacade checkoutFacade;

    @Mock
    private TargetCartData cartData;

    @Mock
    private TargetDeliveryModeHelper deliveryModeHelper;

    @Mock
    private Model model;

    @InjectMocks
    private final TargetPaymentHelper targetPaymentHelper = new TargetPaymentHelper();

    @Mock
    private TargetZoneDeliveryModeModel cncDeliveryModeModel;

    @Mock
    private SessionService sessionService;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Before
    public void setup() {
        Mockito.when(cncDeliveryModeModel.getIsDeliveryToStore()).thenReturn(Boolean.TRUE);
        Mockito.when(deliveryModeHelper.getDeliveryModeModel(Mockito.startsWith("click-and-collect")))
                .thenReturn(cncDeliveryModeModel);
    }

    private void setupDeliveryAddress(final TargetAddressData addressData) {
        addressData.setId("123");
        addressData.setTitleCode("Mr");
        addressData.setFirstName("firstname");
        addressData.setLastName("lastname");
        addressData.setLine1("line 1");
        addressData.setLine2("line 2");
        addressData.setTown("town");
        addressData.setPostalCode("postalCode");
        final CountryData countryData = new CountryData();
        countryData.setIsocode("AU");
        addressData.setCountry(countryData);
        addressData.setState("state");
    }

    private void setupBillingAddress(final TargetAddressData addressData) {
        addressData.setId("456");
        addressData.setTitleCode("Mr");
        addressData.setFirstName("billingFirstname");
        addressData.setLastName("billingLastname");
        addressData.setLine1("billing line 1");
        addressData.setLine2("billing line 2");
        addressData.setTown("billing town");
        addressData.setPostalCode("billing postalCode");
        final CountryData countryData = new CountryData();
        countryData.setIsocode("AU");
        addressData.setCountry(countryData);
        addressData.setState("billing state");
    }

    /**
     * If billing address is not set then delivery address should be copied to billing address for pin pad if delivery
     * mode is not click and collect.
     */
    @Ignore
    @Test
    public void testCopyDeliveryAddressToBillingAddressForPinPadWhenNotCNC() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("home-delivery");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final BillingAddressOnlyForm billingAddressOnlyForm = new BillingAddressOnlyForm();
        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);

        final BillingAddressForm billingAddressForm = billingAddressOnlyForm.getBillingAddress();

        Assert.assertNotNull(billingAddressForm);
        Assert.assertEquals("123", billingAddressForm.getAddressId());
        Assert.assertEquals("Mr", billingAddressForm.getTitleCode());
        Assert.assertEquals("firstname", billingAddressForm.getFirstName());
        Assert.assertEquals("lastname", billingAddressForm.getLastName());
        Assert.assertEquals("line 1", billingAddressForm.getLine1());
        Assert.assertEquals("line 2", billingAddressForm.getLine2());
        Assert.assertEquals("town", billingAddressForm.getTownCity());
        Assert.assertEquals("postalCode", billingAddressForm.getPostcode());
        Assert.assertEquals("AU", billingAddressForm.getCountryIso());
        Assert.assertEquals("state", billingAddressForm.getState());
    }

    /**
     * If billing address is not set then delivery address should not be copied to billing address for pin pad if
     * delivery mode is click and collect.
     */
    @Test
    public void testCopyDeliveryAddressToBillingAddressForPinPadWhenCNC() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final BillingAddressOnlyForm billingAddressOnlyForm = new BillingAddressOnlyForm();

        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);

        final BillingAddressForm billingAddressForm = billingAddressOnlyForm.getBillingAddress();

        Assert.assertNull(billingAddressForm);
    }

    /**
     * If billing address is not set then delivery address should be copied to billing address for credit card if
     * delivery mode is not click and collect.
     */
    @Ignore
    @Test
    public void testCopyDeliveryAddressToBillingAddressForCreditCardWhenNotCNC() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("home-delivery");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final NewCardPaymentDetailsForm newCardPaymentDetailsForm = new NewCardPaymentDetailsForm();
        final ExistingCardPaymentForm existingCardPaymentForm = new ExistingCardPaymentForm();
        final VendorPaymentForm vendorPaymentForm = new VendorPaymentForm();

        targetPaymentHelper.populatePaymentDefaultForms(model, existingCardPaymentForm, vendorPaymentForm,
                newCardPaymentDetailsForm, cartData, null);

        final BillingAddressForm billingAddressForm = newCardPaymentDetailsForm.getBillingAddress();

        Assert.assertNotNull(billingAddressForm);
        Assert.assertEquals("123", billingAddressForm.getAddressId());
        Assert.assertEquals("Mr", billingAddressForm.getTitleCode());
        Assert.assertEquals("firstname", billingAddressForm.getFirstName());
        Assert.assertEquals("lastname", billingAddressForm.getLastName());
        Assert.assertEquals("line 1", billingAddressForm.getLine1());
        Assert.assertEquals("line 2", billingAddressForm.getLine2());
        Assert.assertEquals("town", billingAddressForm.getTownCity());
        Assert.assertEquals("postalCode", billingAddressForm.getPostcode());
        Assert.assertEquals("AU", billingAddressForm.getCountryIso());
        Assert.assertEquals("state", billingAddressForm.getState());
    }

    /**
     * If billing address is not set then delivery address should not be copied to billing address for credit card if
     * delivery mode is click and collect.
     */
    @Test
    public void testCopyDeliveryAddressToBillingAddressForCreditCardWhenCNC() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final NewCardPaymentDetailsForm newCardPaymentDetailsForm = new NewCardPaymentDetailsForm();
        final ExistingCardPaymentForm existingCardPaymentForm = new ExistingCardPaymentForm();
        final VendorPaymentForm vendorPaymentForm = new VendorPaymentForm();

        targetPaymentHelper.populatePaymentDefaultForms(model, existingCardPaymentForm, vendorPaymentForm,
                newCardPaymentDetailsForm, cartData, null);

        final BillingAddressForm billingAddressForm = newCardPaymentDetailsForm.getBillingAddress();

        Assert.assertNull(billingAddressForm);
    }

    /**
     * If billing address is set then delivery address should not get copied to billing address for credit card.
     */
    @Ignore
    @Test
    public void testCopyDeliveryAddressToBillingAddressForCreditCardIfBillingAddressSet() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("home-delivery");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        final TargetAddressData billingAddress = new TargetAddressData();
        setupBillingAddress(billingAddress);
        final TargetCCPaymentInfoData paymentInfo = new TargetCCPaymentInfoData();
        paymentInfo.setBillingAddress(billingAddress);

        Mockito.when(cartData.getPaymentInfo()).thenReturn(paymentInfo);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final NewCardPaymentDetailsForm newCardPaymentDetailsForm = new NewCardPaymentDetailsForm();
        final ExistingCardPaymentForm existingCardPaymentForm = new ExistingCardPaymentForm();
        final VendorPaymentForm vendorPaymentForm = new VendorPaymentForm();

        targetPaymentHelper.populatePaymentDefaultForms(model, existingCardPaymentForm, vendorPaymentForm,
                newCardPaymentDetailsForm, cartData, null);

        final BillingAddressForm billingAddressForm = newCardPaymentDetailsForm.getBillingAddress();

        Assert.assertNotNull(billingAddressForm);
        Assert.assertEquals("456", billingAddressForm.getAddressId());
        Assert.assertEquals("Mr", billingAddressForm.getTitleCode());
        Assert.assertEquals("billingFirstname", billingAddressForm.getFirstName());
        Assert.assertEquals("billingLastname", billingAddressForm.getLastName());
        Assert.assertEquals("billing line 1", billingAddressForm.getLine1());
        Assert.assertEquals("billing line 2", billingAddressForm.getLine2());
        Assert.assertEquals("billing town", billingAddressForm.getTownCity());
        Assert.assertEquals("billing postalCode", billingAddressForm.getPostcode());
        Assert.assertEquals("AU", billingAddressForm.getCountryIso());
        Assert.assertEquals("billing state", billingAddressForm.getState());
    }

    /**
     * If billing address is set then delivery address should not get copied to billing address for pin pad.
     */
    @Ignore
    @Test
    public void testCopyDeliveryAddressToBillingAddressForPinPadIfBillingAddressSet() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("home-delivery");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        final TargetAddressData billingAddress = new TargetAddressData();
        setupBillingAddress(billingAddress);
        final TargetCCPaymentInfoData paymentInfo = new TargetCCPaymentInfoData();
        paymentInfo.setBillingAddress(billingAddress);

        Mockito.when(cartData.getPaymentInfo()).thenReturn(paymentInfo);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final BillingAddressOnlyForm billingAddressOnlyForm = new BillingAddressOnlyForm();

        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);

        final BillingAddressForm billingAddressForm = billingAddressOnlyForm.getBillingAddress();

        Assert.assertNotNull(billingAddressForm);
        Assert.assertEquals("456", billingAddressForm.getAddressId());
        Assert.assertEquals("Mr", billingAddressForm.getTitleCode());
        Assert.assertEquals("billingFirstname", billingAddressForm.getFirstName());
        Assert.assertEquals("billingLastname", billingAddressForm.getLastName());
        Assert.assertEquals("billing line 1", billingAddressForm.getLine1());
        Assert.assertEquals("billing line 2", billingAddressForm.getLine2());
        Assert.assertEquals("billing town", billingAddressForm.getTownCity());
        Assert.assertEquals("billing postalCode", billingAddressForm.getPostcode());
        Assert.assertEquals("AU", billingAddressForm.getCountryIso());
        Assert.assertEquals("billing state", billingAddressForm.getState());
    }

    @Test
    public void testPopulatePaymentDefaultFormsWhenTNSDisabled() {
        mockFormData();
        final NewCardPaymentDetailsForm newCardPaymentDetailsForm = new NewCardPaymentDetailsForm();
        Mockito.when(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(Mockito.anyString()))).thenReturn(
                Boolean.FALSE);
        targetPaymentHelper.populatePaymentDefaultForms(model, null, null,
                newCardPaymentDetailsForm, cartData, null);

        Assert.assertEquals(false, newCardPaymentDetailsForm.isEnabled());
    }

    @Test
    public void testPopulatePaymentDefaultFormsWhenTNSEnabled() {
        mockFormData();
        final NewCardPaymentDetailsForm newCardPaymentDetailsForm = new NewCardPaymentDetailsForm();
        Mockito.when(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(Mockito.anyString()))).thenReturn(
                Boolean.TRUE);
        targetPaymentHelper.populatePaymentDefaultForms(model, null, null,
                newCardPaymentDetailsForm, cartData, null);

        Assert.assertEquals(true, newCardPaymentDetailsForm.isEnabled());
    }

    @Test
    public void testPopulatePaymentDefaultFormsWhenPaypalDisabled() {
        mockFormData();
        final VendorPaymentForm vendorPaymentForm = new VendorPaymentForm();
        Mockito.when(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(Mockito.anyString()))).thenReturn(
                Boolean.FALSE);
        targetPaymentHelper.populatePaymentDefaultForms(model, null, vendorPaymentForm,
                null, cartData, null);

        Assert.assertEquals(false, vendorPaymentForm.isEnabled());
    }

    protected void mockFormData() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        final TargetAddressData billingAddress = new TargetAddressData();
        setupBillingAddress(billingAddress);
        final TargetCCPaymentInfoData paymentInfo = new TargetCCPaymentInfoData();
        paymentInfo.setBillingAddress(billingAddress);

        Mockito.when(cartData.getPaymentInfo()).thenReturn(paymentInfo);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);
        Mockito.when(deliveryModeHelper.getDeliveryModeModel(Mockito.startsWith("click-and-collect")))
                .thenReturn(cncDeliveryModeModel);
    }

    @Test
    public void testPopulatePaymentDefaultFormsWhenPaypalEnabled() {

        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);
        final VendorPaymentForm vendorPaymentForm = new VendorPaymentForm();
        Mockito.when(Boolean.valueOf(targetFeatureSwitchFacade.isFeatureEnabled(Mockito.anyString()))).thenReturn(
                Boolean.TRUE);
        targetPaymentHelper.populatePaymentDefaultForms(model, null, vendorPaymentForm,
                null, cartData, null);

        Assert.assertEquals(true, vendorPaymentForm.isEnabled());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCheckIPGFeatureWithModel() {
        when(targetFeatureSwitchFacade.isFeatureEnabled("payment.ipg")).thenReturn(true);
        assertTrue(targetPaymentHelper.checkIfIPGFeatureIsEnabled());
    }

    /**
     * If billing address is not set then delivery address should not be copied to billing address for pin pad if
     * delivery mode is click and collect.
     */
    @Test
    public void testPopulateBillingAddressDetailsFormForGiftcard() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final TargetCCPaymentInfoData targetCCPaymentInfoData = new TargetCCPaymentInfoData();
        final IpgPaymentInfoData ipgPaymentInfoData = new IpgPaymentInfoData();
        ipgPaymentInfoData.setIpgPaymentTemplateType(IpgPaymentTemplateType.GIFTCARDMULTIPLE);
        targetCCPaymentInfoData.setIpgPaymentInfoData(ipgPaymentInfoData);
        Mockito.when(cartData.getPaymentInfo()).thenReturn(targetCCPaymentInfoData);

        final BillingAddressOnlyForm billingAddressOnlyForm = new BillingAddressOnlyForm(
                ControllerConstants.PAYMENT
                        + ControllerConstants.PAYMENT_GIFT_CARD_BILLING, TgtFacadesConstants.GIFT_CARD);

        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);

        Assert.assertEquals(true, billingAddressOnlyForm.isActive());
    }

    @Test
    public void testPopulateBillingAddressDetailsFormForGiftcardWithoutActive() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final TargetCCPaymentInfoData targetCCPaymentInfoData = new TargetCCPaymentInfoData();
        final IpgPaymentInfoData ipgPaymentInfoData = new IpgPaymentInfoData();
        ipgPaymentInfoData.setIpgPaymentTemplateType(IpgPaymentTemplateType.CREDITCARDSINGLE);
        targetCCPaymentInfoData.setIpgPaymentInfoData(ipgPaymentInfoData);
        Mockito.when(cartData.getPaymentInfo()).thenReturn(targetCCPaymentInfoData);

        final BillingAddressOnlyForm billingAddressOnlyForm = new BillingAddressOnlyForm(
                ControllerConstants.PAYMENT
                        + ControllerConstants.PAYMENT_GIFT_CARD_BILLING, TgtFacadesConstants.GIFT_CARD);

        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);

        Assert.assertEquals(false, billingAddressOnlyForm.isActive());
    }

    @Test
    public void testPopulateBillingAddressDetailsFormForCreditcardActive() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final TargetCCPaymentInfoData targetCCPaymentInfoData = new TargetCCPaymentInfoData();
        final IpgPaymentInfoData ipgPaymentInfoData = new IpgPaymentInfoData();
        ipgPaymentInfoData.setIpgPaymentTemplateType(IpgPaymentTemplateType.CREDITCARDSINGLE);
        targetCCPaymentInfoData.setIpgPaymentInfoData(ipgPaymentInfoData);
        Mockito.when(cartData.getPaymentInfo()).thenReturn(targetCCPaymentInfoData);

        final BillingAddressOnlyForm billingAddressOnlyForm = new BillingAddressOnlyForm(
                ControllerConstants.PAYMENT
                        + ControllerConstants.PAYMENT_BILLING, TgtFacadesConstants.CREDIT_CARD);

        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);

        Assert.assertEquals(true, billingAddressOnlyForm.isActive());
    }

    @Test
    public void testPopulateBillingAddressDetailsFormForCreditcardWithoutActive() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final TargetCCPaymentInfoData targetCCPaymentInfoData = new TargetCCPaymentInfoData();
        final IpgPaymentInfoData ipgPaymentInfoData = new IpgPaymentInfoData();
        ipgPaymentInfoData.setIpgPaymentTemplateType(IpgPaymentTemplateType.GIFTCARDMULTIPLE);
        targetCCPaymentInfoData.setIpgPaymentInfoData(ipgPaymentInfoData);
        Mockito.when(cartData.getPaymentInfo()).thenReturn(targetCCPaymentInfoData);

        final BillingAddressOnlyForm billingAddressOnlyForm = new BillingAddressOnlyForm(
                ControllerConstants.PAYMENT
                        + ControllerConstants.PAYMENT_BILLING, TgtFacadesConstants.CREDIT_CARD);

        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);

        Assert.assertEquals(false, billingAddressOnlyForm.isActive());
    }

    @Test
    public void testPopulateBillingAddressDetailsFormForCreditcardWithNullIpgPaymentTemplateType() {
        final DeliveryModeData deliveryModeData = new DeliveryModeData();
        deliveryModeData.setCode("click-and-collect");
        final TargetAddressData deliveryAddress = new TargetAddressData();
        setupDeliveryAddress(deliveryAddress);
        Mockito.when(cartData.getDeliveryMode()).thenReturn(deliveryModeData);
        Mockito.when(cartData.getDeliveryAddress()).thenReturn(deliveryAddress);

        final TargetCCPaymentInfoData targetCCPaymentInfoData = new TargetCCPaymentInfoData();
        final IpgPaymentInfoData ipgPaymentInfoData = new IpgPaymentInfoData();
        ipgPaymentInfoData.setIpgPaymentTemplateType(null);
        targetCCPaymentInfoData.setIpgPaymentInfoData(ipgPaymentInfoData);
        Mockito.when(cartData.getPaymentInfo()).thenReturn(targetCCPaymentInfoData);

        final BillingAddressOnlyForm billingAddressOnlyForm = new BillingAddressOnlyForm(
                ControllerConstants.PAYMENT
                        + ControllerConstants.PAYMENT_BILLING, TgtFacadesConstants.CREDIT_CARD);

        targetPaymentHelper.populateBillingAddressDetailsForm(model, billingAddressOnlyForm, cartData, null);

        Assert.assertEquals(false, billingAddressOnlyForm.isActive());
    }

}

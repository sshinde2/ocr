/**
 * 
 */
package au.com.target.tgtstorefront.controllers.util;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Unit test for {@link ThreePartDateHelper}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ThreePartDateHelperTest {

    private static final String MONTH = "3";
    private static final String YEAR = "2000";
    private static final String DAY = "31";

    @Test
    public void testNullDay() {
        assertThat(ThreePartDateHelper.convertToDate(null, MONTH, YEAR)).isNull();
    }

    @Test
    public void testNullMonth() {
        assertThat(ThreePartDateHelper.convertToDate(DAY, null, YEAR)).isNull();
    }

    @Test
    public void testNullYear() {
        assertThat(ThreePartDateHelper.convertToDate(DAY, MONTH, null)).isNull();
    }

    @Test
    public void testNonIntDay() {
        assertThat(ThreePartDateHelper.convertToDate("x", MONTH, YEAR)).isNull();
    }

    @Test
    public void testNonIntMonth() {
        assertThat(ThreePartDateHelper.convertToDate(DAY, "x", YEAR)).isNull();
    }

    @Test
    public void testNonIntYear() {
        assertThat(ThreePartDateHelper.convertToDate(DAY, MONTH, "x")).isNull();
    }

    @Test
    public void testValidDate() {
        assertThat(ThreePartDateHelper.convertToDate(DAY, MONTH, YEAR)).isNotNull();
    }

    @Test
    public void testInvalidDate() {
        assertThat(ThreePartDateHelper.convertToDate("31", "02", "2000")).isNull();
    }
}

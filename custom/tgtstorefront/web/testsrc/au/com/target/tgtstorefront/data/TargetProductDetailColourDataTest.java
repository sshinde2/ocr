/**
 * 
 */
package au.com.target.tgtstorefront.data;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductDetailColourDataTest {
    @Mock
    private VariantOptionData variantOptionData;

    @Mock
    private VariantOptionQualifierData colour;

    @Mock
    private VariantOptionQualifierData swatch;

    @Mock
    private VariantOptionQualifierData colourName;

    @Before
    public void setUp() {
        given(colour.getQualifier()).willReturn("colour");
        given(colourName.getQualifier()).willReturn("colourName");
        given(swatch.getQualifier()).willReturn("swatch");
        given(variantOptionData.getVariantOptionQualifiers())
                .willReturn(Arrays.asList(colour, swatch, colourName));

    }

    @Test
    public void shouldSetColourImageUrlIfItExistsDuringCreation() {
        final ImageData gridImage = mock(ImageData.class);
        given(gridImage.getUrl()).willReturn("gridImageUrl");
        given(colour.getImage()).willReturn(gridImage);
        final TargetProductDetailColourData colourData = new TargetProductDetailColourData(variantOptionData);
        assertThat(colourData.getColourImageUrl()).isEqualTo("gridImageUrl");
    }

    @Test
    public void noColourImageUrlDuringCreation() {
        given(colour.getImage()).willReturn(null);
        final TargetProductDetailColourData colourData = new TargetProductDetailColourData(variantOptionData);
        assertThat(colourData.getColourImageUrl()).isNull();
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.data;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;

import org.junit.Test;


@UnitTest
public class TargetProductGenericDataTest {

    @Test
    public void shouldPopulateCodeDuringCreation() {
        final VariantOptionData variantOptionData = mock(VariantOptionData.class);
        given(variantOptionData.getCode()).willReturn("code");
        final TargetProductGenericData data = new TargetProductGenericData(variantOptionData);
        assertThat(data.getCode()).isEqualTo("code");
    }

    @Test
    public void shouldPopulateOutOfStockDuringCreation() {
        final VariantOptionData variantOptionData = mock(VariantOptionData.class);
        final TargetProductGenericData data = new TargetProductGenericData(variantOptionData);
        assertThat(data.isOutOfStock()).isTrue();
    }
}

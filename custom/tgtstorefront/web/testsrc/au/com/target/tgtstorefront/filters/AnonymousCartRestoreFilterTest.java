/**
 * 
 */
package au.com.target.tgtstorefront.filters;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.order.TargetCartFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.user.TargetUserFacade;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.util.CookieRetrievalHelper;


/**
 * @author Pradeep
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AnonymousCartRestoreFilterTest {

    private static final String TARGET_ANONYMOUS_TOKEN = "targetAnonymousToken";

    private static final String DUMMY_GUID = "dummyGuid";

    @InjectMocks
    protected final AnonymousCartRestoreFilter filter = new AnonymousCartRestoreFilter();

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain; //NOPMD

    @Mock
    private CartModel cartModel;

    @Mock
    private TargetUserFacade targetUserFacade;
    @Mock
    private TargetCartFacade mockTargetCartFacade;
    @Mock
    private SessionService sessionService;
    @Mock
    private CookieRetrievalHelper cookieRetrievalHelper;

    @Mock
    private SalesApplicationFacade salesApplicationFacade;

    private List<SalesApplication> excludedSalesAppsToRestoreCart;

    @Test
    public void testForAnonymousCartRetainWithValidSession() throws IOException, ServletException
    {
        given(sessionService.getAttribute(WebConstants.SESSION_CART_KEY)).willReturn(cartModel);
        filter.doFilterInternal(request, response, filterChain);
        Mockito.verifyZeroInteractions(mockTargetCartFacade);
    }


    @Test
    public void testForAnonymousCartRetainWithNonAnonymousUser() throws IOException, ServletException,
            CommerceCartRestorationException
    {
        given(sessionService.getAttribute(WebConstants.SESSION_CART_KEY)).willReturn(null);
        given(Boolean.valueOf(targetUserFacade.isCurrentUserAnonymous())).willReturn(Boolean.FALSE);
        filter.doFilterInternal(request, response, filterChain);
        Mockito.verifyZeroInteractions(mockTargetCartFacade);
    }


    @Test
    public void testForAnonymousCartRetainWithAnonymousUser() throws IOException, ServletException,
            CommerceCartRestorationException
    {
        setupCookieData();
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.WEB);
        filter.doFilterInternal(request, response, filterChain);
        Mockito.verify(mockTargetCartFacade).restoreCart(DUMMY_GUID);
    }

    @Test
    public void testForAnonymousCartRetainWithAnonymousUserIfKioskApp() throws IOException, ServletException,
            CommerceCartRestorationException
    {
        setupCookieData();
        given(salesApplicationFacade.getCurrentSalesApplication()).willReturn(SalesApplication.KIOSK);
        filter.doFilterInternal(request, response, filterChain);
        Mockito.verifyZeroInteractions(mockTargetCartFacade);
    }


    protected void setupCookieData() {
        final Cookie cookie = new Cookie(TARGET_ANONYMOUS_TOKEN, DUMMY_GUID);
        final Cookie[] cookies = { cookie };
        filter.setCookieName(TARGET_ANONYMOUS_TOKEN);
        given(request.getCookies()).willReturn(cookies);
        given(sessionService.getAttribute(WebConstants.SESSION_CART_KEY)).willReturn(null);
        given(Boolean.valueOf(targetUserFacade.isCurrentUserAnonymous())).willReturn(Boolean.TRUE);
        given(cookieRetrievalHelper.getCookieValue(request, TARGET_ANONYMOUS_TOKEN)).willReturn(DUMMY_GUID);
        excludedSalesAppsToRestoreCart = new ArrayList<>();
        excludedSalesAppsToRestoreCart.add(SalesApplication.KIOSK);
        filter.setExcludedSalesAppsToRestoreCart(excludedSalesAppsToRestoreCart);
    }



}

package au.com.target.tgtstorefront.forms.validation.validator;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;


public class AbnAcnValidatorTest {

    private static final String[] VALID_ACNS = { "000000019", "000250000", "000500005", "000750005", "001000004",
            "001250004", "001500009", "001749999", "001999999", "002249998", "002499998", "002749993", "002999993",
            "003249992", "003499992", "003749988", "003999988", "004249987", "004499987", "004749982", "004999982",
            "005249981", "005499981", "005749986", "005999977", "006249976", "006499976", "006749980", "006999980",
            "007249989", "007499989", "007749975", "007999975", "008249974", "008499974", "008749979", "008999979",
            "009249969", "009499969", "009749964", "009999964", "010249966", "010499966", "010749961" };

    private static final String[] VALID_ABNS = { "75004250944", "28008984049", "45004189708", "73004700485",
            "36004763526" };

    private final AbnAcnValidator abnAcnValidator = new AbnAcnValidator();

    @Test
    public void testIsAvailableWithValidACNs() {
        for (final String validAcn : VALID_ACNS) {
            final boolean result = abnAcnValidator.isAvailable(validAcn);
            assertThat(result).isTrue();
        }
    }

    @Test
    public void testIsAvailableWithValidABNs() {
        for (final String validAcn : VALID_ABNS) {
            final boolean result = abnAcnValidator.isAvailable(validAcn);
            assertThat(result).isTrue();
        }
    }

    @Test
    public void testIsAvailableWithValueTooShort() {
        final boolean result = abnAcnValidator.isAvailable("01024996");
        assertThat(result).isFalse();
    }

    @Test
    public void testIsAvailableWithValueTooLong() {
        final boolean result = abnAcnValidator.isAvailable("450041897080");
        assertThat(result).isFalse();
    }

    @Test
    public void testIsAvailableWithValueLengthInBetween() {
        final boolean result = abnAcnValidator.isAvailable("4500418970");
        assertThat(result).isFalse();
    }

    @Test
    public void testIsAvailableWithInvalidACN() {
        final boolean result = abnAcnValidator.isAvailable("009599969");
        assertThat(result).isFalse();
    }

    @Test
    public void testIsAvailableWithInvalidABN() {
        final boolean result = abnAcnValidator.isAvailable("73004700484");
        assertThat(result).isFalse();
    }
}

/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.DonationPreferredContactMethod;


/**
 * @author bhuang3
 * 
 */
@UnitTest
public class DonationPreferredContactMethodValidatorTest {

    @Mock
    private DonationPreferredContactMethod donationPreferredContactMethod;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @InjectMocks
    private final DonationPreferredContactMethodValidator donationPreferredContactMethodValidator = new DonationPreferredContactMethodValidator() {
        {
            final MessageSource messageSourceMocked = BDDMockito.mock(MessageSource.class);
            this.messageSource = messageSourceMocked;
            final I18NService i18nServiceMocked = BDDMockito.mock(I18NService.class);
            this.i18nService = i18nServiceMocked;
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);
        donationPreferredContactMethodValidator.initialize(donationPreferredContactMethod);
    }

    @Test
    public void correctContactMethod() {
        Assert.assertTrue(donationPreferredContactMethodValidator.isValid("Email", constraintValidatorContext));
        Assert.assertTrue(donationPreferredContactMethodValidator.isValid("Phone", constraintValidatorContext));
        Assert.assertTrue(donationPreferredContactMethodValidator.isValid("Mail", constraintValidatorContext));
    }

    @Test
    public void incorrectContactMethod() {
        Assert.assertFalse(donationPreferredContactMethodValidator.isValid("Local", constraintValidatorContext));
        Assert.assertFalse(donationPreferredContactMethodValidator.isValid("Fax", constraintValidatorContext));
        Assert.assertFalse(donationPreferredContactMethodValidator.isValid("", constraintValidatorContext));
    }



}

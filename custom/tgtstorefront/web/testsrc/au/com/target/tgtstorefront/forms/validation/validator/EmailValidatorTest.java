/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.Email;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@UnitTest
public class EmailValidatorTest {

    @Mock
    private Email email;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @InjectMocks
    private final EmailValidator emailValidator = new EmailValidator() {
        {
            final MessageSource messageSourceMocked = BDDMockito.mock(MessageSource.class);
            this.messageSource = messageSourceMocked;
            final I18NService i18nServiceMocked = BDDMockito.mock(I18NService.class);
            this.i18nService = i18nServiceMocked;
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);

        BDDMockito.given(email.field()).willReturn(FieldTypeEnum.email);
        emailValidator.initialize(email);
    }

    @Test
    public void correctEmails() {

        Assert.assertTrue(emailValidator.isValid("a@aa.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("ab@aa.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("abC@aa.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("a.b@aa.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("target@aa.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("target@ge.com.au", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("target@ge.com", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("target@g.com", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("target@acom.au.au.aaa", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("target@a888.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("target@TEST8-8.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("jean-michel@laboiteatestdu45.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("target@target.com.au", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("jack@aus.com.au", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("MAXIMELEGRAND'ROIDUCODE@MARTINGMATGNON.COM",
                constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("eclispeIndi{notthebestbytheway}+remembermetoleaveit@doe.fr",
                constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid(
                "eclispeIndi.notthebestbytheway'+remembermetoleaveit@whatisthemaximumsizehereIhavenoidea.fr",
                constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid(
                "johnisaeallybignamebutsincethelimitmightbe64caractersitshouldbe@doe.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("bruno@hotmail.fr", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("niceandsimple@example.com", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("very.common@example.com", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("a.little.lengthy.but.fine@dept.example.com",
                constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("disposable.style.email.with+symbol@example.com",
                constraintValidatorContext));
        Assert.assertTrue(emailValidator
                .isValid("user@[ipv6:2001:db8:1ff::a0b:dbd0]", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("user@[IPv634:2001:db8:1ff:7s:a0b:dbd0]",
                constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("user@[10.192.27.2]", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("user@[0.0.0.0]", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("much.moreunusual@example.com", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("very.unusua@unusual.comexample.com",
                constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("very.unusual@strange.example.com", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("postbox@com.autyyyyyyy", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("admin@mailserver1.com", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("!#$%&'*+-/=?^_`{}|~@example.org", constraintValidatorContext));
        Assert.assertTrue(emailValidator
                .isValid(";!#$%&'*+-=?^_`{}|~a@example.org", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("CanTBEEXMP@example.org", constraintValidatorContext));
        Assert.assertTrue(emailValidator
                .isValid(
                        "musthaveaname@qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm.com",
                        constraintValidatorContext));//234 cara hostname
        Assert.assertTrue(emailValidator
                .isValid(
                        "emptyisnotgood@qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm1234567890123456789qq.ai",
                        constraintValidatorContext));//255 cara hostname
        Assert.assertTrue(emailValidator
                .isValid(
                        "qwertyuiop12345678901234567890123456789012345678901234567890qwer@qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm1234567890123456789qq.org",
                        constraintValidatorContext));//64 local + 255 cara hostname
        Assert.assertTrue(emailValidator.isValid("jean-Francoise@gmail.com.12", constraintValidatorContext));
        Assert.assertTrue(emailValidator.isValid("Martib@1example.org", constraintValidatorContext));
    }

    @Test
    public void incorrectEmails() {
        Assert.assertFalse(emailValidator.isValid("very;.(),:;<>[].VERY.very@", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid("\\;very.unusual@strange.example.com",
                constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid("()<>[]:,;@!#$%&'*+-=?^_`{}| ~.a@example.org",
                constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid(
                "johnisaeallybignamebutsincethelimitmightbe64caractersitshouldbenotallright@doe.fr",
                constraintValidatorContext));
        Assert.assertFalse(emailValidator
                .isValid("user@IPv6:2001:db8:1ff::a0b:dbd0]", constraintValidatorContext));

        Assert.assertFalse(emailValidator.isValid("Martib@ex-ample.org-", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid(" @example.org", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid(".martin@example.org", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid("martin.@example.org", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid("martin@exampleorg", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid(".@exampleorg", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid("\u2000@exampleorg", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid("@exampleorg", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid("martin.@asd@example.org", constraintValidatorContext));
        Assert.assertFalse(emailValidator.isValid("martin@example@.org", constraintValidatorContext));
        Assert.assertFalse(emailValidator
                .isValid(
                        "qwertyuiop12345678901234567890123456789012345678901234567890qwera@qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm1234567890123456789qq",
                        constraintValidatorContext));//65 local + 255 cara hostname
        Assert.assertFalse(emailValidator
                .isValid(
                        "qwertyuiop12345678901234567890123456789012345678901234567890qwer@Aqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnm1234567890123456789qq",
                        constraintValidatorContext));//64 local + 256 cara hostname
    }
}

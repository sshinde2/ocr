/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.ExpectedGender;


/**
 * @author bhuang3
 * 
 */
public class ExpectedGenderValidatorTest {

    @Mock
    private ExpectedGender expectedGender;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @InjectMocks
    private final ExpectedGenderValidator expectedGenderValidator = new ExpectedGenderValidator() {
        {
            final MessageSource messageSourceMocked = BDDMockito.mock(MessageSource.class);
            this.messageSource = messageSourceMocked;
            final I18NService i18nServiceMocked = BDDMockito.mock(I18NService.class);
            this.i18nService = i18nServiceMocked;
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);
        expectedGenderValidator.initialize(expectedGender);
    }

    @Test
    public void correctGender() {
        Assert.assertTrue(expectedGenderValidator.isAvailable("M"));
        Assert.assertTrue(expectedGenderValidator.isAvailable("m"));
        Assert.assertTrue(expectedGenderValidator.isAvailable("F"));
        Assert.assertTrue(expectedGenderValidator.isAvailable("f"));
        Assert.assertTrue(expectedGenderValidator.isAvailable("S"));
        Assert.assertTrue(expectedGenderValidator.isAvailable("s"));
        Assert.assertTrue(expectedGenderValidator.isAvailable("T"));
        Assert.assertTrue(expectedGenderValidator.isAvailable("t"));
    }

    @Test
    public void incorrectGender() {
        Assert.assertFalse(expectedGenderValidator.isAvailable("anyOther"));
        Assert.assertFalse(expectedGenderValidator.isAvailable(""));
        Assert.assertFalse(expectedGenderValidator.isAvailable(null));
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;


/**
 * @author bhuang3
 * 
 */
public class GenderValidatorTest {

    @Mock
    private au.com.target.tgtstorefront.forms.validation.Gender gender;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @InjectMocks
    private final GenderValidator genderValidator = new GenderValidator() {
        {
            final MessageSource messageSourceMocked = BDDMockito.mock(MessageSource.class);
            this.messageSource = messageSourceMocked;
            final I18NService i18nServiceMocked = BDDMockito.mock(I18NService.class);
            this.i18nService = i18nServiceMocked;
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .willReturn(constraintViolationBuilder);
        genderValidator.initialize(gender);
    }

    @Test
    public void correctGender() {
        Assert.assertTrue(genderValidator.isAvailable("M"));
        Assert.assertTrue(genderValidator.isAvailable("m"));
        Assert.assertTrue(genderValidator.isAvailable("F"));
        Assert.assertTrue(genderValidator.isAvailable("f"));
    }

    @Test
    public void incorrectGender() {
        Assert.assertFalse(genderValidator.isAvailable("anyOther"));
        Assert.assertFalse(genderValidator.isAvailable(""));
        Assert.assertFalse(genderValidator.isAvailable(null));
    }

}

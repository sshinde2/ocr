/**
 * 
 */
package au.com.target.tgtstorefront.forms.validation.validator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.i18n.I18NService;

import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

import au.com.target.tgtstorefront.forms.validation.MessageText;


/**
 * @author sbryan6
 *
 */
@UnitTest
public class MessageTextValidatorTest {

    @Mock
    private MessageText messageText;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Mock
    private ConstraintViolationBuilder constraintViolationBuilder;

    @InjectMocks
    private final MessageTextValidator messageTextValidator = new MessageTextValidator() {
        {
            final MessageSource messageSourceMocked = BDDMockito.mock(MessageSource.class);
            this.messageSource = messageSourceMocked;
            final I18NService i18nServiceMocked = BDDMockito.mock(I18NService.class);
            this.i18nService = i18nServiceMocked;
        }
    };

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(constraintValidatorContext.buildConstraintViolationWithTemplate(BDDMockito.anyString()))
                .thenReturn(constraintViolationBuilder);

        Mockito.when(messageText.field()).thenReturn(FieldTypeEnum.messageText);
        messageTextValidator.initialize(messageText);
    }

    @Test
    public void testValidMessages() {

        Assert.assertTrue(messageTextValidator.isValid("Hello hope you like the gift card", constraintValidatorContext));
        Assert.assertTrue(messageTextValidator.isValid("Hi", constraintValidatorContext));

        // 150 chars
        Assert.assertTrue(messageTextValidator
                .isValid(
                        "Have a nice day Have a nice day Have a nice day Have a nice day Have a nice day Have a nice day Have a nice day Have a ",
                        constraintValidatorContext));

        Assert.assertTrue(messageTextValidator.isValid("adfadfadsfdfasdfasdfsadfasdfaerwerwsfsdfdsfad\r\nfadsfasdfwer"
                + "\r\ntsdgasdfas\r\ndfasdfasdfasdf\r\nasdfcvefrwerdsfsf\r\nasdfsadfawerwerfd",
                constraintValidatorContext));

        // Not mandatory by default
        Assert.assertTrue(messageTextValidator.isValid(null, constraintValidatorContext));
        Assert.assertTrue(messageTextValidator.isValid("", constraintValidatorContext));

    }

    @Test
    public void testInvalidMessages() {

        // Size 2-120
        Assert.assertFalse(messageTextValidator.isValid("H", constraintValidatorContext));
        Assert.assertFalse(messageTextValidator
                .isValid(
                        "Have a nice day Have a nice day Have a nice day Have a nice day Have a nice day Have a nice day Have a nice day Have a nice day Have a nice day today!!",
                        constraintValidatorContext));
        Assert.assertFalse(messageTextValidator.isValid("adfadfadsfdfasdfasdfsadfasdfaerwerwsfsdfdsfad\r\nfadsfasdfwer"
                + "\r\ntsdgasdfas\r\ndfasdfasdfasdf\r\nasdfcvefrwerdsfsf\r\nasdfsadfawerdfwerfda",
                constraintValidatorContext));
    }

    @Test
    public void testWithMandatory() {

        Mockito.when(Boolean.valueOf(messageText.mandatory())).thenReturn(Boolean.TRUE);
        messageTextValidator.initialize(messageText);

        Assert.assertFalse(messageTextValidator.isValid(null, constraintValidatorContext));
        Assert.assertFalse(messageTextValidator.isValid("", constraintValidatorContext));
    }


}

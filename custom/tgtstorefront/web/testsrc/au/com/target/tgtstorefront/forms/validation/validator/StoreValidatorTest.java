package au.com.target.tgtstorefront.forms.validation.validator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtstorefront.forms.validation.Store;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StoreValidatorTest {

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @InjectMocks
    private final StoreValidator storeValidator = new StoreValidator();

    @Test
    public void testValidStore() {
        final Integer storeNo = new Integer(1234);
        final TargetPointOfServiceData pointOfServiceData = mock(TargetPointOfServiceData.class);
        given(targetStoreLocatorFacade.getPointOfService(storeNo)).willReturn(pointOfServiceData);

        final boolean available = storeValidator.isType(storeNo.toString())
                && storeValidator.isAvailable(storeNo.toString());
        assertThat(available).isTrue();
    }

    @Test
    public void testInvalidType() {
        final boolean typeCheck = storeValidator.isType("abc");
        assertThat(typeCheck).isFalse();
    }

    @Test
    public void testCncStore() {
        final Integer storeNo = new Integer(1234);
        final TargetPointOfServiceData pointOfServiceData = mock(TargetPointOfServiceData.class);
        given(pointOfServiceData.getAcceptCNC()).willReturn(Boolean.TRUE);
        given(targetStoreLocatorFacade.getPointOfService(storeNo)).willReturn(pointOfServiceData);
        final Store store = mock(Store.class);
        given(Boolean.valueOf(store.checkClickAndCollectAvailable())).willReturn(Boolean.TRUE);
        storeValidator.initialize(store);

        final boolean available = storeValidator.isType(storeNo.toString())
                && storeValidator.isAvailable(storeNo.toString());
        assertThat(available).isTrue();
    }

    @Test
    public void testInvalidCncStore() {
        final Integer storeNo = new Integer(1234);
        final TargetPointOfServiceData pointOfServiceData = mock(TargetPointOfServiceData.class);
        given(pointOfServiceData.getAcceptCNC()).willReturn(Boolean.FALSE);
        given(targetStoreLocatorFacade.getPointOfService(storeNo)).willReturn(pointOfServiceData);
        final Store store = mock(Store.class);
        given(Boolean.valueOf(store.checkClickAndCollectAvailable())).willReturn(Boolean.TRUE);
        storeValidator.initialize(store);

        final boolean available = storeValidator.isType(storeNo.toString())
                && storeValidator.isAvailable(storeNo.toString());
        assertThat(available).isFalse();
    }

    @Test
    public void testIgnoreCncCheck() {
        final Integer storeNo = new Integer(1234);
        final TargetPointOfServiceData pointOfServiceData = mock(TargetPointOfServiceData.class);
        given(pointOfServiceData.getAcceptCNC()).willReturn(Boolean.FALSE);
        given(targetStoreLocatorFacade.getPointOfService(storeNo)).willReturn(pointOfServiceData);
        final Store store = mock(Store.class);
        given(Boolean.valueOf(store.checkClickAndCollectAvailable())).willReturn(Boolean.FALSE);
        storeValidator.initialize(store);

        final boolean available = storeValidator.isType(storeNo.toString())
                && storeValidator.isAvailable(storeNo.toString());
        assertThat(available).isTrue();
    }

}

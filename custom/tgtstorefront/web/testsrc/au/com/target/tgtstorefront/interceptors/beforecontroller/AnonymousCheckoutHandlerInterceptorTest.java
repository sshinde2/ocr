/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.method.HandlerMethod;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.checkout.flow.impl.TargetCheckoutCustomerStrategyImpl;
import au.com.target.tgtfacades.constants.TgtFacadesConstants;
import au.com.target.tgtstorefront.constants.WebConstants;
import au.com.target.tgtstorefront.controllers.ControllerConstants;
import au.com.target.tgtstorefront.security.IdentifyExitingCheckoutStrategy;


/**
 * @author bhuang3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AnonymousCheckoutHandlerInterceptorTest {
    @InjectMocks
    protected final AnonymousCheckoutHandlerInterceptor interceptor = new AnonymousCheckoutHandlerInterceptor();

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private HttpServletRequest request;
    @Mock
    private HttpSession mockHttpSession;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession httpSession;
    @Mock
    private HandlerMethod handlerMethod;
    @Mock
    private UserService userService;
    @Mock
    private TargetCustomerModel customer;
    @Mock
    private CustomerModel mockAnonymousUser;
    @Mock
    private CustomerModel mockCurrentCustomer;
    @Mock
    private TargetCheckoutCustomerStrategyImpl checkoutCustomerStrategy;
    @Mock
    private SessionService sessionService;

    @Mock
    private IdentifyExitingCheckoutStrategy identifyCheckoutStrategy;

    @Before
    public void setUp() {
        given(userService.getAnonymousUser()).willReturn(mockAnonymousUser);
        given(userService.getCurrentUser()).willReturn(mockCurrentCustomer);

        given(request.getSession()).willReturn(mockHttpSession);
    }

    @Test
    public void testForGuestAbandonAnonymousCheckout() throws Exception {
        given(Boolean.valueOf(identifyCheckoutStrategy.isUserMovingOutOfCheckout(request))).willReturn(Boolean.TRUE);
        given(sessionService.getAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT)).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);
        given(checkoutCustomerStrategy.getCurrentUserForCheckout()).willReturn(customer);
        given(customer.getType()).willReturn(CustomerType.GUEST);

        interceptor.beforeController(request, response, handlerMethod);

        verify(sessionService).removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
        verify(checkoutCustomerStrategy).removeGuestCheckoutCustomer(customer, mockAnonymousUser);
    }

    @Test
    public void testForGuestAbandonAnonymousCheckoutAfterPlaceOrderStarts() throws Exception {
        given(Boolean.valueOf(identifyCheckoutStrategy.isUserMovingOutOfCheckout(request))).willReturn(Boolean.TRUE);
        given(sessionService.getAttribute(TgtFacadesConstants.ANONYMOUS_CHECKOUT)).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);
        given(checkoutCustomerStrategy.getCurrentUserForCheckout()).willReturn(customer);
        given(customer.getType()).willReturn(CustomerType.GUEST);
        given(mockHttpSession.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_IN_PROGRESS))
                .willReturn(Boolean.TRUE);
        interceptor.beforeController(request, response, handlerMethod);
        verify(checkoutCustomerStrategy, Mockito.times(0)).getCurrentUserForCheckout();
    }

    @Test
    public void testForGuestAsCustomerTypeNotGuestAnonymousCheckout() throws Exception {
        given(Boolean.valueOf(identifyCheckoutStrategy.isUserMovingOutOfCheckout(request))).willReturn(Boolean.TRUE);
        given(sessionService.getAttribute(WebConstants.ANONYMOUS_CHECKOUT)).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);
        given(checkoutCustomerStrategy.getCurrentUserForCheckout()).willReturn(customer);
        given(customer.getType()).willReturn(null);

        interceptor.beforeController(request, response, handlerMethod);

        verify(sessionService).removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
        verify(checkoutCustomerStrategy, Mockito.never()).removeGuestCheckoutCustomer(customer, mockAnonymousUser);
    }

    @Test
    public void testForRegisteredGuestAbandonAnonymousCheckout() throws Exception {
        given(Boolean.valueOf(identifyCheckoutStrategy.isUserMovingOutOfCheckout(request))).willReturn(Boolean.TRUE);
        given(sessionService.getAttribute(WebConstants.ANONYMOUS_CHECKOUT)).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);
        given(checkoutCustomerStrategy.getCurrentUserForCheckout()).willReturn(customer);
        given(customer.getType()).willReturn(CustomerType.GUEST);

        given(sessionService.getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST)).willReturn(Boolean.TRUE);

        interceptor.beforeController(request, response, handlerMethod);

        verify(sessionService).removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
        verify(checkoutCustomerStrategy).removeGuestCheckoutCustomer(customer, mockCurrentCustomer);
        verify(mockHttpSession).removeAttribute(WebConstants.SECURE_GUID_SESSION_KEY);
    }

    @Test
    public void testForRegisteredGuestAsCustomerTypeNotGuestAnonymousCheckout() throws Exception {
        given(Boolean.valueOf(identifyCheckoutStrategy.isUserMovingOutOfCheckout(request))).willReturn(Boolean.TRUE);
        given(sessionService.getAttribute(WebConstants.ANONYMOUS_CHECKOUT)).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);
        given(checkoutCustomerStrategy.getCurrentUserForCheckout()).willReturn(customer);
        given(customer.getType()).willReturn(null);

        given(sessionService.getAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST)).willReturn(Boolean.TRUE);

        interceptor.beforeController(request, response, handlerMethod);

        verify(sessionService).removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
        verify(sessionService).removeAttribute(TgtFacadesConstants.CHECKOUT_REGISTERED_AS_GUEST);
        verify(checkoutCustomerStrategy, Mockito.never()).removeGuestCheckoutCustomer(any(TargetCustomerModel.class),
                any(CustomerModel.class));
        verify(mockHttpSession).removeAttribute(WebConstants.SECURE_GUID_SESSION_KEY);
    }

    @Test
    public void testForGuestInAnonymousCheckout() throws Exception {
        given(Boolean.valueOf(identifyCheckoutStrategy.isUserMovingOutOfCheckout(request))).willReturn(Boolean.FALSE);
        given(sessionService.getAttribute(WebConstants.ANONYMOUS_CHECKOUT)).willReturn(Boolean.TRUE);
        given(Boolean.valueOf(checkoutCustomerStrategy.isAnonymousCheckout())).willReturn(Boolean.TRUE);

        interceptor.beforeController(request, response, handlerMethod);

        verifyNoMoreInteractions(sessionService);
    }

    @Test
    public void testBeforeControllerForAnonymousUserBeforeCheckout() throws Exception {
        given(Boolean.valueOf(identifyCheckoutStrategy.isUserMovingOutOfCheckout(request))).willReturn(Boolean.TRUE);
        given(sessionService.getAttribute(WebConstants.ANONYMOUS_CHECKOUT)).willReturn(Boolean.FALSE);

        interceptor.beforeController(request, response, handlerMethod);

        verify(checkoutCustomerStrategy, Mockito.times(0)).getCurrentUserForCheckout();
    }

}

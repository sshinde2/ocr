/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.method.HandlerMethod;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtstorefront.customersegment.impl.CustomerSegmentCookieStrategy;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerSegmentCookieBeforeHandlerTest {
    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private UserService userService;

    @Mock
    private CustomerSegmentCookieStrategy customerSegmentCookieStrategy;

    @InjectMocks
    private final CustomerSegmentCookieBeforeHandler customerSegmentCookieBeforeHandler = new CustomerSegmentCookieBeforeHandler();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HandlerMethod handlerMethod;

    @Test
    public void testBeforeControllerFeatureOff() throws Exception {
        BDDMockito.given(
                Boolean.valueOf(targetFeatureSwitchFacade
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CUSTOMER_SEGMENT_COOKIE))).willReturn(
                Boolean.FALSE);
        final boolean success = customerSegmentCookieBeforeHandler.beforeController(request, response, handlerMethod);
        Assertions.assertThat(success).isTrue();
        Mockito.verify(targetFeatureSwitchFacade).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.CUSTOMER_SEGMENT_COOKIE);
        Mockito.verifyNoMoreInteractions(targetFeatureSwitchFacade);
        Mockito.verifyZeroInteractions(userService, customerSegmentCookieStrategy);
    }

    @Test
    public void testBeforeControllerUserAnonymous() throws Exception {
        BDDMockito.given(
                Boolean.valueOf(targetFeatureSwitchFacade
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CUSTOMER_SEGMENT_COOKIE))).willReturn(
                Boolean.TRUE);
        final UserModel user = new UserModel();
        BDDMockito.given(userService.getCurrentUser()).willReturn(user);
        BDDMockito.given(Boolean.valueOf(userService.isAnonymousUser(user))).willReturn(Boolean.TRUE);
        final boolean success = customerSegmentCookieBeforeHandler.beforeController(request, response, handlerMethod);
        Assertions.assertThat(success).isTrue();
        Mockito.verify(targetFeatureSwitchFacade).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.CUSTOMER_SEGMENT_COOKIE);
        Mockito.verifyNoMoreInteractions(targetFeatureSwitchFacade);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verify(userService).isAnonymousUser(user);
        Mockito.verifyNoMoreInteractions(userService);
        Mockito.verifyZeroInteractions(customerSegmentCookieStrategy);
    }

    @Test
    public void testBeforeControllerCreateCookie() throws Exception {
        BDDMockito.given(
                Boolean.valueOf(targetFeatureSwitchFacade
                        .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.CUSTOMER_SEGMENT_COOKIE))).willReturn(
                Boolean.TRUE);
        final UserModel user = new UserModel();
        BDDMockito.given(userService.getCurrentUser()).willReturn(user);
        BDDMockito.given(Boolean.valueOf(userService.isAnonymousUser(user))).willReturn(Boolean.FALSE);
        final boolean success = customerSegmentCookieBeforeHandler.beforeController(request, response, handlerMethod);
        Assertions.assertThat(success).isTrue();
        Mockito.verify(targetFeatureSwitchFacade).isFeatureEnabled(
                TgtCoreConstants.FeatureSwitch.CUSTOMER_SEGMENT_COOKIE);
        Mockito.verifyNoMoreInteractions(targetFeatureSwitchFacade);
        Mockito.verify(userService).getCurrentUser();
        Mockito.verify(userService).isAnonymousUser(user);
        Mockito.verify(customerSegmentCookieStrategy).setCookie(request, response);
        Mockito.verifyNoMoreInteractions(targetFeatureSwitchFacade, userService, customerSegmentCookieStrategy);
    }
}

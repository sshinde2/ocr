/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforecontroller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.web.method.HandlerMethod;

import au.com.target.tgtstorefront.annotations.RequireHardLogIn;
import au.com.target.tgtstorefront.constants.WebConstants;


/**
 * @author Glenn
 * 
 */
public class RequireHardLoginBeforeControllerHandlerTest {

    private static final String GUID_VALID = "valid-guid";

    @Mock
    private final RequireHardLoginBeforeControllerHandler handler = new RequireHardLoginBeforeControllerHandler();

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HttpSession session;

    @Mock
    private UserService userService;

    @Mock
    private SessionService sessionService;

    @Mock
    private RedirectStrategy redirectStrategy;

    @Mock
    private UserModel userModel;

    @Mock
    private HandlerMethod handlerMethod;

    @Mock
    private RequireHardLogIn requireHardLogIn;

    /**
     * @throws java.lang.Exception
     */
    @SuppressWarnings("boxing")
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(request.isSecure()).thenReturn(true);
        when(request.getSession()).thenReturn(session);
        when(userService.getCurrentUser()).thenReturn(userModel);

        when(handler.getUserService()).thenReturn(userService);
        when(handler.getSessionService()).thenReturn(sessionService);
        when(handler.getRedirectStrategy()).thenReturn(redirectStrategy);
        when(handler.findAnnotation(handlerMethod, RequireHardLogIn.class)).thenReturn(requireHardLogIn);
        when(handler.beforeController(request, response, handlerMethod)).thenCallRealMethod();
    }

    @SuppressWarnings("boxing")
    @Test
    public void testInsecureUrl() throws Exception {
        when(request.isSecure()).thenReturn(false);
        assertTrue(handler.beforeController(request, response, handlerMethod));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testNoFindAnnotation() throws Exception {
        when(handler.findAnnotation(handlerMethod, RequireHardLogIn.class)).thenReturn(null);
        assertTrue(handler.beforeController(request, response, handlerMethod));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testLoggedInUserNoRedirection() throws Exception {
        when(userService.isAnonymousUser(userModel)).thenReturn(false);
        when(request.getSession().getAttribute(WebConstants.SECURE_GUID_SESSION_KEY))
                .thenReturn(GUID_VALID);
        when(handler.checkForGUIDCookie(request, response, GUID_VALID)).thenReturn(true);

        assertTrue(handler.beforeController(request, response, handlerMethod));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testLoggedInUserWithRedirection() throws Exception {
        when(userService.isAnonymousUser(userModel)).thenReturn(false);
        when(session.getAttribute(WebConstants.SECURE_GUID_SESSION_KEY))
                .thenReturn(GUID_VALID);
        when(handler.checkForGUIDCookie(request, response, GUID_VALID)).thenReturn(false);

        assertFalse(handler.beforeController(request, response, handlerMethod));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testAnonymousUserNoRedirection() throws Exception {
        when(userService.isAnonymousUser(userModel)).thenReturn(false);
        when(request.getSession().getAttribute(WebConstants.SECURE_GUID_SESSION_KEY))
                .thenReturn(GUID_VALID);
        when(sessionService.getAttribute(
                WebConstants.ANONYMOUS_CHECKOUT)).thenReturn(Boolean.TRUE);
        when(handler.checkForGUIDCookie(request, response, GUID_VALID)).thenReturn(true);

        assertTrue(handler.beforeController(request, response, handlerMethod));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testAnonymousUserWithRedirection() throws Exception {
        when(userService.isAnonymousUser(userModel)).thenReturn(true);
        when(request.getSession().getAttribute(WebConstants.SECURE_GUID_SESSION_KEY))
                .thenReturn(GUID_VALID);
        when(sessionService.getAttribute(
                WebConstants.ANONYMOUS_CHECKOUT)).thenReturn(Boolean.FALSE);

        assertFalse(handler.beforeController(request, response, handlerMethod));
    }

}

/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.config.impl.HybrisConfiguration;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.configuration.ConversionException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtstorefront.constants.WebConstants;


public class DevelopmentModeBeforeViewHandlerTest
{
    @Mock
    private HttpServletRequest mockRequest;

    @Mock
    private ConfigurationService mockConfigurationService;

    @Mock
    private HybrisConfiguration mockHybrisConfiguration;

    @Before
    public void prepare()
    {
        MockitoAnnotations.initMocks(this);

        BDDMockito.given(mockConfigurationService.getConfiguration()).willReturn(mockHybrisConfiguration);
    }

    @Test
    public void testBeforeViewWithFalseConfigValue() throws Exception
    {
        BDDMockito.given(mockHybrisConfiguration.getBoolean(BDDMockito.anyString(), BDDMockito.eq(Boolean.FALSE)))
                .willReturn(
                        Boolean.FALSE);

        final DevelopmentModeBeforeViewHandler developmentModeBeforeViewHandler = new DevelopmentModeBeforeViewHandler(
                mockConfigurationService);
        developmentModeBeforeViewHandler.beforeView(mockRequest, null, null);

        Mockito.verify(mockRequest).setAttribute(WebConstants.STOREFRONT_ASSETMODE_DEVELOP_REQUEST_KEY, Boolean.FALSE);
    }

    @Test
    public void testBeforeViewWithTrueConfigValue() throws Exception
    {
        BDDMockito.given(mockHybrisConfiguration.getBoolean(BDDMockito.anyString(), BDDMockito.eq(Boolean.FALSE)))
                .willReturn(
                        Boolean.TRUE);

        final DevelopmentModeBeforeViewHandler developmentModeBeforeViewHandler = new DevelopmentModeBeforeViewHandler(
                mockConfigurationService);
        developmentModeBeforeViewHandler.beforeView(mockRequest, null, null);

        Mockito.verify(mockRequest).setAttribute(WebConstants.STOREFRONT_ASSETMODE_DEVELOP_REQUEST_KEY, Boolean.TRUE);
    }

    @Test
    public void testBeforeViewWithInvalidConfigValue() throws Exception
    {
        BDDMockito.given(mockHybrisConfiguration.getBoolean(BDDMockito.anyString(), BDDMockito.eq(Boolean.FALSE)))
                .willThrow(
                        new ConversionException());

        final DevelopmentModeBeforeViewHandler developmentModeBeforeViewHandler = new DevelopmentModeBeforeViewHandler(
                mockConfigurationService);
        developmentModeBeforeViewHandler.beforeView(mockRequest, null, null);

        Mockito.verify(mockRequest).setAttribute(WebConstants.STOREFRONT_ASSETMODE_DEVELOP_REQUEST_KEY, Boolean.FALSE);
    }
}

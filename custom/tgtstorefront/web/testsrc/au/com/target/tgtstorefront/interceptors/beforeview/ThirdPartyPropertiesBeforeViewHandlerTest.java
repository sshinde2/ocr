/**
 * 
 */
package au.com.target.tgtstorefront.interceptors.beforeview;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.HostConfigService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.config.impl.HybrisConfiguration;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.ModelAndView;

import au.com.target.tgtfacades.constants.ThirdPartyConstants;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfacades.salesapplication.SalesApplicationFacade;
import au.com.target.tgtfacades.util.SalesApplicationPropertyResolver;


/**
 * @author cbattle
 * 
 */
@UnitTest
public class ThirdPartyPropertiesBeforeViewHandlerTest {

    @Mock
    private ConfigurationService mockConfigurationService;

    @Mock
    private HybrisConfiguration mockHybrisConfiguration;

    @Mock
    private HostConfigService mockHostConfigService;

    @Mock
    private SalesApplicationFacade mockSalesApplicationFacade;

    @Mock
    private SalesApplicationPropertyResolver salesApplicationPropertyResolver;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @InjectMocks
    private final ThirdPartyPropertiesBeforeViewHandler thirdPartyPropertiesBeforeViewHandler = new ThirdPartyPropertiesBeforeViewHandler();

    @Before
    public void prepare() {
        MockitoAnnotations.initMocks(this);
        BDDMockito.given(mockConfigurationService.getConfiguration()).willReturn(mockHybrisConfiguration);
    }

    @Test
    public void testBeforeViewEnvironmentSpecific() {
        final ModelAndView modelAndView = new ModelAndView();
        thirdPartyPropertiesBeforeViewHandler.setHostSuffix("dev");

        BDDMockito.given(mockHostConfigService.getProperty(ThirdPartyConstants.AddThis.PUB_ID, "dev"))
                .willReturn(
                        "addthis.api.key.dev");

        thirdPartyPropertiesBeforeViewHandler.beforeView(null, null, modelAndView);

        Assert.assertEquals("addthis.api.key.dev", modelAndView.getModel().get("addThisPubId"));
    }

    @Test
    public void testBeforeViewFallBack() {
        final ModelAndView modelAndView = new ModelAndView();
        thirdPartyPropertiesBeforeViewHandler.setHostSuffix("");

        BDDMockito.given(mockHostConfigService.getProperty(ThirdPartyConstants.AddThis.PUB_ID, ""))
                .willReturn(
                        "addthis.api.key");

        thirdPartyPropertiesBeforeViewHandler.beforeView(null, null, modelAndView);

        Assert.assertEquals("addthis.api.key", modelAndView.getModel().get("addThisPubId"));
    }

    @Test
    public void testBeforeViewNull() {
        final ModelAndView modelAndView = new ModelAndView();
        thirdPartyPropertiesBeforeViewHandler.setHostSuffix(null);

        BDDMockito.given(mockHostConfigService.getProperty(ThirdPartyConstants.AddThis.PUB_ID, ""))
                .willReturn(
                        "addthis.api.key");

        thirdPartyPropertiesBeforeViewHandler.beforeView(null, null, modelAndView);

        Assert.assertEquals("addthis.api.key", modelAndView.getModel().get("addThisPubId"));
    }

    @Test
    public void testBeforeViewAnalyticsNull() {
        Mockito.when(salesApplicationPropertyResolver.getGAValueForSalesApplication()).thenReturn(null);
        final ModelAndView modelAndView = new ModelAndView();
        thirdPartyPropertiesBeforeViewHandler.beforeView(null, null, modelAndView);
        Assert.assertEquals(null, modelAndView.getModel().get("universalAnalyticsId"));
    }

    @Test
    public void testBeforeViewAnalyticsEmpty() {
        Mockito.when(salesApplicationPropertyResolver.getGAValueForSalesApplication()).thenReturn("");
        final ModelAndView modelAndView = new ModelAndView();
        thirdPartyPropertiesBeforeViewHandler.beforeView(null, null, modelAndView);
        Assert.assertEquals(null, modelAndView.getModel().get("universalAnalyticsId"));
    }

    @Test
    public void testBeforeViewAnalyticsNotEmpty() {
        Mockito.when(salesApplicationPropertyResolver.getGAValueForSalesApplication()).thenReturn("UA-XXXXX-10");
        final ModelAndView modelAndView = new ModelAndView();
        thirdPartyPropertiesBeforeViewHandler.beforeView(null, null, modelAndView);
        Assert.assertEquals("UA-XXXXX-10", modelAndView.getModel().get("universalAnalyticsId"));
    }

}

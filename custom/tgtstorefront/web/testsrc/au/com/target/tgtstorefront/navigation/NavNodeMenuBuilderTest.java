/**
 * 
 */
package au.com.target.tgtstorefront.navigation;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;


/**
 * @author mjanarth
 * 
 */
public class NavNodeMenuBuilderTest {

    @Mock
    private Converter<CategoryModel, CategoryData> mockCategoryConverter;

    @Mock
    private UrlResolver<CategoryData> mockCategoryDataUrlResolver;

    @InjectMocks
    private final NavNodeMenuBuilder navNodeMenuBuilder = new NavNodeMenuBuilder();

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateMenuWithEmptyEntries() {
        final CMSNavigationNodeModel mockNavNodeModel = Mockito.mock(CMSNavigationNodeModel.class);
        final List<CMSNavigationEntryModel> mockEntryList = new ArrayList<>();
        BDDMockito.given(mockNavNodeModel.getEntries()).willReturn(mockEntryList);
        Assert.assertNull(navNodeMenuBuilder.createMenuItem(mockNavNodeModel));


    }

    @Test
    public void testCreateMenuWithCategoryModel() {
        final CategoryModel mockCategory = Mockito.mock(CategoryModel.class);
        final String title = "Title";
        final CMSNavigationNodeModel mockCategoryNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        final CMSNavigationEntryModel mockCategoryNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        final CategoryData mockCategoryData = Mockito.mock(CategoryData.class);

        BDDMockito.given(mockCategoryNavigationNode.getTitle()).willReturn(title);
        BDDMockito.given(mockCategoryConverter.convert(mockCategory)).willReturn(mockCategoryData);

        BDDMockito.given(mockCategoryNavigationEntry.getItem()).willReturn(mockCategory);
        BDDMockito.given(mockCategoryNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCategoryNavigationEntry));
        BDDMockito.given(mockCategoryNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCategoryNavigationEntry));

        final NavigationMenuItem menuItem = navNodeMenuBuilder.createMenuItem(mockCategoryNavigationNode);
        Assert.assertNotNull(menuItem);
        Assert.assertEquals(menuItem.getName(), title);

    }

    @Test
    public void testCreateMenuWithContentPage() {
        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        final String title = "Title";
        final CMSNavigationEntryModel mockContentPageNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageNavigationEntry.getItem()).willReturn(mockContentPage);
        final CMSNavigationNodeModel mockContentPageNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentPageNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockContentPageNavigationEntry));
        BDDMockito.given(mockContentPageNavigationNode.getTitle()).willReturn(title);
        final NavigationMenuItem menuItem = navNodeMenuBuilder.createMenuItem(mockContentPageNavigationNode);
        Assert.assertNotNull(menuItem);
        Assert.assertEquals(menuItem.getName(), title);

    }

    @Ignore
    @Test
    public void testCreateMenuWithLinkComponent() {

        final CMSLinkComponentModel mockCmsLinkComponent = Mockito.mock(CMSLinkComponentModel.class);
        final String title = "Title";
        final CMSNavigationEntryModel mockCmsLinkComponentNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCmsLinkComponentNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);
        final CMSNavigationNodeModel mockCmsLinkComponentNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCmsLinkComponentNavigationNode.getTitle()).willReturn(title);
        BDDMockito.given(mockCmsLinkComponentNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));
        final NavigationMenuItem menuItem = navNodeMenuBuilder.createMenuItem(mockCmsLinkComponentNavigationNode);
        Assert.assertNotNull(menuItem);
        Assert.assertEquals(menuItem.getName(), title);

    }
}

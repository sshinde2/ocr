/**
 * 
 */
package au.com.target.tgtstorefront.navigation;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;
import au.com.target.tgtwebcore.model.cms2.components.NavigationComponentModel;


/**
 * @author rmcalave
 * 
 */
public class NavigationComponentMenuBuilderTest {
    private final NavigationComponentMenuBuilder navigationComponentMenuBuilder = new NavigationComponentMenuBuilder();

    @Test
    public void testCreateNavigationComponentMenuWithNoNavNode() {
        final NavigationComponentModel mockNavigationComponentModel = Mockito.mock(NavigationComponentModel.class);

        final NavigationMenuItem menuItem = navigationComponentMenuBuilder
                .createNavigationComponentMenu(mockNavigationComponentModel);

        Assert.assertNull(menuItem);
    }

    @Test
    public void testCreateNavigationComponentMenuWithNoEntries() {
        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        final NavigationComponentModel mockNavigationComponent = Mockito.mock(NavigationComponentModel.class);
        BDDMockito.given(mockNavigationComponent.getNavigationNode()).willReturn(mockNavigationNode);

        final NavigationMenuItem menuItem = navigationComponentMenuBuilder
                .createNavigationComponentMenu(mockNavigationComponent);

        Assert.assertNull(menuItem);
    }

    @Ignore
    @Test
    public void testCreateNavigationComponentMenuWithContentPageNoChildren() {
        final String pageTitle = "Awesome Page Title";
        final String pageLabel = "/help/faqs";

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getTitle()).willReturn(pageTitle);
        BDDMockito.given(mockContentPage.getLabel()).willReturn(pageLabel);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockContentPage);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));

        final NavigationComponentModel mockNavigationComponent = Mockito.mock(NavigationComponentModel.class);
        BDDMockito.given(mockNavigationComponent.getNavigationNode()).willReturn(mockNavigationNode);

        final NavigationMenuItem menuItem = navigationComponentMenuBuilder
                .createNavigationComponentMenu(mockNavigationComponent);

        Assert.assertNotNull(menuItem);
        Assert.assertNull(menuItem.getChildren());
        Assert.assertEquals(pageTitle, menuItem.getName());
        Assert.assertEquals(pageLabel, menuItem.getLink());
    }

    @Ignore
    @Test
    public void testCreateNavigationComponentMenuWithLinkNoChildren() {
        final String linkName = "Awesome Link";
        final String linkUrl = "http://www.target.com.au";

        final CMSLinkComponentModel mockLink = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockLink.getVisible()).willReturn(Boolean.TRUE);
        BDDMockito.given(mockLink.getLinkName()).willReturn(linkName);
        BDDMockito.given(mockLink.getUrl()).willReturn(linkUrl);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockLink);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));

        final NavigationComponentModel mockNavigationComponent = Mockito.mock(NavigationComponentModel.class);
        BDDMockito.given(mockNavigationComponent.getNavigationNode()).willReturn(mockNavigationNode);

        final NavigationMenuItem menuItem = navigationComponentMenuBuilder
                .createNavigationComponentMenu(mockNavigationComponent);

        Assert.assertNotNull(menuItem);
        Assert.assertNull(menuItem.getChildren());
        Assert.assertEquals(linkName, menuItem.getName());
        Assert.assertEquals(linkUrl, menuItem.getLink());
    }

    @Ignore
    @Test
    public void testCreateNavigationComponentMenuWithContentPageOneChild() {
        // Child Page
        final String childPageTitle = "Awesome Child Page Title";
        final String childPageLabel = "/help/faqs/delivery-faqs";

        final ContentPageModel mockChildContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockChildContentPage.getTitle()).willReturn(childPageTitle);
        BDDMockito.given(mockChildContentPage.getLabel()).willReturn(childPageLabel);

        final CMSNavigationEntryModel mockChildNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockChildNavigationEntry.getItem()).willReturn(mockChildContentPage);

        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockChildNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockChildNavigationEntry));

        // Top Level
        final String pageTitle = "Awesome Page Title";
        final String pageLabel = "/help/faqs";

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getTitle()).willReturn(pageTitle);
        BDDMockito.given(mockContentPage.getLabel()).willReturn(pageLabel);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockContentPage);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final NavigationComponentModel mockNavigationComponent = Mockito.mock(NavigationComponentModel.class);
        BDDMockito.given(mockNavigationComponent.getNavigationNode()).willReturn(mockNavigationNode);

        final NavigationMenuItem menuItem = navigationComponentMenuBuilder
                .createNavigationComponentMenu(mockNavigationComponent);

        Assert.assertNotNull(menuItem);
        Assert.assertEquals(pageTitle, menuItem.getName());
        Assert.assertEquals(pageLabel, menuItem.getLink());

        final Set<NavigationMenuItem> childMenuItems = menuItem.getChildren();
        Assert.assertNotNull(childMenuItems);
        Assert.assertTrue(childMenuItems.size() == 1);

        final NavigationMenuItem childMenuItem = childMenuItems.iterator().next();
        Assert.assertNotNull(childMenuItem);
        Assert.assertEquals(childPageTitle, childMenuItem.getName());
        Assert.assertEquals(childPageLabel, childMenuItem.getLink());
    }

    @Ignore
    @Test
    public void testCreateNavigationComponentMenuWithContentPageOneChildNodeNoEntries() {
        final CMSNavigationNodeModel mockChildNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);

        // Top Level
        final String pageTitle = "Awesome Page Title";
        final String pageLabel = "/help/faqs";

        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        BDDMockito.given(mockContentPage.getTitle()).willReturn(pageTitle);
        BDDMockito.given(mockContentPage.getLabel()).willReturn(pageLabel);

        final CMSNavigationEntryModel mockNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockNavigationEntry.getItem()).willReturn(mockContentPage);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(Collections.singletonList(mockNavigationEntry));
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(
                Collections.singletonList(mockChildNavigationNode));

        final NavigationComponentModel mockNavigationComponent = Mockito.mock(NavigationComponentModel.class);
        BDDMockito.given(mockNavigationComponent.getNavigationNode()).willReturn(mockNavigationNode);

        final NavigationMenuItem menuItem = navigationComponentMenuBuilder
                .createNavigationComponentMenu(mockNavigationComponent);

        Assert.assertNotNull(menuItem);
        Assert.assertEquals(pageTitle, menuItem.getName());
        Assert.assertEquals(pageLabel, menuItem.getLink());

        final Set<NavigationMenuItem> childMenuItems = menuItem.getChildren();
        Assert.assertNotNull(childMenuItems);
        Assert.assertTrue(CollectionUtils.isEmpty(childMenuItems));
    }
}

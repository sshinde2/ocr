/**
 * 
 */
package au.com.target.tgtstorefront.navigation;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtfacades.navigation.NavigationMenuItem;


/**
 * @author rmcalave
 * 
 */
@UnitTest
public class SiteMapMenuBuilderTest {
    //CHECKSTYLE:OFF mockito requires this not to be private for InjectMocks to work
    @InjectMocks
    protected final SiteMapMenuBuilder siteMapMenuBuilder = new SiteMapMenuBuilder();
    //CHECKSTYLE:ON

    @Mock
    private Converter<CategoryModel, CategoryData> mockCategoryConverter;

    @Mock
    private UrlResolver<CategoryData> mockCategoryDataUrlResolver;

    @Mock
    private CMSNavigationNodeModel cmsNavigationNodeModel;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateMenuItemsForChildrenWithNullList() {
        final Set<NavigationMenuItem> menuItems = siteMapMenuBuilder.createMenuItemsForChildren(null);

        Assert.assertNull(menuItems);
    }

    @Test
    public void testCreateMenuItemsForChildrenWithEmptyList() {
        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        final Set<NavigationMenuItem> menuItems = siteMapMenuBuilder.createMenuItemsForChildren(navNodeList);

        Assert.assertNull(menuItems);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateMenuItemsForChildrenWithNotVisibleNavNode() {
        final CMSNavigationNodeModel mockInvisibleNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockInvisibleNavigationNode.isVisible()).willReturn(false);
        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        navNodeList.add(mockInvisibleNavigationNode);
        final Set<NavigationMenuItem> menuItems = siteMapMenuBuilder.createMenuItemsForChildren(navNodeList);

        Assert.assertNotNull(menuItems);
        Assert.assertTrue(menuItems.isEmpty());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateMenuItemsForChildrenWithNoEntriesNoChildren() {
        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.isVisible()).willReturn(true);
        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        navNodeList.add(mockNavigationNode);
        final Set<NavigationMenuItem> menuItems = siteMapMenuBuilder.createMenuItemsForChildren(navNodeList);

        Assert.assertNotNull(menuItems);
        Assert.assertTrue(menuItems.isEmpty());
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateMenuItemsForChildrenWithNoEntriesWithChildWithAllTypes() {
        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        final CMSNavigationEntryModel mockContentPageNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageNavigationEntry.getItem()).willReturn(mockContentPage);
        final CMSNavigationNodeModel mockContentPageNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentPageNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockContentPageNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockContentPageNavigationEntry));

        final CategoryModel mockCategory = Mockito.mock(CategoryModel.class);
        final CMSNavigationEntryModel mockCategoryNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCategoryNavigationEntry.getItem()).willReturn(mockCategory);
        final CMSNavigationNodeModel mockCategoryNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCategoryNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockCategoryNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCategoryNavigationEntry));
        final CategoryData mockCategoryData = Mockito.mock(CategoryData.class);
        BDDMockito.given(mockCategoryConverter.convert(mockCategory)).willReturn(mockCategoryData);

        final CMSLinkComponentModel mockCmsLinkComponent = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.TRUE);
        final CMSNavigationEntryModel mockCmsLinkComponentNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCmsLinkComponentNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);
        final CMSNavigationNodeModel mockCmsLinkComponentNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCmsLinkComponentNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockCmsLinkComponentNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));

        final ItemModel mockItem = Mockito.mock(ItemModel.class);
        final CMSNavigationEntryModel mockItemNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockItemNavigationEntry.getItem()).willReturn(mockItem);
        final CMSNavigationNodeModel mockItemNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockItemNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockItemNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockItemNavigationEntry));

        final List<CMSNavigationNodeModel> mockChildNavigationNodeList = new ArrayList<>();
        mockChildNavigationNodeList.add(mockContentPageNavigationNode);
        mockChildNavigationNodeList.add(mockCategoryNavigationNode);
        mockChildNavigationNodeList.add(mockCmsLinkComponentNavigationNode);
        mockChildNavigationNodeList.add(mockItemNavigationNode);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(mockChildNavigationNodeList);

        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        navNodeList.add(mockNavigationNode);

        final Set<NavigationMenuItem> menuItems = siteMapMenuBuilder.createMenuItemsForChildren(navNodeList);

        Assert.assertNotNull(menuItems);
        Assert.assertTrue(menuItems.size() == 3);
    }

    @Test
    public void testCreateMenuItemNull()
    {
        Assert.assertNull(siteMapMenuBuilder.createMenuItem(null));
    }

    @Test
    public void testCreateMenuItemEmpty()
    {
        BDDMockito.given(cmsNavigationNodeModel.getChildren()).willReturn(null);
        BDDMockito.given(cmsNavigationNodeModel.getEntries()).willReturn(null);
        Assert.assertNull(siteMapMenuBuilder.createMenuItem(cmsNavigationNodeModel));
    }

    @Test
    public void testCreateMenuItemEmptyEntries()
    {
        final List<CMSNavigationNodeModel> nodes = new ArrayList<>();
        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        nodes.add(mockNavigationNode);
        BDDMockito.given(cmsNavigationNodeModel.getChildren()).willReturn(nodes);
        BDDMockito.given(cmsNavigationNodeModel.getEntries()).willReturn(null);
        Assert.assertNull(siteMapMenuBuilder.createMenuItem(cmsNavigationNodeModel));
    }

    @Test
    public void testCreateMenuItemNullChildrenAndEmptyEntries()
    {
        final List<CMSNavigationEntryModel> entries = new ArrayList<>();
        final CMSNavigationEntryModel mockEntry = Mockito.mock(CMSNavigationEntryModel.class);
        entries.add(mockEntry);
        BDDMockito.given(cmsNavigationNodeModel.getChildren()).willReturn(null);
        BDDMockito.given(cmsNavigationNodeModel.getEntries()).willReturn(entries);
        BDDMockito.given(mockEntry.getItem()).willReturn(null);
        Assert.assertNull(siteMapMenuBuilder.createMenuItem(cmsNavigationNodeModel));
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCreateMenuItemForChildrenWithNoEntriesWithChildWithAllTypes() {
        final ContentPageModel mockContentPage = Mockito.mock(ContentPageModel.class);
        final CMSNavigationEntryModel mockContentPageNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockContentPageNavigationEntry.getItem()).willReturn(mockContentPage);
        final CMSNavigationNodeModel mockContentPageNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockContentPageNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockContentPageNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockContentPageNavigationEntry));

        final CategoryModel mockCategory = Mockito.mock(CategoryModel.class);
        final CMSNavigationEntryModel mockCategoryNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCategoryNavigationEntry.getItem()).willReturn(mockCategory);
        final CMSNavigationNodeModel mockCategoryNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCategoryNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockCategoryNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCategoryNavigationEntry));
        final CategoryData mockCategoryData = Mockito.mock(CategoryData.class);
        BDDMockito.given(mockCategoryConverter.convert(mockCategory)).willReturn(mockCategoryData);

        final CMSLinkComponentModel mockCmsLinkComponent = Mockito.mock(CMSLinkComponentModel.class);
        BDDMockito.given(mockCmsLinkComponent.getVisible()).willReturn(Boolean.TRUE);
        final CMSNavigationEntryModel mockCmsLinkComponentNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockCmsLinkComponentNavigationEntry.getItem()).willReturn(mockCmsLinkComponent);
        final CMSNavigationNodeModel mockCmsLinkComponentNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockCmsLinkComponentNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockCmsLinkComponentNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));

        final ItemModel mockItem = Mockito.mock(ItemModel.class);
        final CMSNavigationEntryModel mockItemNavigationEntry = Mockito.mock(CMSNavigationEntryModel.class);
        BDDMockito.given(mockItemNavigationEntry.getItem()).willReturn(mockItem);
        final CMSNavigationNodeModel mockItemNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockItemNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockItemNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockItemNavigationEntry));

        final List<CMSNavigationNodeModel> mockChildNavigationNodeList = new ArrayList<>();
        mockChildNavigationNodeList.add(mockContentPageNavigationNode);
        mockChildNavigationNodeList.add(mockCategoryNavigationNode);
        mockChildNavigationNodeList.add(mockCmsLinkComponentNavigationNode);
        mockChildNavigationNodeList.add(mockItemNavigationNode);

        final CMSNavigationNodeModel mockNavigationNode = Mockito.mock(CMSNavigationNodeModel.class);
        BDDMockito.given(mockNavigationNode.isVisible()).willReturn(true);
        BDDMockito.given(mockNavigationNode.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));
        BDDMockito.given(mockNavigationNode.getChildren()).willReturn(null);

        final List<CMSNavigationNodeModel> navNodeList = new ArrayList<>();
        navNodeList.add(mockNavigationNode);
        BDDMockito.given(cmsNavigationNodeModel.getChildren()).willReturn(navNodeList);
        BDDMockito.given(cmsNavigationNodeModel.getEntries()).willReturn(
                Collections.singletonList(mockCmsLinkComponentNavigationEntry));
        final NavigationMenuItem menuItem = siteMapMenuBuilder.createMenuItem(cmsNavigationNodeModel);

        Assert.assertNotNull(menuItem);
        //Assert.assertTrue(menuItems.size() == 3);
    }



}

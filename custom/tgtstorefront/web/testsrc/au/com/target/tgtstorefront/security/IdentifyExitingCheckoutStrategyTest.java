/**
 * 
 */
package au.com.target.tgtstorefront.security;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class IdentifyExitingCheckoutStrategyTest {

    private final IdentifyExitingCheckoutStrategy identifyExistingCheckoutStrategy = new IdentifyExitingCheckoutStrategy();

    @Before
    public void setup() {
        final List<String> excludedPath = Arrays.asList("/checkout");
        identifyExistingCheckoutStrategy.setExcludedPaths(excludedPath);
    }

    @Test
    public void testIsUserMovingOutOfCheckoutNotMovingOutOfCheckout() {
        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        BDDMockito.given(request.getServletPath()).willReturn("/checkout/address");
        Assertions.assertThat(identifyExistingCheckoutStrategy.isUserMovingOutOfCheckout(request)).isFalse();
    }

    @Test
    public void testIsUserMovingOutOfCheckoutMovingOutOfCheckout() {
        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        BDDMockito.given(request.getServletPath()).willReturn("/p/P1000");
        Assertions.assertThat(identifyExistingCheckoutStrategy.isUserMovingOutOfCheckout(request)).isTrue();
    }

}

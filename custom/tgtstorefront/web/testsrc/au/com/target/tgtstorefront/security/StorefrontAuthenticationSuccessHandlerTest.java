package au.com.target.tgtstorefront.security;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.login.TargetAuthenticationFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtstorefront.controllers.ControllerConstants;


@RunWith(MockitoJUnitRunner.class)
public class StorefrontAuthenticationSuccessHandlerTest {
    @Mock
    private CustomerFacade customerFacade;

    @Mock
    private TargetAuthenticationFacade targetAuthenticationFacade;

    @Mock
    private UiExperienceService uiExperienceService;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private HttpSession session;

    @Mock
    private TargetOrderFacade targetOrderFacade;

    @InjectMocks
    private final StorefrontAuthenticationSuccessHandler storefrontAuthenticationSuccessHandler = new StorefrontAuthenticationSuccessHandler();

    @Before
    public void setup() {
        given(request.getSession()).willReturn(session);
    }

    @Test
    public void testOnAuthenticationSuccess() throws Exception {
        final String username = "username@domain.com";

        given(request.getParameter("j_username")).willReturn(username);

        storefrontAuthenticationSuccessHandler.onAuthenticationSuccess(request, response, null);

        verify(targetAuthenticationFacade).resetFailedLoginAttemptCounter(username);
        verify(customerFacade).loginSuccess();
        verify(targetOrderFacade, never()).linkOrderToCustomer(Mockito.anyString());
    }

    @Test
    public void testOnAuthenticationSuccessAfterPlaceOrder() throws Exception {
        final String username = "username@domain.com";

        given(request.getParameter("j_username")).willReturn(username);
        given(session.getAttribute(ControllerConstants.SESSION_PARAM_PLACE_ORDER_ORDER_CODE)).willReturn("1234");

        storefrontAuthenticationSuccessHandler.onAuthenticationSuccess(request, response, null);

        verify(targetAuthenticationFacade).resetFailedLoginAttemptCounter(username);
        verify(customerFacade).loginSuccess();
        verify(targetOrderFacade).linkOrderToCustomer("1234");
    }

}

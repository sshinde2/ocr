/**
 * 
 */
package au.com.target.tgtstorefront.security;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class TargetPermanentRedirectStrategyTest {

    private final TargetPermanentRedirectStrategy targetPermanentRedirectStrategy = new TargetPermanentRedirectStrategy();

    @Test
    public void testPermanentRedirectWhenFeatureSeoSwitchOn() throws IOException {
        final String url = "https://localhost:9002";
        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        final HttpServletResponse response = Mockito.mock(MockHttpServletResponse.class);
        BDDMockito.given(response.encodeURL(url)).willReturn(url);
        targetPermanentRedirectStrategy.sendRedirect(request, response, url);
        Mockito.verify(response).setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        Mockito.verify(response).setHeader(TargetPermanentRedirectStrategy.LOCATION, url);
    }

}

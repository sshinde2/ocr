package au.com.target.tgtstorefront.security;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ClassMismatchException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;


@RunWith(MockitoJUnitRunner.class)
public class TargetPreAuthenticationChecksTest {
    @Mock
    private TargetAuthenticationFacade mockTargetAuthenticationFacade;

    @Mock
    private UserService mockUserService;

    @InjectMocks
    private final TargetPreAuthenticationChecks targetPreAuthenticationChecks = new TargetPreAuthenticationChecks();

    @Test(expected = BadCredentialsException.class)
    public void testCheckWithAnonymousUser() throws Exception {
        final UserDetails mockUserDetails = Mockito.mock(UserDetails.class);
        BDDMockito.given(mockUserDetails.getUsername()).willReturn("anonymous");

        targetPreAuthenticationChecks.check(mockUserDetails);
    }

    @Test(expected = BadCredentialsException.class)
    public void testCheckWithAdminUser() throws Exception {
        final UserDetails mockUserDetails = Mockito.mock(UserDetails.class);
        BDDMockito.given(mockUserDetails.getUsername()).willReturn("admin");

        targetPreAuthenticationChecks.check(mockUserDetails);
    }

    @SuppressWarnings("boxing")
    @Test(expected = BadCredentialsException.class)
    public void testCheckWithUserWithAdminAuthority() throws Exception {
        final UserDetails mockUserDetails = Mockito.mock(UserDetails.class);
        BDDMockito.given(mockUserDetails.getUsername()).willReturn("user@domain.com");
        Mockito.doReturn(Collections.<GrantedAuthority> singleton(new SimpleGrantedAuthority("ROLE_ADMINGROUP")))
                .when(mockUserDetails).getAuthorities();

        targetPreAuthenticationChecks.check(mockUserDetails);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCheckWithUserWithNoAdminAuthorities() throws Exception {
        final String username = "user@domain.com";

        final UserDetails mockUserDetails = Mockito.mock(UserDetails.class);
        BDDMockito.given(mockUserDetails.getUsername()).willReturn("user@domain.com");
        Mockito.doReturn(Collections.<GrantedAuthority> singleton(new SimpleGrantedAuthority("ROLE_ADMINGROUP")))
                .when(mockUserDetails).getAuthorities();

        final TargetCustomerModel mockCustomerModel = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(mockUserService.getUserForUID(username, TargetCustomerModel.class)).willReturn(
                mockCustomerModel);

        BDDMockito.given(mockTargetAuthenticationFacade.isAccountLocked(username)).willReturn(Boolean.FALSE);

        targetPreAuthenticationChecks.setAdminGroup("");
        targetPreAuthenticationChecks.check(mockUserDetails);
    }

    @Test(expected = BadCredentialsException.class)
    public void testCheckWithUnknownUser() throws Exception {
        final String username = "user@domain.com";

        final UserDetails mockUserDetails = Mockito.mock(UserDetails.class);
        BDDMockito.given(mockUserDetails.getUsername()).willReturn("user@domain.com");

        BDDMockito.given(mockUserService.getUserForUID(username, TargetCustomerModel.class)).willThrow(
                new UnknownIdentifierException(""));

        targetPreAuthenticationChecks.check(mockUserDetails);
    }

    @Test(expected = BadCredentialsException.class)
    public void testCheckWithNonTargetCustomerUser() throws Exception {
        final String username = "user@domain.com";

        final UserDetails mockUserDetails = Mockito.mock(UserDetails.class);
        BDDMockito.given(mockUserDetails.getUsername()).willReturn("user@domain.com");

        BDDMockito.given(mockUserService.getUserForUID(username, TargetCustomerModel.class)).willThrow(
                new ClassMismatchException(TargetCustomerModel.class, CustomerModel.class));

        targetPreAuthenticationChecks.check(mockUserDetails);
    }

    @SuppressWarnings("boxing")
    @Test(expected = LockedException.class)
    public void testCheckWithLockedAccount() throws Exception {
        final String username = "user@domain.com";

        final UserDetails mockUserDetails = Mockito.mock(UserDetails.class);
        BDDMockito.given(mockUserDetails.getUsername()).willReturn("user@domain.com");

        final TargetCustomerModel mockCustomerModel = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(mockUserService.getUserForUID(username, TargetCustomerModel.class)).willReturn(
                mockCustomerModel);

        BDDMockito.given(mockTargetAuthenticationFacade.isAccountLocked(username)).willReturn(Boolean.TRUE);

        targetPreAuthenticationChecks.check(mockUserDetails);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testCheck() throws Exception {
        final String username = "user@domain.com";

        final UserDetails mockUserDetails = Mockito.mock(UserDetails.class);
        BDDMockito.given(mockUserDetails.getUsername()).willReturn("user@domain.com");
        Mockito.doReturn(Collections.<GrantedAuthority> singleton(new SimpleGrantedAuthority("ROLE_CUSTOMER")))
                .when(mockUserDetails).getAuthorities();

        final TargetCustomerModel mockCustomerModel = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(mockUserService.getUserForUID(username, TargetCustomerModel.class)).willReturn(
                mockCustomerModel);

        BDDMockito.given(mockTargetAuthenticationFacade.isAccountLocked(username)).willReturn(Boolean.FALSE);

        targetPreAuthenticationChecks.check(mockUserDetails);
    }
}

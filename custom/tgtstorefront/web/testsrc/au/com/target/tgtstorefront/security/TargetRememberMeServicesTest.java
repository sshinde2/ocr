/**
 * 
 */
package au.com.target.tgtstorefront.security;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.codec.Hex;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtfacades.salesapplication.impl.SalesApplicationFacadeImpl;


/**
 * Unit tests for TargetRememberMeService.
 * 
 * @author jjayawa1
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetRememberMeServicesTest {

    private static final String SERVICE_KEY = "targetStoreFront";

    private static final String USER_NAME = "testUser";

    private static final String PASSWORD = "testPassword";

    @Mock
    private HttpServletRequest httpservletrequest;

    @Mock
    private HttpServletResponse httpservletresponse;

    @Mock
    private SessionService sessionService;

    @Mock
    private UserService userService;

    @Mock
    private TargetSalesApplicationService targetSalesApplicationService;

    @Mock
    private CustomerFacade customerFacade;

    private final UserDetailsService userDetailsService = Mockito.mock(UserDetailsService.class);

    @InjectMocks
    private final TargetRememberMeServices targetRememberMeServices = new TargetRememberMeServices(SERVICE_KEY,
            userDetailsService);


    @Test
    public void testProcessAutoLoginCookie() throws NoSuchAlgorithmException {
        final Date currentDate = new Date();
        final UserModel userModel = new UserModel();
        final UserDetails userDetails = Mockito.mock(UserDetails.class);
        final long expiryDate = currentDate.getTime() + 900000;
        final String expiryDateString = Long.toString(expiryDate);
        final MessageDigest digest = MessageDigest.getInstance("MD5");
        final String token = USER_NAME + ":" + expiryDate + ":" + PASSWORD + ":" + SERVICE_KEY;
        final String[] cookieTokens = new String[3];
        cookieTokens[0] = USER_NAME;
        cookieTokens[1] = expiryDateString;
        cookieTokens[2] = new String(Hex.encode(digest.digest(token.getBytes())));
        BDDMockito.given(userDetails.getUsername()).willReturn(USER_NAME);
        BDDMockito.given(userDetails.getPassword()).willReturn(PASSWORD);
        BDDMockito.given(userDetailsService.loadUserByUsername(cookieTokens[0])).willReturn(userDetails);
        BDDMockito.given(Boolean.valueOf(userService.isUserExisting(USER_NAME))).willReturn(Boolean.TRUE);
        BDDMockito.given(userService.getCurrentUser()).willReturn(userModel);
        final SalesApplicationFacadeImpl salesApplicationFacade = new SalesApplicationFacadeImpl();
        salesApplicationFacade.setTargetSalesApplicationService(targetSalesApplicationService);
        salesApplicationFacade.setSessionService(sessionService);
        targetRememberMeServices.setSalesApplicationFacade(salesApplicationFacade);
        targetRememberMeServices.setUserService(userService);
        targetRememberMeServices.setCustomerFacade(customerFacade);
        final UserDetails returnedUserDetails = targetRememberMeServices.processAutoLoginCookie(cookieTokens,
                httpservletrequest, httpservletresponse);
        Assert.assertEquals(USER_NAME, returnedUserDetails.getUsername());
        Assert.assertEquals(PASSWORD, returnedUserDetails.getPassword());
    }
}

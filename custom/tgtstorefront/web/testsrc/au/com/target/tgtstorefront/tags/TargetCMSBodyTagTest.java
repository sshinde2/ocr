package au.com.target.tgtstorefront.tags;

import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.PK;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class TargetCMSBodyTagTest {

    private final TargetCMSBodyTagToTest targetCMSBodyTag = new TargetCMSBodyTagToTest();

    private final JspWriter mockJspWriter = Mockito.mock(JspWriter.class);

    private class TargetCMSBodyTagToTest extends TargetCMSBodyTag {

        private boolean previewDataModelValid;

        private boolean liveEdit;


        @Override
        protected boolean isPreviewDataModelValid() {
            return previewDataModelValid;
        }

        @Override
        protected boolean isLiveEdit() {
            return liveEdit;
        }

        /**
         * @param previewDataModelValid
         *            the previewDataModelValid to set
         */
        public void setPreviewDataModelValid(final boolean previewDataModelValid) {
            this.previewDataModelValid = previewDataModelValid;
        }

        /**
         * @param liveEdit
         *            the liveEdit to set
         */
        public void setLiveEdit(final boolean liveEdit) {
            this.liveEdit = liveEdit;
        }
    }


    @Before
    public void setup() {
        targetCMSBodyTag.setPreviewDataModelValid(false);
        targetCMSBodyTag.setLiveEdit(false);
        final PageContext mockPageContext = Mockito.mock(PageContext.class);
        final HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        final AbstractPageModel mockPageModel = Mockito.mock(ContentPageModel.class);
        final PK mockPK = PK.createFixedCounterPK(1233, 2223);
        BDDMockito.given(mockPageContext.getRequest()).willReturn(mockRequest);
        BDDMockito.given(mockRequest.getAttribute("currentPage")).willReturn(mockPageModel);
        BDDMockito.given(mockPageModel.getPk()).willReturn(mockPK);

        BDDMockito.given(mockPageContext.getOut()).willReturn(mockJspWriter);
        targetCMSBodyTag.setPageContext(mockPageContext);


    }

    @Test
    public void testDoStartTagWhenHeaderOnlyAndDisableGetInfoIsTrue() throws Exception {
        targetCMSBodyTag.setHeaderOnly(Boolean.TRUE);
        targetCMSBodyTag.setBodyCssClass("test-class");
        final ArgumentCaptor<String> htmlFormed = ArgumentCaptor.forClass(String.class);
        targetCMSBodyTag.doStartTag();
        Mockito.verify(mockJspWriter).print(htmlFormed.capture());
        Assertions.assertThat(htmlFormed.getValue()).isEqualTo("<body class=\" header-only test-class\">\n");

    }

    @Test
    public void testDoStartTagWhenHeaderOnlyIsTrueAndDisableGetInfoIsFalse() throws Exception {
        targetCMSBodyTag.setHeaderOnly(Boolean.TRUE);
        targetCMSBodyTag.setBodyCssClass(null);
        final ArgumentCaptor<String> htmlFormed = ArgumentCaptor.forClass(String.class);
        targetCMSBodyTag.doStartTag();
        Mockito.verify(mockJspWriter).print(htmlFormed.capture());
        Assertions.assertThat(htmlFormed.getValue()).isEqualTo("<body class=\" header-only\">\n");

    }

    @Test
    public void testDoStartTagWhenHeaderOnlyIsFalseAndDisableGetInfoIsTrue() throws Exception {
        targetCMSBodyTag.setHeaderOnly(Boolean.FALSE);
        targetCMSBodyTag.setBodyCssClass("test-class");
        final ArgumentCaptor<String> htmlFormed = ArgumentCaptor.forClass(String.class);
        targetCMSBodyTag.doStartTag();
        Mockito.verify(mockJspWriter).print(htmlFormed.capture());
        Assertions.assertThat(htmlFormed.getValue()).isEqualTo("<body class=\" test-class\">\n");

    }

    @Test
    public void testDoStartTagWhenHeaderOnlyAndDisableGetInfoIsFalse() throws Exception {
        targetCMSBodyTag.setHeaderOnly(Boolean.FALSE);
        targetCMSBodyTag.setBodyCssClass("");
        final ArgumentCaptor<String> htmlFormed = ArgumentCaptor.forClass(String.class);
        targetCMSBodyTag.doStartTag();
        Mockito.verify(mockJspWriter).print(htmlFormed.capture());
        Assertions.assertThat(htmlFormed.getValue()).isEqualTo("<body>\n");

    }

    @Test
    public void testDoStartTagWhenHeaderOnlyIsFalseAndDisableGetInfoIsTrueAndItemTypeSet() throws Exception {
        targetCMSBodyTag.setHeaderOnly(Boolean.FALSE);
        targetCMSBodyTag.setItemType("http://schema.org/WebPage");
        final ArgumentCaptor<String> htmlFormed = ArgumentCaptor.forClass(String.class);
        targetCMSBodyTag.doStartTag();
        Mockito.verify(mockJspWriter).print(htmlFormed.capture());
        Assertions.assertThat(htmlFormed.getValue())
                .isEqualTo("<body itemscope itemtype=\"http://schema.org/WebPage\">\n");

    }

}

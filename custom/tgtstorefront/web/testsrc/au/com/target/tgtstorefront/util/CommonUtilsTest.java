/**
 * 
 */
package au.com.target.tgtstorefront.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Test;

import au.com.target.tgtstorefront.forms.ThreePartDateForm;


/**
 * @author bhuang3
 *
 */
public class CommonUtilsTest {

    @Test
    public void testTransformThreePartDateFormToDate() {
        final ThreePartDateForm testForm = new ThreePartDateForm();
        testForm.setDay("01");
        testForm.setMonth("07");
        testForm.setYear("2000");
        final Date dateResult = CommonUtils.transformThreePartDateFormToDate(testForm);
        final SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
        Assert.assertEquals("2000-07-01", sdf.format(dateResult));
    }

    @Test
    public void testTransformThreePartDateFormToDateWithInvalidNumber() {
        final ThreePartDateForm testForm = new ThreePartDateForm();
        testForm.setDay("01");
        testForm.setMonth("invalid");
        testForm.setYear("2000");
        final Date dateResult = CommonUtils.transformThreePartDateFormToDate(testForm);
        Assert.assertEquals(null, dateResult);
    }

    @Test
    public void testTransformThreePartDateFormToDateWithInvalidDate() {
        final ThreePartDateForm testForm = new ThreePartDateForm();
        testForm.setDay("31");
        testForm.setMonth("02");
        testForm.setYear("2000");
        final Date dateResult = CommonUtils.transformThreePartDateFormToDate(testForm);
        Assert.assertEquals(null, dateResult);
    }

    @Test
    public void testTransformThreePartDateFormToDateWithNullForm() {
        final ThreePartDateForm testForm = null;
        final Date dateResult = CommonUtils.transformThreePartDateFormToDate(testForm);
        Assert.assertEquals(null, dateResult);
    }
}

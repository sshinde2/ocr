/**
 * 
 */
package au.com.target.tgtstorefront.variants.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionQualifierData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.product.data.TargetVariantOptionQualifierData;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCustomVariantSortStrategyTest {

    private final TargetCustomVariantSortStrategy sortStrategy = new TargetCustomVariantSortStrategy();

    @Mock
    private Comparator comparator;

    @Mock
    private VariantOptionData variantOptionData1;

    @Mock
    private VariantOptionData variantOptionData2;

    @Mock
    private VariantOptionQualifierData variantOptionQualifierData1;

    @Mock
    private VariantOptionQualifierData variantOptionQualifierData2;

    @Mock
    private TargetVariantOptionQualifierData targetVariantOptionQualifierData1;

    @Mock
    private TargetVariantOptionQualifierData targetVariantOptionQualifierData2;

    @Before
    public void setUp() {
        final Map<String, Comparator<String>> map = new HashMap<String, Comparator<String>>();
        map.put("productSize", comparator);
        sortStrategy.setCustomComparators(map);
        sortStrategy.setSortingFieldsOrder(Arrays.asList("productSize"));
        sortStrategy.setDefaultComparator(comparator);
    }

    @Test
    public void testCompareSortingFieldsOfNull() {
        sortStrategy.setSortingFieldsOrder(null);
        final int compared = sortStrategy.compare(variantOptionData1, variantOptionData2);
        Assertions.assertThat(compared).isEqualTo(-1);
    }

    @Test
    public void testCompareWithQualifierNotInstanceofTargetQualifier() {
        BDDMockito.when(variantOptionData1.getVariantOptionQualifiers()).thenReturn(
                Arrays.asList(variantOptionQualifierData1));
        BDDMockito.when(variantOptionQualifierData1.getQualifier()).thenReturn("productSize");
        BDDMockito.when(variantOptionQualifierData1.getValue()).thenReturn("productSizeName1");

        BDDMockito.when(variantOptionData2.getVariantOptionQualifiers()).thenReturn(
                Arrays.asList(variantOptionQualifierData2));
        BDDMockito.when(variantOptionQualifierData2.getQualifier()).thenReturn("productSize");
        BDDMockito.when(variantOptionQualifierData2.getValue()).thenReturn("productSizeName2");

        BDDMockito.when(Integer.valueOf(comparator.compare("productSizeName1", "productSizeName2"))).thenReturn(
                Integer.valueOf(1));
        final int compared = sortStrategy.compare(variantOptionData1, variantOptionData2);
        Assertions.assertThat(compared).isEqualTo(1);

        Mockito.verify(variantOptionQualifierData1).getValue();
        Mockito.verify(variantOptionQualifierData2).getValue();
    }

    @Test
    public void testCompareWithQualifierInstanceofTargetQualifier() {
        final List<VariantOptionQualifierData> qualifier1 = new ArrayList<>();
        qualifier1.add(targetVariantOptionQualifierData1);
        BDDMockito.when(variantOptionData1.getVariantOptionQualifiers()).thenReturn(
                qualifier1);
        BDDMockito.when(targetVariantOptionQualifierData1.getQualifier()).thenReturn("productSize");
        BDDMockito.when(targetVariantOptionQualifierData1.getObjectValue()).thenReturn("productSizeName1");

        final List<VariantOptionQualifierData> qualifier2 = new ArrayList<>();
        qualifier2.add(targetVariantOptionQualifierData2);
        BDDMockito.when(variantOptionData2.getVariantOptionQualifiers()).thenReturn(qualifier2);
        BDDMockito.when(targetVariantOptionQualifierData2.getQualifier()).thenReturn("productSize");
        BDDMockito.when(targetVariantOptionQualifierData2.getObjectValue()).thenReturn("productSizeName2");

        BDDMockito.when(Integer.valueOf(comparator.compare("productSizeName1", "productSizeName2"))).thenReturn(
                Integer.valueOf(1));
        final int compared = sortStrategy.compare(variantOptionData1, variantOptionData2);
        Assertions.assertThat(compared).isEqualTo(1);

        Mockito.verify(targetVariantOptionQualifierData1).getObjectValue();
        Mockito.verify(targetVariantOptionQualifierData2).getObjectValue();
    }
}

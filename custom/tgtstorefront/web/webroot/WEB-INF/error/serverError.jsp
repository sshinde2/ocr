<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
<title>Hmm... That&rsquo;s not supposed to happen</title>
<style>body {font-family: monospace;} .st {padding-left: 30px;} h1 {color:#ba0000}</style>
</head>
<body>
<h1>Hmm... That&rsquo;s not supposed to happen</h1>
<c:set var="exception" value="${requestScope['javax.servlet.error.exception']}"/>
<c:if test="${not empty exception.cause and not empty exception.cause.message}">
	<c:set var="exceptionCause" value="${exception.cause}"/>
</c:if>
<c:set var="strackTraceLineLimit" value="${ empty exceptionCause ? 60 : 30}" />


<h3>Error: ${exception['class']} &ndash; ${exception.message}</h3>
<div class="st">
	<c:set var="stLength" value="${fn:length(exception.stackTrace)}" />
	<c:forEach items="${exception.stackTrace}" var="stackTraceLine" begin="0" end="${strackTraceLineLimit}">
		${stackTraceLine}<br>
	</c:forEach>
	<c:if test="${stLength > strackTraceLineLimit}">
	 	... truncated (view logs) ...
	</c:if>
</div>

<c:if test="${ not empty exceptionCause and not empty exceptionCause.message}">
	<h3>Cause: ${exceptionCause['class']} &ndash; ${exceptionCause.message}</h3>
	<div class="st">
		<c:set var="stCauseLength" value="${fn:length(exceptionCause.stackTrace)}" />
		<c:forEach items="${exceptionCause.stackTrace}" var="stackTraceLine" begin="0" end="${strackTraceLineLimit}">
			${stackTraceLine}<br>
		</c:forEach>
		<c:if test="${stCauseLength > strackTraceLineLimit}">
	 		... truncated (view logs) ...
		</c:if>
	</div>
</c:if>

</body>
</html>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="customer-payment" tagdir="/WEB-INF/tags/desktop/customer/payment" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>

	<c:if test="${not empty paymentMethods}">
		<h3><spring:theme code="checkout.multi.paymentDetails.newPaymentDetailsOr" /></h3>
	</c:if>
	
	<ul class="block-list card-list">
		<customer-payment:paymentFormVendor />		
		<customer-payment:paymentFormNew />
	</ul>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>

<form:form method="post" commandName="mumsHubPersonaliseForm" novalidate="true" cssClass="mumshub-personalise-form">
	<h3 class="section-head"><spring:theme code="mumshub.formHead" /></h3>
	<spring:hasBindErrors name="mumsHubPersonaliseForm">
		<c:set var="hasErrors" value="${true}" />
	</spring:hasBindErrors>

	<p class="required"><spring:theme code="form.mandatory.message"/></p>
	<formElement:formSelectBox idKey="mumshub-title" labelKey="mumshub.title" path="titleCode" mandatory="true" skipBlank="false"  skipBlankMessageKey="form.select.empty" items="${titles}"/>
	<formElement:formInputBox idKey="mumshub-firstName" labelKey="mumshub.firstName" path="firstName" inputCSS="text" mandatory="true" maxlength="32" />
	<formElement:formInputBox idKey="mumshub-lastName" labelKey="mumshub.lastName" path="lastName" inputCSS="text" mandatory="true" maxlength="32" />
	<formElement:formRadios idKey="mumshub-gender" labelKey="mumshub.gender" path="gender" items="${genders}" />
	<formElement:formThreePartDate idKey="mumshub-dateOfBirth" labelKey="mumshub.dateOfBirth" skipBlankMessageKey="mumshub.dateOfBirth.option" path="dateOfBirth" years="${birthYears}" />

	<h5><spring:theme code="mumshub.currentlyPregnant" /></h5>
	<formElement:formThreePartDate idKey="mumshub-dueDate" labelKey="mumshub.dueDate" skipBlankMessageKey="mumshub.dueDate.option" path="dueDate" years="${dueDateYears}"/>
	<formElement:formRadios idKey="mumshub-expected-gender" labelKey="mumshub.expectedGender" path="expectedGender" items="${expectedBabyGenders}" />

	<h3 class="section-head"><spring:theme code="mumshub.familyHead" /></h3>
	<div class="mumshub-kids">
		<c:forEach var="index" begin="0" end="4" varStatus="status">
			<div class="kid-details ${not status.first and not hasErrors ? 'kid-hidden' : ''}">
				<h4 class="kid-head ${status.first ? 'kid-head-first' : ''}"><spring:theme code="mumshub.childHead" arguments="${status.count}"/></h4>
				<formElement:formInputBox idKey="mumshub-childsName-${index}" labelKey="mumshub.childsName" path="children[${index}].firstName" inputCSS="text" mandatory="false" type="email" autoCorrect="off"/>
				<formElement:formRadios idKey="mumshub-gender-${index}" labelKey="mumshub.gender" path="children[${index}].gender" items="${babyGenders}" />
				<formElement:formThreePartDate idKey="mumshub-dateOfBirth-${index}" labelKey="mumshub.dateOfBirth" skipBlankMessageKey="mumshub.dateOfBirth.option" path="children[${index}].dateOfBirth" years="${birthYears}"/>
			</div>
		</c:forEach>
		<c:if test="${not hasErrors}">
			<button class="add-kid button-norm" type="button"><span class="icon">+&nbsp;</span><spring:theme code="mumshub.addChild" /></button>
		</c:if>
	</div>
	<button type="submit" class="button-fwd"><spring:theme code="mumshub.pesonalise.submit" /><spring:theme code="icon.right-arrow-small" /></button>

</form:form>

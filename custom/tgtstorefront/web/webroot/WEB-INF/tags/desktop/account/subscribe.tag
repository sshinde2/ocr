<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String" %>
<%@ attribute name="titleKey" required="false" type="java.lang.String" %>
<%@ attribute name="descriptionKey" required="false" type="java.lang.String" %>
<%@ attribute name="beforeFields" required="false" fragment="true" %>
<%@ attribute name="afterFields" required="false" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<form:form method="post" commandName="ENewsSubscriptionForm" cssClass="single-action enews-subscription-form" novalidate="true">
	<div class="f-narrow register-form has-select ${panelClass}">
		<jsp:invoke fragment="beforeFields"/>
		<formElement:formSelectBox idKey="enews.title" labelKey="enews.title" path="title" mandatory="true" skipBlank="false"  skipBlankMessageKey="form.select.empty" items="${titles}"/>
		<formElement:formInputBox idKey="enews.firstName" labelKey="enews.firstName" path="firstName" inputCSS="text" mandatory="true" maxlength="32" />
		<formElement:formInputBox idKey="enews.lastName" labelKey="enews.lastName" path="lastName" inputCSS="text" mandatory="true" maxlength="32" />
		<formElement:formInputBox idKey="enews.email" labelKey="enews.email" path="email" inputCSS="text enews-quick-email" mandatory="true" type="email" autoCorrect="off"/>
		<form:hidden path="subscriptionSource"/>
		<p class="required"><spring:theme code="form.mandatory.message"/></p>
	</div>
	<jsp:invoke fragment="afterFields"/>
	<button type="submit" class="button-fwd"><spring:theme code='${actionNameKey}'/><spring:theme code="icon.right-arrow-small" /></button>
	<target:csrfInputToken/>
</form:form>

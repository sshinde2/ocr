<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ attribute name="heading" required="false" type="java.lang.String" %>

<common:summaryPanel>
	<dl>
		<c:if test="${not empty title.name}">
			<dt><spring:theme code="profile.title"/></dt>
			<dd>${fn:escapeXml(title.name)}</dd>
		</c:if>
		<dt><spring:theme code="profile.firstName"/></dt>
		<dd>${fn:escapeXml(customerData.firstName)}</dd>
		<dt><spring:theme code="profile.lastName"/></dt>
		<dd>${fn:escapeXml(customerData.lastName)}</dd>
		<dt><spring:theme code="profile.email"/></dt>
		<dd>${fn:escapeXml(customerData.uid)}</dd>
	</dl>
</common:summaryPanel>
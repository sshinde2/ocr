<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="heartType" required="false" type="java.lang.String" %>
<%@ attribute name="useClass" required="false" type="java.lang.String" %>

<%-- Default view box --%>
<c:set var="viewBoxValues" value="-1 -1 40 40" />
<c:set var="href" value="#FavHeartIcon" />

<c:choose>
	<c:when test="${heartType eq 'header'}">
		<c:set var="viewBoxValues" value="0 0 45 45" />
	</c:when>
	<c:when test="${heartType eq 'button'}">
		<c:set var="viewBoxValues" value="4 5 32 30" />
	</c:when>
	<c:when test="${heartType eq 'fatHeart'}">
		<c:set var="viewBoxValues" value="0 0 32 30" />
		<c:set var="href" value="#FatFavHeartIcon" />
	</c:when>
	<c:when test="${heartType eq 'miniFatHeart'}">
		<c:set var="viewBoxValues" value="-12 -14 54 52" />
		<c:set var="href" value="#FatFavHeartIcon" />
	</c:when>
</c:choose>

<svg viewBox="${viewBoxValues}">
	<use xlink:href="${href}" class="${useClass}"></use>
</svg>

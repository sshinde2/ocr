<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entry" required="false" type="java.lang.Object" %>
<%@ attribute name="product" required="false" type="java.lang.Object" %>
<%@ attribute name="quantity" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<c:if test="${not empty entry}">
	<c:set var="product" value="${entry.product}" />
	<c:set var="quantity" value="${entry.quantity}" />
</c:if>

<c:url value="${product.url}" var="entryProductUrl"/>
<div class="cart-entry">
	<div class="entry-image">
		<a href="${entryProductUrl}">
			<product:productPrimaryImage product="${product}" format="thumbnail"/>
		</a>
	</div>
	<div class="entry-info">
		<product:productSpecSummary product="${product}" quantity="${quantity}" renderLink="true" showGiftCardSpec="${true}" />
	</div>
	<div class="entry-price">
		<format:price priceData="${entry.basePrice}"/>
	</div>
</div>

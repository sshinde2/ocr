<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="modifyEntry" required="false" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="giftRecipientNumber" required="false" type="java.lang.Integer" %>
<%@ attribute name="recipient" required="true" type="au.com.target.tgtfacades.order.data.GiftRecipientData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:disabled name="featureEditGiftcardRecipientDetails">
	<form action="/basket/update-giftcards" method="post">
		<input type="hidden" name="entryNumber" value="${modifyEntry.entryNumber}"/>
		<input type="hidden" name="id" value="${recipient.id}"/>
		<input type="hidden" name="productCode" value="${modifyEntry.product.code}"/>
		<input type="hidden" name="giftRecipientNumber" value="${giftRecipientNumber}"/>
		<button type="submit" name="action" value="remove" class="button-norm button-subtle Recipient-btnRemove">
			<spring:theme code="basket.page.remove"/>&nbsp;<spring:theme code="icon.plain-cross"/>
		</button>
		<target:csrfInputToken/>
	</form>
</feature:disabled>

<feature:enabled name="featureEditGiftcardRecipientDetails">

	<jsp:useBean id="updateGiftCardForm" class="au.com.target.tgtstorefront.forms.UpdateGiftCardForm" scope="request" />
    <c:set target="${updateGiftCardForm}" property="giftRecipientForm" value="${target:getUpdatedRecipientForm(recipient)}"/>
	<form:form method="post" class="Recipient-form" commandName="updateGiftCardForm" id="updateGiftCardForm${giftRecipientNumber}" action="/basket/update-giftcards">
		<input type="hidden" name="entryNumber" value="${modifyEntry.entryNumber}"/>
		<input type="hidden" name="id" value="${recipient.id}"/>
		<input type="hidden" name="productCode" value="${modifyEntry.product.code}"/>
		<input type="hidden" name="giftRecipientNumber" value="${giftRecipientNumber}"/>
		<div class="Recipient-controls">
			<button type="submit" name="action" value="remove" class="button-norm button-subtle Recipient-btnRemove">
				<spring:theme code="basket.page.remove"/>&nbsp;<spring:theme code="icon.plain-cross"/>
			</button>
			<button type="button" class="button-norm button-subtle Recipient-btnEdit">
				<spring:theme code="basket.page.editcard"/>
			</button>
		</div>
		<div class="Recipient-editor">
			<product:productGiftRecipientForm modify="true">
				<div class="Recipient-editorButtons">
					<button type="button" class="button-norm Recipient-btnCancel">
						<spring:theme code="basket.page.cancel"/>
					</button>
					<button type="submit" name="action" value="update"  class="button-fwd Recipient-btnSave">
						<spring:theme code="basket.page.save"/>
					</button>
				</div>
			</product:productGiftRecipientForm>
		</div>
		<target:csrfInputToken/>
	</form:form>
</feature:enabled>

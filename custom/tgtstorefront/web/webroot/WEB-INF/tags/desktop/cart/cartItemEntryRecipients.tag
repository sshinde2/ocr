<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="modifyEntry" required="false" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="recipients" required="true" type="java.util.List" %>
<%@ attribute name="disabledMessage" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:if test="${not empty recipients}">
	<ul class="recipients">
		<c:forEach items="${recipients}" var="recipient" varStatus="recipientStatus">
			<li class="Recipient">
				<cart:cartItemEntryRecipientSummary recipient="${recipient}" />
				<c:if test="${not empty modifyEntry}">
					<cart:cartItemEntryModifyRecipient giftRecipientNumber="${recipientStatus.index}" modifyEntry="${modifyEntry}" recipient="${recipient}" />
				</c:if>
			</li>
		</c:forEach>
	</ul>
</c:if>

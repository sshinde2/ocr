<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<form:form class="updateCartForm"
	id="removeCartForm${entry.entryNumber}"
	action="${targetUrlsMapping.cartUpdate}"
	method="post"
	commandName="updateQuantityForm${entry.product.code}"
	data-initial-quantity="${entry.quantity}"
	data-product-code="${entry.product.code}"
	novalidate="true">
	<jsp:doBody />
	<input type="hidden" name="quantity" value="0"/>
	<c:if test="${entry.updateable}">
		<c:set var="waitText"><spring:theme code="basket.page.remove"/></c:set>
		<button type="submit" name="action" value="remove" class="button-norm button-subtle cust-state-trig button-wait basket-remove" data-wait-text="${waitText}">
			<spring:theme code="basket.page.remove"/>&nbsp;<spring:theme code="icon.plain-cross"/></button>
		<c:remove var="waitText" />
	</c:if>
	<target:csrfInputToken/>
</form:form>


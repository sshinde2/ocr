<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<spring:eval var="PREORDER_AVAILABLE" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).PREORDER_AVAILABLE"/>

<c:set var="dealsHelp"><cart:dealsHelp /></c:set>

<feature:enabled name="featureZipEnabled">
	<c:set var="featureZipEnabled" value="${true}" />
</feature:enabled>

<feature:enabled name="featureAfterpayEnabled">
	<c:set var="featureAfterpayEnabled" value="${true}" />
</feature:enabled>

<table class="basket-table">
	<thead>
		<tr>
			<th id="entry-description" class="head-description" colspan="2"><spring:theme code="basket.page.header.description"/></th>
			<th id="entry-quantity" class="head-quantity"><spring:theme code="basket.page.header.quantity"/></th>
			<th id="entry-price" class="head-price"><spring:theme code="basket.page.header.itemPrice"/></th>
			<th id="entry-total" class="head-total"><spring:theme code="basket.page.header.total"/></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${cartData.entries}" var="entry">
			<c:set var="excludedPaymentPartners">
				<c:if test="${(entry.product.excludeForZipPayment or entry.product.productDisplayType eq PREORDER_AVAILABLE) and featureZipEnabled}">
					<spring:theme code="lightbox.anchor" arguments="Zip;;${zipPaymentModal}" argumentSeparator=";;" />;;
				</c:if>
				<c:if test="${(entry.product.excludeForAfterpay or entry.product.productDisplayType eq PREORDER_AVAILABLE) and featureAfterpayEnabled}">
					<spring:theme code="lightbox.anchor" arguments="Afterpay;;${afterpayModal}" argumentSeparator=";;" />
				</c:if>
			</c:set>
			<c:set var="potentialPromotions" value="${target:getPotentialPromotionForCartEntry(cartData, entry.entryNumber)}" />
			<c:set var="appliedPromotions" value="${target:getAppliedPromotionForEntry(cartData, entry.entryNumber)}" />
			<c:set var="deliveryConcerns" value="${entry.product.productDeliveryToStoreOnly or entry.product.bigAndBulky or entry.product.productTypeCode eq 'giftCard'}" />
			<c:set var="showExcludePaymentMessage" value="${(fn:length(excludedPaymentPartners) > 0)}" />
			<c:set var="showExtras" value="${(fn:length(potentialPromotions) > 0) or (fn:length(appliedPromotions) > 0) or deliveryConcerns or (not empty entry.product.productPromotionalDeliveryText) or not empty entry.recipients or showExcludePaymentMessage}" />
			<c:set var="hiddenInputs">
				<input type="hidden" name="entryNumber" value="${entry.entryNumber}"/>
				<input type="hidden" name="productCode" value="${entry.product.code}"/>
				<input type="hidden" name="initialQuantity" value="${entry.quantity}"/>
			</c:set>
			<c:url value="${entry.product.url}" var="productUrl"/>
			<tr class="${showExtras ? 'row-combined' : ''}">
				<td headers="entry-description" class="thumb">
					<a href="${productUrl}">
						<product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
					</a>
				</td>
				<td headers="entry-description" class="description">
					<product:productSpecSummary product="${entry.product}" renderLink="true"/>
					<c:if test="${(not entry.product.giftCard) or (entry.product.giftCard and entry.product.productTypeCode eq 'giftCard')}">
						<cart:cartItemEntryRemove entry="${entry}">
							<c:out value="${hiddenInputs}" escapeXml="false" />
						</cart:cartItemEntryRemove>
					</c:if>
				</td>
				<td headers="entry-quantity" class="quantity">
					<c:choose>
						<c:when test="${entry.product.giftCard and entry.product.productTypeCode ne 'giftCard'}">
							<span class="text without-controls">${entry.quantity}<span>
						</c:when>
						<c:otherwise>
							<cart:cartItemEntryUpdate entry="${entry}">
								<c:out value="${hiddenInputs}" escapeXml="false" />
							</cart:cartItemEntryUpdate>
						</c:otherwise>
					</c:choose>
				</td>
				<td headers="entry-price" class="price">
					<format:price priceData="${entry.basePrice}" displayFreeForZero="true"/>
					<c:if test="${not empty entry.discountPrice}">
						<br /><span class="deals-applied">-<format:price priceData="${entry.discountPrice}" displayFreeForZero="false"/></span>
					</c:if>
				</td>

				<td headers="entry-total" class="total">
					<format:price priceData="${entry.totalPrice}" displayFreeForZero="true"/>
				</td>
			</tr>

			<c:if test="${showExtras}">
				<tr class="entry-extras">
					<td class="thumb"></td>
					<td class="extras" colspan="6">
						<cart:cartItemEntryRecipients recipients="${entry.recipients}" modifyEntry="${entry}" />
						<c:if test="${not kioskMode and showExcludePaymentMessage}">
							<p class="variant-spec-warning">
								<spring:theme code="icon.exclamation" />
								<spring:theme code="product.variants.partner.unavailable"/>
								<c:forTokens items="${excludedPaymentPartners}" var="paymentPartner" varStatus="status" delims=";;">
									<c:if test="${not status.first}">
										<spring:theme code="text.or" />
									</c:if>
									${paymentPartner}
								</c:forTokens>
							</p>
						</c:if>
						<product:productDeliveryConcern product="${entry.product}" />
						<c:forEach items="${appliedPromotions}" var="promotion" >
							<p class="deals ${promotion.canHaveMoreRewards ? 'deals-potential' : 'deals-applied'}"><spring:theme code="${promotion.canHaveMoreRewards ? 'icon.exclamation' : 'icon.tick'}" />${promotion.description}${dealsHelp}</p>
						</c:forEach>
						<c:forEach items="${potentialPromotions}" var="promotion" >
							<p class="deals deals-potential"><spring:theme code="icon.exclamation" />${promotion.description}${dealsHelp}</p>
						</c:forEach>
						<c:if test="${not empty entry.product.productPromotionalDeliveryText}">
							<div class="delivery-promotional-message"><p><spring:theme code="icon.delivery" />${entry.product.productPromotionalDeliveryText}</p></div>
						</c:if>
					</td>
				</tr>
			</c:if>
		</c:forEach>
	</tbody>
</table>

<script type="text/x-handlebars-template" id="xhr-error-feeback">
	<feedback:message type="error" size="small">
		<p><spring:theme code="xhr.error.occurred" /></p>
	</feedback:message>
</script>


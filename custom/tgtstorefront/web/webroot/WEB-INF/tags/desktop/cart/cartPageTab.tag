<%@ tag body-content="empty" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>

<spring:message code="basket.add.to.cart" var="basketAddToCart"/>
<spring:theme code="cart.page.checkout" var="checkoutText"/>

<c:if test="${not empty cartData.entries}">
	<cart:cartItems cartData="${cartData}" />
	<c:if test="${not kioskMode}">
		<div class="${fn:length(cartData.entries) le 2 ? 'only-for-small' : ''}">
			<cart:cartNextAction cartData="${cartData}"/>
		</div>
	</c:if>
	<div class="basket-middle">
		<cms:slot var="middle" contentSlot="${slots['MiddleContent']}">
			<cms:component component="${middle}"/>
		</cms:slot>
	</div>
	<div class="basket-synopsis">
		<div class="basket-supplement">

			<%-- Check for special content form the controller --%>
			<c:if test="${not empty deliveryTermsComponent or not empty cNCErrorMessageComponent}">
				<div class="basket-info">
					<%-- Delivery Content Slot (with aus post price injection) --%>
					${deliveryTermsComponent}

					<%-- Does the basket entries not eligible for CNC --%>
<%--
					<c:if test="${not cartData.eligibleCnc}">
						<feedback:message icon="false">
							<h6>${cNCErrorMessageComponent}</h6>
						</feedback:message>
					</c:if>
quick and nasty till code we do UI work later
 --%>
				</div>
			</c:if>

			<div class="basket-summary">
				<cart:cartTotals cartData="${cartData}"/>
			</div>

		</div>
	</div>
	<cart:cartNextAction cartData="${cartData}"/>

</c:if>

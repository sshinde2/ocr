<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="false" type="au.com.target.tgtfacades.order.data.TargetCartData" %>
<%@ attribute name="orderData" required="false" type="au.com.target.tgtfacades.order.data.TargetOrderData" %>
<%@ attribute name="showEntries" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>



<%-- Choose Data --%>
<c:set var="absoluteOrderData" value="${not empty orderData ? orderData : cartData}" />
<c:set var="isConfirmation" value="${not empty orderData}" />

<c:set var="oddClass" value="odd" />
<c:set var="evenClass" value="even" />
<c:set var="rowClass" value="${oddClass}" />
<c:set var="cartHasPreOrder" value="${not empty absoluteOrderData.preOrderOutstandingAmount}" />
<feature:enabled name="featureZipEnabled">
	<c:set var="featureZipEnabled" value="${true}" />
</feature:enabled>
<feature:enabled name="featureAfterpayEnabled">
	<c:set var="featureAfterpayEnabled" value="${true}" />
</feature:enabled>

<spring:theme code="checkout.summary.updating" var="summaryUpdatingText" text="Updating..." />
<spring:theme code="checkout.summary.error" var="summaryErrorText" text="Unable to update at this time." />

<c:if test="${not empty absoluteOrderData}">

	<table class="summary-table cart-summary-table ${cartHasPreOrder ? 'summary-table--preorder' : ''}" data-updating-text="${summaryUpdatingText}" data-error-text="${summaryErrorText}">
		<c:if test="${showEntries}">
			<thead>
				<tr class="hide-for-small">
					<th colspan="2" id="entry-item" class="item"><spring:theme code="basket.page.entry.item"/></th>
					<th id="entry-quantity" class="quantity"><spring:theme code="basket.page.entry.quantity"/></th>
					<th id="entry-price" class="price"><spring:theme code="basket.page.entry.price"/></th>
				</tr>
			</thead>
		</c:if>
		<tbody>
			<c:if test="${showEntries}">
				<c:forEach items="${absoluteOrderData.entries}" var="entry">
					<c:url value="${entry.product.url}" var="productUrl"/>
					<c:set var="appliedPromotions" value="${target:getAppliedPromotionForEntry(absoluteOrderData, entry.entryNumber)}" />

					<c:set var="extras">
						<cart:cartItemEntryRecipients recipients="${entry.recipients}" disabledMessage="true" />
						<c:forEach items="${appliedPromotions}" var="promotion" >
							<p class="deals deals-applied"><spring:theme code="icon.tick" />${promotion.description}</p>
						</c:forEach>
					</c:set>
					<c:set var="showExtras" value="${not empty extras}" />

					<tr class="product-entry hide-for-small ${showExtras ? 'row-combined' : ''}">
						<td colspan="2" headers="entry-item" class="item">
							<product:productSpecSummary product="${entry.product}" showCode="false" />
						</td>
						<td headers="entry-quantity" class="quantity">
							${entry.quantity}
						</td>
						<td headers="entry-price" class="price">
							<format:price priceData="${entry.basePrice}" displayFreeForZero="true"/>
							<c:if test="${not empty entry.discountPrice}">
								<br /><span class="deals-applied">-<format:price priceData="${entry.discountPrice}" displayFreeForZero="false"/></span>
							</c:if>
						</td>
					</tr>

					<c:if test="${showExtras}">
						<tr class="entry-extras hide-for-small">
							<td colspan="4">${extras}</td>
						</tr>
					</c:if>
				</c:forEach>

				<tr class="summary-row row-modifier only-for-small ${rowClass}">
					<td colspan="4">
						<spring:theme code="basket.page.entry.hidden.message" /> <a href="#summary" class="summary-reveal-items"><spring:theme code="basket.page.entry.hidden.link" /></a>
					</td>
				</tr>
				<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />
			</c:if>

			<%-- Product Discounts --%>
			<c:if test="${absoluteOrderData.productDiscounts.value > 0}">
				<tr class="summary-row row-discount">
					<th colspan="2" id="st-discounts" class="title">
						<spring:theme code="basket.page.totals.product.discounts"/>
					</th>
					<td colspan="2" headers="st-discounts" class="price">
						-&nbsp;<format:price priceData="${absoluteOrderData.productDiscounts}"/>
					</td>
				</tr>
			</c:if>

			<%-- Sub Total --%>
			<tr class="summary-row row-subtotal ${rowClass}">
				<th colspan="2" id="st-subtotal" class="title">
					<spring:theme code="basket.page.totals.subtotal"/>
				</th>
				<td colspan="2" headers="st-subtotal" class="price">
					<format:price priceData="${absoluteOrderData.subTotal}"/>
				</td>
			</tr>
			<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />

			<%-- Delivery --%>
			<c:if test="${not empty absoluteOrderData.deliveryCost}">
				<tr class="summary-row row-modifier delivery ${rowClass}">
					<th colspan="2" id="st-delivery" class="title">
						<c:choose>
							<c:when test="${not empty absoluteOrderData.deliveryMode}">
								<spring:theme code="basket.page.totals.delivery.${absoluteOrderData.deliveryMode.code}"/>
							</c:when>
							<c:otherwise>
								<spring:theme code="basket.page.totals.delivery"/>
							</c:otherwise>
						</c:choose>
					</th>
					<td colspan="2" headers="st-delivery" class="price">
						<format:price priceData="${absoluteOrderData.deliveryCost}" displayFreeForZero="TRUE"/>
					</td>
				</tr>
				<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />
			</c:if>

			<%-- Order Discounts  --%>

			<c:if test="${absoluteOrderData.orderDiscounts.value > 0}">
				<spring:theme code="basket.page.totals.order.discounts${ not empty absoluteOrderData.flybuysDiscountData ? '.flybuys' : '' }"
					var="discountLabel"
					arguments="${absoluteOrderData.flybuysDiscountData.pointsConsumed}"
					argumentSeparator=";;;"/>

				<tr class="summary-row row-discount">
					<th colspan="2" id="st-discounts" class="title">
						${discountLabel}
					</th>
					<td colspan="2" headers="st-discounts" class="price">
						-&nbsp;<format:price priceData="${absoluteOrderData.orderDiscounts}"/>
					</td>
				</tr>
			</c:if>

			<%-- Total Row Class Logic --%>
			<c:set var="rowClassTotal" value="row-total" />

			<%-- Total --%>
			<tr class="summary-row ${rowClassTotal}">
				<th colspan="2" id="st-total" class="title">
					<spring:theme code="basket.page.totals.total"/>
				</th>
				<td colspan="2" headers="st-total" class="price">
					<format:price priceData="${absoluteOrderData.totalPrice}"/>
				</td>
			</tr>

			<%-- GST --%>
			<tr class="summary-row row-modifier gst ${rowClass}">
				<th colspan="2" id="st-gst" class="title">
					<spring:theme code="basket.page.totals.gstTax"/>
				</th>
				<td colspan="2" headers="st-gst" class="price">
					<format:price priceData="${absoluteOrderData.totalTax}"/>
				</td>
			</tr>

			<c:if test="${not kioskMode}">
				<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />
				<c:set var="orderValue" value="${absoluteOrderData.totalPrice.value}" />
				<c:set var="excludedPaymentPartners">
					<c:if test="${(absoluteOrderData.excludeForZipPayment or cartHasPreOrder) and featureZipEnabled}">
						<spring:theme code="lightbox.anchor" arguments="Zip;;${zipPaymentModal}" argumentSeparator=";;" />;;
					</c:if>
					<c:if test="${(absoluteOrderData.excludeForAfterpay or cartHasPreOrder) and featureAfterpayEnabled}">
						<spring:theme code="lightbox.anchor" arguments="Afterpay;;${afterpayModal}" argumentSeparator=";;" />
					</c:if>
				</c:set>
				<c:set var="showExcludePaymentMessage" value="${(fn:length(excludedPaymentPartners) > 0)}" />
				<c:set var="availablePaymentPartners">
					<c:if test="${not absoluteOrderData.excludeForZipPayment and not cartHasPreOrder and featureZipEnabled}">
						<c:set var="isOutsideRange" value="${orderValue gt zipPaymentThresholdAmount.value}" />
						<th colspan="2" class="zippayment">
							<spring:theme code="zippayment.logo.small" />
							<spring:theme code="zippayment.messages.learnmore" arguments="${zipPaymentModal}" />
						</th>
						<td colspan="2" class="zippayment">
							<c:choose>
								<c:when test="${isOutsideRange}">
									<spring:theme code="zippayment.messages.availability" />
								</c:when>
								<c:otherwise>
									<spring:theme code="zippayment.messages.available" />
								</c:otherwise>
							</c:choose>
						</td>;;
					</c:if>
					<c:if test="${not absoluteOrderData.excludeForAfterpay and not cartHasPreOrder and featureAfterpayEnabled}">
						<c:set var="isOutsideRange" value="${orderValue lt afterpayConfig.minimumAmount.value or orderValue gt afterpayConfig.maximumAmount.value}" />
						<th colspan="2" class="afterpay ${isOutsideRange ? 'afterpay--notAvailable' : ''}">
								<spring:theme code="afterpay.logo.small" />
								<spring:theme code="afterpay.messages.learnmore" arguments="${afterpayModal}" />
						</th>
						<td colspan="2" class="afterpay ${isOutsideRange ? 'afterpay--notAvailable' : ''}">
							<c:choose>
								<c:when test="${isOutsideRange}">
									<c:set var="minValue"><format:price priceData="${afterpayConfig.minimumAmount}" stripDecimalZeros="${true}" /></c:set>
									<c:set var="maxValue"><format:price priceData="${afterpayConfig.maximumAmount}" stripDecimalZeros="${true}" /></c:set>
									<spring:theme code="afterpay.messages.availability" arguments="${minValue}||${maxValue}" argumentSeparator="||" />
								</c:when>
								<c:otherwise>
									<c:set var="repaymentAmount"><format:price priceData="${absoluteOrderData.installmentPrice}" /></c:set>
									<spring:theme code="afterpay.messages.available" arguments="${repaymentAmount}" />
								</c:otherwise>
							</c:choose>
						</td>
					</c:if>
				</c:set>

				<c:forTokens items="${availablePaymentPartners}" var="paymentPartnerRowContent" delims=";;">
					<tr class="summary-row row-modifier ${rowClass}">
						${paymentPartnerRowContent}
					</tr>
				</c:forTokens>
				<c:if test="${showExcludePaymentMessage}">
					<tr class="summary-row row-modifier ${rowClass}">
						<th colspan="4" class="partner-unavailable">
							<c:set var="excludedPaymentPartnersLinks">
								<c:forTokens items="${excludedPaymentPartners}" var="paymentPartner" varStatus="status" delims=";;">
									<c:if test="${not status.first}">
										<spring:theme code="text.and" />
									</c:if>
									${paymentPartner}
									<c:set var="excludePaymentMessageCount">${status.count}</c:set>
								</c:forTokens>
							</c:set>
							<spring:theme code="icon.exclamation" />
							<spring:theme code="paymentpartner.messages.basket.unavailable" arguments="${excludedPaymentPartnersLinks};;${excludePaymentMessageCount eq 1?' a':''};;${excludePaymentMessageCount eq 1?'':'s'}" argumentSeparator=";;"/>
						</th>
					</tr>
				</c:if>
			</c:if>

			<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />

			<%-- Total Savings --%>
			<c:if test="${absoluteOrderData.totalDiscounts.value > 0}">
				<tr class="total-savings">
					<th colspan="2" id="st-total-savings" class="title">
						<spring:theme code="basket.page.totals.totalsavings" />
					</th>
					<td colspan="2" class="price">
						<format:price priceData="${absoluteOrderData.totalDiscounts}"/>
					</td>
				</tr>
			</c:if>

			<c:if test="${cartHasPreOrder}">
				<tr class="summary-row row-deposit ">
					<th colspan="2" id="st-deposit" class="title">
						<spring:theme code="account.page.preorder.deposit"/>
					</th>
					<td headers="st-deposit" class="price">
						<format:price priceData="${absoluteOrderData.preOrderDepositAmount }"/>
					</td>
				</tr>
				<tr class="summary-row row-balance">
					<th colspan="2" id="st-balance" class="title">
						<strong><spring:theme code="account.page.preorder.balance"/></strong>
						<p class="orderDetailsContent orderDetailsContent--disclaimer"><spring:theme code="account.page.preorder.balance.disclaimer"/></p>
					</th>
					<td headers="st-balance" class="price">
						<format:price priceData="${absoluteOrderData.preOrderOutstandingAmount}"/>
					</td>
				</tr>
				<tr>
					<th colspan="4">
						<p class="u-fontSize14 u-lineHeight16 u-marginLeft100 u-alignRight">
							<spring:theme code="icon.info" arguments="u-displayInlineBlock"/>&nbsp;<spring:theme code="basket.page.preorder.paymentOption"/>
						</p>
					</th>
				</tr>
			</c:if>
		</tbody>
	</table>
</c:if>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<spring:theme code="text.dealshelp" var="dealshelp" text="" />
&nbsp;<a href="/modal/promotional-deals-infomation" class="lightbox deals-help" title="${dealshelp}"><span class="visuallyhidden">${dealshelp}</span><spring:theme code="icon.question"  /></a>
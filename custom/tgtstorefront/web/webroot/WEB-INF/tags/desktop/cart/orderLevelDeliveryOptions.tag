<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<c:set var="hasDigital" value="${fn:length(cartData.digitalDeliveryModes) gt 0}" />
<c:set var="hasPhysical" value="${fn:length(cartData.deliveryModes) gt 0}" />

<c:set var="showDeliverySection" value="${hasPhysical}" />
<c:if test="${featuresEnabled.featureGiftCardMixedModeMessages}">
	<c:set var="showDeliverySection" value="${true}" />
</c:if>

<c:if test="${showDeliverySection}">

	<h4><spring:theme code="basket.page.delivery.heading"/></h4>

	<c:if test="${featuresEnabled.featureGiftCardMixedModeMessages}">
		<c:if test="${hasDigital}">
			<cart:digitalDeliveryInfoMessage cartData="${cartData}" mixedMode="${hasDigital and hasPhysical}" />
		</c:if>
	</c:if>

	<c:if test="${isDeliveryModeSelectionByPostcodeEnabled and hasPhysical}">
		<c:set var="missPostcodeMessage"><spring:theme code="deliveryMode.postcode.missing" /></c:set>
		<div class="delivery-mode-postcode" data-submit-postcode-url="/basket/show-delivery-mode" data-change-postcode-url="/basket/remove-delivery-mode" data-miss-postcode-message="${missPostcodeMessage}">
			<div class="delivery-mode-postcode-base ${not empty deliveryPostcode ? '' : 'is-visible'}">
				<form:form method="post" commandName="deliveryModePostcodeForm">
					<formElement:formInputBox idKey="delivery-mode-postcode-input" labelCSS="delivery-mode-postcode-label" labelKey="deliveryMode.postcode.input.heading" path="postcode" inputCSS="text" mandatory="false" checkButton="deliveryMode.postcode.input.submit" buttonCSS="button-submit" pattern="[0-9]*" maxlength="4" buttonAside="true"/>
				</form:form>
			</div>
			<c:if test="${not empty deliveryPostcode}">
				<cart:deliveryModePostcodeChange deliveryPostcode="${deliveryPostcode}"/>
			</c:if>
			<script type="text/x-handlebars-template" id="delivery-mode-postcode-error-feeback">
				<feedback:message type="error" size="small">
				<p><spring:theme code="deliveryMode.postcode.error.occurred" /></p>
				</feedback:message>
			</script>
		</div>
	</c:if>

	<cart:deliveryModeChoice cartData="${cartData}" deliveryPostcode="${deliveryPostcode}"/>

</c:if>

<div class="arrow-link-container hfma">
	<ul class="arrow-bullet-links">
		<li class="inline-title"><spring:theme code="product.moreInfo.title"/></li>
		<cms:slot var="component" contentSlot="${slots.Links}">
			<li><cms:component component="${component}"/></li>
		</cms:slot>
	</ul>
</div>

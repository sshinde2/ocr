<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="showHandle" %>

<div id="mini-cart-popup" class="cart-popup">
	<feature:disabled name="uiStickyHeader"><div class="popup-handle"></div></feature:disabled>
	<div class="popup-content"></div>
</div>

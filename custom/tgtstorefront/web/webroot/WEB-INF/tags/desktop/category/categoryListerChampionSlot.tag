<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ attribute name="slotName" required="true" %>
<%@ attribute name="searchPageData" required="true" type="au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<c:set var="firstPage" value="${searchPageData.pagination.currentPage eq 0}" />

<c:if test="${firstPage and empty brandSearchNavigation}">
	<common:championSlot slotName="${slotName}" />
</c:if>
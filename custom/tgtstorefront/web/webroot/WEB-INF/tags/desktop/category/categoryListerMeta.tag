<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="hideContextualLinks" required="false" type="java.lang.Boolean" %>
<%@ attribute name="searchPageData" required="false" type="au.com.target.tgtfacades.search.data.TargetProductCategorySearchPageData"%>

<c:set var="firstPage" value="${searchPageData.pagination.currentPage eq 0}" />
<c:if test="${not hideContextualLinks and not empty contextualLinks and firstPage}">
	<div class="SupplementMeta">
		<h2 class="SupplementMeta-categoryName">${categoryName}</h2>
		&ndash;
		${contextualLinks}</div>
</c:if>

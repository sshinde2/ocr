<%@ attribute name="cartData" required="false" type="au.com.target.tgtfacades.order.data.TargetCartData" %>
<%@ attribute name="orderData" required="false" type="au.com.target.tgtfacades.order.data.TargetOrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/desktop/checkout" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>

<h2 class="co-heading shopping-heading">Order Summary</h2>
<div class="summary-table-container">
	<cart:cartTotals showEntries="true" cartData="${cartData}" orderData="${orderData}" />
</div>
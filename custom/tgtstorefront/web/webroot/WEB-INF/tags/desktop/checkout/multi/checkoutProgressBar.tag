<%@ tag body-content="empty" %>
<%@ attribute name="steps" required="true" type="java.util.List" %>
<%@ attribute name="stepName" required="true" type="java.lang.String"%>
<%@ attribute name="stepsFinished" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="stepKey" value="${featuresEnabled.featureIPGEnabled or kioskMode ? '' : 'earlyPay'}" />
<c:set var="purchaseOptionCode" value="${ kioskMode ? 'kiosk' : (fn:toLowerCase( not empty orderData ? orderData.purchaseOptionCode : cartData.purchaseOptionCode))}" />
<c:set var="currentStep" />

<div class="checkout-progress">
	<ul class="steps-${fn:length(steps)}">
		<c:forEach items="${steps}" var="checkoutStep" varStatus="status" >
			<c:remove var="stepTitle" />
			<c:remove var="stepLink" />

			<c:choose>
				<c:when test="${checkoutStep.stepName eq stepName}">
				 	<c:set var="stepClassName" value="step-active" />
				 	<c:set var="currentStep" value="${status.count}" />
				</c:when>
				<c:when test="${empty currentStep}">
					<c:set var="stepClassName" value="step-done" />
				</c:when>
				<c:otherwise>
				 	<c:set var="stepClassName" value="step-disabled" />
				</c:otherwise>
			</c:choose>

			<c:if test="${empty currentStep and not stepsFinished}">
			 	<c:set var="stepLink"><a href="${checkoutStep.url}">||</a></c:set>
			 	<c:set var="stepLink" value="${fn:split(stepLink, '||')}"/>
		 	</c:if>

			<c:if test="${not empty stepKey}">
				<spring:theme code="checkout.multi.${checkoutStep.stepName}.${stepKey}" text="" var="stepTitle" />
			</c:if>
			<c:if test="${empty stepTitle}">
				<spring:theme code="checkout.multi.${checkoutStep.stepName}" text="" var="stepTitle" />
			</c:if>

			<li class="step ${stepClassName} ${status.first ? 'first-step' : ''} ${status.last ? 'last-step' : ''}"
				data-step-num="${status.count}"
				data-step-title="${stepTitle}"
				data-purchase-type="${purchaseOptionCode}">

 				<c:if test="${not empty stepLink}">
			 		<c:out value="${stepLink[0]}" escapeXml="false" />
				</c:if>

				<span class="num">${status.count}</span>
				<span class="details ${ stepClassName eq 'step-active' ? 'hide-for-tiny' : 'hide-for-small'}">
					<span class="title">${stepTitle}</span>
				</span>

 				<c:if test="${not empty stepLink}">
					<c:out value="${stepLink[1]}" escapeXml="false" />
				</c:if>
			</li>
		</c:forEach>
	</ul>
</div>

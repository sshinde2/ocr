<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryAddresses" required="true" type="java.util.List" %>
<%@ attribute name="selectedAddressId" required="false" type="java.lang.String" %>
<%@ attribute name="deliveryModeCode" required="true" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<ul class="block-list address-list">
	<c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
		<multi-checkout:deliveryAddressDetails deliveryAddress="${deliveryAddress}" isSelected="${deliveryAddress.id eq selectedAddressId}" count="${status.count}" deliveryModeCode="${deliveryModeCode}"/>
	</c:forEach>	
</ul>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="selectedStore" required="false" type="au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData" %>
<%@ attribute name="deliveryModeCode" required="true" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty clickAndCollectDetailsForm}">

	<form:form method="post" id="cnc-delivery-form" commandName="clickAndCollectDetailsForm" novalidate="true" action="/checkout/your-address">
		<input type="hidden" name="deliveryMode" value="${deliveryModeCode}"/>

		<div class="cnc-details">
			<h3>
				<spring:theme code="checkout.multi.deliveryAddress.myDetails" text="Person Collecting Items"/>
			</h3>
			<p class="required"><spring:theme code="form.mandatory" text="*Mandatory"/></span>

			<formElement:formSelectBox idKey="cnc-title" labelKey="address.title" path="title" skipBlank="false" mandatory="false" skipBlankMessageKey="address.title.pleaseSelect" items="${titles}" selectedValue="${clickAndCollectDetailsForm.title}"/>
			<formElement:formInputBox idKey="cnc-firstName" labelKey="address.firstName" path="firstName" inputCSS="text" mandatory="true"/>
			<formElement:formInputBox idKey="cnc-surname" labelKey="address.surname" path="lastName" inputCSS="text" mandatory="true"/>
			<formElement:formInputBox idKey="cnc-mobileNumber" labelKey="address.mobileNumber" path="phoneNumber" inputCSS="text" mandatory="false" type="tel"/>
		</div>

		<spring:bind path="selectedStoreNumber">
			<c:set var="selectedStoreNumberError" value="${not stateError and not empty status.errorMessages}" />

			<spring:theme code="storeFinder.location.unavailable" var="searchLocationUnavailable" />
			<div class="cnc-store-finder ${selectedStoreNumberError ? ' cnc-store-error' : ''}"
				data-location-url="${targetUrlsMapping.cncSearchLocation}"
				data-position-url="${targetUrlsMapping.cncSearchPosition}"
				data-unavailable-text="${searchLocationUnavailable}"
				data-checking-text="Checking"
				data-selected-store-number="${selectedStore.storeNumber}">

				<div class="cnc-store-head">
					<h3 class="cnc-store-heading"><spring:theme code="storeFinder.find.a.store" /></h3>

					<c:if test="${selectedStoreNumberError}">
						<p class="f-err-msg">
							<form:errors path="selectedStoreNumber"/>
						</p>
					</c:if>
				</div>

				<div class="controls-search">
					<div class="control-search-position">
						<button class="control-search-position-button button-norm wide-for-tiny" type="button">
							<spring:theme code="storeFinder.use.my.current.location" />
						</button>
						<div class="or"><spring:theme code="storeFinder.or" /></div>
					</div>

					<div class="control-search-location">
						<spring:theme code="storeFinder.placeholder" var="searchPlaceholder" />
						<label class="visuallyhidden" for="search">${searchPlaceholder}</label>
						<input class="text" id="control-search-location-query" name="control-search-location-query" placeholder="${searchPlaceholder}" maxlength="100" type="text" value="${enteredPostCode}"/>
						<button class="control-search-location-button button-norm">
							<spring:theme code="storeFinder.search" />
						</button>
					</div>
				</div>

				<div class="cnc-store-results">
					<h5 class="cnc-store-results-heading">
						<c:choose>
							<c:when test="${not empty selectedStore}">
								<spring:theme code="storeFinder.selectedStore" text="Your selected store" />
							</c:when>
							<c:when test="${not empty storeList}">
								<spring:theme code="checkout.multi.delivery.storesresults.location" arguments="${fn:length(storeList.results)};;${storeList.locationText}" argumentSeparator=";;" />
							</c:when>
						</c:choose>
					</h5>

					<ul class="cnc-store-results-list block-list">
						<c:choose>
							<c:when test="${not empty selectedStore}">
								<multi-checkout:deliveryStoreDetails store="${selectedStore}" checked="true" />
							</c:when>
							<c:when test="${not empty storeList}">
								<c:forEach items="${storeList.results}" var="store" varStatus="storeStatus">
										<c:if test="${not empty store}">
											<multi-checkout:deliveryStoreDetails store="${store}" checked="true" />
										</c:if>
								</c:forEach>
							</c:when>
						</c:choose>
					</ul>
					
				</div>

			</div>
		</spring:bind>

		<button class="non-js button-fwd" type="submit">Submit</button>

		<target:csrfInputToken/>
	</form:form>
</c:if>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="isolated" required="false"%>
<%@ attribute name="form" type="java.lang.String" %>
<%@ attribute name="path" type="java.lang.String" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>

<spring:theme code="field.flyBuys" var="flybuysText" />
<spring:theme code="field.invalid.notAvailable" arguments="${flybuysText}" var="validationError" />

<c:set var="loggedIn" value="${not empty redeemOptions}" />
<c:set var="labelHelp" value="${form}.${path}.labelHelp" />

<spring:theme code="payment.flybuys.redeem.label.voucher" var="exclusionMessage" />
<spring:theme code="payment.flybuys.redeem.label" var="nonExclusionMessage" />

<c:set var="fieldData">
	data-login-url="${targetUrlsMapping.paymentFlybuysLogin}"
	data-validate-url="${targetUrlsMapping.paymentFlybuysValidate}"
	data-select-url="${targetUrlsMapping.paymentFlybuysSelectOption}"
	data-validation-error="${validationError}"
	data-logged-in="${loggedIn}"
	data-last-valid-num="${cartData.flybuysNumber}"
</c:set>

<c:set var="flybuysLogo">
	<span class="flybuys-logo"><span class="visuallyhidden">flybuys</span></span>
</c:set>

<div class="co-groupe flybuys-control" ${fieldData}>
	<formElement:formInputBox
		idKey="${path}"
		labelKey="${form}.${path}${flybuysRedeemConfigData.active ? '' : '.collect.only'}"
		labelArgs="${flybuysLogo}"
		labelHelp="${flybuysRedeemConfigData.active ? labelHelp : ''}"
		path="${path}"
		elementCSS="f-input-field"
		labelCSS="f-input-label"
		inputCSS="text flybuys-card-num"
		isolated="${empty isolated or isolated}"
		pattern="[0-9]*"
		maxlength="16"
		disabled="${loggedIn}"
		placeholder="${form}.${path}.placeholder" />

		<multi-checkout:flybuysRedeemSummary redeemOptions="${redeemOptions}" />

</div>

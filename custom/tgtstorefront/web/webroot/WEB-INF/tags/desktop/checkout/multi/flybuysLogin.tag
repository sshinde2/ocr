<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:if test="${flybuysRedeemConfigData.active}">
	<form:form method="post" commandName="flybuysLoginForm" cssClass="single-action" novalidate="true">

		<dl class="flybuys-attr">
			<dt><spring:theme code="flybuys.cardNumber.label"/></dt>
			<dd>${flybuysLoginForm.cardNumber}</dd>
		</dl>

		<spring:bind path="flybuysLoginForm">
			<c:if test="${not empty status.errorMessages}">
				<c:set var="dobDetailsErrors">
					${dobDetailsErrors}<span class="f-err-line"><form:errors path="${fieldPath}" /></span>
				</c:set>
				<c:set var="fHiddenError" value="f-hidden-error" />
			</c:if>
		</spring:bind>

		<h4><spring:theme code="flybuys.login.details.heading" /></h4>

		<div class="flybuys-birthdate-area f-element ${not empty dobDetailsErrors ? ' f-error' : ''}">

			<label>
				<spring:theme code="flybuys.birthDay.label"/>
				<formElement:label mandatory="true" />
			</label>

			<div class="f-set">
				<div class="flybuys-dob-select">
					<formElement:formSelectBox idKey="flybuys.birthDay" skipBlankMessageKey="flybuys.options.birthDay" elementCSS="flybuys-birth-day ${fHiddenError}" hideError="true" path="birthDay" items="${daysOfMonth}" dataSearchThreshold="9999" />
				</div>
				<div class="flybuys-dob-select">
					<formElement:formSelectBox idKey="flybuys.birthMonth" skipBlankMessageKey= "flybuys.options.birthMonth" hideError="true" path="birthMonth" elementCSS="flybuys-birth-month ${fHiddenError}" items="${months}" dataSearchThreshold="9999" />
				</div>
				<div class="flybuys-dob-select">
					<formElement:formSelectBox idKey="flybuys.birthYear" skipBlankMessageKey="flybuys.options.birthYear" hideError="true" path="birthYear" elementCSS="flybuys-birth-year ${fHiddenError}" items="${flybuysBirthYears}" dataSearchThreshold="9999" />
				</div>
			</div>

			<c:if test="${not empty dobDetailsErrors}">
				<p class="f-err-msg error-message">${dobDetailsErrors}</p>
			</c:if>
		</div>
		<div class="flybuys-postcode-area">
			<formElement:formInputBox idKey="flybuys.postCode" labelKey="flybuys.postCode.label" path="postCode" inputCSS="text mini-text" mandatory="true" pattern="[0-9]*" autoCorrect="off"/>
		</div>
		<div class="form-actions">
			<button type="button" class="button-norm close-lightbox"><spring:theme code="text.cancel" /></button>
			<button type="submit" class="button-fwd flybuys-login-button"><spring:theme code="flybuys.modalButton.value"/></button>
		</div>
		<target:csrfInputToken/>
	</form:form>
</c:if>
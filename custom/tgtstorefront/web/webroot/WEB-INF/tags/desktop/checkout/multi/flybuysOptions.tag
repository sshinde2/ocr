<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<c:if test="${flybuysRedeemConfigData.active}">
<c:choose>
	<c:when test="${empty redeemOptions.redeemTiers}">
		<feedback:message type="error" size="small" icon="false">
			<h3 class="heading"><spring:theme code="payment.flyBuys.options.notiers" arguments="${flybuysRedeemConfigData.minPointsBalance}" /></h3>
		</feedback:message>
		<form:form method="post" commandName="flybuysRedeemForm" novalidate="true">
			<input type="hidden" name="redeemCode" value="remove" />
			<div class="form-actions">
				<button type="button" class="button-large button-fwd button-norm forward"><spring:theme code="text.close" /></button>
			</div>
			<target:csrfInputToken/>
		</form:form>
	</c:when>
	<c:otherwise>
		<form:form method="post" commandName="flybuysRedeemForm" novalidate="true">
			<p><spring:theme code="payment.flyBuys.options.label" /></p>
			<template:formElement path="redeemCode">
				<c:forEach var="redeemTier" items="${redeemOptions.redeemTiers}" varStatus="status">
					<form:radiobutton id="redeemTier${status.count}" path="redeemCode" value="${redeemTier.redeemCode}" />
					<label for="redeemTier${status.count}">
						<span class="redeem-value"><format:price priceData="${redeemTier.dollars}" isNegative="true" stripDecimalZeros="true" /></span> <spring:theme code="payment.flyBuys.options.points" arguments="${redeemTier.points}" />
					</label>
				</c:forEach>
				<form:radiobutton id="redeemTier0" path="redeemCode" value="remove" />
				<label for="redeemTier0" class="no-redeem">
					<span class="redeem-value"><spring:theme code="payment.flybuys.transaction.zeroValue" /></span> <spring:theme code="payment.flyBuys.options.zero" />
				</label>
			</template:formElement>
			<div class="form-actions">
				<button type="button" class="button-large button-fwd forward"><spring:theme code="payment.flyBuys.options.confirm" /></button>
			</div>
			<target:csrfInputToken/>
		</form:form>
	</c:otherwise>
</c:choose>
</c:if>
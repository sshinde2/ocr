<%@ tag trimDirectiveWhitespaces="true" %>
<%@ attribute name="backCode" required="false" type="java.lang.String" %>
<%@ attribute name="backUrl" required="false" type="java.lang.String" %>
<%@ attribute name="nextCode" required="false" type="java.lang.String" %>
<%@ attribute name="nextUrl" required="false" type="java.lang.String" %>
<%@ attribute name="endCode" required="false" type="java.lang.String" %>
<%@ attribute name="endUrl" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="next-action">
	<c:if test="${not empty backUrl}">
		<a class="continue continue-large hfma" href="${backUrl}">
			<c:set var="backText"><span class="hide-for-tiny"><spring:theme code="${backCode}" /></span></c:set>
			<spring:theme code="icon.left-arrow-small" /><span><spring:theme code="checkout.multi.common.backTo" arguments="${backText}" argumentSeparator=";;;" /></span>
		</a>
	</c:if>
	<c:choose>
		<c:when test="${not empty nextUrl}">
			<a class="button-fwd button-large forward" href="${nextUrl}">
				<spring:theme code="${nextCode}" /><spring:theme code="icon.right-arrow-large" />
			</a>
		</c:when>
		<c:when test="${not empty nextCode}">
			<button type="submit" class="button-fwd button-large forward co-next" id="co-submit-form">
				<spring:theme code="${nextCode}" /><spring:theme code="icon.right-arrow-large" />
			</button>
		</c:when>
	</c:choose>
	<c:if test="${not empty endUrl}">
		<a class="button-fwd button-large forward hfma" href="${endUrl}" id="co-end">
			<spring:theme code="${endCode}" /><spring:theme code="icon.right-arrow-large" />
		</a>
	</c:if>
	<jsp:doBody />
</div>

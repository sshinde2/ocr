<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>

<div class="abstract-fields">
	<multi-checkout:flybuys path="flyBuys" form="payment" isolated="false" />
	<multi-checkout:voucher path="voucher" form="payment" isolated="false" flybuysRedeemApplied="${not empty cartData.flybuysDiscountData}" />
	<multi-checkout:tmd path="teamMemberCode" form="payment" isolated="false" paymentPage="true" />
</div>

<div class="non-js f-buttons">
	<button type="submit" class="button-fwd">Submit</button>
</div>
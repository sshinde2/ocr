<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="order-summary-card" tagdir="/WEB-INF/tags/desktop/order/summaryCard" %>

<order-summary-card:giftCard />

<%--  Delivery Details --%>
<c:choose>
	<c:when test="${not empty cartData.cncStoreNumber and not empty selectedStore}">
		<order-summary-card:storeDetails selectedStore="${selectedStore}" editUrl="${targetUrlsMapping.yourAddress}" />
		<order-summary-card:cncContact addressData="${cartData.deliveryAddress}" />
	</c:when>
	<c:when test="${not empty cartData.deliveryAddress and empty cartData.cncStoreNumber}">
		<order-summary-card:address
			heading="order.summarycard.checkout.heading.deliveryAddress"
			addressData="${cartData.deliveryAddress}"
			editUrl="${targetUrlsMapping.yourAddress}" />
	</c:when>
</c:choose>

<%--  Billing Address --%>
<order-summary-card:address
	heading="order.summarycard.checkout.heading.billingAddress"
	addressData="${cartData.paymentInfo.billingAddress}"
	editUrl="${targetUrlsMapping.payment}" />

<%--  Payment Detail --%>
<order-summary-card:paymentDetails
	paymentsInfo="${cartData.paymentsInfo}"
	flybuysNumber="${cartData.maskedFlybuysNumber}"
	editUrl="${targetUrlsMapping.payment}"
	hidePayment="${kioskMode or cartData.paymentInfo.ipgPaymentInfo}" />

<c:if test="${featuresEnabled.singlePageLoginAndCheckout}">
	<div class="summary-promo">
		<a class="button-norm-border u-text-align-left wide-for-small u-sm-text-align-left button-small" href="${targetUrlsMapping.payment}"><span class="text-with-logo"><spring:theme code="icon.left-arrow-small" /><spring:theme code="order.summarycard.checkout.button.flybuy" /><span class="logo logo-flybuys"></span></span></a>
		<a class="button-norm-border u-text-align-left wide-for-small u-sm-text-align-left button-small" href="${targetUrlsMapping.payment}"><spring:theme code="icon.left-arrow-small" /><spring:theme code="order.summarycard.checkout.button.promotionDiscount" /></a>
	</div>
</c:if>

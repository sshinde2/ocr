<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="order-summary-card" tagdir="/WEB-INF/tags/desktop/order/summaryCard" %>


<%-- Order Status Heading --%>
<c:if test="${not empty orderData.status}">
	<c:set var="lowerCaseStatus" value="${fn:toLowerCase(orderData.status.code)}" />

	<h4 class="status-copy">
		<c:choose>
			<c:when test="${lowerCaseStatus eq 'pending' }">
				<spring:theme code="checkout.multi.thankYou.orderStatus.pending" text="" />
			</c:when>
			<c:otherwise>
				<spring:theme code="${kioskMode ? 'kiosk.' : ''}checkout.multi.thankYou.orderStatus.created" text="" />
			</c:otherwise>
		</c:choose>
	</h4>
</c:if>

<%-- Receipt Details --%>
<order-summary-card:receiptDetails orderData="${orderData}" />

<%-- e-Gift Card Delivery --%>
<order-summary-card:giftCard />

<%--  Delivery Details --%>
<c:choose>
	<c:when test="${not empty orderData.cncStoreNumber and not empty selectedStore}">
		<order-summary-card:storeDetails selectedStore="${selectedStore}" />
		<order-summary-card:cncContact addressData="${orderData.deliveryAddress}" />
	</c:when>
	<c:when test="${not empty orderData.deliveryAddress and empty orderData.cncStoreNumber}">
		<order-summary-card:address
			heading="order.summarycard.checkout.heading.deliveryAddress"
			addressData="${orderData.deliveryAddress}" />
	</c:when>
</c:choose>

<%--  Billing Address --%>
<c:if test="${not empty orderData.paymentInfo.billingAddress}">
	<order-summary-card:address
		heading="order.summarycard.checkout.heading.billingAddress"
		addressData="${orderData.paymentInfo.billingAddress}" />
</c:if>

<%--  Payment Detail --%>
<order-summary-card:paymentDetails
	paymentsInfo="${orderData.paymentsInfo}"
	flybuysNumber="${orderData.maskedFlybuysNumber}"
	hidePayment="${kioskMode}"/>


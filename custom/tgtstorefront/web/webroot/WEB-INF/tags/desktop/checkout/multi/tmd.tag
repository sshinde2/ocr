<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="form" type="java.lang.String" %>
<%@ attribute name="path" type="java.lang.String" %>
<%@ attribute name="paymentPage" type="java.lang.Boolean" %>
<%@ attribute name="isolated" required="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty path}">

	<c:set var="tmdFieldData">data-validate-url="${targetUrlsMapping.paymentValidateTmd}"</c:set>
	
	<spring:bind path="${path}">
		<c:set var="validAndProvided" value="${not empty status.value and empty status.errorMessages}" />
		<c:set var="elementCSS">tmd ${validAndProvided ? 'f-success' : ''}</c:set>
		<formElement:formInputBox idKey="${form}.${path}" 
			labelKey="${form}.${path}" 
			path="${path}" 
			inputCSS="text redeemable-code" 
			isolated="${empty isolated or isolated}" 
			elementCSS="${elementCSS}" 
			checkButton="${form}.teamMemberCode.check" 
			fieldData="${tmdFieldData}" 
			disabled="${validAndProvided}" 
			buttonDisabled="${validAndProvided}" 
			pattern="[0-9]*" 
			maxlength="64" 
			placeholder="${form}.${path}.placeholder" />
	</spring:bind>
	
</c:if>
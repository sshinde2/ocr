<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<c:if test="${otherCarts}">
	<div class="global-messages feedback-call-to-action">
		<feedback:message>
			<p><spring:theme code="checkout.multi.thankYou.otherCart.message"/></p>
			<div class="btns">
				<a href="${targetUrlsMapping.cart}" class="button-fwd button-large"><spring:theme code="checkout.multi.thankYou.otherCart.button"/></a>
			</div>
		</feedback:message>
	</div>
</c:if>

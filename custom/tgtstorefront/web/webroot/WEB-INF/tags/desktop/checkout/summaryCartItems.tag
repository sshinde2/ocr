<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>

<table>
	<thead>
		<tr>
			<th id="entry-title" class="title"><spring:theme code="basket.page.title"/></th>
			<th id="entry-quantity" class="quantity"><spring:theme code="basket.page.quantity"/></th>
			<th id="entry-price" class="price"><spring:theme code="basket.page.price"/></th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${cartData.entries}" var="entry">
			<c:url value="${entry.product.url}" var="productUrl"/>
			<tr>
				<td headers="entry-title" class="title">
					${entry.product.name}
				</td>
				<td headers="entry-quantity" class="quantity">
					${entry.quantity}
				</td>
				<td headers="entry-price" class="price">
					<format:price priceData="${entry.basePrice}" displayFreeForZero="true"/>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
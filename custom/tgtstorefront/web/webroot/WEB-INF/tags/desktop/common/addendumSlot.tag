<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<cms:slot var="component" contentSlot="${slots.Addendum}">
	<c:set var="hasVisibleComponentsAddendum" value="${true}" />
</cms:slot>
<c:if test="${hasVisibleComponentsAddendum}">
	<c:set var="contentSlotContext" value="full" scope="request" />
	<div class="addendum">
		<cms:slot var="component" contentSlot="${slots['Addendum']}">
			<cms:component component="${component}"/>
		</cms:slot>
	</div>
	<c:remove var="contentSlotContext" scope="request" />
</c:if>

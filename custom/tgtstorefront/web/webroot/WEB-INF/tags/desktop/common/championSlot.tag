<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ attribute name="slotName" required="true" %>

<cms:slot var="component" contentSlot="${slots[slotName]}">
	<c:set var="hasVisibleComponentsListerChampion" value="${true}" />
</cms:slot>

<c:if test="${hasVisibleComponentsListerChampion}">
	<div class="champion">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="listerChampionComponent" contentSlot="${slots[slotName]}">
			<cms:component component="${listerChampionComponent}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>
</c:if>
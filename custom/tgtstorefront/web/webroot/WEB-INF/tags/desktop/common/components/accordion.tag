<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="accordionContent" required="true" fragment="true" %>
<%@ attribute name="type" required="true" fragment="java.lang.String" %>
<%@ attribute name="cssClass" required="false" %>

<div class="Accordion ${cssClass}" data-type="${type}">
	<jsp:invoke fragment="accordionContent"/>
</div>

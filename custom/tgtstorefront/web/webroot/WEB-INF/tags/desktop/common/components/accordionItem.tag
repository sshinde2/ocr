<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="accordionItemContent" required="true" fragment="true" %>
<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="type" required="true" type="java.lang.String" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ attribute name="bellowCss" required="false" type="java.lang.String" %>
<%@ attribute name="checked" required="false" type="java.lang.Boolean" %>
<%@ attribute name="contentClass" required="false" type="java.lang.String" %>
<%@ attribute name="inputAttributes" required="false" type="java.lang.String" %>
<%@ attribute name="heading" required="true" type="java.lang.String" %>
<%@ attribute name="groupName" required="true" type="java.lang.String" %>

<div class="Accordion-item ${cssClass}">
	<input class="Accordion-bellowTrigger visuallyhidden" type="${type}" name="${groupName}" id="${id}" ${checked ? 'checked="checked"' : ''} ${inputAttributes} />
	<button type="button" class="Accordion-bellow ${bellowCss}" data-for="${id}">
		<span class="Accordion-bellowHeading"><spring:theme code="${heading}" text="${heading}" /></span>
	</button>
	<div class="Accordion-content ${checked ? ' opened ' : ''} ${contentClass}">
		<jsp:invoke fragment="accordionItemContent"/>
	</div>
</div>

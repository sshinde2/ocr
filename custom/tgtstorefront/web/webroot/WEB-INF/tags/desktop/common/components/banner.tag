<%@ tag body-content="scriptless" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="banner" required="true" type="java.lang.Object" %>
<%@ attribute name="status" required="false" type="java.lang.Object" %>
<%@ attribute name="bannerType" required="false" type="java.lang.String" %>
<%@ attribute name="bannerClass" required="false" type="java.lang.String" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<c:url var="encodedUrl" value="${not empty banner.page ? banner.page.label : banner.urlLink}" />

<c:if test="${not empty statusForBannerComponent and empty status}">
	<c:set var="status" value="${statusForBannerComponent}" />
</c:if>

<c:if test="${not empty bannerType}">
	<c:set var="bannerClass" value="${bannerClass} banner-${bannerType}" />
</c:if>

<c:set var="imageAltTitle" value="${not empty banner.headline ? banner.headline : banner.media.altText}" />
<c:set var="imageSrc">src="${banner.media.url}"</c:set>
<c:set var="noTab">tabindex="-1"</c:set>
<c:set var="tabIndex" value="${not empty status ? noTab : ''}"/>

<div class="banner ${bannerClass}">

	<c:if test="${not empty encodedUrl}">
		<c:if test="${banner.external}">
			<c:set var="cssClass">class="external"</c:set>
		</c:if>
		<c:if test="${empty banner.cmsLink}">
			<c:set var="wrap"><a href="${encodedUrl}" ${cssClass} ${tabIndex} >||</a></c:set>
			<c:set var="wrap" value="${fn:split(wrap, '||')}"/>
		</c:if>
	</c:if>

	<c:out value="${wrap[0]}" escapeXml="false"/>

	<div class="thumb">
		<c:set var="bannerBody">
			<c:choose>
				<c:when test="${not empty status and not status.first}">
					<noscript>
						<img ${imageSrc} alt="${imageAltTitle}" title="${imageAltTitle}"/>
					</noscript>
					<c:set var="imageSrc">class="defer-img unveil-img" data-${imageSrc}</c:set>
					<img ${imageSrc} alt="${imageAltTitle}" title="${imageAltTitle}"/>
				</c:when>
				<c:otherwise>
					<img ${imageSrc} alt="${imageAltTitle}" title="${imageAltTitle}"/>
				</c:otherwise>
			</c:choose>
		</c:set>
		<c:choose>
			<c:when test="${not empty banner.cmsLink}">
				<c:set var="cmsLinkBodyOverride" value="${bannerBody}" scope="request" />
				<cms:component component="${banner.cmsLink}"/>
				<c:remove var="cmsLinkBodyOverride" scope="request" />
			</c:when>
			<c:otherwise>
				<c:out value="${bannerBody}" escapeXml="false" />
			</c:otherwise>
		</c:choose>
	</div>
	<c:if test="${not empty banner.content}">
		<div class="details">
			${banner.content}
		</div>
	</c:if>

	<c:out value="${wrap[1]}" escapeXml="false"/>
	<jsp:doBody />
</div>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="url" required="false" type="String" %>
<%@ attribute name="parentClasses" required="false" type="String" %>
<%@ attribute name="unveil" required="false" type="Boolean" %>
<%@ attribute name="linkClass" required="false" type="String" %>
<%@ attribute name="details" required="false" fragment="true" %>
<%@ attribute name="image" required="false" fragment="true" %>
<%@ attribute name="listAttributes" required="false" type="String" %>


<jsp:invoke fragment="details" var="detailsPage" />

<c:set var="innerThumb">
	<c:choose>
		<c:when test="${not empty url}"><a href="${url}" class="${linkClass}">||</a></c:when>
		<c:otherwise><span class="${linkClass}">||</span></c:otherwise>
	</c:choose>
</c:set>
<c:set var="innerThumb" value="${fn:split(innerThumb, '||')}" />

<li class="Slider-slideItem ${parentClasses}" ${listAttributes}>
	<div class="slick-thumb ${unveil ? ' lazy-spinner' : '' }">
		<div class="thumb">
			<c:out value="${innerThumb[0]}" escapeXml="false" />
				<jsp:invoke fragment="image" />
			<c:out value="${innerThumb[1]}" escapeXml="false" />
		</div>
		<c:if test="${not empty detailsPage}">
			<div class="details">
				${detailsPage}
			</div>
			<div class="hover-mask">
				<a href="${url}" class="mask-link ${linkClass}"></a>
			</div>
		</c:if>
	</div>
</li>

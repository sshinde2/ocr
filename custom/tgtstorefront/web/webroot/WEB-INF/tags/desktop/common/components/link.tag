<%@ tag trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="link" required="false" type="java.lang.Object" %>
<%@ attribute name="linkUrl" required="false" type="java.lang.String" %>
<%@ attribute name="name" required="false" type="java.lang.String" %>
<%@ attribute name="className" required="false" type="java.lang.String" %>
<%@ attribute name="target" required="false" type="java.lang.String" %>
<%@ attribute name="buttonLink" required="false" %>
<%@ attribute name="quickAdd" required="false" %>
<%@ attribute name="coordinates" required="false" %>
<%@ attribute name="cmsComponentId" required="false" %>

<c:set var="bodyContent">
	<jsp:doBody/>
</c:set>
<c:if test="${empty cmsLinkBodyOverride and not empty bodyContent}">
	<c:set var="cmsLinkBodyOverride" value="${bodyContent}"/>
</c:if>
<c:if test="${(not empty url or not empty linkUrl) and (not empty link.linkName or  not cmsLinkBodyOverride)}">
	<c:url value="${not empty url ? url : linkUrl}" var="encodedUrl" />
	<c:set var="linkTarget" value="${not empty link ? link.target : target}"/>
	<c:set var="linkName" value="${not empty link ? link.linkName : name}"/>
	<c:if test="${buttonLink}">
		<c:set var="linkClass">button-fwd button-small</c:set>
	</c:if>
	<c:if test="${not empty className}">
		<c:set var="linkClass">${linkClass} ${className}</c:set>
	</c:if>
	<c:if test="${quickAdd}">
		<c:set var="linkClass">${linkClass} quick-add ga-ec-quick-click hide-for-small</c:set>
		<c:if test="${not empty cmsComponentId}">
			<c:set var="dataAttr">data-rel="${cmsComponentId}"</c:set>
		</c:if>
		<c:set var="linkName"><span class="visuallyhidden">${linkName}</span></c:set>
	</c:if>

	<c:if test="${coordinates and not empty link.XCoordinate and not empty link.YCoordinate}">
		<c:set var="linkClass">${linkClass} button-coord</c:set>
		<c:set var="styleAttr">left:${link.XCoordinate}%;top:${link.YCoordinate}%;</c:set>
	</c:if>

	<c:if test="${not quickAdd}">
		<c:if test="${fn:contains(linkTarget, 'MODAL')}">
			<c:set var="linkClass">${linkClass} lightbox</c:set>
		</c:if>
		<c:if test="${linkTarget eq 'MODALCONTENT'}">
			<c:set var="dataAttr">data-lightbox-type="content"</c:set>
		</c:if>
		<c:if test="${linkTarget eq 'MODALFRAME'}">
			<c:set var="dataAttr">data-lightbox-type="frame"</c:set>
		</c:if>
		<c:if test="${linkTarget eq 'MODALGALLERY'}">
			<c:set var="dataAttr">data-lightbox-type="gallery"</c:set>
		</c:if>
		<c:if test="${linkTarget eq 'MODALVIDEO'}">
			<c:set var="dataAttr">data-lightbox-type="video"</c:set>
		</c:if>
		<c:if test="${linkTarget eq 'NEWWINDOW'}">
			<c:set var="linkClass">${linkClass} external</c:set>
		</c:if>
	</c:if>

	<c:if test="${not empty linkClass}">
		<c:set var="linkClass">class="${linkClass}"</c:set>
	</c:if>
	<c:if test="${not empty styleAttr}">
		<c:set var="styleAttr">style="${styleAttr}"</c:set>
	</c:if>

	<a href="${encodedUrl}" ${linkClass} ${dataAttr} ${styleAttr} title="${linkName}">
		<c:choose>
			<c:when test="${not empty cmsLinkBodyOverride}">
				<c:out value="${cmsLinkBodyOverride}" escapeXml="false" />
			</c:when>
			<c:otherwise>
				${linkName}
			</c:otherwise>
		</c:choose>
	</a>

</c:if>

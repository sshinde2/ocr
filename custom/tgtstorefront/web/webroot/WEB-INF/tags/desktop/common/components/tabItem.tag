<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="name" required="true" type="java.lang.String" %>
<%@ attribute name="selected" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean" %>
<%@ attribute name="glyph" required="false" type="java.lang.String" %>
<%@ attribute name="nameClass" required="false" type="java.lang.String" %>
<%@ attribute name="triggerData" required="false" type="java.lang.String" %>
<%@ attribute name="hash" required="false" type="java.lang.String" %>
<%@ attribute name="afterName" required="false" fragment="true" %>

<c:set var="hash" value="${not empty hash ? hash : '#'}" />

<div class="Tab-item ${selected ? ' is-selected' : ''} ${disabled ? ' is-disabled' : ''}">
	<c:if test="${not disabled}">
		<c:set var="wrap"><a href="${hash}" class="ToggleTrigger" data-trigger="${triggerData}">||</a></c:set>
		<c:set var="wrap" value="${fn:split(wrap, '||')}"/>
	</c:if>
	<c:out value="${wrap[0]}" escapeXml="false"/>
		<c:if test="${not empty glyph}">
			<i class="Icon Icon--${glyph}"></i>
		</c:if>
		<span class="Tab-name ${nameClass}">
			<spring:theme code="${name}${disabled?'.disabled':''}"/>
		</span>
	<c:out value="${wrap[1]}" escapeXml="false"/>
	<jsp:invoke fragment="afterName" />
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>

<%-- to be replaced to tgt:tag --%>
<cms:slot var="component" contentSlot="${slots.FooterSupportContainer}">
	<c:set var="hasVisibleComponentsFooterSupport" value="${true}" />
</cms:slot>
<cms:slot var="component" contentSlot="${slots.FooterSecurityContainer}">
	<c:set var="hasVisibleComponentsFooterSecurity" value="${true}" />
</cms:slot>
<cms:slot var="component" contentSlot="${slots.FooterUtilityLeftContainer}">
    <c:set var="hasVisibleComponentsFooterUtilityLeft" value="${true}" />
</cms:slot>
<cms:slot var="component" contentSlot="${slots.FooterUtilityRightContainer}">
    <c:set var="hasVisibleComponentsFooterUtilityRight" value="${true}" />
</cms:slot>
<cms:slot var="component" contentSlot="${slots.FooterLegalContainer}">
	<c:set var="hasVisibleComponentsFooterLegal" value="${true}" />
</cms:slot>
<cms:slot var="component" contentSlot="${slots.FooterSocialContainer}">
	<c:set var="hasVisibleComponentsFooterSocial" value="${true}" />
</cms:slot>
<cms:slot var="component" contentSlot="${slots.FooterLinksColumn1}">
	<c:set var="hasVisibleComponentsFooterLinksColumn1" value="${true}" />
</cms:slot>
<cms:slot var="component" contentSlot="${slots.FooterLinksColumn2}">
	<c:set var="hasVisibleComponentsFooterLinksColumn2" value="${true}" />
</cms:slot>
<cms:slot var="component" contentSlot="${slots.FooterLinksColumn3}">
	<c:set var="hasVisibleComponentsFooterLinksColumn3" value="${true}" />
</cms:slot>
<c:set var="hasVisibleComponentsFooterLinks" value="${hasVisibleComponentsFooterLinksColumn1 or hasVisibleComponentsFooterLinksColumn2 or hasVisibleComponentsFooterLinksColumn3 or hasVisibleComponentsFooterLegal}" />

<footer>
	<div class="footer">
		<div class="inner-footer">
			<div class="footer-wrap">

				<div class="company-info">
					<c:if test="${hasVisibleComponentsFooterSupport}">
						<div class="contact-info">
							<cms:slot var="component" contentSlot="${slots.FooterSupportContainer}">
								<cms:component component="${component}"/>
							</cms:slot>
						</div>
					</c:if>


				</div>

				<c:if test="${hasVisibleComponentsFooterLinks}">
					<div class="footer-nav-group">
						<c:if test="${hasVisibleComponentsFooterLinksColumn1}">
							<div class="footer-nav content-nav">
								<c:set var="hideSubNavigationNodes" scope="request" value="${true}" />
									<cms:slot var="component" contentSlot="${slots.FooterLinksColumn1}">
										<cms:component component="${component}"/>
									</cms:slot>
								<c:remove var="hideSubNavigationNodes" />
							</div>
						</c:if>

						<c:if test="${hasVisibleComponentsFooterLinksColumn2}">
							<div class="footer-nav content-nav">
								<c:set var="hideSubNavigationNodes" scope="request" value="${true}" />
									<cms:slot var="component" contentSlot="${slots.FooterLinksColumn2}">
										<cms:component component="${component}"/>
									</cms:slot>
								<c:remove var="hideSubNavigationNodes" />
							</div>
						</c:if>

						<c:if test="${hasVisibleComponentsFooterLinksColumn3}">
							<div class="footer-nav content-nav">
								<c:set var="hideSubNavigationNodes" scope="request" value="${true}" />
									<cms:slot var="component" contentSlot="${slots.FooterLinksColumn3}">
										<cms:component component="${component}"/>
									</cms:slot>
								<c:remove var="hideSubNavigationNodes" />
							</div>
						</c:if>

						<c:if test="${hasVisibleComponentsFooterSocial}">
							<div class="footer-social hfk">
								<cms:slot var="component" contentSlot="${slots.FooterSocialContainer}">
									<cms:component component="${component}"/>
								</cms:slot>
							</div>
						</c:if>
					</div>
				</c:if>

				<c:if test="${hasVisibleComponentsFooterUtilityLeft}">
					<div class="footer-utility-left" ${ycommerce:getTestIdAttribute('footer-utility', pageContext)}>
						<cms:slot var="component" contentSlot="${slots.FooterUtilityLeftContainer}">
							<cms:component component="${component}"/>
					</cms:slot>
					</div>
				</c:if>

				<c:if test="${hasVisibleComponentsFooterUtilityRight}">
					<div class="footer-utility-right" ${ycommerce:getTestIdAttribute('footer-utility', pageContext)}>
						<cms:slot var="component" contentSlot="${slots.FooterUtilityRightContainer}">
							<cms:component component="${component}"/>
						</cms:slot>
					</div>
				</c:if>

				<c:if test="${hasVisibleComponentsFooterLegal}">
					<div class="footer-legal" ${ycommerce:getTestIdAttribute('footer-legal', pageContext)}>
						<cms:slot var="component" contentSlot="${slots.FooterLegalContainer}">
							<cms:component component="${component}"/>
						</cms:slot>
					</div>
				</c:if>

			</div>
		</div>
	</div>
</footer>

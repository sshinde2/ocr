<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<%-- to be replaced to tgt:tag --%>
<cms:slot var="component" contentSlot="${slots.FooterSupportContainer}">
	<c:set var="hasVisibleComponentsFooterSupport" value="${true}" />
</cms:slot>

<cms:slot var="component" contentSlot="${slots.FooterUtilityLeftContainer}">
    <c:set var="hasVisibleComponentsFooterUtilityLeft" value="${true}" />
</cms:slot>

<cms:slot var="component" contentSlot="${slots.FooterUtilityRightContainer}">
    <c:set var="hasVisibleComponentsFooterUtilityRight" value="${true}" />
</cms:slot>

<cms:slot var="component" contentSlot="${slots.FooterLegalContainer}">
	<c:set var="hasVisibleComponentsFooterLegal" value="${true}" />
</cms:slot>

<cms:slot var="component" contentSlot="${slots.FooterSecurityContainer}">
	<c:set var="hasVisibleComponentsFooterSecurity" value="${true}" />
</cms:slot>

<footer>
	<div class="footer">
		<div class="inner-footer">
			<div class="footer-wrap">
				<div class="co-footer">

					<c:if test="${hasVisibleComponentsFooterSupport}">
						<div class="company-info">
							<div class="contact-info">
								<cms:slot var="component" contentSlot="${slots.FooterSupportContainer}">
									<cms:component component="${component}"/>
								</cms:slot>
							</div>
						</div>
					</c:if>

					<div class="site-links">
						<c:if test="${hasVisibleComponentsFooterUtilityLeft}">
							<div class="footer-utility-left">
								<cms:slot var="component" contentSlot="${slots.FooterUtilityLeftContainer}">
									<cms:component component="${component}"/>
							</cms:slot>
							</div>
						</c:if>
					</div>

					<div class="site-links">
						<c:if test="${hasVisibleComponentsFooterUtilityRight}">
							<div class="footer-utility-right">
								<cms:slot var="component" contentSlot="${slots.FooterUtilityRightContainer}">
									<cms:component component="${component}"/>
								</cms:slot>
							</div>
						</c:if>
					</div>
					
					<div class="site-links">
						<c:if test="${hasVisibleComponentsFooterLegal}">
							<div class="footer-legal">
								<cms:slot var="component" contentSlot="${slots.FooterLegalContainer}">
									<cms:component component="${component}"/>
								</cms:slot>
							</div>
						</c:if>
					</div>

				</div>
			</div>
		</div>
	</div>
</footer>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:if test="${not empty accConfMsgs or not empty accInfoMsgs or not empty accErrorMsgs}">
	<div class="global-messages">

		<c:set var="msgCodes" value="${fn:split('accConfMsgs;accInfoMsgs;accErrorMsgs',';')}" />
		<c:forEach items="${msgCodes}" var="msgCode">
			<c:remove var="strongHeadingCode"/>
			<c:remove var="headingCode"/>
			<c:remove var="type"/>
			<c:set var="msgs" value="${requestScope[msgCode]}" />
			<c:if test="${not empty msgs and fn:length(msgs) gt 0}">

				<c:choose>
					<c:when test="${msgCode eq 'accErrorMsgs'}">
						<c:set var="type" value="error" />
					</c:when>
					<c:when test="${msgCode eq 'accInfoMsgs'}">
						<c:set var="type" value="normal" />
						<c:set var="headingCode" value="feedback.information" />
					</c:when>
					<c:when test="${msgCode eq 'accConfMsgs'}">
						<c:set var="type" value="success" />
					</c:when>
				</c:choose>

				<feedback:message type="${type}" strongHeadingCode="${strongHeadingCode}" headingCode="${headingCode}">
					<c:choose>
						<c:when test="${fn:length(msgs) eq 1}">
							<c:forEach items="${msgs}" var="msg">
								<p>
									<spring:theme code="${msg.key}" arguments="${msg.argumentsString}" argumentSeparator=";" />
								</p>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<ul>
								<c:forEach items="${msgs}" var="msg">
									<li>
										<spring:theme code="${msg.key}" arguments="${msg.argumentsString}" argumentSeparator=";" />
									</li>
								</c:forEach>
							</ul>
						</c:otherwise>
					</c:choose>
				</feedback:message>
			</c:if>

		</c:forEach>
	</div>
</c:if>

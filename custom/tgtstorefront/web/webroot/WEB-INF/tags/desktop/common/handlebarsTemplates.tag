<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ attribute name="template" required="false" type="java.lang.String" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/common/handlebarsTemplates" %>

<c:set var="output">
	<c:choose>
		<c:when test="${template eq 'instoreStock'}">
			<template:instoreStockTemplate />
		</c:when>
		<c:when test="${template eq 'findNearestStore'}">
			<template:findNearestStoreTemplate />
			<template:storeListItemTemplate />
		</c:when>
		<c:when test="${template eq 'facetAccordion'}">
			<template:facetAccordionTemplate />
			<template:facetItemTemplate />
		</c:when>
		<c:when test="${template eq 'listerItemTemplate'}">
			<template:productGridTemplate />
			<template:listerItemTemplate />
			<template:productSwatchTemplate />
		</c:when>
		<c:when test="${template eq 'consolidatedStockMessageTemplate'}">
			<template:consolidatedStockMessageTemplate />
		</c:when>
		<c:when test="${template eq 'fisUnavailableErrorTemplate'}">
			<template:fisUnavailableErrorTemplate />
		</c:when>
		<c:when test="${template eq 'preferredStoreDetailsTemplate'}">
			<template:preferredStoreDetailsTemplate />
		</c:when>
		<c:when test="${template eq 'externalRecommendationsCarousel'}">
			<template:externalRecommendationsCarousel />
			<template:productCarouselItemTemplate />
		</c:when>
		<c:when test="${template eq 'shoppingLocation'}">
			<template:shoppingLocation />
		</c:when>
	</c:choose>
</c:set>

${output}

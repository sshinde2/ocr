<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<script type="text/x-handlebars-template" id="stock-level-messages">
	<product:productStockLevelIndicator
		stockIndicator="StockInfo-indicator--{{stockLevel}}"
		stockLevel="{{stockLevel}}"
		selectMessage="{{message}}"
		messageOnly="${true}"
		messageClass="StockInfo-message--{{stockLevel}}"
		wrapMessage="{{#unless hideMessage}}||{{/unless}}">
		<jsp:attribute name="additionalMessage">
			{{#if additionalMessage}}
				<div class="StockInfo-additionalMessages is-{{mode}}Mode">
					{{#if online}}
						<a href="{{url}}#online" class="StockInfo-additionalMessage StockInfo-additionalMessage--online StockInfo-additionalMessage--button ToggleTab" data-trigger="online"><i class="Icon Icon--online Icon--size150"></i><spring:theme code="product.tab.availableOnline" /></a>
					{{/if}}
					{{#if store}}
						<a href="{{url}}#store" class="StockInfo-additionalMessage StockInfo-additionalMessage--store {{#if tabSwitch}}ToggleTab{{/if}} {{#unless tabSwitch}}find-in-another-store{{/unless}} StockInfo-additionalMessage--button" data-trigger="store"><i class="Icon Icon--store Icon--size150"></i><spring:theme code="product.tab.availableInAnotherStore" /></a>
					{{/if}}
				</div>
			{{/if}}
		</jsp:attribute>
	</product:productStockLevelIndicator>
</script>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>

<spring:theme var="listName" code="text.carousel.external.recommendations.list" />
<script type="text/x-handlebars-template" id="ExternalRecommendationsCarousel">
	<comp:carousel type="product" carouselClassName="Slider--products" perPage="6" itemCount="12">
		<jsp:attribute name="itemList">
			{{#each items as |product position|}}
				{{> ProductCarouselItem product list="${listName}" position=position}}
			{{/each}}
		</jsp:attribute>
		<jsp:attribute name="heading">
			<comp:component-heading title="{{title}}" />
		</jsp:attribute>
	</comp:carousel>
</script>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<script type="text/x-handlebars-template" id="FacetItemTemplate">
	<c:set var="className">FacetAccordion {{#if subgroup}}Accordion-item--tinted{{else}}Accordion-item--light{{/if}}</c:set>
	<spring:theme code="search.nav.facet.nofollow" var="nofollowStr" />
	<component:accordionItem id="{{id}}"
		groupName="{{id}}"
		contentClass="{{#if expand}}opened{{/if}} {{#unless subgroup}}is-scrollable{{/unless}}"
		inputAttributes="{{#if expand}}checked='checked'{{/if}}"
		heading="{{{name}}}"
		type="checkbox"
		cssClass="${className}"
	>
		<jsp:attribute name="accordionItemContent">
			{{#if facetOptionList}}
				<facet:itemList>
					{{#each facetOptionList as |facet|}}
						<facet:item
							url="{{facet.url}}"
							name="{{{facet.name}}}"
							count="{{facet.count}}"
							itemClass="{{#if facet.selected}}selected{{/if}}"
							category="{{{../name}}}"
							id="{{facet.id}}"
							queryUrl="{{queryUrl}}"
							additionalAttributes="{{#unless ../seoFollowEnabled}}${nofollowStr}{{/unless}}"
							/>
					{{/each}}
				</facet:itemList>
			{{/if}}
			{{#if facetList}}
				<facet:itemList className="FacetAccordion-list--collapse">
					{{#each facetList as |facet|}}
						{{> FacetItemTemplate facet subgroup='true'}}
					{{/each}}
				</facet:itemList>
			{{/if}}
		</jsp:attribute>
	</component:accordionItem>
</script>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<script type="text/x-handlebars-template" id="store-stock-error">
	<div class="FisError">
		<feedback:message type="warning" size="small" cssClass="is-no-bg">
			<p><i class="Icon Icon--warning Icon--size150"></i><spring:theme code="product.fis.unavailable" /></p>
		</feedback:message>
	</div>
</script>

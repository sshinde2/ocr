<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<script type="text/x-handlebars-template" id="InstoreStockTemplate">
	{{#if stores}}
		<%-- Just checking stores at this point; it's empty for location --%>
		{{#if stockAvailable}}
			<h5 class="InstoreStockCheck-closeStoresHead">
				<spring:theme code="instore.stock.nearest.heading" />
				<p class="InstoreStockCheck-availabilityTime">
					<spring:theme code="instore.stock.selectedLocation.availabilityTime" /><br/>
					<spring:theme code="instore.stock.selectedLocation.confirm" />
				</p>
			</h5>
		{{/if}}
		{{#unless stockAvailable}}
			<h5 class="InstoreStockCheck-closeStoresHead"><spring:theme code="instore.stock.closest.heading" /></h5>
		{{/unless}}
	{{/if}}
	{{#each stores}}
		<div class="InstoreStockCheck-store">
			<a href="{{url}}" class="InstoreStockCheck-pin InstoreStockCheck-storeDetailsLink"><span class="visuallyhidden"><spring:theme code="instore.stock.map.link" /></span></a>
			<div class="InstoreStockCheck-storeDetails">
				<h4 class="InstoreStockCheck-storeName"><a href="{{url}}" class="InstoreStockCheck-storeDetailsLink">{{name}}</a></h4>
				<address class="InstoreStockCheck-address">
					{{#if address.building}}
						<span class="InstoreStockCheck-addressDetail InstoreStockCheck-addressDetail--building">{{address.building}}</span>
					{{/if}}
					<span class="InstoreStockCheck-addressDetail InstoreStockCheck-addressDetail--adddressLine">{{address.line1}}</span>
					<span class="InstoreStockCheck-addressDetail InstoreStockCheck-addressDetail--state">{{address.town}}, {{address.state}}, {{address.postalCode}}</span>
				</address>
				<a class="InstoreStockCheck-phoneNumber" href="tel:{{address.phone}}"><span class="hfs">Ph: </span>{{address.phone}}</a>
				<span class="InstoreStockCheck-storeDistance">
					{{#if directionUrl}}
						<a class="InstoreStockCheck-directions external" href="{{directionUrl}}" data-location="{{name}}">
					{{/if}}
						{{formattedDistance}} away
					{{#if directionUrl}}
						</a>
					{{/if}}
				</span>
			</div>
			{{#if ../stockAvailable}}
				{{#if openingHours.openingDays}}
					<div class="InstoreStockCheck-storeHours {{#if @first}}is-active{{/if}}">
						<h5 class="InstoreStockCheck-storeHoursHeading"><spring:theme code="instore.stock.hours.heading" /></h5>
						<dl class="InstoreStockCheck-storeHoursList">
							{{#each openingHours.openingDays}}
								<dt class="InstoreStockCheck-storeHoursDay">
									{{#if formattedRelativeDay}}
										{{formattedRelativeDay}}:&nbsp;
									{{else}}
										{{formattedDay}}:&nbsp;
									{{/if}}
								</dt>
								<dd class="InstoreStockCheck-storeHoursTime">
									{{#if closed}}
										<spring:theme code="storeFinder.details.closed" />
									{{else}}
										{{#if allDayTrade}}
											<spring:theme code="storeFinder.details.allDay" />
										{{else}}
											{{formattedOpeningTime}} - {{formattedClosingTime}}
										{{/if}}
									{{/if}}
								</dd>
							{{/each}}
						</dl>
					</div>
				{{/if}}
			{{/if}}
			<div class="InstoreStockCheck-stockLevel">
				<product:productStockLevelIndicator
					stockIndicator="StockInfo-indicator--{{stockLevel}}"
					compact="${true}"
					stockLevel="{{stockLevel}}"
					selectMessage="{{message}}"
				/>
			</div>
			{{#if ../stockAvailable}}
				{{#if @first}}
					<h5 class="InstoreStockCheck-closeStoresHead"><spring:theme code="instore.stock.other.closest.heading" /></h5>
				{{/if}}
			{{/if}}
		</div>
	{{/each}}
	{{#if stores}}
		<div class="InstoreStockCheck-currentSearch">
			<button class="button-fwd-alt-outline InstoreStockCheck-change"><spring:theme code="instore.stock.selectedLocation.change" /></button>
		</div>
	{{/if}}
</script>

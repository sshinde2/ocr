<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="lister" tagdir="/WEB-INF/tags/desktop/product/lister" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="uiTrackDisplayOnly">
	<c:set var="displayOnly" value="{{displayOnly}}" />
</feature:enabled>

<script type="text/x-handlebars-template" id="ListerItemTemplate">
	<c:set var="productEcommJson">
		<template:productEcommAttributesJson
			id="{{code}}"
			name="{{{name}}}"
			list="{{list}}"
			category="{{{topLevelCategory}}}"
			brand="{{{brand}}}"
			assorted="{{assorted}}"
			displayOnly="${displayOnly}"
			position="{{add position 1}}"
		/>
	</c:set>
	<c:set var="emptyImg"><asset:resource code="img.missingProductImage.grid" /></c:set>
	<c:set var="primaryImage" value="{{#compare primaryImageUrl.length 'ne' 0}}{{primaryImageUrl}}{{else}}${emptyImg}{{/compare}}" />
	<lister:productListItem
		primaryImage="${primaryImage}"
		secondaryImage="{{secondaryImageUrl}}"
		baseCode="{{baseProductCode}}"
		productCode="{{code}}"
		currentProduct="{{code}}"
		ecProduct="${productEcommJson}">
		<div class="lead">
			<a href="{{url}}" title="{{{name}}}" class="thumb ga-ec-click">
				<lister:unveilImage src="${primaryImage}" name="{{{name}}}" className="thumb-img" />
				{{#if isProductDisplayTypePreOrder}}
					<span class="promo-status promotion-sticker preorder-sticker-tag">
						<spring:theme code="product.details.page.preorder.sticker" />
					</span>
				{{else if isProductDisplayTypeComingSoon}}
					<span class="promo-status promotion-sticker comingsoon-sticker-tag comingsoon-sticker-tag--tinyText">
						<spring:theme code="product.details.page.comingsoon.sticker" />
					</span>
				{{else}}
						{{#compare productPromotionalDeliverySticker.length 'gt' 0}}
							<span class="promo-status promotion-sticker delivery-promotion-sticker"><%--
								--%>{{{productPromotionalDeliverySticker}}}<%--
							--%></span>
						{{else if newArrived}}
							<span class="promo-status promotion-sticker new-sticker-tag">
								<spring:theme code="product.details.page.new.sticker" />
							</span>
						{{else}}
							{{#compare promotionStatus.length 'gt' 0}}
								<span class="promo-status promo-status-image {{promotionStatus}}">
									<span class="visuallyhidden">{{replace promotionStatus '-' ' '}}</span>
								</span>
							{{/compare}}
						{{/compare}}
				{{/if}}
			</a>
			{{#unless giftCard}}
				<lister:quickView url="{{url}}" name="{{{name}}}" dataRel="lister" title="Quick view &ndash; {{{name}}}" />
			{{/unless}}
		</div>
		<div class="detail">
			<div class="summary">
				<h3 class="name-heading">
					<a href="{{url}}" title="{{{name}}}" class="ga-ec-click">{{{abbreviate name 65}}}</a>
				</h3>
			</div>
			<div class="price-info">
				<span class="price {{#if isPriceRange}}price-range{{/if}} {{#if showWasPrice}}price-reduced{{else}}price-regular{{/if}}"><%--
					--%>{{formattedPrice}}<%--
				--%></span>
				{{#if showWasPrice}}
					<span class="was-price"><spring:theme code="product.price.was" arguments="{{formattedWasPrice}}" /></span>
				{{/if}}
			</div>
			{{#if preview}}
				<lister:comingSoon />
			{{/if}}
			{{#if showRatings}}
				<lister:ratings rating="{{averageRating}}" numberOfReviews="{{numberOfReviews}}" />
			{{/if}}
			{{#compare productVariantDataList.length 'gt' 1}}
				<ul class="swat-list">
					{{#each productVariantDataList as |variantProduct count|}}
						{{#compare count 'lt' 5}}
							{{> ProductSwatchTemplate variantProduct count=count}}
						{{/compare}}
					{{/each}}
				</ul>
				{{#compare productVariantDataList.length 'gt' 3}}
					<spring:theme code="text.viewMoreColours" var="viewMoreColours" />
					<p class="swat-list-note {{#compare productVariantDataList.length 'lte' 5}}only-for-small{{/compare}}"><a title="${viewMoreColours}" href="{{url}}">${viewMoreColours}</a></p>
				{{/compare}}
			{{/compare}}
			{{#if showDeals}}
				<lister:dealLink className="deals-center" description="{{{dealDescription}}}" />
			{{/if}}
			{{#unless preview}}
				{{#if inStock}}
					<c:if test="${not iosAppMode}">
						{{#compare maxAvailOnlineQty 'eq' 0}}
							{{#compare maxAvailStoreQty 'gt' 0}}
								{{#unless onlineExclusive}}
									<a class="stock-position-fis" href="{{url}}">
										<spring:theme code="product.stockposition.findInStore" />
									</a>
								{{/unless}}
							{{/compare}}
						{{/compare}}
					</c:if>
				{{else}}
					{{#unless isProductAvailableSoon}}
							<p class="stock-position"><spring:theme code="icon.dash" />
										{{#if onlineExclusive}}
											<spring:theme code="product.stockposition.soldout.onlineonly" />
										{{else}}
											<spring:theme code="product.stockposition.soldout.instore" />
										{{/if}}
							</p>
					{{/unless}}
				{{/if}}
			{{/unless}}
			{{#if isProductDisplayTypePreOrder}}
					<div class="StockInfo-availableDate StockInfo-availableDate--preOrder">
						<spring:theme code="product.stockposition.availableSoon" />&nbsp;{{productVariantDataList.[0].normalSaleStartDate}}
					</div>
			{{/if}}
			{{#if isProductDisplayTypeComingSoon}}
					<div class="StockInfo-availableDate StockInfo-availableDate--comingSoon">
						<spring:theme code="product.stockposition.availableSoon" />&nbsp;{{productVariantDataList.[0].normalSaleStartDate}}
					</div>
			{{/if}}
		</div>
		{{#unless giftCard}}
			<lister:favHeart baseCode="{{baseProductCode}}" />
		{{/unless}}
	</lister:productListItem>
</script>

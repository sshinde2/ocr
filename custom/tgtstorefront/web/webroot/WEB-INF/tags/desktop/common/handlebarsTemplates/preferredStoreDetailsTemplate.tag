<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<script type="text/x-handlebars-template" id="preferred-store-details">
	<product:storeDetails
		storeAddress="{{address.line1}}"
		postalCode="{{address.postalCode}}"
		town="{{address.town}}"
		state="{{address.state}}"
		phone="{{address.phone}}"
		storeNumber="{{storeNumber}}"
		directionUrl="{{directionUrl}}"
		storeName="{{name}}" />
</script>

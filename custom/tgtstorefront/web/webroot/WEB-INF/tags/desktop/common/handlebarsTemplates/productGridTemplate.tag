<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<script type="text/x-handlebars-template" id="ProductGridTemplate">
	{{#compare productData.length 'eq' 0}}
		<h5 class="u-alignCenter results-problem" data-results="grid" data-problem="No results"><spring:theme code="${not empty bulkyBoardStoreNumber ? 'kiosk.bulkyboard.' : ''}search.products.empty" /></h5>
	{{/compare}}
	<product:productBaseLayout listerType="grid" listClass="FavouritableProducts">
		{{#each productData as |product position|}}
			{{> ListerItemTemplate product position=position}}
		{{/each}}
	</product:productBaseLayout>
</script>

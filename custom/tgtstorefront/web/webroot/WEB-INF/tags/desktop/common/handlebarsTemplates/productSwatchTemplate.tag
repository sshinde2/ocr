<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="lister" tagdir="/WEB-INF/tags/desktop/product/lister" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<script type="text/x-handlebars-template" id="ProductSwatchTemplate">
	<c:set var="colourAppended">{{colourName}}{{#unless inStock}} : <spring:theme code="product.stockposition.soldout" />{{/unless}}</c:set>
	<c:set var="emptyImg"><asset:resource code="img.missingProductImage.grid" /></c:set>
	<c:set var="thumbImageUrl" value="{{#if thumbImageUrl}}{{thumbImageUrl}}{{else}}${emptyImg}{{/if}}" />
	<c:set var="gridImageUrl" value="{{#if gridImageUrl}}{{gridImageUrl}}{{else}}${emptyImg}{{/if}}" />
	<lister:swatchItem gridImage="${gridImageUrl}" className="{{#unless inStock}}swat-list-item-oos{{/unless}} {{#compare count 'gte' 3}}hide-for-small{{/compare}}">
		<a title="${colourAppended}" href="{{url}}" class="ga-ec-click">
			{{#unless inStock}}
				<span class="swat-item-icon-oos"><spring:theme code="icon.plain-cross"/></span>
			{{/unless}}
			<lister:unveilImage className="swat-list-img" name="${colourAppended}" src="${thumbImageUrl}" />
		</a>
	</lister:swatchItem>
</script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/x-handlebars-template" id="shoppingLocation">
	<header:flyoutItem>
		<spring:theme code="header.link.preferredstore" arguments="{{storeName}}" />
	</header:flyoutItem>
	<c:if test="${not kioskMode}">
		<header:flyoutItem divider="${true}">
			<spring:theme code="header.link.deliverylocation" arguments="{{delivery}}" />
		</header:flyoutItem>
	</c:if>
</script>

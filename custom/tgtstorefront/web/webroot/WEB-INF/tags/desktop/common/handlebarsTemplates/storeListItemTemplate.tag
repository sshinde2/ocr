<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>

<script type="text/x-handlebars-template" id="StoreListItemTemplate">
	<div class="FindNearestStore-storeDetails">
		<div class="FindNearestStore-storeNameContainer">
			{{#if stockDataIncluded}}
				<div class="FindNearestStore-stockLevel FindNearestStore-stockLevel--{{stockLevel}}">{{message}}</div>
			{{/if}}
			<div class="FindNearestStore-storeName">{{name}}</div>
		</div>
		{{#if stockDataIncluded}}
			<div class="FindNearestStore-storeIcons">
				<div class="FindNearestStore-linkContainer">
					<a href="tel:{{address.phone}}" class="FindNearestStore-link" title="Call {{name}}">
						<i class="Icon Icon--storePhone Icon--size200 FindNearestStore-icon"></i>
						<span class="StoreDetails-linkText FindNearestStore-linkText">
							{{address.phone}}
						</span>
					</a>
				</div>
				<div class="FindNearestStore-linkContainer external">
					<a href="{{directionUrl}}" class="external StoreDetails-location FindNearestStore-link" title="Get directions to {{name}}">
						<i class="Icon Icon--storeLocation Icon--size200 FindNearestStore-icon"></i>
						<span class="StoreDetails-linkText FindNearestStore-linkText">
							<spring:theme code="instore.stock.store.directions"/>
						</span>
					</a>
				</div>
			</div>
		{{else}}
			<div class="FindNearestStore-storeDistance">
				{{formattedDistance}}
			</div>
		{{/if}}

		{{#if preferredStore}}
			<div class="FindNearestStore-setMyStore FindNearestStore-myStoreText">
				<spring:theme code="storeFinder.storeDetails.myStore" />
			</div>
		{{else}}
			<button class="Button FindNearestStore-button FindNearestStore-setMyStore FindNearestStore-setMyStoreButton control-findstore-set-my-store-button" value="{{storeNumber}}" type="button">
				<span class="FindNearestStore-setMyStoreText"><spring:theme code="storeFinder.storeDetails.setMyStore" /></span>
			</button>
		{{/if}}
	</div>
</script>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<cms:slot var="component" contentSlot="${slots.BelowNavigationBar}">
	<c:set var="hasVisibleComponentsBelowNavigation" value="${true}" />
</cms:slot>
<c:if test="${hasVisibleComponentsBelowNavigation}">
	<div class="below-nav-bar hide-for-small">
		<div class="below-nav-outer">
			<div class="below-nav-inner">
				<cms:slot var="component" contentSlot="${slots['BelowNavigationBar']}">
					<cms:component component="${component}"/>
				</cms:slot>
			</div>
		</div>
	</div>
</c:if>

<%@ tag trimDirectiveWhitespaces="true" %>
<%@ attribute name="divider" required="false" type="java.lang.Boolean" %>
<%@ attribute name="className" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:if test="${divider}">
	<tr class="FlyOut-divider">
		<td></td><td class="FlyOut-divider--border"></td><td></td>
	</tr>
</c:if>

<tr class="FlyOut-item ${className}">
	<td colspan="3" class="FlyOut-content">
		<jsp:doBody />
	</td>
</tr>

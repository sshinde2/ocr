<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="disabledCart" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disabledSearch" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disabledSitewideNotification" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disabledFavourites" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<spring:theme code="search.minimum.chars" var="searchMinimum" text="1" />

<c:if test="${not mobileAppMode}">
	<header:smartAppBanner />
</c:if>

<c:if test="${not disabledSitewideNotification}">
	<header:sitewideNotification />
</c:if>

<div class="header">
	<div class="header-inner">

		<feature:disabled name="uiNewMegamenuDisplay">
			<div class="device-menu only-for-small">
				<span class="device-menu-inner device-menu-trigger">
					<span class="device-menu-label"><spring:theme code="header.link.menu" /></span>
				</span>
			</div>
		</feature:disabled>
		<feature:enabled name="uiNewMegamenuDisplay">
			<button class="Menu-mobileCloseContainer device-menu-trigger">
				<div class="Menu-mobileClose">
					<span class="visuallyhidden"><spring:theme code="text.close" /></span>
					<div class="TransformingRondel">
						<span class="TransformingRondel-line TransformingRondel-line--1"></span>
						<span class="TransformingRondel-line TransformingRondel-line--2"></span>
						<span class="TransformingRondel-line TransformingRondel-line--3"></span>
					</div>
				</div>
			</button>
			<c:set var="minisearchClass" value=" menu-trigger" />
		</feature:enabled>

		<%-- Site logo --%>
		<div class="site-logo">
			<header:sitelogo print="true" />
		</div>

		<%-- Search Form --%>
		<c:if test="${not disabledSearch}">
			<div class="mini-search mini-search-header ${not kioskMode ? 'site-search-auto-suggest' : '' } ${minisearchClass}" data-minimum-length="${searchMinimum}">
				<header:search />
			</div>
		</c:if>

		<header:utilityMenu disabledCart="${disabledCart}" menuStyle="hide-for-small" />

		<c:if test="${not disabledFavourites}">
			<feature:enabled name="featureWishlistFavourites">
				<c:if test="${not kioskMode and not mobileAppMode}">
					<header:favHeart />
				</c:if>
			</feature:enabled>
		</c:if>
		<%-- Mini Cart --%>
		<c:if test="${not disabledCart}">
			<cms:slot var="cart" contentSlot="${slots.MiniCart}">
				<cms:component component="${cart}"/>
			</cms:slot>
		</c:if>
	</div>
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="stepName" required="true" type="java.lang.String"%>
<%@ attribute name="stepsFinished" required="false" type="java.lang.Boolean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>


<div class="header header-subtle">
	<div class="header-inner">
	
		<%-- Site logo --%>
		<div class="site-logo hfma">
			<header:sitelogo print="true" />
		</div>

		<div class="site-contacts">
			<cms:slot var="contacts" contentSlot="${slots.ContactInfo}">
				<cms:component component="${contacts}"/>
			</cms:slot>
		</div>
	</div>
</div>

<div class="pro-bar">
	<multi-checkout:checkoutProgressBar steps="${checkoutSteps}" stepName="${stepName}" stepsFinished="${stepsFinished}" />
</div>

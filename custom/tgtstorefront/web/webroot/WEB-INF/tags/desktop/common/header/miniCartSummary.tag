<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty totalItems and totalItems gt 0}">
	<span class="qty">
		<c:choose>
			<c:when test="${totalItems gt 99}">
				<span class="num">99<span class="u-mus500">+</span></span>
			</c:when>
			<c:otherwise>
				<span class="num">${totalItems}</span>
			</c:otherwise>
		</c:choose>
	</span>
</c:if>

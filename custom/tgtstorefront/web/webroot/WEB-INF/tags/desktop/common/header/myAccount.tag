<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="smallScreen" required="false" %>


<c:if test="${not smallScreen and not anonymousCachable}">
	<sec:authorize access="hasRole('ROLE_CUSTOMERGROUP')">
		<c:url var="myAccountUrl" value="${fullyQualifiedDomainName}/my-account"/>
		<a href="${myAccountUrl}" class="utility-menu-link"><spring:theme code="header.link.account"/>

			<%-- really long first names risk breaking the layout so we will accommodate shorter names on the small layout --%>
			<span class="hat welcome hide-for-small">
				<spring:theme code="${fn:length(user.firstName) lt 12 ? 'header.welcome' : 'header.welcome.toolong'}" arguments="${user.firstName}" htmlEscape="true"/>
			</span>
		</a>

			<ul class="sub-util hide-for-small">
				<li>
					<form:form action="${targetUrlsMapping.logout}" method="post">
						<button type="submit"  class="utility-menu-link sub-util-link button-anchor cust-state-trig "><spring:theme code="header.link.logout"/></button>
						<target:csrfInputToken/>
					</form:form>
				</li>
			</ul>
	</sec:authorize>

	<sec:authorize access="!hasRole('ROLE_CUSTOMERGROUP')">
		<c:url var="loginUrl" value="${fullyQualifiedDomainName}/login" />
		<a href="${loginUrl}"><spring:theme code="header.link.login"/></a>

		<ul class="sub-util hide-for-small">
			<li>
				<c:url var="myAccountUrl" value="${fullyQualifiedDomainName}/my-account"/>
				<a href="${myAccountUrl}"  class="utility-menu-link sub-util-link "><spring:theme code="header.link.createaccount"/></a>
			</li>
		</ul>
	</sec:authorize>
</c:if>

<c:if test="${smallScreen}">
	<%--
		Not checking authorised here, relying on spring security redirect /my-account to /login page.
		Otherwise we would need to update /customer/getinfo to inject to here as well.
	--%>
	<c:url var="myAccountUrl" value="${fullyQualifiedDomainName}/my-account"/>
	<a href="${myAccountUrl}"><spring:theme code="header.link.account"/></a>
</c:if>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<cms:slot var="component" contentSlot="${slots.SitewideNotification}">
	<c:set var="hasVisibleComponentsSitewideNotification" value="${true}" />
</cms:slot>

<c:if test="${hasVisibleComponentsSitewideNotification}">
	<div class="sitewide-notification hfma">
		<div class="notification-content">
			<button type="button" class="close-notification">
				<spring:theme code="icon.thin-cross" />
			</button>
			<cms:slot var="component" contentSlot="${slots.SitewideNotification}">
				<cms:component component="${component}"/>
			</cms:slot>
		</div>
	</div>
</c:if>
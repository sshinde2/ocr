<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<c:set var="buttons">
	<button class="SmartAppBanner-button SmartAppBanner-useApp button-vibrant button-small-text">Use the app</button>
	<button class="SmartAppBanner-button SmartAppBanner-dismiss button-norm-border button-small-text">No thanks</button>
</c:set>

<cms:slot var="component" contentSlot="${slots.SmartAppBannerApple}">
	<c:set var="hasSmartAppBannerApple" value="${true}" />
</cms:slot>

<c:if test="${hasSmartAppBannerApple}">
	<div class="SmartAppBanner"
		data-type="apple"
		data-app-id="${appleSmartAppBannerAppId}"
		data-scheme="${appleSmartAppBannerAppScheme}"
		data-app-store-url="${appleSmartAppBannerAppStoreUrl}">
		<div class="SmartAppBanner-inner">
			<cms:slot var="component" contentSlot="${slots.SmartAppBannerApple}">
				<cms:component component="${component}"/>
			</cms:slot>
			${buttons}
		</div>
	</div>
</c:if>

<cms:slot var="component" contentSlot="${slots.SmartAppBannerAndroid}">
	<c:set var="hasSmartAppBannerAndroid" value="${true}" />
</cms:slot>

<c:if test="${hasSmartAppBannerAndroid}">
	<div class="SmartAppBanner"
		data-type="android"
		data-app-id="${androidSmartAppBannerAppId}"
		data-scheme="${androidSmartAppBannerAppScheme}"
		data-app-store-url="${androidSmartAppBannerAppStoreUrl}">
		<div class="SmartAppBanner-inner">
			<cms:slot var="component" contentSlot="${slots.SmartAppBannerAndroid}">
				<cms:component component="${component}"/>
			</cms:slot>
			${buttons}
		</div>
	</div>
</c:if>

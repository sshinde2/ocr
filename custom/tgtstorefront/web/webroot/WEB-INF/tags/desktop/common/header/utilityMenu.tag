<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="disabledCart" required="false" type="java.lang.Boolean" %>
<%@ attribute name="smallScreen" required="false" %>
<%@ attribute name="menuStyle" required="false" %>

<c:set var="stickyHeader" value="${false}" />
<feature:enabled name="uiStickyHeader">
	<c:set var="stickyHeader" value="${true}" />
</feature:enabled>

<c:choose>
	<c:when test="${not stickyHeader}">
		<ul class="utility-menu ${smallScreen ? 'only-for-small' : ''} ${disabledCart ? 'utility-menu-right' : ''} ${menuStyle}"><%--
		--%><cms:slot var="menuItem" contentSlot="${slots.UtilityMenu}"><%--
			--%><li><%--
				--%><cms:component component="${menuItem}"/><%--
			--%></li><%--
		--%></cms:slot><%--
		--%><c:if test="${not disabledCart}"><%--
			--%><li class="${smallScreen ? 'my-account-small' : 'my-account'} hfk"><%--
				--%><header:myAccount smallScreen="${smallScreen}" /><%--
			--%></li><%--
			--%><c:if test="${anonymousCachable}"><%--
				--%><script>if(window.t_Inject){t_Inject('account');}</script><%--
			--%></c:if><%--
		--%></c:if>	<%--
		--%><cms:slot var="menuItem" contentSlot="${slots.MyAccountMenu}"><%--
			--%><li><%--
				--%><cms:component component="${menuItem}"/><%--
			--%></li><%--
		--%></cms:slot>
		</ul>
	</c:when>
	<c:otherwise>
		<feature:enabled name="featureLocationServices">
			<c:set var="accountFurniture">data-location-service="${true}"</c:set>
		</feature:enabled>
		<div class="my-account TargetHeader-child hide-for-small" ${accountFurniture}>
			<header:myAccountIcons smallScreen="${smallScreen}" />
			<c:if test="${anonymousCachable}">
				<script>if(window.t_Inject){t_Inject('account');}</script>
			</c:if>
		</div>
	</c:otherwise>
</c:choose>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ attribute name="states" required="true" type="java.util.Collection" %>
<%@ attribute name="selectedState" required="true" %>

<formElement:formInputBox idKey="address.line1" labelKey="address.line1" path="line1" inputCSS="text address-line-1" mandatory="true" autoCorrect="off" />
<formElement:formInputBox idKey="address.line2" labelKey="address.line2" path="line2" inputCSS="text address-line-2" mandatory="false" autoCorrect="off" />
<formElement:formInputBox idKey="address.townCity" labelKey="address.townCity" path="townCity" inputCSS="text address-town" mandatory="true" autoCorrect="off"/>
<formElement:formSelectBox idKey="address.state" labelKey="address.state" path="state" selectCSSClass="full-select address-state" mandatory="true" skipBlank="false" skipBlankMessageKey="address.selectState" items="${states}" selectedValue="${selectedState}"/>
<formElement:formInputBox idKey="address.postcode" labelKey="address.postcode" path="postcode" inputCSS="text mini-text address-post-code" mandatory="true" pattern="[0-9]*"/>

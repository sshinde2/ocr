<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ attribute name="beforeLocationFields" required="false" fragment="true" %>
<%@ attribute name="singleInputActive" required="false" type="java.lang.Boolean" %>

<div class="AddressLookupForm" data-lookup-url="/address-lookup/search" data-format-url="/address-lookup/format">
	<jsp:invoke fragment="beforeLocationFields"/>
	<div class="f-element AddressLookupElement ${singleInputActive ? 'is-active is-visible' : ''}">
		<label for="addressLookup"><spring:theme code="checkout.multi.address.lookup" /><formElement:label mandatory="${true}" /></label>
		<div class="f-set ">
			<span class="f-feedback"></span>
			<spring:theme code="checkout.multi.address.lookup.placeholder" var="lookupPlaceholder" />
			<input class="AddressLookupElement-searchField text" autocorrect="on" type="text" autocomplete="on" placeholder="${lookupPlaceholder}">
		</div>
		<p class="f-err-msg error-message"><spring:theme code="address.autosuggestion.notSelected" /></p>
	</div>
	<div class="AddressLocationFields ${not singleInputActive ? 'is-active is-visible' : ''}">
		<jsp:doBody />
		<a href="#" class="AddressLocationFields-return"><spring:theme code="checkout.multi.address.lookup.return" /></a>
	</div>
</div>

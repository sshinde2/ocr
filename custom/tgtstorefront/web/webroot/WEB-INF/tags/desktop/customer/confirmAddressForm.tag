<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="actionUrl" required="true" %>
<%@ attribute name="deliveryModeCode" required="false" type="java.lang.String" %>

<form:form method="POST" cssClass="confirm-address" data-time-out="${timeOut}" action="${actionUrl}" commandName="confirmAddressForm">

	<h3 class="heading"><spring:theme code="confirmaddress.modal.heading" /></h3>

	<c:choose>
		<c:when test="${timeOut}">
			<p><spring:theme code="confirmaddress.modal.timeout" /></p>
		</c:when>
		<c:when test="${tooManyConfirmAddresses}">
			<p><spring:theme code="confirmaddress.modal.toomanymatches" /></p>
		</c:when>
		<c:when test="${fn:length(confirmAddressList) le 0}">
			<p><spring:theme code="confirmaddress.modal.nomatches" /></p>
		</c:when>
		<c:otherwise>
			<p><spring:theme code="confirmaddress.modal.choose" /></p>
			<formElement:formSelectBox idKey="confirmedAddressCode" selectCSSClass="suggested-address" path="confirmedAddressCode" skipBlank="false" skipBlankMessageKey="address.title.pleaseSelect" items="${confirmAddressList}" />
		</c:otherwise>
	</c:choose>
	
	<h4 class="sub-heading"><spring:theme code="confirmaddress.modal.suppliedheading" /></h4>
	<p>${confirmAddressForm.suppliedAddress.line1},
	<c:if test="${not empty confirmAddressForm.suppliedAddress.line2}">
		${confirmAddressForm.suppliedAddress.line2}, 
	</c:if>
	${confirmAddressForm.suppliedAddress.townCity}, ${confirmAddressForm.suppliedAddress.state}, ${confirmAddressForm.suppliedAddress.postcode}</p>

	<%-- Checkbox is checked and disabled if not options --%>
	<c:set var="fastForwardCheckbox" value="${tooManyConfirmAddresses or fn:length(confirmAddressList) le 0}" />
	<c:if test="${fastForwardCheckbox}">
		<form:hidden path="useSuppliedAddress" value="true" />
	</c:if>

	<formElement:formOptCheckbox idKey="address.supplied" labelKey="address.supplied" path="useSuppliedAddress" mandatory="false" disabled="${fastForwardCheckbox}" checked="${fastForwardCheckbox}" />

	<div class="f-buttons">
		<a href="#close" class="button-bwd close-lightbox"><spring:theme code="confirmaddress.modal.button.close" /></a>
		<button class="button-fwd"><spring:theme code="confirmaddress.modal.button.confirm" /></button>
	</div>

	<form:hidden path="suppliedAddress.addressId"/>
	<form:hidden path="suppliedAddress.titleCode"/>
	<form:hidden path="suppliedAddress.firstName"/>
	<form:hidden path="suppliedAddress.lastName"/>
	<form:hidden path="suppliedAddress.line1"/>
	<form:hidden path="suppliedAddress.line2"/>
	<form:hidden path="suppliedAddress.townCity"/>
	<form:hidden path="suppliedAddress.state"/>
	<form:hidden path="suppliedAddress.postcode"/>
	<form:hidden path="suppliedAddress.countryIso"/>
	<form:hidden path="suppliedAddress.phoneNumber"/>
    <form:hidden path="suppliedAddress.defaultAddress"/>
    <form:hidden path="suppliedAddress.deliveryModeCode"/>
	
	<target:csrfInputToken/>
</form:form>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="deliveryAddress" required="true" type="de.hybris.platform.commercefacades.user.data.AddressData" %>
<%@ attribute name="isSelected" required="false" type="java.lang.Boolean" %>
<%@ attribute name="count" required="false" type="java.lang.Integer" %>
<%@ attribute name="edit" required="false" %>
<%@ attribute name="use" required="false" %>
<%@ attribute name="useText" required="false" %>
<%@ attribute name="defaultAddress" required="false" %>
<%@ attribute name="remove" required="false" %>
<%@ attribute name="invalid" required="false" %>
<%@ attribute name="deliveryModeCode" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<%-- Individual list item --%>
<li class="list-item ${isSelected ? ' active selected-address' : ''} ${invalid ? ' invalid-address' : ''}">
	<address class="PanelActions--detail">
		<strong>
			<c:if test="${not empty deliveryAddress.title}">
				${fn:escapeXml(deliveryAddress.title)}&nbsp;
			</c:if>
			${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}
			<c:if test="${deliveryAddress.defaultAddress}">
				&nbsp;(<spring:theme code="text.default" text="Default"/>)
			</c:if>
		</strong><br />
		${fn:escapeXml(deliveryAddress.line1)}<br />
		<c:if test="${not empty deliveryAddress.line2}">
			${fn:escapeXml(deliveryAddress.line2)}<br />
		</c:if>
		${fn:escapeXml(deliveryAddress.town)}, ${fn:escapeXml(deliveryAddress.state)}, ${fn:escapeXml(deliveryAddress.postalCode)}<br />${fn:escapeXml(deliveryAddress.country.name)}
	</address>
	<c:if test="${invalid}">
		<spring:theme code="checkout.multi.delivery.modes.${deliveryModeCode}" var="delMode" />
		<p class="del-alert address-warning">
			<spring:theme code="icon.exclamation" />&nbsp;
			<spring:theme code="checkout.multi.delivery.storedAddress.invalid" arguments="${delMode}" />
		</p>
	</c:if>
	<ul class="PanelActions">
		<c:if test="${not invalid}">
			<li>
				<a href="${edit}" class="PanelActions-link">
					<spring:theme code="checkout.multi.deliveryAddress.edit" text="Edit" />
				</a>
			</li>
			<c:if test="${not empty use}">
				<c:if test="${isSelected}">
					<li>
						<span class="icon PanelActions--icon"></span>
						<span class="visuallyhidden"><spring:theme code="text.selected" text="Selected"/></span>
					</li>
				</c:if>
				<c:if test="${not isSelected}">
					<li class="PanelActions-button">
						<a href="${use}" class="button-fwd PanelActions-selector button-small-text">
							<spring:theme code="checkout.multi.deliveryAddress.select" text="Select"/>
						</a>
					</li>
				</c:if>
			</c:if>
			<c:if test="${not empty remove}">
				<li>
					<a href="${remove}" class="PanelActions-link">
						<spring:theme code="text.account.addressBook.removeAddress" text="Remove"/>
					</a>
				</li>
			</c:if>
			<c:if test="${not empty defaultAddress and not deliveryAddress.defaultAddress}">
				<li>
					<a href="${defaultAddress}" class="PanelActions-link">
						<spring:theme code="text.account.addressBook.setDefaultDeliveryAddress" text="Set as default"/>
					</a>
				</li>
			</c:if>
		</c:if>
	</ul>
</li>

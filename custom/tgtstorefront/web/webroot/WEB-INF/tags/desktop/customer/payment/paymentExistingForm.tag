<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="beforeForm" required="false" fragment="true" %>
<%@ attribute name="afterForm" required="false" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:if test="${not empty paymentMethods and existingCardPaymentForm.enabled}">
	<form:form method="post" action="${existingCardPaymentForm.action}" commandName="existingCardPaymentForm">
	<%-- Make a payment amount --%>
	<form:hidden path="amountCurrentPayment" />

		<div class="payment-existing">
			<h3><spring:theme code="checkout.multi.paymentDetails.existingPaymentDetails" /></h3>

			<jsp:invoke fragment="beforeForm"/>

			<ul class="block-list card-list exist-card-list">
				<c:forEach items="${paymentMethods}" var="paymentMethod" varStatus="status">
					<multi-checkout:paymentMethodDetails paymentMethod="${paymentMethod}" isSelected="${paymentMethod.id eq selectedPaymentMethodId}" count="${status.count}" radio="${true}" />
				</c:forEach>
			</ul>

			<jsp:invoke fragment="afterForm"/>

			<target:csrfInputToken/>
		</div>
	</form:form>
</c:if>

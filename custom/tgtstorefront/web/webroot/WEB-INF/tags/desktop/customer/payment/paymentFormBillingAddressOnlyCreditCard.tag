<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="beforeForm" required="false" fragment="true" %>
<%@ attribute name="afterForm" required="false" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="customer-payment" tagdir="/WEB-INF/tags/desktop/customer/payment" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<spring:theme code="checkout.multi.paymentDetails.continue.review" var="continueText" />
<spring:theme code="icon.right-arrow-large" var="largeArrow" />
<c:set var="largeArrow">
	<c:out escapeXml="true" value="${largeArrow}" />
</c:set>

<c:set var="formName" value="billingAddressOnlyForm" />

<li class="list-item pay-mode ${isSelected or not multiplePaymentMode ? 'active' : ''}"
	data-continue-form="${formName}"
	data-continue-text="${continueText}${largeArrow}"
	data-continue-disabled="${ipgCreditCardUnavailable}"
	data-mode-type="newcard"
	data-ec-option="ipg-new">

	<form:form method="post" cssClass="pay-details present-cc" action="${billingAddressOnlyForm.action}" commandName="${formName}" novalidate="true">
		<spring:hasBindErrors name="${formName}">
			<c:set var="hasErrors" value="${true}" />
		</spring:hasBindErrors>

		<c:if test="${multiplePaymentMode}">
			<customer-payment:paymentNewCardOption active="${billingAddressOnlyForm.active}" />
		</c:if>

		<div class="item-detail">
			<c:if test="${not ipgCreditCardUnavailable}">
				<jsp:invoke fragment="beforeForm"/>
				<customer-payment:paymentBillingAddress billingAddressForm="${billingAddressOnlyForm.billingAddress}" hasErrors="${hasErrors}" formKey="creditCard" />
				<jsp:invoke fragment="afterForm"/>
			</c:if>

			<c:if test="${ipgCreditCardUnavailable}">
				<p class="important-note">
					<spring:theme code="checkout.error.paymentmethod.ipg.unavailable" >
						<jsp:attribute name="arguments">
							<spring:theme code="checkout.paymentMethod.creditcard" />
						</jsp:attribute>
					</spring:theme>
				</p>
			</c:if>

		</div>

		<target:csrfInputToken/>
	</form:form>
</li>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="beforeForm" required="false" fragment="true" %>
<%@ attribute name="afterForm" required="false" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<spring:theme code="checkout.multi.paymentDetails.continue.paypal" var="continueText" />
<spring:theme code="icon.right-arrow-large" var="largeArrow" />
<c:set var="largeArrow">
	<c:out escapeXml="true" value="${largeArrow}" />
</c:set>

<c:if test="${not empty vendorPaymentForm and  vendorPaymentForm.enabled}">
	<li class="list-item pay-mode" data-continue-form="vendorPaymentForm" data-mode-type="vendor" data-ec-option="paypal" data-continue-text="${continueText}${largeArrow}">
		<form:form method="post" cssClass="pay-details new-paypal" action="${vendorPaymentForm.action}" commandName="vendorPaymentForm" class="create_update_payment_form">

			<div class="summary">
					<input type="radio" name="cardType" id="card-paypal" value="payPal" class="radio pay-radio" ${vendorPaymentForm.active ? ' checked="checked"' : ''} />
					<%-- Make a payment amount --%>
					<form:hidden path="amountCurrentPayment" />

					<label for="card-paypal" class="summary-label">
						<span class="card card-paypal">
							<span class="icon"></span>
							<span class="non-icon"><spring:theme code="checkout.multi.paymentDetails.paypal" /></span>
						</span>
					</label>
			</div>

			<div class="item-detail">
				<jsp:invoke fragment="beforeForm"/>
				<p><spring:theme code="checkout.multi.paymentDetails.paypal.message" /></p>
				<jsp:invoke fragment="afterForm"/>
			</div>

			<target:csrfInputToken/>
		</form:form>
	</li>
</c:if>

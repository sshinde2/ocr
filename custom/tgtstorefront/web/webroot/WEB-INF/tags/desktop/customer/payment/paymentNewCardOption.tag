<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="active" required="false" type="java.lang.Boolean" %>

<div class="summary">
	<input type="radio" name="cardType" id="credit-card" value="creditCard" class="radio pay-radio" ${active ? ' checked="checked"' : ''} />
	<label for="credit-card" class="summary-label">
		<span class="card card-new">
			<span class="visuallyhidden">
				<spring:theme code="checkout.multi.paymentDetails.cardTypes" />
			</span>
		</span>
	</label>
</div>

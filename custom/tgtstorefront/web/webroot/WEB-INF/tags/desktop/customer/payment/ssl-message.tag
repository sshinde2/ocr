<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="security-message">
	<p><spring:theme code="payment.sslMessage" /></p>
	<p><spring:theme code="checkout.multi.paymentDetails.cardCharge" />&nbsp;<spring:theme code="checkout.multi.paymentDetails.billNote" /></p>
</div>

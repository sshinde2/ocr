<%@ tag body-content="scriptless" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags"%>
<%@ attribute name="type" required="false" %>
<%@ attribute name="size" required="false" %>
<%@ attribute name="icon" required="false" %>
<%@ attribute name="shortMessage" required="false" type="java.lang.Boolean" %>
<%@ attribute name="minimal" required="false" type="java.lang.Boolean" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ attribute name="strongHeadingCode" required="false" %>
<%@ attribute name="headingCode" required="false" %>

<%--

	# How to use

	<feedback:message type="error"
		strongHeadingCode="feedback.oops"
		headingCode="feedback.please.fix">
		<ul>
			<li>Username or password incorrect, please check and try again</li>
		</ul>
	</feedback:message>

--%>

<c:set var="classname">feedback-msg</c:set>
<c:if test="${not empty type}">
	<c:set var="classname">${classname} ${type}</c:set>
</c:if>
<c:if test="${not empty size}">
	<c:set var="classname">${classname} ${size}</c:set>
</c:if>
<c:if test="${not empty icon and icon eq false}">
	<c:set var="classname">${classname} no-icon</c:set>
</c:if>
<c:if test="${shortMessage}">
	<c:set var="classname">${classname} short</c:set>
</c:if>
<c:if test="${not empty cssClass}">
	<c:set var="classname">${classname} ${cssClass}</c:set>
</c:if>
<c:if test="${minimal}">
	<c:set var="classname">${classname} is-minimal</c:set>
</c:if>

<div class="${classname}">
	<c:choose>
		<c:when test="${not empty strongHeadingCode and not empty headingCode}">
			<h3 class="heading">
				<span class="strong-heading"><spring:theme code="${strongHeadingCode}" /></span><br />
				<spring:theme code="${headingCode}" />
			</h3>
		</c:when>
		<c:otherwise>
			<c:if test="${not empty strongHeadingCode}">
				<h3 class="strong-heading"><spring:theme code="${strongHeadingCode}" /></h3>
			</c:if>
			<c:if test="${not empty headingCode}">
				<h3 class="heading"><spring:theme code="${headingCode}" /></h3>
			</c:if>
		</c:otherwise>
	</c:choose>
	<jsp:doBody />
</div>

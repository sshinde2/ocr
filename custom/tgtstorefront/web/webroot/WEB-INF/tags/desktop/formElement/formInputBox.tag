<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelKey" required="false" type="java.lang.String" %>
<%@ attribute name="labelHelp" required="false" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="type" required="false" type="java.lang.String" %>
<%@ attribute name="autoCorrect" required="false" type="java.lang.String" %>
<%@ attribute name="autoComplete" required="false" type="java.lang.String" %>
<%@ attribute name="pattern" required="false" type="java.lang.String" %>
<%@ attribute name="dataSameAs" required="false" type="java.lang.String" %>
<%@ attribute name="fieldData" required="false" type="java.lang.String" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ attribute name="labelCSS" required="false" type="java.lang.String" %>
<%@ attribute name="dynamicFillable" required="false" type="java.lang.Boolean" %>
<%@ attribute name="inputCSS" required="false" type="java.lang.String" %>
<%@ attribute name="elementCSS" required="false" type="java.lang.String" %>
<%@ attribute name="buttonCSS" required="false" type="java.lang.String" %>
<%@ attribute name="buttonAside" required="false" type="java.lang.Boolean" %>
<%@ attribute name="placeholder" required="false" type="java.lang.String" %>
<%@ attribute name="tabindex" required="false" rtexprvalue="true" %>
<%@ attribute name="maxlength" required="false" rtexprvalue="true" %>
<%@ attribute name="disabled" required="false" rtexprvalue="true" %>
<%@ attribute name="hint" required="false" rtexprvalue="true" %>
<%@ attribute name="hintLink" required="false" rtexprvalue="true" %>
<%@ attribute name="logo" required="false" rtexprvalue="true" %>
<%@ attribute name="labelArgs" required="false" rtexprvalue="true" %>
<%@ attribute name="labelExtra" required="false" type="java.lang.String" %>
<%@ attribute name="disclaimer" required="false" rtexprvalue="true" %>
<%@ attribute name="checkButton" required="false" rtexprvalue="true" %>
<%@ attribute name="buttonDisabled" required="false" rtexprvalue="true" %>
<%@ attribute name="hideError" required="false" rtexprvalue="true" %>
<%@ attribute name="clearValue" required="false" rtexprvalue="true" %>
<%@ attribute name="message" required="false" type="java.lang.String" %>
<%@ attribute name="messageType" required="false" type="java.lang.String" %>
<%@ attribute name="isolated" required="false"%>
<%@ attribute name="errorPath" required="false" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>

<c:if test="${empty type}">
	<c:set var="type">text</c:set>
</c:if>

<c:if test="${empty autoCorrect}">
	<c:set var="autoCorrect" value="on" />
</c:if>

<c:if test="${empty autoComplete}">
	<c:set var="autoComplete" value="on" />
</c:if>

<c:set var="dataAttr" value="" />
<c:if test="${not empty dataSameAs}">
	<c:set var="dataAttr">${dataAttr} data-same-as="${dataSameAs}"</c:set>
</c:if>
<template:formElement path="${path}" isolated="${isolated}" elementCSS="${elementCSS}" fieldData="${fieldData}" hideError="${hideError}" errorPath="${errorPath}">
	<c:if test="${not empty logo}">
		<spring:theme code="${logo}" var="logoName"/>
		<span class="f-logo f-logo-${logoName}" /></span>
	</c:if>
	<c:if test="${not empty labelKey}">
		<label class="${labelCSS}" for="${idKey}">
			<spring:theme code="${labelKey}" arguments="${labelArgs}"/>
			<formElement:label mandatory="${mandatory}" />
			<c:if test="${not empty labelHelp}">
				<spring:theme code="${labelHelp}" var="labelHelp"/>
				<a class="lightbox l-hint" data-lightbox-type="content" href="${labelHelp}"><spring:theme code="icon.question" /></a>
			</c:if>
			${labelExtra}
		</label>
	</c:if>
	<c:if test="${not empty disclaimer}">
		<p class="note f-disclaimer">
			<spring:theme code="${disclaimer}" />
		</p>
	</c:if>
	<c:if test="${not empty hint}">
		<c:if test="${not empty hintLink}">
			<spring:theme code="${hintLink}" var="hintLink"/>
			<c:set var="hintWrap"><a class="lightbox f-hint" data-lightbox-type="content" href="${hintLink}">||</a></c:set>
			<c:set var="hintWrap" value="${fn:split(hintWrap, '||')}"/>
		</c:if>
		<c:if test="${empty hintLink}">
			<c:set var="hintWrap"><span class="f-hint">||</span></c:set>
			<c:set var="hintWrap" value="${fn:split(hintWrap, '||')}"/>
		</c:if>
		<c:out value="${hintWrap[0]}" escapeXml="false"/>
		<spring:theme code="${hint}" />&nbsp;<spring:theme code="icon.question" />
		<c:out value="${hintWrap[1]}" escapeXml="false"/>
	</c:if>
	<div class="f-set ${not empty checkButton ? ( buttonAside ? 'f-set-has-button-aside' : 'f-set-has-button') : ''} ${dynamicFillable ? 'dynamic-fillable' : ''}" ${dataAttr}>
		<span class="f-feedback"></span>
		<spring:theme code="${placeholder}" var="placeholder" />
		<c:choose>
			<c:when test="${clearValue}">
				<input type="${type}" autocorrect="${autoCorrect}" autocomplete="${autoComplete}" class="${inputCSS}" id="${idKey}" name="${path}" tabindex="${tabindex}" placeholder="${placeholder}" maxlength="${maxlength}" pattern="${pattern}" />
			</c:when>
			<c:otherwise>
				<form:input type="${type}" autocorrect="${autoCorrect}" autocomplete="${autoComplete}" cssClass="${inputCSS}" id="${idKey}" path="${path}" tabindex="${tabindex}" disabled="${disabled}" placeholder="${placeholder}" maxlength="${maxlength}" pattern="${pattern}" />
			</c:otherwise>
		</c:choose>
		<c:if test="${not empty checkButton}">
			<button type="button" class="f-set-button button-norm ${buttonDisabled ? ' disabled-button ' : ''} ${buttonCSS}"><spring:theme code="${checkButton}" /></button>
		</c:if>
	</div>
	<c:if test="${not empty message}">
		<p class="${messageType eq 'good' ? 'f-suc-msg success-message' : (messageType eq 'disabled' ? 'f-inact-msg' : '')}">${message}</p>
	</c:if>
</template:formElement>

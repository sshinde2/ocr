<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelKey" required="true" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ attribute name="labelCSS" required="false" type="java.lang.String" %>
<%@ attribute name="items" required="true" type="java.util.List" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>


<template:formElement path="${path}">
	<label class="${labelCSS}">
		<spring:theme code="${labelKey}"/>
		<formElement:label mandatory="${mandatory}" />
	</label>

	<div class="f-set-group f-set-group-inline">
		<c:forEach items="${items}" var="radio" varStatus="radioStatus">
			<formElement:formRadio path="${path}" value="${radio.code}" label="${radio.name}" nested="true" idKey="${idKey}-idx-${radioStatus.count}" />
		</c:forEach>
	</div>

</template:formElement>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="idKey" required="true" type="java.lang.String" %>
<%@ attribute name="labelKey" required="false" type="java.lang.String" %>
<%@ attribute name="path" required="true" type="java.lang.String" %>
<%@ attribute name="dataSameAs" required="false" type="java.lang.String" %>
<%@ attribute name="dataSameAsValue" required="false" type="java.lang.String" %>
<%@ attribute name="dataSearchThreshold" required="false" type="java.lang.Integer" %>
<%@ attribute name="items" required="true" type="java.util.Collection" %>
<%@ attribute name="itemValue" required="false" type="java.lang.String" %>
<%@ attribute name="itemLabel" required="false" type="java.lang.String" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ attribute name="labelCSS" required="false" type="java.lang.String" %>
<%@ attribute name="dynamicFillable" required="false" type="java.lang.Boolean" %>
<%@ attribute name="selectCSSClass" required="false" type="java.lang.String" %>
<%@ attribute name="elementCSS" required="false" type="java.lang.String" %>
<%@ attribute name="skipBlank" required="false" type="java.lang.Boolean" %>
<%@ attribute name="skipBlankWhenSingle" required="false" type="java.lang.Boolean" %>
<%@ attribute name="skipBlankMessageKey" required="false" type="java.lang.String" %>
<%@ attribute name="selectedValue" required="false" type="java.lang.String" %>
<%@ attribute name="tabindex" required="false" rtexprvalue="true" %>
<%@ attribute name="hint" required="false" rtexprvalue="true" %>
<%@ attribute name="hideError" required="false" rtexprvalue="true" %>
<%@ attribute name="nested" required="false" rtexprvalue="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${skipBlankWhenSingle and not empty items and fn:length(items) le 1}">
	<c:set var="skipBlank" value="true" />
</c:if>

<c:set var="dataSearchThreshold" value="${kioskMode ? '9999' : ( not empty dataSearchThreshold ? dataSearchThreshold : '' ) }" />

<c:set var="dataAttr" />
<c:if test="${not empty dataSameAs}">
	<c:if test="${not empty dataSameAsValue}">
		<c:forEach items="${items}" var="item">
			<c:if test="${item[dataSameAsValue] eq dataSameAs}">
				<c:set var="dataSameAs" value="${item.code}" />
			</c:if>
		</c:forEach>
	</c:if>
	<c:set var="dataAttr">${dataAttr} data-same-as="${dataSameAs}"</c:set>
</c:if>

<template:formElement path="${path}" elementCSS="${elementCSS}" hideError="${hideError or nested}">
	<c:if test="${not empty labelKey}">
		<label class="${labelCSS}" for="${idKey}">
			<spring:theme code="${labelKey}"/>
			<formElement:label mandatory="${mandatory}" />
		</label>
	</c:if>
	<div class="f-set ${dynamicFillable ? 'dynamic-fillable' : ''}" ${dataAttr}>
		<form:select id="${idKey}" path="${path}" cssClass="${selectCSSClass} chosen-select" data-search-threshold="${dataSearchThreshold}" tabindex="${tabindex}">
			<c:if test="${skipBlank == null || skipBlank == false}">
				<option value="" disabled="disabled" selected="${empty selectedValue ? 'selected' : ''}"><spring:theme code='${skipBlankMessageKey}'/></option>
			</c:if>
			<form:options items="${items}" itemValue="${not empty itemValue ? itemValue :'code'}" itemLabel="${not empty itemLabel ? itemLabel :'name'}"/>
		</form:select>
		<spring:theme code="${hint}" var="hint" text="" />
		<c:if test="${not hideError and not nested}">
			<span class="f-feedback"></span>
		</c:if>
		<c:if test="${not empty hint}">
			<span class="f-hint">${hint}</span>
		</c:if>
	</div>
</template:formElement>

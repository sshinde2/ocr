<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="idKey" required="true"  type="java.lang.String" %>
<%@ attribute name="labelKey" required="true"  type="java.lang.String" %>
<%@ attribute name="path" required="true"  type="java.lang.String" %>
<%@ attribute name="mandatory" required="false"  type="java.lang.Boolean" %>
<%@ attribute name="labelCSS" required="false"  type="java.lang.String" %>
<%@ attribute name="areaCSS" required="false"  type="java.lang.String" %>
<%@ attribute name="areaRows" required="false"  type="java.lang.String" %>
<%@ attribute name="fieldData" required="false" type="java.lang.String" %>
<%@ attribute name="elementCSS" required="false" type="java.lang.String" %>
<%@ attribute name="labelExtra" required="false" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>


<template:formElement path="${path}" elementCSS="${elementCSS}" fieldData="${fieldData}">
	<label class="${labelCSS}" for="${idKey}">
		<spring:theme code="${labelKey}"/>
		<formElement:label mandatory="${mandatory}" />
		${labelExtra}
	</label>
	<form:textarea cssClass="${areaCSS}" id="${idKey}" path="${path}" rows="${areaRows}" />
</template:formElement>

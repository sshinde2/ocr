<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="mandatory" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${mandatory != null && mandatory == true}">
	<span class="mandatory">
		<spring:theme code="form.mandatory.character" /><span class="visuallyhidden">Mandatory field</span>
	</span>
</c:if>
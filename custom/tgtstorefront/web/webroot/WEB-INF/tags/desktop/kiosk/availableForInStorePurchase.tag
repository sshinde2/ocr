<%@ attribute name="inStockBulkyBoard" required="true" type="java.lang.Boolean" %>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${inStockBulkyBoard and not empty deliveryInStorePurchaseName and product.bulkyBoard.availableForInStorePurchase}">
	<li class="delivery-item has-tip" title="${deliveryInStorePurchase}">
		<spring:theme code="icon.tick" />
		<span class="label">${deliveryInStorePurchaseName}</span>
	</li>
</c:if>
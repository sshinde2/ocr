<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="kioskNav" tagdir="/WEB-INF/tags/desktop/kiosk/navigation" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ attribute name="hideBack" required="false" type="java.lang.Boolean"%>
<%@ attribute name="hideHome" required="false" type="java.lang.Boolean"%>

<c:if test="${kioskMode}">
	<div class="kiosk-nav small-tiles kiosk-links">
		<div class="core-links">
			<c:if test="${not hideBack}">
				<kioskNav:tile tileType="back" />
			</c:if>
			<c:if test="${not hideHome}">
				<kioskNav:tile tileType="home" />
			</c:if>
		</div>

		<kiosk:signOut />

		<div class="primary-links">
			<cms:slot var="component" contentSlot="${slots.KioskNavigationPrimary}">
				<cms:component component="${component}"/>
			</cms:slot>
		</div>
		<div class="secondary-links">
			<cms:slot var="component" contentSlot="${slots.KioskNavigationSecondary}">
				<cms:component component="${component}"/>
			</cms:slot>
		</div>
	</div>
</c:if>
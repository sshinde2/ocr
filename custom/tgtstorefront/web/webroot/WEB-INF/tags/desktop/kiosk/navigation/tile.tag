<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="tileType" required="true" type="java.lang.String"%>
<%@ attribute name="href" required="false" type="java.lang.String"%>
<%@ attribute name="linkClass" required="false" type="java.lang.String"%>

<c:if test="${empty href}">
	<c:set var="href" value="#kiosk-${tileType}" />
</c:if>

<div class="rich-tile kiosk-${tileType}">
	<a href="${href}" class="${linkClass}">
		<span class="heading">
			<span class="head-content">
				<span class="rich-icon"></span>
				<span class="text"><spring:theme code="kiosk.text.leftNav.${tileType}" /></span>
			</span>
		</span>
	</a>
</div>
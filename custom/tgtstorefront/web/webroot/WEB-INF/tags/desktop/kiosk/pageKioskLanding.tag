<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>


<template:master pageTitle="${pageTitle}">

	<jsp:attribute name="cmsExternalCss">
		<template:cmsExternalCss />
	</jsp:attribute>
	<jsp:attribute name="cmsInlineCss">
		<template:cmsInlineCss />
	</jsp:attribute>
	<jsp:attribute name="cmsExternalJavaScript">
		<template:cmsExternalJavaScript />
	</jsp:attribute>
	<jsp:attribute name="cmsInlineJavaScript">
		<template:cmsInlineJavaScript />
	</jsp:attribute>
	<jsp:body>
		<template:flush />
		<jsp:doBody/>
	</jsp:body>
</template:master>
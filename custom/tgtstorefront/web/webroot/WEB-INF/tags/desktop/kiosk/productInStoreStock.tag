<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="showInStock" required="true" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty product.bulkyBoard}">
	
	<c:if test="${showInStock and product.bulkyBoard.inStockForInStorePurchase}">
		<div class="in-stock"><spring:theme code="icon.tick"/><spring:theme code="kiosk.product.variants.in.stock" arguments="${product.bulkyBoard.posName}" /></div>
	</c:if>

	<c:if test="${not showInStock and not product.bulkyBoard.inStockForInStorePurchase}">
		<div class="out-of-stock"><spring:theme code="icon.cross"/><spring:theme code="kiosk.product.variants.soldout.instore" arguments="${product.bulkyBoard.posName}" /></div>
	</c:if>

</c:if>
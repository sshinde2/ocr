<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:if test="${not empty product.bulkyBoard and product.bulkyBoard.inStockForInStorePurchase}">
	<div class="btn-group variant-changeable">
		<button
			class="button-fwd-alt button-print-docket"
			data-error-href="/modal/print-docket-error"
			data-success-href="/modal/print-docket-success"
			data-product-name="${product.name}"
			data-item-code="${product.code}"
			data-price="${product.price.value}"
			data-barcode="${product.apn}"
			data-store-number="${product.bulkyBoard.posNumber}">
			<span class="icon">&nbsp;</span>
			<spring:theme code="kiosk.text.purchaseInStore" />
		</button>
	</div>
</c:if>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ attribute name="exclusionsLabel" required="false" %>
<%@ attribute name="exclusionsContent" required="true" %>
<%@ attribute name="exclusionsLink" required="false" type="java.lang.Object" %>
<%@ attribute name="displayExclusionLabel" required="false" %>


<div class="${not empty exclusionsLink ? '' : 'vis-toggle'} exclusions">
	<div class="vis-toggle-container">
		<c:choose>
			<c:when test="${not empty exclusionsLink}">
				<span class="exclusions-link">
					<span class="exclusion-button"><cms:component component="${exclusionsLink}"/></span>
					<c:if test="${displayExclusionLabel}">
						<span class="hide-for-small"><cms:component component="${exclusionsLink}"/></span>
					</c:if>
				</span>
			</c:when>
			<c:otherwise>
				<p class="vis-el exclusion-content">${exclusionsContent}<span class="vis-trigger close"><spring:theme code="text.exclusions.close" /></span></p>
				<p class="vis-trigger exclusion-button">
					<span class="visuallyhidden"><spring:theme code="text.exclusions.toggle" /></span>
				</p>
				<c:if test="${not empty exclusionsLabel and displayExclusionLabel}">
					<span class="hide-for-small exclusions-label vis-trigger">${exclusionsLabel}</span>
				</c:if>
			</c:otherwise>
		</c:choose>
	</div>
</div>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData" %>
<%@ attribute name="disableMultiselect" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<div class="facets facets-common">
	<div class="hide-for-small">
		<nav:facetNavAppliedFilters pageData="${pageData}" />
	</div>
	<feature:enabled name="uiNewFacetDisplay">
		<facet:container pageData="${pageData}" disableMultiselect="${disableMultiselect}" />
	</feature:enabled>
	<feature:disabled name="uiNewFacetDisplay">
		<nav:facetNavRefinements pageData="${pageData}" />
	</feature:disabled>
</div>

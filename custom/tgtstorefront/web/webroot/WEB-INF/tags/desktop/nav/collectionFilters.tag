<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="filters" required="true" type="java.util.List" %>
<%@ attribute name="viewAll" required="false" type="java.lang.Boolean" %>
<%@ attribute name="viewAllData" required="false" type="java.lang.String" %>
<%@ attribute name="viewAllLink" required="false" type="java.lang.String" %>
<%@ attribute name="viewAllName" required="false" type="java.lang.String" %>
<%@ attribute name="viewAllTotal" required="false" type="java.lang.Integer" %>


<div class="refine button-vibrant only-for-small facet-panel-trigger ProductCollection-filter"><%--
	--%><a href="#" rel="nofollow" class="refine-button"><%--
		--%><spring:theme code="search.page.refine" /><%--
	--%></a><%--
--%></div><%--
--%><div class="facets facets-common facet-panel-container"><%--
	--%><div class="FilterContainer facet-section"><%--
		--%><div class="panel facet-holder hide-for-small"><%--
			--%><div class="refine-head only-for-small"><%--
				--%><span class="facet-title"><spring:theme code="search.page.refine" /></span><%--
				--%><button class="facet-panel-trigger"><%--
					--%><spring:theme code="icon.plain-cross" /><%--
				--%></button><%--
			--%></div><%--
			--%><div class="facet-options"><%--
				--%><c:forEach items="${filters}" var="_collection"><%--
					--%><a href="#/${_collection.id}" class="Filter"
						data-description="${_collection.description}"
						data-title="${_collection.name}"
						data-results="${_collection.countOfLooks}"
						><%--
					--%><span class="Filter-name">${_collection.name}</span><%--
					--%><span class="Filter-count">(${_collection.countOfLooks})</span><%--
				--%></a><%--
				--%></c:forEach><%--
				--%><c:if test="${viewAll}"><%--
					--%><a href="${viewAllLink}" class="Filter" data-results="${viewAllTotal}" ${viewAllData}><%--
						--%><span class="Filter-name">${viewAllName}</span><%--
						--%><span class="Filter-count">(${viewAllTotal})</span><%--
					--%></a><%--
				--%></c:if><%--
			--%></div><%--
		--%></div><%--
	--%></div><%--
--%></div>

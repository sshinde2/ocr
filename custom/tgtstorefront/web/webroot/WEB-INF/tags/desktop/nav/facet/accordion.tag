<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>

<component:accordion type="checkbox" cssClass="Accordion--slim facet-accordion-list">
	<jsp:attribute name="accordionContent">
		<jsp:doBody />
	</jsp:attribute>
</component:accordion>

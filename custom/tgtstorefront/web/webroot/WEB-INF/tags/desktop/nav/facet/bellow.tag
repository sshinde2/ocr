<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ attribute name="subgroup" required="false" %>
<%@ attribute name="name" required="false" %>
<%@ attribute name="multiSelect" required="false" %>
<%@ attribute name="facets" required="false" type="java.lang.Object" %>
<%@ attribute name="code" required="false" %>
<%@ attribute name="category" required="false" %>
<%@ attribute name="isolated" required="false" %>
<%@ attribute name="seoFollowEnabled" required="false" %>

<c:if test="${not seoFollowEnabled}">
	<spring:theme code="search.nav.facet.nofollow" var="seoNoFollow" />
</c:if>

<c:set var="isParent" value="${not empty facets.facets and fn:length(facets.facets) gt 1}" />
<c:set var="type" value="${subgroup ? 'subgroup' : 'facet'}" />
<c:set var="facetName" value="${name}" />
<c:choose>
	<c:when test="${category}">
		<facet:categoryList facets="${facets}" facetName="${name}" additionalAttributes="${seoNoFollow}" />
	</c:when>
	<c:when test="${isolated}">
		<facet:isolatedList facets="${facets}" facetName="${name}" additionalAttributes="${seoNoFollow}" />
	</c:when>
	<c:otherwise>
		<component:accordionItem id="${code}"
			groupName="${code}"
			checked="${facets.expand}"
			heading="${name}"
			type="checkbox"
			cssClass="FacetAccordion ${subgroup ? 'Accordion-item--tinted' : 'Accordion-item--light'}"
			contentClass="${facets.expand ? ' opened ' : '' } ${not subgroup ? ' is-scrollable ' : ''}"
		>
			<jsp:attribute name="accordionItemContent">
				<facet:itemList className="${isParent ? 'FacetAccordion-list--collapse' : ''}">
					<c:if test="${not empty facets}">
						<facet:section facets="${facets.values}" facetName="${facetName}" facetID="${code}" additionalAttributes="${seoNoFollow}" />
					</c:if>
					<c:if test="${not empty facets.facets}">
						<c:forEach items="${facets.facets}" var="facet" varStatus="loop">
							<c:if test="${fn:length(facets.facets) eq 1}">
								<facet:section facets="${facet.values}" facetName="${facetName}" facetID="${code}" additionalAttributes="${seoNoFollow}" />
							</c:if>
							<c:if test="${fn:length(facets.facets) gt 1}">
								<facet:bellow
									facets="${facet}"
									subgroup="${true}"
									code="${facet.code}"
									multiSelect="${true}"
									name="${facet.name}"
									category="${facet.category}"
									seoFollowEnabled="${facet.seoFollowEnabled}"/>
							</c:if>
						</c:forEach>
					</c:if>
				</facet:itemList>
			</jsp:attribute>
		</component:accordionItem>
	</c:otherwise>
</c:choose>

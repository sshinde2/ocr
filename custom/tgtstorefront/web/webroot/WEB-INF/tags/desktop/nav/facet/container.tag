<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData" %>
<%@ attribute name="disableMultiselect" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<c:forEach items="${pageData.facets}" var="facetItem">
	<c:set var="facet">
		<facet:bellow
			facets="${facetItem}"
			subgroup="${false}"
			name="${facetItem.name}"
			code="${facetItem.code}"
			multiSelect="${facetItem.multiSelect}"
			category="${facetItem.category}"
			isolated="${facetItem.isolated}"
			seoFollowEnabled="${facetItem.seoFollowEnabled}"
		/>
	</c:set>
	<c:choose>
		<c:when test="${facetItem.category}">
			<c:set var="categoryFacet">${categoryFacet}${facet}</c:set>
		</c:when>
		<c:otherwise>
			<c:set var="otherFacets">${otherFacets}${facet}</c:set>
		</c:otherwise>
	</c:choose>
</c:forEach>

<feature:enabled name="newSubCategoriesUx">${categoryFacet}</feature:enabled>

<div class="facet-panel-container ${disableMultiselect ? 'multiselect-disabled' : ''}" data-query-url="${pageData.currentQuery.wsUrl}">
	<div class="panel facet-holder hide-for-small">
		<div class="refine-head only-for-small facet-panel-trigger">
			<feature:enabled name="uiRefinementCount">
				<div class="facet-items-count">
					<span class="count">${pageData.pagination.totalNumberOfResults}</span><br/>
					<span class="link"><spring:theme code="search.page.count.link" /></span>
					<span class="visuallyhidden"><spring:theme code="search.page.count.screenreader" arguments="${pageData.pagination.totalNumberOfResults}" /></span>
				</div>
			</feature:enabled>
			<span class="facet-title"><spring:theme code="search.page.refine" /></span>
			<button class="facet-panel-button"><span class="iconf iconf-plain-cross" aria-hidden="true" data-icon="&#xe608;"><span class="visuallyhidden"><spring:theme code="search.page.close" /></span></span></button>
		</div>
	</div>
	<div class="facet-options">
		<c:if test="${not empty categoryName}">
			<h1 class="facet-category-info only-for-small">${categoryName}</h1>
		</c:if>

		<feature:disabled name="newSubCategoriesUx">${categoryFacet}</feature:disabled>

		<facet:accordion>
			${otherFacets}
			<a class="Accordion-clearAll ${pageData.showClearAllUrl ? 'active': ''}" href="${pageData.clearAllUrl}" data-query-url="${pageData.clearAllQueryUrl}"><%--
				--%><spring:theme code="search.page.clearAll" /><%--
			--%></a>
		</facet:accordion>
	</div>
</div>
<common:handlebarsTemplates template="facetAccordion" />

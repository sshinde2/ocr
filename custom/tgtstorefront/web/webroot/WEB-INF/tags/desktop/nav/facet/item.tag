<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="id" required="false" %>
<%@ attribute name="name" required="true" %>
<%@ attribute name="url" required="true" %>
<%@ attribute name="queryUrl" required="false" %>
<%@ attribute name="count" required="false" %>
<%@ attribute name="type" required="false" %>
<%@ attribute name="selected" required="false" %>
<%@ attribute name="category" required="false" %>
<%@ attribute name="itemClass" required="false" %>
<%@ attribute name="index" required="false" type="java.lang.Integer" %>
<%@ attribute name="additionalAttributes" required="false" type="java.lang.String" %>

<c:choose>
	<c:when test="${type eq 'categoryList'}">
		<c:set var="resultClass" value="FacetCategory-count count" />
		<c:set var="itemClass" value="${itemClass} FacetCategory-listing" />
		<c:set var="titleClass" value="FacetCategory-title facet-selection"  />
	</c:when>
	<c:otherwise>
		<c:set var="resultClass" value="FacetAccordion-count count" />
		<c:set var="itemClass" value="${itemClass} FacetAccordion-listing ${selected ? 'selected' : ''}" />
		<c:set var="titleClass" value="FacetAccordion-title facet-selection" />
	</c:otherwise>
</c:choose>

<li class="${itemClass}">
	<a href="${url}" title="${name}" class="${titleClass}" ${additionalAttributes} data-facet-id="${id}" data-count="${count}" data-query-url="${queryUrl}" data-category="${category}" data-name="${name}"><%--
		--%><c:set var="itemName"><%--
			--%><c:out escapeXml="false" value="${target:abbreviateString(name, 32)}" /><%--
		--%></c:set><%--
		--%><spring:theme code="search.nav.facetApply" arguments="${itemName}||${category}" argumentSeparator="||" /><%--
			--%><c:if test="${count != ''}"><%--
				--%><span class="${resultClass}"> <spring:theme code="search.nav.facetValueCount" arguments="${count}" /></span><%--
			--%></c:if><%--
	--%></a>
</li>

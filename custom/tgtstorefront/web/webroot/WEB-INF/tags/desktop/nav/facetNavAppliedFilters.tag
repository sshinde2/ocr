<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:if test="${not empty pageData.breadcrumbs}">
	<div class="facets-selection">
		<span class="selected-head"><spring:theme code="search.nav.appliedFilters"/></span>
		<ul>
			<c:forEach items="${pageData.breadcrumbs}" var="breadcrumb">
				<li class="selected-facet">
					<a href="${breadcrumb.removeQuery.url}" rel="nofollow" class="facet-link"><c:out escapeXml="false" value="${target:abbreviateString(breadcrumb.facetValueName, 32)}" />&nbsp;<spring:theme code="icon.plain-cross" /></a>
				</li>
			</c:forEach>
		</ul>
	</div>
</c:if>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="facetData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>

<c:if test="${(not empty facetData.values) || (not empty facetData.facets)}">
	<div class="item facet-section" data-collapse="true"
		data-global-persistance="true" data-code="${facetData.name}">
		<div class="facet-category">
			<span class="collapse-toggle facet-title">
				<span class="facet-name">${facetData.name}</span>
				<span class="handle"><spring:theme code="icon.right-arrow-large" /></span>
			</span>
		</div>

		<ul class="facet-block ${facetData.multiSelect ? '' : ' multi-select'}">
			<c:if test="${not empty facetData.values}">
				<nav:facetSection facets="${facetData.values}"
					facetData="${facetData}" searchPageData="${searchPageData}" />
			</c:if>
			<c:if test="${not empty facetData.facets}">
				<c:forEach items="${facetData.facets}" var="facet" varStatus="loop">
					<c:if test="${fn:length(facetData.facets) == 1}">
						<nav:facetSection facets="${facet.values}"
							facetData="${facetData}" searchPageData="${searchPageData}" />
					</c:if>
					<c:if test="${fn:length(facetData.facets) gt 1}">
						<li class="option FacetGroup">
							<div class="FacetGroup-Header">
								<p>${facet.name}</p>
								<span class="iconf iconf-down-arrow-big" aria-hidden="true"></span>
							</div>
							<ul class="FacetGroup-Item ${facetData.multiSelect ? '' : ' multi-select'}">
								<nav:facetSection facets="${facet.values}"
									facetData="${facetData}" searchPageData="${searchPageData}" />
							</ul>
						</li>

					</c:if>
				</c:forEach>
			</c:if>
		</ul>
	</div>
</c:if>

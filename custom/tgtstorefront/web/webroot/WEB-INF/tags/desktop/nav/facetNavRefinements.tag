<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageData" required="true" type="de.hybris.platform.commerceservices.search.facetdata.FacetSearchPageData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<div class="facet-panel-container">

	<div class="panel facet-holder hide-for-small">
		<div class="refine-head only-for-small facet-panel-trigger">
			<feature:enabled name="uiRefinementCount">
				<div class="facet-items-count">
					<span class="count">${pageData.pagination.totalNumberOfResults}</span><br/>
					<span class="link"><spring:theme code="search.page.count.link" /></span>
					<span class="visuallyhidden"><spring:theme code="search.page.count.screenreader" arguments="${pageData.pagination.totalNumberOfResults}" /></span>
				</div>
			</feature:enabled>
			<span class="facet-title"><spring:theme code="search.page.refine" /></span>
			<button class="facet-panel-button"><span class="iconf iconf-plain-cross" aria-hidden="true" data-icon="&#xe608;"><span class="visuallyhidden"><spring:theme code="search.page.close" /></span></span></button>
		</div>
		<div class="facet-options">
			<c:if test="${not empty categoryName}">
				<h1 class="facet-category-info only-for-small">${categoryName}</h1>
			</c:if>
			<c:forEach items="${pageData.facets}" var="facet">
				<nav:facetNavRefinementFacet facetData="${facet}" />
			</c:forEach>
		</div>
	</div>
</div>

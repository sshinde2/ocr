<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ attribute name="facets" required="true" type="java.lang.Object" %>
<%@ attribute name="facetData" required="true" type="java.lang.Object" %>
<%@ attribute name="searchPageData" required="true" type="java.lang.Object" %>
<%@ attribute name="topLimit" required="false" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:forEach items="${facets}" var="facetValue" varStatus="facetStatus">
	<c:set var="className" value="facet-link" />
	<c:if test="${facetData.multiSelect}">
		<c:set var="className" value="${className} multi" />
	</c:if>
	<c:if test="${facetValue.selected}">
		<c:set var="className" value="${className} selected" />
	</c:if>
	<c:set var="classAttr">class="${className}"</c:set>
	<li class="option" >
		<a href="${facetValue.query.url}" title="${facetValue.name}" ${not empty className ? classAttr : ""} rel="nofollow"><c:out escapeXml="false" value="${target:abbreviateString(facetValue.name, 32)}" />&nbsp;<span class="result-count"><spring:theme code="search.nav.facetValueCount" arguments="${facetValue.count}"/></span></a>
	</li>
</c:forEach>

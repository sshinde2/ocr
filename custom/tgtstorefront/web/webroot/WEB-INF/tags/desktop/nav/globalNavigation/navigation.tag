<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="globalNavigation" tagdir="/WEB-INF/tags/desktop/nav/globalNavigation" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<li class="only-for-small Menu-mobileHeading">
	<a href="${fullyQualifiedDomainName}" title="Target Home">
		<spring:theme code="header.link.home" arguments="Rondel--medium Rondel--white" />
		<span class="Menu-mobileHeadingText"><spring:theme code="link.withArrow" arguments="Homepage" /></span>
	</a>
</li>
<c:forEach items="${departments}" var="department" varStatus="departmentStatus">
	<c:set var="noColumns" value="${fn:length(department.columns)}" />
	<c:set var="departmentItemClass">MenuItem MenuItem--${departmentStatus.count}</c:set>
	<c:if test="${noColumns gt 0}"><c:set var="departmentItemClass">${departmentItemClass} is-openable</c:set></c:if>
	<c:if test="${departmentStatus.first}"><c:set var="departmentItemClass">${departmentItemClass} is-first</c:set></c:if>
	<c:if test="${departmentStatus.last}"><c:set var="departmentItemClass">${departmentItemClass} is-last</c:set></c:if>

	<li class="${departmentItemClass}">
		<a href="${department.link}" class="MenuItem-title" data-title="${department.title}" title="${department.title}">
			<c:if test="${not empty departmentsSVGUrl}">
				<c:set var="svg">
					<div class="mm-Departments-picture u-scalingSvgContainer">
						<svg class="u-scalingSvg" viewBox="0 0 75 75">
							<use xlink:href="#shape-department-${department.title}"></use>
						</svg>
					</div>
				</c:set>
				<script type="text/x-image-deferred" class="defer-svg" data-screen-support="tiny small" data-definition-url="${departmentsSVGUrl}">${svg}</script>
			</c:if>
			<span class="MenuItem-titleText" data-title="${department.title}" title="${department.title}">
				<c:out value="${department.title}" />
			</span>
		</a>

		<c:if test="${noColumns > 0}">
			<div class="Menu-container Menu-container--columns${target:intToString(noColumns)}" data-id="${department.title}">
				<div class="Menu-containerInner">
					<c:set var="highlightedCount" value="0" />
					<c:forEach items="${department.columns}" var="column">
						<c:if test="${column.style eq 'HighlightedColumn'}">
							<c:set var="highlightedCount" value="${highlightedCount + 1}" />
						</c:if>
					</c:forEach>

					<c:forEach items="${department.columns}" var="column" varStatus="columnStatus">
						<c:set var="isAlwaysOpen" value="${column.style eq 'HighlightedColumn' and highlightedCount eq 1}" />
						<globalNavigation:column
							column="${column}"
							lastColumn="${columnStatus.last}"
							department="${department}"
							isAlwaysOpen="${isAlwaysOpen}" />
					</c:forEach>
				</div>
			</div>
		</c:if>
	</li>
</c:forEach>

<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="megamenu" tagdir="/WEB-INF/tags/desktop/nav/megamenu" %>
<%@ attribute name="column" required="true" type="au.com.target.tgtstorefront.navigation.megamenu.MegaMenuColumn" %>
<%@ attribute name="columnsStr" required="true" type="java.lang.String" %>
<%@ attribute name="lastColumn" required="true" type="java.lang.Boolean" %>

<c:set var="columnClass">mm-Cat-column</c:set>
<c:if test="${column.style eq 'HighlightedColumn'}">
	<c:set var="columnClass">${columnClass} mm-Cat-column--highlighted</c:set>
</c:if>
<c:if test="${lastColumn}">
	<c:set var="columnClass">${columnClass} is-last</c:set>
</c:if>

<div class="${columnClass}">
	<c:forEach items="${column.sections}" var="section">
		<megamenu:section section="${section}" />
	</c:forEach>
</div>

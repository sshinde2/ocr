<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="section" required="true" type="au.com.target.tgtstorefront.navigation.megamenu.MegaMenuSection" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="megamenu" tagdir="/WEB-INF/tags/desktop/nav/megamenu" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:set var="missingTitle" value="${empty section.title}" />
<c:set var="isAccordion" value="${not missingTitle and fn:length(section.entries) > 0}" />

<c:if test="${isAccordion}">
	<c:set var="accordionClass" value="mm-Cat-accordion" />
</c:if>

<c:if test="${not isAccordion}">
	<c:set var="hideForSmall" value="${missingTitle ? 'hide-for-small' : ''}" />
	<c:set var="headingLinkOnly">mm-Cat-heading--linkOnly</c:set>
</c:if>

<div class="mm-Cat-section ${accordionClass} ${hideForSmall}">
	<c:if test="${not empty section.title}">
		<c:if test="${not empty section.link}">
			<c:set var="wrap"><a href="${target:validUrlString(section.link)}" title="${section.title}">||</a></c:set>
			<c:set var="anchor" value="${fn:split(wrap, '||')}" />
		</c:if>
		<c:choose>
			<c:when test="${section.style eq 'NewLink'}">
				<c:set var="sectionClass" value=" mm-Cat-heading--new" />
			</c:when>
			<c:when test="${section.style eq 'SaleLink'}">
				<c:set var="sectionClass" value=" mm-Cat-heading--sale" />
			</c:when>
		</c:choose>
		<span class="mm-Cat-heading ${headingLinkOnly} ${sectionClass}">
			<c:out value="${anchor[0]}" escapeXml="false" />
			<c:out value="${section.title}" />
			<c:out value="${anchor[1]}" escapeXml="false" />
			<c:if test="${isAccordion}">
				<span class="mm-Cat-toggle">
					<spring:theme code="icon.expand" />
					<spring:theme code="icon.contract" />
				</span>
			</c:if>
		</span>
	</c:if>
	<nav class="mm-Cat-list">
		<c:forEach items="${section.entries}" var="entry">
			<megamenu:sectionEntry entry="${entry}" />
		</c:forEach>
	</nav>
</div>

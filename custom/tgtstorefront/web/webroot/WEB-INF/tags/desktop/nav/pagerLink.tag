<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="isEnabled" required="true" %>
<%@ attribute name="pagerWide" required="true" %>
<%@ attribute name="code" required="true" %>
<%@ attribute name="direction" required="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="wsUrl" required="false" %>
<%@ attribute name="number" required="true" %>
<%@ attribute name="themeMsgKey" required="true" %>
<%@ attribute name="cssClass" required="false" %>
<%@ attribute name="disabledLink" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:theme code="${themeMsgKey}.link${code}Page" var="linkTitle" />
<c:set var="cssClass">${cssClass} ${pagerWide ? ' pager-wide' : ''}</c:set>

<c:if test="${not isEnabled}">
	<c:set var="cssClass">${cssClass} disabled</c:set>
</c:if>
<c:set var="delimiter" value="${fn:contains(searchUrl, '?') ? '&' : '?'}" />
<c:if test="${number > 0}">
	<c:set var="searchUrl">${searchUrl}${delimiter}page=${number}</c:set>
</c:if>
<a href="${disabledLink ? '' : searchUrl}" title="${linkTitle}" class="${cssClass}" data-query-url="${wsUrl}&page=${number}">
	<spring:theme code="icon.${direction}-arrow-large-nospace"/>
</a>

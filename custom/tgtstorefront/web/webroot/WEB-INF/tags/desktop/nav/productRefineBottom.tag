<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="wsUrl" required="false" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="msgKey" required="false" %>
<%@ attribute name="hasProducts" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="themeMsgKey" value="${not empty msgKey ? msgKey : 'search.page'}"/>
<spring:theme var="itemsPerPageText" code="${themeMsgKey}.itemsperpage" text="Items per page" />

<feature:enabled name="uiNewFacetDisplay">
	<div class="RefineMenu RefineMenu--noBorder RefineMenu--verticalForMobile">
		<div class="RefineMenu-section RefineMenu-section--noBorder is-nrpp">
			<c:forEach items="${itemsPagePageOptions}" var="itemPagePageOption">
				<c:remove var="itemsPerPageUrl" />
				<c:set var="active" value="${itemPagePageOption.value == searchPageData.pagination.pageSize}"/>
				<c:set var="itemsPerPageUrl" value="${fn:replace(searchUrl, '%7BrecordsPerPage%7D', itemPagePageOption.value)}" />
				<c:set var="itemWsUrl" value="${fn:replace(wsUrl, '%7BrecordsPerPage%7D', itemPagePageOption.value)}" />
				<a href="${itemsPerPageUrl}"
					class="RefineMenu-nrpp ${active ? 'is-active' : ''} nrpp"
					title="${itemsPerPageText} - ${itemPagePageOption.displayValue}"
					rel="nofollow"
					data-query-url="${itemWsUrl}"
					data-size="${itemPagePageOption.value}"
				>${itemPagePageOption.displayValue}</a>
			</c:forEach>
		</div>

		<c:set var="searchUrl" value="${fn:replace(searchUrl, '%7BrecordsPerPage%7D', searchPageData.pagination.pageSize)}" />

		<nav:paginationSection
			currentPage="${searchPageData.pagination.currentPage}"
			numberOfPages="${searchPageData.pagination.numberOfPages}"
			pageClassName="${searchPageData.pagination.numberOfPages eq 1 ? 'is-hidden' : ''}"
			themeMsgKey="${themeMsgKey}"
			searchUrl="${searchUrl}"
			wsUrl="${wsUrl}"
			section="bottom"
		/>
	</div>
</feature:enabled>
<feature:disabled name="uiNewFacetDisplay">
	<div class="prod-refine prod-refine-bottom ${not hasProducts ? 'is-hidden' : ''}">
		<ul class="pager pager-options hide-for-small">
			<li class="label">${itemsPerPageText}</li>

			<c:forEach items="${itemsPagePageOptions}" var="itemPagePageOption">
				<c:remove var="itemsPerPageUrl" />
				<c:set var="active" value="${itemPagePageOption.value == searchPageData.pagination.pageSize}"/>
				<c:set var="itemsPerPageUrl" value="${fn:replace(searchUrl, '%7BrecordsPerPage%7D', itemPagePageOption.value)}" />
				<c:set var="itemWsUrl" value="${fn:replace(wsUrl, '%7BrecordsPerPage%7D', itemPagePageOption.value)}" />
				<li class="option ${active ? 'is-active' : ''}">
					<a href="${itemsPerPageUrl}"
						class="nrpp"
						title="${itemsPerPageText} - ${itemPagePageOption.displayValue}"
						rel="nofollow"
						data-query-url="${itemWsUrl}"
						data-size="${itemPagePageOption.value}"
					>${itemPagePageOption.displayValue}</a>
				</li>
			</c:forEach>
		</ul>

		<c:set var="searchUrl" value="${fn:replace(searchUrl, '%7BrecordsPerPage%7D', searchPageData.pagination.pageSize)}" />

		<nav:pagination searchPageData="${searchPageData}" searchUrl="${searchUrl}" wsUrl="${wsUrl}" />
	</div>
</feature:disabled>

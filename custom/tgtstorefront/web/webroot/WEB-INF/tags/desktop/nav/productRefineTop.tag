<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="searchUrl" required="true" %>
<%@ attribute name="wsUrl" required="false" %>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ attribute name="urlElements" required="true" type="au.com.target.tgtstorefront.controllers.pages.url.URLElements" %>
<%@ attribute name="msgKey" required="false" %>
<%@ attribute name="hasProducts" required="false" %>
<%@ attribute name="contentAboveRefineTop" required="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"  %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"  %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="facet" tagdir="/WEB-INF/tags/desktop/nav/facet" %>

<c:set var="themeMsgKey" value="${not empty msgKey ? msgKey : 'search.page'}"/>
<c:set var="searchUrl" value="${fn:replace(searchUrl, '%7BrecordsPerPage%7D', searchPageData.pagination.pageSize)}" />
<c:set var="newFacetDisplay" value="${false}" />
<feature:enabled name="uiNewFacetDisplay">
	<c:set var="newFacetDisplay" value="${true}" />
</feature:enabled>

<div class="prod-refine prod-refine-top ${contentAboveRefineTop and not newFacetDisplay ? 'prod-refine-divider' : ''} ${not hasProducts ? 'is-hidden' : ''}">
	<c:choose>
		<c:when test="${not newFacetDisplay}">
			<c:if test="${not empty searchPageData.breadcrumbs}">
				<div class="only-for-small">
					<nav:facetNavAppliedFilters pageData="${searchPageData}" />
				</div>
			</c:if>
			<div class="refine-sort">

				<c:if test="${fn:length(searchPageData.facets) > 0}">
					<div class="refine button-vibrant only-for-small facet-panel-trigger">
						<a href="#" rel="nofollow" class="refine-button">
							<spring:theme code="search.page.refine" />
						</a>
					</div>
				</c:if>

				<c:if test="${not empty searchPageData.targetSortData}">
					<form class="sortby sort-form" id="sort-form" method="get">
						<label for="sort-form" class="hide-for-small"><spring:theme code="${themeMsgKey}.sortTitle"/></label>
						<select id="sort-options" name="sort-options" class="chosen-select sort-options">
							<c:forEach items="${searchPageData.targetSortData}" var="sort">
								<option value="${sort.url}" ${sort.selected ? 'selected="selected"' : ''}>${sort.code}</option>
							</c:forEach>
						</select>
					</form>
				</c:if>

			</div>

			<div class="filter">
				<ul class="view-as">
					<li class="label hide-for-tiny"><spring:theme code="${themeMsgKey}.viewAsTitle" text="Change view:" /></li>

					<c:set var="currentViewAs">viewAs=${urlElements.viewAs}</c:set>
					<c:set var="toggleOption" value="${actualViewAsType eq 'grid' ? 'look' : 'grid'}" />


					<li class="option view-as-${toggleOption}">
						<c:set var="newViewAs">viewAs=${toggleOption}</c:set>
						<c:set var="viewAsUrl" value="${fn:replace(searchUrl, currentViewAs, newViewAs)}" />
						<%-- currentQuery.url is based on selected sort options so the page will not already be there. --%>
						<c:if test="${searchPageData.pagination.currentPage > 0}">
							<c:set var="viewAsUrl">${viewAsUrl}&page=${searchPageData.pagination.currentPage}</c:set>
						</c:if>
						<a href="${viewAsUrl}" rel="nofollow">
							<spring:theme code="icon.view-${toggleOption}" />
							<span class="visuallyhidden">
								<spring:theme code="${themeMsgKey}.viewAs.${viewAsType.value}" />
							</span>
						</a>
					</li>
				</ul>
				<nav:pagination searchPageData="${searchPageData}" searchUrl="${searchUrl}" labelClass="hide-for-small" wsUrl="${wsUrl}" />
			</div>
		</c:when>
		<c:otherwise>
			<div class="RefineMenu RefineMenu--noBorder is-sticky">
				<div class="RefineMenu-section RefineMenu-section--fullWidth ${kioskMode ? 'RefineMenu-section--noBorder' : ''}">
					<button type="button" class="RefineMenu-refineButton facet-panel-trigger"><%--
						--%><spring:theme code="icon.refine" />&nbsp;<spring:theme code="search.page.refine" /><%--
					--%></button>

					<%-- Available Online Facet in Refine Menu --%>
					<c:set var="facetClass" value="RefineMenu-facet is-hidden" />
					<c:forEach items="${searchPageData.facets}" var="facet">
						<c:if test="${facet.code eq 'availableOnline'}">
							<c:set var="facetClass" value="RefineMenu-facet" />
							<c:set var="availableOnlineFacet" value="${facet.values[0]}" />
						</c:if>
					</c:forEach>

					<facet:item
						itemClass="${facetClass}"
						id="${availableOnlineFacet.code}"
						name="${availableOnlineFacet.name}"
						category="${availableOnlineFacet.name}"
						url="${availableOnlineFacet.query.url}"
						selected="${availableOnlineFacet.selected}"
						count=""
						queryUrl="${availableOnlineFacet.query.wsUrl}" />

					<c:choose>
						<c:when test="${kioskMode}">
							<form class="sortby sort-form" id="sort-form" method="get">
								<select name="sort-options" class="chosen-select sort-options-multi">
									<c:forEach items="${searchPageData.targetSortData}" var="sort">
										<option value="${sort.wsUrl}" ${sort.selected ? 'selected="selected"' : ''} data-name="${sort.code}">${sort.code}</option>
									</c:forEach>
								</select>
							</form>
						</c:when>
						<c:otherwise>
							<div class="RefineMenu-sort sort-form">
								<c:set var="sortOptions" />
								<c:forEach items="${searchPageData.targetSortData}" var="sort">
									<c:if test="${sort.selected}">
										<c:set var="sortLabel" value="${sort.code}" />
									</c:if>
									<c:set var="sortOptions">
										${sortOptions}
										<option value="${sort.wsUrl}" ${sort.selected ? 'selected="selected"' : ''} data-name="${sort.code}">${sort.code}</option>
									</c:set>
								</c:forEach>
								<c:set var="firstSortOption">${fn:length(searchPageData.targetSortData) > 0 ? searchPageData.targetSortData[0].code : ''}</c:set>
								<label for="refine-form" class="RefineMenu-sortLabel">${not empty sortLabel ? sortLabel : firstSortOption}</label>
								<select name="refine-form" id="refine-form" class="RefineMenu-sortSelect sort-options-multi">
									${sortOptions}
								</select>
							</div>
						</c:otherwise>
					</c:choose>
				</div>
				<c:if test="${searchPageData.pagination.numberOfPages eq 1}">
					<c:set var="pageClassName" value="is-hidden" />
				</c:if>
				<nav:paginationSection
					currentPage="${searchPageData.pagination.currentPage}"
					numberOfPages="${searchPageData.pagination.numberOfPages}"
					pageClassName="${pageClassName} hide-for-small"
					themeMsgKey="${themeMsgKey}"
					searchUrl="${searchUrl}"
					wsUrl="${wsUrl}"
				/>
			</div>
		</c:otherwise>
	</c:choose>
</div>

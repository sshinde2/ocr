<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="spellingSuggestions" required="true" type="java.util.List" %>
<%@ attribute name="hasResults" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="did-you-mean">
	<c:choose>
		<c:when test="${fn:length(spellingSuggestions) > 0}">
			<c:set var="spellingSuggestion" value="${spellingSuggestions[0]}" />
			<c:url value="${targetUrlsMapping.search}" var="didYouMeanUrl">
				<c:param name="text" value="${spellingSuggestion}" />
			</c:url>
			<c:set var="suggestionTag">
				<a class="suggestion" href="${didYouMeanUrl}">${spellingSuggestion}</a>
			</c:set>
			<spring:theme code="search.spellingSuggestion.prompt" arguments="${suggestionTag}" />
		</c:when>
		<c:when test="${hasResults}">
			<spring:theme code="search.lookingFor" />
		</c:when>
	</c:choose>
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/desktop/order" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:set var="oddClass" value="odd" />
<c:set var="evenClass" value="even" />
<c:set var="rowClass" value="${oddClass}" />
<c:set var="cncOrderStatus" value="${orderData.cncCustomOrderStatus}" />
<c:set var="cncDeliveryMode" value="${orderData.deliveryModeStoreDelivery}" />
<c:set var="completedCncOrderWithConsignmentPickedUpDate" value="${cncDeliveryMode and orderData.cncOrderWithConsignmentPickedUpDate}" />
<c:set var="completedCncOrderReadyForPickup" value="${cncDeliveryMode and orderData.cncOrderReadyForPickup}" />
<c:set var="cncOrderRefunded" value="${ orderData.cncOrderRefunded }" />
<c:set var="cncOrderPartialRefund" value="${orderData.cncOrderPartialRefund }" />
<c:set var="columns" value="4" />
<table class="summary-table full-table">
	<thead>
		<tr>
			<th id="entry-description" class="description"><spring:theme code="account.page.entry.description" /></th>
			<th id="entry-price" class="price"><spring:theme code="account.page.entry.price" /></th>
			<th id="entry-quantity" class="quantity"><spring:theme code="account.page.entry.quantity" /></th>
			<th id="entry-total" class="total price"><spring:theme code="account.page.entry.total" /></th>
		</tr>
	</thead>
	<tbody>
		<c:if test="${orderData.status eq 'PARKED'}">
			<tr class="summary-row row-shipstatus"><td colspan="4">Order has not shipped yet</td></tr>
		</c:if>
		<c:if test="${(completedCncOrderWithConsignmentPickedUpDate or completedCncOrderReadyForPickup or cncOrderPartialRefund) and not cncOrderRefunded}">
			<tr class="divider">
				<td colspan="${columns}">
					<spring:theme code="account.page.${cncOrderStatus}" />
				</td>
			</tr>
		</c:if>
		<c:set var="sentDigitalItems" value="" />
		<c:choose>
			<c:when test="${not empty orderData.consignment}">
				<c:forEach items="${orderData.consignment}" var="consignment">
					<c:choose>
						<c:when test="${consignment.isDigitalDelivery and not empty consignment.code and fn:length(consignment.entries) gt 0}">
							<c:set var="sentDigitalItems">
								${sentDigitalItems}
								<c:forEach items="${consignment.entries}" var="entry">
									<c:set var="appliedPromotions" value="${target:getAppliedPromotionForEntry(orderData, entry.orderEntry.entryNumber)}" />
									<c:set var="consignmentEntryQuantity" value="${entry.quantity}" />
									<c:set var="consigmentEntryTotalPrice" value="${entry.totalPrice}" />
									<c:if test="${entry.shippedQuantity > 0 and (consignment.statusDisplay eq 'Picked' or consignment.statusDisplay eq 'Shipped')}">
										<c:set var="consignmentEntryQuantity" value="${entry.shippedQuantity}" />
										<c:set var="consigmentEntryTotalPrice" value="${entry.totalShippedPrice}" />
									</c:if>
									<order:orderEntryItem
										entry="${entry.orderEntry}"
										appliedPromotions="${appliedPromotions}"
										consignmentEntryQuantity="${consignmentEntryQuantity}"
										consigmentEntryTotalPrice="${consigmentEntryTotalPrice}"
									/>
								</c:forEach>
							</c:set>
						</c:when>
						<c:otherwise>
							<c:set var="displayHeading" value="${not completedCncOrderWithConsignmentPickedUpDate and not completedCncOrderReadyForPickup and not cncOrderPartialRefund}" />
							<c:if test="${not empty consignment.code and fn:length(consignment.entries) gt 0}">
								<c:if test="${displayHeading}">
									<tr class="divider">
										<td colspan="${columns}">
											<strong><spring:theme code="account.page.consignment.id" /></strong> ${consignment.code}
											<c:if test="${not empty consignment.trackingID}">
												&ndash; <a class="external" href="${consignment.trackingID}"><spring:theme code="account.page.consignment.track" /></a>
											</c:if>
											<c:if test="${not empty consignment.statusDisplay}">
												<c:choose>
													<c:when test="${not empty cncOrderStatus}">
														&ndash; <spring:theme code="text.account.order.consignment.status.${cncOrderStatus}" />
													</c:when>
													<c:when test="${consignment.statusDisplay eq 'Picked'}">
														&ndash; ${consignment.statusDisplay}
													</c:when>
													<c:when test="${consignment.statusDisplay eq 'Shipped'}">
														&ndash; ${consignment.statusDisplay}
													</c:when>
													<c:otherwise>
														&ndash; <spring:theme code="text.account.order.inProgress" />
													</c:otherwise>
												</c:choose>
											</c:if>
										</td>
									</tr>
								</c:if>
							</c:if>
							<c:forEach items="${consignment.entries}" var="entry">
								<c:set var="appliedPromotions" value="${target:getAppliedPromotionForEntry(orderData, entry.orderEntry.entryNumber)}" />
								<c:set var="consignmentEntryQuantity" value="${entry.quantity}" />
								<c:set var="consigmentEntryTotalPrice" value="${entry.totalPrice}" />
								<c:if test="${entry.shippedQuantity > 0 and (consignment.statusDisplay eq 'Picked' or consignment.statusDisplay eq 'Shipped')}">
									<c:set var="consignmentEntryQuantity" value="${entry.shippedQuantity}" />
									<c:set var="consigmentEntryTotalPrice" value="${entry.totalShippedPrice}" />
								</c:if>
								<order:orderEntryItem
									entry="${entry.orderEntry}"
									appliedPromotions="${appliedPromotions}"
									consignmentEntryQuantity="${consignmentEntryQuantity}"
									consigmentEntryTotalPrice="${consigmentEntryTotalPrice}"
								/>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<c:forEach items="${orderData.entries}" var="entry">
					<c:set var="appliedPromotions" value="${target:getAppliedPromotionForEntry(orderData, entry.entryNumber)}" />
					<order:orderEntryItem entry="${entry}" consignmentEntryQuantity="${entry.quantity}" appliedPromotions="${appliedPromotions}" />
				</c:forEach>
			</c:otherwise>
		</c:choose>
		<c:if test="${not empty sentDigitalItems}">
			<tr class="divider">
				<td colspan="${columns}">
					<strong><spring:theme code="account.page.title.digitaldelivery" /></strong>
				</td>
			</tr>
			<c:out value="${sentDigitalItems}" escapeXml="false" />
		</c:if>
		<c:if test="${fn:length(orderData.cancelledProducts) > 0}">
			<tr class="divider">
				<td colspan="${columns}">
					<spring:theme code="account.page.cancelled" />
				</td>
			</tr>
		</c:if>

		<c:forEach items="${orderData.cancelledProducts}" var="cancelledProduct">
			<order:orderEntryItem entry="${cancelledProduct}" totalsZero="true" hideDiscount="true" />
		</c:forEach>
	</tbody>
</table>

		<%-- Product level Discounts --%>

<table class="summary-table full-table">
	<tbody>
		<c:if test="${order.productDiscounts.value > 0}">
			<tr class="summary-row row-discount">
				<th colspan="3" id="st-discounts" class="title">
					<spring:theme code="account.page.totals.product.discounts" />
				</th>
				<td headers="st-discounts" class="price">
					-&nbsp;<format:price priceData="${order.productDiscounts}" />
				</td>
			</tr>
		</c:if>


		<%-- Sub Total --%>
		<tr class="summary-row row-subtotal ${rowClass}">
			<th colspan="${columns - 1}" id="st-subtotal" class="title">
				<spring:theme code="account.page.totals.subtotal" />
			</th>
			<td headers="st-subtotal" class="price">
				<format:price priceData="${order.subTotal}" />
			</td>
		</tr>
		<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />

		<%-- Delivery --%>
		<c:if test="${not empty orderData.deliveryCost}">
			<tr class="summary-row row-modifier delivery ${rowClass}">
				<th colspan="${columns - 1}" id="st-delivery" class="title">
					<spring:theme code="account.page.totals.delivery.${orderData.deliveryMode.code}" />
				</th>
				<td headers="st-delivery" class="price">
					<format:price priceData="${orderData.deliveryCost}" displayFreeForZero="TRUE" />
				</td>
			</tr>
			<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />
		</c:if>

		<%-- Discounts --%>
		<c:if test="${orderData.orderDiscounts.value > 0}">
			<spring:theme code="basket.page.totals.order.discounts${ not empty orderData.flybuysDiscountData ? '.flybuys' : '' }"
				var="discountLabel"
				arguments="${orderData.flybuysDiscountData.pointsConsumed}"
				argumentSeparator=";;;" />
			<tr class="summary-row discounts row-discount ${rowClass}">
				<th colspan="${columns - 1}" id="st-discounts" class="title">
					${discountLabel}
				</th>
				<td headers="st-discounts" class="price">
					-&nbsp;<format:price priceData="${orderData.orderDiscounts}" />
				</td>
			</tr>
			<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />
		</c:if>

		<%-- Total --%>
		<tr class="summary-row row-total">
			<th colspan="${columns - 1}" id="st-total" class="title">
				<spring:theme code="account.page.totals.total" />
			</th>
			<td headers="st-total" class="price">
				<format:price priceData="${order.totalPrice}" />
			</td>
		</tr>
		<tr class="summary-row">
			<td colspan="4">
				<c:set var="gstTax"><format:price priceData="${orderData.totalTax}" /></c:set>
				<p class="disc-table"><spring:theme code="account.page.disclaimer.tax" arguments="${gstTax}" argumentSeparator=";;;" /></p>
			</td>
		</tr>
		<c:if test="${orderData.status eq 'PARKED' and order.preOrderDepositAmount ne null and order.preOrderDepositAmount.value > 0}">
			<tr class="summary-row row-deposit ">
				<th colspan="${columns - 1}" id="st-deposit" class="title">
					<spring:theme code="account.page.preorder.deposit" />
				</th>
				<td headers="st-deposit" class="price">
					<format:price priceData="${order.preOrderDepositAmount }" />
				</td>
			</tr>
			<tr class="summary-row row-balance">
				<th colspan="${columns - 1}" id="st-balance" class="title">
					<p class="orderDetailsContent orderDetailsContent--balance"><spring:theme code="account.page.preorder.balance" /></p>
					<p class="orderDetailsContent orderDetailsContent--disclaimer"><spring:theme code="account.page.preorder.balance.disclaimer" /></p>
				</th>
				<td headers="st-balance" class="price">
					<format:price priceData="${order.preOrderOutstandingAmount}" />
				</td>
			</tr>
		</c:if>
	</tbody>
</table>

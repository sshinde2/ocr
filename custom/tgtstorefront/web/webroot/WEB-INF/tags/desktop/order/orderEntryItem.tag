<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="appliedPromotions" required="false" type="java.lang.Object" %>
<%@ attribute name="consignmentEntryQuantity" required="false" type="java.lang.Object" %>
<%@ attribute name="consigmentEntryTotalPrice" required="false" type="java.lang.Object" %>
<%@ attribute name="entry" required="true" type="de.hybris.platform.commercefacades.order.data.OrderEntryData" %>
<%@ attribute name="totalsZero" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideDiscount" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/desktop/order" %>

<c:set var="useStatusMethod" value="${not fn:contains(entry['class'], 'OrderEntryData')}" />
<c:set var="dealsHelp"><cart:dealsHelp /></c:set>

<c:set var="extras">
	<cart:cartItemEntryRecipients recipients="${entry.recipients}" disabledMessage="false" />
	<c:forEach items="${appliedPromotions}" var="promotion" >
		<p class="deals deals-applied"><spring:theme code="icon.tick" />${promotion.description}${dealsHelp}</p>
	</c:forEach>
</c:set>
<c:set var="showExtras" value="${not empty extras}" />

<tr class="product-entry ${showExtras ? 'row-combined' : ''}">
	<td headers="entry-description" class="description">
		<product:productSpecSummary product="${entry.product}" showCode="true" renderLink="true" />
	</td>
	<td headers="entry-price" class="price">
		<format:price priceData="${entry.basePrice}" displayFreeForZero="false"/>
		<c:if test="${not hideDiscount and not empty entry.discountPrice}">
			<br /><span class="deals-applied">-<format:price priceData="${entry.discountPrice}" displayFreeForZero="false"/></span>
		</c:if>
	</td>
	<td headers="entry-quantity" class="quantity">
       <c:if test="${empty consignmentEntryQuantity}">
           0
       </c:if>
       <c:if test="${consignmentEntryQuantity >= 0}">
           ${consignmentEntryQuantity}
       </c:if>		
	</td>
	<td headers="entry-total" class="total price">
		<c:if test="${not totalsZero}">
            <c:if test="${empty consignmentEntryQuantity}">
                $0.00
            </c:if>
            <c:if test="${consignmentEntryQuantity >= 0}">
                <c:if test="${empty consigmentEntryTotalPrice}">
                    <c:set var="consigmentEntryTotalPrice" value="${entry.totalPrice}" />
                </c:if>
                <format:price priceData="${consigmentEntryTotalPrice}" displayFreeForZero="false"/>
            </c:if>
		</c:if>
		<c:if test="${totalsZero}">
			$0.00
		</c:if>
	</td>
</tr>

<c:if test="${showExtras}">
	<tr class="entry-extras">
		<td colspan="4">
			${extras}
		</td>
	</tr>
</c:if>

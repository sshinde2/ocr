<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="returns" required="true" type="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:if test="${fn:length(returns) > 0}">
	<div class="scroll-for-tiny">
		<div class="scrollable-pane">
			<table class="summary-table full-table">
				<thead>
					<tr>
						<th id="returns-description" class="description"><spring:theme code="account.page.returns.description"/></th>
						<th id="returns-price" class="price"><spring:theme code="account.page.returns.price"/></th>
						<th id="returns-quantity" class="quantity"><spring:theme code="account.page.returns.quantity"/></th>
						<th id="returns-total" class="total price"><spring:theme code="account.page.returns.total"/></th>
						<th id="returns-date" class="date"><spring:theme code="account.page.returns.date"/></th>
						<th id="returns-status" class="status"><spring:theme code="account.page.returns.status"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${returns}" var="returnItem">
						<tr class="product-entry">
							<td header="returns-description">
								<product:productSpecSummary product="${returnItem.product}"  showCode="true" renderLink="true"/>
							</td>
							<td header="returns-price" class="price">
								<format:price priceData="${returnItem.basePrice}" displayFreeForZero="false"/>
							</td>
							<td header="returns-quantity">
								${returnItem.quantity}
							</td>
							<td header="returns-total" class="price">
								<format:price priceData="${returnItem.totalPrice}" displayFreeForZero="false" isNegative="true"/>
							</td>
							<td header="returns-date" class="date">
								<span class="hide-for-small"><format:formatDateAndTime date="${returnItem.date}" /></span>
								<span class="only-for-small"><format:formatShortDate date="${returnItem.date}" /></span>
							</td>
							<td header="returns-status">
								<c:if test="${not empty returnItem.replacementOrderCode}">
									<a href="${targetUrlsMapping.myAccountOrderDetails}${order.code}" title="View order ${order.code}">${returnItem.status}</a>
								</c:if>
								<c:if test="${empty returnItem.replacementOrderCode}">
									${returnItem.status}
								</c:if>
							</td>
						</tr>
						<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<p class="disc-table"><spring:theme code="account.page.disclaimer.refunds" /></p>	
</c:if>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ attribute name="heading" required="true" type="java.lang.String" %>
<%@ attribute name="addressData" required="true" type="java.lang.Object" %>
<%@ attribute name="editUrl" required="false" type="java.lang.String" %>
<%@ attribute name="customerCode" required="false" type="java.lang.String" %>
<%@ attribute name="actionCode" required="false" type="java.lang.String" %>
<%@ attribute name="actionContent" required="false" type="java.lang.String" %>
<%@ attribute name="formData" required="false" type="java.lang.Boolean" %>
<%@ attribute name="titles" required="false" type="java.util.Collection" %>
<%@ attribute name="countries" required="false" type="java.util.Collection" %>

<c:set var="customerCode" value="${not empty customerCode ? customerCode : 'checkout'}" />
<c:set var="defaultActionCode" value="order.summarycard.${customerCode}.editDetails" />
<c:set var="actionCode" value="${not empty actionCode ? actionCode : defaultActionCode}" />

<common:summaryPanel 
	headingCode="${heading}"
	actionCode="${actionCode}"
	actionUrl="${editUrl}"
	actionContent="${actionContent}">

	<c:choose>
		<c:when test="${formData}">

			<address>
				<strong>
					<c:if test="${not empty addressData.titleCode}">
						<c:forEach items="${titles}" var="title">
							<c:if test="${addressData.titleCode eq title.code}">
								${fn:escapeXml(title.name)}&nbsp;
							</c:if>
						</c:forEach>
					</c:if>
					<c:if test="${not empty addressData.firstName}">
						${fn:escapeXml(addressData.firstName)}&nbsp;</c:if>
					<c:if test="${not empty addressData.lastName}">
						${fn:escapeXml(addressData.lastName)}</c:if>
				</strong><br />
				<c:if test="${not empty addressData.line1}">
					${fn:escapeXml(addressData.line1)}<br />
				</c:if>
				<c:if test="${not empty addressData.line2}">
					${fn:escapeXml(addressData.line2)}<br />
				</c:if>
				<c:if test="${not empty addressData.townCity}">
					${fn:escapeXml(addressData.townCity)}&nbsp;</c:if>

				<c:if test="${not empty addressData.state}">
					${fn:escapeXml(addressData.state)}&nbsp;</c:if>
				<c:if test="${not empty addressData.postcode}">
					${fn:escapeXml(addressData.postcode)}</c:if><br />
				<c:if test="${not empty addressData.countryIso}">
					<c:forEach items="${countries}" var="country">
						<c:if test="${addressData.countryIso eq country.isocode}">
							${fn:escapeXml(country.name)}
						</c:if>
					</c:forEach>
				</c:if>
			</address>


		</c:when>
		<c:otherwise>

			<address>
				<strong>
					<c:if test="${not empty addressData.title}">
						${fn:escapeXml(addressData.title)}&nbsp;</c:if>
					<c:if test="${not empty addressData.firstName}">
						${fn:escapeXml(addressData.firstName)}&nbsp;</c:if>
					<c:if test="${not empty addressData.lastName}">
						${fn:escapeXml(addressData.lastName)}</c:if>
				</strong><br />
				<c:if test="${not empty addressData.line1}">
					${fn:escapeXml(addressData.line1)}<br />
				</c:if>
				<c:if test="${not empty addressData.line2}">
					${fn:escapeXml(addressData.line2)}<br />
				</c:if>
				<c:if test="${not empty addressData.town}">
					${fn:escapeXml(addressData.town)}&nbsp;</c:if>

				<c:if test="${not empty addressData.state}">
					${fn:escapeXml(addressData.state)}&nbsp;</c:if>
				<c:if test="${not empty addressData.postalCode}">
					${fn:escapeXml(addressData.postalCode)}</c:if><br />
				<c:if test="${not empty addressData.country.name}">
					${fn:escapeXml(addressData.country.name)}</c:if>
			</address>
			<c:if test="${not empty addressData.phone}">
				<p>
					<strong><spring:theme code="order.summarycard.checkout.label.phone" /></strong>&nbsp;${fn:escapeXml(addressData.phone)}
				</p>
			</c:if>
			
		</c:otherwise>
	</c:choose>
	
	
</common:summaryPanel>

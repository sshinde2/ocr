<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ attribute name="addressData" required="true" type="java.lang.Object" %>
<%@ attribute name="customerCode" required="false" type="java.lang.String" %>
<c:set var="customerCode" value="${not empty customerCode ? customerCode : 'checkout'}" />

<common:summaryPanel headingCode="order.summarycard.${customerCode}.heading.details">
	<p>
		<strong>${fn:escapeXml(addressData.title)}&nbsp;${fn:escapeXml(addressData.firstName)}&nbsp;${fn:escapeXml(addressData.lastName)}</strong><br />
		${fn:escapeXml(addressData.phone)}
	</p>
</common:summaryPanel>
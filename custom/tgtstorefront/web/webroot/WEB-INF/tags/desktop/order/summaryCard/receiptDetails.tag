<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="orderData" required="true" type="java.lang.Object" %>
<%@ attribute name="customerCode" required="false" type="java.lang.String" %>
<c:set var="customerCode" value="${not empty customerCode ? customerCode : 'checkout'}" />

<common:summaryPanel headingCode="order.summarycard.${customerCode}.heading.orderDetails">
	<dl>
		<dt><spring:theme code="order.summarycard.${customerCode}.label.orderNumber" /></dt>
		<dd>${fn:escapeXml(orderData.code)}</dd>
		<dt><spring:theme code="order.summarycard.${customerCode}.label.dateCreated" /></dt>
		<dd><format:formatDate date="${orderData.created}"/></dd>		
	</dl>
</common:summaryPanel>

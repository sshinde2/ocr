<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ attribute name="bottomActionUrl" required="false" type="java.lang.String" %>
<%@ attribute name="bottomActionCode" required="false" type="java.lang.String" %>
<%@ attribute name="paymentsInfo" required="true" type="java.lang.Object" %>

<common:summaryPanel
	headingCode="order.summarycard.myaccount.heading.remainingBalancePayment"
	bottomActionCode="${bottomActionCode}"
	bottomActionUrl="${bottomActionUrl}">
	<div class="summary-detail-info">
		<spring:theme code="icon.info"/>
		<p>
			<spring:theme code="order.summarycard.myaccount.info.remainingBalancePayment" />
		</p>
	</div>
	<c:if test="${not empty paymentsInfo}">
		<c:forEach var="payment" items="${paymentsInfo}">
			<dl>
				<dt><spring:theme code="order.summarycard.myaccount.label.paymentType" /></dt>
				<dd>${fn:escapeXml(payment.cardTypeData.name)}</dd>
				<dt><spring:theme code="order.summarycard.myaccount.label.cardNumber" /></dt>
				<dd>${fn:escapeXml(payment.cardNumber)}</dd>
				<c:if test="${not empty payment.accountHolderName}">
					<dt><spring:theme code="order.summarycard.myaccount.label.nameOnCard" /></dt>
					<dd>${fn:escapeXml(payment.accountHolderName)}</dd>
				</c:if>
				<c:if test="${not empty payment.expiryMonth and not empty payment.expiryYear}">
					<dt><spring:theme code="order.summarycard.myaccount.label.expiryDate" /></dt>
					<dd>${fn:escapeXml(payment.expiryMonth)}/${fn:escapeXml(payment.expiryYear)}</dd>
				</c:if>
			</dl>
		</c:forEach>
	</c:if>
</common:summaryPanel>
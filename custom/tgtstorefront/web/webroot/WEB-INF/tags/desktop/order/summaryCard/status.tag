<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="orderData" required="true" type="java.lang.Object" %>
<%@ attribute name="customerCode" required="false" type="java.lang.String" %>

<c:set var="customerCode" value="${not empty customerCode ? customerCode : 'checkout'}" />
<c:set var="cncOrderStatus" value="${orderData.cncCustomOrderStatus}" />
<common:summaryPanel headingCode="order.summarycard.${customerCode}.heading.orderStatus">
	<dl>
		<dt><spring:theme code="order.summarycard.${customerCode}.label.orderPlaced" /></dt>
		<dd><format:formatDateAndTime date="${orderData.created}"/></dd>
		<dt><spring:theme code="order.summarycard.${customerCode}.label.orderTotal" /></dt>
		<dd><format:price priceData="${orderData.totalPrice}"/></dd>
		<dt><spring:theme code="order.summarycard.${customerCode}.label.email" /></dt>
		<dd>${orderData.email}</dd>
		<dt><spring:theme code="order.summarycard.${customerCode}.label.status" /></dt>
		<dd>
			<c:choose>
				<c:when test="${orderData.status eq 'PARKED'}">
					<spring:theme code="text.account.myOrders.status.parked" text="Status"/>
				</c:when>
				<c:when test="${not empty cncOrderStatus}">
					<spring:theme code="order.summarycard.myaccount.label.status.${cncOrderStatus}" />
				</c:when>
				<c:otherwise>
					${fn:escapeXml(orderData.statusDisplay)}
				</c:otherwise>
			</c:choose>
		</dd>
	</dl>
</common:summaryPanel>

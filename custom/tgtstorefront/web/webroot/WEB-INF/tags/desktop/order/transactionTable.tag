<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="transactions" required="true" type="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<c:set var="oddClass" value="odd" />
<c:set var="evenClass" value="even" />
<c:set var="rowClass" value="${evenClass}" />

<c:choose>
	<c:when test="${fn:length(transactions) > 0}">
		<div class="scroll-for-tiny">
			<div class="scrollable-pane">
				<table class="summary-table full-table">
					<thead>
						<tr>
							<th id="transaction-receipt" class="receipt"><spring:theme code="account.page.transaction.receipt"/></th>
							<th id="transaction-date" class="date hide-for-small"><spring:theme code="account.page.transaction.date"/></th>
							<th id="transaction-card" class="card"><spring:theme code="account.page.transaction.card"/></th>
							<th id="transaction-total" class="total"><spring:theme code="account.page.transaction.total"/></th>
							<th id="transaction-status" class="status"><spring:theme code="account.page.transaction.status"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${transactions}" var="transaction">
							<tr class="${rowClass}">
								<td header="transaction-receipt">${transaction.receiptNumber}
									<span class="only-for-small">
										<format:formatShortDate date="${transaction.paymentDate}"/>
									</span>
								</td>
								<td header="transaction-date" class="hide-for-small"><format:formatDate date="${transaction.paymentDate}"/></td>
								<td header="transaction-card">${transaction.paymentType}</td>
								<td header="transaction-total"><format:price priceData="${transaction.total}" displayFreeForZero="false"/></td>
								<td header="transaction-status">${transaction.status}</td>
							</tr>
							<c:set var="rowClass" value="${rowClass eq evenClass ? oddClass : evenClass}" />
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<p><spring:theme code="account.page.transaction.none" text="No payments" /></p>
	</c:otherwise>
</c:choose>
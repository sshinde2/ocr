<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="colours" required="true" type="java.lang.Object" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedStock" value="${true}" />
</feature:enabled>

<c:if test="${not empty colours and fn:length(colours) > 1}">
	<div class="ProductVariants ProductVariants-colours" data-new-variant="true" data-prod-context="${product.baseProductCode}">
		<div class="tse-scrollable horizontal">
			<div class="tse-content">
				<span class="visuallyhidden"><spring:theme code="product.variants.colour" /></span>
				<ul class="ProductVariants-list ProductVariants-colourList">
					<c:forEach items="${colours}" var="colour">
						<c:if test="${not consolidatedStock}">
							<c:set var="stockClass" value="ProductVariants-link--${colour.outOfStock ? 'no' : 'in'}" />
						</c:if>
						<c:set var="colourName" value="${not empty colour.swatch ? colour.swatch :
							not empty colour.name ? colour.name : ''}" />
						<c:set var="altColourName" value="${not empty colourName ? colourName : product.name}" />
						<product:variantItem>
							<a href="${colour.url}" title="${altColourName}" data-type="colour" data-code="${colour.code}" class="ProductVariants-link ProductVariants-colourLink ProductVariants-link--large ${stockClass} ${colour.active ? ' is-selected' : ''}" >
								<img src="${colour.colourImageUrl}" class="ProductVariants-img" alt="${altColourName}" />
								<c:if test="${not empty colourName}">
									<span class="ProductVariants-colourName"><spring:theme code="product.variants.colour.select" arguments="${colourName}" /></span>
								</c:if>
							</a>
						</product:variantItem>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</c:if>

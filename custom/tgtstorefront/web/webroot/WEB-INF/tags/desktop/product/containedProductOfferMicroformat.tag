<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ tag import="au.com.target.tgtcore.product.data.ProductDisplayType" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="schemaData" required="true" type="au.com.target.tgtstorefront.controllers.pages.data.SchemaData" %>

<c:if test="${not empty schemaData}">
	<spring:eval var="PREORDER_AVAILABLE" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).PREORDER_AVAILABLE"/>
	<spring:eval var="COMING_SOON" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).COMING_SOON"/>
	<c:set var="inStockOnline" value="${schemaData.maxAvailQtyOnline gt 0}" />
	<c:set var="inStockAtStore" value="${schemaData.maxAvailQtyInStore gt 0}" />
	<c:set var="isPriceRange" value="${not empty schemaData.priceRange and schemaData.priceRange.maxPrice.value ne schemaData.priceRange.minPrice.value}" />
	<c:set var="isPreOrder" value="${schemaData.productDisplayType eq PREORDER_AVAILABLE or schemaData.productDisplayType eq COMING_SOON}" />
	<c:set var="inStockForPreorder" value="${schemaData.productDisplayType eq PREORDER_AVAILABLE}" />
	<spring:theme code="text.headerLogo" var="sellerName" />

	<c:choose>
		<c:when test="${isPreOrder}">
			<c:if test="${inStockForPreorder}">
				<c:set var="itempropAvailabilityContent" value="PreOrder" />
			</c:if>
			<c:if test="${not inStockForPreorder}">
				<c:set var="itempropAvailabilityContent" value="OutOfStock" />
			</c:if>
		</c:when>
		<c:when test="${inStockOnline and inStockAtStore}">
			<c:set var="itempropAvailabilityContent" value="InStock" />
		</c:when>
		<c:when test="${inStockOnline}">
			<feature:disabled name="featureConsolidatedStoreStockVisibility">
				<c:set var="itempropAvailabilityContent" value="InStock" />
			</feature:disabled>
			<feature:enabled name="featureConsolidatedStoreStockVisibility">
				<c:set var="itempropAvailabilityContent" value="OnlineOnly" />
			</feature:enabled>
		</c:when>
		<c:when test="${inStockAtStore}">
			<c:set var="itempropAvailabilityContent" value="InStoreOnly" />
		</c:when>
		<c:otherwise>
			<c:set var="itempropAvailabilityContent" value="OutOfStock" />
		</c:otherwise>
	</c:choose>

	<meta itemprop="name" content="${schemaData.name}" />
	<meta itemprop="productID" content="${schemaData.productCode}" />
	<div itemprop="offers" itemscope="itemscope" itemtype="${isPriceRange ? 'http://schema.org/AggregateOffer' : 'http://schema.org/Offer' }">
		<meta itemprop="category" content="${schemaData.categoryName}" />
		<meta itemprop="itemCondition" itemtype="http://schema.org/OfferItemCondition" content="http://schema.org/NewCondition" />
		<meta itemprop="priceCurrency" content="AUD" />
		<meta itemprop="seller" content="${sellerName}" />

		<c:if test="${not empty schemaData.primaryImage}">
			<meta itemprop="image" content="${target:splitEndecaUrlField(schemaData.primaryImage)}" />
		</c:if>

		<c:choose>
			<c:when test="${isPriceRange}">
				<meta itemprop="highPrice" content="${schemaData.priceRange.maxPrice.value}" />
				<meta itemprop="lowPrice" content="${schemaData.priceRange.minPrice.value}" />
			</c:when>
			<c:otherwise>
				<meta itemprop="price" content="${not empty schemaData.price ? schemaData.price.value : ''}" />
				<meta itemprop="availability" content="http://schema.org/${itempropAvailabilityContent}" />
				<c:if test="${isPreOrder}"><%--
					References:
						- http://schema.org/releaseDate
						- http://schema.org/DateTime

					--%><c:set var="availabilityDate"><fmt:formatDate value="${schemaData.normalSaleDate}" pattern="YYYY-MM-dd hh:mm:ss" /></c:set>
					<meta itemprop="availabilityStarts" content="${availabilityDate}" />
				</c:if>
			</c:otherwise>
		</c:choose>
	</div>
</c:if>

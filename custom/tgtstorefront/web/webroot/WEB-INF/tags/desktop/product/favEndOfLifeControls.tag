<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="originalCategoryCode" required="false" type="java.lang.String" %>

<a href="/c/${originalCategoryCode}" class="button-fwd FavProducts-productsLink">
	<spring:theme code="favourites.product.similar.items"/>
</a>
<button class="button-anchor FavRemoveButton button-wide FavProducts-remove">
	<spring:theme code="favourites.product.remove" /><span class="visuallyhidden">Remove</span>
</button>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="assets" tagdir="/WEB-INF/tags/desktop/assets" %>
<%@ taglib prefix="lister" tagdir="/WEB-INF/tags/desktop/product/lister" %>

<%@ attribute name="product" required="true" type="java.lang.Object" %>

<c:if test="${not empty product and not product.giftCard}">
	<lister:favHeart baseCode="${product.baseProductCode}" />
</c:if>

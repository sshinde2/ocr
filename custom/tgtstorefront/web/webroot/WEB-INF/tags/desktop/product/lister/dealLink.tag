<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="description" required="true" %>

<c:set var="helpLink"><cart:dealsHelp /></c:set>
<p class="deals deals-available ${className}"><spring:theme code="icon.dollar" />${description}${helpLink}</p>

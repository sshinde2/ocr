<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="assets" tagdir="/WEB-INF/tags/desktop/assets" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="baseCode" required="false" %>


<feature:enabled name="featureWishlistFavourites">
	<c:if test="${not kioskMode and not iosAppMode}">
		<a href="#" class="FavHeart" data-base-prod-code="${baseCode}">
			<feature:enabled name="uiStickyHeader">
				<assets:svgFavHeart heartType="miniFatHeart" useClass="FavHeart-heart" />
			</feature:enabled>
			<feature:disabled name="uiStickyHeader">
				<assets:svgFavHeart useClass="FavHeart-heart" />
			</feature:disabled>
			<span class="visuallyhidden"><spring:theme code="favourites.fav" /></span>
		</a>
	</c:if>
</feature:enabled>

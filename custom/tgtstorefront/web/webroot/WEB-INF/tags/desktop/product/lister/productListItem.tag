<%@ tag trimDirectiveWhitespaces="true" %>
<%@ attribute name="primaryImage" required="true" %>
<%@ attribute name="secondaryImage" required="true" %>
<%@ attribute name="baseCode" required="true" %>
<%@ attribute name="productCode" required="true" %>
<%@ attribute name="currentProduct" required="true" %>
<%@ attribute name="ecProduct" required="true" %>
<%@ attribute name="className" required="false" %>

<li class="product ga-ec-impression ${className}" data-primary-image="${primaryImage}" data-secondary-image="${secondaryImage}" data-base-prod-code="${baseCode}" data-product-code="${productCode}" data-current-product="${currentProduct}" data-ec-product='${ecProduct}'>
	<jsp:doBody />
</li>

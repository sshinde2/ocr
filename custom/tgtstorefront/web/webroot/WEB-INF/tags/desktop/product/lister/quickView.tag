<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="url" required="false" %>
<%@ attribute name="name" required="false" %>
<%@ attribute name="dataRel" required="false" %>
<%@ attribute name="title" required="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<a href="${url}" title="${title}" class="quick-add-text ga-ec-quick-click hide-for-small hfma" data-rel="${dataRel}" data-product-name="${name}"><%--
	--%><spring:theme code="text.quickViewWrap" /><%--
--%></a>

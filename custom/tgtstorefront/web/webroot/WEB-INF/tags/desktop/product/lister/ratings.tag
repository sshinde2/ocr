<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="rating" required="false" %>
<%@ attribute name="numberOfReviews" required="false" %>

<feature:disabled name="uiBazaarvoiceOverwrite">
	<c:set var="cssClass" value="Ratings" />
</feature:disabled>

<spring:theme code="text.ratings" arguments="${rating}" var="outOfText" />
<p class="ratings ${cssClass}">
	<%-- (0 to 5) * 20 creates a percentage out of 100 --%>
	<span class="rating-base Ratings-base"><span class="rating-stars Ratings-stars" style="width: calc(${rating}*20%)">${outOfText}</span></span>
	<span class="rating-num-reviews">(${numberOfReviews})</span>
</p>

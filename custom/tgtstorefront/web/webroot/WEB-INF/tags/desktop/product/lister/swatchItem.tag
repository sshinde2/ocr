<%@ tag trimDirectiveWhitespaces="true" %>
<%@ attribute name="className" required="false" %>
<%@ attribute name="gridImage" required="false" %>
<li class="swat-list-item ${className}" data-image-url="${gridImage}"><jsp:doBody /></li>

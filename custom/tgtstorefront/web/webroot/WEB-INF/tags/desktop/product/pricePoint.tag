<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="nudge" required="false"  type="java.lang.Boolean" %>
<%@ attribute name="newStyle" required="false"  type="java.lang.Boolean" %>
<%@ attribute name="beforePrice" required="false" fragment="true" %>
<%@ attribute name="smallCurrencySign" required="false" type="java.lang.Boolean" %>


<c:set var="classname" value="price-regular" />
<c:if test="${product.showWasPrice}">
	<c:set var="classname" value="price-reduced" />
</c:if>
<c:if test="${nudge}">
	<c:set var="classname" value="${classname} price-align" />
</c:if>
<c:if test="${not empty product.priceRange and not empty product.priceRange.minPrice and not empty product.priceRange.maxPrice}">
	<c:set var="classname" value="${classname} price-range" />
</c:if>
<c:choose>
	<c:when test="${newStyle}">
		<c:set var="classname" value="${classname} PricePoint " />
		<c:set var="wasClass" value="PricePoint-wasPrice " />
	</c:when>
	<c:otherwise>
		<c:set var="classname" value="${classname} price " />
		<c:set var="wasClass" value="was-price " />
	</c:otherwise>
</c:choose>

<jsp:invoke fragment="beforePrice" />
<span class="${classname}">
	<c:choose>
		<c:when
			test="${product['class'].simpleName eq 'TargetProductListerData' and not empty product.selectedVariantData and not empty product.selectedVariantData.price}">
			<format:priceRange price="${product.selectedVariantData.price}"
				smallCurrencySign="${smallCurrencySign}"
				newStyle="${newStyle}" />
		</c:when>
		<c:otherwise>
			<format:priceRange price="${product.price}"
				priceRange="${product.priceRange}"
				smallCurrencySign="${smallCurrencySign}"
				newStyle="${newStyle}" />
		</c:otherwise>
	</c:choose>
</span>
<c:if test="${product.showWasPrice}">
	<span class="was-price ${wasClass}">
		<c:choose>
		<c:when
			test="${product['class'].simpleName eq 'TargetProductListerData' and not empty product.selectedVariantData and not empty product.selectedVariantData.wasPrice}">
			<format:wasPriceRange price="${product.selectedVariantData.wasPrice}"  />
		</c:when>
		<c:otherwise>
			<format:wasPriceRange price="${product.wasPrice}" priceRange="${product.wasPriceRange}" />
		</c:otherwise>
	</c:choose>
	</span>
</c:if>

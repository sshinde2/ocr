<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="productExcluded" type="java.lang.Boolean" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="isPriceRange" value="${not empty product.priceRange and not empty product.priceRange.minPrice and not empty product.priceRange.maxPrice}" />
<c:set var="isOutsideRange" value="${product.price.value lt afterpayConfig.minimumAmount.value or product.price.value gt afterpayConfig.maximumAmount.value}" />

<c:set var="availableMessage">
	<p class="ProductAfterpayMessage-lead">
		<c:set var="minValue"><format:price priceData="${afterpayConfig.minimumAmount}" stripDecimalZeros="${true}" /></c:set>
		<c:set var="maxValue"><format:price priceData="${afterpayConfig.maximumAmount}" stripDecimalZeros="${true}" /></c:set>
		<spring:theme
			code="afterpay.messages.available"
			arguments="${minValue}||${maxValue}"
			argumentSeparator="||" />
	</p>
	<p class="ProductAfterpayMessage-lead">
		<spring:theme code="afterpay.logo.small" /><spring:theme code="afterpay.messages.learnmore" arguments="${afterpayModal}" />
	</p>
</c:set>
<c:set var="installmentMessage">
	<p class="ProductAfterpayMessage-lead">
		<c:set var="installmentPrice"><format:price priceData="${product.installmentPrice}" /></c:set>
		<spring:theme code="afterpay.messages.installments" arguments="${installmentPrice}" />
	</p>
	<p class="ProductAfterpayMessage-lead">
		<spring:theme code="afterpay.logo.small" /><spring:theme code="afterpay.messages.learnmore" arguments="${afterpayModal}" />
	</p>
</c:set>
<c:set var="notAvailableMessage">
	<p class="ProductAfterpayMessage-error">
		<spring:theme code="icon.exclamation" /><spring:theme code="afterpay.messages.pdp.unavailable" arguments="${afterpayModal}" />
	</p>
</c:set>

<c:if test="${not kioskMode and not product.preview}">
	<feature:enabled name="featureAfterpayEnabled">
		<div class="ProductAfterpayMessage">
			<c:choose>
				<c:when test="${productExcluded}">
					${notAvailableMessage}
				</c:when>
				<c:when test="${isPriceRange or isOutsideRange}">
					${availableMessage}
				</c:when>
				<c:otherwise>
					${installmentMessage}
				</c:otherwise>
			</c:choose>
		</div>
	</feature:enabled>
</c:if>

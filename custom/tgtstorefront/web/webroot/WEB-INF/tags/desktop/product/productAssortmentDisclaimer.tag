<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>


<c:if test="${product.assorted}">
	<c:set var="instoreLink">
		<a href="${targetUrlsMapping.storeFinder}"><spring:theme code="product.page.link.storeFinder" /></a>
	</c:set>
	<feature:enabled name="featureInStoreStockVisibility">
		<c:if test="${product.showStoreStockForProduct}">
			<c:set var="instoreLink">
				<button class="u-notButton AccordionControl u-underline AddCart-assortmentLink"
					data-for="showStoreStock-${product.baseProductCode}"
					data-open="${true}"
					data-scroll="${true}"
					>
						<spring:theme code="product.page.link.storeFinder" />
				</button>
			</c:set>
		</c:if>
	</feature:enabled>
	<feature:disabled name="uiPDPaddToCart">
		<div class="variant-changeable assortment-notice">
			<c:if test="${product.inStock}">
				<feedback:message size="small" type="warning">
					<p><spring:theme code="text.addToCart.assorted.disclaimer" arguments="${instoreLink}" /></p>
				</feedback:message>
			</c:if>
		</div>
	</feature:disabled>
	<feature:enabled name="uiPDPaddToCart">
		<c:if test="${product.assorted and product.inStock}">
			<div class="AddCart-assortmentMessage">
				<spring:theme code="text.addToCart.assorted.message" arguments="${instoreLink}" />
			</div>
		</c:if>
	</feature:enabled>
</c:if>


<%@ tag trimDirectiveWhitespaces="true" %>
<%@ attribute name="listerType" required="true" %>
<%@ attribute name="listClass" required="true" %>
<%@ attribute name="data" required="false" %>

<ul class="product-base-layout product-${listerType}-layout product-${listerType}-wide ${listClass}" ${data}>
	<jsp:doBody />
</ul>

<%@ tag import="au.com.target.tgtcore.product.data.ProductDisplayType" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="products" required="false" type="java.util.List" %>
<%@ attribute name="productReferences" required="false" type="java.util.List" %>
<%@ attribute name="width" required="false" type="java.lang.String" %>
<%@ attribute name="title" required="false" type="java.lang.String" %>
<%@ attribute name="link" required="false" type="java.lang.Object" %>
<%@ attribute name="listType" required="false" type="java.lang.String" %>
<%@ attribute name="unveil" required="false" type="java.lang.Boolean" %>


<c:set var="emptypixel"><asset:resource code="img.emptypixel" /></c:set>
<c:set var="carouselClassName" value="Slider--products" />

<c:set var="isProductReferences" value="${not empty productReferences}" />
<c:set var="isProducts" value="${not empty products}" />
<c:set var="perPage" value="6" />
<c:if test="${width eq 'wide'}">
	<c:set var="perPage" value="5" />
	<c:set var="carouselClassName" value="${carouselClassName} slider-products-slim" />
</c:if>

<c:if test="${fn:length(isProductReferences ? productReferences : products) <= perPage}">
	<c:set var="carouselClassName" value="${carouselClassName} Slider--collapse" />
</c:if>

<c:if test="${isProducts or isProductReferences}">
	<comp:carousel type="products" itemCount="${fn:length(isProductReferences ? productReferences : products)}" carouselClassName="${carouselClassName}" perPage="${perPage}" hidePause="${true}">
			<jsp:attribute name="heading">
				<c:if test="${not empty title}">
					<comp:component-heading title="${title}" compLink="${link}" />
				</c:if>
			</jsp:attribute>
		<jsp:attribute name="itemList">
			<c:forEach items="${isProductReferences ? productReferences : products}" var="_product" varStatus="status" begin="0" end="100">
				<c:choose>
					<c:when test="${isProductReferences}">
						<c:set var="product" value="${_product.target}" />
						<c:set var="productCode" value="${product.code}" />
						<c:set var="assorted" value="${product.assorted}" />
						<c:set var="isProductAvailableSoon" value="false" />
						<feature:enabled name="uiTrackDisplayOnly">
							<c:set var="displayOnly" value="${product.displayOnly}" />
						</feature:enabled>
					</c:when>
					<c:otherwise>
						<c:set var="product" value="${_product}" />
						<spring:eval var="PREORDER_AVAILABLE" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).PREORDER_AVAILABLE"/>
						<spring:eval var="COMING_SOON" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).COMING_SOON"/>
						<c:if test="${fn:length(product.targetVariantProductListerData) > 0 and not empty product.targetVariantProductListerData[0]}">
							<c:set var="firstVariant" value="${product.targetVariantProductListerData[0]}" />
						</c:if>
						<c:set var="isProductPreOrder" value="${not empty firstVariant and firstVariant.productDisplayType eq PREORDER_AVAILABLE}" />
						<c:set var="isProductComingSoon" value="${not empty firstVariant and firstVariant.productDisplayType eq COMING_SOON}" />
						<c:set var="isProductAvailableSoon" value="${isProductPreOrder or isProductComingSoon}" />
						<c:set var="productCode" value="${not empty firstVariant ? firstVariant.colourVariantCode : product.code}" />
						<c:set var="assorted" value="${not empty firstVariant ? firstVariant.assorted : product.assorted}" />
						<feature:enabled name="uiTrackDisplayOnly">
							<c:set var="displayOnly" value="${not empty firstVariant ? firstVariant.displayOnly : product.displayOnly}" />
						</feature:enabled>
					</c:otherwise>
				</c:choose>
				<c:set var="productEcommJson">
					<template:productEcommJson product="${product}" assorted="${assorted}" list="${listType}" position="${status.count}" displayOnly="${displayOnly}" />
				</c:set>
				<c:set var="dataEcProduct">data-ec-product='${productEcommJson}'</c:set>
				<c:set value="${target:abbreviateString(product.name, 34)}" var="productNameAbbreviated" />
				<comp:carouselItem
					url="${product.url}"
					parentClasses="ga-ec-impression"
					listAttributes="data-product-code='${productCode}' ${dataEcProduct}"
					linkClass="ga-ec-click">
					<jsp:attribute name="image">
						<c:set var="deferred" value="${unveil or (status.count > perPage)}" />
						<c:choose>
							<c:when test="${isProductReferences}">
								<product:productPrimaryImage product="${product}" format="store"/>
							</c:when>
							<c:otherwise>
								<c:set var="primaryImageUrl">
									<product:productListerImage product="${product}" format="list" index="0" unavailable="true" />
								</c:set>
								<c:choose>
									<c:when test="${deferred}">
										<img
											src="${emptypixel}"
											data-src="${primaryImageUrl}"
											alt="${product.name}"
											title="${product.name}"
											class="loading-image ${unveil ? 'unveil-img' : 'defer-img unveil-img'}"
										/>
									</c:when>
									<c:otherwise>
										<img src="${primaryImageUrl}" alt="${product.name}" title="${product.name}" />
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
					</jsp:attribute>
					<jsp:attribute name="details">
						<a href="${product.url}" data-product-code="${productCode}" class="ga-ec-click">
							<h3 class="title">${productNameAbbreviated}</h3>
							<p class="price-wrap hide-for-small">
								<product:pricePoint product="${product}" />
							</p>
							<c:set var="soldoutMessage">
								<p class="stock-position"><spring:theme code="product.stockposition.soldout.${product.onlineExclusive ? 'onlineonly' : 'instore'}" /></p>
							</c:set>
							<c:if test="${not isProductAvailableSoon}">
								<c:choose>
									<c:when test="${not isProductReferences}">
										<c:if test="${product.maxAvailOnlineQty eq 0 and product.maxAvailStoreQty gt 0 and not product.preview and not iosAppMode}">
											<p class="stock-position-fis"><i class="Icon Icon--store Icon--size150"></i><spring:theme code="product.stockposition.findInStore" /></p>
										</c:if>
										<c:if test="${product.maxAvailOnlineQty eq 0 and product.maxAvailStoreQty eq 0 and not product.preview}">
											${soldoutMessage}
										</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock' and not product.preview}">
											${soldoutMessage}
										</c:if>
									</c:otherwise>
								</c:choose>
							</c:if>
							<c:if test="${product.preview}">
								<div class="coming-soon-info">
									<span class="promo-status promotion-sticker coming-soon-sticker">
										<spring:theme code="comingsoon.button" />
									</span>
								</div>
							</c:if>
						</a>
					</jsp:attribute>
				</comp:carouselItem>
			</c:forEach>
		</jsp:attribute>
	</comp:carousel>
</c:if>

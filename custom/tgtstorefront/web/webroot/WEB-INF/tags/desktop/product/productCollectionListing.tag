<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="stl" tagdir="/WEB-INF/tags/desktop/stl" %>
<%@ attribute name="isFiltered" required="false" type="java.lang.Boolean" %>

<c:set var="looks" value="${collectionData.lookDetails}" />
<feature:enabled name="featureStepShopTheLook">
	<c:set var="looks" value="${shopTheLookPageData.looks}" />
</feature:enabled>
<ul class="ProductCollection-listing">
	<c:forEach items="${looks}" var="look">
		<%-- non-STEP data model attributes --%>
		<c:set var="visible" value="${look.visible}" />
		<c:set var="url" value="${look.label}" />
		<c:set var="lookName" value="${look.name}" />
		<c:set var="lookImage">
			<img src="${look.groupImgUrl}" alt="${look.name}" title="${look.name}" class="ProductListingItem-thumbImage" />
		</c:set>
		<c:set var="listingClass" value="ProductListingItem" />
		<c:set var="subText" value="" />
		<c:set var="callToAction"><spring:theme code="text.button.shopTheLook" /></c:set>

		<feature:enabled name="featureStepShopTheLook">
			<%-- STEP-specific attributes --%>
			<c:set var="url" value="${look.url}" />
			<c:set var="lookImage">
				<stl:lookImage look="${look}" size="298" className="ProductListingItem-thumbImage" deferred="${true}" unveil="${true}" />
			</c:set>
			<c:set var="visible" value="${true}" /> <%-- backend is only passing through visible looks --%>
			<c:set var="callToAction" value="${look.name}" />

			<c:if test="${isFiltered}">
				<c:set var="collectionUrl" value="${look.collectionId}" />
				<c:set var="listingData">data-collection="/${collectionUrl}"</c:set>
				<c:set var="listingClass" value="ProductListingItem ProductListingItem--hidden" />
				<c:set var="subText">
					<a class="ProductListingItem-collection" href="#/${collectionUrl}">
						${look.collectionName}
					</a>
				</c:set>
			</c:if>
		</feature:enabled>
		<c:if test="${visible}">
			<li class="${listingClass}" ${listingData}>
				<a href="${url}" title="${lookName}" class="ProductListingItem-thumb">
					<c:out value="${lookImage}" escapeXml="false" />
					<c:out value="${callToAction}" />
				</a>
				<c:if test="${not empty subText}"><c:out value="${subText}" escapeXml="false" /></c:if>
			</li>
		</c:if>
	</c:forEach>
</ul>

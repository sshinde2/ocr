<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="colours" required="true" type="java.lang.Object" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:if test="${not empty colours and fn:length(colours) > 1}">
	<div class="prod-colours prod-var-list">
		<label><spring:theme code="product.variants.${product.giftCard ? 'carddesign': 'colour'}"/></label>
		<div class="prod-var-group">
			<ul class="swat-list swat-list-colour var-change-list" data-includes-colour="true" data-prod-context="${product.baseProductCode}">
				<c:forEach items="${colours}" var="colour">
					<c:set var="colourTitle">${colour.swatch}<c:if test="${colour.outOfStock}"> : <spring:theme code="product.stockposition.soldout" /></c:if></c:set>
					<c:set var="colourFragment">
						<c:set var="activeClass" value="${colour.active ? 'swat-list-item-active ' : ''}" />
						<c:set var="outOfStockClass" value="${colour.outOfStock and not product.preview ? 'swat-list-item-oos ' : ''}" />

						<li class="swat-list-item var-change-item ${activeClass} ${outOfStockClass}">
							<a title="${colourTitle}" href="${colour.url}">
								<c:if test="${colour.outOfStock}">
									<span class="swat-item-icon-oos"><spring:theme code="icon.plain-cross"/></span>
								</c:if>
								<img class="swat-list-img" title="${colour.swatch}" alt="${colour.swatch}" src="${colour.swatchUrl}">
							</a>
						</li>
					</c:set>
					<c:if test="${colour.active}">
						<c:set var="activeColourName" value="${colour.swatch}" />
					</c:if>
					<c:choose>
						<c:when test="${colour.outOfStock}">
							<c:set var="oosColourFragments">${oosColourFragments}${colourFragment}</c:set>
						</c:when>
						<c:otherwise>
							<c:out value="${colourFragment}" escapeXml="false" />
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:out value="${oosColourFragments}" escapeXml="false" />
			</ul>

			<c:if test="${not empty activeColourName and not product.giftCard}">
				<p class="active-swatch "><spring:theme code="product.variants.selected.colour"/><span class="swatch-name variant-super-changeable">${activeColourName}</span></p>
			</c:if>
		</div>

	</div>
</c:if>

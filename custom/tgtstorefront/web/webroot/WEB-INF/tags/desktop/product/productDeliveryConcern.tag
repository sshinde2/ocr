<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>

<c:if test="${(product.productDeliveryToStoreOnly or product.bigAndBulky or product.productTypeCode eq 'giftCard') and not product.preview}">
	<div class="delivery-concerns">
		<c:choose>
			<c:when test="${product.productDeliveryToStoreOnly}">
				<p class="delivery-concern-cnconly">
					<spring:theme code="icon.exclamation" /><span class="delivery-concern"><kiosk:deliveryConcernPrefix /><spring:theme code="product.page.cncOnly" /></span>
				</p>
			</c:when>
			<c:when test="${product.bigAndBulky}">
				<p class="delivery-concern-bigbulky">
					<c:choose>
						<c:when test="${empty product.bulkyCncFreeThreshold and empty product.bulkyHomeDeliveryFee}">
							<spring:theme code="icon.exclamation" /><span class="delivery-concern"><kiosk:deliveryConcernPrefix /><spring:theme code="product.page.bigBulkyGeneric" /></span>
						</c:when>
						<c:when test="${product.allowDeliverToStore}">
							<c:choose>
								<c:when
									test="${product.bulkyCncFreeThreshold.value gt 0 and product.bulkyHomeDeliveryFee.value gt 0}">
									<spring:theme code="icon.exclamation" />
									<span class="delivery-concern"><kiosk:deliveryConcernPrefix />
										<spring:theme code="product.page.bigBulky"
											arguments="${product.bulkyCncFreeThreshold.formattedValue}" /></span>
								</c:when>
								<c:when
									test="${ product.bulkyCncFreeThreshold.value gt 0 and not (product.bulkyHomeDeliveryFee.value gt 0)}">
									<spring:theme code="icon.exclamation" />
									<span class="delivery-concern"><kiosk:deliveryConcernPrefix />
										<spring:theme code="product.page.bigBulky.freehd"
											arguments="${product.bulkyCncFreeThreshold.formattedValue}" /></span>
								</c:when>
								<c:when
									test="${product.bulkyHomeDeliveryFee.value gt 0 and not (product.bulkyCncFreeThreshold.value gt 0)}">
									<spring:theme code="icon.exclamation" />
									<span class="delivery-concern"><kiosk:deliveryConcernPrefix />
										<spring:theme code="product.page.bigBulky.freecnc" /></span>
								</c:when>
							</c:choose>
						</c:when>
						<c:when test="${product.bulkyHomeDeliveryFee.value gt 0}">
							<spring:theme code="icon.exclamation" /><span class="delivery-concern"><kiosk:deliveryConcernPrefix /><spring:theme code="product.page.bigBulky.hdOnly" /></span>
						</c:when>
					</c:choose>
				</p>
			</c:when>
			<c:when test="${product.productTypeCode eq 'giftCard' and not kioskMode}">
				<p class="delivery-concern-physicalGiftCard">
					<spring:theme code="icon.exclamation" />
					<span class="delivery-concern">
					<spring:theme code="product.giftcard.delivery.fee.message" arguments="${product.giftCardDeliveryFee}" /></span>
				</p>
			</c:when>
		</c:choose>
	</div>
</c:if>

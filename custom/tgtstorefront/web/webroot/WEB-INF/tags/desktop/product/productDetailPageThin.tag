<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>
<%@ attribute name="pageMode" required="true" type="java.lang.String" %>
<%@ attribute name="availQtyInStoreorOnline" type="java.lang.Boolean" %>
<%@ attribute name="galleryImages" type="java.util.List" %>

<feature:enabled name="productEndOfLife">
	<c:set var="productEndOfLife" value="${true}" />
</feature:enabled>

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedStoreStockVisibilityEnabled" value="${true}" />
</feature:enabled>

<%-- to be replaced to tgt:tag --%>
<cms:slot var="component" contentSlot="${slots.Promotions}">
	<c:set var="hasVisibleComponentsPromotions" value="${true}" />
</cms:slot>

<c:set var="carouselType" value="cloudzoom" />
<c:set var="reactDetailsPanel" value="${false}" />

<c:set var="stockDataAttr">
	<product:productConsolidatedStockDataAttrs
		pageMode="${pageMode}"
		availableForStore="${availableForStore}"
		multipleModes="${availableOnline and product.showStoreStockForProduct and not product.onlineExclusive}"
	/>
</c:set>


<c:set var="ecProductJson"><product:productDetailAnalytics /></c:set>

<feature:enabled name="uiReactPDPCarousel">
	<c:set var="carouselType" value="react" />

	<feature:enabled name="uiReactPDPDetailPanel">
		<c:set var="reactDetailsPanel" value="${true}" />
	</feature:enabled>
</feature:enabled>
<c:choose>
	<c:when test="${not reactDetailsPanel}">
		<c:set var="reactDetailClasses"><%--
			--%>has-detail-panel is-${pageMode}-mode ${not availQtyInStoreorOnline and consolidatedStoreStockVisibilityEnabled ? ' is-sold-out' : ''}<%--
		--%></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="reactDetailClasses"><%--
			--%>has-react-panel<%--
		--%></c:set>
	</c:otherwise>
</c:choose>

<div class="prod-detail ProductDetails ${reactDetailClasses} is-${carouselType}" ${ecProductJson} ${stockDataAttr} data-colour-variant-code="${product.colourVariantCode}" data-base-prod-code="${product.baseProductCode}" data-prod-code="${product.code}" data-prod-context="${product.baseProductCode}" data-stock-status="${not availQtyInStoreorOnline ? ' soldout' : ''}">
	<div class="main">
		<c:if test="${not reactDetailsPanel}">
			<product:productDetailsBasic product="${product}" />
		</c:if>
		<%-- The react-root will move around the page as we convert to react PDP --%>
		<feature:enabled name="uiReactPDPCarousel">
			<div class="react-root">
				<c:if test="${reactDetailsPanel}">
					<product:containedProductOfferMicroformat schemaData="${schemaData}" />
				</c:if>
				<util:reactInitialState
					productData='"${product.baseProductCode}": ${targetProductListerData}'
					applicationData='"page": {"type": "${pageType}","data":{"baseProductCode": "${product.baseProductCode}"}},"environment":${environmentData}'
					featureData="${uiFeaturesEnabled}"
					lookData="${shopTheLookData}" />
			</div>
		</feature:enabled>
		<feature:disabled name="uiReactPDPCarousel">
			<product:productDetailsImages product="${product}" galleryImages="${galleryImages}"/>
		</feature:disabled>

		<div class="prod-intro">
			<cms:slot var="comp" contentSlot="${slots.Introduction}" position="Introduction">
				<cms:component component="${comp}"/>
			</cms:slot>
		</div>
		<c:if test="${not reactDetailsPanel}">
			<product:productDetailsPanel product="${product}" hideBasic="${true}" />
		</c:if>
		<product:productPageAccordion product="${product}" type="checkbox" />
		<div class="prod-help">
			<ul class="arrow-bullet-links">
				<cms:slot var="comp" contentSlot="${slots.Links}" position="Links">
					<li><cms:component component="${comp}"/></li>
				</cms:slot>
			</ul>
		</div>
		<product:productPromoTerms product="${product}"/>
	</div>
	<div class="aside ${fn:length(galleryImages) le 1 ? 'no-thumbs' : ''}">
		<c:if test="${not mobileAppMode}">
			<product:productSharing />
		</c:if>

		<product:productRelatedLooks />

		<c:if test="${hasVisibleComponentsPromotions}">
			<div class="prod-promotions">
				<cms:slot var="comp" contentSlot="${slots.Promotions}" position="Promotions">
					<cms:component component="${comp}"/>
				</cms:slot>
			</div>
		</c:if>
	</div>
</div>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<div class="prod-basic" data-product-code="${product.code}">
	<h1 class="title variant-changeable u-alignCenter u-cocogoose" itemprop="name">${product.name}</h1>
	<c:if test="${not hideDeals}">
		<div class="prod-deals deals-center">
			<product:productPotentialDeals promotions="${product.potentialPromotions}" dealDescription="${product.dealDescription}" />
		</div>
	</c:if>
	<c:if test="${not product.preview}">
		<div id="BVRRSummaryContainer" class="bv bv-summary bv-centered" data-api="${bazaarVoiceApiUri}" data-base-prod-code="${product.baseProductCode}" data-base-url="${fullyQualifiedDomainName}${product.baseProductCanonical.url}" data-task="read">
			<product:productRatings product="${product}" />
		</div>
	</c:if>
</div>

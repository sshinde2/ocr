<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="productGroup" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>
<%@ attribute name="hideBasic" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideDeals" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideDeliveryConcern" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideDetailsAvailable" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideTabs" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideStoreDetails" required="false" type="java.lang.Boolean" %>
<%@ attribute name="pageMode" required="false" type="java.lang.String" %>

<%--

 TargetProduct
      |
      |+ (One or more)
 TargetColourVariantProduct
      |
      |* (Zero or more)
 TargetSizeVariantProduct

--%>

<%-- Required Variables --%>
<c:set var="inStock" value="${product.stock.stockLevelStatus.code ne 'outOfStock' }" />
<c:set var="inStockAtStore" value="${not empty product.consolidatedStoreStock and product.consolidatedStoreStock.stockLevelStatus.code ne 'outOfStock' }" />
<c:set var="variantSelected" value="${not empty product.sellableVariantDisplayCode}" />
<c:set var="availableInstoreOrOnline" value="${availableOnline or availableInStore}"/>

<feature:enabled name="findInStore2PDP">
	<c:set var="newPDP" value="${true}" />
</feature:enabled>

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedPDP" value="${true}" />
	<c:set var="displayOnlyPDPLink" value="${hideTabs and pageMode eq 'store'}" />
</feature:enabled>


<%-- Page --%>
<c:if test="${not hideBasic}">
	<product:productDetailsBasic product="${product}" />
</c:if>
<c:if test="${not availableInstoreOrOnline and consolidatedPDP}">
	<div class="ProductDetails-unavailable">
		<spring:theme code="product.page.message.soldout"/>
	</div>
</c:if>

<c:if test="${(not hideTabs) and availableInstoreOrOnline}">
	<product:productDetailsToggle/>
</c:if>

<c:set var="noVariants" value="${(fn:length(colours) le '1') and (fn:length(sizes) le '1')}" />
<div class="prod-variants ProductVariantDetails ${variantSelected and product.inStock ? 'is-purchasable' : ''}">
	<feature:enabled name="findInStore2PDP">
		<div class="ProductVariantsContainer ${noVariants ? 'ProductVariantsContainer--noVariants' : ''}">
			<c:choose>
				<c:when test="${displayOnlyPDPLink}">
					<p class="u-alignCenter">
						<a href="${fullyQualifiedDomainName}${product.url}" class="u-noDecoration">
							<span class="u-cocogoose u-fontSize18 u-marginLeft-20"><i class="Icon Icon--store Icon--size175"></i><spring:theme code="product.displayOnly.fragment.message" /></span>
							<br /><span class="u-fontSize14"><spring:theme code="product.displayOnly.fragment.check" /></span>
						</a>
					</p>
				</c:when>
				<c:otherwise>
					<c:if test="${consolidatedPDP}">
						<div class="ProductDetails-findInStore">
							<button class="ProductDetails-findInStoreButton ProductDetails-setPreferredStore" type="button" title="${fisText}">
								<i class="Icon Icon--store Icon--size175"></i><spring:theme code="product.fis.setPreferredStore" />
							</button>
						</div>
					</c:if>
					<util:loading color="red" inline="${true}" />
					<product:colourVariants
						colours="${colours}" />
					<product:sizeVariants
						sizes="${sizes}"
						sizeChartUrl="${not product.giftCard and product.hasSizeChart ? product.sizeChartURL : ''}"/>
					<c:if test="${consolidatedPDP}">
						<div class="consolidated-stock-container variant-changeable">
							<c:if test="${not variantSelected}">
								<product:productStockLevel
									product="${product}"
									variantSelected="${variantSelected}"
									colours="${colours}"
									sizes="${sizes}"
									messageOnly="${newPDP}"
								/>
							</c:if>
							<c:if test="${variantSelected}">
								<div class="StockContainer">

								</div>
							</c:if>
						</div>
					</c:if>
				</c:otherwise>
			</c:choose>
		</div>
	</feature:enabled>
	<feature:disabled name="findInStore2PDP">
		<product:productColoursPanel colours="${colours}" product="${product}" />
		<product:productClassificationSelectionPanel
			baseProductCode="${product.baseProductCode}"
			classifications="${sizes}"
			sizeChartUrl="${not product.giftCard and product.hasSizeChart ? product.sizeChartURL : ''}" />
	</feature:disabled>
	<product:productAddToCartPanel
		product="${product}"
		allowAddToCart="${showAddToCart}"
		inStock="${inStock}"
		inStockAtStore="${inStockAtStore}"
		variantSelected="${variantSelected}"
		productGroup="${productGroup}"
		disableKioskFeatures="${not kioskMode}"
		colours="${colours}"
		sizes="${sizes}"
		hideStock="${displayOnlyPDPLink}"
		hideStoreDetails="${hideStoreDetails}" />
	<c:if test="${consolidatedPDP}">
		<c:if test="${not hideStoreDetails}">
			<div class="StoreDetails">
			</div>
			<script>if (window.t_Inject) { t_Inject('storeDetails');}</script>
		</c:if>
		<div class="FavControls">
			<product:favouritesPDP
				 product="${product}" />
		</div>
		<c:if test="${product.excludeForAfterpay}">
			<product:productAfterpayMessage product="${product}" productExcluded="${true}" />
		</c:if>
		<c:if test="${not hideStoreDetails}">
			<div class="StoreDetails-availabilityContainer">
				<p class="StoreDetails-availabilityTime"><spring:theme code="instore.stock.store.stockavailabilitytime"/><span class="AvailabilityTime StoreDetails-availabilityTime"></span></p>
			</div>
		</c:if>
	</c:if>
	<c:if test="${not hideDeliveryConcern and not hideTabs}">
		<product:productDeliveryConcern product="${product}" />
	</c:if>
	<c:if test="${not hideDetailsAvailable and not hideTabs and not (kioskMode and product.productTypeCode eq 'giftCard')}">
		<product:productDetailsAvailable product="${product}" />
	</c:if>
</div>

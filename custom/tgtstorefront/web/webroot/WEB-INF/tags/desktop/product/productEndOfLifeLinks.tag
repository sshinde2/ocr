<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:if test="${not empty productEndOfLifeLinks}">
	<div class="ProductEndOfLife">
		<h3 class="ProductEndOfLife-linkHeading"><spring:theme code="product.endoflife.link.heading"/></h3>
		<div class="ProductEndOfLife-linkContainer">
			<c:forEach items="${productEndOfLifeLinks}" var="productEndOfLifeLinkData">
 				<a href="${productEndOfLifeLinkData.url}" class="ProductEndOfLife-link Button Button--slim Button--white" title="${productEndOfLifeLinkData.name}">
 					<span class="ProductEndOfLife-linkText">
 						<spring:theme code="product.endoflife.link.shop" arguments="${productEndOfLifeLinkData.name}"/>
 					</span>
 				</a>
 			</c:forEach>
		</div>
	</div>
</c:if>



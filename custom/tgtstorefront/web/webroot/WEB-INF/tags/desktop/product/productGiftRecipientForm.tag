<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="modify" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>

<div class="Form Form--border GiftRecipientForm">
	<c:choose>
		<c:when test="${modify}">
			<p class="Form-required Form-required--aligned"><spring:theme code="form.mandatory.message" /></p>
		</c:when>
		<c:otherwise>
			<h3 class="Form-heading"><spring:theme code="product.page.giftrecipientform.heading" /></h3>
			<p class="Form-required Form-required--aligned"><spring:theme code="form.mandatory.message" /></p>
		</c:otherwise>
	</c:choose>
	<formElement:formInputBox
		idKey="giftrecipientform.firstName"
		labelKey="giftrecipientform.firstName"
		path="giftRecipientForm.firstName"
		inputCSS="text"
		mandatory="true" />
	<formElement:formInputBox
		idKey="giftrecipientform.lastName"
		labelKey="giftrecipientform.lastName"
		path="giftRecipientForm.lastName"
		inputCSS="text"
		mandatory="true" />
	<formElement:formInputBox
		idKey="giftrecipientform.recipientEmailAddress"
		labelKey="giftrecipientform.recipientEmailAddress"
		path="giftRecipientForm.recipientEmailAddress"
		inputCSS="text"
		mandatory="true" />
	<c:set var="countdown"><span class="text-countdown-message"></span></c:set>
	<spring:theme code="giftrecipientform.messageText.maximum" var="maximumText" arguments="${countdown}" />
	<spring:theme code="giftrecipientform.messageText.remaining" var="remainingData" arguments="{{digit}}" />
	<spring:theme code="giftrecipientform.messageText.toomany" var="toomanyData" arguments="{{digit}}" />
	<c:set var="textAreaData">
		data-limit="120"
		data-remaining-text="${remainingData}"
		data-too-many-text="${toomanyData}"
	</c:set>
	<c:set var="textAreaLabel"><p class="label-note text-countdown-label">${maximumText}</p></c:set>
	<formElement:formTextArea
		idKey="giftrecipientform.messageText"
		labelKey="giftrecipientform.messageText"
		path="GiftRecipientForm.messageText"
		mandatory="false"
		elementCSS="text-countdown"
		fieldData="${textAreaData}"
		labelExtra="${textAreaLabel}" />
	<jsp:doBody />
</div>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="products" required="false" type="java.util.List" %>
<%@ attribute name="title" required="false" type="java.lang.String" %>
<%@ attribute name="unveil" required="false" type="java.lang.Boolean" %>
<%@ attribute name="deferred" required="false" type="java.lang.Boolean" %>
<%@ attribute name="width" required="false" type="java.lang.String" %>
<%@ attribute name="staticLink" required="false" type="java.lang.String" %>
<%@ attribute name="callToAction" required="false" type="java.lang.String" %>

<c:set var="emptypixel"><asset:resource code="img.emptypixel" /></c:set>

<c:set var="perPage" value="4" />
<c:set var="carouselClassName" value="Slider--groupPage" />

<c:if test="${width eq 'wide'}">
	<c:set var="perPage" value="3" />
</c:if>
<c:if test="${fn:length(products) <= perPage}">
	<c:set var="carouselClassName" value="${carouselClassName} Slider-collapse" />
</c:if>
<c:set var="carouselClassName" value="${carouselClassName} Slider--groupBy${perPage}" />

<comp:carousel type="group" itemCount="${fn:length(products)}" carouselClassName="${carouselClassName}" perPage="${perPage}" hidePause="${true}">
	<jsp:attribute name="heading">
		<comp:component-heading title="${title}" staticLink="${staticLink}" />
	</jsp:attribute>
	<jsp:attribute name="itemList">
		<c:forEach items="${products}" var="product" varStatus="status" begin="0" end="100">
			<comp:carouselItem
				url="${product.label}"
				parentClasses=""
				listAttributes=""
				linkClass="Slider-groupSubtext">
				<jsp:attribute name="image">
					<c:choose>
						<c:when test="${deferred}">
							<img src="${emptypixel}"
								data-src="${product.groupImage.url}"
								alt="${product.name}"
								title="${product.name}"
								class="loading-image ${unveil ? 'unveil-img' : 'defer-img unveil-img'}"
							/>
						</c:when>
						<c:otherwise>
							<img src="${product.groupImage.url}" alt="${product.name}" title="${product.name}" />
						</c:otherwise>
					</c:choose>
					${callToAction}
				</jsp:attribute>
			</comp:carouselItem>
		</c:forEach>
	</jsp:attribute>
</comp:carousel>

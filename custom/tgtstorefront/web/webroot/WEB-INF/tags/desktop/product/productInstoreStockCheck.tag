<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<c:set var="noStock" value="0" />

<spring:theme code="instore.stock.level.message.no" var="noStockMsg" />
<spring:theme code="instore.stock.level.message.low" var="lowStockMsg" />
<spring:theme code="instore.stock.level.message.high" var="highStockMsg" />

<%-- Low and High stock are the same as there's no medium level and low is lt and high is ge --%>
<div class="InstoreStockCheck"
	data-position-url="${product.sellableVariantDisplayCode}/store/stock"
	data-location-url="${product.sellableVariantDisplayCode}/store/stock"
	data-no-lvl="${noStock}"
	data-no-msg="${noStockMsg}"
	data-low-lvl="${limitedStock}"
	data-low-msg="${lowStockMsg}"
	data-high-lvl="${limitedStock}"
	data-high-msg="${highStockMsg}"
	data-feat-key="iss">
	<c:set var="variantSelected" value="${not empty product.sellableVariantDisplayCode}" />
	<c:choose>
		<c:when test="${not variantSelected}">
			<p class="InstoreStockCheck-message"><spring:theme code="instore.stock.variants.select.size"/></p>
		</c:when>
		<c:otherwise>
			<form class="InstoreStockCheck-form">
				<label for="control-search-location-query"><spring:theme code="instore.stock.search.label" /></label>
				<div class="InstoreStockCheck-search">
					<label class="visuallyhidden" for="control-search-location-query"><spring:theme code="instore.stock.search.label" /></label>
					<spring:theme code="instore.stock.search.placeholder" var="placeholder" />
					<input class="text InstoreStockCheck-searchInput" type="text" id="control-search-location-query" name="search" placeholder="${placeholder}" maxlength="100" />
					<button class="control-search-location-button button-fwd-alt">
						<spring:theme code="instore.stock.search.check"/>
					</button>
				</div>
				<span class="InstoreStockCheck-fieldError store-search-field-error"><spring:theme code="instore.stock.search.fieldError" /></span>
				<button class="control-search-position-button button-fwd-alt-outline button-wide" type="button"><spring:theme code="storeFinder.use.my.current.location" /></button>
			</form>
			<div class="InstoreStockCheck-stores">

			</div>
		</c:otherwise>
	</c:choose>
	<common:handlebarsTemplates template="instoreStock" />
</div>

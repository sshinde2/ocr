<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="showBasketButton" required="false" %>
<%@ attribute name="imgClass" required="false" %>
<%@ attribute name="listerName" required="false" type="java.lang.String" %>
<%@ attribute name="trackDisplayOnly" required="false" type="java.lang.Boolean" %>
<%@ attribute name="cmsComponentId" required="false" %>
<%@ attribute name="position" required="false" %>
<%@ attribute name="absoluteIndex" required="false" type="java.lang.Integer" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<product:productListerItem
	listerType="grid"
	product="${product}"
	showBasketButton="${showBasketButton}"
	cmsComponentId="${cmsComponentId}"
	listerName="${listerName}"
	position="${position}"
	absoluteIndex="${absoluteIndex}"
	trackDisplayOnly="${trackDisplayOnly}"
	/>

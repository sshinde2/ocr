<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="format" required="true" type="java.lang.String" %>
<%@ attribute name="index" required="false" %>
<%@ attribute name="unavailable" required="false" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<c:set var="isVariants" value="${fn:length(product.targetVariantProductListerData) > 0}" />
<c:if test="${empty index}">
	<c:set var="index" value="${0}" />
</c:if>

<c:set var="formatGetter">${format}ImageUrls</c:set>
<c:if test="${isVariants and fn:length(product.targetVariantProductListerData[0][formatGetter]) > index}">
	<c:set var="imageUrl" value="${product.targetVariantProductListerData[0][formatGetter][index]}"  />
</c:if>

<c:choose>
	<c:when test="${empty imageUrl and unavailable}">
		<asset:resource code="img.missingProductImage.${format}" />
	</c:when>
	<c:otherwise>
		${target:splitEndecaUrlField(imageUrl)}
	</c:otherwise>
</c:choose>

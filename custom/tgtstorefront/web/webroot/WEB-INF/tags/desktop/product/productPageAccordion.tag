<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ attribute name="showDetails" required="false" type="java.lang.Boolean" %>
<%@ attribute name="type" required="true" type="java.lang.String" %>

<c:set var="showDesc" value="${not empty product.youTubeLink or not empty product.description}" />
<c:set var="showStoreStockForProduct" value="${product.showStoreStockForProduct}" />
<c:set var="showFeature" value="${not empty product.productFeatures or not empty product.materials or not empty product.careInstructions or not empty product.extraInfo}" />
<c:set var="inStock" value="${product.stock.stockLevelStatus.code ne 'outOfStock' }" />
<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedPDP" value="${true}" />
</feature:enabled>
<c:set var="checked" value="${consolidatedPDP ? availableOnline or availableInStore : true}"/>

<c:set var="instoreStockPanel">
	<c:if test="${showStoreStockForProduct}">
		<%-- Additional class for hiding for VWO/Kiosk test --%>
		<component:accordionItem
			id="showStoreStock-${product.baseProductCode}"
			groupName="ProdDetails-${product.baseProductCode}"
			checked="${not inStock}"
			heading="product.tab.showStoreStock"
			cssClass="variant-changeable AccordionItem-instoreStock"
			bellowCss="Accordion-bellow--icon Accordion-bellow--iconPin"
			type="${type}">
			<jsp:attribute name="accordionItemContent">
				<div class="product-stock-check">
					<product:productInstoreStockCheck product="${product}" />
				</div>
			</jsp:attribute>
		</component:accordionItem>
	</c:if>
</c:set>

<component:accordion type="${type}">
	<jsp:attribute name="accordionContent">
		<feature:enabled name="featureInStoreStockVisibility">
			<c:if test="${not consolidatedPDP}">
				${instoreStockPanel}
			</c:if>
		</feature:enabled>
		<c:if test="${showDesc}">
			<component:accordionItem
				id="ProdDescription-${product.baseProductCode}"
				groupName="ProdDetails-${product.baseProductCode}"
				checked="${checked}"
				heading="product.tab.description"
				cssClass="variant-changeable"
				type="${type}">
				<jsp:attribute name="accordionItemContent">
					<div class="product-description">
						<product:productDescription product="${product}" />
					</div>
				</jsp:attribute>
			</component:accordionItem>
		</c:if>
		<c:if test="${showFeature}">
			<component:accordionItem
				id="ProdFeatures${product.baseProductCode}"
				groupName="ProdDetails-${product.baseProductCode}"
				heading="product.tab.features"
				cssClass="variant-changeable"
				checked="${not showDesc}"
				type="${type}">
				<jsp:attribute name="accordionItemContent">
					<div class="product-features">
						<product:productFeatures product="${product}" />
					</div>
				</jsp:attribute>
			</component:accordionItem>
		</c:if>
	</jsp:attribute>
</component:accordion>

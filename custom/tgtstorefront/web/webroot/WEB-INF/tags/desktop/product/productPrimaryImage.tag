<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="format" required="true" type="java.lang.String" %>
<%@ attribute name="imgClass" required="false" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<c:set value="${ycommerce:productImage(product, format)}" var="primaryImage"/>

<c:if test="${format eq 'zoom'}">
	<c:set var="hero" value="zoom" />
	<c:set value="${ycommerce:productImage(product, hero)}" var="zoomImage"/>
	<c:set var="data" value="zoomImage: '${zoomImage.url}'" />
	<c:set var="imgAttrs">data-cloudzoom="${data}"</c:set>
</c:if>

<c:if test="${fn:length(imgClass) gt 0}">
	<c:set var="imgClass">class="${imgClass}"</c:set>
</c:if>

<c:choose>
	<c:when test="${not empty primaryImage}">
		<c:choose>
			<c:when test="${not empty primaryImage.altText}">
				<img itemprop="image" src="${primaryImage.url}" alt="${primaryImage.altText}" title="${primaryImage.altText}" ${imgAttrs} ${imgClass} />
			</c:when>
			<c:otherwise>
				<img itemprop="image" src="${primaryImage.url}" alt="${product.name}" title="${product.name}" ${imgAttrs} ${imgClass} />
			</c:otherwise>
		</c:choose>
	</c:when>
	<c:otherwise>
		<c:set var="missingImage"><asset:resource code="img.missingProductImage.${format}" /></c:set>
		<img src="${missingImage}" alt="${product.name}" title="${product.name}" imgClass="${imgClass}" />
	</c:otherwise>
</c:choose>

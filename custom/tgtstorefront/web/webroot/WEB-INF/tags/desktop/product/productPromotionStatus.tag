<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ tag import="au.com.target.tgtcore.product.data.ProductDisplayType" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ attribute name="isFlexible" required="false" type="java.lang.Boolean" %>
<%@ attribute name="hideNew" required="false" type="java.lang.Boolean" %>
<%@ attribute name="listingPage" required="false" type="java.lang.Boolean" %>


<c:if test="${listingPage}">
	<c:if test="${fn:length(product.targetVariantProductListerData) > 0 and not empty product.targetVariantProductListerData[0]}">
		<c:set var="firstVariant" value="${product.targetVariantProductListerData[0]}" />
		<spring:eval var="PREORDER_AVAILABLE" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).PREORDER_AVAILABLE"/>
		<spring:eval var="COMING_SOON" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).COMING_SOON"/>
	</c:if>
</c:if>

<c:choose>
	<c:when test="${not empty firstVariant and firstVariant.productDisplayType eq PREORDER_AVAILABLE}">
		<span class="promo-status promotion-sticker preorder-sticker-tag">
			<spring:theme code="product.details.page.preorder.sticker" />
		</span>
	</c:when>
	<c:when test="${not empty firstVariant and firstVariant.productDisplayType eq COMING_SOON}">
		<span class="promo-status promotion-sticker comingsoon-sticker-tag comingsoon-sticker-tag--tinyText">
			<spring:theme code="product.details.page.comingsoon.sticker" />
		</span>
	</c:when>
	<c:when test="${not empty product.productPromotionalDeliverySticker}">
		<span class="promo-status promotion-sticker delivery-promotion-sticker ${isFlexible ? ' promo-status-position-flex ' : ''}">${product.productPromotionalDeliverySticker}</span>
	</c:when>
	<c:when test="${product.newArrived and not hideNew}">
		<span class="promo-status promotion-sticker new-sticker-tag">
			<spring:theme code="product.details.page.new.sticker" />
		</span>
	</c:when>
	<c:otherwise>
		<c:forEach items="${product.promotionStatuses}" var="promotionStatus" begin="0" end="0">
			<span class="promo-status promo-status-image ${promotionStatus.promotionClass} ${isFlexible ? ' promo-status-position-flex ': ''} ">
				<span class="visuallyhidden">
					<spring:theme code="text.promostatus.${promotionStatus.promotionClass}"/>
				</span>
			</span>
		</c:forEach>
	</c:otherwise>
</c:choose>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lister" tagdir="/WEB-INF/tags/desktop/product/lister" %>

<c:if test="${product.numberOfReviews > 0 and product.averageRating > 0}">
	<fmt:formatNumber type="number" maxFractionDigits="2" value="${product.averageRating}" var="averageRating" />
	<lister:ratings rating="${averageRating}" numberOfReviews="${product.numberOfReviews}" />
</c:if>

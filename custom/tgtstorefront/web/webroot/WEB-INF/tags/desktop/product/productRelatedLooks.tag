<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="stl" tagdir="/WEB-INF/tags/desktop/stl" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="lookProductCount" value="${fn:length(lookDetailsData)}" />

<c:if test="${lookProductCount > 0}">
	<c:set var="singleTile" value="${true}" />
	<c:set var="relatedLooksSmallClass" value="" />

	<c:if test="${lookProductCount > 1}">
		<c:set var="singleTile" value="${false}" />
		<c:set var="relatedLooksSmallClass" value="RelatedLooks--small" />
	</c:if>

	<div class="RelatedLooks ${relatedLooksSmallClass}">
		<h2 class="RelatedLooks-heading"><spring:theme code="product.group.completeTheLookHeading" /></h2>
		<c:forEach var="lookProduct" items="${lookDetailsData}">
			<c:set var="lookLink">
				<feature:enabled name="featureStepShopTheLook">
					${lookProduct.url}
				</feature:enabled>
				<feature:disabled name="featureStepShopTheLook">
					${lookProduct.label}
				</feature:disabled>
			</c:set>
			<c:set var="image">
				<feature:enabled name="featureStepShopTheLook">
					<stl:lookImage look="${lookProduct}" size="330" className="RelatedLooks-img" />
				</feature:enabled>
				<feature:disabled name="featureStepShopTheLook">
					<img class="RelatedLooks-img" src="${lookProduct.groupImgUrl}" alt="${lookProduct.name}" title="${lookProduct.name}" />
				</feature:disabled>
			</c:set>
			<c:if test="${not singleTile}">
				<%-- Use smaller image if available --%>
					<feature:enabled name="featureStepShopTheLook">
						<c:set var="image">
							<stl:lookImage look="${lookProduct}" size="155" className="RelatedLooks-img" />
						</c:set>
					</feature:enabled>
					<feature:disabled name="featureStepShopTheLook">
						<c:if test="${not empty lookProduct.groupThumbUrl}">
							<c:set var="image">
								<img class="RelatedLooks-img" src="${lookProduct.groupThumbUrl}" alt="${lookProduct.name}" title="${lookProduct.name}" />
							</c:set>
						</c:if>
					</feature:disabled>
			</c:if>
			<a class="RelatedLooks-link" href="${lookLink}">
				<c:out value="${image}" escapeXml="${false}" />
				<spring:theme code="icon.right-arrow-large-nospace" />
			</a>
		</c:forEach>
	</div>
</c:if>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:if test="${cmsPage.showSocialLinks and not kioskMode}">

	<div class="product-sharing">
		<h5 class="prod-heading"><spring:theme code="product.sharing.heading" /></h5>
		<a class="addthis_button_pinterest_pinit"></a>
		<a class="addthis_button_tweet" tw:count="none"></a>
		<a class="addthis_button_google_plusone" g:plusone:size="medium" g:plusone:annotation="none"></a>
		<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
	</div>

	<script type="text/javascript">
	var addthis_config = { 'pubid':'${addThisPubId}','ui_use_css':true,'data_track_clickback': true, 'data_ga_tracker':'${googleAnalyticsTrackingId}', 'url':'${product.canonical.url}' };

	(function(w, d, m) {
		function atfn() {
			var at = document.createElement('script'); at.type = 'text/javascript'; at.async = true;
			at.src = 'https://s7.addthis.com/js/300/addthis_widget.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(at, s);
		}
		function fn(){ setTimeout(atfn, 100); }
		if (m && m.afterPageLoad) {
			fn();
		} else if (w.addEventListener) {
			w.addEventListener('load',fn,false);
		} else if (w.attachEvent) {
			w.attachEvent("onload",fn);
		} else {
			fn();
		}
	}(window, document, Modernizr));
	</script>

</c:if>

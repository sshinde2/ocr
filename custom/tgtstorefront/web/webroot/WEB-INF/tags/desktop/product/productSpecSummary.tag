<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ tag import="au.com.target.tgtcore.product.data.ProductDisplayType" %>
<%@ attribute name="product" required="true" type="java.lang.Object" %>
<%@ attribute name="quantity" required="false" %>
<%@ attribute name="showCode" required="false" type="java.lang.Boolean" %>
<%@ attribute name="renderLink" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showGiftCardSpec" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<spring:eval var="PREORDER_AVAILABLE" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).PREORDER_AVAILABLE"/>
<spring:eval var="COMING_SOON" expression="T(au.com.target.tgtcore.product.data.ProductDisplayType).COMING_SOON"/>

<c:url value="${product.url}" var="entryProductUrl"/>
<div class="variant-specs">
	<p class="variant-name">
		<c:choose>
			<c:when test="${renderLink}">
				<a href="${entryProductUrl}" title="${product.name}">${product.name}</a>
			</c:when>
			<c:otherwise>
				${product.name}
			</c:otherwise>
		</c:choose>
	</p>
	<c:if test="${empty showCode or showCode}">
		<p class="variant-spec spec-code">
			<span class="variant-spec-label"><spring:theme code="popup.cart.code"/></span> ${product.code}
		</p>
	</c:if>
	<c:if test="${not empty quantity}">
		<p class="variant-spec spec-quantity">
			<span class="variant-spec-label"><spring:theme code="popup.cart.quantity"/></span> ${quantity}
		</p>
	</c:if>
	<c:forEach items="${product.baseOptions}" var="baseOptions">
		<c:forEach items="${baseOptions.selected.variantOptionQualifiers}" var="baseOptionQualifier">
			<c:if test="${baseOptionQualifier.qualifier eq 'swatch' and not empty baseOptionQualifier.value}">
				<p class="variant-spec">
					<span class="variant-spec-label"><spring:theme code="product.variants.${product.giftCard ? 'carddesign' : 'colour'}"/></span> ${baseOptionQualifier.value}
				</p>
			</c:if>
			<c:if test="${baseOptionQualifier.qualifier eq 'size' and not empty baseOptionQualifier.value}">
				<p class="variant-spec">
					<span class="variant-spec-label"><spring:theme code="product.variants.${product.giftCard ? 'cardvalue' : 'size'}"/></span> ${baseOptionQualifier.value}
				</p>
			</c:if>
		</c:forEach>
	</c:forEach>
	<c:if test="${product.assorted}">
		<p class="variant-spec variant-spec-warning">
			<spring:theme code="product.variants.assortments.disclaimer"/>
		</p>
	</c:if>
	<c:if test="${showGiftCardSpec and product.productTypeCode eq 'giftCard'}">
		<p class="variant-spec variant-spec-alert">
				<spring:theme code="icon.exclamation" />
				<spring:theme code="product.giftcard.delivery.fee.message" arguments="${product.giftCardDeliveryFee}" />
		</p>
	</c:if>
	<c:if test="${product.productDisplayType eq PREORDER_AVAILABLE or product.productDisplayType eq COMING_SOON}">
		<p class="variant-spec variant-spec-preorder">
			<a class="lightbox" href="/modal/preorder-deposit"><strong><spring:theme code="account.page.preorder"/></strong></a>
			<spring:theme code="account.page.preorder.availableDate" arguments="${(empty product.normalSaleStartDate) ? 'soon' : product.normalSaleStartDate}"/>
		</p>
	</c:if>
</div>

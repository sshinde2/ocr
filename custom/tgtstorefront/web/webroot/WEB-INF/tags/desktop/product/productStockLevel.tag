<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="colours" required="false" type="java.lang.Object" %>
<%@ attribute name="sizes" required="false" type="java.lang.Object" %>
<%@ attribute name="variantSelected" required="true" type="java.lang.Boolean" %>
<%@ attribute name="compact" required="false" type="java.lang.Boolean" %>
<%@ attribute name="messageOnly" required="false" type="java.lang.Boolean" %>
<%@ attribute name="changeable" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<c:set var="noQuantity" value="${true}" />
<c:set var="selectMessage"><spring:theme code="product.variants.select.size"/></c:set>
<c:if test="${(not empty colours and fn:length(colours) gt 1) and ((not empty sizes and fn:length(sizes) gt 1))}">
	<c:set var="selectMessage"><spring:theme code="product.variants.select.sizeandcolor"/></c:set>
</c:if>

<c:if test="${variantSelected}">
	<c:set var="availableQuantity" value="${product.stock.stockLevel}" />
	<c:set var="noQuantity" value="${false}" />
	<c:set var="forceIn" value="${product.inStock}" />
</c:if>

<product:productStockLevelIndicator
	availableQuantity="${availableQuantity}"
	selectMessage="${selectMessage}"
	forceIn="${forceIn}"
	noQuantity="${noQuantity}"
	messageOnly="${messageOnly}"
	noVariantSelected="${not variantSelected}"
/>

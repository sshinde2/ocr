<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="availableQuantity" required="false" type="java.lang.Integer" %>
<%@ attribute name="forceIn" required="false" type="java.lang.Boolean" %>
<%@ attribute name="selectMessage" required="false" type="java.lang.String" %>
<%@ attribute name="stockLevel" required="false" type="java.lang.String" %>
<%@ attribute name="stockIndicator" required="false" type="java.lang.String" %>
<%@ attribute name="messageClass" required="false" type="java.lang.String" %>
<%@ attribute name="compact" required="false" type="java.lang.Boolean" %>
<%@ attribute name="row" required="false" type="java.lang.Boolean" %>
<%@ attribute name="noQuantity" required="false" type="java.lang.Boolean" %>
<%@ attribute name="messageOnly" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showFis" required="false" type="java.lang.Boolean" %>
<%@ attribute name="changeable" required="false" type="java.lang.Boolean" %>
<%@ attribute name="additionalMessage" required="false" fragment="true" %>
<%@ attribute name="productUrl" required="false" type="java.lang.String" %>
<%@ attribute name="wrapMessage" required="false" type="java.lang.String" %>
<%@ attribute name="noVariantSelected" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>

<c:set var="stockText">${selectMessage}</c:set>

<c:if test="${not empty availableQuantity and not noQuantity}">
	<c:set var="highStockLevel"><spring:theme code="product.variants.level.highstock"/></c:set>
	<c:set var="medStockLevel"><spring:theme code="product.variants.level.medstock"/></c:set>
	<c:set var="lowStockLevel"><spring:theme code="product.variants.level.lowstock"/></c:set>

	<c:if test="${availableQuantity ge highStockLevel}">
		<c:set var="stockLevel" value="high" />
	</c:if>
	<c:if test="${availableQuantity le medStockLevel}">
		<c:set var="stockLevel" value="med" />
	</c:if>
	<c:if test="${availableQuantity le lowStockLevel}">
		<c:set var="stockLevel" value="low" />
	</c:if>
	<c:if test="${availableQuantity le 0}">
		<c:set var="stockLevel" value="no" />
		<c:if test="${forceIn}">
			<c:set var="stockLevel" value="high" />
		</c:if>
	</c:if>

	<c:set var="stockText"><spring:theme code="product.variants.in.${messageOnly ? 'new.' : ''}${stockLevel}stock"/></c:set>
	<c:set var="productStockLevel" value="${product.stock.stockLevel}"/>
	<c:set var="stockClass">StockInfo-level ${changeable ? 'variant-changeable' : ''}</c:set>
</c:if>

<c:set var="stockIndicator">StockInfo-indicator--${stockLevel}</c:set>
<c:if test="${messageOnly}">
	<c:set var="messageClass">StockInfo-message--${stockLevel}</c:set>
</c:if>
<c:if test="${noVariantSelected}">
	<c:set var="messageClass">${messageClass} StockInfo-message--novariant</c:set>
</c:if>

<c:if test="${not empty productStockLevel}">
	<c:set var="stockClass">StockInfo-level ${changeable ? 'variant-changeable' : ''}</c:set>
</c:if>

<c:set var="wrapper" value="${fn:split(wrapMessage, '||')}" />
<div ${itempropAvailability} class="StockInfo ${stockClass} ${compact ? ' StockInfo--compact' : ''} ${row ? ' StockInfo--row' : ''} ${messageOnly ? ' StockInfo--messageOnly' : ''}">
	<c:choose>
		<c:when test="${showFis}">
			<a href="${productUrl}#store" class="FavStore">
				<i class="Icon Icon--store Icon--size125"></i>
				<span>
					<spring:theme code="product.tab.availableInStore" />
				</span>
			</a>
		</c:when>
		<c:otherwise>
			<span class="StockInfo-indicator ${stockIndicator}"></span>
			<c:out value="${wrapper[0]}" />
			<p class="StockInfo-message ${messageClass}">${stockText}</p>
			<c:out value="${wrapper[1]}" />
			<jsp:invoke fragment="additionalMessage" />
		</c:otherwise>
	</c:choose>
</div>

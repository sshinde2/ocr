<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="variants" required="true" type="java.util.List"%>
<%@ attribute name="availabilityTime" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ attribute name="availableForStore" required="false" type="java.lang.Boolean" %>

<json:object>
	<json:property name="success" value="${not empty variants}" />
	<c:if test="${not empty variants}">
		<json:object name="stockData">
			<c:forEach items="${variants}" var="variant">
				<json:object name="${variant.code}">
					<json:property name="parent" value="${variant.baseProduct}" />
					<c:choose>
						<c:when test="${variant.stock.stockLevelStatus.code eq 'inStock'}">
							<json:property name="stock" value="in" />
						</c:when>
						<c:when test="${variant.stock.stockLevelStatus.code eq 'lowStock'}">
							<json:property name="stock" value="low" />
						</c:when>
						<c:when test="${not empty variant.consolidatedStoreStock and variant.consolidatedStoreStock.stockLevelStatus.code ne 'outOfStock'}">
							<json:property name="stock" value="network" />
						</c:when>
						<c:when test="${not empty variant.consolidatedStoreStock and variant.consolidatedStoreStock.stockLevelStatus.code eq 'outOfStock' and availableForStore and not variant.displayOnly}">
							<json:property name="stock" value="unavailable" />
						</c:when>
						<c:when test="${empty variant.consolidatedStoreStock and availableForStore and not variant.displayOnly}">
							<json:property name="stock" value="unavailable" />
						</c:when>
						<c:otherwise>
							<json:property name="stock" value="no" />
						</c:otherwise>
					</c:choose>
				</json:object>
			</c:forEach>
			<json:property name="availabilityTime" value="${availabilityTime}" />
		</json:object>
	</c:if>
</json:object>

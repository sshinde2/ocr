<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:theme code="product.variants.in.new.highstock" var="highStock" />
<spring:theme code="product.variants.in.new.lowstock" var="lowStock" />
<spring:theme code="product.variants.in.new.nostock" var="noStock" />
<spring:theme code="product.variants.in.new.unavailablestock" var="unavailable" />

<json:object>
	<json:property name="no" value="${noStock}" />
	<json:property name="network" value="${noStock}" />
	<json:property name="online" value="${noStock}" />
	<json:property name="low" value="${lowStock}" />
	<json:property name="in" value="${highStock}" />
	<json:property name="unavailable" value="${unavailable}" />
</json:object>

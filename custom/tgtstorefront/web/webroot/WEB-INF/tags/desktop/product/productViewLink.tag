<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="prod-link">
	<ul class="arrow-bullet-links">
		<c:url value="${product.url}" var="productUrl">
			<%-- kiosk --%>
			<c:if test="${not empty product.bulkyBoard and not empty product.bulkyBoard.posNumber}">
				<c:param name="bbsn" value="${product.bulkyBoard.posNumber}" />
			</c:if>
		</c:url>		
		<li>
			<a href="${productUrl}"><spring:theme code="product.page.quickView.call" arguments="${product.name}" argumentSeparator=";;;" /></a>
		</li>
	<ul>
</div>
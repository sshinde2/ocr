<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav"%>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components"%>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ attribute name="recommendations" required="false" type="java.util.List"%>
<%@ attribute name="listType" required="false" type="java.lang.String"%>
<%@ attribute name="unveil" required="false" type="java.lang.Boolean"%>
<%@ attribute name="title" required="false" type="java.lang.String"%>
<%@ attribute name="width" required="false" type="java.lang.String"%>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target"%>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<c:set var="emptypixel"><asset:resource code="img.emptypixel" /></c:set>
<c:set var="placeholder"><asset:resource code="img.missingProductImage.thumbnail" /></c:set>
<c:set var="carouselClassName" value="Slider--products" />

<c:set var="perPage" value="6" />
<c:if test="${width eq 'wide'}">
	<c:set var="perPage" value="5" />
	<c:set var="carouselClassName" value="${carouselClassName} slider-products-slim" />
</c:if>
<c:if test="${fn:length(recommendations) <= perPage}">
	<c:set var="carouselClassName" value="${carouselClassName} slider-collapse" />
</c:if>

<c:if test="${not empty recommendations}">
	<comp:carousel type="products" itemCount="${fn:length(recommendations)}" perPage="${perPage}" carouselClassName="${carouselClassName}" hidePause="${true}">
		<jsp:attribute name="heading">
			<div class="product-carousel-heading">
			<c:if test="${not empty title}">
				<comp:component-heading title="${title}" />
			</c:if>
		</div>
		</jsp:attribute>
		<jsp:attribute name="itemList">
			<c:forEach items="${recommendations}" var="recommendation" varStatus="status" begin="0" end="100">
				<c:set var="deferred" value="${unveil or (status.count > perPage)}" />
				<c:set var="productEcommJson"><template:productEcommAttributesJson
						id="${recommendation.attributes.productCode}"
						name="${recommendation.attributes.productName}"
						price="${recommendation.attributes.min_price.value}"
						brand="${recommendation.records[0].attributes.brand}"
						category="${recommendation.records[0].attributes.topLevelCategoryName}"
						list="${listType}"
						position="${status.count}"
						assorted="${recommendation.attributes.assorted}"/></c:set>
				<c:set var="dataEcProduct">data-ec-product='${productEcommJson}'</c:set>
				<c:set var="productUrl" value="${recommendation.detailsAction.recordState}" />
				<comp:carouselItem
					url="${productUrl}"
					parentClasses="ga-ec-impression"
					listAttributes="data-product-code='${recommendation.attributes.colourVariantCode}' ${dataEcProduct}"
					linkClass="ga-ec-click">
					<jsp:attribute name="image">
						<c:if test="${ not empty recommendation.detailsAction.recordState}">
							<c:set var="productUrl" value="${recommendation.detailsAction.recordState}" />
						</c:if>
						<c:if test="${ not empty recommendation.attributes.imgList}">
							<c:set var="imageUrl" value="${target:splitEndecaUrlField(recommendation.attributes.imgList)}" />
						</c:if>
						<c:if test="${empty recommendation.attributes.imgList}">
							<c:set var="imageUrl" value="${placeholder}" />
						</c:if>
						<c:choose>
							<c:when test="${deferred}">
								<img src="${emptypixel}" data-src="${imageUrl}"
									alt="${recommendation.attributes.productName}"
									title="${recommendation.attributes.productName}"
									class="${unveil ? 'unveil-img' : 'defer-img unveil-img'}" />
							</c:when>
							<c:otherwise>
								<img src="${imageUrl}"
									alt="${recommendation.attributes.productName}"
									title="${recommendation.attributes.productName}" />
							</c:otherwise>
						</c:choose>
					</jsp:attribute>
					<jsp:attribute name="details">
						<a href="#" class="ga-ec-click">
							<h3 class="title">${recommendation.attributes.productName}</h3>
							<p class="price-wrap hide-for-small">
								<span class="price price-regular">
									<format:priceRange minPrice="${recommendation.attributes.min_price}"
										maxPrice="${recommendation.attributes.max_price}"
										priceKey="from" />
								</span>
								<c:if test="${not empty recommendation.attributes.min_wasPrice}">
									<span class="was-price">
										<format:priceRange minPrice="${recommendation.attributes.min_wasPrice}"
											maxPrice="${recommendation.attributes.max_wasPrice}"
											priceKey="was" />
									</span>
								</c:if>
							</p>
							<c:if test="${recommendation.attributes.inStockFlag eq '0'}">
								<c:if test="${fn:length(recommendation.records) > 0 and not empty recommendation.records[0]}">
									<c:set var="firstVariant" value="${recommendation.records[0]}" />
								</c:if>
								<c:set var="isProductPreOrder" value="${not empty firstVariant and firstVariant.attributes.productDisplayType eq 'PREORDER_AVAILABLE'}" />
								<c:set var="isProductComingSoon" value="${not empty firstVariant and firstVariant.attributes.productDisplayType eq 'COMING_SOON'}" />
								<c:set var="isProductAvailableSoon" value="${isProductPreOrder or isProductComingSoon}" />
								<c:if test="${not isProductAvailableSoon}">
									<p class="stock-position"><spring:theme code="product.stockposition.soldout" /></p>
								</c:if>
							</c:if>
						</a>
					</jsp:attribute>
				</comp:carouselItem>
			</c:forEach>
		</jsp:attribute>
	</comp:carousel>
</c:if>

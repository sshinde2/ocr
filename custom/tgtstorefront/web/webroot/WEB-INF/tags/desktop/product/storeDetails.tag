<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="storeAddress" required="false" type="java.lang.String" %>
<%@ attribute name="postalCode" required="false" type="java.lang.String" %>
<%@ attribute name="town" required="false" type="java.lang.String" %>
<%@ attribute name="state" required="false" type="java.lang.String" %>
<%@ attribute name="phone" required="false" type="java.lang.String" %>
<%@ attribute name="directionUrl" required="false" type="java.lang.String" %>
<%@ attribute name="storeNumber" required="false" type="java.lang.String" %>
<%@ attribute name="storeName" required="false" type="java.lang.String" %>

<div class="StoreDetails-title">
	<p class="StoreDetails-heading">${storeName}</p>
	<p class="StoreDetails-address">${storeAddress}</p>
	<p class="StoreDetails-address"> ${town}, ${state},${postalCode}</p>
</div>
<div class="StoreDetails-container">
	<a href="tel:${phone}" class="StoreDetails-link StoreDetails-phone" data-location="${storeName}">
		<i class="Icon Icon--storePhone Icon--size200 StoreDetails-icon"></i>
		<span class="StoreDetails-linkText">
			${phone}
		</span>
	</a>
	<a href="${directionUrl}" class="StoreDetails-link external StoreDetails-location" data-location="${storeName}">
		<i class="Icon Icon--storeLocation Icon--size200 StoreDetails-icon"></i>
		<span class="StoreDetails-linkText">
			<spring:theme code="instore.stock.store.directions" />
		</span>
	</a>
	<a href="/store/${storeNumber}/hours" class=" lightbox StoreDetails-link StoreDetails-hours" data-lightbox-type="storeHours" data-location="${storeName}">
		<i class="Icon Icon--storeHours Icon--size200 StoreDetails-icon"></i>
		<span class="StoreDetails-linkText">
			<spring:theme code="instore.stock.store.hours" />
		</span>
	</a>
</div>
<a href="#" class="StoreDetails-changeStore">
	<spring:theme code="instore.stock.store.changeStore" />
</a>

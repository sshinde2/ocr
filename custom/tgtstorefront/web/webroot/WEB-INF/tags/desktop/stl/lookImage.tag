<%@ tag body-content="empty" trimDirectiveWhitespaces="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ attribute name="look" required="true" type="au.com.target.tgtfacades.looks.data.LookDetailsData" %>
<%@ attribute name="format" required="false" type="java.lang.String" %>
<%@ attribute name="size" required="false" type="java.lang.Integer" %>
<%@ attribute name="attributes" required="false" type="java.lang.String" %>
<%@ attribute name="className" required="false" type="java.lang.String" %>
<%@ attribute name="deferred" required="false" type="java.lang.Boolean" %>
<%@ attribute name="unveil" required="false" type="java.lang.Boolean" %>


<c:set var="emptypixel"><asset:resource code="img.emptypixel" /></c:set>

<c:if test="${empty format}">
	<c:set var="format">
		<c:choose>
			<c:when test="${size le 20}">swatch</c:when>
			<c:when test="${size le 58}">thumb</c:when>
			<c:when test="${size le 138}">list</c:when>
			<c:when test="${size le 218}">grid</c:when>
			<c:when test="${size le 298}">hero</c:when>
			<c:otherwise>large</c:otherwise>
		</c:choose>
	</c:set>
</c:if>
<c:set var="formatGetter">${format}ImageUrl</c:set>
<c:set var="imageUrl">
	<c:choose>
		<c:when test="${empty look.images[formatGetter]}">
			<asset:resource code="img.missingProductImage.${format}" />
		</c:when>
		<c:otherwise>
			${look.images[formatGetter]}
		</c:otherwise>
	</c:choose>
</c:set>

<c:set var="commonAttributes" value="alt='${look.name}' title='${look.name}' ${attributes}" />
<c:choose>
	<c:when test="${deferred}">
		<img src="${emptypixel}" data-src="${imageUrl}"	class="loading-image ${unveil ? 'unveil-img' : 'defer-img unveil-img'} ${className}" ${commonAttributes} />
	</c:when>
	<c:otherwise>
		<img src="${imageUrl}" class="${className}" ${commonAttributes} />
	</c:otherwise>
</c:choose>

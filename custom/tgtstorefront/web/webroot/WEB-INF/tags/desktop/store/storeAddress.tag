<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="store" required="true" type="java.lang.Object" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store"%>

<c:if test="${not empty store.targetAddressData}">
	<h4 class="store-heading" itemprop="name">${fn:escapeXml(store.type)} &ndash; ${fn:escapeXml(store.name)}</h4>
	<address itemprop="address" itemscope="itemscope" itemtype="http://schema.org/PostalAddress">
		<store:storeAddressData addressData="${store.targetAddressData}" />
	</address>
	<p class="phone">
		<strong><spring:theme code="storeFinder.details.phone" /></strong> <span itemprop="telephone">${fn:escapeXml(store.targetAddressData.phone)}</span>
	</p>
</c:if>
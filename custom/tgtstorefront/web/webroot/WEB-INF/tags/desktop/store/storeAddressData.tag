<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="addressData" required="true" type="au.com.target.tgtfacades.user.data.TargetAddressData" %>

<c:if test="${not empty addressData}">
	<span itemprop="streetAddress">
		<c:if test="${not empty addressData.building}"><strong>${fn:escapeXml(addressData.building)}</strong><br/></c:if>
		${fn:escapeXml(addressData.line1)}<br />
		<c:if test="${not empty addressData.line2}">
			${fn:escapeXml(addressData.line2)}<br />
		</c:if>
	</span>
	<span itemprop="addressLocality">${fn:escapeXml(addressData.town)}</span>, <span itemprop="addressRegion">${fn:escapeXml(addressData.state)}</span>, <span itemprop="postalCode">${fn:escapeXml(addressData.postalCode)}</span>
</c:if>
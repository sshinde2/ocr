<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="storeSearchPageData" required="false" type="de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData"%>
<%@ attribute name="locationQuery" required="false" type="java.lang.String"%>
<%@ attribute name="showMoreUrl" required="false" type="java.lang.String"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld"%>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<c:if test="${storeSearchPageData ne null and !empty storeSearchPageData.results}">

	<c:set var="showDistance" value="${storeSearchPageData.sourceLongitude ne 0 and storeSearchPageData.sourceLatitude ne 0}" />

	<table class="store-locator table-tap">
		<thead>
			<tr>
				<th id="store-name"><spring:theme code="storeFinder.table.store" /></th>
				<c:if test="${showDistance}">
					<th id="store-distance"><spring:theme code="storeFinder.table.distance" /></th>
				</c:if>
				<th id="store-address" class="hide-for-tiny"><spring:theme code="storeFinder.table.address" /></th>
				<th id="store-opening" class="hide-for-small"><spring:theme code="storeFinder.table.opening" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${storeSearchPageData.results}" var="pos" varStatus="loopStatus">
				<c:url value="${pos.url}" var="posUrl"/>
				<tr class="${loopStatus.count mod 2 eq 0 ? 'odd' : 'even'}">
					<td headers="store-name" class="storefinder-name">
						<a href="${posUrl}" class="table-tap-canonical">${pos.name}</a>
					</td>
					<c:if test="${showDistance}">
						<td headers="store-distance" class="store-distance">
							${pos.formattedDistance}
						</td>
					</c:if>
					<td headers="store-address" class="hide-for-tiny">
						<store:storeAddressData addressData="${pos.targetAddressData}" />
						<br />${pos.targetAddressData.phone}
					</td>
					<td headers="store-opening" class="hide-for-small">
						<a href="${posUrl}"><spring:theme code="storeFinder.view.opening.hours" /></a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<c:if test="${not empty showMoreUrl}">
		<c:url value="${showMoreUrl}" var="showMoreLink"/>
		<a href="${showMoreLink}" class="button-fwd"><spring:theme code="storeFinder.see.more" /></a>
	</c:if>
</c:if>

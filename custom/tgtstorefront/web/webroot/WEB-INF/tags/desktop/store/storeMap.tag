<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="store" required="false" type="de.hybris.platform.commercefacades.storelocator.data.PointOfServiceData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store"%>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<c:set var="storePinUrl"><asset:resource code="store.map.pin.url" /></c:set>

<c:if test="${store ne null and store.geoPoint ne null and store.geoPoint.latitude ne null and store.geoPoint.longitude ne null}">

	<%-- Map --%>
	<div class="imap store-map"
		data-embedded-json="store-content-data"
		data-name="${store.name}"
		data-lat="${store.geoPoint.latitude}"
		data-lng="${store.geoPoint.longitude}"
		data-marker-image-path="${storePinUrl}"></div>

	<%-- Map Data --%>
	<script type="text/x-json" id="store-content-data">
	<json:object>
		<json:property name="content" escapeXml="false">
			<c:url value="${store.url}" var="storeUrl"/>
			<h4><a href="${storeUrl}">${store.name}</a></h4>
			<p><store:storeAddressData addressData="${store.targetAddressData}" /></p>
			<p>${store.targetAddressData.phone}</p>
		</json:property>
	</json:object>
	</script>

</c:if>

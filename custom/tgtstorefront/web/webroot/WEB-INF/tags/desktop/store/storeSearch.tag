<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="errorNoResults" required="true" type="java.lang.String"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement"%>

<div class="store-head">
	<h1 class="store-heading"><spring:theme code="storeFinder.find.a.store" /></h1>
</div>

<div class="controls-search">
	<c:url value="${targetUrlsMapping.storeFinder}" var="storeFinderFormAction" />
	<form:form action="${storeFinderFormAction}" method="get" commandName="storeFinderForm">

		<div class="control-search-position">
			<spring:theme code="storeFinder.location.unavailable" var="searchLocationUnavailable" />
			<button id="use-my-location" class="control-search-position-button button-norm wide-for-tiny" data-position-url="${targetUrlsMapping.storeFinderPosition}?latitude={{latitude}}&amp;longitude={{longitude}}" data-unavailable-text="${searchLocationUnavailable}" data-checking-text="Checking" type="button"><spring:theme code="storeFinder.use.my.current.location" /></button>
			<span class="or"><spring:theme code="storeFinder.or" /></span>
		</div>

		<div class="control-search-location">
			<spring:theme code="storeFinder.placeholder" var="searchPlaceholder" />
			<label class="visuallyhidden" for="search">${searchPlaceholder}</label>
			<form:input cssClass="text" id="control-search-location-query" path="q" placeholder="${searchPlaceholder}" maxlength="100"  />
			<button class="control-search-location-button button-norm">
				<spring:theme code="storeFinder.search"/>
			</button>
		</div>

	</form:form>
</div>

<c:if test="${storeSearchPageData eq null or empty storeSearchPageData.results}">
	<div class="store-head">
		<h2 class="store-heading"><spring:theme code="storeFinder.find.by.state" /></h2>
		<ul class="store-states">
			<c:forEach items="${states}" var="state">
				<li class="store-state-${fn:toLowerCase(state.code)}">
					<a href="${targetUrlsMapping.storeFinderState}/${fn:toLowerCase(state.code)}">${state.code}</a>
				</li>
			</c:forEach>
		</ul>
	</div>
</c:if>

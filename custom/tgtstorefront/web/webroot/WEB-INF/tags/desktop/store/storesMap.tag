<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="storeSearchPageData" required="false" type="de.hybris.platform.commerceservices.storefinder.data.StoreFinderSearchPageData"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store"%>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<c:set var="storePinUrl"><asset:resource code="store.map.pin.url" /></c:set>

<c:if test="${storeSearchPageData ne null and !empty storeSearchPageData.results}">

	<%-- Map --%>
	<div class="store-locator-map imap-container hide-for-small">
		<div class="imap" data-embedded-json="store-json-data" data-marker-image-path="${storePinUrl}" data-respond-screens="large" ></div>
	</div>

	<%-- Map Data --%>
	<script type="text/x-json" id="store-json-data">
	<json:object>
		<json:object name="center">
			<json:property name="lat" value="${storeSearchPageData.sourceLatitude}" />
			<json:property name="lng" value="${storeSearchPageData.sourceLongitude}" />
		</json:object>
		<json:array name="locations">
			<c:forEach items="${storeSearchPageData.results}" var="store">
				<json:object>
					<json:property name="name" value="${store.name}" />
					<json:property name="lat" value="${store.geoPoint.latitude}" />
					<json:property name="lng" value="${store.geoPoint.longitude}" />
					<json:property name="content" escapeXml="false">
<c:url value="${store.url}" var="storeUrl"/>
<h4><a href="${storeUrl}">${store.name}</a></h4>
<p><store:storeAddressData addressData="${store.targetAddressData}" /></p>
<p>${store.targetAddressData.phone}</p>
<p><a href="${storeUrl}"><spring:theme code="storeFinder.view.opening.hours" /></a></p>
					</json:property>
				</json:object>
			</c:forEach>
		</json:array>
	</json:object>
	</script>

</c:if>

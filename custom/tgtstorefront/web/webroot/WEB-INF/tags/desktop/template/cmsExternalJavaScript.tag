<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty cmsPage.externalJavascript}">
	<c:set var="js" value="${fn:split(cmsPage.externalJavascript, ',')}" />
	<c:forEach items="${js}" var="file">
		<c:set var="query" value="${fn:contains(file, '?') ? '&' : '?'}" />
		<script type="text/javascript" src="${file}${query}b=${cacheBusterTimestamp}"></script>
	</c:forEach>
</c:if>
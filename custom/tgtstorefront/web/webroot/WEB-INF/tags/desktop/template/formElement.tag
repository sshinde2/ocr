<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="path" required="true" rtexprvalue="true"%>
<%@ attribute name="errorPath" required="false" rtexprvalue="true"%>
<%@ attribute name="groupPaths" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@ attribute name="isolated" required="false"%>
<%@ attribute name="isGroup" required="false" type="java.lang.Boolean"%>
<%@ attribute name="elementCSS" required="false" type="java.lang.String" %>
<%@ attribute name="fieldData" required="false" type="java.lang.String" %>
<%@ attribute name="hideError" required="false" rtexprvalue="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:if test="${not hideError}">
	<c:choose>
		<c:when test="${not empty groupPaths}">
			<c:forEach items="${fn:split(groupPaths, ',')}" var="fieldPath">
				<spring:bind path="${fieldPath}">
					<c:if test="${not empty status.errorMessages}">
						<c:set var="errorStatus">
							${errorStatus}<li class="f-err-line"><form:errors path="${fieldPath}" /></li>
						</c:set>
					</c:if>
				</spring:bind>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<spring:bind path="${not empty errorPath ? errorPath : path}">
				<c:if test="${not empty status.errorMessages}">
					<c:set var="errorStatus">
						<p class="f-err-msg error-message">
							<form:errors path="${not empty errorPath ? '' : path}"/>
						</p>
					</c:set>
				</c:if>
			</spring:bind>
		</c:otherwise>
	</c:choose>
</c:if>

<c:set var="coreClass" value="${not empty groupPaths ? 'f-element-group ' : 'f-element '}" />
<c:set var="isolatedClass" value="${isolated ? ' f-isolated' : ''}" />
<c:if test="${not empty errorStatus}">
	<c:set var="errorClass" value="${hideError ? 'f-hidden-error ' : 'f-error '}" />
</c:if>

<div class="${coreClass} ${errorClass} ${elementCSS} ${isolatedClass}" ${fieldData}>
	<jsp:doBody/>
	${errorStatus}
</div>

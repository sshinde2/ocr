<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="abstractOrderData" required="true" type="de.hybris.platform.commercefacades.order.data.AbstractOrderData" %>
<%@ attribute name="conversion" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<json:object name="dataLayer" escapeXml="false">
	<json:property name="ecommCurrency"			value="${abstractOrderData.totalPrice.currencyIso}" />
	<json:property name="ecommTotalValue"		value="${abstractOrderData.totalPrice.value}" />
	<json:property name="ecommTotalTax"			value="${abstractOrderData.totalTax.value}" />
	<c:if test="${conversion}">
		<json:property name="ecommSite"				value="${siteName}" />
		<json:property name="ecommCode"				value="${abstractOrderData.code}" />
		<json:property name="ecommDeliveryValue"	value="${abstractOrderData.deliveryCost.value}" />
		<json:property name="ecommDeliveryTown"		value="${abstractOrderData.deliveryAddress.town}" />
		<json:property name="ecommDeliveryPostCode"	value="${abstractOrderData.deliveryAddress.postalCode}" />
		<json:property name="ecommDeliveryCountry"	value="${abstractOrderData.deliveryAddress.country.name}" />
		<json:property name="ecommDeliveryMode"		value="${abstractOrderData.deliveryMode.name}" />
		<c:if test="${not empty abstractOrderData.affiliateOrderData}">
			<json:property name="affiliateCodes"		value="${abstractOrderData.affiliateOrderData.codes}" />
			<json:property name="affiliateQuantities"	value="${abstractOrderData.affiliateOrderData.quantities}" />
			<json:property name="affiliatePrices"		value="${abstractOrderData.affiliateOrderData.prices}" />
		</c:if>
	</c:if>

	<json:array name="ecommEntries" items="${abstractOrderData.entries}" var="entry">
		<json:object>
			<json:property name="baseCode"	value="${entry.product.baseProductCode}"/>
			<json:property name="code"		value="${entry.product.code}"/>
			<json:property name="name"		value="${entry.product.name}"/>
			<json:property name="quantity"	value="${entry.quantity}"/>
			<json:property name="price"	value="${entry.basePrice.value}"/>
			<c:if test="${not empty entry.product.categories}">
				<c:set var="categoryData" value="${entry.product.categories[fn:length(entry.product.categories) - 1]}" />
				<c:set var="category">${categoryData.code}/${categoryData.name}</c:set>
				<json:property name="category" value="${category}"/>
			</c:if>
		</json:object>
	</json:array>
</json:object>

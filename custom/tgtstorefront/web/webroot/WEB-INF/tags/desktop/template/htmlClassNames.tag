<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="cssClasses" value="" />

<c:if test="${mobileAppMode}">
	<c:set var="cssClasses" value="${cssClasses} MobileApp" />
</c:if>

<c:if test="${androidAppMode}">
	<c:set var="cssClasses" value="${cssClasses} AndroidMobileApp" />
</c:if>

<c:if test="${iosAppMode}">
	<c:set var="cssClasses" value="${cssClasses} IosMobileApp" />
</c:if>

<feature:enabled name="uiLayoutResponsiveIPad">
	<c:set var="cssClasses" value="${cssClasses} RespIPad" />
</feature:enabled>

<feature:enabled name="deferNoncriticalCSS">
	<c:set var="cssClasses" value="${cssClasses} deferred" />
</feature:enabled>

<c:set var="htmlClass">no-js ${cssClasses}</c:set>

<%--
	Add a HTML class per feature to enabled optimisation of page render

	eg. Positively enforced
	<c:if test="${featuresEnabled.featureDeliveryFeesPriceFrenzy}">
		<c:set var="htmlClass">${htmlClass} delivery-frenzy</c:set>
	</c:if>

	eg. Negatively enforced
	<c:if test="${not featuresEnabled.featureHtmlSelectChosen}">
		<c:set var="htmlClass">${htmlClass} no-chosen</c:set>
	</c:if>
--%>

${htmlClass}

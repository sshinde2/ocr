<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="htmlDataAttrs" value="" />

<feature:enabled name="uiLayoutResponsiveIPad">
	<c:set var="htmlDataAttrs">${htmlDataAttrs} data-small-size="768" data-tiny-size="479"</c:set>
</feature:enabled>

${htmlDataAttrs}

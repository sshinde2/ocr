<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>

<jsp:useBean id="now" class="java.util.Date" />

<c:set var="isProd" value="${not assetModeDevelop || param.assetmode eq 'prod'}" />

<%--
	Choose the production or development javascript
--%>
<script>
<c:if test="${not empty gmapsApiClient and not empty gmapsApiVersion}">var t_gm_c="${gmapsApiClient}",t_gm_v="${gmapsApiVersion}";</c:if>
var require=<json:object>
	<json:property name="waitSeconds" value="200" />
	<json:object name="paths">
		<asset:requireJsPath module="main" />
		<asset:requireJsPath module="customer" />
		<asset:requireJsPath module="bootstrap/enews-modal" />
		<asset:requireJsPath module="customer/modify-giftcards" />
		<c:if test="${kioskMode}">
			<asset:requireJsPath module="kiosk/k-main" />
		</c:if>
	</json:object>
	<json:object name="map">
		<json:object name="*">
			<json:property name="feature/mock" value="${featuresEnabled.featureExampleNotReal ? 'feature/mock' : 'feature/null' }" />
			<json:property name="customer/modify-giftcards" value="${featuresEnabled.featureEditGiftcardRecipientDetails ? 'customer/modify-giftcards' : 'feature/null' }" />

		</json:object>
	</json:object>
	<c:choose>
		<c:when test="${isProd}">
			<json:property name="baseUrl" value="${assetResourcePath}/" />
		</c:when>
		<c:otherwise>
			<json:property name="baseUrl" value="${commonResourcePath}/src/js/" />
			<json:property name="urlArgs" value="b=${now.time}" />
		</c:otherwise>
	</c:choose>
</json:object>;
</script>
<c:choose>
	<c:when test="${not isProd}">
		<script src="${commonResourcePath}/src/js/head/session-start.js?b=${now.time}"></script>
		<script src="${commonResourcePath}/src/js/head/storage-injector.js?b=${now.time}"></script>
		<script src="${commonResourcePath}/src/js/head/picturefill.js?b=${now.time}"></script>
		<script src="${commonResourcePath}/src/js/head/lazysizes-config.js?b=${now.time}"></script>
		<script src="${commonResourcePath}/src/js/head/sitewide-notification.js?b=${now.time}"></script>
		<script src="${commonResourcePath}/src/js/head/segment-dimension.js?b=${now.time}"></script>
		<script src="${commonResourcePath}/src/js/head/window-messages.js?b=${now.time}"></script>
		<c:if test="${kioskMode}">
			<script src="${commonResourcePath}/src/js/kiosk/head/feature-check.js?b=${now.time}"></script>
			<script src="${commonResourcePath}/src/js/kiosk/head/dimensions.js?b=${now.time}"></script>
		</c:if>
		<script src="${commonResourcePath}/src/js/lib/modernizr.custom.js?b=${now.time}"></script>
		<script src="${commonResourcePath}/src/js/require.js?b=${now.time}"></script>
		<script src="${commonResourcePath}/src/js/require-config.js?b=${now.time}"></script>
		<script>require(['main'], function(){});</script>
	</c:when>
	<c:otherwise>
		<script src="${assetResourcePath}/${requestScope['ui_resource_js/require.js']}" data-main="main"></script>
		<c:if test="${kioskMode}">
			<script src="${assetResourcePath}/${requestScope['ui_resource_js/kiosk/k-head.js']}"></script>
		</c:if>
	</c:otherwise>
</c:choose>

<kiosk:templateJavaScript />

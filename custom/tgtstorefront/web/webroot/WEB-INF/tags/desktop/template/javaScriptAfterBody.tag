<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<jsp:useBean id="now" class="java.util.Date" />
<c:set var="isProd" value="${not assetModeDevelop || param.assetmode eq 'prod'}" />

<feature:enabled name="uiServeNewAssets">
	<c:choose>
		<c:when test="${not isProd}">
			<script src="${commonResourcePath}/src/js/bundle.js?b=${now.time}"></script>
		</c:when>
		<c:otherwise>
			<script src="${assetResourcePath}/${requestScope['ui_resource_js/bundle.js']}"></script>
		</c:otherwise>
	</c:choose>
</feature:enabled>

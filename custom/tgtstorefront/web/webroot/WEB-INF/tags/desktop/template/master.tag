<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaKeywords" required="false" %>
<%@ attribute name="cmsExternalCss" required="false" fragment="true" %>
<%@ attribute name="cmsInlineCss" required="false" fragment="true" %>
<%@ attribute name="cmsExternalJavaScript" required="false" fragment="true" %>
<%@ attribute name="cmsInlineJavaScript" required="false" fragment="true" %>
<%@ attribute name="outerNavigation" required="false" fragment="true" %>
<%@ attribute name="optionalMeta" required="false" fragment="true" %>
<%@ attribute name="bareMinimum" required="false" type="java.lang.Boolean" %>
<%@ attribute name="jsRequired" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disableExternalScripts" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disableSnippets" required="false" type="java.lang.Boolean" %>
<%@ attribute name="headerOnly" required="false" type="java.lang.Boolean" %>
<%@ attribute name="footerOnly" required="false" type="java.lang.Boolean" %>
<%@ attribute name="checkoutStepName" required="false" type="java.lang.String" %>
<%@ attribute name="bodyCssClass" required="false" type="java.lang.String" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
<%@ taglib prefix="debug" tagdir="/WEB-INF/tags/shared/debug" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>

<!DOCTYPE html>
<% response.setHeader("X-UA-Compatible","IE=edge"); %>
<c:set var="htmlClass"><template:htmlClassNames /></c:set>
<c:set var="htmlDataAttrs"><template:htmlDataAttrs /></c:set>
<html class="${htmlClass}" ${htmlDataAttrs} lang="${currentLanguage.isocode}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<feature:enabled name="uiLayoutResponsiveIPad">
		<meta name="viewport" content="width=device-width">
	</feature:enabled>
	<title><c:choose>
		<c:when test="${not empty pageTitle}">${pageTitle}</c:when>
		<c:when test="${not empty cmsPage.title}">${cmsPage.title}</c:when>
		<c:otherwise><spring:message code="page.title.suffix"/></c:otherwise>
	</c:choose></title>

	<%-- Canonical URL --%>
	<c:if test="${not empty canonicalUrl}">
		<link rel="canonical" href="${canonicalUrl}" />
	</c:if>

	<feature:disabled name="uiLayoutResponsiveIPad">
		<%--
			We don't want viewport screens less than 768
			Using script to do this so the page can remain cachable
		--%>
		<script>if(!/ipad/i.test(navigator.userAgent)){document.write('<meta name="viewport" content="width=device-width">')}</script>
	</feature:disabled>

	<%-- Mobile devices with js disabled are forced to a 960 viewport --%>
	<noscript><meta name="viewport" content="width=960"></noscript>

	<!--[if lte IE 9]><script>document.documentElement.className+=' lt-ie9 lt-ie10';</script><![endif]-->

	<%-- To tracking timings --%>
	<script>var t_time = (new Date()).getTime(), WRInitTime = t_time;</script>

	<c:if test="${not bareMinimum}" >
		<meta name="msapplication-config" content="none"/>
		<jsp:invoke fragment="optionalMeta" />
		<template:searchMetadata metaDescription="${metaDescription}" metaKeywords="${metaKeywords}" />
		<template:openGraph metaDescription="${metaDescription}" />

		<%-- Favourite Icon --%>
		<c:set var="faviconURL"><asset:resource code="img.favIcon" /></c:set>
		<link rel="shortcut icon" href="${faviconURL}" type="image/x-icon"/>
	</c:if>

	<%-- CSS Files Are Loaded First as they can be downloaded in parallel --%>
	<feature:enabled name="deferNoncriticalCSS">
		<style type="text/css">
			<spring:message code="critical.css" />
			<c:if test="${kioskMode}">
				<spring:message code="critical.kiosk" />
			</c:if>
		</style>
	</feature:enabled>
	<feature:disabled name="deferNoncriticalCSS">
		<template:styleSheets mediaQueriesDisabled="${(not empty headerOnly and headerOnly) or (not empty footerOnly and footerOnly)}"/>
	</feature:disabled>

	<jsp:invoke fragment="cmsExternalCss" />
	<jsp:invoke fragment="cmsInlineCss" />

	<%-- Load googleAnalytics isogram snippet --%>
	<c:if test="${not disableExternalScripts}">
		<analytics:googleAnalyticsObject />
	</c:if>

	<%-- Inject any additional CSS required by the page --%>
	<c:if test="${not bareMinimum or jsRequired}" >
		<%-- Load JavaScript required by the site --%>
		<template:javaScript/>
		<c:if test="${not disableExternalScripts}">
			<analytics:googleExperiments />
		</c:if>
	</c:if>

	<%-- Load moderinzr for site furniture --%>
	<c:if test="${bareMinimum and not empty headerOnly and headerOnly}">
		<template:modernizr />
	</c:if>

	<%-- Build and fire analytics data layer information --%>
	<c:if test="${not disableExternalScripts}">
		<analytics:googleAnalytics checkoutStepName="${checkoutStepName}" />
	</c:if>

	<c:if test="${not disableSnippets}">
		<template:mvtJavascript />

		<cms:slot var="mvtComponent" contentSlot="${slots.MVT}">
			<cms:component component="${mvtComponent}"/>
		</cms:slot>
	</c:if>
</head>
<template:flush />
<feature:enabled name="uiWebPageSchema">
	<c:set var="webpageSchema" value="${true}" />
</feature:enabled>
<target:body headerOnly="${not empty headerOnly and headerOnly}" bodyCssClass="${bodyCssClass}" itemType="${not homepage and webpageSchema ? 'http://schema.org/WebPage' : ''}" >

	<template:inlineAssets />

	<jsp:invoke fragment="outerNavigation" />

	<c:if test="${not anonymousCachable}">
		<sec:authorize access="hasRole('ROLE_CUSTOMERGROUP')">
			<c:set var="knownUser"> data-known-user="true"</c:set>
		</sec:authorize>
	</c:if>

	<feature:enabled name="featureWishlistFavourites">
		<c:set var="featureData">data-favourites-server="true"</c:set>
	</feature:enabled>

	<div class="set ${anonymousCachable ? 'anonymous-cachable' : ''}" data-anonymous-cachable="${anonymousCachable}" ${knownUser} ${featureData}>
		<c:if test="${not disableExternalScripts}">
			<analytics:googleTagManager />
		</c:if>

		<%-- Inject the page body here --%>
		<jsp:doBody/>

		<c:if test="${not bareMinimum}" >
			<form name="accessiblityForm">
				<input type="hidden" id="accesibility_refreshScreenReaderBufferField" name="accesibility_refreshScreenReaderBufferField" value=""/>
			</form>
		</c:if>

		<jsp:invoke fragment="cmsExternalJavaScript" />
		<jsp:invoke fragment="cmsInlineJavaScript" />

		<script>
		/mobi/i.test(navigator.userAgent) && !location.hash && setTimeout(function () {
		  if (!window.pageYOffset) window.scrollTo(0, 1);
		}, 1000);
		</script>
	</div>

	<%-- Footer Snippet Slot --%>
	<c:if test="${not disableSnippets}">
		<cms:slot var="footerSnippetComponent" contentSlot="${slots.FooterSnippet}">
			<cms:component component="${footerSnippetComponent}"/>
		</cms:slot>
	</c:if>

	<%-- Global handlebars templates --%>

	<feature:enabled name="featureConsolidatedStoreStockVisibility">
		<common:handlebarsTemplates template="consolidatedStockMessageTemplate" />
		<common:handlebarsTemplates template="fisUnavailableErrorTemplate" />
	</feature:enabled>

	<feature:enabled name="featureLocationServices">
		<common:handlebarsTemplates template="shoppingLocation" />
	</feature:enabled>

	<common:handlebarsTemplates template="externalRecommendationsCarousel" />

	<feature:enabled name="deferNoncriticalCSS">
		<template:styleSheets deferred="${true}" mediaQueriesDisabled="${(not empty headerOnly and headerOnly) or (not empty footerOnly and footerOnly)}" />
	</feature:enabled>

	<c:if test="${not bareMinimum or jsRequired}" >
		<%-- Load JavaScript required by the site --%>
		<template:javaScriptAfterBody />
	</c:if>
</target:body>
<c:if test="${not bareMinimum}" >
	<debug:debugFooter/>
</c:if>
</html>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>


<%--
	Only for site Furniture
--%>
<c:choose>
	<c:when test="${assetModeDevelop}">
		<c:set var="modernizrUrl">${commonResourcePath}/src/js/lib/modernizr.custom.js</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="modernizrUrl">
			<asset:resource critical="js/lib/modernizr.custom.js" basePath="${commonResourcePath}" />
		</c:set>
	</c:otherwise>
</c:choose>

<script src="${modernizrUrl}"></script>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ attribute name="metaDescription" required="false" type="java.lang.String" %>

<%-- Social Defaults --%>
<c:set var="socialImage"><asset:resource code="opengraph.logo" /></c:set>
<spring:theme code="opengraph.title" text="" var="socialTitle"/>
<spring:theme code="opengraph.type" text="" var="opengraphType"/>
<spring:theme code="opengraph.locale" text="" var="opengraphLocale"/>
<spring:theme code="opengraph.sitename" text="" var="opengraphSitename"/>
<spring:theme code="twitter.username" text="" var="twitterUsername" />

<%-- Open Graph for Products --%>
<c:if test="${not empty product}">
	<spring:theme code="opengraph.product.title" text="{0}" arguments="${product.name}" var="socialTitle" />
	<c:set var="socialDescription" value="${target:abbreviateHTMLString(product.description, 155)}" />
	<c:set var="productImage" value="${ycommerce:productImage(product, 'product')}" />
	<c:if test="${not empty productImage and not empty productImage.url}">
		<c:set var="socialImage" value="${productImage.url}" />
		<c:set var="socialProductImage" value="${productImage.url}" />
	</c:if>
	<c:set var="socialCanonicalUrl" value="${fullyQualifiedDomainName}${product.canonical.url}" />
</c:if>

<%-- Open Graph for non-Products --%>
<c:if test="${empty product}">
	<c:set var="socialTitle" value="${not empty pageTitle ? pageTitle : not empty cmsPage.title ? cmsPage.title : socialTitle}" />
	<c:if test="${not empty metaDescription}">
		<c:set var="socialDescription" value="${metaDescription}" />
	</c:if>
	<c:if test="${not empty canonicalUrl}">
		<c:set var="canonicalUrlWithDomainName" value="${fullyQualifiedDomainName}${canonicalUrl}" />
		<c:set var="socialCanonicalUrl" value="${fn:contains(canonicalUrl,fullyQualifiedDomainName) ? canonicalUrl : canonicalUrlWithDomainName}" />
	</c:if>
	<c:if test="${not empty metaImage and not empty metaImage.url}">
		<c:set var="socialImage" value="${metaImage.url}" />
	</c:if>
</c:if>

<%-- Google+ --%>
<meta itemprop="name" content="${opengraphSitename}" />
<meta itemprop="description" content="${socialDescription}" />
<meta itemprop="image" content="${fullyQualifiedDomainName}${socialImage}" />

<%-- Facebook / LinkedIn --%>

<meta property="og:title" content="${socialTitle}" />
<meta property="og:description" content="${socialDescription}" />
<meta property="og:image" content="${fullyQualifiedDomainName}${socialImage}" />
<meta property="og:type" content="${opengraphType}" />
<meta property="og:locale" content="${opengraphLocale}" />
<meta property="og:site_name" content="${opengraphSitename}" />
<c:if test="${not empty socialCanonicalUrl}">
	<meta property="og:url" content="${socialCanonicalUrl}" />
</c:if>

<%-- Twitter Cards --%>

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="${twitterUsername}" />
<meta name="twitter:creator" content="${twitterUsername}" />
<meta name="twitter:title" content="${socialTitle}" />
<meta name="twitter:description" content="${socialDescription}" />
<meta name="twitter:url" content="${socialCanonicalUrl}" />
<c:if test="${not empty socialImage}">
	<meta name="twitter:image:src" content="${fullyQualifiedDomainName}${socialImage}" />
</c:if>

<%-- Facebook Insights --%>
<spring:theme code="facebook.admins" text="" var="facebookAdmins" />
<spring:theme code="facebook.appid" text="" var="facebookAppid" />
<c:if test="${not empty facebookAdmins}">
	<meta property="fb:admins" content="${facebookAdmins}" />
</c:if>
<c:if test="${not empty facebookAppid}">
	<meta property="fb:app_id" content="${facebookAppid}" />
</c:if>

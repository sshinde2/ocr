<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="customer" tagdir="/WEB-INF/tags/desktop/customer" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/desktop/common/footer" %>

<template:master pageTitle="${pageTitle}" bareMinimum="${true}">
	<div id="wrapper" data-context-path="${request.contextPath}">
		<div id="page" class="modal-page">
			<div class="header hfma">
				<div class="header-inner">
					<div class="site-logo">
						<header:sitelogo print="true" />
					</div>
				</div>
			</div>
			<template:flush />
			<div class="content">
				<jsp:doBody/>
			</div>
		</div>
	</div>
	<c:if test="${not mobileAppMode}">
		<footer:footerCheckout />
	</c:if>
</template:master>

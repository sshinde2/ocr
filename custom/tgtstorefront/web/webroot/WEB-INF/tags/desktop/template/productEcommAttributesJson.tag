<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="price" required="false" type="java.math.BigDecimal" %>
<%@ attribute name="list" required="false" type="java.lang.String" %>
<%@ attribute name="quantity" required="false" type="java.lang.String" %>
<%@ attribute name="isArray" required="false" type="java.lang.Boolean" %>
<%@ attribute name="assorted" required="false" type="java.lang.String" %>
<%@ attribute name="displayOnly" required="false" type="java.lang.String" %>
<%@ attribute name="id" required="false" type="java.lang.String" %>
<%@ attribute name="name" required="false" type="java.lang.String" %>
<%@ attribute name="code" required="false" type="java.lang.String" %>
<%@ attribute name="brand" required="false" type="java.lang.String" %>
<%@ attribute name="category" required="false" type="java.lang.String" %>
<%@ attribute name="position" required="false" type="java.lang.String" %>
<%@ attribute name="variant" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%--
json taglib forcing me to repeat the json:object, one with a name and one without.
because the name throws an error when nested inside an array.
--%>
<c:set var="templatedPosition" value="${fn:contains(position, '{{')}" />
<c:if test="${not templatedPosition}">
	<fmt:parseNumber value="${position}" var="numPosition" />
</c:if>
<c:set var="json"><c:choose><c:when test="${isArray}">
<json:object escapeXml="false">
	<json:property name="id" value="${id}" />
	<json:property name="name" value="${name}" />
	<c:if test="${not empty price}">
		<json:property name="price" value="${price}" />
	</c:if>
	<c:if test="${not empty list}">
		<json:property name="list" value="${list}" />
	</c:if>
	<c:if test="${not empty category}">
		<json:property name="category" value="${category}"/>
	</c:if>
	<c:if test="${not empty brand}">
		<json:property name="brand" value="${brand}"/>
	</c:if>
	<c:if test="${numPosition > 0 or templatedPosition}">
		<json:property name="position" value="${position}" />
	</c:if>
	<c:if test="${not empty variant and variant ne 'No Colour'}">
		<json:property name="variant" value="${variant}" />
	</c:if>
	<c:if test="${not empty quantity}">
		<json:property name="quantity" value="${quantity}" />
	</c:if>
	<c:if test="${not empty assorted}">
		<json:property name="${targetCustomDimensionsMapping.assorted}" value="${assorted}" />
	</c:if>
	<c:if test="${not empty displayOnly}">
		<json:property name="${targetCustomDimensionsMapping.displayOnly}" value="${displayOnly}" />
	</c:if>
</json:object>
</c:when><c:otherwise>
<json:object name="ecProduct" escapeXml="false">
	<json:property name="id" value="${id}" />
	<json:property name="name" value="${name}" />
	<c:if test="${not empty price}">
		<json:property name="price" value="${price}" />
	</c:if>
	<c:if test="${not empty list}">
		<json:property name="list" value="${list}" />
	</c:if>
	<c:if test="${not empty category}">
		<json:property name="category" value="${category}"/>
	</c:if>
	<c:if test="${not empty brand}">
		<json:property name="brand" value="${brand}"/>
	</c:if>
	<c:if test="${numPosition > 0 or templatedPosition}">
		<json:property name="position" value="${position}" />
	</c:if>
	<c:if test="${not empty variant and variant ne 'No Colour'}">
		<json:property name="variant" value="${variant}" />
	</c:if>
	<c:if test="${not empty quantity}">
		<json:property name="quantity" value="${quantity}" />
	</c:if>
	<c:if test="${not empty assorted}">
		<json:property name="${targetCustomDimensionsMapping.assorted}" value="${assorted}" />
	</c:if>
	<c:if test="${not empty displayOnly}">
		<json:property name="${targetCustomDimensionsMapping.displayOnly}" value="${displayOnly}" />
	</c:if>
</json:object>
</c:otherwise></c:choose></c:set>
${fn:replace(json, '\'', '&#39;')}

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ attribute name="priceData" required="false" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="list" required="false" type="java.lang.String" %>
<%@ attribute name="position" required="false" type="java.lang.Integer" %>
<%@ attribute name="variant" required="false" type="java.lang.String" %>
<%@ attribute name="quantity" required="false" type="java.lang.String" %>
<%@ attribute name="isArray" required="false" type="java.lang.Boolean" %>
<%@ attribute name="assorted" required="false" type="java.lang.String" %>
<%@ attribute name="displayOnly" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>

<c:set var="id" value="${product.baseProductCode}"/>
<c:set var="price" value="${product.price.value}" />
<c:set var="brand" value="${product.brand}" />
<c:set var="name" value="${product.baseName}" />

<%-- Endeca based products in a list, Do not know sellable variant code so do not send --%>
<c:if test="${empty list}">
	<c:set var="variant" value="${product.code}" />
</c:if>

<%-- Express the price specifically --%>
<c:if test="${not empty priceData}">
	<c:set var="price" value="${priceData.value}" />
</c:if>

<%-- set topLevelCategory as category if present --%>
<c:if test="${not empty product.topLevelCategory}">
	<c:set var="category" value="${product.topLevelCategory}" />
</c:if>

<template:productEcommAttributesJson
	id="${id}"
	name="${name}"
	price="${price}"
	list="${list}${personalised}"
	category="${category}"
	brand="${brand}"
	position="${position}"
	variant="${variant}"
	quantity="${quantity}"
	isArray="${isArray}"
	assorted="${assorted}"
	displayOnly="${displayOnly}" />

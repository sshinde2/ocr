<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="metaDescription" required="false" type="java.lang.String" %>
<%@ attribute name="metaKeywords" required="false" type="java.lang.String" %>

<c:if test="${not empty metaDescription}">
	<meta name="description" content="${metaDescription}"/>
</c:if>
<c:if test="${not empty metaKeywords}">
	<meta name="keywords" content="${metaKeywords}"/>
</c:if>
<c:if test="${not empty metaRobots}">
	<meta name="robots" content="${metaRobots}"/>
</c:if>
<c:if test="${not empty product}">
	<c:url var="canonicalUrl" value="${fullyQualifiedDomainName}${product.canonical.url}" />
	<link rel="canonical" href="${canonicalUrl}" />
</c:if>


<c:if test="${not empty canonical}">
	<c:url value="${fullyQualifiedDomainName}${canonical.url}" var="canonicalUrl" />
	<link rel="canonical" href="${canonicalUrl}" />
</c:if>
<c:if test="${not empty canonical.prevUrl}">
	<c:url value="${fullyQualifiedDomainName}${canonical.prevUrl}" var="prevUrl" />
	<link rel="prev" href="${prevUrl}" />
</c:if>
<c:if test="${not empty canonical.nextUrl}">
	<c:url value="${fullyQualifiedDomainName}${canonical.nextUrl}" var="nextUrl" />
	<link rel="next" href="${nextUrl}" />
</c:if>

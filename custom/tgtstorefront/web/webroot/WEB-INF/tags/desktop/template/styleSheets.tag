<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="mobileApp" tagdir="/WEB-INF/tags/desktop/mobileapp" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>
<%@ attribute name="mediaQueriesDisabled" required="false" type="java.lang.Boolean" %>
<%@ attribute name="deferred" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="screenAssetPath" value="css/screen.css" />
<feature:enabled name="uiLayoutResponsiveIPad">
	<c:set var="screenAssetPath" value="css/zcreen.css" />
</feature:enabled>

<c:set var="screenCSS"><asset:resource critical="${screenAssetPath}" basePath="${themeResourcePath}" /></c:set>
<c:set var="deferredLoad"><c:if test="${deferred}">onload="window._co()"</c:if></c:set>

<%-- The ID on the style sheet is used to help indicate to the JavaScript the intent to use media queries --%>
<!--[if gt IE 9]><!--><c:set var="screenLink"><link id="screen-mq" href="${screenCSS}" rel="stylesheet" type="text/css" ${deferredLoad} /></c:set><!--<![endif]-->

<c:choose>
	<c:when test="${deferred}">
		<%-- https://developers.google.com/speed/docs/insights/OptimizeCSSDelivery --%>
		<noscript id="deferred-styles">
			<c:out value="${screenLink}" escapeXml="false" />
			<kiosk:templateStyleSheets />
			<mobileApp:templateStyleSheets />
		</noscript>
		<script>
			(function(et,au) {
				var t = function() {
					var r = au.getElementById("deferred-styles"), g = au.createElement("div");
					g.innerHTML = r.textContent;
					au.body.appendChild(g)
					r.parentElement.removeChild(r);
				}, a = requestAnimationFrame || mozRequestAnimationFrame || webkitRequestAnimationFrame || msRequestAnimationFrame,
				r=function() {et.setTimeout(t, 0);};
				a ? a(r) : et.addEventListener('load', t);
				et._co = function() {
					et && et.publish && et.publish('/html-inject/load');
				}
			})(window, document);
		</script>
	</c:when>
	<c:otherwise>
		<c:out value="${screenLink}" escapeXml="false" />
		<kiosk:templateStyleSheets />
		<mobileApp:templateStyleSheets />
	</c:otherwise>
</c:choose>

<c:if test="${mobileAppMode}">
	<c:if test="${iosAppMode}">
		<%-- Inlining <style> because it is only one selector. If any more selectors are required, then this should be moved to an external file  --%>
		<style type="text/css">
			.hfma-ios {
				display: none !important;
			}
		</style>
	</c:if>

	<c:if test="${androidAppMode}">
		<%-- Inlining <style> because it is only one selector. If any more selectors are required, then this should be moved to an external file  --%>
		<style type="text/css">
			.hfma-android {
				display: none !important;
			}
		</style>
	</c:if>
</c:if>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="conditionsOfUseLink">
	<a href="/modal/conditions-of-use" class="lightbox" data-lightbox-type="content">
		<spring:theme code="login.optIn.conditions" />
	</a>
</c:set>

<c:set var="privacyLink">
	<a href="/modal/privacy" class="lightbox" data-lightbox-type="content">
		<spring:theme code="login.optIn.privacy" />
	</a>
</c:set>

<p class="subtle"><spring:theme code="login.optIn" arguments="${conditionsOfUseLink},${privacyLink}" /></p>

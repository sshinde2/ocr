<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>


<p class="required"><spring:theme code="form.required"/></p>
<form:form method="post" commandName="updatePwdForm" cssClass="${cssClass}">
	<formElement:formPasswordBox idKey="password" labelKey="updatePwd.pwd" path="pwd" inputCSS="text password strength" mandatory="true" strength="${true}"/>
	<formElement:formPasswordBox idKey="updatePwd.checkPwd" labelKey="updatePwd.checkPwd" path="checkPwd" inputCSS="text password" mandatory="true" errorPath="updatePwdForm"/>
	<div class="f-buttons">
		<button type="submit" class="button-fwd button-single"><spring:theme code='text.account.button.saveChanges'/></button>
	</div>
	<target:csrfInputToken/>	
</form:form>

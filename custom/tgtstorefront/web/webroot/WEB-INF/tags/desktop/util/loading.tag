<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="color" required="false" type="java.lang.String" %>
<%@ attribute name="inline" required="false" type="java.lang.Boolean" %>
<%@ attribute name="className" required="false" type="java.lang.String" %>

<c:set var="colorClass" value="${not empty color ? 'Loading--' : ''}${color}" />
<div class="Loading ${colorClass} ${not empty inline ? 'Loading--inline' : ''} ${className}">
	<div class="Loading-bounce"></div>
	<div class="Loading-bounce"></div>
	<div class="Loading-bounce"></div>
</div>

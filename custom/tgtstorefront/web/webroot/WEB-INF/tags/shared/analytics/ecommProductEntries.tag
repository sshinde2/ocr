<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="entries" required="true" type="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>

<c:forEach items="${entries}" var="entry" varStatus="entryStatus">
	<c:set var="ecProductItem">${ecProductItem}ga.apply(ga,<json:array escapeXml="false">
		<json:property value="ec:addProduct" />
			<template:productEcommJson
				isArray="true"
				product="${entry.product}"
				quantity="${entry.quantity}"
				priceData="${entry.basePrice}"
				assorted="${entry.product.assorted}" />
	</json:array>);</c:set>
</c:forEach>
${ecProductItem}

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
ga.apply(ga,<json:array escapeXml="false">
	<json:property value="ec:setAction" />
	<json:property value="purchase" />
	<json:object escapeXml="false">
		<json:property name="id" value="${orderData.code}" />
		<json:property name="affiliation" value="${siteName}" />
		<json:property name="revenue" value="${orderData.totalPrice.value}" />
		<json:property name="shipping" value="${orderData.deliveryCost.value}" />
		<json:property name="tax" value="${orderData.totalTax.value}" />
		<c:if test="${fn:length(orderData.appliedVoucherCodes) > 0}">
			<%-- It appears that GA:EC only supports one coupon --%>
			<json:property name="coupon" value="${orderData.appliedVoucherCodes[0]}" />
		</c:if>
	</json:object>
</json:array>);
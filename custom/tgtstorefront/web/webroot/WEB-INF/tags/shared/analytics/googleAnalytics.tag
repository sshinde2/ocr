<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="checkoutStepName" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:if test="${not empty universalAnalyticsId}">

	<c:set var="pageview">
		<json:object escapeXml="false">
			<json:property name="hitType" value="pageview" />
			<c:if test="${not empty trackPageViewUrlOverride}">
				<json:property name="page" value="${trackPageViewUrlOverride}" />
			</c:if>
		</json:object>
	</c:set>

	<c:set var="ecAddProducts"><analytics:ecommProductEntries entries="${not empty orderData ? orderData.entries : cartData.entries}" /></c:set>

	<%-- If we are in the Checkout, create ecAddProducts and ecSetActionCheckout --%>
	<c:if test="${not empty checkoutSteps and not empty checkoutStepName}">
		<c:forEach items="${checkoutSteps}" var="checkoutStep" varStatus="status">
			<c:if test="${checkoutStep.stepName eq checkoutStepName}">
				<c:set var="ecSetActionCheckout">${ecAddProducts}ga.apply(ga,<json:array>
					<json:property value="ec:setAction" />
					<json:property value="checkout" />
					<json:object escapeXml="false">
						<json:property name="step" value="${status.count + 1}" />
						<c:if test="${checkoutStepName eq 'paymentDetails' and kioskMode}">
							<json:property name="option" value="pinpad" />
						</c:if>
						<c:if test="${checkoutStepName eq 'reviewYourOrder' and expedited}">
							<json:property name="option" value="expedited" />
						</c:if>
					</json:object>
				</json:array>);</c:set>
			</c:if>
		</c:forEach>
	</c:if>

	<c:choose>

		<c:when test="${pageType.value == 'ProductSearch' || pageType.value == 'Category' || pageType.value == 'Brand'}">

			<c:if test="${searchPageData.pagination.totalNumberOfResults > 0 and not empty searchPageData.breadcrumbs}">
				<c:forEach items="${searchPageData.breadcrumbs}" var="breadcrumb">
					<c:set var="universalFacetTrackEvent">${universalFacetTrackEvent}ga.apply(ga,<json:array>
						<json:property escapeXml="false" value="send" />
						<json:property escapeXml="false" value="event" />
						<json:property escapeXml="false" value="Facet" />
						<json:property escapeXml="false" value="${breadcrumb.facetName}" />
						<json:property escapeXml="false" value="${breadcrumb.facetValueName}" />
						<json:object escapeXml="false">
							<json:property name="nonInteraction" value="true" />
						</json:object>
					</json:array>);</c:set>
				</c:forEach>
			</c:if>

		</c:when>
		<c:when test="${pageType.value == 'OrderConfirmation'}">
			<c:set var="ecSetActionPurchase">${ecAddProducts}<analytics:ecommPurchase /></c:set>
		</c:when>
		<c:when test="${pageType.value == 'CheckoutLogin'}">
			<c:set var="ecSetActionCheckout">${ecAddProducts}ga.apply(ga,<json:array>
				<json:property value="ec:setAction" />
				<json:property value="checkout" />
				<json:object>
					<json:property name="step" value="1" />
				</json:object>
			</json:array>);</c:set>
		</c:when>

	</c:choose>

	<%-- If there is any ec:setAction action, we are going to need to require ec.js --%>
	<%-- If there is either a checkout or purchase action type, then lodge it with the page view. --%>
	<c:if test="${not empty ecSetActionCheckout or not empty ecSetActionPurchase}">
		<c:set var="ecommInit">ga('require', 'ec');</c:set>
		<c:set var="ecommForPage" value="${not empty ecSetActionPurchase ? ecSetActionPurchase : ( not empty ecSetActionCheckout ? ecSetActionCheckout : '')}" />
	</c:if>

	<%-- If there is both a checkout or purchase action type, then lodge the left overs with another event. --%>
	<c:if test="${not empty ecSetActionCheckout and not empty ecSetActionPurchase}">
		<c:set var="ecommForEvent">${ecSetActionCheckout}ga.apply(ga,<json:array>
			<json:property escapeXml="false" value="send" />
			<json:property escapeXml="false" value="event" />
			<json:property escapeXml="false" value="Checkout" />
			<json:property escapeXml="false" value="${checkoutStepName}" />
			<json:object escapeXml="false">
				<json:property name="nonInteraction" value="true" />
			</json:object>
		</json:array>);</c:set>
	</c:if>

	<c:if test="${anonymousCachable}">
		<c:set var="crossDeviceUIDCode">
		(function(ls) {
			try {
				var ga_uid = ls.getItem('ga_uid');
				if( typeof ga_uid === 'string' ) {
					ga('set', '&uid', ga_uid);
				}
			} catch(e) {}
		}(localStorage));
		</c:set>
	</c:if>

	<c:if test="${not anonymousCachable}">
		<sec:authorize access="hasRole('ROLE_CUSTOMERGROUP')">
			<c:if test="${user.uid ne 'anonymous' and user.allowTracking}">
				<c:set var="allowedTrackingGuid" value="${user.trackingGuid}" />
			</c:if>
		</sec:authorize>
		<c:set var="crossDeviceUIDCode">
		(function(ls) {
			var ga_uid = '${allowedTrackingGuid}';
			if( ga_uid ) {
				try {
					ga('set', '&uid', ga_uid);
					ls.setItem('ga_uid',ga_uid);
				} catch(e) {}
			} else {
				try {
					ls.removeItem('ga_uid');
				} catch(e) {}
			}
		}(localStorage));
		</c:set>
	</c:if>

	<c:if test="${kioskMode}">

		<c:set var="kioskDimensions">
			if( typeof t_ga_kiosk_set === "object" ) {
				ga('set', t_ga_kiosk_set);
			}
		</c:set>

	</c:if>

	<c:set var="segmentDimensions">
		if( typeof T_GA_SEGMENT_SET === "object" ) {
			ga('set', T_GA_SEGMENT_SET);
		}
	</c:set>

	<feature:enabled name="productEndOfLife">
		<c:if test="${pageType.value eq 'Product'}">
			<c:set var="availQtyInStoreorOnline" value="${availableOnline or availableInStore}" />
			<c:if test="${not availQtyInStoreorOnline}">
				<c:set var="endOfLife">
					ga('set', '${targetCustomDimensionsMapping.endOfLife}', true);
				</c:set>
			</c:if>
		</c:if>
	</feature:enabled>

	<feature:enabled name="uiGoogleOptimize">
		<c:set var="gOptimizeBefore">
			(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
			h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
			(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
			})(window,document.documentElement,'async-hide','dataLayer',4000,
			{'${gOptimizeContainer}':true});
		</c:set>
		<c:set var="gOptimize">
			ga('require', '${gOptimizeContainer}');
		</c:set>
	</feature:enabled>

	<%--
	Some References
	https://developers.google.com/analytics/devguides/collection/analyticsjs/cross-domain#linkerparam
	https://developers.google.com/analytics/devguides/collection/analyticsjs/display-features
	https://support.google.com/analytics/answer/2558867/?hl=en-GB&utm_id=ad&authuser=0
	--%>
	<script type="text/javascript">
		if ( window.ga ) {
			${gOptimizeBefore}
			${crossDeviceUIDCode}
			ga('require', 'displayfeatures');
			ga('require', 'linkid', 'linkid.js');
			${ecommInit}
			${ecommForPage}
			${kioskDimensions}
			${segmentDimensions}
			${gOptimize}
			${endOfLife}
			ga('send', ${pageview});
			${universalFacetTrackEvent}
			${ecommForEvent}
		}
	</script>

</c:if>

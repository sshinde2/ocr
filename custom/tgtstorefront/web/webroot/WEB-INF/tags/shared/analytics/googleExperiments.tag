<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--

 ## Experiments By Google

 This requires `ga.js` or `analytics.js` for the experiment to work.
 An experiement must be created in the GA analytics profile and the id placed in the cmsPage.experimentId

 Developer Notes
 	https://developers.google.com/analytics/devguides/collection/gajs/experiments
	
 Example: Using the Helper with original and 1 other variant: 
	ctxPageExecute(function(){
		$('.product-carousel:eq(0) > h3').text('Hot Items');
	});

 Example: Using the Helper with original and 2 other variants:
	var mvt_selector = '.product-carousel:eq(0) > h3';
	ctxPageExecute(function(){
		$(mvt_selector).text('Hot Items');
	},function(){
		$(mvt_selector).text('Cool Items');
	});

--%>
<c:choose>
	<c:when test="${(not empty analyticsAccountId or not empty universalAnalyticsId) and not empty cmsPage.experimentId}">

		<script src="//www.google-analytics.com/cx/api.js?experiment=${cmsPage.experimentId}"></script>
		<script>
		(function(g, dE, fn, v){
			if( typeof g.cxApi !== 'undefined' ) {
				cxApi.setDomainName(".target.com.au");
				v = cxApi.chooseVariation();
				dE.className += ' ctx-active-v'+v+' ctx-before';
				g[fn] = function() {
					if( v > 0 && typeof arguments[ v - 1 ] === 'function' ) {
						arguments[ v - 1 ].call(v);
					}
					dE.className = dE.className.replace('ctx-before', 'ctx-after');
				}
			} else {
				g[fn] = function(){};
			}			 
		}(window, document.documentElement, 'ctxPageExecute'));
		</script>

	</c:when>
	<c:otherwise>
		<%-- Stub, so as not to cause js errors, by unfortunate content authoring --%>
		<script>function ctxPageExecute(){}</script>
	</c:otherwise>
</c:choose>
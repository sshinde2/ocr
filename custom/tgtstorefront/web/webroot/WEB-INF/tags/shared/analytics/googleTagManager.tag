<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<%-- Google Tag Manager --%>
<c:if test="${not empty tagManagerContainer}">

	<c:if test="${not anonymousCachable}">
		<sec:authorize access="hasRole('ROLE_CUSTOMERGROUP')">
			<c:set var="dataLayerUser">
				<json:object escapeXml="false">
					<json:property name="event" value="uidLoad" />
					<util:jsonArrayUid />
				</json:object>
			</c:set>
			<c:set var="dataLayerUserPush">dataLayer.push(${dataLayerUser});</c:set>
		</sec:authorize>
	</c:if>
	<feature:enabled name="oracleRightNowLiveChat">
		<c:set var="liveChatProperty">
			dataLayer.push(<json:object escapeXml="false">
				<json:property name="event" value="liveChatEnabled" />
			</json:object>);
		</c:set>
	</feature:enabled>
	<feature:enabled name="oracleRightNowCoBrowse">
		<c:set var="coBrowseProperty">
			dataLayer.push(<json:object escapeXml="false">
				<json:property name="event" value="coBrowseEnabled" />
			</json:object>);
		</c:set>
	</feature:enabled>

	<c:if test="${not empty liveChatProperty or not empty coBrowseProperty}">
		<c:set var="dataLayerFunctionality">
			${liveChatProperty}
			${coBrowseProperty}
		</c:set>
	</c:if>

	<c:if test="${not empty pageType}">

		<c:set var="dataLayerPage">
			<json:object escapeXml="false">
				<json:property name="pageType" value="${pageType.value}" />
			</json:object>
		</c:set>

		<c:if test="${pageType.value == 'OrderConfirmation'}">

			<c:set var="dataLayerEcomm">
				<template:gtmEcommJson abstractOrderData="${orderData}" conversion="true" />
			</c:set>

		</c:if>

		<c:if test="${pageType.value == 'Checkout'}">

			<c:set var="dataLayer">
				<json:object escapeXml="false">
					<json:property name="tmid" value="${threatMatrixSessionID}" />
				</json:object>
			</c:set>

			<c:set var="dataLayerEcomm">
				<template:gtmEcommJson abstractOrderData="${cartData}" />
			</c:set>

		</c:if>

		<c:if test="${pageType.value == 'CheckoutLogin'}">
			<c:set var="dataLayerEcomm">
				<template:gtmEcommJson abstractOrderData="${cartData}" />
			</c:set>
		</c:if>

		<c:if test="${pageType.value == 'Cart'}">
			<c:set var="dataLayerEcomm">
				<template:gtmEcommJson abstractOrderData="${cartData}" />
			</c:set>
		</c:if>

		<c:if test="${not empty dataLayerEcomm}">
			<c:set var="dataLayerEcommPush">dataLayer.push(${dataLayerEcomm});</c:set>
		</c:if>

		<c:if test="${not empty dataLayer}">
			<c:set var="dataLayerPush">dataLayer.push(${dataLayer});</c:set>
		</c:if>

	</c:if>

	<script>var dataLayer=[${dataLayerPage}];${dataLayerPush}${dataLayerEcommPush}${dataLayerUserPush}${dataLayerFunctionality}</script>

	<c:if test="${param.gtmContainer ne 'off'}">
		<noscript>
			<iframe src="//www.googletagmanager.com/ns.html?id=${tagManagerContainer}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
		</noscript>
		<script>
		(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','${tagManagerContainer}');
		</script>
	</c:if>
</c:if>
<%-- End Google Tag Manager --%>

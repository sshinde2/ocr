<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="critical" required="false" type="java.lang.String" %>
<%@ attribute name="code" required="false" type="java.lang.String" %>
<%@ attribute name="basePath" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:if test="${not empty code}">
	<spring:theme code="${code}" text="" var="criticalTemp" />
	<c:set var="replaceString">${commonResourcePath}/</c:set>
	<c:set var="critical" value="${fn:replace(criticalTemp, replaceString, '')}" />
	<c:set var="fallbackCode" value="${code}" />
</c:if>


<%-- Primary /_ui/_assets/ based paths --%>
<c:if test="${not assetModeDevelop and featuresEnabled.featureUIAssetManifest}">
	<c:set var="resourceKey">ui_resource_${critical}</c:set>
	<c:set var="output">${assetResourcePath}/${requestScope[resourceKey]}</c:set>
</c:if>

<%-- Fallback to a spring:theme based path by code --%>
<c:if test="${empty output and not empty fallbackCode}">
	<spring:theme code="${fallbackCode}" text="" var="outputTemp" />
	<c:url value="${outputTemp}" var="output" />
</c:if>

<%-- Fallback to a path and cache bust it --%>
<c:set var="fallbackPath">${basePath}/build/${assetModeDevelop ? 'development' : 'production'}/${critical}</c:set>
<c:if test="${empty output}">
	<c:set var="output">${not disableCachebuster ? target:cacheBusterUrl(cacheBusterRewriteEnabled, cacheBusterTimestamp, fallbackPath) : fallbackPath}</c:set>
</c:if>


${output}

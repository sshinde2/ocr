<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="priceData" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="displayFreeForZero" required="false" type="java.lang.Boolean" %>
<%@ attribute name="isNegative" required="false" type="java.lang.Boolean" %>
<%@ attribute name="stripDecimalZeros" required="false" type="java.lang.Boolean" %>
<%@ attribute name="smallCurrencySign" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:if test="${not empty isNegative and isNegative}">
	<c:set var="pricePrefix" value="-${smallCurrencySign ? '' : '&nbsp;'}" />
</c:if>

<c:choose>
	<c:when test="${stripDecimalZeros}">
		<c:set var="formattedValue"><target:formatWholeNumberPrices value="${priceData.formattedValue}"/></c:set>
	</c:when>
	<c:otherwise>
		<c:set var="formattedValue" value="${priceData.formattedValue}" />
	</c:otherwise>
</c:choose>

<%--
 Tag to render a currency formatted price.
 Includes the currency symbol for the specific currency.
 * $XX.XX
 * $XX
 * -$XX.XX
 * -$XX
--%>

<c:if test="${smallCurrencySign}">
	<spring:theme code="text.currency" var="currencySign"/>
	<c:set var="smallSign"><span class="small-dollar">${pricePrefix}${currencySign}</span></c:set>

	<c:if test="${not empty pricePrefix}">
		<c:remove var="pricePrefix" />
	</c:if>

	<c:set var="formattedValue">${fn:replace( formattedValue, currencySign, smallSign )}</c:set>
</c:if>

<c:choose>
	<c:when test="${priceData.value > 0}">
		${pricePrefix}${formattedValue}
	</c:when>
	<c:otherwise>
		<c:if test="${displayFreeForZero}">
			<spring:theme code="text.free" text="FREE"/>
		</c:if>
		<c:if test="${not displayFreeForZero}">
			${pricePrefix}${formattedValue}
		</c:if>
	</c:otherwise>
</c:choose>

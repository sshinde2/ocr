<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="price" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:set var="splitPrice" value="${fn:split(price, '.')}" />
<c:set var="price">${splitPrice[0]}</c:set>
<c:if test="${fn:length(splitPrice) gt 0}">
	<c:set var="price">${splitPrice[0]}<span class="PricePoint-cents"><span class="visuallyhidden">.</span>${splitPrice[1]}</span></c:set>
</c:if>
${price}

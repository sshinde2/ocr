<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="price" required="false" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="minPrice" required="false" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="maxPrice" required="false" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="priceRange" required="false" type="de.hybris.platform.acceleratorfacades.order.data.PriceRangeData" %>
<%@ attribute name="priceKey" required="false" type="java.lang.String" %>
<%@ attribute name="smallCurrencySign" required="false" type="java.lang.Boolean" %>
<%@ attribute name="newStyle" required="false" type="java.lang.Boolean" %>

<%--
Tag used to render Price and Was Price
 * $XX.XX (Price Data)
 * $XX.XX - $XX.XX (Price Range Data)
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty priceKey}">
	<c:set var="priceKey" value="from" />
</c:if>

<%-- Special cases for endeca based prices --%>
<c:if test="${not empty minPrice and not empty maxPrice and minPrice.value.compareTo(maxPrice.value) == 0}">
	<c:set var="price" value="${minPrice}" />
</c:if>
<c:if test="${not empty minPrice and empty maxPrice}">
	<c:set var="price" value="${minPrice}" />
</c:if>

<spring:theme code="text.currency" var="currencySign"/>
<c:set var="smallSign"><span class="${newStyle ? 'PricePoint-currency--small' : 'small-dollar'}">${currencySign}</span></c:set>

<c:choose>
	<c:when test="${not empty priceRange and not empty priceRange.minPrice and not empty priceRange.maxPrice}">
		<c:set var="formattedFromPrice"><target:formatWholeNumberPrices value="${priceRange.minPrice.formattedValue}"/></c:set>
		<c:set var="formattedToPrice"><target:formatWholeNumberPrices value="${priceRange.maxPrice.formattedValue}"/></c:set>
		<c:if test="${newStyle}">
			<c:set var="formattedFromPrice"><format:priceCents price="${formattedFromPrice}"/></c:set>
			<c:set var="formattedToPrice"><format:priceCents price="${formattedToPrice}"/></c:set>
		</c:if>
		<c:if test="${smallCurrencySign}">
			<c:set var="formattedFromPrice">${fn:replace( formattedFromPrice, currencySign, smallSign )}</c:set>
			<c:set var="formattedToPrice">${fn:replace( formattedToPrice, currencySign, smallSign )}</c:set>
		</c:if>
		<spring:theme code="product.price.${priceKey}.range" arguments="${formattedFromPrice};${formattedToPrice}" argumentSeparator=";"/>
	</c:when>
	<c:when test="${not empty price}">
		<c:set var="formattedPrice"><target:formatWholeNumberPrices value="${price.formattedValue}"/></c:set>
		<c:if test="${newStyle}">
			<c:set var="formattedPrice"><format:priceCents price="${formattedPrice}"/></c:set>
		</c:if>
		<c:if test="${smallCurrencySign}">
			<c:set var="formattedPrice">${fn:replace( formattedPrice, currencySign, smallSign )}</c:set>
		</c:if>

		<spring:theme code="product.price.${priceKey}" arguments="${formattedPrice}" argumentSeparator=";"/>
	</c:when>
	<c:when test="${not empty minPrice and not empty maxPrice}">

		<c:set var="formattedFromPrice"><target:formatWholeNumberPrices value="${minPrice.formattedValue}"/></c:set>
		<c:set var="formattedToPrice"><target:formatWholeNumberPrices value="${maxPrice.formattedValue}"/></c:set>
		<c:if test="${newStyle}">
			<c:set var="formattedFromPrice"><format:priceCents price="${formattedFromPrice}"/></c:set>
			<c:set var="formattedToPrice"><format:priceCents price="${formattedToPrice}"/></c:set>
		</c:if>
		<c:if test="${smallCurrencySign}">
			<c:set var="formattedFromPrice">${fn:replace( formattedFromPrice, currencySign, smallSign )}</c:set>
			<c:set var="formattedToPrice">${fn:replace( formattedToPrice, currencySign, smallSign )}</c:set>
		</c:if>
		<spring:theme code="product.price.${priceKey}.range" arguments="${formattedFromPrice};${formattedToPrice}" argumentSeparator=";"/>
	</c:when>
</c:choose>

<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="price" required="false" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="priceRange" required="false" type="de.hybris.platform.acceleratorfacades.order.data.PriceRangeData" %>
<%@ attribute name="priceKey" required="false" type="java.lang.String" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>

<format:priceRange price="${price}" priceRange="${priceRange}" priceKey="was" />
<?xml version="1.0" encoding="UTF-8"?>
<taglib
		version="2.0"
		xmlns="http://java.sun.com/xml/ns/j2ee"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/javaee/web-jsptaglibrary_2_1.xsd">
	<tlib-version>1.0</tlib-version>
	<short-name>target</short-name>
	<uri>http://target.com.au/jsp/target</uri>
	
	<tag>
		<description>Body tag. Needed for live edit mode</description>
		<name>body</name>
		<tag-class>au.com.target.tgtstorefront.tags.TargetCMSBodyTag</tag-class>
		<body-content>JSP</body-content>
		<attribute>
			<name>headerOnly</name>
			<type>java.lang.Boolean</type>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>bodyCssClass</name>
			<type>java.lang.String</type>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
		<attribute>
			<name>itemType</name>
			<required>false</required>
			<type>java.lang.String</type>
			<rtexprvalue>true</rtexprvalue>
		</attribute>
	</tag>
	
    <tag>
        <description>MegaMenu render to be cached</description>
        <name>megaMenuCache</name>
        <tag-class>au.com.target.tgtstorefront.tags.CacheMegaMenuTag</tag-class>
        <body-content>JSP</body-content>
        <attribute>
			<name>cacheKey</name>
			<required>true</required>
			<type>java.lang.String</type>
			<rtexprvalue>false</rtexprvalue>
		</attribute>
    </tag>

	<tag>
		<description>Renders a hidden input tag with spring generated CSRF token.</description>
		<name>csrfInputToken</name>
		<tag-class>au.com.target.tgtstorefront.tags.CSRFInputTag</tag-class>
		<body-content>empty</body-content>
	</tag>
	
    <tag>
        <description>Strips cents from a price if it is a whole dollar amount.</description>
        <name>formatWholeNumberPrices</name>
        <tag-class>au.com.target.tgtstorefront.tags.PriceFormatTag</tag-class>
        <body-content>empty</body-content>
        <attribute>
            <name>value</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>
    
   	<tag>
		<description>Invalidate current session</description>
		<name>invalidateSession</name>
		<tag-class>au.com.target.tgtstorefront.tags.InvalidateSessionTag</tag-class>
		<body-content>empty</body-content>		 
	</tag>	 
    
	<function>
		<name>abbreviateString</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.String abbreviateString(java.lang.String, int)</function-signature>
	</function>

	<function>
		<name>abbreviateHTMLString</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.String abbreviateHTMLString(java.lang.String, int)</function-signature>
	</function>

	<function>
		<name>intToString</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.String intToString(int)</function-signature>
	</function>

	<function>
		<name>gridColumnsPerMegamenuColumn</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.String gridColumnsPerMegamenuColumn(int)</function-signature>
	</function>
	
	<function>
		<name>cacheBusterUrl</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.String cacheBusterUrl(boolean, java.lang.String, java.lang.String)</function-signature>
    </function>
    
    <function>
		<name>validUrlString</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.String validUrlString(java.lang.String)</function-signature>
    </function>

    <function>
		<name>validateNumberListToArray</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.List validateNumberListToArray(java.lang.String)</function-signature>
    </function>

    <function>
		<name>getUriComponents</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>org.springframework.web.util.UriComponents getUriComponents(java.lang.String)</function-signature>
    </function>
    
    <function>
		<name>splitEndecaUrlField</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.String splitEndecaUrlField(java.lang.String)</function-signature>
    </function>

	<function>
		<name>csrfTokenParameter</name>
		<function-class>au.com.target.tgtstorefront.tags.CSRFTokenTag</function-class>
		<function-signature>java.lang.String getTokenParameterName()</function-signature>
	</function>
	
	<function>
		<name>reverseArrayList</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.util.List reverseArrayList(java.util.ArrayList)</function-signature>
    </function>
    
    <function>
		<name>replaceAllNewLineWithBreak</name>
		<function-class>au.com.target.tgtstorefront.util.ElFunctions</function-class>
		<function-signature>java.lang.String replaceAllNewLineWithBreak(java.lang.String)</function-signature>
    </function>
	
	<function>
		<description>
			get the applied promotions for order entry number in a cart.
		</description>
		<name>getAppliedPromotionForEntry</name>
		<function-class>au.com.target.tgtstorefront.tags.Functions</function-class>
		<function-signature>java.util.List getAppliedPromotionForEntry(de.hybris.platform.commercefacades.order.data.AbstractOrderData,int)</function-signature>
	</function>
	
	<function>
  		<description>
   			get the potential promotion for the  order entry number in a cart.
  		</description>
  		<name>getPotentialPromotionForCartEntry</name>
  		<function-class>au.com.target.tgtstorefront.tags.Functions</function-class>
  		<function-signature>java.util.List getPotentialPromotionForCartEntry(de.hybris.platform.commercefacades.order.data.CartData,int)</function-signature>
 	</function>
 	
    <function>
        <description> 
            get the updateRecipientDetails form for edit
        </description>
        <name>getUpdatedRecipientForm</name>
        <function-class>au.com.target.tgtstorefront.tags.Functions</function-class>
        <function-signature>java.util.List getUpdatedRecipientForm(au.com.target.tgtfacades.order.data.GiftRecipientData)</function-signature>
    </function>
    
	
</taglib>
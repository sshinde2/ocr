<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:set var="trim" value="70" />

<%-- Set variables based on if there's a headline for the article; value will be used on images and anchor tags --%>

<c:if test="${article.headlineAvailable}">
	<c:set var="headline">${article.headline}</c:set>
	<c:set var="anchorTitle">title="${headline}"</c:set>
	<c:set var="alt">alt="${headline}"</c:set>
</c:if>

<div class="thumb hide-for-tiny">
	<c:choose>
		<c:when test="${article.validLink}">

			<%-- If it's a youtube link build up the class, rel and data attributes required --%>

			<c:if test="${not empty article.youTubeLink and article.youTubeLink eq true}">
				<c:set var="linkClass">class="lightbox"</c:set>
				<c:set var="dataAttr">data-lightbox-type="video"</c:set>
			</c:if>

			<%-- If it's an external link overwrite the rel attribute if it's an external link but not if youtube was selected --%>

			<c:if test="${article.external and article.youTubeLink ne true}">
				<c:set var="relAttr">rel="external"</c:set>
			</c:if>
			<a href="${article.imageLink}" ${anchorTitle} ${linkClass} ${relAttr} ${dataAttr}><img ${alt} data-screen-support="small large" data-src="${article.imageURL}" class="defer-img unveil-img" width="220" height"95" /></a>
		</c:when>
		<c:otherwise>
			<img ${alt} data-screen-support="small large" data-src="${article.imageURL}" class="defer-img unveil-img" width="220" height"95" />
		</c:otherwise>
	</c:choose>
</div>

<c:if test="${article.headlineAvailable or not empty article.content}">
	<div class="description">
		<c:if test="${article.headlineAvailable}">
			<h4>
				<c:choose>
					<c:when test="${article.validLink}">
						<a href="${article.imageLink}" ${anchorTitle} ${linkClass} ${relAttr} ${dataAttr}><c:out value="${headline}" /></a>
					</c:when>
					<c:otherwise>
						<c:out value="${headline}" />
					</c:otherwise>
				</c:choose>
			</h4>
		</c:if>
		<c:if test="${not empty article.content}">
			<p>
				<c:out value="${target:abbreviateString(article.content, trim)}" />
			</p>
		</c:if>

		<c:if test="${article.validLink}">

			<%-- Set link class and text for text link --%>

			<c:set var="linkClass">class="more"</c:set>
			<c:set var="linkText">Read More</c:set>

			<c:if test="${not empty article.youTubeLink and article.youTubeLink eq true}">

				<%-- Overwrite link class and text for text link if youtube video --%>

				<c:set var="linkClass">class="more lightbox"</c:set>
				<c:set var="linkText">View Video</c:set>
			</c:if>

			<a href="${article.imageLink}" ${anchorTitle} ${linkClass} ${relAttr} ${dataAttr}>${linkText}</a>
		</c:if>
	</div>
</c:if>

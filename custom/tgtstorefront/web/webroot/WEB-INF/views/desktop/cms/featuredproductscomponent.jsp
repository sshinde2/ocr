<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="visibleItems" value="${3}" />
<c:set var="variantClass" value="wide" />
<c:if test="${not empty contentSlotContext}">
	<c:set var="variantClass" value="${contentSlotContext}" />
</c:if>
<c:if test="${contentSlotContext eq 'full'}">
	<c:set var="visibleItems" value="${4}" />
</c:if>
<c:if test="${contentSlotContext eq 'narrow'}">
	<c:set var="visibleItems" value="${1}" />
</c:if>

<c:if test="${not empty productData and fn:length(productData) ge visibleItems}">

	<feature:enabled name="uiTrackDisplayOnly">
		<c:set var="trackDisplayOnly" value="${true}" />
	</feature:enabled>

	<div class="featured-products">
		<c:if test="${not empty component.title}">
		<comp:component-heading
			title="${component.title}"
			compLink="${component.link}"/>
		</c:if>
		<div class="featured-product-listing">
			<feature:enabled name="featureWishlistFavourites">
				<c:if test="${not kioskMode and not iosAppMode}">
					<c:set var="listClass">FavouritableProducts</c:set>
				</c:if>
			</feature:enabled>
			<ul class="${listClass} product-base-layout product-grid-layout product-grid-${contentSlotContext}" ${favouritesData}>
				<c:forEach items="${productData}" var="product" varStatus="status" end="${visibleItems - 1}">
					<c:if test="${status.count le visibleItems}">
						<product:productListerGridItem
							product="${product}"
							showBasketButton="true"
							cmsComponentId="list-${component.uid}"
							position="${status.count}"
							absoluteIndex="${status.count}"
							listerName="Featured"
							trackDisplayOnly="${trackDisplayOnly}"
							/>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>
</c:if>

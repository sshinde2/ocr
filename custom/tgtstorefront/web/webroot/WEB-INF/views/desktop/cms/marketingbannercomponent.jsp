<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="marketing" tagdir="/WEB-INF/tags/desktop/marketing" %>

<comp:banner banner="${component}" bannerType="marketing">
	<c:if test="${not empty component.exclusionsContent or not empty component.exclusionsLink}">
		<marketing:exclusions
			exclusionsContent="${component.exclusionsContent}"
			displayExclusionLabel="false"
			exclusionsLink="${component.exclusionsLink}" />
	</c:if>
	<c:if test="${not empty component.cmsLink and not component.hideButton}">
		<c:set var="cmsLinkAsButton" value="true" scope="request" />
		<cms:component component="${component.cmsLink}"/>
		<c:remove var="cmsLinkAsButton" scope="request" />
	</c:if>
</comp:banner>
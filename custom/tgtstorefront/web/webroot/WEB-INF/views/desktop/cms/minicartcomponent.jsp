<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="wrapperData">id="mini-cart" data-component-uid="${component.uid}" data-total-display="${totalDisplay}"</c:set>
<c:set var="cartUrl" value="${fullyQualifiedDomainName}${targetUrlsMapping.cart}" />
<spring:theme text="Go to basket" code="cart.goto" var="cartGoTo" />
<feature:disabled name="uiStickyHeader">
	<div class="cart-header" ${wrapperData}>
		<div class="cart-content minicart-trigger">
			<div class="summary">
				<a href="${cartUrl}" title="${cartGoTo}" class="mini-cart-summary">
					<c:if test="${not anonymousCachable}">
						<header:miniCartSummary />
					</c:if>
				</a>
			</div>
		</div>
	</div>
	<cart:rolloverCartPopup />
</feature:disabled>
<feature:enabled name="uiStickyHeader">
	<c:set var="linkClass" value="TargetHeader-icon TargetHeader-icon--cart minicart-trigger mini-cart-summary ${not empty totalItems and totalItems gt 0 ? ' is-filled' : ''} " />
	<div class="TargetHeader-child">
		<div class="TargetHeader-child is-cart" ${wrapperData}>
			<a href="${cartUrl}" title="${cartGoTo}" class="${linkClass}">
				<c:if test="${not anonymousCachable}">
					<header:miniCartSummary />
				</c:if>
			</a>
		</div>
		<cart:rolloverCartPopup />
	</div>
</feature:enabled>
<c:if test="${anonymousCachable}">
	<script>if(window.t_Inject){t_Inject('minicart');}</script>
</c:if>

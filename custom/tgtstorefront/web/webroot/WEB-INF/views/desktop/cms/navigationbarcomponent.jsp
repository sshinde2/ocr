<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="megamenu" tagdir="/WEB-INF/tags/desktop/nav/megamenu" %>
<%@ taglib prefix="globalNavigation" tagdir="/WEB-INF/tags/desktop/nav/globalNavigation" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="uiNewMegamenuDisplay">
	<globalNavigation:navigation />
</feature:enabled>
<feature:disabled name="uiNewMegamenuDisplay">
	<megamenu:navigation />
</feature:disabled>

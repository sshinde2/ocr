<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="component" tagdir="/WEB-INF/tags/shared/component" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>

<c:if test="${not empty navigationComponentMenuItem}">
	<c:url var="url" value="${navigationComponentMenuItem.link}" />
	<div class="nav-component">
		<c:if test="${component.showTitle}">
			<a href="${url}" class="Heading--h4" title="${navigationComponentMenuItem.name}">${navigationComponentMenuItem.name}</a>
		</c:if>
		<c:if test="${not hideSubNavigationNodes}" >
			<ul class="nav-level-1">
				<nav:navTree collection="${navigationComponentMenuItem.children}" />
			</ul>
		</c:if>
	</div>
</c:if>

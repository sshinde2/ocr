<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="leftRegionLayoutOne" value="${component.leftRegionLayout eq 'ONE'}"/>
<c:set var="rightRegionLayoutOne" value="${component.rightRegionLayout eq 'ONE'}" />

<c:set var="allLarge" value="${leftRegionLayoutOne and rightRegionLayoutOne}" />
<c:set var="allSmall" value="${not leftRegionLayoutOne and not rightRegionLayoutOne}" />
<c:set var="emptyRegion" value="${empty leftRegionSlots or empty rightRegionSlots}" />

<c:set var="cssClass" value="${allLarge or allSmall or emptyRegion ? 'eng-equi-size' : ''}" />

<c:set var="cmsComponentId" value="eng-${component.uid}" />

<c:set var="hasBackground" value="${not empty component.backgroundColour}" />

<c:if test="${hasBackground}">
	<c:set var="cssClass">${cssClass} engagement-padded</c:set>
	<c:set var="isPadded">-padded</c:set>
</c:if>

<c:if test="${not empty component.backgroundColour}">
	<c:set var="styleAttr">style="background-color:${fn:replace(component.backgroundColour, '"', '')};"</c:set>
</c:if>

<c:if test="${empty component.richDisclaimer}">
	<c:set var="cssClass">${cssClass} engagement${isPadded}-collapse-below</c:set>
</c:if>
<c:if test="${empty component.title and empty component.richDescription}">
	<c:set var="cssClass">${cssClass} engagement${isPadded}-collapse-above</c:set>
</c:if>

<div class="engagement ${cssClass}" ${styleAttr}>

	<c:if test="${not empty component.title}">
		<comp:component-heading
			title="${component.title}"
			compLink="${component.link}"/>
	</c:if>
	<c:if test="${not empty component.richDescription}">
		<div class="comp-desc">
			${component.richDescription}
		</div>
	</c:if>
	<feature:enabled name="featureWishlistFavourites">
		<c:if test="${not kioskMode and not iosAppMode}">
			<c:set var="listClass">FavouritableProducts</c:set>
		</c:if>
	</feature:enabled>
	<div class="eng-items ${listClass}" ${favouritesData}>

		<c:choose>
			<c:when test="${not leftRegionLayoutOne and rightRegionLayoutOne}">
				<comp:engagement
					list="${rightRegionSlots}"
					large="${rightRegionLayoutOne}"
					secondary="${true}"
					cmsComponentId="${cmsComponentId}"
					impressionPosition="1" />
				<comp:engagement
					list="${leftRegionSlots}"
					large="${leftRegionLayoutOne}"
					cmsComponentId="${cmsComponentId}"
					impressionPosition="5" />
			</c:when>
			<c:otherwise>
				<comp:engagement
					list="${leftRegionSlots}"
					large="${leftRegionLayoutOne}"
					cmsComponentId="${cmsComponentId}"
					impressionPosition="1" />

				<comp:engagement
					list="${rightRegionSlots}"
					large="${rightRegionLayoutOne}"
					cmsComponentId="${cmsComponentId}"
					impressionPosition="5" />
			</c:otherwise>
		</c:choose>
	</div>

	<c:if test="${not empty component.richDisclaimer}">
		<div class="comp-disclaimer">
			${component.richDisclaimer}
		</div>
	</c:if>
</div>

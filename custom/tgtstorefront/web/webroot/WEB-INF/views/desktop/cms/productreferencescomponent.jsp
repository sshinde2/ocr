<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<c:set var="isProductReferences" value="${not empty productReferences}" />
<c:set var="isListerProducts" value="${not empty productListerData}" />

<c:if test="${(isProductReferences or isListerProducts) and component.maximumNumberProducts > 0}">
	<product:productCarousel productReferences="${productReferences}" products="${productListerData}" width="${contentSlotContext}" title="${component.title}" listType="${isListerProducts ? 'Recom Related - Generated' : 'Recom Related'}" />
</c:if>

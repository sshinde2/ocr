<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<c:choose>
	<c:when test="${fn:length(banners) gt 1}">
		<%-- .slider for js and .banner-slider for css --%>
		<comp:carousel type="banner" itemCount="${fn:length(banners)}" carouselClassName="Slider--banner" perPage="1" hidePause="${false}">
			<jsp:attribute name="itemList">
				<c:forEach items="${banners}" var="banner" varStatus="status">
					<c:choose>
						<c:when test="${empty banner.media}">
							<li class="Slider-slideItem txt-slide">${banner.content}</li>
						</c:when>
						<c:otherwise>
							<li class="Slider-slideItem img-slide">
								<c:set var="statusForBannerComponent" value="${status}" scope="request" />
								<cms:component component="${banner}"/>
								<c:remove var="statusForBannerComponent" scope="request" />
							</li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</jsp:attribute>
		</comp:carousel>
	</c:when>
	<c:otherwise>
		<c:forEach items="${banners}" var="banner" varStatus="status">
			<cms:component component="${banner}"/>
		</c:forEach>
	</c:otherwise>
</c:choose>

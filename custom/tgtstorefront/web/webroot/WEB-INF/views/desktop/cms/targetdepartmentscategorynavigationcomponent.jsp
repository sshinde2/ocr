<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="megamenu" tagdir="/WEB-INF/tags/desktop/nav/megamenu" %>


<nav class="only-for-small">
	<div class="mm-Cat mm-Cat--accordian">
		<c:forEach items="${columns}" var="column" varStatus="columnStatus">
			<megamenu:column column="${column}"
				columnsStr="${target:gridColumnsPerMegamenuColumn(fn:length(columns))}"
				lastColumn="${columnStatus.last}" />
		</c:forEach>
	</div>
</nav>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>

<c:if test="${not empty requestURL and not kioskMode}">
	<div class="ExternalRecommendations" data-request-url="${requestURL}" data-request-index="${component.carouselIndex}">
		<util:loadingCarousel />
	</div>
</c:if>

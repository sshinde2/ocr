<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="stl" tagdir="/WEB-INF/tags/desktop/stl" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="title" value="${component.title}" />
<c:set var="callToAction">
	<c:choose>
		<c:when test="${not empty component.callToAction}">
			<c:out value="${component.callToAction}" />
		</c:when>
		<c:otherwise>
			<spring:theme code="text.carousel.productGroup.callToAction" />
		</c:otherwise>
	</c:choose>
</c:set>
<feature:enabled name="featureStepShopTheLook">
	<c:if test="${not empty looks}">
		<c:set var="staticLink"><a href='${lookCollection.url}'>See All ${lookCollection.name} &gt;</a></c:set>
		<stl:lookCarousel looks="${looks}" title="${component.showTitle ? title : ''}" unveil="${true}" deferred="${true}" width="${contentSlotContext}" staticLink="${staticLink}" callToAction="${callToAction}" />
	</c:if>
</feature:enabled>
<feature:disabled name="featureStepShopTheLook">
	<c:if test="${not empty groupPageModels}">
		<c:set var="staticLink"><a href='${collectionPageModel.label}'>See All ${collectionPageModel.title} &gt;</a></c:set>
		<product:productGroupCarousel products="${groupPageModels}" title="${component.showTitle ? title : ''}" unveil="${true}" deferred="${true}" width="${contentSlotContext}" staticLink="${staticLink}" callToAction="${callToAction}" />
	</c:if>
</feature:disabled>

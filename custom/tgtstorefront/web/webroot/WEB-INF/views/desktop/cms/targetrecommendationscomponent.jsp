<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>

<c:if test="${(not empty recommendedProducts) and (fn:length(recommendedProducts) gt 0)
	and (not empty recommendedProducts[0].records) and (fn:length(recommendedProducts[0].records) gt 0)}">
 	<product:recommendationCarousel title="${component.title}" recommendations="${recommendedProducts[0].records}" width="${contentSlotContext}" unveil="true" listType="Recom BoughtBought ${pageType eq 'Cart'? 'Cart' : ''}" />
</c:if>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>
<%@ taglib prefix="marketing" tagdir="/WEB-INF/tags/desktop/marketing" %>
<%@ taglib prefix="comp" tagdir="/WEB-INF/tags/desktop/common/components" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<c:set var="title" value="${not empty component.headline ? component.headline : component.media.altText}" />
<c:set var="ratio" value="${component.desktopHeight / component.desktopWidth}" />
<c:set var="theme" value="${fn:toLowerCase(component.theme)}" />
<c:set var="hasVideo" value="${not empty component.videoUrl and not empty component.videoType}" />

<%-- handle lazyloading media --%>
<c:choose>
	<c:when test="${component.disableLazyLoading}">
		<c:set var="srcset" value="srcset" />
		<c:set var="src" value="src" />
		<c:set var="mediaSmall" value="(max-width: 768px)" />
		<c:set var="mediaLarge" value="(min-width: 768px)" />
	</c:when>
	<c:otherwise>
		<c:set var="srcset" value="data-srcset" />
		<c:set var="src" value="data-src" />
		<c:set var="mediaSmall" value="screenSmall" />
		<c:set var="mediaLarge" value="screenLarge" />
	</c:otherwise>
</c:choose>

<%-- anchor tags only when needed --%>
<c:set var="contentContainer">||</c:set>
<c:if test="${not empty component.button1Link}">
	<c:set var="contentContainer"><a href="${component.button1Link}" title="${title}" tabindex="-1">||</a></c:set>
</c:if>

<c:set var="wrapper" value="${fn:split(contentContainer, '||')}" />
<c:set var="pictureClass" value="${not component.disableLazyLoading ? 'unveil-img' : 'is-loaded'}" />
<div class="SmartHeroBanner banner ${(component.disableLazyLoading or empty component.media.url) ? ' is-disableLazyLoading is-loaded ' : ''} ${component.uid}">
	<c:if test="${not empty component.media.url}">
		<div class="SmartHeroBanner-container">
			<c:out value="${wrapper[0]}" escapeXml="false" />
				<picture>
					<c:set var="desktopMedia" value="${mediaLarge}" />
					<c:if test="${empty component.mobileImage.url}">
						<c:set var="desktopMedia" value="all" />
					</c:if>
					<source media="${desktopMedia}" ${srcset}="${component.media.url}">
					<c:if test="${not empty component.mobileImage.url}">
						<source media="${mediaSmall}" ${srcset}="${component.mobileImage.url}">
					</c:if>
					<img class="SmartHeroBanner-picture ${pictureClass} ${hasVideo ? ' only-for-small' : ''}" alt="${title}" title="${title}" />
				</picture>
				<c:if test="${hasVideo}">
					<div class="SmartHeroBanner-picture ${pictureClass} hide-for-small">
						<video
							${component.disableLazyLoading ? 'autoplay=""' : ''}
							muted="" loop="" controls="" poster="${component.media.url}"
							class="SmartHeroBanner-video ${component.disableLazyLoading ? '' : ' unveil-img'}"
						>
							<source ${src}="${component.videoUrl}" type="video/${fn:toLowerCase(component.videoType)}" />
							<c:if test="${not empty component.altVideoUrl and not empty component.altVideoType}">
								<source ${src}="${component.altVideoUrl}" type="video/${fn:toLowerCase(component.altVideoType)}" />
							</c:if>
							<c:if test="${not empty component.alt2VideoUrl and not empty component.alt2VideoType}">
								<source ${src}="${component.alt2VideoUrl}" type="video/${fn:toLowerCase(component.alt2VideoType)}" />
							</c:if>
						</video>
					</div>
				</c:if>
			<c:out value="${wrapper[1]}" escapeXml="false" />
			<c:if test="${hasVideo}">
				<button type="button" class="SmartHeroBanner-videoControl is-playing hide-for-small">
					<span class="visuallyhidden"><spring:theme code="component.smartHeroBanner.toggleVideo" /></span>
				</button>
			</c:if>
			<div class="SmartHeroBanner-overlay">
				<c:if test="${not empty component.headline}">
					<h2 class="SmartHeroBanner-heading SmartHeroBanner-heading--${theme}">
						${component.headline}
					</h2>
				</c:if>
				<c:if test="${not empty component.subtitle}">
					<h3 class="SmartHeroBanner-subtitle SmartHeroBanner-subtitle--${theme}">
						${component.subtitle}
					</h3>
				</c:if>
				<c:if test="${not empty button1Link && not empty button1Title}">
					<div class="SmartHeroBanner-callToActionContainer">
						<div class="SmartHeroBanner-callToAction">
							<comp:link className="SmartHeroBanner-button SmartHeroBanner-button--${theme}" linkUrl="${button1Link}" name="${button1Title}"
							target="${component.button1Target}">
								<spring:theme code="link.withArrow" arguments="${button1Title}" />
							</comp:link>
							<c:if test="${not empty button2Link && not empty button2Title}">
								<comp:link className="SmartHeroBanner-button SmartHeroBanner-button--${theme}" 	linkUrl="${button2Link}"  name="${button2Title}"
								target="${component.button2Target}">
									<spring:theme code="link.withArrow" arguments="${button2Title}" />
								</comp:link>
							</c:if>
						</div>
					</div>
				</c:if>
			</div>
			<c:if test="${not empty component.exclusionsContent or not empty component.exclusionsLink}">
				<marketing:exclusions
				exclusionsContent="${component.exclusionsContent}"
				exclusionsLabel="${component.exclusionsLabel}"
				displayExclusionLabel="true"
				exclusionsLink="${component.exclusionsLink}" />
			</c:if>
		</div>
	</c:if>
	<c:if test="${not empty component.navigation1Link and not empty component.navigation1Title}">
		<div class="FlexiNavigationController">
			<comp:link className="FlexiNavigationController-child" linkUrl="${component.navigation1Link}"
			name="${component.navigation1Title}" target="${component.navigation1Target}">
				<spring:theme code="link.withArrow" arguments="${component.navigation1Title}" />
			</comp:link>
			<c:if test="${not empty component.navigation2Link and not empty component.navigation2Title}">
				<comp:link className="FlexiNavigationController-child" linkUrl="${component.navigation2Link}"
				name="${component.navigation2Title}" target="${component.navigation2Target}">
					<spring:theme code="link.withArrow" arguments="${component.navigation2Title}" />
				</comp:link>
			</c:if>
			<c:if test="${not empty component.navigation3Link and not empty component.navigation3Title}">
				<comp:link className="FlexiNavigationController-child" linkUrl="${component.navigation3Link}"
				name="${component.navigation3Title}" target="${component.navigation3Target}">
					<spring:theme code="link.withArrow" arguments="${component.navigation3Title}" />
				</comp:link>
			</c:if>
			<c:if test="${not empty component.navigation4Link and not empty component.navigation4Title}">
				<comp:link className="FlexiNavigationController-child" linkUrl="${component.navigation4Link}"
				name="${component.navigation4Title}" target="${component.navigation4Target}">
					<spring:theme code="link.withArrow" arguments="${component.navigation4Title}" />
				</comp:link>
			</c:if>
		</div>
	</c:if>
</div>

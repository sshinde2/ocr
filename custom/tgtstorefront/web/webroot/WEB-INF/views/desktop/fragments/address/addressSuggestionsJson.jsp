<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:theme code="address.autosuggestion.manual" var="manualEntry" />
<c:set var="errorMessage"><spring:theme code="icon.cross" />&nbsp;<spring:theme code="address.autosuggestion.error" /></c:set>

<json:array name="items">

	<c:choose>
		<c:when test="${not empty addressSuggestionResult and not empty addressSuggestionResult.suggestions and fn:length(addressSuggestionResult.suggestions) gt '0'}">
			<c:forEach items="${addressSuggestionResult.suggestions}" var="autoSuggest">
				<json:object>
					<json:property name="label" value="${autoSuggest.name}" />
					<json:property name="name" value="${autoSuggest.name}" />
					<json:property name="code" value="${autoSuggest.code}" />
				</json:object>
			</c:forEach>
		</c:when>
		<c:when test="${addressSuggestionResult.status ne 'TooManyMatchesFound'}">
			<json:object>
				<json:property name="name" value="${errorMessage}" escapeXml="false" />
				<json:property name="code" value="error"/>
			</json:object>
		</c:when>
	</c:choose>
	<c:if test="${addressSuggestionResult.status ne 'TooManyMatchesFound'}">
		<json:object>
			<json:property name="name" value="${manualEntry}" escapeXml="false" />
			<json:property name="code" value="manual"/>
		</json:object>
	</c:if>

</json:array>

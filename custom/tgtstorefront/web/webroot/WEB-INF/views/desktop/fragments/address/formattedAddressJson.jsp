<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<json:object>
	<c:if test="${not empty formattedAddress}">
		<json:property name="addressLine1" value="${formattedAddress.addressLine1}" />
		<json:property name="addressLine2" value="${formattedAddress.addressLine2}" />
		<json:property name="city" value="${formattedAddress.city}" />
		<json:property name="state" value="${formattedAddress.state}" />
		<json:property name="postcode" value="${formattedAddress.postcode}" />
	</c:if>
</json:object>
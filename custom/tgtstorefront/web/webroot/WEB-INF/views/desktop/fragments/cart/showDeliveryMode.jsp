<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<json:object>
	<json:property name="success" value="${empty errorMsg}"/>
	<c:if test="${not empty errorMsg}">
		<json:property name="errorMsg" escapeXml="false">
			<spring:theme code="${errorMsg}" text="${errorMsg}"/>
		</json:property>
	</c:if>
	<c:if test="${empty errorMsg}">
		<json:property name="catchmentArea" value="${catchmentArea}"/>
		<json:property name="deliveryModeChoiceHtml" escapeXml="false">
			<cart:deliveryModeChoice cartData="${cartData}" deliveryPostcode="${deliveryPostcode}" wrapDiv="false"/>
		</json:property>
		<json:property name="deliveryModePostcodeChangeHtml" escapeXml="false">
			<cart:deliveryModePostcodeChange deliveryPostcode="${deliveryPostcode}"/>
		</json:property>
	</c:if>
</json:object>

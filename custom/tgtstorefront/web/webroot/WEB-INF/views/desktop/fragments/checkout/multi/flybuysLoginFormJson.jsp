<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<json:object>
	<json:property name="success" value="${success}"/>
	<c:if test="${not success}">
		<json:property name="formHtml" escapeXml="false">
			<multi-checkout:flybuysLogin />
		</json:property>
		<c:if test="${not empty flybuysRequestStatusData}">
			<json:property name="errorMessage" escapeXml="false">
				<feedback:message type="error" size="small" icon="false">
					<p>${flybuysRequestStatusData.errorMessage}</p>
				</feedback:message>
			</json:property>
		</c:if>
	</c:if>
</json:object>
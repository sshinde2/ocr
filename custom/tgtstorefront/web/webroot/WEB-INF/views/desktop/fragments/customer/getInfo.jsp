<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="util" tagdir="/WEB-INF/tags/desktop/util" %>
<%@ taglib prefix="kioskNav" tagdir="/WEB-INF/tags/desktop/kiosk/navigation" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>


<sec:authorize access="hasRole('ROLE_CUSTOMERGROUP')"><c:set var="isAuthenticated" value="${true}" /></sec:authorize>

<%-- Customer Bundle --%>
<json:object>

	<json:object name="authentication">
		<json:property name="knownUser" value="${!!isAuthenticated}" />
	</json:object>

	<c:if test="${isAuthenticated}">
		<json:object name="dataLayer" escapeXml="false">
			<util:jsonArrayUid />
		</json:object>
	</c:if>

	<%-- Minicart data --%>
	<json:object name="minicart">
		<json:property name="fragName" value="totalHtml" />
		<json:property name="totalHtml" escapeXml="false">
			<header:miniCartSummary />
		</json:property>
	</json:object>

	<%-- Account data --%>
	<json:object name="account">
		<json:property name="fragName" value="topbarHtml" />
		<json:property name="topbarHtml" escapeXml="false">
			<feature:enabled name="uiStickyHeader">
				<header:myAccountIcons />
			</feature:enabled>
			<feature:disabled name="uiStickyHeader">
				<header:myAccount />
			</feature:disabled>
		</json:property>
	</json:object>

	<%-- Global Messages --%>
	<json:object name="globalMessages">
		<json:property name="feedbackHtml" escapeXml="false">
			<common:globalMessages/>
		</json:property>
	</json:object>

	<%-- Product Data only used for PDP --%>
	<json:object name="productDetails">
		<json:property name="url" value="${product.url}" />
		<json:property name="code" value="${product.code}" />
		<json:object name="fragments">
			<json:property name="dealsHtml" escapeXml="false">
				<product:productPotentialDeals promotions="${product.potentialPromotions}" dealDescription="${product.dealDescription}" />
			</json:property>
		</json:object>
	</json:object>

	<feature:enabled name="featureConsolidatedStoreStockVisibility">
		<%-- Store Details for PDP ,store view --%>
		<json:object name="storeDetails">
			<json:property name="fragName" value="storeDetailsHtml" />
			<feature:enabled name="uiReactPDPDetailPanel">
				<json:object name="store">
					<json:property name="number" value="${store.storeNumber}" />
					<json:property name="name" value="${store.name}" escapeXml="false" />
					<json:property name="address" value="${store.targetAddressData.line1}" escapeXml="false" />
					<json:property name="postalCode" value="${store.targetAddressData.postalCode}" />
					<json:property name="town" value="${store.targetAddressData.town}" escapeXml="false" />
					<json:property name="state" value="${store.targetAddressData.state}" escapeXml="false" />
					<json:property name="phone" value="${store.targetAddressData.phone}" />
					<json:property name="directionUrl" value="${store.directionUrl}" escapeXml="false" />
				</json:object>
			</feature:enabled>
			<feature:disabled name="uiReactPDPDetailPanel">
				<json:property name="storeDetailsHtml" escapeXml="false">
					<c:if test="${not empty store}">
						<product:storeDetails
							storeNumber="${store.storeNumber}"
							storeAddress="${store.targetAddressData.line1}"
							storeName="${store.name}"
							postalCode="${store.targetAddressData.postalCode}"
							town="${store.targetAddressData.town}"
							state="${store.targetAddressData.state}"
							phone="${store.targetAddressData.phone}"
							directionUrl="${store.directionUrl}"/>
					</c:if>
				</json:property>
			</feature:disabled>
		</json:object>

		<%-- Store Name for PDP  --%>
		<json:object name="preferredStoreName">
			<json:property name="fragName" value="preferredStoreNameHtml" />
			<json:property name="preferredStoreNameHtml" value="${store.name}" />
		</json:object>
	</feature:enabled>

	<c:if test="${kioskMode}">
		<%-- Kiosk Sign Out link --%>
		<json:object name="kioskSignout">
			<json:property name="fragName" value="kioskSignoutHtml" />
			<json:property name="kioskSignoutHtml" escapeXml="false">
				<sec:authorize access="hasRole('ROLE_CUSTOMERGROUP')">
					<kioskNav:signOutTile />
				</sec:authorize>
			</json:property>
		</json:object>
	</c:if>

</json:object>


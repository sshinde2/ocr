<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<json:object>
    <json:property name="subscriptionType" value="${subscriptionType}"/>
	<json:property name="messageType" value="${messageType}"/>
	<json:property name="message" escapeXml="${false}">
		<feedback:message type="${messageType}" size="small">
			<p>${message}</p>
		</feedback:message>
	</json:property>
</json:object>

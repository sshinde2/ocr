<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<template:pageModal pageTitle="${pageTitle}">

	<jsp:body>
		<div class="main enews-quick-popup">
			<common:enewsQuick source="modal" customerSubscriptionUrl="/enews/quick">
				<jsp:attribute name="cmsEnewsQuick">
					<cms:slot var="component" contentSlot="${slots.Main}">
						<cms:component component="${component}"/>
					</cms:slot>
				</jsp:attribute>
				<jsp:attribute name="afterForm">
					<button type="button" class="button-fwd close-lightbox"><spring:theme code='enews.quick.close'/><spring:theme code="icon.right-arrow-small" /></button>
				</jsp:attribute>
			</common:enewsQuick>
		</div>
		<a href="javascript:history.back();">Back</a>
	</jsp:body>

</template:pageModal>
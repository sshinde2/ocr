<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="priceCheckFailReason" value="${posProduct.failureReason}" />

<c:set var="hasImage" value="${(not empty product) and (not empty ycommerce:productImage(product, 'zoom'))}" />

<c:choose>
	<c:when test="${empty priceCheckFailReason && not empty posProduct}">
		<%-- Haven't used prod-detail because it's associated with the ProductDetailView which we don't want here. --%>
		<div class="pricechk-detail ${ not hasImage ? 'no-img' : ''}">
			<div class="main">
				<c:if test="${hasImage}">
					<product:productDetailsImages product="${product}" galleryImages="${galleryImages}" hidePromoStatus="true" />
				</c:if>
				<kiosk:productPriceCheckDetailsPanel product="${product}" posProduct="${posProduct}"/>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div class="pricechk-missing">
			<h2><spring:theme code="kiosk.product.pricecheck.unavailable"/></h2>
			<c:if test="${not empty priceCheckFailReason}">
				<p>${priceCheckFailReason}</p>
			</c:if>
		</div>
	</c:otherwise>
</c:choose>

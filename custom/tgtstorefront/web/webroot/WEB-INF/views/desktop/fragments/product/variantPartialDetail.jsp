<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="carouselType" value="cloudzoom" />
<feature:enabled name="uiReactPDPCarousel">
	<c:set var="carouselType" value="react" />
</feature:enabled>

<c:set var="ecProductJson"><product:productDetailAnalytics /></c:set>
<div>
	<div class="prod-detail is-${carouselType} has-detail-panel" ${ecProductJson} data-colour-variant-code="${product.colourVariantCode}" data-base-prod-code="${product.baseProductCode}">
		<div class="main">
			<product:productDetailsImages product="${product}" galleryImages="${galleryImages}"/>
			<product:productDetailsPanel product="${product}"/>
			<product:productPageAccordion product="${product}" type="checkbox" />
			<product:productPageTabs product="${product}" tabClass="prod-tabs" tabContentClass="tab-content"/>
			<product:productViewLink product="${product}" />
		</div>
	</div>
</div>

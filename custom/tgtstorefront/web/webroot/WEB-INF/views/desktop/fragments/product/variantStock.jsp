<%@ page trimDirectiveWhitespaces="true" contentType="application/json"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<product:productStockLevelJson variants="${variantsStock}" availabilityTime="${availabilityTime}" />

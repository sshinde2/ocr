<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="featureWishListFavouriteVariants">
	<c:set var="favVariants" value="${true}" />
</feature:enabled>

<feature:enabled name="uiTrackDisplayOnly">
	<c:set var="trackDisplayOnly" value="${true}" />
</feature:enabled>

<c:if test="${not empty productData}">
	<c:set var="listHtml">
		<c:forEach items="${productData}" var="product" varStatus="status">
			<product:productListerItem
				product="${product}"
				cssClass="FavProduct ${favVariants ? 'reduced-detail' : ''}"
				position="${status.index}"
				listerType="grid"
				favControls="${true}"
				favVariants="${favVariants}"
				listerName="Favourites"
				hideDeals="${true}"
				unveilStartPosition="4"
				showInStock="${true}"
				hideRatings="${true}"
				trackDisplayOnly="${trackDisplayOnly}" />
		</c:forEach>
	</c:set>
</c:if>

<json:object>
	<json:array name="baseCodes">
		<c:forEach items="${productData}" var="product">
			<json:property name="productCode" value="${product.baseProductCode}" />
		</c:forEach>
	</json:array>
	<c:if test="${not empty customerProductData}">
		<json:array name="products">
			<c:forEach items="${customerProductData}" var="product">
				<json:object>
					<json:property name="pc" value="${product.selectedVariantCode}" />
					<json:property name="bc" value="${product.baseProductCode}" />
					<json:property name="t" value="${product.timeStamp}" />
				</json:object>
			</c:forEach>
		</json:array>
	</c:if>
	<c:if test="${not empty listHtml}">
		<json:property name="listHtml" escapeXml="false">
				${listHtml}
		</json:property>
	</c:if>
</json:object>

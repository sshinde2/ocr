<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="success" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>

<json:object>
	<json:property name="success" value="${success}"/>
	<c:choose>
		<c:when test="${success}">
			<json:property name="successHtml" escapeXml="false">
				<feedback:message type="success" size="small">
					<p><spring:theme code="favourites.share.confirmation.success" arguments="${recipientEmailAddress}" /></p>
				</feedback:message>
				<button class="FavShareForm-done"><spring:theme code="favourites.share.confirmation.done" /></button>
				<button class="FavShareForm-another"><spring:theme code="favourites.share.confirmation.sendAnother" /></button>
			</json:property>
		</c:when>
		<c:otherwise>
			<c:if test="${integrationError}">
				<json:property name="errorMessage" escapeXml="false">
					<feedback:message type="error" size="small">
						<p><spring:theme code="favourites.share.form.error" /></p>
					</feedback:message>
				</json:property>
			</c:if>
			<json:property name="formHtml" escapeXml="false">
				<user:favouritesShare
					shareLimit="${shareLimit}" />
			</json:property>
		</c:otherwise>
	</c:choose>
</json:object>

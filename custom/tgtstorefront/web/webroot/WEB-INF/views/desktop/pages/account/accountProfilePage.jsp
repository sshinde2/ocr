<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>

<account:page pageTitle="${pageTitle}" breadcrumbs="${breadcrumbs}">
	<h1 class="heading hfma"><spring:theme code="text.account.profile.heading"/></h1>
	<account:userDetails />
	<ul class="profile-updates">
		<c:url value="${targetUrlsMapping.myAccountUpdatePassword}" var="encodedUrl" />
		<li><a href="${encodedUrl}"><spring:theme code="text.account.profile.changePassword" /></a></li>
		<c:url value="${targetUrlsMapping.myAccountUpdatePersonalDetails}" var="encodedUrl" />
		<li><a href="${encodedUrl}"><spring:theme code="text.account.profile.changePersonalDetails" /></a></li>
		<c:url value="${targetUrlsMapping.myAccountUpdateEmail}" var="encodedUrl" />
		<li><a href="${encodedUrl}"><spring:theme code="text.account.profile.changeEmail" /></a></li>
	</ul>
</account:page>
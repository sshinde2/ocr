<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="ycommerce" uri="/WEB-INF/tld/ycommercetags.tld" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>

<c:set var="emptyBasket" value="${fn:length(cartData.entries) eq 0}" />
<c:set var="unitCount" value="${not empty cartData ? cartData.totalUnitCount : '0'}" />

<spring:theme text="Shopping Basket" var="title" code="cart.page.title"/>

<template:page pageTitle="${pageTitle}">

	<div class="inner-content">
		<common:globalMessages/>
		<cart:sohMessage cartMode="multicart" />
		<div class="main">

			<c:if test="${not emptyBasket}">
				<c:if test="${not hideIncentiveMessage}">
					<cart:deliveryIncentiveMessages />
				</c:if>
				<h2 class="cart-heading">
					<a href="${targetUrlsMapping.cartCheckout}" title="Proceed to Checkout"><spring:theme code="cart.page.tab.buynow" /></a>
				</h2>
			</c:if>

			<c:choose>
				<c:when test="${not emptyBasket}">
					<cart:cartPageTab cartData="${cartData}" />
				</c:when>
				<c:otherwise>
					<div class="basket-empty">
						<p><spring:theme code="cart.page.empty" /></p>

						<cms:slot var="emptyCart" contentSlot="${slots['EmptyCart']}">
							<cms:component component="${emptyCart}"/>
						</cms:slot>

						<p class="hfma">
							<br />
							<a class="button-norm" href="${targetUrlsMapping.cartContinueShopping}">
								<spring:theme code="icon.left-arrow-small" /><span><spring:theme code="cart.page.continue" /></span>
							</a>
						</p>
					</div>
				</c:otherwise>
			</c:choose>

		</div>

		<div class="supplement">
			<cms:slot var="supplement" contentSlot="${slots['Supplement']}">
				<cms:component component="${supplement}"/>
			</cms:slot>
		</div>

		<cart:cartModificationAnalytics />

	</div>
</template:page>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/desktop/common/layout" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="category" tagdir="/WEB-INF/tags/desktop/category" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="firstPage" value="${searchPageData.pagination.currentPage eq 0}" />
<c:set var="featureNewSubCategoriesUx" value="${false}" />
<feature:enabled name="newSubCategoriesUx">
	<c:set var="featureNewSubCategoriesUx" value="${true}" />
</feature:enabled>

<layout:productListingPage listerName="${pageType}" listerType="grid" heading="${categoryName}" unveilStartPosition="6">
	<jsp:attribute name="championSlot">
		<category:categoryListerChampionSlot slotName="ListerChampion" searchPageData="${searchPageData}" />
	</jsp:attribute>
	<jsp:attribute name="mainAbove">
		<category:categoryListerHead extraClass="${featureNewSubCategoriesUx ? 'hide-for-small' : ''}" />
		<c:if test="${firstPage and empty brandSearchNavigation}">
			<category:categoryListerSlot slotName="Main" />
		</c:if>
		<category:categoryBanner banner="${categoryBanner}" heading="${categoryName}" />
	</jsp:attribute>

	<jsp:attribute name="mainBelow">
		<c:if test="${firstPage and empty brandSearchNavigation}">
			<category:categoryListerSlot slotName="MainBottom" />
		</c:if>
	</jsp:attribute>

	<jsp:attribute name="supplementAbove">
		<category:categoryListerMeta searchPageData="${searchPageData}" />
		<c:if test="${not empty hotProducts}">
			<spring:theme code="search.page.hotItemsTitle" var="carouselTitle" />
			<product:productCarousel products="${hotProducts}" width="full" title="${carouselTitle}" />
		</c:if>
	</jsp:attribute>

</layout:productListingPage>

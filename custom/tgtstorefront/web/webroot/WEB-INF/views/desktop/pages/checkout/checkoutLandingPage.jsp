<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<template:page pageTitle="${pageTitle}">

	<common:globalMessages/>

	<div class="main">

		<ul class="contained login-landing">

			<li class="log-option">

				<%-- Login --%>

				<div class="log-container">

					<div class="log-account" data-ec-option="account">

						<cms:slot var="component" contentSlot="${slots.LoginLanding}">
							<cms:component component="${component}"/>
						</cms:slot>
						<c:url value="/checkout/j_spring_security_check" var="loginAndCheckoutActionUrl" />
						<user:login action="${loginAndCheckoutActionUrl}" />

					</div>

					<%-- Register --%>
					<c:if test="${featuresEnabled.featureCheckoutAllowGuest}">
						<div class="log-register log-register-openable" data-ec-option="account">
							<c:url value="/checkout/login/register" var="registerAndCheckoutActionUrl" />
							<user:register openablePanel="true" action="${registerAndCheckoutActionUrl}"/>
						</div>
					</c:if>

				</div>

			</li>

			<li class="log-option">

				<%-- Guest --%>
				<c:if test="${featuresEnabled.featureCheckoutAllowGuest}">
					<sec:authorize access="!isRememberMe()">
						<c:if test="${not empty cartData and cartData.allowGuestCheckout}">
							<c:url value="/checkout/login/guest" var="loginAndCheckoutActionUrl" />
							<user:anonymous action="${loginAndCheckoutActionUrl}" />
						</c:if>
					</sec:authorize>
				</c:if>

				<%-- Register --%>
				<c:if test="${not featuresEnabled.featureCheckoutAllowGuest}">
					<div class="log-container">
						<div class="log-register" data-ec-option="account">
							<c:url value="/checkout/login/register" var="registerAndCheckoutActionUrl" />
							<user:register action="${registerAndCheckoutActionUrl}"/>
						</div>
					</div>
				</c:if>


			</li>


		</ul>

		<div class="supplement">
			<cms:slot var="component" contentSlot="${slots.Supplement}">
				<cms:component component="${component}"/>
			</cms:slot>
		</div>

	</div>

</template:page>

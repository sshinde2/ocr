<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>

<c:if test="${not empty placeIpgOrderUrl}">
	<c:url var="placeIpgOrderUrl" value="${fullyQualifiedDomainName}${placeIpgOrderUrl}" />
</c:if>

<spring:theme code="checkout.multi.holdingpage.title" var="pageTitle" text="Please wait..." />
<template:pageModal pageTitle="${pageTitle}">

	<c:if test="${empty placeIpgOrderUrl}">
		<meta http-equiv="refresh" content="5">
	</c:if>
	<div class="main">
		<h5 class="co-heading-loader"><spring:theme code="checkout.multi.holdingpage.heading"/></h5>
		<c:if test="${empty placeIpgOrderUrl}">
			<p><spring:theme code="checkout.multi.holdingpage.automatically"/><a href="${targetUrlsMapping.placeOrderHolding}"><spring:theme code="checkout.multi.holdingpage.refresh"/></a>.</p>
		</c:if>
		<c:if test="${not empty placeIpgOrderUrl}">
			<form name="placeIpgOrderForm" method="POST" action="${placeIpgOrderUrl}">
				<input type="hidden" name="${ipgTokenName}" value="${ipgTokenValue}" /> 
				<input type="hidden" name="${ipgSessionName}" value="${ipgSessionValue}" />
				<target:csrfInputToken/>
			</form>
			<script>
				window.onload = function() {
					window.document.placeIpgOrderForm.submit();
				}
			</script>
		</c:if>	
	</div>
</template:pageModal>
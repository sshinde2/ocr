<%@ page trimDirectiveWhitespaces="true" contentType="application/json" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>

<json:object>
	<json:property name="success" value="true"/>
	<json:property name="cartSummaryHtml" escapeXml="false">
		<cart:cartTotals showEntries="true" cartData="${cartData}"/>
	</json:property>
	<json:object name="cartData">
		<json:property name="paymentTotalHtml" escapeXml="false">
			<multi-checkout:payment-total />
		</json:property>
	</json:object>
	<json:property name="redeemSummaryHtml" escapeXml="false">
		<multi-checkout:flybuysRedeemSummary redeemOptions="${redeemOptions}" />
	</json:property>
</json:object>
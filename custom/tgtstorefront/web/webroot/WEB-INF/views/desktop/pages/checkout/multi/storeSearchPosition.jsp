<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<json:object>
	<json:property name="success" value="true"/>
	<json:property name="headline" escapeXml="true">
		<spring:theme code="checkout.multi.delivery.storesresults.position" arguments="${fn:length(storeSearchPageData.results)}"/>
	</json:property>
	<json:array name="stores">
		<c:forEach items="${storeSearchPageData.results}" var="store" varStatus="storeStatus">
			<json:object>
				<json:property name="number" value="${store.storeNumber}" />
				<json:property name="markup" escapeXml="false"><multi-checkout:deliveryStoreDetails store="${store}" /></json:property>
			</json:object>
		</c:forEach>
	</json:array>
</json:object>

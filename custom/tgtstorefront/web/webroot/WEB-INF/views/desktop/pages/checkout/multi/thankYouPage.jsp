<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/desktop/checkout" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/desktop/order" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="kiosk" tagdir="/WEB-INF/tags/desktop/kiosk" %>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/desktop/checkout/multi" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<template:checkoutPage stepName="thankYou" stepsFinished="true" hideHome="false" pageTitle="${pageTitle}">

	<common:globalMessages/>
	<c:if test="${featuresEnabled.featureIPGEnabled}">
		<cart:sohMessage orderCompleted="${true}"/>
	</c:if>
	<multi-checkout:youHaveOtherCarts />
	<multi-checkout:signOutAfterOrder />

	<div class="co-page">

		<div class="main" data-checkout-spc="${spc}">
			<h2 class="co-heading co-thank-you-heading">
				<spring:theme code="icon.tick" />
				<span class="heading-text">
					<spring:theme code="checkout.multi.thankYou.heading" />
				</span>
				<spring:theme code="text.print" var="printText" />
				<a class="co-print hide-for-small hfk" href="javascript:print()" title="${printText}">
					<span class="visuallyhidden">${printText}</span>
				</a>
			</h2>
			<multi-checkout:thankYou />
			<c:if test="${not isAnonymousCheckout}">
				<div class="co-groupe enews-signup hfma">
					<h3><spring:theme code="checkout.multi.thankYou.enews.heading" /></h3>
					<c:url value="/eNewsSubscription" var="eNewsURL" >
 						<c:param name="accredit" value="checkout"/>					</c:url>
					<p><spring:theme code="checkout.multi.thankYou.enews.message" arguments="${eNewsURL}" /></p>
				</div>
			</c:if>
			<c:if test="${isAnonymousCheckout}">
				<div class="co-groupe guest-create">
					<c:url value="${targetUrlsMapping.guestRegistration}${orderData.code}" var="createAccountActionUrl" />
					<user:register action="${createAccountActionUrl}" openablePanel="true" />
				</div>
			</c:if>
		</div>

		<div class="aside">
			<c:if test="${not kioskMode}">
				<multi-checkout:checkoutBarcode orderData="${orderData}" />
			</c:if>
			<multi-checkout:checkoutOrderDetails orderData="${orderData}" />
		</div>

		<div class="co-controls">
			<multi-checkout:nextActions
				endUrl="${otherCarts ? targetUrlsMapping.cart : targetUrlsMapping.home}"
				endCode="checkout.multi.thankYou.continue" />
		</div>

	</div>

	<div class="hide-for-small">
		<multi-checkout:youHaveOtherCarts />
	</div>

</template:checkoutPage>

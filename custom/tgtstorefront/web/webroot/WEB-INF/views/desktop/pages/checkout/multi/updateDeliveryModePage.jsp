<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="showEntries" value="${notShowEntries ? false : true}"/>
<json:object>
	<json:property name="success" value="${empty errorMsg}"/>
	<c:if test="${not empty errorMsg}">
		<json:property name="errorMsg" escapeXml="false">
			<feedback:message type="error"
				strongHeadingCode="feedback.oops">
				<p><spring:theme code="${errorMsg}" /></p>
			</feedback:message>	
		</json:property>
	</c:if>
	<c:if test="${empty errorMsg}">
		<json:property name="cartSummaryHtml" escapeXml="false">
			<cart:cartTotals showEntries="${showEntries}" cartData="${cartData}"/>
		</json:property>
	</c:if>
</json:object>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/shared/asset" %>

<c:set var="storePinUrl"><asset:resource code="store.map.pin.url" /></c:set>

<div class="main">
	<div class="store-info">
		<div class="store-details">
				<store:storeContact store="${store}"/>
				<store:storeHours openingSchedule="${store.openingHours}"/>
		</div>
		<div class="store-map imap" data-lat="${store.geoPoint.latitude}" data-lng="${store.geoPoint.longitude}" data-marker-image-path="${storePinUrl}"></div>
	</div>
</div>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:if test="${not empty ipgRedirectUrl}">
	<c:url var="ipgRedirectUrl" value="${fullyQualifiedDomainName}${ipgRedirectUrl}" />
	<script>
		// Kill bfcache
		window.onunload = function(){
			return;
		}

		// Kill bfcache
		window.top.onunload = function(){
			return;
		}

		window.top.location.replace('${ipgRedirectUrl}');
	</script>
</c:if>

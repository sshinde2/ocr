<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>
<%@ taglib prefix="target" uri="http://target.com.au/jsp/target" %>


<template:page pageTitle="${pageTitle}" disableFooterEnews="${true}">

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages />
		
	<div class="main">

		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="component" contentSlot="${slots.Main}">
			<cms:component component="${component}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />
		<form:form method="post" commandName="donationRequestForm" novalidate="true">
			<div class="f-narrow">
				<formElement:formSelectBox idKey="donate.request.field.store" labelKey="donate.request.field.store" path="storeNumber" skipBlank="false" mandatory="true" skipBlankMessageKey="form.select.empty" items="${storeList}"/>
				<formElement:formInputBox idKey="donate.request.field.eventName" labelKey="donate.request.field.eventName" path="eventName" inputCSS="text" mandatory="false" />
				<formElement:formInputBox idKey="donate.request.field.orgName" labelKey="donate.request.field.orgName" path="organisationName" inputCSS="text" mandatory="true" />
				<formElement:formInputBox idKey="donate.request.field.abnOrAcn" labelKey="donate.request.field.abnOrAcn" path="abnOrAcn" inputCSS="text" mandatory="false" />
				<formElement:formInputBox idKey="donate.request.field.contactName" labelKey="donate.request.field.contactName" path="contactName" inputCSS="text" mandatory="true" />
				<formElement:formInputBox idKey="donate.request.field.role" labelKey="donate.request.field.role" path="role" inputCSS="text" mandatory="false" />
				<formElement:formInputBox idKey="donate.request.field.orgStreet" labelKey="donate.request.field.orgStreet" path="organisationStreet" inputCSS="text" mandatory="true" />
				<formElement:formInputBox idKey="donate.request.field.orgCity" labelKey="donate.request.field.orgCity" path="organisationCity" inputCSS="text" mandatory="true" />
				<formElement:formInputBox idKey="donate.request.field.orgState" labelKey="donate.request.field.orgState" path="organisationState" inputCSS="text" mandatory="true" />
				<formElement:formInputBox idKey="donate.request.field.orgPostcode" labelKey="donate.request.field.orgPostcode" path="organisationPostCode" inputCSS="text" mandatory="true" />
				<formElement:formInputBox idKey="donate.request.field.email" labelKey="donate.request.field.email" path="email" inputCSS="text" mandatory="true" />
				<formElement:formInputBox idKey="donate.request.field.phone" labelKey="donate.request.field.phone" path="phoneNumber" inputCSS="text" mandatory="true" />
				<formElement:formTextArea idKey="donate.request.field.reason" labelKey="donate.request.field.reason" path="reason" mandatory="true" areaRows="5"/>
				<formElement:formTextArea idKey="donate.request.field.requestNotes" labelKey="donate.request.field.requestNotes" path="notes" mandatory="false" areaRows="5"/>
				<formElement:formSelectBox idKey="donate.request.field.preferredMethodOfContact" labelKey="donate.request.field.preferredMethodOfContact" path="preferredMethodofContact" skipBlank="false" mandatory="true" skipBlankMessageKey="form.select.empty" items="${preferredMethodofContact}"/>
			</div>
			<target:csrfInputToken/>
			<button type="submit" class="button-fwd"><spring:theme code='donate.request.submit'/><spring:theme code="icon.right-arrow-small" /></button>
		</form:form>

	</div>

	<div class="supplement">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="component" contentSlot="${slots.Supplement}">
			<cms:component component="${component}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>

</template:page>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>

<c:set var="fragmentComponents">
	<cms:slot var="mainComponent" contentSlot="${slots.Main}">
		<cms:component component="${mainComponent}"/>
	</cms:slot>
</c:set>

<c:if test="${not empty fragmentComponents}">
	<div class="PersonaliseSegment-components">
		${fragmentComponents}
	</div>
</c:if>

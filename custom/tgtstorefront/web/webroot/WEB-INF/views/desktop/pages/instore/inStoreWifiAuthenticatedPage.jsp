<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="account" tagdir="/WEB-INF/tags/desktop/account" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<template:pagePortal pageTitle="${pageTitle}"  pageHeading="store.wifi.title" bareMinimum="${true}" jsRequired="${true}" disableSnippets="${true}">

	<div class="main instore-wifi">
		<cms:slot var="component" contentSlot="${slots.Main}">
			<cms:component component="${component}"/>
		</cms:slot>

		<div class="instore-wifi-time">
			<p class="time-message"><spring:theme code="icon.clock" /><spring:theme code="store.wifi.time" /></p>
		</div>

		<spring:theme code="store.wifi.authenticated" var="authenticated"/>
		<feedback:message type="success" size="small" shortMessage="${fn:length(authenticated) lt 40}">
			<p><spring:theme code="store.wifi.authenticated" /></p>
		</feedback:message>

		<cms:slot var="component" contentSlot="${slots.Supplement}">
			<cms:component component="${component}"/>
		</cms:slot>
	</div>

</template:pagePortal>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>

<json:object>
	<json:property name="success" value="${not empty slots and fn:length(slots) gt 0}" />
	<json:object name="data" escapeXml="false">
		<json:object name="slots" escapeXml="false">
			<c:forEach items="${slots}" var="slot">
				<json:property name="${slot.key}" escapeXml="false">
					<cms:slot var="component" contentSlot="${slots[slot.key]}">
						<cms:component component="${component}"/>
					</cms:slot>
				</json:property>
			</c:forEach>
		</json:object>
	</json:object>
</json:object>

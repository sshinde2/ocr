<html>
<head>
<script>
function kioskConfig( event ) {
	if( event.origin === "${fullyQualifiedDomainName}") {
		localStorage.setItem('kiosk-config', event.data);
		parent.postMessage('kiosk-config-success', "${fullyQualifiedDomainName}");
	}
}
window.addEventListener('message', kioskConfig ,false);
</script>
</head>
</html>
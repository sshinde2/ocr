<html>
<head>
<style>
body { background: #ba0000; color:white; font-family: arial;}
h1 {text-align: center; margin-top: 12em; -webkit-transition: opacity 0.2s ease; }
</style>
<script>
(function(){
	var ls = JSON.parse(window.localStorage.getItem('kiosk-config'));
	document.location.href = ls && typeof ls.landingPageUrl === "string" ? ls.landingPageUrl : "/";
})();
</script>
</head>
<body >
<h1>Redirecting...</h1>
</body>
</html>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/desktop/user" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>

<template:page pageTitle="${pageTitle}">

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>

	<div class="has-aside content">

		<div class="main">
			<h1 class="heading"><spring:theme code="forgottenPwd.heading"/></h1>
			<p><spring:theme code="forgottenPwd.description"/></p>
			<user:forgottenPwd cssClass="f-narrow" />

		</div>

		<div class="aside hide-for-small">
			<div class="nav-seg">
				<h3><a href="${targetUrlsMapping.home}"><spring:theme code="text.home" /></a></h3>
			</div>
		</div>

		<div class="supplement">
			<cms:slot var="component" contentSlot="${slots.Supplement}">
				<cms:component component="${component}"/>
			</cms:slot>
		</div>

	</div>
</template:page>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<feature:enabled name="uiFullWidthPDP">
	<c:set var="uiFullWidthPDP" value="${true}" />
</feature:enabled>

<feature:enabled name="uiBazaarvoiceOverwrite">
	<c:set var="bvClass" value="bv-overwrite" />
</feature:enabled>

<c:set var="availQtyInStoreorOnline" value="${availableOnline or availableInStore}" />

<feature:enabled name="featureConsolidatedStoreStockVisibility">
	<c:set var="consolidatedStoreStockVisibilityEnabled" value="${true}" />
</feature:enabled>

<c:set var="pageMode" value="${not availableOnline and availableInStore ? 'store' : 'online'}" />

<template:page pageTitle="${pageTitle}" bodyCssClass="${bvClass} ${uiFullWidthPDP ? ' page-content-full-width' : ''}">
	<c:if test="${not empty message}">
		<spring:theme code="${message}"/>
	</c:if>
	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:championSlot slotName="ListerChampion" />
	<common:globalMessages/>
	<div itemscope="itemscope" itemtype="http://schema.org/Product">
		<c:if test="${not uiFullWidthPDP}">
			<product:productDetailPageThin pageMode="${pageMode}" availQtyInStoreorOnline="${availQtyInStoreorOnline}" galleryImages="${galleryImages}" />
		</c:if>

		<c:if test="${uiFullWidthPDP}">
			<product:productDetailPage />
		</c:if>

		<div class="supplement">
			<c:if test="${not uiFullWidthPDP}">
				<c:set var="contentSlotContext" value="full" scope="request" />
				<cms:slot var="relatedProductsComponent" contentSlot="${slots.RelatedProducts}" position="RelatedProducts">
					<cms:component component="${relatedProductsComponent}"/>
				</cms:slot>
			</c:if>

			<c:if test="${productEndOfLife}">
				<c:if test="${not availQtyInStoreorOnline}">
					<spring:theme code="product.endoflife.search.placeholder" var="placeholderText" />
					<div class="search-box mini-search ProductEndOfLife-searchBox">
							<header:search formId="search-productendoflife" placeholderText="${placeholderText}" className="ProductEndOfLife-input" />
						</div>
					</c:if>
				<product:productEndOfLifeLinks />
			</c:if>

			<c:remove var="contentSlotContext" scope="request" />
			<c:if test="${not uiFullWidthPDP}">
				<div class="bv bv-reviews">
					<h2><spring:theme code="product.page.reviews.heading" /></h2>
					<div id="BVRRContainer">
					</div>
				</div>
			</c:if>
			<c:if test="${bvSeoStatus}">
				<%-- Stripping out BVs itemtype declaration and name as it doubles up with ours --%>
				<c:set var="bvSeoContent">${fn:replace(bvSeoContent, 'itemscope itemtype="http://schema.org/Product"', '')}</c:set>
				<c:set var="seoContent">${fn:replace(bvSeoContent, 'itemprop="name"', '')}</c:set>
				${seoContent}
			</c:if>
		</div>
	</div>

	<c:if test="${consolidatedStoreStockVisibilityEnabled}">
		<common:handlebarsTemplates template="findNearestStore" />
	</c:if>
</template:page>

<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="stl" tagdir="/WEB-INF/tags/desktop/stl" %>

<feature:enabled name="featureStepShopTheLook">
	<c:set var="pageTitle">
		${lookPageData.name}&nbsp;<spring:theme code="page.title.separator" />&nbsp;<spring:theme code="page.title.suffix" />
	</c:set>
	<c:set var="metaDescription" value="${lookPageData.description}" />
</feature:enabled>
<c:set var="fis2StoreStockVisibility" value="${false}" />

<feature:enabled name="fis2StoreStockVisibility">
	<c:set var="fis2StoreStockVisibility" value="${true}" />
</feature:enabled>

<template:page pageTitle="${pageTitle}" metaDescription="${metaDescription}">
	<div class="global-messages">
		<common:globalMessages/>
	</div>

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:championSlot slotName="ListerChampion" />
	<div class="main">
		<c:if test="${not empty lookPageData.name}">
			<h1>${lookPageData.name}</h1>
		</c:if>
		<div class="ProdGroupSummary">
			<div class="ProdGroupSummary-item">
				<feature:enabled name="featureStepShopTheLook">
					<stl:lookImage look="${lookPageData}" size="313" className="ProdGroupSummary-heroImage" />
				</feature:enabled>
				<feature:disabled name="featureStepShopTheLook">
					<img class="ProdGroupSummary-heroImage" src="${lookPageData.groupImgUrl}" alt="${lookPageData.name}" />
				</feature:disabled>
			</div>
			<div class="ProdGroupSummary-item ProdGroupSummary-groupSummary">
				<div class="ProdGroupDetails ProdGroupDetails-groupDetails hfma">
					<div class="ProdGroupDetails-header">
						<c:set var="itemCount" value="${fn:length(productsData)}" />
						<c:set var="itemClass" value="is-noPrice" />
						<h5 class="ProdGroupDetails-heading"><spring:theme code="product.group.getTheLookHead" /></h5>
						<feature:disabled name="featureStepShopTheLook">
							<c:if test="${not empty lookPageData.priceData}">
								<span class="ProdGroupDetails-priceDetails"><%--
									--%><format:price priceData="${lookPageData.priceData}" smallCurrencySign="${true}" stripDecimalZeros="${true}"/><%--
								--%></span>
								<c:set var="itemClass" value="" />
							</c:if>
						</feature:disabled>
						<p class="ProdGroupDetails-itemCount ${itemClass}"><spring:theme code="product.group.itemCount" arguments="${itemCount}" /></p>
					</div>
					<ul class="ProdGroupDetails-thumbs">
						<c:set var="firstAvailableItem" value="" />
						<c:forEach items="${productsData}" var="product">
							<c:set var="isActualStoreStockAvailable" value="${product.maxActualConsolidatedStoreStock gt 0 and fis2StoreStockVisibility}"/>
							<c:if test="${empty firstAvailableItem}">
								<c:set var="firstAvailableItem" value="${product.code}" />
							</c:if>

							<li class="ProdGroupThumb ProdGroupThumb--navigable ${not product.inStock and not isActualStoreStockAvailable ?'ProdGroupThumb--oos':''}">
								<c:url value="${product.url}" var="productUrl"/>
								<a href="${productUrl}" class="ProdGroupThumb-item" data-prod-code="${product.code}">
									<c:if test="${not product.inStock and not isActualStoreStockAvailable}">
										<spring:theme code="icon.plain-cross" />
									</c:if>
									<c:set var="imageUrl"><product:productListerImage product="${product}" format="list" unavailable="true"/></c:set>
									<img src="${imageUrl}" alt="${product.name}" title="${product.name}" />
								</a>
							</li>
						</c:forEach>
					</ul>
					<div class="ProdGroupDetails-actions hfma">
						<c:if test="${not empty firstAvailableItem}">
							<button class="button-fwd-alt ProdGroupDetails-actionButton ProdGroupDetails-getLookButton" data-prod-code="${firstAvailableItem}" ><spring:theme code="product.group.getTheLookButton" /><spring:theme code="icon.down-arrow-big" /></button>
						</c:if>
					</div>
				</div>
			</div>

			<div class="ProdGroupSummary-item ProdGroupAddedItems hfma">
				<div class="ProdGroupDetails">
					<div class="ProdGroupDetails-header">
						<h5 class="ProdGroupDetails-heading"><spring:theme code="product.group.yourLookHead" /></h5>
						<p class="ProdGroupDetails-priceDetails"><spring:theme code="product.group.zeroDollars" /></p>
						<p class="ProdGroupDetails-itemCount"><spring:theme code="product.group.itemCount" arguments="0" /></p>
					</div>
					<ul class="ProdGroupDetails-thumbs">
						<li><spring:theme code="product.group.noItems" /></li>
					</ul>
					<div class="ProdGroupDetails-actions">
						<a href="${targetUrlsMapping.cart}" class="button-fwd ProdGroupDetails-actionButton" ><spring:theme code="product.group.viewBasket" /><spring:theme code="icon.right-arrow-large" /></a>
					</div>
				</div>
			</div>
		</div>
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="mainComponent" contentSlot="${slots.Main}">
			<cms:component component="${mainComponent}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />

		<div class="ProdSeries">
			<h2 class="ProdSeries-heading"><spring:theme code="product.group.getTheLookHead" /></h2>
			<c:forEach items="${productsData}" var="product" varStatus="status">
				<c:if test="${fn:length(product.targetVariantProductListerData) > 0 and not empty product.targetVariantProductListerData[0]}">
					<c:set var="firstVariant" value="${product.targetVariantProductListerData[0]}" />
				</c:if>
				<c:set var="productCode" value="${not empty firstVariant ? firstVariant.colourVariantCode : product.code}" />
				<feature:enabled name="uiTrackDisplayOnly">
					<c:set var="displayOnly" value="${not empty firstVariant ? firstVariant.displayOnly : product.displayOnly}" />
				</feature:enabled>
				<c:set var="productEcommJson"><template:productEcommJson
					product="${product}"
					list="Product Look"
					position="${status.count}"
					assorted="${not empty firstVariant ? firstVariant.assorted : product.assorted}"
					displayOnly="${displayOnly}" /></c:set>
				<c:set var="dataEcProduct">data-ec-product='${productEcommJson}'</c:set>
				<c:set var="gaClassName" value="ga-ec-impression" />
				<c:url value="${product.url}" var="productUrl"/>
				<spring:theme var="nextButtonText" code="${status.last ? 'product.group.finishButtonText' : 'product.group.nextButtonText'}" />
				<div class="ProdSeriesItem ${not mobileAppMode ? 'ProdSeriesItem--navigable ' : '' } ${gaClassName}" ${dataEcProduct}
					data-prod-code="${product.code}" data-prod-url="${productUrl}" data-next-button-text="${nextButtonText}" data-product-code="${productCode}">
					<a href="${productUrl}" class="ProdSeriesItem-preview ga-ec-look-click">
						<span class="ProdGroupThumb ${not product.inStock and not isActualStoreStockAvailable ?'ProdGroupThumb--oos':''}">
							<span class="ProdGroupThumb-item">
								<c:if test="${not product.inStock and not isActualStoreStockAvailable}">
									<spring:theme code="icon.plain-cross" />
								</c:if>
								<c:set var="imageUrl"><product:productListerImage product="${product}" format="list" unavailable="true"/></c:set>
								<img src="${imageUrl}" alt="${product.name}" title="${product.name}" />
							</span>
						</span>
						<span class="ProdSeriesItem-prodName ${fn:length(product.name) lt 33 ? 'ProdSeriesItem-prodName--short' : '' }">
							${product.name}
						</span>
						<c:if test="${product.inStock or isActualStoreStockAvailable}">
							<span class="ProdSeriesItem-price hide-for-small ${product.showWasPrice ? 'is-wasPrice' : ''}">
								<product:pricePoint product="${product}" smallCurrencySign="${true}" />
							</span>
						</c:if>
					</a>
					<div class="ProdSeriesItem-prod">
					</div>
				</div>
			</c:forEach>
		</div>
	</div>

	<div class="supplement">
		<spring:theme code="text.carousel.productGroup.callToAction" var="callToAction" />
		<spring:theme code="text.carousel.productGroup.title" var="title" />

		<c:set var="staticLink"><a href='${relatedLooksUrl}'>See All ${lookPageData.collectionName} &gt;</a></c:set>
		<stl:lookCarousel looks="${relatedLooks}" title="${title}" unveil="${true}" deferred="${true}" width="full" staticLink="${staticLink}" callToAction="${callToAction}" />

		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="supplementComponent" contentSlot="${slots.Supplement}">
			<cms:component component="${supplementComponent}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>

</template:page>

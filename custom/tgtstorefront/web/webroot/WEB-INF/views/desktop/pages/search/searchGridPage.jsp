<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="layout" tagdir="/WEB-INF/tags/desktop/common/layout" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="category" tagdir="/WEB-INF/tags/desktop/category" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<c:set var="hasProducts" value="${fn:length(searchPageData.results) > 0}" />
<c:set var="featureNewSubCategoriesUx" value="${false}" />
<spring:theme code="search.page.headingText" text="Products Found" var="headingText" />
<spring:theme code="search.no.results.no.keyword" text="Search" var="headingTextEmpty" arguments="${autoCorrectedTerm}"/>

<layout:productListingPage listerName="Search" listerType="grid"  unveilStartPosition="6" heading="${hasProducts ? headingText : headingTextEmpty}">
	<jsp:attribute name="championSlot">
		<category:categoryListerChampionSlot slotName="ListerChampion" searchPageData="${searchPageData}" />
	</jsp:attribute>
	<jsp:attribute name="asideAbove">
		<feature:enabled name="newSubCategoriesUx">
			<c:set var="featureNewSubCategoriesUx" value="${true}" />
			<c:if test="${hasProducts}">
				<nav:searchHeading searchPageData="${searchPageData}" extraClass="only-for-small" />
			</c:if>
		</feature:enabled>
	</jsp:attribute>
	<jsp:attribute name="mainAbove">
		<c:if test="${hasProducts}">
			<nav:searchHeading searchPageData="${searchPageData}" extraClass="${featureNewSubCategoriesUx ? 'hide-for-small' : ''}" />
		</c:if>
	</jsp:attribute>
</layout:productListingPage>

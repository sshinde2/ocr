<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>

<analytics:removeAnalytics />
<template:master pageTitle="${pageTitle}" bareMinimum="${true}" headerOnly="true">
	<header:header disabledCart="true" disabledSearch="true" disabledSitewideNotification="true" disabledFavourites="true" />
	<div id="wrapper" data-context-path="${request.contextPath}">
		<div id="page">
			<nav:topNavigation/>
		</div>
	</div>
	<common:siteFurnitureScript />
</template:master>

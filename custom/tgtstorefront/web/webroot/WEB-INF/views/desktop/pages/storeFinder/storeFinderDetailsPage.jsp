<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="store" tagdir="/WEB-INF/tags/desktop/store" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
	<common:globalMessages/>

	<div class="main">
		
		<div class="store-locator-store">
			<div class="store-details">
				<store:storeContact store="${store}"/>
				<store:storeHours openingSchedule="${store.openingHours}"/>
				<c:if test="${not empty store.targetAddressData}">
					<c:url value="${targetUrlsMapping.storeFinder}" var="nearbyUrl">
						 <c:param name="q" value="${store.targetAddressData.postalCode}"/>
					</c:url>
					<p>
						<a href="${nearbyUrl}" class="button-fwd hide-for-small"><spring:theme code="storeFinder.find.nearby.stores" arguments="${store.name}" argumentSeparator=";;;" /></a>
					</p>
				</c:if>
			</div>
			<store:storeMap store="${store}"/>
		</div>
		<c:if test="${not empty store.targetAddressData}">
			<p>
				<a href="${nearbyUrl}" class="button-fwd only-for-small wide-for-tiny"><spring:theme code="storeFinder.find.nearby.stores" arguments="${store.name}" argumentSeparator=";;;" /></a>
			</p>
		</c:if>
	</div>
	
</template:page>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>


<template:pageComponentTests pageTitle="${pageTitle}">
	<div class="inner-content">
		<div class="main">
			<c:set var="contentSlotContext" value="wide" scope="request" />
			<cms:slot var="pageContentComponent" contentSlot="${slots.PageContent}">
				<cms:component component="${pageContentComponent}"/>
			</cms:slot>
			<c:remove var="contentSlotContext" scope="request" />
		</div>
	</div>
</template:pageComponentTests>
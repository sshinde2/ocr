<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cms" uri="/cms2lib/cmstags/cmstags.tld" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="feedback" tagdir="/WEB-INF/tags/desktop/feedback" %>
<%@ taglib prefix="assets" tagdir="/WEB-INF/tags/desktop/assets" %>
<%@ taglib prefix="feature" tagdir="/WEB-INF/tags/desktop/feature" %>

<template:page pageTitle="${pageTitle}">
	<div class="global-messages">
		<common:globalMessages/>
	</div>

	<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>

	<div class="main">

		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="mainComponent" contentSlot="${slots.Main}">
			<cms:component component="${mainComponent}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />

		<feature:enabled name="featureWishlistFavourites">
			<c:if test="${not kioskMode and not iosAppMode}">
				<c:set var="favouritesData">data-favourites-url="/favourites" </c:set>

				<spring:theme code="favourites.share.button.shareText" var="shareText" />
				<spring:theme code="favourites.share.button.signInText" var="signInText" />
				<c:set var="shareLink">
					<a href="/favourites/share" class="FavToolBar-Button FavToolBar-Button--Share button-norm-border button-small-text" data-share-text="${shareText}" data-sign-in-text="${signInText}" data-share-url="/favourites/share" data-sign-in-url="/login" >
						<span class="FavShareButton-text hide-for-small"></span>
					</a>
				</c:set>

				<spring:theme code="favourites.share.button.deleteAll" var="deleteAllText" />
				<c:set var="deleteButton">
					<spring:theme code="favourites.delete.confirmation.message" var="deleteMessage" />
					<button class="FavToolBar-Button FavToolBar-Button--Delete button-norm-border button-small-text" data-message="${deleteMessage}">
						<span class="hide-for-small">${deleteAllText}</span>
					</button>
				</c:set>

				<div class="FavProducts" ${favouritesData}>

					<div class="FavProducts-header">
						<h1 class="FavProducts-heading hfma"><spring:theme code="favourites.heading" /></h1>
						<div class="FavToolBar">
							<%-- TODO uncomment this once the security vulnerability in favourites share feature has been fixed --%>
							<%-- <feature:enabled name="featureWishlistFavouritesSharing">
								${shareLink}
							</feature:enabled> --%>
							${deleteButton}
						</div>
					</div>

					<div class="FavProducts-content">
						<noscript>
							<feedback:message size="small">
								<p><spring:theme code="favourites.nojs" /></p>
							</feedback:message>
						</noscript>
						<ul class="FavProducts-list product-base-layout product-grid-layout product-grid-full">

						</ul>
					</div>

				</div>

			</c:if>
		</feature:enabled>

	</div>

	<div class="supplement">
		<c:set var="contentSlotContext" value="full" scope="request" />
		<cms:slot var="supplementComponent" contentSlot="${slots.Supplement}">
			<cms:component component="${supplementComponent}"/>
		</cms:slot>
		<c:remove var="contentSlotContext" scope="request" />
	</div>

	<c:set var="tinyHeart">
		<span class="FavHeart--tiny">
			&nbsp;
			<assets:svgFavHeart />
		</span>
	</c:set>

	<feature:enabled name="uiStickyHeader">
		<c:set var="tinyHeart">
			<span class="FavHeart--tiny">
				<assets:svgFavHeart heartType="miniFatHeart" />
			</span>
		</c:set>
	</feature:enabled>

	<script type="text/x-handlebars-template" id="favourite-list-empty">
		<feedback:message >
			<p><spring:theme code="favourites.empty.heading"/><spring:theme code="favourites.empty.text" arguments="${tinyHeart}" /></p>
		</feedback:message>
	</script>

	<script type="text/x-handlebars-template" id="favourite-list-xhr-error">
		<feedback:message type="error" size="small">
			<p><spring:theme code="favourites.xhr.error" /></p>
		</feedback:message>
	</script>

	<script type="text/x-handlebars-template" id="favourites-guest-prompt">
		<feedback:message>
			<p>
				<spring:theme code="favourites.guest.prompt" />
				<br>
				<%-- TODO uncomment this once the security vulnerability in favourites share feature has been fixed --%>
				<%--<feature:enabled name="featureWishlistFavouritesSharing">
					<spring:theme code="favourites.guest.shareEmailPrompt" />
				</feature:enabled>--%>
			</p>
		</feedback:message>
	</script>

	<script type="text/x-handlebars-template" id="ajax-error-feedback">
		<feedback:message type="error" size="small" icon="false">
			<p><spring:theme code="xhr.error.occurred" /></p>
		</feedback:message>
	</script>

</template:page>

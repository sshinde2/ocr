<?xml version="1.0" encoding="iso-8859-1"?>
<web-app version="2.5"
	xmlns="http://java.sun.com/xml/ns/javaee"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
		http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd">

	<display-name>tgtstorefront</display-name>

	<!--
	 We have to explicitly clear the welcome file list.
	 We don't need to serve a default or index page as we can handle all the requests via spring MVC.
	-->
	<welcome-file-list>
		<welcome-file/>
	</welcome-file-list>


	<!-- filters -->

	<!-- log4j logging context filter -->
	<filter>
		<description>
			Set the jsessionid into the log4j Mapped Diagnostic Context.
		</description>
		<filter-name>loggingContextServletFilter</filter-name>
		<filter-class>au.com.target.tgtwebcore.filter.LoggingContextServletFilter</filter-class>
	</filter>

	<filter>
	<filter-name>characterEncodingFilter</filter-name>
		<filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
		<init-param>
			<param-name>encoding</param-name>
			<param-value>UTF-8</param-value>
		</init-param>
		<init-param>
			<param-name>forceEncoding</param-name>
			<param-value>true</param-value>
		</init-param>
	</filter>

	<!-- resource filter -->
	<filter>
		<description>
			ResourceFilter
			Filter used to server file resources by bypassing the other filters.
		</description>
		<filter-name>resourceFilter</filter-name>
		<filter-class>au.com.target.tgtstorefront.filters.StaticResourceFilter</filter-class>
	</filter>

    <!-- XSS filter -->
    <filter>
        <filter-name>XSSFilter</filter-name>
        <filter-class>de.hybris.platform.servicelayer.web.XSSFilter</filter-class>
    </filter>
    
	<!-- spring based filter chain -->
	<filter>
		<description>
			Spring configured based chain of the spring configurable filter beans
		</description>
		<filter-name>storefrontFilterChain</filter-name>
		<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
	</filter>

	<!-- spring security chain -->
	<filter>
		<description>
			SpringSecurityFilterChain
			Supports delegating to a chain of spring configured filters. The filter name
			must match the bean name.
		</description>
		<filter-name>springSecurityFilterChain</filter-name>
		<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
	</filter>

	<!-- filter mappings -->
	<filter-mapping><filter-name>loggingContextServletFilter</filter-name><url-pattern>/*</url-pattern></filter-mapping>

	<filter-mapping>
		<filter-name>characterEncodingFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	
	
	
	<filter-mapping><filter-name>resourceFilter</filter-name><url-pattern>/_ui/*</url-pattern></filter-mapping>
	<filter-mapping><filter-name>XSSFilter</filter-name><url-pattern>/*</url-pattern></filter-mapping>
	<filter-mapping><filter-name>storefrontFilterChain</filter-name><servlet-name>DispatcherServlet</servlet-name></filter-mapping>
	<filter-mapping><filter-name>springSecurityFilterChain</filter-name><servlet-name>DispatcherServlet</servlet-name></filter-mapping>

	<!-- HTTP Session Listeners -->
	<listener>
		<description>
			The HybrisContextLoaderListener extends the usual SpringContextLoaderListener (which loads
			the context from specified location) by adding the global application context of
			the platform as parent context. With having the global context set as parent you can access
			or override beans of the global context and get the 'tenant' scope.
		</description>
		<listener-class>de.hybris.platform.spring.HybrisContextLoaderListener</listener-class>
	</listener>

	<listener>
		<description>
			The RequestContextListener exposes the 'request' scope to the context.
			Furthermore it is needed when overriding the 'jalosession' bean for your web application.
		</description>
		<listener-class>org.springframework.web.context.request.RequestContextListener</listener-class>
	</listener>

	<listener>
		<description>
			Creates a unique token that is stored in the session. This token is used by
			forms that accept payment to ensure that the same form cannot be submitted twice.
		</description>
    	<listener-class>au.com.target.tgtstorefront.listener.TokenListener</listener-class>
    </listener>
    
    <listener>
        <listener-class>org.springframework.security.web.session.HttpSessionEventPublisher</listener-class>
    </listener>

	<!-- config -->
	<context-param>
		<description>
			The 'contextConfigLocation' param specifies where your configuration files are located.
			The 'WEB-INF/config/web-application-config.xml' file includes several other XML config
			files to build up the configuration for the application.
		</description>
		<param-name>contextConfigLocation</param-name>
		<param-value>WEB-INF/config/web-application-config.xml</param-value>
	</context-param>

	<!-- Servlets -->
	<servlet>
		<description>
			DispatcherServlet
			Spring MVC dispatcher servlet. This is the entry point for the Spring MVC application.
		</description>
		<servlet-name>DispatcherServlet</servlet-name>
		<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
		<init-param>
			<description>
				Specifies the location for Spring MVC to load an additional XML configuration file.
				Because hybris is already configured with the XML spring configuration files to load
				we must set this param value to EMPTY in order to prevent loading of the default
				/WEB-INF/applicationContext.xml file.
			</description>
			<param-name>contextConfigLocation</param-name>
			<param-value></param-value>
		</init-param>
		<load-on-startup>1</load-on-startup>
	</servlet>
	
	
	<!--
	<servlet>
        <servlet-name>CompressServlet</servlet-name>
        <servlet-class>com.granule.CompressServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>


    <servlet-mapping>
        <servlet-name>CompressServlet</servlet-name>
        <url-pattern>/combined.js</url-pattern>
    </servlet-mapping>
    <servlet-mapping>
        <servlet-name>CompressServlet</servlet-name>
        <url-pattern>/combined.css</url-pattern>
    </servlet-mapping>
	-->
	
	<servlet>
       <servlet-name>admin</servlet-name>
        <servlet-class>com.endeca.infront.assembler.servlet.spring.admin.SpringAdminServlet</servlet-class>
        <init-param>
            <param-name>adminServiceId</param-name>
            <param-value>adminService</param-value>
        </init-param>
    </servlet>
	<servlet-mapping>
        <servlet-name>admin</servlet-name>
        <url-pattern>/endeca-target/admin/*</url-pattern>
     </servlet-mapping>
    
	<servlet-mapping>
		<servlet-name>DispatcherServlet</servlet-name>
		<url-pattern>/</url-pattern>
	</servlet-mapping>

	<error-page>
		<exception-type>java.lang.Exception</exception-type>
		<location>/WEB-INF/error/serverError.jsp</location>
	</error-page>
	<error-page>
		<error-code>500</error-code>
		<location>/WEB-INF/error/serverError.jsp</location>
	</error-page>
	

	<!-- Session -->

	<session-config>
		<!-- Session timeout of 30 minutes -->
		<session-timeout>30</session-timeout>
	</session-config>

	<!-- JSP Configuration -->

	<jsp-config>
		<jsp-property-group>
			<url-pattern>*.jsp</url-pattern>
			<!-- Disable JSP scriptlets and expressions -->
			<scripting-invalid>true</scripting-invalid>
			<!-- Remove additional whitespace due to JSP directives -->
			<trim-directive-whitespaces>true</trim-directive-whitespaces>
		</jsp-property-group>
	</jsp-config>

</web-app>

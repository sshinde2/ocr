/**
 * 
 */
package au.com.target.tgttest.automation;

import de.hybris.platform.tx.Transaction;

import org.apache.log4j.Logger;


/**
 * Created with code copied from TransactionRunListener and simplified to use string for description.
 * 
 */
public class AutomationTransactionRunner {

    private static final Logger LOG = Logger.getLogger(AutomationTransactionRunner.class.getName());

    /** Transaction of currently running test. */
    private Transaction curTrans;

    /**
     * Call when non-transactional test is finished
     * 
     * @param test
     */
    public void nonTransactionalTestFinished(final String test)
    {
        assertNoTransactionRunning(test, false);
    }


    /**
     * Call when transactional test is finished
     * 
     * @param description
     * @param commit
     */
    public void transactionalTestFinished(final String description, final boolean commit)
    {
        if (curTrans != null && curTrans.isRunning())
        {
            rollbackTestTx(description, curTrans, commit);
            curTrans = null;
        }
        else
        {
            LOG.error("Can not rollback transaction after test " + description + " - it is not running anymore",
                    new Exception("Illegal Test Tx Outcome of " + description));
        }
    }

    /**
     * Call when non-transactional test is started
     * 
     * @param description
     */
    public void nonTransactionalTestStarted(final String description)
    {
        assertNoTransactionRunning(description, true);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Transactional support is not enabled for test " + description);
        }
    }

    /**
     * Call when transactional test is started
     * 
     * @param description
     * @param enableDelayedStore
     */
    public void transactionalTestStarted(final String description, final boolean enableDelayedStore)
    {
        assertNoTransactionRunning(description, true);

        curTrans = startTestTransaction(description, enableDelayedStore);

        if (LOG.isDebugEnabled())
        {
            LOG.debug("Transactional support enabled for test " + description + " with "
                    + (enableDelayedStore ? "enabled" : "disabled") + " delayed store");
        }
    }

    private Transaction startTestTransaction(final String description, final boolean enableDelayedStore)
    {
        try
        {
            final Transaction tx = Transaction.current();
            tx.begin();
            tx.enableDelayedStore(enableDelayedStore);

            return tx;
        }
        catch (final Exception e)
        {
            LOG.error("Error starting test transaction before test " + description, e);
            return null;
        }
    }

    private void rollbackTestTx(final String description, final Transaction tx, final boolean commit)
    {
        try
        {
            int counter = 0;
            do
            {
                if (commit)
                {
                    tx.commit();
                }
                else
                {
                    tx.rollback();
                }
                counter++;
            }
            while (tx.isRunning()); // we must expect that more than one nested transaction has been left open

            if (counter > 1)
            {
                LOG.error("Found transaction open after performing test " + description
                        + " - required " + counter
                        + " attempts to finish transaction",
                        new Exception("Illegal Test Tx Outcome: " + description));
            }
        }
        catch (final Exception e)
        {
            LOG.error("Error committing transaction after test " + description, e);
        }
    }

    private void assertNoTransactionRunning(final String description, final boolean before)
    {
        final String when = before ? "before" : "after";
        final Transaction tx = Transaction.current();
        if (tx.isRunning())
        {
            final String msg = "Found running transaction " + tx + " " + when + "starting " + description
                    + " - trying to rollback..";
            if (before)
            {
                LOG.error(msg);
            }
            else
            {
                LOG.error(msg, new Exception("Illegal Test Tx Outcome: " + description));
            }
            rollbackLeftoverTx(tx);
        }
    }

    private void rollbackLeftoverTx(final Transaction tx)
    {
        try
        {
            do
            {
                tx.rollback();
            }
            while (tx.isRunning());
        }
        catch (final Exception e)
        {
            LOG.error("Error rolling back leftover transaction " + tx, e);
        }
    }


}

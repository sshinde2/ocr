/**
 * 
 */
package au.com.target.tgttest.automation.cucumber;

import au.com.target.tgttest.automation.AutomationPlatformRunner;
import au.com.target.tgttest.automation.AutomationPlatformRunner.SessionUser;
import au.com.target.tgttest.automation.AutomationTransactionRunner;
import cucumber.api.java.After;
import cucumber.api.java.Before;


/**
 * Implement before/after scenario hooks for Cucumber test runner
 * 
 */
public class CucumberHooks {

    // Unfortunately we can't easily get the cucumber scenario name - 
    // for solution using an aspect see http://daveayan.blogspot.com.au/2012/08/get-feature-and-scenario-name-with.html 
    private static final String TEST_NAME = "test";

    private final AutomationPlatformRunner apr = new AutomationPlatformRunner();
    private final AutomationTransactionRunner atr = new AutomationTransactionRunner();

    private boolean transactional = false;

    private SessionUser sessionUser = SessionUser.ANONYMOUS;


    /**
     * Handle rollback tag
     */
    @Before(value = "@rollback", order = 10)
    public void beforeScenarioSetRollback() {
        transactional = true;
    }


    /**
     * Handle norollback tag (the default)
     */
    @Before(value = "@norollback", order = 10)
    public void beforeScenarioSetNoRollback() {
        transactional = false;
    }

    /**
     * This scenario should run as admin
     */
    @Before(value = "@adminUser", order = 20)
    public void beforeScenarioSetAdminUser() {
        sessionUser = SessionUser.ADMIN;
    }

    @Before(value = "@esbPriceUpdate", order = 20)
    public void beforeScenarioSetEsbPriceUpdateUser() {
        sessionUser = SessionUser.ESBPRICEUPDATE;
    }

    @Before(value = "@esb", order = 20)
    public void beforeScenarioSetEsbUser() {
        sessionUser = SessionUser.ESB;
    }

    @Before(value = "@esbonline", order = 20)
    public void beforeScenarioSetEsbOnlineUser() {
        sessionUser = SessionUser.ESBONLINE;
    }

    @Before(value = "@devsupport", order = 20)
    public void beforeScenarioImportDevSupport() {
        sessionUser = SessionUser.DEV;
    }

    @Before(value = "@dealsauthor", order = 20)
    public void beforeScenarioSetDealsauthorUser() {
        sessionUser = SessionUser.DEALSAUTHOR;
    }

    /**
     * beforeScenario hooks
     */
    @Before(order = 100)
    public void beforeScenario() {

        try {
            apr.logInfo("Start before scenario hooks");
            apr.testStarted(sessionUser);

            // Transactional or not transactional
            apr.logInfo("transactional=" + transactional);

            if (transactional) {
                atr.transactionalTestStarted(TEST_NAME, false);
            }
            else {
                atr.nonTransactionalTestStarted(TEST_NAME);
            }

            apr.logInfo("Finished before scenario hooks");
        }
        catch (final Exception e) {
            apr.logError("Exception in beforeScenario", e);
        }

    }


    /**
     * afterScenario hooks
     */
    @After(order = 1000)
    public void afterScenario() {
        apr.logInfo("After scenario hooks");

        try {
            apr.testFinished();
        }
        catch (final Exception e) {
            apr.logError("Exception in afterScenario running apr.testFinished", e);
        }
        finally {
            if (transactional) {
                atr.transactionalTestFinished(TEST_NAME, false);
            }
            else {
                atr.nonTransactionalTestFinished(TEST_NAME);
            }
        }

        apr.logInfo("Finished After scenario hooks");
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.holder;

import au.com.target.tgtfacades.product.data.TargetProductData;


/**
 * @author rmcalave
 *
 */
public class DataHolder {
    private static DataHolder instance;

    private TargetProductData targetProductData;

    public static DataHolder getInstance() {
        if (instance == null) {
            instance = new DataHolder();
        }

        return instance;
    }

    /**
     * @return the targetProductData
     */
    public TargetProductData getTargetProductData() {
        return targetProductData;
    }

    /**
     * @param targetProductData
     *            the targetProductData to set
     */
    public void setTargetProductData(final TargetProductData targetProductData) {
        this.targetProductData = targetProductData;
    }
}

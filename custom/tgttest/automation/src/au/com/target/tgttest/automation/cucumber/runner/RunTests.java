package au.com.target.tgttest.automation.cucumber.runner;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@Cucumber.Options(glue = { "classpath:au.com.target.tgttest.automation.cucumber" }, format = {
        "pretty", "html:cuc-report/cucumber-html-report", "json:cuc-report/cucumber.json",
        "junit:cuc-report/cucumber-junit.xml" },
        tags = { "~@wip", "~@toFix", "~@notAutomated" })
public class RunTests {
    // Cucumber test runner class
}

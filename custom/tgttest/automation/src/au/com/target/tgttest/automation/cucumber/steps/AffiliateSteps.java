/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import au.com.target.tgtfacades.affiliate.data.AffiliateOrderData;
import au.com.target.tgtfacades.order.data.TargetOrderData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.ExpectedAffiliateOrderData;
import au.com.target.tgttest.automation.facade.domain.Order;
import cucumber.api.java.en.When;


/**
 * @author sbryan6
 *
 */
public class AffiliateSteps {

    private TargetOrderData orderData;

    @When("^thankyou page data is retrieved$")
    public void getThankyouPageData() {

        final String code = Order.getInstance().getOrderNumber();
        orderData = ServiceLookup.getTargetOrderFacade().getOrderDetailsForCode(code);
    }


    @When("^affiliate data is:$")
    public void verifyAffiliateData(final List<ExpectedAffiliateOrderData> expectedList) {

        assertThat(expectedList).hasSize(1);
        final ExpectedAffiliateOrderData expected = expectedList.get(0);

        final AffiliateOrderData actual = orderData.getAffiliateOrderData();

        assertThat(actual.getCodes()).isEqualTo(expected.getCodes());
        assertThat(actual.getQuantities()).isEqualTo(expected.getQuantities());
        assertThat(actual.getPrices()).isEqualTo(expected.getPrices());

    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtauspost.integration.dto.AddressDTO;
import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.integration.dto.ConsignmentDTO;
import au.com.target.tgtauspost.integration.dto.ConsignmentDetailsDTO;
import au.com.target.tgtauspost.integration.dto.DeliveryAddressDTO;
import au.com.target.tgtauspost.integration.dto.ParcelDetailsDTO;
import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtauspost.integration.dto.TargetManifestDTO;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.AuspostUtil;
import au.com.target.tgttest.automation.facade.bean.AusPostRequestInfo;
import au.com.target.tgttest.automation.facade.bean.ConsignmentForStore;
import au.com.target.tgttest.automation.facade.bean.PostcodeData;
import au.com.target.tgttinker.mock.auspost.MockDispatchLabelClient;
import au.com.target.tgttinker.mock.auspost.MockTransmitManifestClient;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcel;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;


/**
 * @author sbryan6
 *
 */
public class AusPostSteps {

    @Given("^webmethods returns '(SUCCESS|ERROR|UNAVAILABLE|success|error|unavailable)' for auspost manifest transmission$")
    public void setWebmethodsAuspostManifestTransmitResponse(final String responseType) {

        ServiceLookup.getTransmitManifestClient().setResponseType(
                MockTransmitManifestClient.TransmitManifestResponseType.valueOf(responseType.toUpperCase()));
    }

    @Given("^webmethods returns '(SUCCESS|ERROR|UNAVAILABLE|success|error|unavailable)' for auspost dispatch label$")
    public void setWebmethodsAuspostDispatchLabelResponse(final String responseType) {

        ServiceLookup.getDispatchLabelClient().setResponseType(
                MockDispatchLabelClient.DispatchLabelResponseType.valueOf(responseType.toUpperCase()));
    }

    @Given("postcode chargezone mappings are:$")
    public void setupPostCodesWithChargeZones(final List<PostcodeData> postcodeData) {
        for (final PostcodeData postcode : postcodeData) {
            AuspostUtil.insertUpdatePostcodeEntry(postcode);
        }
    }

    @Then("^manifest request is sent to AusPost with:$")
    public void verifyManifestRequestSentToAusPost(final List<AusPostRequestInfo> expectedList) {

        final AuspostRequestDTO request = ServiceLookup.getTransmitManifestClient().getLastRequest();
        assertThat(request).isNotNull();

        assertThat(expectedList).isNotNull().hasSize(1);
        final AusPostRequestInfo expected = expectedList.get(0);

        final StoreDTO store = request.getStore();
        assertThat(store).as("store").isNotNull();

        final AddressDTO address = store.getAddress();
        verifyAddress(expected.getStoreAddress(), address, "store address");

        assertThat(store.getName()).isEqualTo(expected.getStore());
        assertThat(store.getMerchantLocationId()).isEqualTo(expected.getMerchantLocationId());

    }

    @And("^verify contact details for notification :$")
    public void verifyContactDetailsForAusPostNotifications(final List<ConsignmentForStore> expectedCons) {
        final AuspostRequestDTO request = ServiceLookup.getTransmitManifestClient().getLastRequest();
        assertThat(request).isNotNull();

        final StoreDTO store = request.getStore();
        assertThat(store).as("store").isNotNull();
        assertThat(store.getManifest()).isNotNull();
        assertThat(store.getManifest().getConsignments()).isNotEmpty();

        for (final ConsignmentForStore expectedConsignment : expectedCons) {
            for (final ConsignmentDTO result : store.getManifest().getConsignments()) {
                if (result.getConsignment().getTrackingId().equals(expectedConsignment.getTrackingId())) {

                    assertThat(result.getConsignment().getDeliveryAddress().getPhone())
                            .isEqualTo(expectedConsignment.getPhone());
                    assertThat(result.getConsignment().getDeliveryAddress().getEmail())
                            .isEqualTo(expectedConsignment.getEmail());
                    assertThat(result.getConsignment().getDeliveryAddress().getSendNotifications())
                            .isEqualTo(expectedConsignment.getSendNotifications());
                }
            }
        }
    }


    @Then("^manifest request consignments are:$")
    public void verifyManifestRequestConsignments(final List<AusPostRequestInfo> expectedList) {

        final AuspostRequestDTO request = ServiceLookup.getTransmitManifestClient().getLastRequest();
        assertThat(request).isNotNull();

        final StoreDTO store = request.getStore();
        assertThat(store).as("store").isNotNull();

        final TargetManifestDTO manifest = store.getManifest();
        assertThat(manifest).as("manifest").isNotNull();

        final List<ConsignmentDTO> consignments = manifest.getConsignments();
        assertThat(consignments).as("consignments").isNotNull();
        assertThat(consignments).hasSize(expectedList.size());

        for (final AusPostRequestInfo expectedCon : expectedList) {
            boolean found = false;
            for (final ConsignmentDTO conWrapper : consignments) {
                final ConsignmentDetailsDTO con = conWrapper.getConsignment();
                if (con.getTrackingId().equals(expectedCon.getTrackingId())) {
                    found = true;
                    assertThat(con.getDeliveryAddress().getName()).as("delivery name").isEqualTo(
                            expectedCon.getDeliveryName());
                    verifyAddress(expectedCon.getDeliveryAddress(), con.getDeliveryAddress(), "delivery address");
                }
            }
            assertThat(found).as("found consignment " + expectedCon.getTrackingId()).isTrue();
        }


    }


    @Then("^label request is sent to AusPost with:$")
    public void verifyLabelRequestSentToAusPost(final List<AusPostRequestInfo> expectedList) {

        final AuspostRequestDTO request = ServiceLookup.getDispatchLabelClient().getLastRequest();
        assertThat(request).isNotNull();

        assertThat(expectedList).isNotNull().hasSize(1);
        final AusPostRequestInfo expected = expectedList.get(0);

        final StoreDTO store = request.getStore();
        assertThat(store).as("store").isNotNull();

        final AddressDTO address = store.getAddress();
        verifyAddress(expected.getStoreAddress(), address, "store address");

        assertThat(store.getName()).isEqualTo(expected.getStore());
        assertThat(store.getMerchantLocationId()).isEqualTo(expected.getMerchantLocationId());

        assertThat(store.getLabel()).as("label").isNotNull();
        assertThat(store.getLabel().getLayout()).as("layout").isNotNull().isEqualTo(expected.getLayout());
        assertThat(Boolean.valueOf(store.getLabel().getBranding())).as("branding").isNotNull()
                .isEqualTo(Boolean.valueOf(expected.getBranding()));

        final ConsignmentDetailsDTO consignment = store.getConsignment();
        assertThat(address).as("consignment").isNotNull();
        assertThat(consignment.getTrackingId()).isEqualTo(expected.getTrackingId());

        final DeliveryAddressDTO deliveryAddress = consignment.getDeliveryAddress();
        verifyAddress(expected.getDeliveryAddress(), deliveryAddress, "delivery address");
        assertThat(deliveryAddress.getName()).as("delivery name").isEqualTo(expected.getDeliveryName());



    }

    private void verifyAddress(final String expected, final AddressDTO address, final String addressRef) {

        assertThat(address).as(addressRef).isNotNull();

        String addressAsString = "";

        if (StringUtils.isNotEmpty(address.getAddressLine1())) {
            addressAsString += address.getAddressLine1() + ", ";
        }
        addressAsString += address.getSuburb() + ", " + address.getState() + ", " + address.getPostcode();

        assertThat(addressAsString).as(addressRef).isEqualTo(expected);
    }

    @Then("^label request has parcels:$")
    public void verifyLabelRequestParcels(final List<Parcel> expectedParcelList) {

        final AuspostRequestDTO request = ServiceLookup.getDispatchLabelClient().getLastRequest();
        assertThat(request).isNotNull();

        final StoreDTO store = request.getStore();
        final ConsignmentDetailsDTO consignment = store.getConsignment();
        final List<ParcelDetailsDTO> parcels = consignment.getParcels();

        assertThat(parcels.size()).as("number of parcels").isEqualTo(expectedParcelList.size());

        int count = 0;
        for (final Parcel expectedParcel : expectedParcelList) {
            boolean found = false;
            count++;
            for (final ParcelDetailsDTO parcel : parcels) {
                if (Double.valueOf(parcel.getParcelDetails().getHeight()).equals(expectedParcel.getHeight())
                        && Double.valueOf(parcel.getParcelDetails().getWeight()).equals(expectedParcel.getWeight())
                        && Double.valueOf(parcel.getParcelDetails().getWidth()).equals(expectedParcel.getWidth())
                        && Double.valueOf(parcel.getParcelDetails().getLength()).equals(expectedParcel.getLength())) {
                    found = true;
                    break;
                }
            }
            assertThat(found).as("found parcel " + count).isTrue();
        }
    }


}

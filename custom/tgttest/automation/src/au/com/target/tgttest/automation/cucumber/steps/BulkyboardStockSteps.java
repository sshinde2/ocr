/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BulkyboardFacade;
import au.com.target.tgttest.automation.facade.CatalogUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.bean.BulkyboardStockUpdate;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps relating to bulkyboard stock updates from POS
 * 
 */
public class BulkyboardStockSteps {

    private List<AbstractTargetVariantProductModel> products;

    @Given("^store stock levels are:$")
    public void storeStockLevelsAre(final List<BulkyboardStockUpdate> levels) {

        // Configure mock to return response represented by table data
        ServiceLookup.getTargetStockUpdateClient().setResponse(BulkyboardFacade.generateResponseDto(levels));

        // The list of products in the request may as well match the set of products in the data table
        generateProductsListFromStockLevels(levels);
    }

    private void generateProductsListFromStockLevels(final List<BulkyboardStockUpdate> levels) {

        final Set<String> productCodes = new HashSet<>();
        for (final BulkyboardStockUpdate update : levels) {
            productCodes.add(update.getProduct());
        }

        products = new ArrayList<>();
        for (final String productCode : productCodes) {
            products.add((AbstractTargetVariantProductModel)ProductUtil.getStagedProductModel(productCode));
        }
    }

    @When("^update hybris stocklevels$")
    public void updateHybrisStocklevels() throws TargetIntegrationException {
        CatalogUtil.setSessionCatalogVersion();
        ServiceLookup.getTargetStoreStockService().updateStoreStockLevels(products);
    }

    @Then("^hybris stock levels are:$")
    public void hybrisStockLevelsAre(final List<BulkyboardStockUpdate> expectedStocks)
            throws Exception {

        for (final BulkyboardStockUpdate expected : expectedStocks) {
            final int actual = BulkyboardFacade.getStockForProductAtStore(expected.getStore(), expected.getProduct());

            assertThat(actual).isEqualTo(expected.getSoh());
        }
    }
}

/**
 *
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CncOrderUtil;
import au.com.target.tgttest.automation.facade.FeatureSwitchUtil;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttest.automation.mock.MockTargetCaShippingService;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import au.com.target.tgtwebmethods.ca.data.request.SubmitOrderShipmentRequest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author knemalik
 *
 */
public class ClickAndCollectOrderSteps {

    private static final String READY_FOR_PICKUP = "ReadyForPickup";
    private static final String PICKED_UP = "Pickedup";
    private static final String RETURNED_TO_FLOOR = "ReturnedToFloor";
    private static final String COMPLETE = "Complete";
    private static final String LETTERTYPE = "14";

    private ProcessState sendSmsStatus = null;

    private final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();

    @Given("^cnc sms notification retry interval is set to (\\d+) times every (\\d+) milliseconds$")
    public void setconfigurationValues(final int allowRetryNumber, final int retryInterval) {
        ServiceLookup.getSendClickAndCollectSmsNotificationaction().setAllowedRetries(allowRetryNumber);
        ServiceLookup.getSendClickAndCollectSmsNotificationaction().setRetryInterval(retryInterval);
    }

    @Given("^cnc order is ready for collection$")
    public void cncOrderIsReadyForCollection() throws Exception {
        CncOrderUtil.createCncOrder();
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("placeOrderProcess");
        BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("acceptOrderProcess");
        fulfilCncOrder();
    }

    private void fulfilCncOrder() throws Exception {
        SessionUtil.setAdminUserSession();
        CncOrderUtil.fulfillOrder();
    }

    @When("^hybris receives a cnc collection notification from pos$")
    public void receiveCncNotificationFromPos() throws InterruptedException {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        receivedReadyForPickUpNotification(orderModel);
    }

    @Then("^cnc sms notification for onsite is sent to the customer$")
    public void sendSmsNotificationForOrderOnSite() {
        assertThat(sendSmsStatus).isNotNull();
        assertThat(sendSmsStatus.toString()).isEqualTo(ProcessState.SUCCEEDED.toString());
    }

    @Then("^cnc sms notification for offsite is sent to the customer$")
    public void sendSmsNotificationForOrderOffSite() {
        assertThat(sendSmsStatus).isNotNull();
        assertThat(sendSmsStatus.toString()).isEqualTo(ProcessState.SUCCEEDED.toString());
    }

    @When("^webmethod is offline$")
    public void sendSmsNotificationWhenWebmethodIsOffline() {
        CncOrderUtil.setMockResponseStatus("UNAVAILABLE");
    }

    @Given("^sms service provider is not able to process SMS request$")
    public void smsServiceProviderProcessError() {
        CncOrderUtil.setMockResponseStatus("INVALID");
    }

    @Then("^hybris will not send sms notification$")
    public void hybrisWillNotSendSmsNotification() {
        assertThat(sendSmsStatus).isNotNull();
        assertThat(sendSmsStatus.toString()).isEqualTo(ProcessState.FAILED.toString());
    }

    @Then("^hybris retries to send sms notification$")
    public void retrySendSmsNotification() {
        assertThat(sendSmsStatus).isNotNull();
        assertThat(sendSmsStatus.toString()).isNotEqualTo(ProcessState.FAILED.toString());
    }

    @Then("^consignment status date for '(ReadyForPickup|Pickedup|ReturnedToFloor)' is set to '(TODAY|YESTERDAY|N/A|null)'$")
    public void theConsignmentStatus(final String status, final String day) {
        final TargetConsignmentModel consignment = getCNCConsignment();
        if (StringUtils.equalsIgnoreCase(status, READY_FOR_PICKUP)) {
            if (StringUtils.equalsIgnoreCase(day, "N/A") || StringUtils.equalsIgnoreCase(day, "null")) {
                assertThat(consignment.getReadyForPickUpDate()).isNull();
            }
            else {
                assertThat(
                        DateUtils.isSameDay(dateUtil.interpretStringAsDate(day), consignment.getReadyForPickUpDate()))
                                .isTrue();
            }
        }

        if (StringUtils.equalsIgnoreCase(status, PICKED_UP)) {
            if (StringUtils.equalsIgnoreCase(day, "N/A") || StringUtils.equalsIgnoreCase(day, "null")) {
                assertThat(consignment.getPickedUpDate()).isNull();
            }
            else {
                assertThat(DateUtils.isSameDay(dateUtil.interpretStringAsDate(day), consignment.getPickedUpDate()))
                        .isTrue();
            }
        }

        if (StringUtils.equalsIgnoreCase(status, RETURNED_TO_FLOOR)) {
            if (StringUtils.equalsIgnoreCase(day, "N/A") || StringUtils.equalsIgnoreCase(day, "null")) {
                assertThat(consignment.getReturnedToFloorDate()).isNull();
            }
            else {
                assertThat(
                        DateUtils.isSameDay(dateUtil.interpretStringAsDate(day), consignment.getReturnedToFloorDate()))
                                .isTrue();
            }
        }
    }

    @Then("^order last cnc notification time is set to 'TODAY'$")
    public void lastCNCNotificationtime() {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        assertThat(DateUtils.isSameDay(new Date(), orderModel.getCncLastNotification()))
                .isTrue();
    }

    @Given("^consignment status date for '(ReadyForPickup|Pickedup|ReturnedToFloor)' is already set to '(TODAY|YESTERDAY|N/A|null)'$")
    public void setPastNotificationDateInConsignment(final String notificationType, final String day) {
        final TargetConsignmentModel consignment = getCNCConsignment();

        if (StringUtils.equalsIgnoreCase(notificationType, READY_FOR_PICKUP)) {
            consignment.setReadyForPickUpDate(dateUtil.interpretStringAsDate(day));
        }

        if (StringUtils.equalsIgnoreCase(notificationType, PICKED_UP)) {
            consignment.setPickedUpDate(dateUtil.interpretStringAsDate(day));
        }

        if (StringUtils.equalsIgnoreCase(notificationType, RETURNED_TO_FLOOR)) {
            consignment.setReturnedToFloorDate(dateUtil.interpretStringAsDate(day));
        }
        ServiceLookup.getModelService().saveAll(consignment);
    }

    @When("^cnc notification received for '(ReadyForPickup|Pickedup|ReturnedToFloor)'$")
    public void cncNotificationReceived(final String notificationType) throws InterruptedException {
        final OrderModel orderModel = Order.getInstance().getOrderModel();

        if (StringUtils.equalsIgnoreCase(notificationType, READY_FOR_PICKUP)) {
            receivedReadyForPickUpNotification(orderModel);
        }

        if (StringUtils.equalsIgnoreCase(notificationType, PICKED_UP)) {
            ServiceLookup.getClickAndCollectnotificationFacade().notifyCncPickedUp(orderModel.getCode(),
                    String.valueOf(orderModel.getCncStoreNumber()));
            BusinessProcessUtil.waitForProcessToFinish("sendClickAndCollectPickedupNotificationProcess");
        }

        if (StringUtils.equalsIgnoreCase(notificationType, RETURNED_TO_FLOOR)) {
            ServiceLookup.getClickAndCollectnotificationFacade().notifyCncReturnedToFloor(orderModel.getCode(),
                    String.valueOf(orderModel.getCncStoreNumber()));
        }

    }

    @When("^fluent cnc notification received for '(ReadyForPickup|Pickedup|ReturnedToFloor)' and fluent order id '(.*)'$")
    public void fluentCncNotificationReceived(final String notificationType, final String fluentId)
            throws InterruptedException {
        FeatureSwitchUtil.switchOnTheFeature(TgtCoreConstants.FeatureSwitch.FLUENT);

        final OrderModel orderModel = Order.getInstance().getOrderModel();
        orderModel.setFluentId(fluentId);
        ServiceLookup.getModelService().save(orderModel);
        ServiceLookup.getModelService().refresh(orderModel);
        if (StringUtils.equalsIgnoreCase(notificationType, READY_FOR_PICKUP)) {
            receivedReadyForPickUpNotification(orderModel);
        }

        if (StringUtils.equalsIgnoreCase(notificationType, PICKED_UP)) {
            ServiceLookup.getClickAndCollectnotificationFacade().notifyCncPickedUp(orderModel.getCode(),
                    String.valueOf(orderModel.getCncStoreNumber()));
            BusinessProcessUtil.waitForProcessToFinish("sendClickAndCollectPickedupNotificationProcess");
        }

        if (StringUtils.equalsIgnoreCase(notificationType, RETURNED_TO_FLOOR)) {
            ServiceLookup.getClickAndCollectnotificationFacade().notifyCncReturnedToFloor(orderModel.getCode(),
                    String.valueOf(orderModel.getCncStoreNumber()));
            BusinessProcessUtil.waitForProcessToFinish("sendClickAndCollectReturnedToFloorNotificationProcess");
        }

        FeatureSwitchUtil.switchOffTheFeature(TgtCoreConstants.FeatureSwitch.FLUENT);
    }

    private void receivedReadyForPickUpNotification(final OrderModel orderModel) throws InterruptedException {
        ServiceLookup.getClickAndCollectnotificationFacade().triggerCNCNotification(orderModel.getCode(),
                String.valueOf(orderModel.getCncStoreNumber()),
                LETTERTYPE);
        sendSmsStatus = BusinessProcessUtil.waitForProcessToFinish("sendClickAndCollectNotificationProcess");
    }

    private TargetConsignmentModel getCNCConsignment() {
        final OrderModel orderModel = Order.getInstance().getOrderModel();
        final List<TargetConsignmentModel> consignments = ServiceLookup.getTargetConsignmentService()
                .getActiveDeliverToStoreConsignmentsForOrder(orderModel);
        return consignments.get(0);
    }

    @Then("^consignment event sent to fluent is:$")
    public void verifyFluentEvent(final List<Event> events) {
        final Event event = events.get(0);
        final Event eventStub = ServiceLookup.getFluentClientMock().getEventStub();
        assertThat(eventStub).isNotNull();
        assertThat(eventStub.getName()).isEqualTo(event.getName());
        assertThat(eventStub.getEntityType()).isEqualTo(event.getEntityType());
        assertThat(eventStub.getEntityId()).isEqualTo(getCNCConsignment().getCode());
        assertThat(eventStub.getEntitySubtype()).isEqualTo(event.getEntitySubtype());
    }

    @Then("^consignment event is not sent to fluent$")
    public void verifyFluentEventIsNull() {
        final Event eventStub = ServiceLookup.getFluentClientMock().getEventStub();
        assertThat(eventStub).isNull();
    }

    @Then("^order event sent to fluent is:$")
    public void verifyFluentOrderEvent(final List<Event> events) {
        final Event event = events.get(0);
        final Event eventStub = ServiceLookup.getFluentClientMock().getEventStub();
        assertThat(eventStub).isNotNull();
        assertThat(eventStub.getName()).isEqualTo(event.getName());
        assertThat(eventStub.getEntityType()).isEqualTo(event.getEntityType());
        assertThat(eventStub.getEntitySubtype()).isEqualTo(event.getEntitySubtype());
    }

    @Given("^a new cnc order with sales channel as '(eBay|Web)'$")
    public void cncOrderWithSalesChannel(final String salesChannel) throws Exception {
        if (StringUtils.equalsIgnoreCase("Web", salesChannel)) {
            cncOrderIsReadyForCollection();
        }
        else {
            new WebmethodsSteps().givenEbayOrder("pickup");
            fulfilCncOrder();
        }
    }

    @Then("^notification '(ReadyForPickup|Complete|N/A)' sent to eBay$")
    public void pickNotificationToEbay(final String status) {
        final SubmitOrderShipmentRequest request = MockTargetCaShippingService.getShipmentRequest();
        if (StringUtils.equalsIgnoreCase(status, READY_FOR_PICKUP)) {
            assertThat(request).isNotNull();
            assertThat(request.getShipmentList()).isNotNull();
            assertThat(request.getShipmentList().get(0).getFulfillmentStatus().equalsIgnoreCase(READY_FOR_PICKUP))
                    .isTrue();
        }
        else if (StringUtils.equalsIgnoreCase(status, COMPLETE)) {
            assertThat(request).isNotNull();
            assertThat(request.getShipmentList()).isNotNull();
            assertThat(request.getShipmentList().get(0).getFulfillmentStatus().equalsIgnoreCase(COMPLETE))
                    .isTrue();
        }
        else {
            assertThat(request).isNull();
        }
    }

    @Given("^consignment '(ReadyForPickup|Pickedup|AutoPickedup)' date is '(\\d+ days ago|TODAY|not saved)'$")
    public void consignmentAutoPickedupDateSavedAs(final String notificationType, final String day) {
        if (!StringUtils.equalsIgnoreCase(day, "not saved")) {
            final TargetConsignmentModel consignment = getCNCConsignment();
            if (StringUtils.equalsIgnoreCase(notificationType, READY_FOR_PICKUP)) {
                consignment.setReadyForPickUpDate(dateUtil.interpretStringAsDate(day));
            }
            else if (StringUtils.equalsIgnoreCase(notificationType, PICKED_UP)) {
                consignment.setPickedUpDate(dateUtil.interpretStringAsDate(day));
            }
            else {
                consignment.setPickedUpAutoDate(dateUtil.interpretStringAsDate(day));
            }
            ServiceLookup.getModelService().save(consignment);
        }
    }

    @Given("^consignment auto pickedup days are configured as '(\\d+)'$")
    public void consignmentReadyForPickupDayConfiguredAs(final int day) {
        ServiceLookup.getAutoUpdatePickedupjob().setAutoPickedUpWaitingDays(day);
    }

    @When("^autoPickedupCronJob runs$")
    public void autoPickedupCronJobRuns() throws InterruptedException {
        final CronJobModel cronJob = ServiceLookup.getModelService().create(CronJobModel.class);
        ServiceLookup.getAutoUpdatePickedupjob().perform(cronJob);
        BusinessProcessUtil.waitForProcessToFinish("sendClickAndCollectPickedupNotificationProcess");
    }

    @Then("^consignment 'AutoPickedupDate' is '(\\d+ days ago|TODAY|not saved)'$")
    public void consignmentAutoPickedupDateIs(final String day) {
        final TargetConsignmentModel consignment = getCNCConsignment();
        if (!StringUtils.equalsIgnoreCase(day, "not saved")) {
            assertThat(DateUtils.isSameDay(dateUtil.interpretStringAsDate(day), consignment.getPickedUpAutoDate()))
                    .isTrue();
        }
        else {
            assertThat(consignment.getPickedUpAutoDate()).isNull();
        }
    }

}
/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dao.TargetGlobalStoreFulfilmentCapabilitiesDao;
import au.com.target.tgtfulfilment.jobs.AutoRejectInStoreAckByWarehouse;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.bean.ConsignmentForStore;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author Siddharam
 *
 */
public class ConsignmentReRouteSteps {

    private static final String AUTOREJECT_INSTORE_WAVED_CONSIGNMENT_CRONJOB = "autoRejectInStoreWavedCronJob";

    private static final String AUTOREJECT_INSTORE_OPEN_CONSIGNMENT_CRONJOB = "autoRejectInStoreAckByWarehouseCronJob";

    @Given("^accepted orders timeout period is (\\d+) minutes$")
    public void givenMaxTimeToPickUp(final int acceptedOrderTimeout) {
        setMaxTimeToPickForGlobalFulfilment(acceptedOrderTimeout);
    }

    @Given("^order age tolerance is set to (\\d+) Minutes")
    public void givenTimeToPullBack(final int pullBackTolarance) throws InterruptedException {
        setPullBackTolerance(pullBackTolarance);
    }

    @Given("^order age tolerance is not set")
    public void givenTimeToPullBack() {
        setPullBackTolerance(0);
    }

    @Given("^some existing orders routed to store:$")
    public void givenOrdersWithConsignmentsForStore(final List<ConsignmentForStore> entries) throws Exception {
        for (final ConsignmentForStore consignmentEntry : entries) {
            final WarehouseModel warehouse = getWareHouse(consignmentEntry);
            final Date today = new Date();
            Date consignmentCreationTime = null;
            final Calendar calendar = Calendar.getInstance();
            if (consignmentEntry.getMinutesSinceCreated() > 0) {
                calendar.add(Calendar.MINUTE, -consignmentEntry.getMinutesSinceCreated());
                consignmentCreationTime = calendar.getTime();
            }
            final OrderModel order = ServiceLookup.getOrderCreationHelper().createOrder("click-and-collect",
                    consignmentEntry.getOrder(), true,
                    Integer.valueOf(00), Double.valueOf(00), "WEB");
            final TargetConsignmentModel consignment = ServiceLookup
                    .getConsignmentCreationHelper()
                    .createConsignment("a" + consignmentEntry.getOrder(), StringUtils.EMPTY,
                            ConsignmentStatus.valueOf(consignmentEntry.getConsignmentStatus()),
                            warehouse, getShippingAddress(warehouse),
                            consignmentCreationTime != null ? consignmentCreationTime : today, today, today, today,
                            today,
                            null, null, null, null, null, order);
            setAdditionalValues(consignmentEntry, consignment, order);
        }
    }

    @Given("^the excludedStates are '(.*)' and includedStates are '(.*)' for the pullback job$")
    public void setStatesListsInRejectInstoreOpenConsignmentsJob(final List<String> excludedStates,
            final List<String> includedStates) {
        final Predicate clearNulls = new Predicate() {

            @Override
            public boolean evaluate(final Object arg0) {
                return !"NULL".equals(arg0);
            }
        };
        CollectionUtils.filter(excludedStates, clearNulls);
        CollectionUtils.filter(includedStates, clearNulls);

        final AutoRejectInStoreAckByWarehouse autoRejectInStoreAckByWarehouse = ServiceLookup
                .getAutoRejectInStoreAckByWarehouse();
        autoRejectInStoreAckByWarehouse.setExcludedStates(excludedStates);
        autoRejectInStoreAckByWarehouse.setIncludedStates(includedStates);
    }

    @Then("^clear states config in the pullback job$")
    public void clearStatesConfigInRejectInstoreOpenConsignmentsJob() {
        final AutoRejectInStoreAckByWarehouse autoRejectInStoreAckByWarehouse = ServiceLookup
                .getAutoRejectInStoreAckByWarehouse();
        autoRejectInStoreAckByWarehouse.setExcludedStates(null);
        autoRejectInStoreAckByWarehouse.setIncludedStates(null);
    }

    @When("^accepted orders pull back process is initiated$")
    public void rejectInstoreWavedConsignmentsJobInitiated() throws InterruptedException {
        triggerCronJob(AUTOREJECT_INSTORE_WAVED_CONSIGNMENT_CRONJOB);
    }


    @When("^the open orders pull back process is initiated$")
    public void rejectInstoreOpenConsignmentsJobInitiated() throws InterruptedException {
        triggerCronJob(AUTOREJECT_INSTORE_OPEN_CONSIGNMENT_CRONJOB);
    }



    @Then("^orders are updated as follows$")
    public void thenOrderAreUpdatedAs(final List<ConsignmentForStore> entries) throws InterruptedException {
        for (final ConsignmentForStore consignmentEntry : entries) {
            final OrderModel order = ServiceLookup.getTargetOrderService().findOrderModelForOrderId(
                    consignmentEntry.getOrder());
            BusinessProcessUtil.waitForAllOrderProcessesToFinish(order);

            boolean secondConsignmentTestRequired = isSecondConsignmentTestRequired(consignmentEntry
                    .getConsignmentTwoStatus());
            final String orderCode = order.getCode();
            ServiceLookup.getModelService().refresh(order);
            for (final ConsignmentModel eachConsignment : order.getConsignments()) {
                ServiceLookup.getModelService().refresh(eachConsignment);
                if (secondConsignmentTestRequired
                        && StringUtils.contains(eachConsignment.getCode(), 'b')) {
                    assertThat(StringUtils.equalsIgnoreCase(eachConsignment.getWarehouse().getName(),
                            consignmentEntry.getConsignmentTwoWarehouse())
                            || (!StringUtils.equalsIgnoreCase(eachConsignment.getWarehouse().getName(), "Fastline")
                            && StringUtils.equals(consignmentEntry.getConsignmentTwoWarehouse(), "NO_CHANGE")))
                            .overridingErrorMessage(
                                    "Unexpected second warehouse found for the rerouted order:"
                                            + orderCode
                                            + "Actual Warehouse Name" +
                                            eachConsignment.getWarehouse().getName())
                            .isTrue();
                    assertThat(StringUtils.equalsIgnoreCase(eachConsignment.getStatus().getCode(),
                            consignmentEntry.getConsignmentTwoStatus())).overridingErrorMessage(
                            "Unexpected second consignment status found for the rerouted order:" + orderCode
                                    + "Actual Consignment status" +
                                    eachConsignment.getStatus().getCode())
                            .isTrue();
                    secondConsignmentTestRequired = false;
                }
                else {
                    assertThat(StringUtils.equalsIgnoreCase(eachConsignment.getStatus().getCode(),
                            consignmentEntry.getConsignmentOneStatus())).overridingErrorMessage(
                            "Unexpected  consignment status found for the order:" + orderCode
                                    + "Actual Consignment Status"
                                    + eachConsignment.getStatus().getCode())
                            .isTrue();
                    assertThat(StringUtils.equalsIgnoreCase(
                            ObjectUtils.toString(((TargetConsignmentModel)eachConsignment).getRejectReason(),
                                    "NULL"),
                            consignmentEntry.getConsignmentOneReason())).overridingErrorMessage(
                            "Unexpected  consignment RejectionReason found for the order:" + orderCode
                                    + "Actual Rejection Reason"
                                    + ((TargetConsignmentModel)eachConsignment).getRejectReason())
                            .isTrue();
                }
            }
            if (isSecondConsignmentTestRequired(consignmentEntry
                    .getConsignmentTwoStatus())) {
                assertThat(secondConsignmentTestRequired).overridingErrorMessage(
                        "second Consignment Not created for the order:" + orderCode).isFalse();
            }

            ServiceLookup.getModelService().removeAll(order.getConsignments());
            ServiceLookup.getModelService().remove(order);

        }
    }


    /**
     * @param consignmentStatus
     * @return isSecondConsignmentTestRequired
     */
    private boolean isSecondConsignmentTestRequired(final String consignmentStatus) {
        return (!StringUtils.equals("N/A", consignmentStatus));
    }

    @Given("^accepted orders timeout period is not configured$")
    public void givenMaxTimeToPickUpIsNull() {
        setMaxTimeToPickForGlobalFulfilment(0);
    }

    /**
     * @param consignmentEntry
     * @return the warehousemodel
     */
    private WarehouseModel getWareHouse(final ConsignmentForStore consignmentEntry) {
        return ServiceLookup.getWarehouseService().getWarehouseForCode(consignmentEntry.getWarehouse() + "Warehouse");
    }

    /**
     * @param consignmentEntry
     * @return the Date
     */
    private Date getWavedDate(final ConsignmentForStore consignmentEntry) {
        final Calendar wavedDated = Calendar.getInstance();
        if (StringUtils.isNumeric(consignmentEntry.getMinutesSinceWaved())) {
            final int minutesSinceWaved = Integer.parseInt(consignmentEntry.getMinutesSinceWaved());
            wavedDated.add(Calendar.MINUTE, -minutesSinceWaved);
            return wavedDated.getTime();
        }
        return null;
    }

    /**
     * @param acceptedOrderTimeout
     */
    private void setMaxTimeToPickForGlobalFulfilment(final int acceptedOrderTimeout) {
        final TargetGlobalStoreFulfilmentCapabilitiesDao storeFulfilmentCapabilitiesDao = ServiceLookup
                .getTargetGlobalStoreFulfilmentCapabilitiesDao();
        final GlobalStoreFulfilmentCapabilitiesModel globalStoreFulfilmentCapabilitiesModel = storeFulfilmentCapabilitiesDao
                .getGlobalStoreFulfilmentCapabilities();
        globalStoreFulfilmentCapabilitiesModel.setMaxTimeToPick(Integer.valueOf(acceptedOrderTimeout));
        ServiceLookup.getModelService().save(globalStoreFulfilmentCapabilitiesModel);
    }

    /**
     * @param pullBackTolarance
     */
    private void setPullBackTolerance(final int pullBackTolarance) {
        final TargetGlobalStoreFulfilmentCapabilitiesDao storeFulfilmentCapabilitiesDao = ServiceLookup
                .getTargetGlobalStoreFulfilmentCapabilitiesDao();
        final GlobalStoreFulfilmentCapabilitiesModel globalStoreFulfilmentCapabilitiesModel = storeFulfilmentCapabilitiesDao
                .getGlobalStoreFulfilmentCapabilities();
        globalStoreFulfilmentCapabilitiesModel.setPullBackTolerance(Integer.valueOf(pullBackTolarance));
        ServiceLookup.getModelService().save(globalStoreFulfilmentCapabilitiesModel);
    }

    /**
     * @param owner
     * @return the address
     */
    public static AddressModel getShippingAddress(final ItemModel owner) {
        final AddressModel shippingAddress = ServiceLookup.getModelService().create(TargetAddressModel.class);
        shippingAddress.setOwner(owner);
        ServiceLookup.getModelService().save(shippingAddress);
        return shippingAddress;
    }

    /**
     * @param consignmentEntry
     * @param consignment
     * @param order
     */
    private void setAdditionalValues(final ConsignmentForStore consignmentEntry,
            final TargetConsignmentModel consignment, final OrderModel order) {
        order.setDeliveryAddress(consignment.getShippingAddress());
        ServiceLookup.getModelService().save(order);
        consignment.setWavedDate(getWavedDate(consignmentEntry));
        ServiceLookup.getModelService().save(consignment);
    }


    /**
     * @param cronJobCode
     * @throws InterruptedException
     */
    private void triggerCronJob(final String cronJobCode) throws InterruptedException {
        final CronJobModel cronJob = ServiceLookup.getModelService().create(CronJobModel.class);
        if (AUTOREJECT_INSTORE_WAVED_CONSIGNMENT_CRONJOB.equals(cronJobCode)) {
            ServiceLookup.getAutoRejectNonPickedByWarehouse().perform(cronJob);
        }
        else {
            ServiceLookup.getAutoRejectInStoreAckByWarehouse().perform(cronJob);
        }
    }

}

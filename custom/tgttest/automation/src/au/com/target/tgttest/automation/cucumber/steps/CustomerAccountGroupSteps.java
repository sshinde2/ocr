/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import org.fest.assertions.Assertions;

import au.com.target.tgttest.automation.facade.CustomerUtil;
import au.com.target.tgttest.automation.facade.SalesApplicationUtil;
import au.com.target.tgttest.automation.facade.domain.Customer;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author pthoma20
 * 
 */
public class CustomerAccountGroupSteps {


    @Given("^that the customer doesn't have a Target account$")
    public void givenCustomerNotHavingTargetAccount() {
        //Do nothing
    }

    @When("^the customer creates a new account and logs in through '(.*)'$")
    public void createsAccount(final String salesApplication) throws InterruptedException, DuplicateUidException {
        SalesApplicationUtil.setSalesApplication(salesApplication);
        CustomerUtil.registeruser();
        CustomerUtil.loginCustomer();
    }

    @Then("^the customer account is created and is assigned to the '(.*)'$")
    public void thenCustomerAccountIsCreatedWithSepcifiedGroup(final String group) {
        Assertions.assertThat(CustomerUtil.isCurrentCustomerPartOfTheGroup(group));
        SalesApplicationUtil.resetSalesApplicationToDefault();
    }

    @Given("^that the customer is already a Target customer$")
    public void givenRegisteredTargetCustomer() {
        Customer.getInstance().setRegisteredCustomerData();
    }

    @When("^the customer logs in through '(.*)'$")
    public void customerLoginsInThroughSpecificSalesChannel(final String salesApplication) {
        SalesApplicationUtil.setSalesApplication(salesApplication);
        CustomerUtil.loginCustomer();
    }

    @Then("^the customer account is associated to the '(.*)'$")
    public void theCustomerAccountIsAssociatedToSpecificSalesGroup(final String group) {
        Assertions.assertThat(CustomerUtil.isCurrentCustomerPartOfTheGroup(group));
        SalesApplicationUtil.resetSalesApplicationToDefault();
    }


}

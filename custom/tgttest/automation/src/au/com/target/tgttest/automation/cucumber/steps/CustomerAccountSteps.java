/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.util.List;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtpayment.dto.CreditCard;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CustomerUtil;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.CreditCardData;
import au.com.target.tgttest.automation.facade.bean.CustomerAccountData;
import au.com.target.tgttest.automation.facade.domain.Customer;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author mjanarth
 * 
 */
public class CustomerAccountSteps {

    private static final String CUST_ADDRESS_LINE1 = "10/12 Emert St";
    private static final String CUST_ADDRESS_TOWN = "Wentworthville";
    private static final String CUST_ADDRESS_POST_CODE = "3215";

    private static final String UPDATED_CUST_FIRSTNAME = "newtest";
    private static final String UPDATED_CUST_EMAIL = "custemail@target.com.au";

    private boolean customerAlreadyExists = false;
    private boolean passwordMismatch = false;


    @Given("^that the user doesn't have a Target account$")
    public void givenUserNotHavingTargetAccount() {
        //Do nothing
    }

    @When("^the user creates a new account$")
    public void createsAccount() throws InterruptedException {
        try {
            CustomerUtil.registeruser();
        }
        catch (final DuplicateUidException e) {
            customerAlreadyExists = true;
        }

    }

    @Given("^an email address '(.*)' is present in hybris$")
    public void givenEmailIsPresent(final String email) throws InterruptedException {
        Customer.setCustomer(email);
        Customer.getInstance().initialize();
        Customer.getInstance().getCustomerData().setLogin(email);
        createsAccount();
        resetSubscriberKey();
    }

    @Given("^that the customer has a Target account$")
    public void givenRegisteredUser() {
        Customer.getInstance().setRegisteredCustomerData();
        CustomerUtil.loginCustomer();
    }

    @Given("^customer has saved ipg credit cards$")
    public void givenCustomerHasIpgSavedCreditCard(final List<CreditCardData> savedCreditCards) {
        givenRegisteredUser();
        CustomerUtil.setIpgSavedCreditCards(savedCreditCards);
        assertThat(CustomerUtil.getSavedCreditCards().size()).isEqualTo(savedCreditCards.size());
    }

    @When("^customer has saved addresses$")
    public void givenCustomerAddsAddress(final List<String> address) {
        givenRegisteredUser();
        for (final String addressValue : address) {
            final String[] addressFragments = addressValue.split(",");
            final TargetAddressData addressData = new TargetAddressData();
            addressData.setLine1(addressFragments[0]);
            addressData.setTown(addressFragments[1]);
            addressData.setPostalCode(addressFragments[2]);
            final CountryData countryData = new CountryData();
            countryData.setIsocode("AU");
            countryData.setName(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
            addressData.setCountry(countryData);
            addressData.setShippingAddress(true);
            addressData.setVisibleInAddressBook(true);
            ServiceLookup.getCheckoutResponseFacade().createAddress(addressData);
        }
    }

    @When("^customer '(.*)' logs in$")
    public void whenCustomerLogsIn(final String emailAddress) {
        CustomerUtil.loginCustomer(emailAddress);
    }

    @When("^the customer modifies password$")
    public void whenCustomerChangesPassword() throws InterruptedException {
        CustomerUtil.changePassword();
    }

    @When("^the customer modifies first name$")
    public void whenCustomerModifiesFirstName() throws DuplicateUidException {

        CustomerUtil.updateFirstName(UPDATED_CUST_FIRSTNAME);
    }

    @When("^the customer modifies emailAddress$")
    public void whenCustomerModifiesEmailAddress() throws DuplicateUidException {

        CustomerUtil.updateEmailAddress(UPDATED_CUST_EMAIL);
    }

    @When("^the customer modifies emailAddress with invalid password$")
    public void whenCustomerModifiesEmailAddressInvalidPasswd() throws DuplicateUidException {
        try {

            CustomerUtil.updateEmailAddressWithInvalidPasswd(UPDATED_CUST_EMAIL);
        }
        catch (final PasswordMismatchException e) {
            passwordMismatch = true;
        }
    }

    @When("^the user modifies the saved address$")
    public void whenUserModifiesAddress() {
        CustomerUtil.updateDefaultAddress(CUST_ADDRESS_LINE1, CUST_ADDRESS_TOWN, CUST_ADDRESS_POST_CODE);
    }

    @When("^the customer removes the address$")
    public void whenCustomerRemovesAddress() {
        CustomerUtil.removeCustomerAddress();
    }

    @When("^the customer deletes the primary card$")
    public void deletePrimaryCard() {
        final List<CCPaymentInfoData> savedCards = CustomerUtil.getSavedCreditCards();
        for (final CCPaymentInfoData card : savedCards) {
            if (card.isDefaultPaymentInfo()) {
                CustomerUtil.deleteSavedCard(card.getId());
                return;
            }
        }
    }

    @When("^the customer adds a new address$")
    public void whenCustomerAddsAddress() {
        Customer.getInstance().setNewCustomerAddress("31 Smith Street", "Wentworthville", "2145", true);
        CustomerUtil.addNewAddress();
    }

    @Then("^the customer account is not created$")
    public void thenCustomerNotCreated() {
        assertThat(customerAlreadyExists).isTrue();
    }

    @Then("^the customer account is created$")
    public void thenCustomerAccountCreatedandVerifyDetails() {
        CustomerUtil.loginCustomer();
        final CustomerAccountData customerData = CustomerUtil.getLoggedInCustomerData();
        assertThat(Customer.getInstance().getCustomerData().getFirstName()).isEqualTo(customerData.getFirstName());
        assertThat(Customer.getInstance().getCustomerData().getLastName()).isEqualTo(customerData.getLastName());
    }

    @Then("^the customer account is updated successfully$")
    public void thenCustomerAccountUpdated() {
        assertThat(CustomerUtil.getLoggedInCustomerData().getFirstName()).isEqualToIgnoringCase("newtest");
    }

    @Then("^the customer email Address is updated successfully$")
    public void thenCustomerEmailAddressUpdated() {
        assertThat(CustomerUtil.getLoggedInCustomerData().getLogin()).isEqualTo(UPDATED_CUST_EMAIL);
    }

    @Then("^the customer email Address is not updated$")
    public void thenCustomerEmailAddressNotUpdated() {
        assertThat(passwordMismatch).isEqualTo(true);
    }

    @Then("^the customer Address is updated successfully$")
    public void thenVerifyCustomerAddressIsUpdated() {
        final AddressEntry entry = CustomerUtil.getDefaultAddress();
        assertThat(entry.getAddressLine1()).isEqualTo(CUST_ADDRESS_LINE1);
        assertThat(entry.getPostalCode()).isEqualTo(CUST_ADDRESS_POST_CODE);
        assertThat(entry.getTown()).isEqualTo(CUST_ADDRESS_TOWN);
    }

    @Then("^the Address is removed from Customer Account$")
    public void thenVerifyAddressRemoved() {
        final AddressEntry entry = CustomerUtil.getDefaultAddress();
        assertThat(entry).isNull();
    }

    @Then("^the new Address gets added to customer account$")
    public void thenVerifyNewAddress() {
        final AddressEntry entry = CustomerUtil.getDefaultAddress();
        assertThat(entry.getAddressLine1()).isEqualTo(Customer.getInstance().getAddressEntry().getAddressLine1());
        assertThat(entry.getTown()).isEqualTo(Customer.getInstance().getAddressEntry().getTown());
        assertThat(entry.getPostalCode()).isEqualTo(Customer.getInstance().getAddressEntry().getPostalCode());
    }

    @Then("^the same unique subscriber key will be assigned to the record in hybris$")
    public void thenVerifySubscriberKey() {
        final TargetCustomerModel targetCustomer = (TargetCustomerModel)ServiceLookup.getUserService().getUserForUID(
                Customer.getInstance().getUid());
        assertThat(targetCustomer.getSubscriberKey()).isNotEmpty();
    }

    @SuppressWarnings("boxing")
    @Then("^customer will have saved ipg credit cards$")
    public void verifySavedCreditCard(final List<CreditCardData> expectedCards) {
        final List<CreditCard> savedCards = CustomerUtil.getSavedIpgCreditCards();
        final int size = savedCards.size();
        assertThat(size).isEqualTo(expectedCards.size());
        int i = 1;
        for (final CreditCard saved : savedCards) {
            final CreditCardData expected = expectedCards.get(size - i);
            assertThat(saved.getMaskedCard()).isEqualTo(expected.getCardNumber());
            assertThat(saved.getCardExpiry()).isEqualTo(expected.getCardExpiry());
            assertThat(saved.getDefaultCard()).isEqualTo(expected.isDefaultCard());
            assertThat(saved.getCardOnFile()).isEqualTo(expected.getCardOnFile());
            assertThat(saved.getFirstCredentialStorage()).isEqualTo(expected.getFirstCredentialStorage());

            i++;
        }
    }

    @SuppressWarnings("boxing")
    @Then("^customer will have no saved cards$")
    public void verifyNoSavedCreditCard() {
        final List<CCPaymentInfoData> savedCards = CustomerUtil.getSavedCreditCards();
        final int size = savedCards.size();
        assertThat(size).isEqualTo(0);
    }

    private void resetSubscriberKey() {
        final TargetCustomerModel targetCustomer = (TargetCustomerModel)ServiceLookup.getUserService().getUserForUID(
                Customer.getInstance().getUid());
        targetCustomer.setSubscriberKey(null);
        ServiceLookup.getModelService().save(targetCustomer);
        ServiceLookup.getModelService().refresh(targetCustomer);
    }

}

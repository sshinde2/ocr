/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.DealUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.bean.DealBreakPointEntry;
import au.com.target.tgttest.automation.facade.bean.DealParameters;
import au.com.target.tgttest.automation.facade.bean.DealProducts;
import au.com.target.tgttest.automation.facade.bean.DealReferenceData;
import au.com.target.tgttest.automation.facade.bean.ProductInCartWithDealInfo;
import cucumber.api.java.en.Given;


/**
 * Steps for setting up deals
 * 
 */
public class DealSteps {

    public static final String QUANTITY_BREAK_DEAL_CODE = "5002";
    public static final String QUANTITY_BREAK_CAT = "autodeal5002cat";

    @Given("^quantity break deal product '(.*)'$")
    public void setQuantityBreakDealProduct(final String product) {
        ProductUtil.setProductInCategory(product, QUANTITY_BREAK_CAT);
    }


    @Given("^quantity break deal break points:$")
    public void setQuantityBreakDealBreaks(final List<DealBreakPointEntry> breakPoints) {
        DealUtil.setQuantityBreakDealBreaks(QUANTITY_BREAK_DEAL_CODE, breakPoints);
    }

    @Given("^a buy-get deal:$")
    public void setABuyGetDeal() {
        final DealParameters buyget = new DealParameters(DealReferenceData.DEAL_NAME_BUYGET, "PercentOffEach",
                Double.valueOf(50.0), Integer.valueOf(1));
        final List<DealParameters> entries = new ArrayList<>();
        entries.add(buyget);
        setDealParameters(entries);
    }

    @Given("^set deal parameters:$")
    public void setDealParameters(final List<DealParameters> entries) {

        for (final DealParameters dealParams : entries) {
            DealUtil.updateDealParameters(dealParams);
        }
    }

    @Given("^cart entries with buy-get deal\\(s\\):$")
    public void givenCartEntriesWithBuyGetDeals(final List<ProductInCartWithDealInfo> entries) throws Exception {
        for (final ProductInCartWithDealInfo entry : entries) {
            if (entry.isDeal()) {
                entry.setDealRole(Character.valueOf('R'));
                entry.setDealName(DealReferenceData.DEAL_NAME_BUYGET);
            }
        }
        givenCartEntries(entries);
    }


    @Given("^cart entries with deals:$")
    public void givenCartEntries(final List<ProductInCartWithDealInfo> entries) throws Exception {

        final Map<String, DealProducts> mapDealProducts = new HashMap<String, DealProducts>();

        for (final ProductInCartWithDealInfo entry : entries) {

            CartUtil.addToCart(entry.getProduct(), entry.getQty());

            ProductUtil.updateProductPrice(entry.getProduct(), entry.getPrice().doubleValue());

            final String deal = entry.getDealName();
            if (StringUtils.isNotEmpty(deal)) {
                if (!mapDealProducts.containsKey(deal)) {
                    mapDealProducts.put(deal, new DealProducts(deal));
                }
                if (entry.getDealRole() != null && entry.getDealRole().charValue() == 'Q') {
                    mapDealProducts.get(deal).addQualifier(entry.getProduct());
                }
                if (entry.getDealRole() != null && entry.getDealRole().charValue() == 'R') {
                    mapDealProducts.get(deal).addReward(entry.getProduct());
                }
            }
        }

        // Set up the deal qualifiers and rewards
        for (final DealProducts dealProds : mapDealProducts.values()) {
            DealUtil.setDealQualifiers(dealProds.getDealName(), dealProds.getQualifiers());
            DealUtil.setDealRewards(dealProds.getDealName(), dealProds.getRewards());
        }

        CartUtil.recalculateSessionCart();
    }

}

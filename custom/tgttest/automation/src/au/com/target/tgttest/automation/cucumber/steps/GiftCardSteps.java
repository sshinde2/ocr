/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;

import au.com.target.tgtcore.model.GiftRecipientModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.giftcards.validate.GiftCardValidationErrorType;
import au.com.target.tgtfacades.order.data.GiftRecipientData;
import au.com.target.tgtfacades.order.data.TargetCartData;
import au.com.target.tgtfacades.order.data.TargetOrderEntryData;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.OrderDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ProductDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.RecipientDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.ConsignmentUtil;
import au.com.target.tgttest.automation.facade.DealUtil;
import au.com.target.tgttest.automation.facade.GiftCardUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.bean.ConsignmentEntryToWebmethodsInfo;
import au.com.target.tgttest.automation.facade.bean.ConsignmentToWebmethodsInfo;
import au.com.target.tgttest.automation.facade.bean.DealProducts;
import au.com.target.tgttest.automation.facade.bean.DropShipConsignmentEntryToWMInfo;
import au.com.target.tgttest.automation.facade.bean.GiftCardCartEntry;
import au.com.target.tgttest.automation.facade.bean.GiftCardData;
import au.com.target.tgttest.automation.facade.bean.GiftCardProductDetails;
import au.com.target.tgttest.automation.facade.bean.GiftCardRecipientData;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttinker.client.impl.MockSendToWarehouseRestClient;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author sbryan6
 *
 */
public class GiftCardSteps {

    private static final String DOLLAR_SIGN = "$";
    private static final String SAMPLE_EMAIL_ADDR = "pratik.kumar@uxcoxygen.com";
    private static final String SAMPLE_FIRST_NAME = "Pratik";
    private static final String SAMPLE_LAST_NAME = "Kumar";
    private static final String GIFT_CARD_PRODUCT_CODE = "PGC1000_iTunes_10";
    private static final String SAMPLE_TEXT = "Sample text for gift card.";

    private final Map<String, List<GiftRecipientModel>> giftCardModelEntries = new HashMap<String, List<GiftRecipientModel>>();

    private GiftCardValidationErrorType errorType;

    private List<GiftCardCartEntry> giftCardEntries;

    @Given("^gift card data are set up as:$")
    public void setupGiftCards(final List<GiftCardData> giftCards) {

        for (final GiftCardData data : giftCards) {

            GiftCardUtil.setGiftCardLimitations(data.getBrandID(), data.getMaxOrderQuantity(), data.getMaxOrderValue());
        }
    }

    @Given("^gift cards are added to cart with recipient details with deals:$")
    public void givenCartEntries(final List<GiftCardCartEntry> entries) throws Exception {

        final Map<String, DealProducts> mapDealProducts = new HashMap<String, DealProducts>();

        int qualifierMinQty = 0;

        for (final GiftCardCartEntry entry : entries) {

            validateAndAddToCart(entry);

            ProductUtil.updateProductPrice(entry.getProductCode(), entry.getPrice().doubleValue());

            final String deal = entry.getDealName();
            if (StringUtils.isNotEmpty(deal)) {
                if (!mapDealProducts.containsKey(deal)) {
                    mapDealProducts.put(deal, new DealProducts(deal));
                }
                if (entry.getDealRole() != null && entry.getDealRole().charValue() == 'Q') {
                    mapDealProducts.get(deal).addQualifier(entry.getProductCode());
                    qualifierMinQty = entry.getMinQty();
                }
                if (entry.getDealRole() != null && entry.getDealRole().charValue() == 'R') {
                    mapDealProducts.get(deal).addReward(entry.getProductCode());
                }
            }
        }

        // Set up the deal qualifiers and rewards
        for (final DealProducts dealProds : mapDealProducts.values()) {
            DealUtil.setDealQualifiers(dealProds.getDealName(), dealProds.getQualifiers(), qualifierMinQty);
            DealUtil.setDealRewards(dealProds.getDealName(), dealProds.getRewards());
        }

        CartUtil.recalculateSessionCart();
    }

    @When("^(?:a gift card is|gift cards are) added to cart with recipient details:$")
    public void addGiftCardToCart(final List<GiftCardCartEntry> giftCards)
            throws ProductNotFoundException, CommerceCartModificationException {
        Assertions.assertThat(giftCards).isNotNull().isNotEmpty();
        for (final GiftCardCartEntry giftCardCartEntry : giftCards) {
            validateAndAddToCart(giftCardCartEntry);
        }
        populateGiftCardRecipients();
    }

    @When("^a digital gift card with recipient details added to the cart returns '(.*)':$")
    public void addDigitalGiftCardToCartAsMixedProduct(final String error,
            final List<GiftCardCartEntry> giftCards)
                    throws ProductNotFoundException, CommerceCartModificationException {
        Assertions.assertThat(giftCards).isNotNull().isNotEmpty();
        for (final GiftCardCartEntry giftCardCartEntry : giftCards) {
            validateDigitalGiftCardAndAddToCart(giftCardCartEntry, error);
        }
    }

    @When("^recipient entry '(\\d+)' of the product '(.*)' is removed from cart$")
    public void removeGiftCardEntry(final Integer recipientEntry, final String productCode)
            throws NumberFormatException, Exception {
        final String id = getIdForEntry(recipientEntry, productCode);
        GiftCardUtil.removeRecipientEntry(productCode, recipientEntry.intValue(), id);
    }



    @Then("^the gift card validation is '(.*)'")
    public void verifyCartValidation(final String value) {
        if ("SUCCESS".equals(value)) {
            Assertions.assertThat(errorType).isNull();
        }
        else if ("QUANTITY".equals(value)) {
            Assertions.assertThat(errorType).isNotNull().isEqualTo(GiftCardValidationErrorType.QUANTITY);
        }
        else if ("VALUE".equals(value)) {
            Assertions.assertThat(errorType).isNotNull().isEqualTo(GiftCardValidationErrorType.VALUE);
        }
    }


    @Then("^gift card product details are:$")
    public void verifyGiftCardProductDetails(final List<GiftCardProductDetails> expectedEntries) {

        final TargetCartData actualCart = getSessionCartData();
        final List<OrderEntryData> actualEntries = actualCart.getEntries();

        assertThat(expectedEntries.size()).as("Number of cards").isEqualTo(actualEntries.size());

        for (int i = 0; i < expectedEntries.size(); i++) {
            verifyGiftCardEntry(actualEntries.get(i), expectedEntries.get(i));
        }
    }

    @Then("^gift recipient details for the product '(.*)' are:$")
    public void verifyGiftCardProductDetails(final String productCode,
            final List<GiftCardRecipientData> expectedRecipients) {

        final TargetCartData actualCart = getSessionCartData();
        final List<OrderEntryData> actualEntries = actualCart.getEntries();

        final TargetOrderEntryData entry = findOrderEntry(productCode, actualEntries);
        assertThat(entry).as("Entry for product " + productCode).isNotNull();

        final List<GiftRecipientData> actualRecipients = entry.getRecipients();

        assertThat(expectedRecipients.size()).as("Number of recipients").isEqualTo(actualRecipients.size());

        for (int i = 0; i < expectedRecipients.size(); i++) {
            verifyGiftCardRecipient(actualRecipients.get(i), expectedRecipients.get(i));
        }
    }

    private void verifyGiftCardEntry(final OrderEntryData actual, final GiftCardProductDetails expected) {
        assertThat(actual).isInstanceOf(TargetOrderEntryData.class);

        final TargetOrderEntryData actualEntryData = (TargetOrderEntryData)actual;
        assertThat(expected.getQuantity()).isEqualTo(actual.getQuantity().intValue());
        assertThat(actual.getProduct()).isInstanceOf(TargetProductData.class);

        final TargetProductData product = (TargetProductData)actual.getProduct();
        assertThat(product).isNotNull();
        assertThat(expected.getProductCode()).isEqualTo(product.getCode());
        assertThat(expected.isGiftCardFlag()).isEqualTo(product.isGiftCard());
        validatePriceData(expected, actualEntryData);
        assertThat(DOLLAR_SIGN + expected.getCardValue()).isEqualTo(product.getSize());

        verifyDeliveryModes(product.getDeliveryModes(), expected.getDeliveryModes());
    }

    /**
     * @param expected
     * @param actualEntryData
     */
    protected void validatePriceData(final GiftCardProductDetails expected,
            final TargetOrderEntryData actualEntryData) {
        if (expected.getBasePrice() != null) {
            assertThat(actualEntryData.getBasePrice()).isNotNull();
            assertThat(actualEntryData.getBasePrice().getValue()).isNotNull();
            assertThat(expected.getBasePrice()).isEqualTo(actualEntryData.getBasePrice().getValue().doubleValue());
        }
        if (expected.getTotalPrice() != null) {
            assertThat(actualEntryData.getTotalPrice()).isNotNull();
            assertThat(actualEntryData.getTotalPrice().getValue()).isNotNull();
            assertThat(expected.getTotalPrice()).isEqualTo(actualEntryData.getTotalPrice().getValue().doubleValue());
        }
        if (expected.getDiscountPrice() != null) {
            assertThat(actualEntryData.getDiscountPrice()).isNotNull();
            assertThat(actualEntryData.getDiscountPrice().getValue()).isNotNull();
            assertThat(expected.getDiscountPrice()).isEqualTo(
                    actualEntryData.getDiscountPrice().getValue().doubleValue());
        }
    }

    private void verifyDeliveryModes(final List<TargetZoneDeliveryModeData> actualDelModes, final String expected) {

        final String[] expectedModes = expected.split(",");
        assertThat(expectedModes.length).isEqualTo(actualDelModes.size());
        for (final TargetZoneDeliveryModeData actualDelMode : actualDelModes) {
            // All available
            assertThat(actualDelMode.isAvailable()).isTrue();
            assertThat(expected).contains(actualDelMode.getCode());
        }
    }

    private TargetOrderEntryData findOrderEntry(final String productCode, final List<OrderEntryData> entries) {

        for (final OrderEntryData entry : entries) {
            if (entry.getProduct().getCode().equalsIgnoreCase(productCode)) {
                assertThat(entry).isInstanceOf(TargetOrderEntryData.class);
                return (TargetOrderEntryData)entry;
            }
        }
        return null;
    }

    private void verifyGiftCardRecipient(final GiftRecipientData actual, final GiftCardRecipientData expected) {

        assertThat(actual.getFirstName()).isEqualToIgnoringCase(expected.getFirstName());
        assertThat(actual.getLastName()).isEqualToIgnoringCase(expected.getLastName());
        assertThat(actual.getMessageText()).isEqualToIgnoringCase(expected.getMessageText());
        assertThat(actual.getRecipientEmailAddress()).isEqualToIgnoringCase(expected.getEmail());
    }

    private TargetCartData getSessionCartData() {

        return CartUtil.getSessionCartData();
    }

    @Given("^webmethods returns '(SUCCESS|ERROR|UNAVAILABLE|success|error|unavailable)' for send to warehouse$")
    public void setWebmethodsSendToWarehouseResponse(final String responseType) {

        ServiceLookup.getSendToWarehouseRestClient().setResponseType(
                MockSendToWarehouseRestClient.SendToWarehouseResponseType.valueOf(responseType.toUpperCase()));
    }

    @Then("^consignment is sent to webmethods with:$")
    public void verifyConsignmentSentToWarehouse(final List<ConsignmentToWebmethodsInfo> expectedList) {

        assertThat(expectedList).isNotNull().hasSize(1);
        final ConsignmentToWebmethodsInfo expected = expectedList.get(0);

        // Consignment id and order id
        final ConsignmentDTO actualCons = getConsignmentSentToWarehouse();

        final TargetConsignmentModel cons = ConsignmentUtil.verifyAndGetSingleOrderConsignment();

        if (expected.checkCreatedConsignmentId()) {

            assertThat(actualCons.getId()).isEqualTo(cons.getCode());
        }
        else {
            assertThat(actualCons.getId()).isEqualTo(expected.getConsignmentId());
        }

        final OrderDTO actualOrder = actualCons.getOrder();
        assertThat(actualOrder).isNotNull();

        if (expected.checkCreatedOrderId()) {

            assertThat(actualOrder.getId()).isEqualTo(cons.getOrder().getCode());
        }

        assertThat(actualCons.getWarehouseId()).isEqualTo(expected.getWarehouseId());
        assertThat(actualCons.getShippingMethod()).isEqualTo(expected.getShippingMethod());
        assertThat(actualOrder.getCustomer().getFirstName()).isEqualTo(expected.getCustomerFirstName());
        assertThat(actualOrder.getCustomer().getLastName()).isEqualTo(expected.getCustomerLastName());

    }


    @Then("^consignment entries are sent to webmethods with:$")
    public void verifyConsignmentEntriesSentToWarehouse(final List<ConsignmentEntryToWebmethodsInfo> expectedList) {

        final List<ConsignmentEntryDTO> actualEntries = getConsignmentEntriesSentToWarehouse();
        assertThat(actualEntries).isNotEmpty().hasSize(expectedList.size());

        for (final ConsignmentEntryToWebmethodsInfo expected : expectedList) {

            final ConsignmentEntryDTO actualEntry = getConsignmentEntrySentToWarehouse(expected.getProductId());
            assertThat(actualEntry).as("Entry for " + expected.getProductId()).isNotNull();

            final ProductDTO actualProduct = actualEntry.getProduct();
            assertThat(actualProduct).isNotNull();
            assertThat(actualProduct.getBrandId()).isEqualTo(expected.getBrandId());
            assertThat(actualProduct.getDenomination()).isEqualTo(expected.getDenomination());
            assertThat(actualProduct.getGiftCardStyle()).isEqualTo(expected.getGiftCardStyle());
            assertThat(actualProduct.getName()).isEqualTo(expected.getName());
            assertThat(actualProduct.getProductId()).isEqualTo(expected.getGiftCardProductId());

        }
    }

    @Then("^dropship consignment entries are sent to webmethods with:$")
    public void verifyDropshipConsignmentEntriesSentToWarehouse(
            final List<DropShipConsignmentEntryToWMInfo> expectedList) {

        final List<ConsignmentEntryDTO> actualEntries = getConsignmentEntriesSentToWarehouse();
        assertThat(actualEntries).isNotEmpty().hasSize(expectedList.size());

        for (final DropShipConsignmentEntryToWMInfo expected : expectedList) {

            final ConsignmentEntryDTO actualEntry = getConsignmentEntrySentToWarehouse(expected.getProductId());
            assertThat(actualEntry).as("Entry for " + expected.getProductId()).isNotNull();

            final ProductDTO actualProduct = actualEntry.getProduct();
            assertThat(actualProduct).isNotNull();
            assertThat(actualProduct.getName()).isEqualTo(expected.getName());
            assertThat(actualProduct.getId()).isEqualTo(expected.getProductId());
            assertThat(actualProduct.getEan()).isEqualTo(expected.getEan());

        }
    }


    @Then("^gift recipient details are sent to webmethods for product '(.*)' with:$")
    public void verifyProductRecipientsSentToWarehouse(final String productCode,
            final List<GiftCardRecipientData> expectedList) {

        final ConsignmentEntryDTO actualEntry = getConsignmentEntrySentToWarehouse(productCode);
        assertThat(actualEntry).as("Entry for " + productCode).isNotNull();

        final List<RecipientDTO> actualRecipients = actualEntry.getRecipients();
        assertThat(actualRecipients).as("Recipient list").hasSize(expectedList.size());

        for (int i = 0; i < expectedList.size(); i++) {

            final RecipientDTO actualRecipient = actualRecipients.get(i);
            final GiftCardRecipientData expectedRecipient = expectedList.get(i);

            assertThat(actualRecipient.getEmailAddress()).isEqualTo(expectedRecipient.getEmail());
            assertThat(actualRecipient.getFirstName()).isEqualTo(expectedRecipient.getFirstName());
            assertThat(actualRecipient.getLastName()).isEqualTo(expectedRecipient.getLastName());
            assertThat(actualRecipient.getMessage()).isEqualTo(expectedRecipient.getMessageText());
        }
    }


    private ConsignmentDTO getConsignmentSentToWarehouse() {

        final SendToWarehouseRequest request = ServiceLookup.getSendToWarehouseRestClient().getLastRequest();
        assertThat(request).isNotNull();

        final ConsignmentDTO actualCons = request.getConsignment();
        assertThat(actualCons).isNotNull();
        return actualCons;
    }

    private List<ConsignmentEntryDTO> getConsignmentEntriesSentToWarehouse() {

        final ConsignmentDTO actualCons = getConsignmentSentToWarehouse();
        final List<ConsignmentEntryDTO> actualEntries = actualCons.getConsignmentEntries();

        return actualEntries;
    }

    private ConsignmentEntryDTO getConsignmentEntrySentToWarehouse(final String productCode) {

        final List<ConsignmentEntryDTO> actualEntries = getConsignmentEntriesSentToWarehouse();

        for (final ConsignmentEntryDTO actualEntry : actualEntries) {

            assertThat(actualEntry.getProduct()).isNotNull();
            final ProductDTO actualProduct = actualEntry.getProduct();
            if (actualProduct.getId().equals(productCode)) {

                return actualEntry;
            }
        }

        return null;

    }

    /**
     * @throws ProductNotFoundException
     * 
     */
    private void populateGiftCardRecipients() throws ProductNotFoundException {
        final CartModel checkoutCart = CheckoutUtil.getCheckoutCart();
        final List<AbstractOrderEntryModel> entries = checkoutCart.getEntries();
        Assertions.assertThat(entries).isNotNull().isNotEmpty();
        for (final AbstractOrderEntryModel entry : entries) {
            Assertions.assertThat(entry).isNotNull().isInstanceOf(CartEntryModel.class);
            final CartEntryModel cartEntry = (CartEntryModel)entry;
            final ProductModel product = cartEntry.getProduct();
            if (GiftCardUtil.isProductAGiftCard(product)) {
                giftCardModelEntries.put(product.getCode(), cartEntry.getGiftRecipients());
            }
        }
    }

    /**
     * @param recipientEntry
     * @param productCode
     * @return PK as String
     */
    private String getIdForEntry(final Integer recipientEntry, final String productCode) {
        final List<GiftRecipientModel> giftcardModels = giftCardModelEntries.get(productCode);
        Assertions.assertThat(giftcardModels).isNotNull().isNotEmpty();
        Assertions.assertThat(recipientEntry).isLessThan(giftcardModels.size());
        final GiftRecipientModel giftRecipientModel = giftcardModels.get(recipientEntry.intValue());
        Assertions.assertThat(giftRecipientModel).isNotNull();
        return giftRecipientModel.getPk().toString();
    }

    /**
     * @param giftCardCartEntry
     * @throws ProductNotFoundException
     * @throws CommerceCartModificationException
     */
    protected void validateAndAddToCart(final GiftCardCartEntry giftCardCartEntry)
            throws ProductNotFoundException, CommerceCartModificationException {
        errorType = GiftCardUtil.validateGiftCardCart(giftCardCartEntry.getProductCode());
        if (errorType == null) {
            GiftCardUtil.addToCart(giftCardCartEntry);
        }
    }

    /**
     * @param giftCardCartEntry
     * @param error
     * @throws ProductNotFoundException
     * @throws CommerceCartModificationException
     */
    protected void validateDigitalGiftCardAndAddToCart(final GiftCardCartEntry giftCardCartEntry,
            final String error)
                    throws ProductNotFoundException, CommerceCartModificationException {

        final CartModificationData cartModificationData = GiftCardUtil.digitalGiftCardAddToCart(giftCardCartEntry);

        assertThat(cartModificationData.getStatusCode()).isEqualTo(error);

    }

    @Given("^a consignment is sent to incomm$")
    public void createOrderForIncommWarehouse() throws Exception {

        giftCardEntries = new ArrayList<>();
        giftCardEntries.add(createSampleGiftCardEntry());

        addGiftCardToCart(giftCardEntries);
        Checkout.getInstance().setDeliveryMode("digital-gift-card");

        final OrderSteps orderSteps = new OrderSteps();
        orderSteps.placeOrder();
    }

    /**
     * Creating sample gift card entry
     * 
     * @return GiftCardCartEntry
     */
    private GiftCardCartEntry createSampleGiftCardEntry() {
        final GiftCardCartEntry giftCardEntry = new GiftCardCartEntry();
        giftCardEntry.setEmail(SAMPLE_EMAIL_ADDR);
        giftCardEntry.setFirstName(SAMPLE_FIRST_NAME);
        giftCardEntry.setLastName(SAMPLE_LAST_NAME);
        giftCardEntry.setProductCode(GIFT_CARD_PRODUCT_CODE);
        giftCardEntry.setMessageText(SAMPLE_TEXT);
        return giftCardEntry;
    }

    @Given("^incomm returns '(AlreadyExists|InsufficientInventory|BrandNotFound|ServiceUnavailable)' for order$")
    public void setErrorResponseFromWarehouse(final String errorResponse) {
        GiftCardUtil.setErrorResponseFromIncommWarehouse(errorResponse);
    }

    @Then("^error logged with '(.*)' as error code$")
    public void thenErrorIsLoggedWithCode(final String errorCode) {
        final String loggedMessage = ServiceLookup.getTargetWmSendToWarehouseLogger().getLoggedMessage();

        assertThat(loggedMessage).isNotNull();
        assertThat(loggedMessage.contains("errorCode=" + errorCode)).isTrue();
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgtfacades.response.data.ProductStockSearchResultResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.cucumber.holder.DataHolder;
import au.com.target.tgttest.automation.facade.FluentClientUtil;
import au.com.target.tgttest.automation.facade.InStoreStockUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author rmcalave
 *
 */
public class InStoreStockSteps {

    private final TargetStoreFinderStockFacade targetStoreFinderStockFacade = ServiceLookup
            .getTargetStoreFinderStockFacade();

    private Response response;
    private ProductStockSearchResultResponseData responseData;

    @Given("^the Post Code/Suburb field has been displayed$")
    public void thePostCodeSuburbFieldHasBeenDisplayed() {
        assertThat(DataHolder.getInstance().getTargetProductData().isShowStoreStockForProduct()).isTrue();
    }

    @Given("^the following stock levels are configured:$")
    public void theFollowingStockLevelsAreConfigured(final List<StockVisibilityItemResponseDto> stockLevels) {
        InStoreStockUtil.enableMocks(stockLevels);
        FluentClientUtil.setStoreStockMock(stockLevels);
    }

    @When("^the store stock API gets called$")
    public void callStoreStockService() {
        response = targetStoreFinderStockFacade.doSearchProductStock("P1001", "3030", null,
                null, null, null, true);
    }

    @When("^the user searches for stores in '(.*)'$")
    public void theUserSearchesForStoresIn(final String searchString) {
        final PageableData pd = new PageableData();
        pd.setCurrentPage(0);
        pd.setPageSize(5);
        response = targetStoreFinderStockFacade.doSearchProductStock(null, searchString, null,
                null, null, pd, true);

        if (response.getData() instanceof ProductStockSearchResultResponseData) {
            responseData = (ProductStockSearchResultResponseData)response.getData();
        }
    }

    @When("^the user searches for stores near store '(.*)'$")
    public void theUserSearchesForStoresNearStore(final int storeNumber) {
        final PageableData pd = new PageableData();
        pd.setCurrentPage(0);
        pd.setPageSize(5);

        response = targetStoreFinderStockFacade.doSearchProductStock(null, null, null,
                null, Integer.valueOf(storeNumber), pd, true);

        if (response.getData() instanceof ProductStockSearchResultResponseData) {
            responseData = (ProductStockSearchResultResponseData)response.getData();
        }
    }

    @When("^the user searches for stock in '(.*)'$")
    public void theUserSearchesForStockIn(final String searchString) {
        final TargetProductData targetProductData = DataHolder.getInstance().getTargetProductData();

        final PageableData pd = new PageableData();
        pd.setCurrentPage(0);
        pd.setPageSize(5);
        response = targetStoreFinderStockFacade.doSearchProductStock(targetProductData.getCode(), searchString, null,
                null, null, pd, true);

        if (response.getData() instanceof ProductStockSearchResultResponseData) {
            responseData = (ProductStockSearchResultResponseData)response.getData();
        }
    }

    @When("^the user searches for stock in stores near store '(.*)'$")
    public void theUserSearchesForStockInStoresNearStore(final int storeNumber) {
        final TargetProductData targetProductData = DataHolder.getInstance().getTargetProductData();

        final PageableData pd = new PageableData();
        pd.setCurrentPage(0);
        pd.setPageSize(5);

        response = targetStoreFinderStockFacade.doSearchProductStock(targetProductData.getCode(), null, null,
                null, Integer.valueOf(storeNumber), pd, true);

        if (response.getData() instanceof ProductStockSearchResultResponseData) {
            responseData = (ProductStockSearchResultResponseData)response.getData();
        }
    }

    @Then("^the location summary will be '(.*)'$")
    public void theLocationSummaryWillBe(final String locationSummary) {
        assertThat(responseData).isNotNull();
        assertThat(responseData.getSelectedLocation()).isEqualTo(locationSummary);
    }

    @Then("^the location summary will be blank$")
    public void theLocationSummaryWillBeBlank() {
        assertThat(responseData).isNotNull();
        assertThat(responseData.getSelectedLocation()).isNullOrEmpty();
    }

    @Then("^the closest stores will be '(.*)'$")
    public void theClosestStoresWillBe(final String closestStores) {
        final List<String> closestStoreStrings = splitAndTrim(closestStores);

        assertThat(responseData).isNotNull();

        assertThat(responseData.getStores()).hasSize(closestStoreStrings.size());

        assertThat(responseData.getStores()).onProperty("name").isEqualTo(closestStoreStrings);
    }

    @Then("^the stock levels will be '(.*)'$")
    public void theStockLevelsWillBe(final String stockLevels) {
        final List<String> stockLevelStrings = splitAndTrim(stockLevels);

        assertThat(responseData).isNotNull();

        assertThat(responseData.getStores()).hasSize(stockLevelStrings.size());

        final List<String> stockLevelLongs = new ArrayList<>();
        for (final String stockLevel : stockLevelStrings) {
            stockLevelLongs.add(stockLevel);
        }

        for (int i = 0; i < responseData.getStores().size(); i++) {
            final String actualStockLevel = responseData.getStores().get(i).getStockLevel();
            final String expectedStockLevel = stockLevelLongs.get(i);

            assertThat(actualStockLevel).isEqualTo(expectedStockLevel);
        }
    }

    @Then("^the stock levels will be missing$")
    public void theStockLevelsWillBeMissing() {
        assertThat(responseData).isNotNull();

        for (int i = 0; i < responseData.getStores().size(); i++) {
            final String actualStockLevel = responseData.getStores().get(i).getStockLevel();

            assertThat(actualStockLevel).isNull();
        }
    }

    @Then("^the availability time is present$")
    public void theAvailabilityTimeIsPresent() {
        assertThat(responseData.getAvailabilityTime()).isNotNull();
    }

    @Then("^it will return an error: '(.*)', '(.*)'$")
    public void verifyError(final String errorCode, final String errorMsg) {
        assertThat(response.getData().getError().getCode()).isEqualTo(errorCode);
        assertThat(response.getData().getError().getMessage()).isEqualTo(errorMsg);
    }

    private List<String> splitAndTrim(final String string) {
        final String[] splitStrings = StringUtils.split(string, ",");

        final List<String> trimmedStrings = new ArrayList<>();
        for (final String splitString : splitStrings) {
            trimmedStrings.add(splitString.trim());
        }

        return trimmedStrings;
    }
}

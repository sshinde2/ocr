/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import java.util.List;

import au.com.target.tgttest.automation.facade.bean.InstoreTransferProductEntry;
import au.com.target.tgttest.automation.facade.domain.InterStoreTransferUtil;
import cucumber.api.java.en.Then;


/**
 * @author Vivek
 *
 */
public class InterStoreTransferSteps {

    @Then("^inter store transfer contains fromStore as (\\d+)$")
    public void verifyFromStore(final Integer fromStore) {
        InterStoreTransferUtil.getLastInterStoreTransfer().verifyFromStore(fromStore);
    }

    @Then("^inter store transfer contains items as:$")
    public void verifyInterStoreTransferEntires(final List<InstoreTransferProductEntry> instoreTransferEntries) {
        InterStoreTransferUtil.getLastInterStoreTransfer().verifyInstoreTransferEntries(instoreTransferEntries);
    }

    @Then("^inter store transfer is not triggered$")
    public void verifyInterStoreNotTriggered() {
        InterStoreTransferUtil.getLastInterStoreTransfer().verifyInstoreTransferNotTriggered();
    }
}

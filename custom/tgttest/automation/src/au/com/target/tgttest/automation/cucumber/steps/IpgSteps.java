/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.order.impl.TargetFindOrderTotalPaymentMadeStrategy;
import au.com.target.tgtfacades.response.data.PaymentResponseData;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgtpayment.util.PriceCalculator;
import au.com.target.tgtpaymentprovider.ipg.data.CardDetails;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultData;
import au.com.target.tgtpaymentprovider.ipg.data.CardResultsData;
import au.com.target.tgtpaymentprovider.ipg.data.soap.QueryResponse;
import au.com.target.tgtpaymentprovider.ipg.data.soap.Response;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.TestInitialiser;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.bean.AccertifyCardInfo;
import au.com.target.tgttest.automation.facade.bean.CreditCardData;
import au.com.target.tgttest.automation.facade.bean.IpgPaymentEntry;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttinker.mock.payment.IpgClientMock;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author htan3
 *
 */
public class IpgSteps {

    private static Map<String, CreditCardData> cardMap = new HashMap<String, CreditCardData>();

    private final TargetFindOrderTotalPaymentMadeStrategy findOrderTotalPaymentMadeStrategy = new TargetFindOrderTotalPaymentMadeStrategy();

    @Given("^ipg check response is '(.*)'$")
    public void setIpgCheckResponseCode(final String responseCode) {
        ServiceLookup.getIpgClientMock().setCheckResponseCode(responseCode);
    }

    @Given("^ipg is down$")
    public void ipgDown() {
        ServiceLookup.getIpgClientMock().setServiceUnavailable(true);
    }

    @Given("^ipg capture payment is down$")
    public void ipgCapturePaymentDown() {
        ServiceLookup.getIpgClientMock().setServiceUnavailable(true);
        ServiceLookup.getIpgClientMock().setMockQuery(false);
    }

    @Given("^ipg submit payment response is '(.*)'$")
    public void setIpgSinglePaymentResponseCode(final String responseCode) {
        ServiceLookup.getIpgClientMock().setSinglePayResponseCode(responseCode);
    }

    @When("^the IPG refund will '(.*)' for the order$")
    public void setStatusIPGRefund(final String status) {
        ServiceLookup.getIpgClientMock().setRefundRequest(null);
        if ("succeed".equalsIgnoreCase(status)) {
            ServiceLookup.getIpgClientMock().setMockRefundResponse("Success");
        }
        else if ("fail".equalsIgnoreCase(status)) {
            ServiceLookup.getIpgClientMock().setMockRefundResponse("Decline");
            ServiceLookup.getIpgClientMock().setDeclineCode("159");
            ServiceLookup.getIpgClientMock().setDeclineMessage("Purchase not found");
        }
    }

    @Given("^ipg returns saved cards$")
    public void givenCustomerHasSavedCreditCardWithIpg(final List<CreditCardData> savedCreditCards) {
        final IpgClientMock ipgClientMock = ServiceLookup.getIpgClientMock();
        ipgClientMock.resetSavedCards();
        for (final CreditCardData creditCard : savedCreditCards) {
            ipgClientMock.addSavedCreditCard(creditCard.getCardType(), creditCard.getCardNumber(),
                    creditCard.getCardExpiry(), creditCard.getToken(), creditCard.getTokenExpiry(),
                    creditCard.getBin(),
                    creditCard.isDefaultCard(), creditCard.getCardOnFile(),creditCard.getFirstCredentialStorage());
        }
    }

    @Given("^giftcard/creditcard details:$")
    public void givenCardData(final List<CreditCardData> savedCreditCards) {
        cardMap.clear();
        for (final CreditCardData cardData : savedCreditCards) {
            cardMap.put(cardData.getPaymentId(), cardData);
            cardMap.put(cardData.getCardNumber(), cardData);
        }
    }

    @Given("customer submits payment for a combination of cards with amount:$")
    public void givenCustomerPaysWithCards(final List<IpgPaymentEntry> entries) {
        TestInitialiser.resetIPG();
        for (final IpgPaymentEntry entry : entries) {
            addPaymentInternal(entry.getPaymentEntry(), entry.isDeclined());
        }
    }

    @Given("customer submits payment for a card with amount '(.*)'")
    public void addPayment(final String payment) {
        addPaymentInternal(payment, true);
    }

    private void addPaymentInternal(final String payment, final boolean declined) {
        if (StringUtils.isNotEmpty(payment)) {
            if (payment.startsWith("GC")) {
                Checkout.getInstance().setPaymentMethod(CheckoutUtil.IPG_MIXED);
            }
            else if (cardMap.isEmpty()) {
                Checkout.getInstance().setPaymentMethod(CheckoutUtil.IPG);
            }
            else {
                Checkout.getInstance().setPaymentMethod(CheckoutUtil.IPG_MIXED);
            }
            final String[] paymentDetail = payment.split(",");
            assertThat(paymentDetail.length).isEqualTo(2);
            if (cardMap.containsKey(paymentDetail[0])) {
                final int amount = (int)(Double.parseDouble(paymentDetail[1]) * 100d);
                final IpgClientMock ipgClientMock = ServiceLookup.getIpgClientMock();
                final CreditCardData creditCardData = cardMap.get(paymentDetail[0]);
                creditCardData.setAmount(paymentDetail[1]);
                CardResultsData cardResultsData = ipgClientMock.getCardResultsData();
                if (cardResultsData == null) {
                    cardResultsData = new CardResultsData();
                    ipgClientMock.setCardResultsData(cardResultsData);
                }
                List<CardResultData> cardResults = cardResultsData.getCardResults();
                if (cardResults == null) {
                    cardResults = new ArrayList<>();
                    cardResultsData.setCardResults(cardResults);
                }
                final CardResultData cardResultData = new CardResultData();
                final CardDetails cardDetails = new CardDetails();
                cardDetails.setBin(creditCardData.getBin());
                cardDetails.setCardExpiry(creditCardData.getCardExpiry());
                cardDetails.setCardNumber(creditCardData.getCardNumber());
                cardDetails.setCardType(creditCardData.getCardType());
                cardDetails.setToken(creditCardData.getToken());
                cardDetails.setTokenExpiry(creditCardData.getTokenExpiry());
                cardResultData.setCardDetails(cardDetails);
                cardResultData.setTransactionDetails(IpgClientMock.populateTransactionDetails(
                        amount, creditCardData.getCardNumber(),
                        "Giftcard".equals(cardDetails.getCardType())));
                cardResults.add(cardResultData);

                if (declined) {
                    ipgClientMock.addCardResponse(cardDetails.getCardNumber(), Boolean.TRUE);
                }
            }
        }

    }

    @Given("^payment type is giftcard$")
    public void givenPaymentTypeIsGiftCard() {
        ServiceLookup.getIpgClientMock().setGiftCardPayment(true);
    }

    @SuppressWarnings("boxing")
    @Given("^there are gift card payment attempts:$")
    public void givenFailedGCAttempts(final List<AccertifyCardInfo> cardInfoList) {
        final QueryResponse queryResponse = new QueryResponse();
        final List<Response> responseList = new ArrayList<>();
        for (final AccertifyCardInfo cardInfo : cardInfoList) {
            final Response response = new Response();
            response.setAmount(""
                    + (new BigDecimal(cardInfo.getCardAuthorizedAmount()).multiply(new BigDecimal(100)).intValue()));
            response.setReceipt(cardInfo.getCardReceipt());
            response.setTrnTypeId("47");
            response.setTrnStatusId(cardInfo.isPaymentSuccess() ? "1" : "2");
            responseList.add(response);
        }
        queryResponse.setResponse(responseList);
        ServiceLookup.getIpgClientMock().setQueryResponse(queryResponse);
    }

    @SuppressWarnings("boxing")
    @Given("^credit card '(.*)' is declined$")
    public void givenCreditCardIsDeclined(final String cardId) {
        ServiceLookup.getIpgClientMock().addCardResponse(cardMap.get(cardId).getCardNumber(), Boolean.TRUE);
    }

    @SuppressWarnings("boxing")
    @Given("^refund for credit card '(.*)' is declined$")
    public void givenRefundCreditCardIsDeclined(final String cardId) {
        ServiceLookup.getIpgClientMock().addRefundResponse(cardMap.get(cardId).getCardNumber(), Boolean.TRUE);
    }

    @SuppressWarnings("boxing")
    @Given("^refund for gift card '(.*)' is declined$")
    public void givenRefundGiftcardIsDeclined(final String cardId) {
        ServiceLookup.getIpgClientMock().addVoidResponse(cardMap.get(cardId).getCardNumber(), Boolean.TRUE);
    }

    @Then("the payment details of the transaction will be recorded correctly")
    public void verifyOrderTransactionDetails() {
        final List<PaymentTransactionModel> transactions = Order.getInstance().getOrderModel().getPaymentTransactions();
        assertThat(transactions).isNotNull();
        assertThat(transactions.size()).isEqualTo(1);

        final List<PaymentTransactionEntryModel> entries = transactions.get(0).getEntries();
        for (final PaymentTransactionEntryModel entry : entries) {
            final PaymentInfoModel paymentInfo = entry.getIpgPaymentInfo();
            if (paymentInfo instanceof IpgCreditCardPaymentInfoModel) {
                final IpgCreditCardPaymentInfoModel ipgCCPaymentInfo = (IpgCreditCardPaymentInfoModel)paymentInfo;
                final CreditCardData expectedData = cardMap.get(ipgCCPaymentInfo.getNumber());
                assertThat(expectedData).isNotNull();
                assertThat(entry.getAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN)).isEqualTo(
                        new BigDecimal(expectedData.getAmount()).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                assertThat(ipgCCPaymentInfo.getBin()).isEqualTo(expectedData.getBin());
                assertThat(ipgCCPaymentInfo.getToken()).isEqualTo(expectedData.getToken());
                assertThat(ipgCCPaymentInfo.getTokenExpiry()).isEqualTo(expectedData.getTokenExpiry());
                assertThat(ipgCCPaymentInfo.getValidToMonth() + "/" + ipgCCPaymentInfo.getValidToYear()).isEqualTo(
                        expectedData.getCardExpiry());
            }
            else {
                final IpgGiftCardPaymentInfoModel ipgGCPaymentInfo = (IpgGiftCardPaymentInfoModel)paymentInfo;
                final CreditCardData expectedData = cardMap.get(ipgGCPaymentInfo.getMaskedNumber());
                assertThat(expectedData).isNotNull();
                assertThat(entry.getAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN)).isEqualTo(
                        new BigDecimal(expectedData.getAmount()).setScale(2, BigDecimal.ROUND_HALF_DOWN));
                assertThat(ipgGCPaymentInfo.getBin()).isEqualTo(expectedData.getBin());
            }
        }

    }

    @Then("all successful payment should be reversed")
    public void verifyPaidSoFarShouldBeZero() {
        assertThat(findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(CartUtil.getSessionCartModel())).isEqualTo(0d);
    }

    @Then("The payment of amount '(.*)' is not reversed")
    public void verifyPaidSoFar(final String expectedAmount) {
        final BigDecimal amount = new BigDecimal(expectedAmount).setScale(2, RoundingMode.HALF_UP);
        final Double paidSoFar = findOrderTotalPaymentMadeStrategy.getAmountPaidSoFar(CartUtil.getSessionCartModel());
        final BigDecimal realAmount = PriceCalculator.getAsPrice(paidSoFar);
        assertThat(realAmount).isEqualTo(amount);
    }

    @Then("order will have successful refund:$")
    public void verifySuccessfulRefund(final List<IpgPaymentEntry> refunds) {
        for (final IpgPaymentEntry refund : refunds) {
            verifySuccessfulRefund(refund.getPaymentEntry());
        }
    }

    @Then("order will have successful manual refund (.*)$")
    public void verifySuccessfulManualRefund(final String refundEntryInfo) {
        final String[] refundEntry = refundEntryInfo.split(",");
        final List<PaymentTransactionModel> transactions = Order.getInstance().getOrderModel()
                .getPaymentTransactions();
        final PaymentTransactionModel paymentTransactionModel = transactions.get(0);
        ServiceLookup.getModelService().refresh(paymentTransactionModel);
        final PaymentTransactionEntryModel entry = PaymentTransactionHelper.getLastPayment(transactions,
                ImmutableList.of(PaymentTransactionType.REFUND_STANDALONE),
                TransactionStatus.ACCEPTED);
        Assert.assertEquals(new BigDecimal(refundEntry[1]).setScale(2, RoundingMode.HALF_UP), entry.getAmount()
                .setScale(2, RoundingMode.HALF_UP));
        Assert.assertEquals(refundEntry[0], entry.getReceiptNo());
    }

    @Then("order has a successful refund '(.*)'$")
    public void verifySuccessfulRefund(final String paymentEntry) {
        if (StringUtils.isNotEmpty(paymentEntry)) {
            boolean findMatch = false;
            final String[] paymentDetail = paymentEntry.split(",");
            assertThat(paymentDetail.length).isEqualTo(2);
            final List<PaymentTransactionModel> transactions = Order.getInstance().getOrderModel()
                    .getPaymentTransactions();
            assertThat(transactions).isNotNull();
            assertThat(transactions.size()).isEqualTo(1);

            final String expectedCardNumber = cardMap.get(paymentDetail[0]).getCardNumber();
            final String amount = paymentDetail[1];
            String curCardNumber;
            final PaymentTransactionModel paymentTransactionModel = transactions.get(0);
            ServiceLookup.getModelService().refresh(paymentTransactionModel);
            for (final PaymentTransactionEntryModel entry : paymentTransactionModel.getEntries()) {
                final PaymentInfoModel ipgPaymentInfo = entry.getIpgPaymentInfo();
                if (ipgPaymentInfo instanceof IpgCreditCardPaymentInfoModel) {
                    curCardNumber = ((IpgCreditCardPaymentInfoModel)ipgPaymentInfo).getNumber();
                }
                else {
                    curCardNumber = ((IpgGiftCardPaymentInfoModel)ipgPaymentInfo).getMaskedNumber();
                }
                if (PaymentTransactionType.REFUND_FOLLOW_ON.equals(entry.getType())
                        && TransactionStatus.ACCEPTED.toString().equals(entry.getTransactionStatus()) &&
                        curCardNumber.equals(expectedCardNumber) &&
                        entry.getAmount().compareTo(new BigDecimal(amount)) == 0) {
                    findMatch = true;
                    break;
                }
            }
            Assert.assertTrue("Could not find a matching refund for payment entry " + paymentEntry, findMatch);
        }
    }

    @Given("^reverse giftcard process retry interval is set to (\\d+) times every (\\d+) milliseconds$")
    public void setconfigurationValues(final int allowRetryNumber, final int retryInterval) {
        ServiceLookup.getReverseGiftCardAction().setAllowedRetries(allowRetryNumber);
        ServiceLookup.getReverseGiftCardAction().setRetryInterval(retryInterval);
    }

    @Then("^the payment method selection is saved on the cart$")
    public void verifyPaymentMethodOnCart() {
        final CartModel cartModel = CartUtil.getSessionCartModel();
        final Checkout checkout = Checkout.getInstance();
        assertThat(cartModel.getPaymentMode().getCode()).isEqualTo(checkout.getPaymentMethod());
        assertThat(cartModel.getPaymentInfo()).isInstanceOf(IpgPaymentInfoModel.class);
    }

    @Then("^the IPG session token and iframe URL are obtained$")
    public void verifyIpgTokenAndIFrameUrl() {
        final CartModel cartModel = CartUtil.getSessionCartModel();
        final Checkout checkout = Checkout.getInstance();
        final IpgPaymentInfoModel ipgPaymentInfo = (IpgPaymentInfoModel)cartModel.getPaymentInfo();
        final PaymentResponseData responseData = (PaymentResponseData)checkout.getLastResponse().getData();
        assertThat(responseData.getIframeUrl()).contains(ipgPaymentInfo.getToken());
        assertThat(responseData.getIframeUrl()).contains(cartModel.getUniqueKeyForPayment());
        assertThat(responseData.getIpgToken()).isEqualTo(ipgPaymentInfo.getToken());
        assertThat(responseData.getSessionId()).isEqualTo(cartModel.getUniqueKeyForPayment());
    }
}

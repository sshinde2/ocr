/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Assert;

import au.com.target.tgtfacades.looks.data.LookDetailsData;
import au.com.target.tgtfacades.marketing.data.SocialMediaProductsListData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.ProductUtil;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author Nandini
 *
 */
public class LooksSteps {

    private static final int SOCIAL_MEDIA_PRODUCTS_COUNT = 2;

    private List<LookDetailsData> looksDataList;

    private SocialMediaProductsListData socialMediaProducts;

    @When("^product details is retrieved for (?:colour|size) variant product '(.*)'$")
    public void whenUserOnPDPPage(final String productId) {
        final ProductModel productModel = ProductUtil.getBaseProduct(ProductUtil
                .getProductModel(productId));
        final int maxSize = ServiceLookup.getConfigService().getConfiguration()
                .getInt("tgtstorefront.product.looks.maximium.size");
        looksDataList = ServiceLookup.getTargetLookPageFacade().getActiveLooksForProduct(productModel, maxSize);
    }

    @Then("^the looks in the grid are '(.*)'$")
    public void looksInGrid(final String looksDisplayed) {
        if (!looksDisplayed.equalsIgnoreCase("N/A")) {
            final String[] looks = looksDisplayed.split(",");
            Assert.assertEquals(looks.length, looksDataList.size());
        }
        else {
            Assert.assertNull(looksDataList);
        }
    }

    @When("^social media products are retrieved for mobile apps$")
    public void whenFetchSocialMediaProductsList() {
        socialMediaProducts = ServiceLookup.getTargetSocialMediaProductsFacade()
                .getSocialMediaProductsByCreationTimeDescending(SOCIAL_MEDIA_PRODUCTS_COUNT);
    }

    @Then("^the social media product instagram URLs are '(.*)'$")
    public void socialMediaProducts(final List<String> instagramUrls) {
        Assertions.assertThat(socialMediaProducts.getTargetSocialMediaProductsList()).onProperty("url")
                .containsExactly(instagramUrls.toArray());
    }
}

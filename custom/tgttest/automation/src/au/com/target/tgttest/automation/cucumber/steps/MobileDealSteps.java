/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.promotions.model.AbstractDealModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;

import au.com.target.tgtfacades.deals.data.TargetDealData;
import au.com.target.tgtfacades.deals.data.TargetDealsData;
import au.com.target.tgtfacades.offers.data.TargetMobileOfferHeadingData;
import au.com.target.tgtmarketing.model.TargetMobileOfferHeadingModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CatalogUtil;
import au.com.target.tgttest.automation.facade.DealUtil;
import au.com.target.tgttest.automation.facade.bean.MobileDealParameters;
import au.com.target.tgttest.automation.facade.bean.MobileOfferHeadingsParameter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for setting up Mobile deals
 * 
 */
public class MobileDealSteps {

    private TargetDealsData targetDealsData;


    @Given("^the following deals are setup in the system with following details:$")
    public void givenTheDealsAreSetup(final List<MobileDealParameters> mobileDealParametersList) {
        CatalogUtil.setSessionCatalogVersion();
        for (final MobileDealParameters mobileDealParameters : mobileDealParametersList) {
            final AbstractDealModel dealModel = DealUtil.getDealModelByCode(mobileDealParameters.getDealId());


            dealModel.setAvailableForMobile(mobileDealParameters.getAvailableForMobile());

            dealModel.setLongTitle(mobileDealParameters.getLongTitle());

            dealModel.setFeatured(mobileDealParameters.getFeatured());

            final List<String> offerHeadings = mobileDealParameters.getOfferHeadingsAsList();
            if (CollectionUtils.isNotEmpty(offerHeadings)) {
                final List<TargetMobileOfferHeadingModel> targetOfferHeadingModelList = new ArrayList<>();
                for (final String offerHeading : offerHeadings) {
                    final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel = DealUtil
                            .findOfferHeadingForCode(offerHeading);
                    if (null != targetMobileOfferHeadingModel) {
                        targetOfferHeadingModelList.add(targetMobileOfferHeadingModel);
                    }
                }
                dealModel.setTargetMobileOfferHeadings(targetOfferHeadingModelList);
            }
            if (MobileDealParameters.FUTURE.equalsIgnoreCase(mobileDealParameters.getStartDate())) {

                assertThat((dealModel.getStartDate().after(new Date()))).isTrue();
            }
            else {
                assertThat((dealModel.getStartDate().before(new Date()))).isTrue();
            }
            dealModel.setEnabled(mobileDealParameters.getEnabled());

            ServiceLookup.getModelService().save(dealModel);
        }
    }

    @When("^the deals applicable for mobile are being fetched$")
    public void fetchDealsForMobile() {
        targetDealsData = ServiceLookup.getTargetDealsfacade().getAllMobileActiveDealsWithOfferHeadings();

    }

    @Then("^the retrieved data will contain the following deals:$")
    public void fetchedDealsForMobileWillContainThePassedDeals(
            final List<MobileDealParameters> mobileDealParametersList) {
        assertThat(targetDealsData).isNotNull();
        final List<TargetDealData> targetDealDataList = targetDealsData.getTargetDeals();
        assertThat(targetDealDataList).isNotNull();
        final List<String> dealIdsFound = new ArrayList<>();
        for (final TargetDealData targetDealData : targetDealDataList) {
            for (final MobileDealParameters mobileDealParameters : mobileDealParametersList) {
                assertThat(targetDealData).isNotNull();
                assertThat(targetDealData.getCode()).isNotNull();
                if (targetDealData.getCode().equals(mobileDealParameters.getDealId())) {
                    dealIdsFound.add(targetDealData.getCode());
                    assertThat(mobileDealParameters.getAvailableForMobile()).isEqualTo(
                            targetDealData.getAvailableForMobile());
                    assertThat(mobileDealParameters.getLongTitle()).isEqualTo(targetDealData.getLongTitle());
                    assertThat(mobileDealParameters.getEnabled()).isEqualTo(targetDealData.getEnabled());
                    assertThat(mobileDealParameters.getFeatured()).isEqualTo(targetDealData.getFeatured());
                    final List<String> mobileOfferHeadingCodeExpected = mobileDealParameters.getOfferHeadingsAsList();
                    final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingsResultList = targetDealData
                            .getTargetMobileOfferHeadings();
                    assertThat(targetMobileOfferHeadingsResultList).isNotEmpty();
                    for (final TargetMobileOfferHeadingData targetMobileOfferHeading : targetMobileOfferHeadingsResultList) {
                        assertThat(mobileOfferHeadingCodeExpected).contains(targetMobileOfferHeading.getCode());
                        assertThat(targetMobileOfferHeading.getCode()).isNotNull();
                        assertThat(targetMobileOfferHeading.getColour()).isNotNull();
                        assertThat(targetMobileOfferHeading.getName()).isNotNull();
                    }
                    assertThat(targetDealData.getPageTitle()).isNotNull();
                    assertThat(targetDealData.getStartTimeFormatted()).isNotNull();
                    assertThat(targetDealData.getEndDate().after(new Date())).isTrue();
                    assertThat(targetDealData.getStartDate().before(new Date())).isTrue();
                }
            }
        }
        for (final MobileDealParameters mobileDealParameters : mobileDealParametersList) {
            assertThat(dealIdsFound).contains(mobileDealParameters.getDealId());
        }
    }

    @Then("^will not contain the following deals with id:$")
    public void fetchedDealsForMobileWillNotContainThePassedDeals(
            final List<String> mobileDealIdList) {
        assertThat(targetDealsData).isNotNull();
        final List<TargetDealData> targetDealDataList = targetDealsData.getTargetDeals();
        assertThat(targetDealDataList).isNotNull();
        for (final TargetDealData targetDealData : targetDealDataList) {
            for (final String dealId : mobileDealIdList) {
                assertThat(targetDealData).isNotNull();
                assertThat(targetDealData.getCode()).isNotNull();
                if (targetDealData.getCode().equals(dealId)) {
                    Assert.fail("Deal which should not get retrieved found in response" + targetDealData.getCode());
                }
            }
        }
    }


    @Given("^the following mobile offer headings are setup in the system with following details:$")
    public void givenTheOfferHeadingsArePresentInTheSystem(final List<String> offerHeadings) {
        for (final String offerHeading : offerHeadings) {
            final TargetMobileOfferHeadingModel targetMobileOfferHeadingModel = DealUtil
                    .findOfferHeadingForCode(offerHeading);
            assertThat(targetMobileOfferHeadingModel).isNotNull();
        }
    }

    @Then("^the retrieved data will contain the following mobile offer headings:$")
    public void fetchedDealsForMobileWillContainThePassedOfferHeadings(
            final List<MobileOfferHeadingsParameter> mobileOfferHeadingsParameterList) {
        assertThat(targetDealsData).isNotNull();
        final List<TargetMobileOfferHeadingData> targetMobileOfferHeadingDataList = targetDealsData
                .getTargetMobileOfferHeadings();
        final List<String> mobileOfferHeadingsFound = new ArrayList<>();
        assertThat(targetMobileOfferHeadingDataList).isNotNull();
        for (final TargetMobileOfferHeadingData targetMobileOfferHeadingData : targetMobileOfferHeadingDataList) {
            assertThat(targetMobileOfferHeadingData.getCode()).isNotNull();
            for (final MobileOfferHeadingsParameter mobileOfferHeadingsParameter : mobileOfferHeadingsParameterList) {
                if (mobileOfferHeadingsParameter.getCode().equals(targetMobileOfferHeadingData.getCode())) {
                    assertThat(mobileOfferHeadingsParameter.getColour()).isEqualTo(
                            targetMobileOfferHeadingData.getColour());
                    assertThat(mobileOfferHeadingsParameter.getName())
                            .isEqualTo(targetMobileOfferHeadingData.getName());
                    mobileOfferHeadingsFound.add(targetMobileOfferHeadingData.getCode());
                }
            }
        }
        for (final MobileOfferHeadingsParameter mobileOfferHeadingsParameter : mobileOfferHeadingsParameterList) {
            assertThat(mobileOfferHeadingsFound).contains(mobileOfferHeadingsParameter.getCode());
        }

    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.PosPriceUpdateUtil;
import au.com.target.tgttest.automation.facade.PreviousPermanentPriceUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.bean.POSPriceEntry;
import au.com.target.tgttest.automation.facade.domain.PosProductsDto;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * 
 * 
 * @author cwijesu1
 * 
 */
public class POSPriceUpdateSteps {


    @Given("^product (.*) in Hybris price update has promoEvent (.*)$")
    public void productInHybrisPriceUpdateHasPromoEvent(final String prouductId, final boolean flag) {
        PosPriceUpdateUtil.setPosProductsDto(prouductId, flag);
    }


    @When("^Hybris POS price update runs$")
    public void hybrisPriceUpdateRun() {
        SessionUtil.userLogin("esbpriceupdate");
        ServiceLookup.getPosPriceImportIntegrationFacade().importProductPriceFromPos(
                PosProductsDto.getInstance().getIntegrationPosProductsDto().getProducts());
    }


    @Then("^the Hybris TargetPriceRow promoEvent flag is set (.*)$")
    public void veritytheHybrisTargetPriceRowPromoEventFlag(final boolean flag) {
        final Boolean promoEvent = PosPriceUpdateUtil.getPricePromoEventFromProductId(PosProductsDto.getInstance()
                .getIntegrationPosProductsDto().getProducts().get(0).getItemCode());
        assertThat(flag).isEqualTo(promoEvent);
    }

    @Given("^POS updates price for product '(.*)':$")
    public void createPOSIntegrationData(final String variantCode, final List<POSPriceEntry> posPriceEntries) {
        PreviousPermanentPriceUtil.createPOSIntegrationData(variantCode, posPriceEntries);
    }

    @Given("^POS sets the product as '(.*)'$")
    public void posUpdatesProductasBanned(final String prodStatus) {
        PosProductsDto.getInstance().getIntegrationPosProductsDto().getProducts().get(0)
                .setRemoveFromSale(prodStatus.equalsIgnoreCase("banned"));
    }

    @Then("^the status for '(.*)' is '(.*)'$")
    public void verifyProductStatus(final String productCode, final String status) {
        final ProductModel product = ProductUtil.getStagedProductModel(productCode);
        assertThat(product.getApprovalStatus().getCode()).isEqualTo(status);
    }

    @Then("^Hybris variantCode '(.*)' set to approved$")
    public void setProductHybrisStatus(final String productCode) {
        final ProductModel product = ProductUtil.getStagedProductModel(productCode);
        product.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        ServiceLookup.getModelService().save(product);
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttest.automation.facade.domain.StoreEmployee;
import cucumber.api.java.en.When;


/**
 * @author ssundara
 * 
 */
public class PayPalHereSteps {

    @When("^store employee is '(.*)'$")
    public void setStore(final String uid) {
        StoreEmployee.setStoreEmployee(uid);
    }


    @When("^place order with paypalhere")
    public void startOrderWithPaypal() throws Exception {

        Checkout.getInstance().setPaymentMethod("paypalhere");
        CheckoutUtil.startPlaceOrder();

        final OrderModel orderModel = CheckoutUtil.finishOrder();
        Order.setOrder(orderModel);
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;

import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.PinPadUtil;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttest.automation.facade.domain.Order;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps specific to PinPad
 * 
 */
public class PinPadSteps {

    private String orderId;

    @When("^start order with pinpad store (.*)$")
    public void startOrderWithPinpad(final int store) throws Exception {

        // Remember cart id we start with
        orderId = CheckoutUtil.getCheckoutCart().getCode();

        // Set up checkout for pinpad payment
        Checkout.getInstance().setStoreNumber(store);
        Checkout.getInstance().setPaymentMethod("pinpad");

        CheckoutUtil.startPlaceOrder();
    }

    @When("^pinpad sends payment update with success '(.*)'$")
    public void pinpadSendsPaymentUpdate(final boolean success) {

        PinPadUtil.updatePinPadPayment(orderId, success);
    }

    @When("^finish order$")
    public void finishOrder() {

        final OrderModel orderModel = CheckoutUtil.finishOrder();
        Order.setOrder(orderModel);
    }

    @Then("^cart pinpad payment info is empty$")
    public void verifyCartPaymentInfoIsEmpty() {

        verifyPinPadPaymentInfoIsEmpty(CheckoutUtil.getCheckoutCart());
    }

    @Then("^order pinpad payment info is empty$")
    public void verifyOrderPaymentInfoIsEmpty() {

        verifyPinPadPaymentInfoIsEmpty(Order.getInstance().getOrderModel());
    }

    private void verifyPinPadPaymentInfoIsEmpty(final AbstractOrderModel abstractOrder) {

        final PaymentInfoModel info = abstractOrder.getPaymentTransactions().get(0).getInfo();

        assertThat(info).as("payment info").isInstanceOf(PinPadPaymentInfoModel.class);

        final PinPadPaymentInfoModel pinpadInfo = (PinPadPaymentInfoModel)info;

        PinPadUtil.verifyPaymentInfoIsEmpty(pinpadInfo);
    }


    @Then("^order pinpad payment info is populated$")
    public void verifyOrderPaymentInfoIsPopulated() {

        final PaymentInfoModel info = Order.getInstance().getOrderModel()
                .getPaymentTransactions().get(0).getInfo();

        assertThat(info).as("payment info").isInstanceOf(PinPadPaymentInfoModel.class);

        final PinPadPaymentInfoModel pinpadInfo = (PinPadPaymentInfoModel)info;

        assertThat(pinpadInfo).as("Pinpad info").isNotNull();

        PinPadUtil.verifyPaymentInfoIsPopulated(pinpadInfo);
    }


}

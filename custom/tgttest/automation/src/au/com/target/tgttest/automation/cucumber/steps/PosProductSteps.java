/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtsale.product.data.POSProduct;
import au.com.target.tgtsale.product.dto.request.ProductRequestDto;
import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.PosProductData;
import au.com.target.tgttest.automation.facade.bean.PosRequest;
import au.com.target.tgttest.automation.mock.MockTargetPOSProductClient;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for pos product feature
 * 
 */
public class PosProductSteps {

    private POSProduct result;


    @Given("^pos product data:$")
    public void givenPosProductData(final List<PosProductData> fields) {
        final PosProductData posData = fields.get(0);

        // Set the expected response in the mock corresponding to this data
        // Assumes we're only dealing with one store
        final ProductResponseDto response = new ProductResponseDto();
        response.setDescription(posData.getDesc());
        response.setItemcode(posData.getItemcode());
        response.setPrice(posData.getPrice());
        response.setWasPrice(posData.getWas());
        response.setErrorMessage(posData.getErrorCode());
        response.setSuccess(StringUtils.isEmpty(posData.getErrorCode()));

        MockTargetPOSProductClient.addResponse(posData.getEan(), response);
    }

    @When("^get pos product (.*) from store (\\d+)$")
    public void getPosProductFromStore(final String ean, final Integer store) {

        result = ServiceLookup.getTargetPOSProductService().getPOSProductDetails(store, ean);
    }

    @Then("^pos request is empty$")
    public void posRequestEmpty() {
        assertThat(MockTargetPOSProductClient.getRequest()).isNull();
    }

    @Then("^pos request is$")
    public void posRequestIs(final List<PosRequest> requestFields) {

        final PosRequest expectedRequest = requestFields.get(0);
        final ProductRequestDto request = MockTargetPOSProductClient.getRequest();

        assertThat(request.getEan()).isEqualTo(expectedRequest.getEan());
        assertThat(request.getStoreNumber()).isEqualTo(Integer.toString(expectedRequest.getStore()));
        assertThat(request.getStoreState()).isEqualTo(expectedRequest.getState());
    }

    @Then("^result product info is:$")
    public void resultProductInfo(final List<PosProductData> fields) {

        final PosProductData expectedResult = fields.get(0);


        compareStrings(result.getDescription(), expectedResult.getDesc());
        compareStrings(result.getItemCode(), expectedResult.getItemcode());
        compareStrings(result.getError(), expectedResult.getErrorCode());
        assertThat(result.getPriceInCents()).isEqualTo(expectedResult.getPrice());
        assertThat(result.getWasPriceInCents()).isEqualTo(expectedResult.getWas());
    }

    /**
     * Allow null to be equivalent to empty
     * 
     * @param actual
     * @param expected
     */
    private void compareStrings(final String actual, final String expected) {

        if (StringUtils.isEmpty(actual)) {
            assertThat(StringUtils.isEmpty(expected)).isEqualTo(true);
        }
        else {
            assertThat(actual).isEqualTo(expected);
        }
    }

}

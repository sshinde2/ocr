/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.PostCodeGroupModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetPostCodeGroupZDMVRestrictionModel;
import au.com.target.tgtcore.model.TargetProductTypeQuantityZDMVRestrictionModel;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.response.data.CartDetailResponseData;
import au.com.target.tgtfacades.response.data.CheckoutOptionsResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CatalogUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.bean.DeliveryModeAvailability;
import au.com.target.tgttest.automation.facade.bean.DeliveryModeRestrictionData;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author siddharam
 *
 */
public class PostCodeRestrictionSteps {

    public static final String NOTAPPLICABLE = "n/a";
    private List<AbstractTargetZoneDeliveryModeValueModel> valueModelList;

    private final List<AbstractTargetZoneDeliveryModeValueModel> newlyCreatedValueModelList = new ArrayList<>();

    @Given("^Postcodes allocated for the catchment area and also delivery fee set up based on product type and quantity.$")
    public void givenPostAllocatedToCatchmentArea(
            final List<DeliveryModeRestrictionData> deliveryModeRestrictionsData) {
        CatalogUtil.setSessionCatalogVersion();
        int bulkyPriority = 999999;
        int normalPriority = 888888;
        for (final DeliveryModeRestrictionData data : deliveryModeRestrictionsData) {

            final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel = ServiceLookup
                    .getModelService().create(RestrictableTargetZoneDeliveryModeValueModel.class);
            setupCommonData(bulkyPriority, normalPriority, data, restrictableTargetZoneDeliveryModeValueModel);

            final TargetProductTypeQuantityZDMVRestrictionModel targetProductTypeQuantityZDMVRestriction = ServiceLookup
                    .getModelService().create(TargetProductTypeQuantityZDMVRestrictionModel.class);
            setQuantity(targetProductTypeQuantityZDMVRestriction, data.getQty());
            targetProductTypeQuantityZDMVRestriction.setProductType(getProductType(data
                    .getProductType()));
            targetProductTypeQuantityZDMVRestriction
                    .setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
            ServiceLookup.getModelService().save(targetProductTypeQuantityZDMVRestriction);
            if (!NOTAPPLICABLE.equalsIgnoreCase(data.getCatchmentArea())) {

                final TargetPostCodeGroupZDMVRestrictionModel targetPostCodeGroupZDMVRestriction = ServiceLookup
                        .getModelService().create(TargetPostCodeGroupZDMVRestrictionModel.class);
                targetPostCodeGroupZDMVRestriction.setPostCodeGroup(getPostCodeGroup(data.getCatchmentArea()));
                targetPostCodeGroupZDMVRestriction
                        .setRestrictableDeliveryModeValue(restrictableTargetZoneDeliveryModeValueModel);
                ServiceLookup.getModelService().save(targetPostCodeGroupZDMVRestriction);
            }
            newlyCreatedValueModelList.add(restrictableTargetZoneDeliveryModeValueModel);

            bulkyPriority--;
            normalPriority--;
        }
    }


    private void setupCommonData(final int bulkyPriority, final int normalPriority,
            final DeliveryModeRestrictionData data,
            final AbstractTargetZoneDeliveryModeValueModel zoneDeliveryModeValue) {
        zoneDeliveryModeValue.setDeliveryMode((ZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService()
                .getDeliveryModeForCode(CheckoutUtil.HOME_DELIVERY_CODE));

        zoneDeliveryModeValue.setPriority(Integer.valueOf(getPriority(data.getProductType(),
                bulkyPriority, normalPriority)));
        zoneDeliveryModeValue.setZone(ServiceLookup.getZoneDeliveryModeService().getZoneForCode(
                "australia"));
        zoneDeliveryModeValue
                .setCurrency(ServiceLookup.getCommonI18nService().getCurrency("AUD"));
        zoneDeliveryModeValue
                .setMinimum(Double.valueOf(1));
        zoneDeliveryModeValue.setValue(Double.valueOf(StringUtils.remove(data.getDeliveryFee(), "$")));
        if (StringUtils.isNotEmpty(data.getLowestDeliveryFee())) {
            zoneDeliveryModeValue.setLowestPossibleValue(Double.valueOf(StringUtils.remove(data.getLowestDeliveryFee(),
                    "$")));
        }
        else {
            zoneDeliveryModeValue.setLowestPossibleValue(null);
        }
        ServiceLookup.getModelService().save(zoneDeliveryModeValue);
        ServiceLookup.getModelService().refresh(zoneDeliveryModeValue);
    }

    private int getPriority(final String productType, final int bulkyPriority, final int normalPriority) {
        if (productType.equalsIgnoreCase("bulky1")) {

            return bulkyPriority;
        }

        return normalPriority;
    }


    private Collection<PostCodeGroupModel> getPostCodeGroup(
            final String catchmentArea) {
        final Collection<PostCodeGroupModel> groups = new HashSet<>();
        final PostCodeGroupModel postCodeGroupModel = ServiceLookup
                .getModelService().create(PostCodeGroupModel.class);
        postCodeGroupModel.setCatchmentArea(catchmentArea);
        groups.add(ServiceLookup.getFlexibleSearchService().getModelByExample(postCodeGroupModel));
        return groups;
    }

    private Collection<ProductTypeModel> getProductType(final String productType) {

        final Collection<ProductTypeModel> types = new HashSet<>();
        types.add(ServiceLookup.getProductTypeService().getByCode(productType));
        return types;
    }

    private void setQuantity(
            final TargetProductTypeQuantityZDMVRestrictionModel targetProductTypeQuantityZDMVRestriction,
            final String qauntity) {
        switch (qauntity) {
            case "1 or more":
                targetProductTypeQuantityZDMVRestriction.setMinQuantity(Integer.valueOf(1));

                break;
            case "3 or more":
                targetProductTypeQuantityZDMVRestriction.setMinQuantity(Integer.valueOf(3));
                break;

            default:
                targetProductTypeQuantityZDMVRestriction.setMinQuantity(Integer.valueOf(1));
                targetProductTypeQuantityZDMVRestriction.setMaxQuantity(Integer.valueOf(2));
                break;
        }

    }

    @Given("^a cart with product '(.*)' and with quantity '(.*)'$")
    public void givenCartWithProducts(final String productCode, final String qty)
            throws NumberFormatException, CommerceCartModificationException {
        final String[] productCodes = productCode.split(",");
        final String[] quantities = qty.split(",");
        for (int i = 0; i < productCodes.length; i++) {
            CartUtil.addToCart(productCodes[i], Long.parseLong(quantities[i]));
        }
    }

    @Given("^the postcode allocated for the cart is '(.*)'$")
    public void givenPostCodeAllocatedToCart(final String postCode) {
        ServiceLookup.getSessionService().setAttribute(TgtCoreConstants.SESSION_POSTCODE, postCode);
    }

    @Given("^delivery fee for cart in spc is '(.*)'$")
    public void givenDeliveryFeeForCartInSPC(final String deliveryFee) {
        if (StringUtils.isNotEmpty(deliveryFee)) {
            final Response response = ServiceLookup.getCheckoutResponseFacade().getCartSummary();
            final CartDetailResponseData responseData = (CartDetailResponseData)response.getData();
            final Double expectedDeliveryCost = Double.valueOf(deliveryFee.replace("$", ""));
            final PriceData cartDeliveryFee = responseData.getDeliveryFee();
            assertThat(cartDeliveryFee.getValue().doubleValue()).isEqualTo(
                    expectedDeliveryCost.doubleValue());
        }
    }

    @When("^the delivery fee for the cart is fetched$")
    public void whenDeliveryFeeFetched() {
        valueModelList = ServiceLookup.getTargetDeliveryService()
                .getAllApplicableDeliveryValuesForModeAndOrder(
                        (ZoneDeliveryModeModel)ServiceLookup
                                .getTargetDeliveryService()
                                .getDeliveryModeForCode(CheckoutUtil.HOME_DELIVERY_CODE),
                        CartUtil.getSessionCartModel());
    }

    @When("^delivery modes for the cart is fetched$")
    public void whenDeliveryFeeForSPCFetched() {
        final Response response = ServiceLookup.getCheckoutResponseFacade().getApplicableDeliveryModes();
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        final List<TargetZoneDeliveryModeData> applicableDeliveryModeData = responseData.getDeliveryModes();
        Checkout.getInstance().setApplicableDeliveryModeData(applicableDeliveryModeData);
    }

    @Given("^pre-order message for express-delivery is '(.*)'$")
    public void expressDeliveryPreOrderMessage(final String expressDeliveryLongMessage) {
        if (!expressDeliveryLongMessage.isEmpty()) {
            final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel = ServiceLookup
                    .getModelService().create(RestrictableTargetZoneDeliveryModeValueModel.class);
            setupPreOrderLongMessageConfig(expressDeliveryLongMessage, restrictableTargetZoneDeliveryModeValueModel);
        }
    }


    /**
     * This method will preconfig the express delivery message for pre-order
     * 
     * @param expressDeliveryLongMessage
     * @param restrictableTargetZoneDeliveryModeValueModel
     */
    private void setupPreOrderLongMessageConfig(final String expressDeliveryLongMessage,
            final RestrictableTargetZoneDeliveryModeValueModel restrictableTargetZoneDeliveryModeValueModel) {
        final ZoneDeliveryModeModel deliverMode = (ZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService().getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE);
        deliverMode.setValues(null);
        ServiceLookup.getModelService().save(deliverMode);
        ServiceLookup.getModelService().refresh(deliverMode);
        restrictableTargetZoneDeliveryModeValueModel.setDeliveryMode((ZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService().getDeliveryModeForCode(CheckoutUtil.EXPRESS_DELIVERY_CODE));

        restrictableTargetZoneDeliveryModeValueModel.setPriority(Integer.valueOf(0));
        restrictableTargetZoneDeliveryModeValueModel
                .setZone(ServiceLookup.getZoneDeliveryModeService().getZoneForCode(
                        "express"));
        restrictableTargetZoneDeliveryModeValueModel.setCurrency(ServiceLookup.getCommonI18nService()
                .getCurrency("AUD"));
        restrictableTargetZoneDeliveryModeValueModel.setMinimum(Double.valueOf(1));
        restrictableTargetZoneDeliveryModeValueModel.setValue(Double.valueOf(15));
        restrictableTargetZoneDeliveryModeValueModel.setPreOrderLongMessage(expressDeliveryLongMessage);
        ServiceLookup.getModelService().save(restrictableTargetZoneDeliveryModeValueModel);
        ServiceLookup.getModelService().refresh(restrictableTargetZoneDeliveryModeValueModel);
    }


    @Then("^applicable deliveryModes are '(.*)'$")
    public void verifyDeliveryModes(final String deliveryModesString) {
        final String[] deliveryModes = deliveryModesString.split(",");
        final List<TargetZoneDeliveryModeData> applicableDeliveryModeData = Checkout.getInstance()
                .getApplicableDeliveryModeData();
        final List<TargetZoneDeliveryModeData> availableDeliveryModes = new ArrayList<>();
        for (final TargetZoneDeliveryModeData targetZoneDeliveryModeData : applicableDeliveryModeData) {
            if (targetZoneDeliveryModeData.isAvailable()) {
                availableDeliveryModes.add(targetZoneDeliveryModeData);
            }
        }
        assertThat(availableDeliveryModes).onProperty("code").containsExactly((Object[])deliveryModes);
    }

    @Then("^the express delivery mode values for the pre-order are :$")
    public void verifyExpressDeliveryPreOrderMessage(final List<DeliveryModeAvailability> deliveryModeList) {

        final List<TargetZoneDeliveryModeData> applicableDeliveryModeData = Checkout.getInstance()
                .getApplicableDeliveryModeData();

        for (final TargetZoneDeliveryModeData uideliveryModes : applicableDeliveryModeData) {
            if (uideliveryModes.getCode().equals(deliveryModeList.get(0).getCode())
                    && BooleanUtils.isTrue(CartUtil.getSessionCartModel().getContainsPreOrderItems())) {
                assertThat(uideliveryModes.getLongDescription())
                        .isEqualTo(deliveryModeList.get(0).getLongDescription());
            }
        }
    }

    @When("^delivery address selected in spc is '(.*)' for delivery mode '(.*)'$")
    public void whenDeliveryAddressSelectedForSPC(final String deliveryAddress, final String deliveryMode) {
        ServiceLookup.getCheckoutResponseFacade().setDeliveryMode(deliveryMode);

        if (StringUtils.isNotEmpty(deliveryAddress)) {
            final Response response = ServiceLookup.getCheckoutResponseFacade().getDeliveryAddresses();
            final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
            final List<AddressData> userAddresses = responseData.getDeliveryAddresses();
            final String[] addressFragments = deliveryAddress.split(",");
            AddressData selectedAddress = null;
            for (final AddressData address : userAddresses) {
                if (address.getLine1().equals(addressFragments[0]) && address.getTown().equals(addressFragments[1])
                        && address.getPostalCode().equals(addressFragments[2])) {
                    selectedAddress = address;
                    break;
                }
            }

            if (null != selectedAddress) {
                final Response addressChangedResponse = ServiceLookup.getCheckoutResponseFacade()
                        .selectDeliveryAddress(
                                selectedAddress.getId());
                if (addressChangedResponse.getData() instanceof CartDetailResponseData) {
                    final CartDetailResponseData addressChangedResponseData = (CartDetailResponseData)addressChangedResponse
                            .getData();
                    Checkout.getInstance().setDeliveryFeeChanged(
                            addressChangedResponseData.getDeliveryFeeChanged().booleanValue());
                }
                Checkout.getInstance().setLastResponse(addressChangedResponse);
            }
        }
        else {
            ServiceLookup.getSessionService().removeAttribute(TgtCoreConstants.SESSION_POSTCODE);
        }
    }

    @Then("delivery fee for cart is '(.*)' and it has changed '(.*)'$")
    public void checkDeliveryFeeAndAssertChanged(final String deliveryFee, final String changed) {
        final Boolean expectedChangedValue = changed.equalsIgnoreCase("yes") ? Boolean.TRUE : Boolean.FALSE;

        final Response response = ServiceLookup.getCheckoutResponseFacade().getCartSummary();
        final CartDetailResponseData summaryData = (CartDetailResponseData)response.getData();
        if (StringUtils.isNotEmpty(deliveryFee)) {
            final Double expectedDeliveryCost = Double.valueOf(deliveryFee.replace("$", ""));
            assertThat(summaryData.getDeliveryFee().getValue()).isEqualTo(
                    BigDecimal.valueOf(expectedDeliveryCost.doubleValue()));
        }
        else {
            assertThat(summaryData.getDeliveryFee().getValue()).isNull();
        }
        assertThat(Checkout.getInstance().isDeliveryFeeChanged()).isEqualTo(
                expectedChangedValue.booleanValue());
    }

    @Then("^delivery fee displayed for the home delivery mode is '\\$(.*)'$")
    public void theDeliveryFeeForCart(final int fee) {
        assertThat(valueModelList).isNotNull();
        assertThat(valueModelList.get(0).getValue().intValue()).isEqualTo(fee);
        assertThat(CartUtil.getApplicableDeliveryModesFromResponseFacade(CheckoutUtil.HOME_DELIVERY_CODE)
                .getDeliveryCost().getValue().intValue()).isEqualTo(fee);

        final CheckoutOptionsResponseData result = (CheckoutOptionsResponseData)ServiceLookup
                .getTargetCheckoutResponseFacade().getApplicableDeliveryModes().getData();
        boolean foundHD = false;
        for (final TargetZoneDeliveryModeData deliveryModeData : result.getDeliveryModes()) {
            if (deliveryModeData.getCode().equals(CheckoutUtil.HOME_DELIVERY_CODE)) {
                foundHD = true;
                assertThat(deliveryModeData.getDeliveryCost().getValue().intValue()).isEqualTo(fee);
            }
        }
        assertThat(foundHD).isTrue();
        ServiceLookup.getModelService().removeAll(newlyCreatedValueModelList);
    }

    @Then("^delivery fee for '(.*)' is '(.*)' and it has '(.*)'$")
    public void theDeliveryFeeForCart(final String deliveryModeCode, final String fee, final String hasRange) {
        final List<TargetZoneDeliveryModeData> applicableDeliveryModeData = Checkout.getInstance()
                .getApplicableDeliveryModeData();
        TargetZoneDeliveryModeData checkedDeliveryMode = null;
        for (final TargetZoneDeliveryModeData deliveryMode : applicableDeliveryModeData) {
            if (deliveryMode.getCode().equals(deliveryModeCode)) {
                checkedDeliveryMode = deliveryMode;
                break;
            }
        }
        assertThat(checkedDeliveryMode).isNotNull();
        if ("N/A".equals(fee)) {
            assertThat(checkedDeliveryMode.getDeliveryCost()).isNull();
        }
        else {
            assertThat("" + checkedDeliveryMode.getDeliveryCost().getValue().intValue())
                    .isEqualTo(fee.replaceAll("\\$", ""));
        }
        if (hasRange.equals("fee range")) {
            assertThat(checkedDeliveryMode.getFeeRange()).isTrue();
        }
        else {
            assertThat(checkedDeliveryMode.getFeeRange()).isFalse();
        }
    }


}

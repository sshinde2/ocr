/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.facade.CatalogUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



/**
 * @author fkhan4
 *
 */
public class ProductEndOfLifeLinksSteps {

    private TargetProductData targetProducttData;

    @Before
    public void setUp() {
        CatalogUtil.setSessionStagedCatalogVersion();
        ImpexImporter.importCsv("/tgttest/automation-impex/product/product-endoflife-links.impex");
    }

    @When("^requested original category for the product '(.*)'$")
    public void getProductDetails(final String productCode) {
        targetProducttData = ProductUtil.populateProductEndOfLifeLinkData(productCode);

    }

    @Then("^link shop by original category is '(.*)'$")
    public void verifyLinkShopByCategory(final String url) {
        if (StringUtils.isEmpty(url)) {
            assertThat(targetProducttData.getProductEndLinks()).isNull();
        }
        else {
            assertThat(targetProducttData.getProductEndLinks().get(0).getUrl()).isEqualTo(url);
        }

    }

    @When("^requested inspiration link for the product '(.*)'$")
    public void getInspirationDetails(final String productCode) {
        targetProducttData = ProductUtil.populateProductEndOfLifeLinkData(productCode);

    }

    @Then("^link shop by inspiration is '(.*)'$")
    public void verifyLinkShopByInspiration(final String url) {
        if (StringUtils.isEmpty(url)) {
            assertThat(targetProducttData.getProductEndLinks().size()).isEqualTo(2);
        }
        else {
            assertThat(targetProducttData.getProductEndLinks().get(1).getUrl()).isEqualTo(url);
        }

    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.security.JaloSecurityException;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfacades.product.data.TargetProductData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.PreviousPermanentPriceUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.StockLevelUtil;
import au.com.target.tgttest.automation.facade.bean.POSPriceEntry;
import au.com.target.tgttest.automation.facade.bean.PrevPermanentPriceEntry;
import au.com.target.tgttest.automation.facade.bean.ProductData;
import au.com.target.tgttest.automation.facade.bean.ProductDimensionsData;
import au.com.target.tgttest.automation.facade.bean.ProductPriceData;
import au.com.target.tgttest.automation.facade.bean.ProductWithDeliveryMode;
import au.com.target.tgttest.automation.facade.domain.PosProductsDto;
import au.com.target.tgttest.constants.TgttestConstants;
import au.com.target.tgtutility.util.TargetDateUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for cart product setup
 * 
 */
public class ProductUpdateSteps {

    protected static final Logger LOG = Logger.getLogger(ProductUpdateSteps.class);
    private String variantproductCode;
    private TargetProductData productData;
    private final SimpleDateFormat onlineDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Given("^a product in hybris:$")
    public void updateProductInHybris(final List<ProductData> productList) throws ParseException {
        final ProductData entry = productList.get(0);
        final String productCode = entry.getProductCode();
        if (StringUtils.isNotEmpty(productCode)) {

            //set price
            setPrice(productCode, entry.getPrice());
            variantproductCode = productCode;
            final ProductModel productStaged = ProductUtil.getStagedProductModel(productCode);
            final ProductModel productOnline = ProductUtil.getOnlineProductModel(productCode);
            final String onlineDateStr = entry.getOnlineDate();
            final Date onlineDate = TgttestConstants.EMPTY_VALUE.equals(onlineDateStr) ? null
                    : onlineDateFormat.parse(onlineDateStr);
            productStaged.setOnlineDate(onlineDate);
            productOnline.setOnlineDate(onlineDate);
            if (productStaged instanceof TargetSizeVariantProductModel) {
                final ProductModel colourVariantStaged = ((TargetSizeVariantProductModel)productStaged)
                        .getBaseProduct();
                final ProductModel colourVariantOnline = ((TargetSizeVariantProductModel)productOnline)
                        .getBaseProduct();
                colourVariantStaged.setOnlineDate(onlineDate);
                colourVariantOnline.setOnlineDate(onlineDate);
                updateProductStatusInHybris(entry.getApprovedStatus(), colourVariantStaged);
                updateProductStatusInHybris(entry.getApprovedStatus(), colourVariantOnline);
            }
            if (productStaged instanceof AbstractTargetVariantProductModel) {
                final AbstractTargetVariantProductModel variantStaged = (AbstractTargetVariantProductModel)productStaged;
                final AbstractTargetVariantProductModel variantOnline = (AbstractTargetVariantProductModel)productOnline;
                variantStaged.setDisplayOnly(Boolean.valueOf(entry.isDisplayOnly()));
                variantOnline.setDisplayOnly(Boolean.valueOf(entry.isDisplayOnly()));
            }
            updateProductStatusInHybris(entry.getApprovedStatus(), productStaged);
            updateProductStatusInHybris(entry.getApprovedStatus(), productOnline);

        }

    }

    /**
     * @param productCode
     * @param price
     */
    private void setPrice(final String productCode, final double price) {
        final POSPriceEntry priceEntry = new POSPriceEntry(productCode, false, price, price);
        PreviousPermanentPriceUtil.createPOSIntegrationData(productCode, ImmutableList.of(priceEntry));
        ServiceLookup.getPosPriceImportIntegrationFacade().importProductPriceFromPos(
                PosProductsDto.getInstance().getIntegrationPosProductsDto().getProducts());
    }

    @Given("^product delivery modes:$")
    public void setProductDeliveryModes(final List<ProductWithDeliveryMode> productList) {
        for (final ProductWithDeliveryMode entry : productList) {
            if (StringUtils.isNotEmpty(entry.getDeliveryMode())) {
                ProductUtil.setDeliveryModeForProduct(entry.getProduct(), entry.getDeliveryMode());
            }
        }
    }

    @Given("^reserve stock on product '(.*)' amount (\\d+)$")
    public void reserveProductStock(final String productCode, final int amount) {
        StockLevelUtil.reserveStock(productCode, amount);

    }

    @Given("^check the stock level of product '(.*)' is great than (\\d+)$")
    public void checkStockLevel(final String productCode, final int amount) {
        final StockLevelModel stockLevelModel = StockLevelUtil.getFastlineStockFor(productCode);
        assertThat(stockLevelModel.getAvailable() - stockLevelModel.getReserved() > amount).as(
                "Avaliable stock level after reduced is greater than " + amount).isTrue();
    }

    @Given("^'(.*)' status in hybris for variant '(.*)'$")
    public void updateProductStatusInHybris(final String status, final String productCode) {

        final ProductModel productStaged = ProductUtil.getStagedProductModel(productCode);
        updateProductStatusInHybris(status, productStaged);

        final ProductModel productOnline = ProductUtil.getOnlineProductModel(productCode);
        updateProductStatusInHybris(status, productOnline);
    }

    @Given("^product '(.*)' is '(.*)'$")
    public void updateProductStatusOnlineOnly(final String productCode, final String status) {
        final ProductModel productOnline = ProductUtil.getOnlineProductModel(productCode);
        updateProductStatusInHybris(status, productOnline);
    }

    private void updateProductStatusInHybris(final String status, final ProductModel product) {

        if (ArticleApprovalStatus.UNAPPROVED.toString().equalsIgnoreCase(status)) {
            product.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
        }
        else if (ArticleApprovalStatus.CHECK.toString().equalsIgnoreCase(status)) {
            product.setApprovalStatus(ArticleApprovalStatus.CHECK);
        }
        else if (ArticleApprovalStatus.APPROVED.toString().equalsIgnoreCase(status)) {
            product.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        }
        ServiceLookup.getModelService().save(product);
    }

    @Given("^product '(.*)' has current price$")
    public void setProductCurrentPrice(final String product, final List<POSPriceEntry> listPrices) {
        variantproductCode = product;
        // For now hard code as 1 day ago
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -1);
        final Date date = cal.getTime();

        for (final POSPriceEntry priceEntry : listPrices) {
            ProductUtil.updateProductPriceRow(product, date, priceEntry.getSellPrice(), priceEntry.getPermPrice(),
                    priceEntry.isPromoEvent());
        }
    }

    @When("^product details are requested$")
    public void populateThePriceIntoProductData() throws JaloSecurityException {

        try {
            Thread.sleep(1000);
        }
        catch (final InterruptedException e) {
            LOG.info("thread Interrupted Exception", e);
            Thread.currentThread().interrupt();
        }
        SessionUtil.createGuestSession();
        if (variantproductCode != null) {
            productData = ProductUtil.populateThePriceIntoProductData(variantproductCode);

        }
    }

    @When("^product details are requested for '(.*)'$")
    public void populateThePriceRangeIntoProductData(final String baseProduct)
            throws JaloSecurityException, InterruptedException {
        Thread.sleep(1000);
        SessionUtil.createGuestSession();
        productData = ProductUtil.populateThePriceIntoProductData(baseProduct);
    }

    /**
     * @param prevPermanentPriceEntrys
     */
    @When("^a colour variant product '(.*)' has size variants:$")
    public void setBaseProduct(final String baseProduct, final List<PrevPermanentPriceEntry> prevPermanentPriceEntrys) {
        //the relation between base product and variant product has been set in the impex
        //check the variant product
        for (final PrevPermanentPriceEntry entry : prevPermanentPriceEntrys) {
            assertThat(ProductUtil.checkVariantProduct(baseProduct, entry.getVariantCode())).isTrue();
        }
    }

    @Then("^stock count for the product is '(.*)'$")
    public void verifyStockLevel(final String stockLevel) {
        assertThat(productData).isNotNull();
        final StockData stock = productData.getStock();
        assertThat(stock).isNotNull();
        if ("null".equals(stockLevel)) {
            assertThat(stock.getStockLevel()).isNull();
        }
        else {
            assertThat(stock.getStockLevel()).isNotNull().isSameAs(Long.valueOf(stockLevel));
        }
    }

    @Then("^price details:$")
    public void verityProductPriceInProductData(final List<ProductPriceData> productPriceDatas) {

        if (productPriceDatas.get(0).getFromPrice() != null && productPriceDatas.get(0).getToPrice() != null) {
            final BigDecimal expectedFromPrice = BigDecimal
                    .valueOf(productPriceDatas.get(0).getFromPrice().doubleValue());
            assertThat(expectedFromPrice.compareTo(productData.getPriceRange().getMinPrice().getValue()) == 0).isTrue();
            final BigDecimal expectedToPrice = BigDecimal.valueOf(productPriceDatas.get(0).getToPrice().doubleValue());
            assertThat(expectedToPrice.compareTo(productData.getPriceRange().getMaxPrice().getValue()) == 0).isTrue();
        }
        else {
            if (productPriceDatas.get(0).getNowPrice() != null) {
                final BigDecimal expectedPrice = BigDecimal
                        .valueOf(productPriceDatas.get(0).getNowPrice().doubleValue());
                assertThat(expectedPrice.compareTo(productData.getPrice().getValue()) == 0).isTrue();
            }
            else {
                assertThat(productData.getPrice()).isNull();
            }
        }
        if (productPriceDatas.get(0).getFromWasPrice() != null && productPriceDatas.get(0).getToWasPrice() != null) {
            assertThat(productData.isShowWasPrice()).isTrue();
            final BigDecimal expectedFromWasPrice = BigDecimal.valueOf(productPriceDatas.get(0).getFromWasPrice()
                    .doubleValue());
            assertThat(expectedFromWasPrice.compareTo(productData.getWasPriceRange().getMinPrice().getValue()) == 0)
                    .isTrue();
            final BigDecimal expectedToWasPrice = BigDecimal.valueOf(
                    productPriceDatas.get(0).getToWasPrice().doubleValue());
            assertThat(expectedToWasPrice.compareTo(productData.getWasPriceRange().getMaxPrice().getValue()) == 0)
                    .isTrue();
        }
        else {
            if (productPriceDatas.get(0).getWasPrice() != null) {
                final BigDecimal expectedWasPrice = BigDecimal.valueOf(
                        productPriceDatas.get(0).getWasPrice().doubleValue());
                assertThat(expectedWasPrice.compareTo(productData.getWasPrice().getValue()) == 0).isTrue();
            }
            else {
                assertThat(productData.getWasPrice()).isNull();
                assertThat(productData.isShowWasPrice()).isFalse();
            }
        }
    }

    @Given("^product weights are updated as:$")
    public void productWeightsAreUpdatedAs(final List<ProductDimensionsData> dimensions) {
        for (final ProductDimensionsData newDim : dimensions) {
            final ProductModel product = ServiceLookup.getTargetProductService()
                    .getProductForCode(newDim.getProductId());
            assertThat(product).isNotNull().isInstanceOf(AbstractTargetVariantProductModel.class);

            final TargetProductDimensionsModel currDim = ((AbstractTargetVariantProductModel)product)
                    .getProductPackageDimensions();
            assertThat(currDim).isNotNull();
            assertThat(currDim.getWeight()).isNotNull();

            currDim.setWeight(newDim.getWeight());
            ServiceLookup.getModelService().save(currDim);
        }
    }

    @Then("its onlineDate will be updated to (.*)")
    public void verifyOnlineDate(final String expectedOnlineDateStr) throws ParseException {
        ProductModel productStaged = ProductUtil.getStagedProductModel(this.variantproductCode);
        if (productStaged instanceof TargetSizeVariantProductModel) {
            productStaged = ((TargetSizeVariantProductModel)productStaged).getBaseProduct();
        }
        final Date productOnlineDate = productStaged.getOnlineDate();
        if (TgttestConstants.EMPTY_VALUE.equals(expectedOnlineDateStr)) {
            assertThat(productOnlineDate).isNull();
        }
        else if ("CURRENT_DATE".equals(expectedOnlineDateStr)) {
            assertThat(productOnlineDate).isEqualTo(TargetDateUtil.getDateWithMidnightTime(new Date()));
        }
        else {
            assertThat(productOnlineDate).isEqualTo(onlineDateFormat.parse(expectedOnlineDateStr));
        }
    }

    @Then("afterpay installment price is '(.*)'")
    public void verifyAfterpayInstallmentPrice(final String expectedPrice) {
        if ("null".equalsIgnoreCase(expectedPrice)) {
            assertThat(productData.getInstallmentPrice()).isNull();
        }
        else {
            assertThat(productData.getInstallmentPrice().getFormattedValue()).isEqualTo(expectedPrice);
        }
    }

    @Then("excludeForAfterpay is '(.*)'")
    public void verifyAfterpayExcludeFlag(final Boolean expected) {
        assertThat(productData.isExcludeForAfterpay()).isEqualTo(expected);
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;

import java.util.List;

import au.com.target.tgtauspost.deliveryclub.dto.ShipsterVerifyEmailResponseDTO;
import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.CheckoutOptionsResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.response.data.ShipsterEmailVerificationResponseData;
import au.com.target.tgttest.automation.ServiceLookup;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author gsing236
 *
 */
public class ShipsterSteps {

    private Response response;

    @Given("^Shipster is (enabled|disabled) for '(.*)'$")
    public void shipsterIsEnabledDisabled(final String configState, final String deliveryMode) {
        final TargetZoneDeliveryModeModel deliveryModeModel = (TargetZoneDeliveryModeModel)ServiceLookup
                .getTargetDeliveryService()
                .getDeliveryModeForCode(deliveryMode);

        final ShipsterConfigModel shipsterConfigModel = ServiceLookup.getShipsterConfigService()
                .getShipsterModelForDeliveryModes(deliveryModeModel);

        if ("enabled".equals(configState)) {
            shipsterConfigModel.setActive(Boolean.TRUE);
        }
        else if ("disabled".equals(configState)) {
            shipsterConfigModel.setActive(Boolean.FALSE);
        }

        ServiceLookup.getModelService().save(shipsterConfigModel);
    }

    @When("^the customer email address is '(registered|unregistered)' for AusPost Shipster$")
    public void customerEmailRegistered(final String status) {

        final ShipsterVerifyEmailResponseDTO verifyEmailResponse = createShipsterResponse("registered".equals(status));
        ServiceLookup.getMockAuspostShipsterClient().setVerifyEmailResponse(verifyEmailResponse);

    }

    @When("^the customer changes the email address is '(registered|unregistered)' for Shipster$")
    public void customerChangeEmail(final String status) {
        customerEmailRegistered(status);
        verifyEmailApiInvokedSuccessfully();
    }

    @Then("^validated each delivery modes based on the order value and shipping value$")
    public void validatedDeliveryModes() {
        response = ServiceLookup.getTargetCheckoutResponseFacade().getApplicableDeliveryModes();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isNotNull();
    }

    @Then("^return shipster available for each '(.*)' as '(true|false)' with '(.*)'$")
    public void verifyShipsterForDeliveryModes(final String deliveryMode, final boolean availableStatus,
            final String reasonCode) {
        final CheckoutOptionsResponseData responseData = (CheckoutOptionsResponseData)response.getData();
        assertThat(responseData.getDeliveryModes()).isNotNull();
        assertThat(responseData.getDeliveryModes()).isNotEmpty();
        final List<TargetZoneDeliveryModeData> targetZoneDeliveryModes = responseData.getDeliveryModes();
        for (final TargetZoneDeliveryModeData targetZoneDeliveryModeData : targetZoneDeliveryModes) {
            if (targetZoneDeliveryModeData.getCode().equals(deliveryMode)) {
                assertThat(targetZoneDeliveryModeData.getShipsterData()).isNotNull();
                assertThat(targetZoneDeliveryModeData.getShipsterData().isAvailable())
                        .isEqualTo(Boolean.valueOf(availableStatus));
                assertThat(targetZoneDeliveryModeData.getShipsterData().getReason()).isEqualTo(reasonCode);
            }
        }
    }


    @Then("^verify email api is invoked successfully$")
    public void verifyEmailApiInvokedSuccessfully() {
        ServiceLookup.getMockAuspostShipsterClient().setVerifyEmailNotAvailable(false);
        response = ServiceLookup.getCheckoutResponseFacade().verifyShipsterStatus();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
    }

    @Then("^verify email api fails$")
    public void verifyEmailApiInvokedUnsuccessfully() {
        ServiceLookup.getMockAuspostShipsterClient().setVerifyEmailNotAvailable(true);
        response = ServiceLookup.getCheckoutResponseFacade().verifyShipsterStatus();
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
    }

    @And("^member is returned as '(verified|not verified)'$")
    public void memberVerfied(final String status) {

        final ShipsterEmailVerificationResponseData shipsterResponse = (ShipsterEmailVerificationResponseData)response
                .getData();
        if ("verified".equals(status)) {
            assertThat(shipsterResponse.isDeliveryClubMember()).isTrue();
        }
        else {
            assertThat(shipsterResponse.isDeliveryClubMember()).isFalse();
        }
    }

    @And("Shipster status reponse is unsuccessful$")
    public void deliveryClubStatusResponseUnsuccessful() {

        final BaseResponseData shipsterResponse = response.getData();
        assertThat(shipsterResponse).isNotNull();
        assertThat(shipsterResponse.getError().getCode()).isNotNull();
        assertThat(shipsterResponse.getError().getMessage()).isNotNull();
        assertThat(shipsterResponse.getError().getCode()).isEqualTo("UNABLE_TO_VERIFY");
        assertThat(shipsterResponse.getError().getMessage()).isEqualTo("Unable to verify email for Shipster");

    }

    private ShipsterVerifyEmailResponseDTO createShipsterResponse(final boolean status) {
        final ShipsterVerifyEmailResponseDTO dto = new ShipsterVerifyEmailResponseDTO();
        dto.setVerified(status);
        return dto;
    }
}

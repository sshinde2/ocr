/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CartUtil;
import au.com.target.tgttest.automation.facade.CustomerUtil;
import au.com.target.tgttest.automation.facade.UserSinglePageCheckoutPreferencesUtil;
import au.com.target.tgttest.automation.facade.bean.PreviousOrderDetails;
import au.com.target.tgttest.automation.facade.bean.SavedUserPreferences;
import au.com.target.tgttest.automation.facade.bean.SinglePageCheckoutPrePopulatedCartDetails;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author ajit
 *
 */
public class SinglePageCheckoutSteps {

    @Given("a registered customer with a cart")
    public void setRegisteredUserWithCreateNewCart() throws Exception {
        UserSinglePageCheckoutPreferencesUtil.setRegisteredUserWithCreateNewCart();
    }

    @Given("a registered customer goes into the checkout process")
    public void setRegisteredUserWithCart() throws DuplicateUidException {
        UserSinglePageCheckoutPreferencesUtil.setRegisteredUserWithCart();
    }

    @Given("saved preferences as")
    public void setUserWithSavedPreferences(final List<SavedUserPreferences> userSavedPreferences) throws Exception {
        UserSinglePageCheckoutPreferencesUtil.setCustomerPreferences(userSavedPreferences);
    }

    @Given("have previous order with following details")
    public void setPreviousOrderSavedPreferences(final List<PreviousOrderDetails> previousOrders) throws Exception {
        UserSinglePageCheckoutPreferencesUtil.setCustomerPreviourOrder(previousOrders);
    }

    @When("the customer navigates to checkout")
    public void doCheckout() throws CalculationException {
        UserSinglePageCheckoutPreferencesUtil.doCheckoutPopulateCart();
    }

    @Then("the customer cart is pre-populated with following details")
    public void prePopulatedUserPreferences(final List<SinglePageCheckoutPrePopulatedCartDetails> prePopulateCart) {
        final CartModel cart = CartUtil.getSessionCartModel();
        UserSinglePageCheckoutPreferencesUtil.verifyCartModel(prePopulateCart, cart);
        ServiceLookup.getOrderCreationHelper().removeAllOrders();
    }

    @Given("had no previous order")
    public void setPreviousOrder() {
        final TargetCustomerModel customerModel = CustomerUtil.getTargetCustomer(CustomerUtil
                .getStandardTestCustomerUid());
        customerModel.setOrders(Collections.EMPTY_LIST);
        ServiceLookup.getModelService().save(customerModel);
    }

    @When("customer navigates to checkout")
    public void doChekout() {
        ServiceLookup.getPrepopulateCheckoutFacade().isCartPrePopulatedForCheckout();
    }

    @Then("the customer cart is not pre-populated")
    public void thenCart() {
        final CartModel cart = CartUtil.getSessionCartModel();
        verifyAssertation(cart);
    }

    private void verifyAssertation(final CartModel cart) {
        Assert.assertNotNull(cart);
        Assert.assertNull(cart.getDeliveryAddress());
        Assert.assertNull(cart.getPaymentInfo());
        Assert.assertNull(cart.getPaymentMode());

    }

}

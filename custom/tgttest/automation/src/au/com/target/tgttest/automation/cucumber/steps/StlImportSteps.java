/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookCollectionDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationShopTheLookDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSupercategoryDto;
import au.com.target.tgtwsfacades.stlimport.ShopTheLookImportIntegrationFacade;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author mgazal
 *
 */
public class StlImportSteps {


    private IntegrationShopTheLookDto shopTheLookDto = null;

    private TargetShopTheLookModel shopTheLookModel = null;

    private final ShopTheLookImportIntegrationFacade importIntegrationFacade = ServiceLookup
            .getShopTheLookImportIntegrationFacade();

    private final TargetShopTheLookFacade shopTheLookFacade = ServiceLookup.getTargetShopTheLookFacade();

    private final ModelService modelService = ServiceLookup.getModelService();

    @Before("@stlImport")
    public void cleanup() {
        final TargetShopTheLookModel shopTheLook = shopTheLookFacade.getShopTheLookByCode("STL_A");
        if (shopTheLook != null) {
            removeCollections(shopTheLook.getCollections());
            modelService.remove(shopTheLook);
        }
    }

    private void removeCollections(final List<TargetLookCollectionModel> collections) {
        for (final TargetLookCollectionModel collection : collections) {
            removeLooks(collection.getLooks());
            modelService.remove(collection);
        }
    }

    private void removeLooks(final Set<TargetLookModel> looks) {
        for (final TargetLookModel look : looks) {
            removeLookProducts(look.getProducts());
            modelService.remove(look);
        }
    }

    private void removeLookProducts(final Set<TargetLookProductModel> lookProducts) {
        for (final TargetLookProductModel lookProduct : lookProducts) {
            modelService.remove(lookProduct);
        }
    }

    @Given("^a shopTheLook with category '(.*)', '(\\d+)' collections each with '(\\d+)' looks and '(\\d+)' products each from STEP$")
    public void shopTheLookFromStep(final String category, final int noOfCollections, final int noOfLooks,
            final int noOfProducts) {
        final IntegrationShopTheLookDto dto = new IntegrationShopTheLookDto();
        final String code = "STL_A";
        final String name = "Shop the Look Automation";
        dto.setCode(code);
        dto.setName(name);
        dto.setSupercategory(new IntegrationSupercategoryDto(category));
        final List<IntegrationLookCollectionDto> collections = new ArrayList<>();
        for (int i = 0; i < noOfCollections; i++) {
            final IntegrationLookCollectionDto collection = new IntegrationLookCollectionDto();
            collection.setCode(code + "LC" + StringUtils.leftPad(Integer.toString(i + 1), 2, '0'));
            collection.setName("Look Collection " + (i + 1));
            final List<IntegrationLookDto> looks = new ArrayList<>();
            for (int j = 0; j < noOfLooks; j++) {
                final IntegrationLookDto look = new IntegrationLookDto();
                look.setCode(collection.getCode() + "L" + StringUtils.leftPad(Integer.toString(j + 1), 2, '0'));
                look.setName("Look " + (j + 1));
                final List<IntegrationLookProductDto> products = new ArrayList<>();
                for (int k = 0; k < noOfProducts; k++) {
                    products.add(new IntegrationLookProductDto(
                            look.getCode() + "P" + StringUtils.leftPad(Integer.toString(k + 1), 2, '0')));
                }
                look.setProducts(products);
                looks.add(look);
            }
            collection.setLooks(looks);
            collections.add(collection);
        }
        dto.setCollections(collections);
        shopTheLookDto = dto;
    }

    @When("^XML export from STEP is passed to hybris through ESB$")
    public void stepImport() {
        SessionUtil.userLogin("esbstlupdate");

        importIntegrationFacade.persistTargetShopTheLook(shopTheLookDto);
    }

    @Then("^'(\\d+)' collection(?:s?) (?:is|are) created$")
    public void verifyCollections(final int collectionCount) {
        shopTheLookModel = shopTheLookFacade.getShopTheLookByCode(shopTheLookDto.getCode());
        assertThat(shopTheLookModel).isNotNull();
        assertThat(shopTheLookModel.getCollections().size()).isEqualTo(collectionCount);
        assertThat(shopTheLookModel.getCollections()).onProperty("name")
                .contains(CollectionUtils.collect(shopTheLookDto.getCollections(), new Transformer() {

                    @Override
                    public Object transform(final Object collection) {
                        return ((IntegrationLookCollectionDto)collection).getName();
                    }
                }).toArray());
    }

    @Then("^there (?:is|are) '(\\d+)' look(?:s?) in each collection$")
    public void verifyLooks(final int lookCount) {
        assertThat(shopTheLookModel).isNotNull();
        for (final TargetLookCollectionModel collectionModel : shopTheLookModel.getCollections()) {
            assertThat(collectionModel.getLooks().size()).isEqualTo(lookCount);
        }
    }


    @Then("^there (?:is|are) '(\\d+)' product(?:s?) in each look$")
    public void verifyProducts(final int productCount) {
        for (final TargetLookCollectionModel collectionModel : shopTheLookModel.getCollections()) {
            for (final TargetLookModel lookModel : collectionModel.getLooks()) {
                assertThat(lookModel.getProducts().size()).isEqualTo(productCount);
            }
        }
    }
}

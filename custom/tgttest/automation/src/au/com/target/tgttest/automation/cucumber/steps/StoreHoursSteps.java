/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.StoreHours;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import au.com.target.tgtwsfacades.integration.dto.IntegrationOpeningDayDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationOpeningScheduleDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationPointOfServiceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author rmcalave
 *
 */
public class StoreHoursSteps {

    private int storeNo;
    private IntegrationPointOfServiceDto pointOfServiceDto;
    private IntegrationResponseDto responseDto;
    private TargetPointOfServiceModel targetPointOfService;

    @Given("^Target store with number '(\\d+)' does not exist$")
    public void givenTargetStoreWithStoreNumberDoesNotExist(final int storeNumber) {
        storeNo = storeNumber;

        try {
            targetPointOfService = ServiceLookup.getTargetPointOfServiceService()
                    .getPOSByStoreNumber(Integer.valueOf(storeNumber));

            assertThat(targetPointOfService).isNull();
        }
        catch (final TargetUnknownIdentifierException ex) {
            assertThat(ex).isNotNull();
        }
        catch (final TargetAmbiguousIdentifierException ex) {
            Assert.fail();
        }
    }

    @Given("^Target store with number '(\\d+)' exists$")
    public void givenTargetStoreWithStoreNumber(final int storeNumber) {
        storeNo = storeNumber;

        try {
            targetPointOfService = ServiceLookup.getTargetPointOfServiceService()
                    .getPOSByStoreNumber(Integer.valueOf(storeNumber));
        }
        catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException ex) {
            Assert.fail("Store with number " + storeNumber + " does not exist");
        }

        assertThat(targetPointOfService).isNotNull();
    }

    @Given("^opening hours for the store are:$")
    public void givenOpeningHoursForStoreAre(final List<StoreHours> storeHoursList)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        assertThat(targetPointOfService).isNotNull();

        final OpeningScheduleModel schedule = targetPointOfService.getOpeningSchedule();
        assertThat(schedule).isNotNull();

        final Collection<OpeningDayModel> openingDays = schedule.getOpeningDays();
        if (CollectionUtils.isNotEmpty(openingDays)) {
            ServiceLookup.getModelService().removeAll(openingDays);
            ServiceLookup.getModelService().refresh(schedule);
        }

        for (final StoreHours storeHours : storeHoursList) {
            createTargetOpeningDayForStore(storeNo, storeHours, schedule);
        }
        ServiceLookup.getModelService().refresh(schedule);
    }

    @When("^Excel spreadsheet containing store opening hours for the store is processed$")
    public void whenExcelSpreadsheetContainingStoreOpeningHoursIsProcessedByESB(final List<StoreHours> storeHoursList) {
        pointOfServiceDto = createPointOfServiceDto(storeNo, storeHoursList);

        responseDto = ServiceLookup.getTargetStoreLocatorIntegrationFacade().persistPointOfService(pointOfServiceDto);
    }

    @Then("^the hours for non-existing store will be ignored$")
    public void thenTheHoursForNonExistingStoreWillBeIgnored() {
        assertThat(responseDto.isSuccessStatus()).isFalse();
        assertThat(responseDto.getMessages()).containsOnly(
                "Cannot create an OpeningSchedule for PointOfService that does not exist");
    }

    @Then("^the opening schedule will be updated$")
    public void thenTheOpeningScheduleWillBeUpdated() {
        assertThat(targetPointOfService).isNotNull();

        assertThat(responseDto.isSuccessStatus()).isTrue();
        assertThat(responseDto.getMessages()).isEmpty();

        final OpeningScheduleModel schedule = targetPointOfService.getOpeningSchedule();
        ServiceLookup.getModelService().refresh(schedule);

        final Collection<OpeningDayModel> openingDays = schedule.getOpeningDays();
        assertThat(openingDays).hasSize(3);

        final List<IntegrationOpeningDayDto> openingDayDtos = pointOfServiceDto.getOpeningSchedule().getOpeningDays();

        int matches = 0;
        for (final IntegrationOpeningDayDto openingDayDto : openingDayDtos) {
            for (final OpeningDayModel openingDay : openingDays) {
                if (openingDay.getOpeningTime().equals(openingDayDto.getOpeningTime())) {

                    matches++;
                    final Date openingTime = openingDayDto.getOpeningTime();
                    final Date closingTime = openingDayDto.getClosingTime();

                    assertThat(openingDay.getOpeningTime()).isEqualTo(openingTime);
                    assertThat(openingDay.getClosingTime()).isEqualTo(closingTime);
                    break;
                }
            }
        }
        Assert.assertEquals(3, matches);
    }

    @Then("^unused opening days deleted for the store are:$")
    public void thenUnusedOpeningDaysAreDeleted(final List<StoreHours> storeHoursList)
            throws TargetAmbiguousIdentifierException {
        assertThat(targetPointOfService).isNotNull();

        final OpeningScheduleModel schedule = targetPointOfService.getOpeningSchedule();

        for (final StoreHours storeHour : storeHoursList) {
            final String targetOpeningDayId = ServiceLookup.getTargetOpeningDayService().getTargetOpeningDayId(
                    String.valueOf(storeNo),
                    parseDayAndTimeToDate(storeHour.getOpeningDay(), storeHour.getOpeningTime()));

            TargetOpeningDayModel dayModel = null;
            dayModel = ServiceLookup.getTargetOpeningDayService()
                    .getTargetOpeningDay(schedule, targetOpeningDayId);
            Assert.assertNull(dayModel);
        }

    }

    private void createTargetOpeningDayForStore(final int storeNumber, final StoreHours storeHours,
            final OpeningScheduleModel openingSchedule) {
        final Date openingTime = parseDayAndTimeToDate(storeHours.getOpeningDay(), storeHours.getOpeningTime());
        final Date closingTime = parseDayAndTimeToDate(storeHours.getOpeningDay(), storeHours.getClosingTime());

        final TargetOpeningDayModel openingDay = ServiceLookup.getModelService().create(TargetOpeningDayModel.class);
        openingDay.setClosed(false);
        openingDay.setOpeningSchedule(openingSchedule);
        openingDay.setOwner(openingSchedule);
        openingDay.setOpeningTime(openingTime);
        openingDay.setClosingTime(closingTime);
        openingDay.setTargetOpeningDayId(ServiceLookup.getTargetOpeningDayService().getTargetOpeningDayId(
                String.valueOf(storeNumber), openingTime));
        ServiceLookup.getModelService().save(openingDay);
    }

    private IntegrationPointOfServiceDto createPointOfServiceDto(final int storeNumber,
            final List<StoreHours> storeHoursList) {
        final IntegrationPointOfServiceDto dto = new IntegrationPointOfServiceDto();

        dto.setStoreNumber(Integer.valueOf(storeNumber));
        dto.setOpeningSchedule(createIntegrationOpeningScheduleDto(storeNumber, storeHoursList));
        dto.setHasOnlySchedule(true);

        return dto;
    }

    private IntegrationOpeningScheduleDto createIntegrationOpeningScheduleDto(final int storeNumber,
            final List<StoreHours> storeHoursList) {
        final IntegrationOpeningScheduleDto openingScheduleDto = new IntegrationOpeningScheduleDto();

        openingScheduleDto.setStoreNumber(String.valueOf(storeNumber));

        final List<IntegrationOpeningDayDto> openingDays = new ArrayList<>();
        for (final StoreHours storeHours : storeHoursList) {
            openingDays.add(createOpeningDayDto(storeHours));
        }

        openingScheduleDto.setOpeningDays(openingDays);

        return openingScheduleDto;
    }

    private IntegrationOpeningDayDto createOpeningDayDto(final StoreHours storeHours) {
        final Date openingTime = parseDayAndTimeToDate(storeHours.getOpeningDay(), storeHours.getOpeningTime());
        final Date closingTime = parseDayAndTimeToDate(storeHours.getOpeningDay(), storeHours.getClosingTime());

        final IntegrationOpeningDayDto openingDayDto = new IntegrationOpeningDayDto();
        openingDayDto.setOpeningTime(openingTime);
        openingDayDto.setClosingTime(closingTime);
        openingDayDto.setClosed(Boolean.FALSE);

        return openingDayDto;
    }

    private Date parseDayAndTimeToDate(final String openingDayString, final String openingTimeString) {
        final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();
        return dateUtil.parseDayAndTimeToDate(openingDayString, openingTimeString);
    }

}

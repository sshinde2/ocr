/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.store.data.GeoPoint;

import java.util.List;

import au.com.target.tgtcore.storelocator.impl.TargetGPS;
import au.com.target.tgtfacades.response.data.Error;
import au.com.target.tgtfacades.response.data.HomeDeliveryLocation;
import au.com.target.tgtfacades.response.data.LocationResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceStockData;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.GeolocationData;
import au.com.target.tgttest.automation.facade.bean.NearestLocationData;
import au.com.target.tgttest.automation.mock.MockGeoWebServiceWrapper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author bbaral1
 *
 */
public class StoreLocationResponseDataSteps {

    private final TargetStoreFinderStockFacade targetStoreFinderStockFacade = ServiceLookup
            .getTargetStoreFinderStockFacade();
    private String deliveryType;
    private Response response;
    private LocationResponseData locationResponseData;

    @Given("^geolocation setup for certain store locations are:$")
    public void setGpsForLocation(final List<GeolocationData> geoLocations) {
        for (final GeolocationData geolocation : geoLocations) {
            final TargetGPS targetGps = new TargetGPS(geolocation.getLatitude(), geolocation.getLongitude());
            targetGps.setLocality(geolocation.getLocality());
            targetGps.setPostcode(geolocation.getPostcode());
            targetGps.setState(geolocation.getState());
            MockGeoWebServiceWrapper.setGps(geolocation.getLocationText(), targetGps);
        }
    }

    @Given("^the delivery type '(.*)'$")
    public void setDeliveryType(final String deliveryTypeValue) {
        deliveryType = deliveryTypeValue;
    }

    @When("^the user searches for the nearest store location '(.*)'$")
    public void theUserSearchesForNearestStores(final String locationString) {
        final PageableData pageableData = new PageableData();
        pageableData.setCurrentPage(0);
        pageableData.setPageSize(1);
        response = targetStoreFinderStockFacade.nearestStoreLocation(deliveryType, locationString, null,
                pageableData);
        if (response.getData() instanceof LocationResponseData) {
            locationResponseData = (LocationResponseData)response.getData();
        }
    }

    @Then("^the home delivery store location details are:$")
    public void theNearestHomeDeliveryLocationWillBe(final List<NearestLocationData> nearestLocationDataList) {
        assertThat(locationResponseData).isNotNull();
        for (final NearestLocationData nearestLocationData : nearestLocationDataList) {
            final String suburb = nearestLocationData.getSuburb();
            final String postalCode = nearestLocationData.getPostcode();
            final int hdShortLeadTime = nearestLocationData.getHdShortLeadTime();
            final int edShortLeadTime = nearestLocationData.getEdShortLeadTime();
            final int bulky1ShortLeadTime = nearestLocationData.getBulky1ShortLeadTime();
            final int bulky2ShortLeadTime = nearestLocationData.getBulky2ShortLeadTime();
            final HomeDeliveryLocation homeDeliveryLocation = locationResponseData.getHomeDeliveryPos();
            assertThat(homeDeliveryLocation.getSuburb()).isEqualTo(suburb);
            assertThat(homeDeliveryLocation.getPostalCode()).isEqualTo(postalCode);
            assertThat(homeDeliveryLocation.getHdShortLeadTime()).isEqualTo(hdShortLeadTime);
            assertThat(homeDeliveryLocation.getEdShortLeadTime()).isEqualTo(edShortLeadTime);
            assertThat(homeDeliveryLocation.getBulky1ShortLeadTime()).isEqualTo(bulky1ShortLeadTime);
            assertThat(homeDeliveryLocation.getBulky2ShortLeadTime()).isEqualTo(bulky2ShortLeadTime);
        }
    }

    @Then("^the click and collect store location details are:$")
    public void theNearestClickAndCollectLocationWillBe(final List<NearestLocationData> nearestLocationDataList) {
        assertThat(locationResponseData).isNotNull();
        for (final NearestLocationData nearestLocationData : nearestLocationDataList) {
            final String state = nearestLocationData.getState();
            final String storeName = nearestLocationData.getStoreName();
            final String formattedAddress = nearestLocationData.getFormattedAddress();
            final String phone = nearestLocationData.getPhone();
            final double latitude = nearestLocationData.getLatitude();
            final double longitude = nearestLocationData.getLongitude();
            final String timeZone = nearestLocationData.getTimeZone();
            final String postCode = nearestLocationData.getPostcode();
            final Integer shortLeadTime = nearestLocationData.getShortLeadTime();
            final Boolean closed = nearestLocationData.getClosed();
            final Boolean acceptCNC = nearestLocationData.getAcceptCNC();
            final Boolean acceptLayBy = nearestLocationData.getAcceptLayBy();
            final Integer storeNumber = nearestLocationData.getStoreNumber();
            final String url = nearestLocationData.getUrl();
            final String type = nearestLocationData.getType();
            final TargetPointOfServiceStockData targetPointOfServiceStockData = locationResponseData
                    .getClickAndCollectPos();
            assertThat(targetPointOfServiceStockData).isNotNull();
            final TargetAddressData targetAddressData = targetPointOfServiceStockData.getTargetAddressData();
            assertThat(targetAddressData).isNotNull();
            assertThat(targetAddressData.getState()).isEqualTo(state);
            assertThat(targetPointOfServiceStockData.getName()).isEqualTo(storeName);
            assertThat(targetPointOfServiceStockData.getClosed()).isEqualTo(closed);
            assertThat(targetPointOfServiceStockData.getAcceptCNC()).isEqualTo(acceptCNC);
            assertThat(targetPointOfServiceStockData.getAcceptLayBy()).isEqualTo(acceptLayBy);
            final GeoPoint geoPoint = targetPointOfServiceStockData.getGeoPoint();
            assertThat(geoPoint).isNotNull();
            assertThat(geoPoint.getLatitude()).isEqualTo(latitude);
            assertThat(geoPoint.getLongitude()).isEqualTo(longitude);
            assertThat(targetPointOfServiceStockData.getTimeZone()).isEqualTo(timeZone);
            final AddressData addressData = targetPointOfServiceStockData.getAddress();
            assertThat(addressData).isNotNull();
            assertThat(addressData.getPostalCode()).isEqualTo(postCode);
            assertThat(addressData.getFormattedAddress()).isEqualTo(formattedAddress);
            assertThat(addressData.getPhone()).isEqualTo(phone);
            assertThat(targetPointOfServiceStockData.getShortLeadTime()).isEqualTo(shortLeadTime);
            assertThat(targetPointOfServiceStockData.getStoreNumber()).isEqualTo(storeNumber);
            assertThat(targetPointOfServiceStockData.getUrl()).isEqualTo(url);
            assertThat(targetPointOfServiceStockData.getType()).isEqualTo(type);
        }
    }

    @Then("^result will return an error: '(.*)', '(.*)'$")
    public void verifyLocationResponseError(final String errorCode, final String errorMessage) {
        final Error responseData = response.getData().getError();
        assertThat(responseData.getCode()).as("Error Code").isEqualTo(errorCode);
        assertThat(responseData.getMessage()).as("Error Message").isEqualTo(errorMessage);
    }
}

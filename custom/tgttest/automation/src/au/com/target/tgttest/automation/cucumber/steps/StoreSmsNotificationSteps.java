/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;


import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.processengine.enums.ProcessState;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author Vivek
 *
 */
public class StoreSmsNotificationSteps {

    private Integer storeNumber;

    private ProcessState sendSmsStatus = null;

    private String mobileNo;

    private Boolean smsNotificationFlag;

    private Date notificationStartTime;

    private Date notificationEndTime;

    private TargetPointOfServiceModel pos;

    @Given("^store for which sms notification will be sent (\\d+)$")
    public void givenStoreForSmsNotification(final Integer store) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        storeNumber = store;
    }

    @Given("^mobile number for store is '(.*)'$")
    public void givenMobileNumberForStore(final String mobileNumber) {
        mobileNo = mobileNumber;
    }

    @Given("^sms notification flag is '(.*)'$")
    public void givenSmsNotificationFlag(final String notificationFlag) {
        if (StringUtils.equalsIgnoreCase(notificationFlag, "yes")) {
            smsNotificationFlag = Boolean.TRUE;
        }
        else {
            smsNotificationFlag = Boolean.FALSE;
        }
    }

    @Given("^sms notification start time is '(.*)'$")
    public void givenSmsNotificationStartTime(final String startTime) {
        final ComplexDateExpressionParser timeUtil = new ComplexDateExpressionParser();
        final int hoursValue = timeUtil.getRelativeHoursFromNowHoursDatePattern(startTime);
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, hoursValue);
        notificationStartTime = cal.getTime();
    }

    @Given("^sms notification end time is '(.*)'$")
    public void givenSmsNotificationEndTime(final String endTime) {
        final ComplexDateExpressionParser timeUtil = new ComplexDateExpressionParser();
        final int hoursValue = timeUtil.getRelativeHoursFromNowHoursDatePattern(endTime);
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, hoursValue);
        notificationEndTime = cal.getTime();
    }

    @Given("^number of open orders at the store are '(\\d+)'$")
    public void givenNumberOfOpenOrders(final int openOrders) throws Exception {
        int code = 11110001;
        final Integer storeNo = storeNumber;
        final List<OrderModel> orders = new ArrayList<>();
        final List<TargetConsignmentModel> consignments = new ArrayList<>();
        pos = ServiceLookup.getTargetPointOfServiceService().getPOSByStoreNumber(
                storeNumber);
        for (int i = 0; i < openOrders; ++i) {
            final String consignmentCode = "auto" + String.valueOf(code++);
            final WarehouseModel warehouseModel = ServiceLookup.getWarehouseService()
                    .getWarehouseForPointOfService(pos);
            final OrderModel orderModel = ServiceLookup.getOrderCreationHelper().createOrder("click-and-collect",
                    consignmentCode, true, storeNo, Double.valueOf(10), "WEB");
            orders.add(orderModel);
            final TargetConsignmentModel consignment = ServiceLookup.getConsignmentCreationHelper()
                    .createConsignment(consignmentCode, StringUtils.EMPTY, ConsignmentStatus.CONFIRMED_BY_WAREHOUSE,
                            warehouseModel,
                            getShippingAddress(warehouseModel), null, null,
                            null, null, null, null, null,
                            null, null, null, orderModel);
            consignments.add(consignment);
        }
    }

    @When("^send sms notification to store job is run$")
    public void smsNotificationJobIsInvokedAtTime() throws InterruptedException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        setStoreCapabilitiesModelWithGivenValues();
        ServiceLookup.getSendSmsToStoreForOpenOrdersJob().perform(null);
        sendSmsStatus = BusinessProcessUtil
                .waitForBusinessProcessWithNameLikeToFinish("sendSmsToStoreForOpenOrdersProcess-" + mobileNo);
    }

    @Then("^send sms to store status is '(.*)'$")
    public void thenVerifySmsSendStatus(final String status) {
        if (StringUtils.endsWithIgnoreCase(status, "yes")) {
            assertThat(sendSmsStatus).isNotNull();
            assertThat(sendSmsStatus.toString()).isEqualTo(ProcessState.SUCCEEDED.toString());
        }
        else if (StringUtils.isEmpty(mobileNo)) {
            assertThat(sendSmsStatus).isNotNull();
            assertThat(sendSmsStatus.toString()).isEqualTo(ProcessState.ERROR.toString());
        }
        else {
            assertThat(sendSmsStatus).isNull();
        }
    }

    private void setStoreCapabilitiesModelWithGivenValues() {
        final StoreFulfilmentCapabilitiesModel capabilitiesModel = pos.getFulfilmentCapability();
        capabilitiesModel.setMobileNumber(mobileNo);
        capabilitiesModel.setNotificationsEnabled(smsNotificationFlag);
        capabilitiesModel.setNotificationStartTime(notificationStartTime);
        capabilitiesModel.setNotificationEndTime(notificationEndTime);
        ServiceLookup.getModelService().save(capabilitiesModel);
    }

    private AddressModel getShippingAddress(final ItemModel owner) {
        final AddressModel shippingAddress = ServiceLookup.getModelService().create(AddressModel.class);
        shippingAddress.setOwner(owner);
        ServiceLookup.getModelService().save(shippingAddress);
        return shippingAddress;
    }
}

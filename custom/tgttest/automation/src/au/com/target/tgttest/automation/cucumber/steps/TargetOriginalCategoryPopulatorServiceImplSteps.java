/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.impl.TargetOriginalCategoryPopulatorServiceImpl;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.ProductData;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author nullah
 *
 */
public class TargetOriginalCategoryPopulatorServiceImplSteps {

    private TargetOriginalCategoryPopulatorServiceImpl targetOriginalCategoryPopulatorServiceImpl;
    private TargetSharedConfigService targetSharedConfigService;
    private CatalogVersionModel stagedProductCatalog;
    private CatalogVersionModel onlineProductCatalog;
    private TargetProductService targetProductService;


    @Before
    public void setUp() {
        targetOriginalCategoryPopulatorServiceImpl = ServiceLookup.getTragetOriginalCategoryPopulatorService();
        targetProductService = ServiceLookup.getTargetProductService();
        targetSharedConfigService = ServiceLookup.getTargetSharedConfigService();
        stagedProductCatalog = ServiceLookup.getCatalogVersionService()
                .getCatalogVersion(
                        TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        onlineProductCatalog = ServiceLookup.getCatalogVersionService()
                .getCatalogVersion(
                        TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.ONLINE_VERSION);
        ImpexImporter.importCsv("/tgttest/automation-impex/product/products-original-category.impex");

    }

    @Given("^batch limit is '(.*)'$")
    public void setBatchLimitToPopulateOriginalCategories(final String batchlimit) {
        final Map<String, String> configEntries = new HashMap<>();
        configEntries.put("products.originalCategory.populate.batchlimit", batchlimit);
        targetSharedConfigService.setSharedConfigs(configEntries);
    }

    @Given("^sub batch limit is '(.*)'$")
    public void setSubBatchLimitToPopulateOriginalCategories(final String subBatchLimit) {
        final Map<String, String> configEntries = new HashMap<>();
        configEntries.put("products.originalCategory.populate.sub.batchlimit", subBatchLimit);
        targetSharedConfigService.setSharedConfigs(configEntries);
    }

    @Given("^clearance category code is set to '(.*)'$")
    public void processAllProducts(final String categoryCode) {
        final Map<String, String> configEntries = new HashMap<>();
        configEntries.put("products.clearanceCategoryCode", categoryCode);
        targetSharedConfigService.setSharedConfigs(configEntries);
    }

    @When("^job runs for populating original category$")
    public void executePopulationProcess() {
        try {
            targetOriginalCategoryPopulatorServiceImpl.populateOriginalCategories();
        }
        catch (final Exception e) {
            //exception
        }
    }


    @Then("^the original should be$")
    public void verifyProductOriginalCategory(final List<ProductData> products) {

        for (final ProductData product : products) {
            final CatalogVersionModel catalog = "Staged".equals(product.getCatalogVersionName())
                    ? stagedProductCatalog : onlineProductCatalog;
            final TargetProductModel productModel = (TargetProductModel)targetProductService.getProductForCode(catalog,
                    product.getProductCode());
            if ("N/A".equalsIgnoreCase(product.getOriginalCategory())) {
                assertThat(productModel.getOriginalCategory()).isNull();
                assertThat(productModel
                        .getApprovalStatus().getCode()).isEqualTo(product.getApprovedStatus());
            }
            else {
                assertThat(productModel.getOriginalCategory().getCode())
                        .isEqualTo(product.getOriginalCategory());
                assertThat(productModel.getApprovalStatus().getCode())
                        .isEqualTo(product.getApprovedStatus());
            }

        }
    }

    @Then("^original category should not be changed$")
    public void verifyProductOriginalCategorySame(final List<ProductData> products) {

        for (int i = 0; i < products.size(); i++) {
            assertThat(((TargetProductModel)targetProductService.getProductForCode(stagedProductCatalog,
                    products.get(i).getProductCode()))
                            .getOriginalCategory().getCode()).isEqualTo(products.get(i).getOriginalCategory());
            assertThat(((TargetProductModel)targetProductService.getProductForCode(stagedProductCatalog,
                    products.get(i).getProductCode()))
                            .getApprovalStatus().getCode()).isEqualTo(products.get(i).getApprovedStatus());
        }
    }
}

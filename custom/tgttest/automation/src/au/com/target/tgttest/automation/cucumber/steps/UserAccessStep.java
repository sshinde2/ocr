/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;

import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CustomerUtil;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author ayushman
 *
 */
public class UserAccessStep {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    private TargetCustomerModel customer;

    private AbstractDealModel deal;

    @Given("^user '(.*)' exists in '(.*)' user group$")
    public void userDevExistsInDevsupportUserGroup(final String user, final String group) {
        Assert.assertNotNull(ServiceLookup.getUserService().getUserGroupForUID(group));
        Assert.assertEquals(user, ServiceLookup.getUserService().getCurrentUser().getUid());
        Assert.assertEquals(group,
                ServiceLookup.getUserService().getCurrentUser().getGroups().iterator().next().getUid());
    }

    @When("^access an existing customer record$")
    public void accessAnExistingCustomerRecord() {
        customer = CustomerUtil.getTargetCustomer("test.user1@target.com.au");
    }

    @Then("^can read email and name$")
    public void canReadEmailAndName() {
        Assert.assertNotNull(customer);
        Assert.assertNotNull(customer.getName());
        Assert.assertNotNull(customer.getContactEmail());
    }

    @Then("^can not update name$")
    public void canNotUpdateName() {
        expectedEx.expect(ModelSavingException.class);
        customer.setName("xyz");
        ServiceLookup.getModelService().save(customer);
    }

    @Given("^deal with ID '(\\d+)' exists$")
    public void dealWithIdExists(final String dealId) {
        deal = ServiceLookup.getTargetDealService().getDealForId(dealId);
        assertThat(deal).isNotNull();
    }

    @When("^the 'featured' value is set to '(true|false)'$")
    public void theFeaturedValueIsSetTo(final Boolean value) {
        deal.setFeatured(value);
    }

    @When("^the 'enabled' value is set to '(true|false)'$")
    public void theEnabledValueIsSetTo(final Boolean value) {
        deal.setEnabled(value);
    }

    @Then("^the deal can be saved$")
    public void theDealCanBeSaved() {
        ServiceLookup.getModelService().save(deal);
    }

    @Then("^the deal can not be saved$")
    public void theDealCanNotBeSaved() {
        try {
            ServiceLookup.getModelService().save(deal);
            fail();
        }
        catch (final ModelSavingException ex) {
            // Do nothing
        }
    }
}

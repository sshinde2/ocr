/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;


import au.com.target.tgttest.automation.facade.VoucherUtil;
import cucumber.api.java.en.When;


/**
 * @author cwijesu1
 * 
 */
public class VoucherSteps {

    @When("^expire the voucher \"(.*)\"$")
    public void expireThisVoucher(final String voucher) {
        VoucherUtil.expireVoucher(voucher);
    }
}

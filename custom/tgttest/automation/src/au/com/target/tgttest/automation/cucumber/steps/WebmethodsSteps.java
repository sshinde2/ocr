/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtebay.constants.TgtebayConstants;
import au.com.target.tgtebay.dto.EbayTargetAddressDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntriesDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderEntryDTO;
import au.com.target.tgtebay.dto.EbayTargetPayInfoDTO;
import au.com.target.tgtebay.exceptions.ImportOrderException;
import au.com.target.tgtpayment.model.PaynowPaymentInfoModel;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.BusinessProcessUtil;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.CustomerSubscriptionUtil;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.CartEntry;
import au.com.target.tgttest.automation.facade.bean.PartnerOrderFields;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttest.automation.mock.MockTargetCaInventoryService;
import au.com.target.tgttest.automation.mock.MockTargetCaShippingService;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwebmethods.ca.data.request.SubmitOrderShipmentRequest;
import au.com.target.tgtwebmethods.ca.data.request.UpdateInventoryItemQuantityRequest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Class for web methods related automation steps.
 *
 * @author bhuang3
 */
public class WebmethodsSteps {

    private static final String TEST_WM_MODIFY_ORDER = "wm_modify_order";

    private static final Logger LOG = Logger.getLogger(WebmethodsSteps.class);

    private final Random random = new Random();

    private EbayTargetOrderDTO orderDto;

    private String importedOrderCode;

    private OrderModel importedOrderModel;

    @Given("^(?:webmethods|provider) is offline for (\\d+) seconds$")
    public void setWebmethodsStatus(final int seconds) {
        CustomerSubscriptionUtil.setWebmethodsOffline(seconds);
        //--Todo add other webmethods mock client.
    }

    @Given("^a new order in webmethods$")
    public void givenANewOrder() {
        orderDto = new EbayTargetOrderDTO();

        createOrderDtoWithDeliveryType(getNextPositiveNumber(), null);
    }

    @Given("^a new order in webmethods with product entries$")
    public void givenANewOrderWithEntry(final List<CartEntry> entries) {
        orderDto = new EbayTargetOrderDTO();
        final EbayTargetOrderEntriesDTO dtoEntries = new EbayTargetOrderEntriesDTO();
        final List<EbayTargetOrderEntryDTO> entryList = new ArrayList<>();
        for (final CartEntry entry : entries) {
            final EbayTargetOrderEntryDTO dto = new EbayTargetOrderEntryDTO();
            dto.setProductCode(entry.getProduct());
            dto.setQuantity(String.valueOf(entry.getQty()));
            dto.setBasePrice(String.valueOf(entry.getPrice()));
            entryList.add(dto);
        }
        dtoEntries.setEntries(entryList);
        orderDto.setEbayTargetOrderEntriesDTO(dtoEntries);
    }

    @Given("^a new order in webmethods with:$")
    public void givenANewOrder(final List<PartnerOrderFields> orderDetails) {
        if (orderDto == null) {
            orderDto = new EbayTargetOrderDTO();
        }

        createOrderDtoWithDeliveryType(getNextPositiveNumber(), null);
        updateOrderEntryDetails(orderDetails.get(0));
        setOrderDetails(orderDetails, orderDto);
    }

    @Given("^a new order in webmethods with order id '(.*)'$")
    public void givenANewOrderWithOrderId(final String orderId) {
        createOrderDtoWithDeliveryType(orderId, null);
    }

    @Given("^a TradeMe order in webmethods$")
    public void givenTradeMeOrder() throws InterruptedException {
        orderDto = new EbayTargetOrderDTO();

        createOrderDtoWithDeliveryType(getNextPositiveNumber(), null);
        givenSalesChannel(SalesApplication.TRADEME.getCode());
        givenPaymentType("TradeMe");
        givenCurrency(TgtebayConstants.CURRENCY_CODE_NZD);
        givenCountry(TgtCoreConstants.COUNTRY_ISO_CODE_NEWZEALAND);
        whenOrderImported();
        thenOrderCreated(OrderStatus.CREATED.getCode());
    }

    @Given("^an eBay (delivery|express delivery|pickup) order in webmethods$")
    public void givenEbayOrder(final String deliveryType) throws InterruptedException {
        orderDto = new EbayTargetOrderDTO();

        createOrderDtoWithDeliveryType(getNextPositiveNumber(), deliveryType);
        givenSalesChannel(SalesApplication.EBAY.getCode());
        givenPaymentType("PayPal");
        givenCurrency(TgtebayConstants.CURRENCY_CODE_AUD);

        if ("express delivery".equals(deliveryType)) {
            populateAddressesFromDetails("Address 1", "3001", "Geelong", "Geelong",
                    TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA, "EXP");
        }
        else {
            populateAddressesFromDetails("Address 1", "3001", "Geelong", "Geelong",
                    TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA, "STD");
        }
        whenOrderImported();
        thenOrderCreated(OrderStatus.CREATED.getCode());
    }

    @Given("^an (eBay|TradeMe) '(delivery|express delivery|pickup)' webmethods order$")
    public void giveneBayorderWithEntries(final String salesChannel, final String deliveryType)
            throws InterruptedException {
        if (StringUtils.equalsIgnoreCase(salesChannel, "TradeMe")) {
            givenTradeMeOrder();
        }
        else {
            givenEbayOrder(deliveryType);
        }
    }

    @Given("^sales channel as '(.*)'$")
    public void givenSalesChannel(final String salesChannel) {
        orderDto.setSalesChannel(salesChannel);
    }

    @Given("^currency as '(.*)'$")
    public void givenCurrency(final String currency) {
        orderDto.setCurrencyCode(currency);
    }

    @Given("^webmethods order with entries:$")
    public void theNoOfEntries(final List<CartEntry> cartEntries) {
        final EbayTargetOrderEntriesDTO entries = new EbayTargetOrderEntriesDTO();
        final List<EbayTargetOrderEntryDTO> entryList = new ArrayList<>();
        for (final CartEntry entry : cartEntries) {
            final EbayTargetOrderEntryDTO entryDTO = new EbayTargetOrderEntryDTO();
            entryDTO.setProductCode(entry.getProduct());
            entryDTO.setQuantity(String.valueOf(entry.getQty()));
            entryDTO.setBasePrice(String.valueOf(entry.getPrice().doubleValue()));
            entryList.add(entryDTO);
        }
        entries.setEntries(entryList);
        orderDto.setEbayTargetOrderEntriesDTO(entries);
    }

    @Given("^an email address of '(.*)'$")
    public void anEmailAddressOf(final String emailAddress) {
        orderDto.setBuyerEmail(emailAddress);
    }

    @When("^the order is imported in hybris$")
    public void whenOrderImported() {
        //setting-up a user that belongs to wm_modify_ordergroup to have the required access 
        SessionUtil.userLogin(TEST_WM_MODIFY_ORDER);
        try {
            importedOrderCode = ServiceLookup.getTargetPartnerOrderService().createPartnerOrders(orderDto);
        }
        catch (final ImportOrderException ex) {
            LOG.info("Exception while creating partner order. Error message=" + ex.getMessage());
        }
    }

    @Then("^a new order in hybris is (created|not created)$")
    public void thenOrderCreated(final String orderCreationStatus) throws InterruptedException {
        if (StringUtils.equalsIgnoreCase(OrderStatus.CREATED.getCode(), orderCreationStatus)) {
            assertThat(importedOrderCode).isNotEmpty();
            importedOrderModel = ServiceLookup.getTargetOrderService().findOrderModelForOrderId(importedOrderCode);
            Order.setOrder(importedOrderModel);
            BusinessProcessUtil.waitForOrderBusinessProcesses();
        }
        else {
            assertThat(importedOrderCode).isNullOrEmpty();
        }
    }

    @Then("^sales channel is set to '(.*)'$")
    public void thenSalesChannelIs(final String salesChannel) {
        assertThat(importedOrderModel.getSalesApplication().toString()).isEqualTo(salesChannel);
    }

    @Then("^currency is set to '(.*)'$")
    public void thenCurrencyIs(final String currency) {
        assertThat(importedOrderModel.getCurrency().getIsocode()).isEqualTo(currency);
    }

    @Then("^date is set to '(.*)'$")
    public void thenDateIs(final String orderDate) {
        assertThat(importedOrderModel.getDate()).isNotNull().isEqualTo(TargetDateUtil.getStringAsDate(orderDate));
    }

    @Then("^exchange rate is set to '(.*)'$")
    public void thenExchangeRate(final String exchangeRate) {
        assertThat(String.valueOf(importedOrderModel.getCurrencyConversionFactor())).isEqualTo(exchangeRate);
    }

    @Then("^country is set to '(.*)'$")
    public void thenCountryIs(final String country) {
        assertThat(importedOrderModel.getDeliveryAddress()).isNotNull();
        assertThat(importedOrderModel.getDeliveryAddress().getCountry()).isNotNull();
        assertThat(importedOrderModel.getDeliveryAddress().getCountry().getIsocode()).isEqualTo(country);
    }

    @Then("^delivery mode is set to '(.*)'$")
    public void thenDeliveryModeIs(final String deliveryMode) {
        assertThat(importedOrderModel.getDeliveryMode().getCode()).isEqualTo(deliveryMode);
    }

    @Then("^delivery address is '(.*)'$")
    public void thenDeliveryAddressIs(final String deliveryaddress) {
        final String[] address = deliveryaddress.split(",");
        assertThat(importedOrderModel.getDeliveryAddress().getPostalcode()).isEqualTo(address[4]);
        assertThat(importedOrderModel.getDeliveryAddress().getDistrict()).isEqualTo(address[3]);
    }

    @Given("^payment type is '(.*)'$")
    public void givenPaymentType(final String paymentType) {
        final EbayTargetPayInfoDTO ebayTargetPayInfoDTO = new EbayTargetPayInfoDTO();
        ebayTargetPayInfoDTO.setPaymentType(paymentType);
        ebayTargetPayInfoDTO.setPaypalAccountId("234234234");
        ebayTargetPayInfoDTO.setPlannedAmount("100");
        ebayTargetPayInfoDTO.setTransactionId("3224234224");
        ebayTargetPayInfoDTO.setReceiptNumber("2323232");
        orderDto.setPaymentInfo(ebayTargetPayInfoDTO);
    }

    @Given("^country as '(.*)'$")
    public void givenCountry(final String country) {
        populateAddressesFromDetails("Moorabool", "3001", "Geelong", "Geelong", country, null);
    }

    @Given("^delivery address details are:$")
    public void deliveryAddressInformation(final List<AddressEntry> addressEntries) {
        final AddressEntry addressEntry = addressEntries.get(0);
        populateAddressesFromDetails(addressEntry.getAddressLine1(), addressEntry.getPostalCode(),
                addressEntry.getTown(), addressEntry.getRegion(), addressEntry.getCountry(),
                addressEntry.getShippingClass());
    }

    @Given("^the phone number is '(.*)'$")
    public void thePhoneNumberIS(final String phoneNumber) {
        orderDto.getPaymentAddress().setPhone1(phoneNumber);
        orderDto.getDeliveryAddress().setPhone1(phoneNumber);
    }

    @Then("^the phone number is stored as '(.*)'$")
    public void thePhoneNumberIsSavedAs(final String phoneNumber) {
        ServiceLookup.getModelService().refresh(importedOrderModel);
        assertThat(importedOrderModel.getDeliveryAddress().getPhone1()).isEqualTo(phoneNumber);
        assertThat(importedOrderModel.getPaymentAddress().getPhone1()).isEqualTo(phoneNumber);
    }

    @Then("^total tax is set to '(.*)'$")
    public void theTotalTaxIs(final String totalTax) {
        final String totalTaxForOrder;
        if (importedOrderModel.getTotalTax().equals(Double.valueOf(0.00))) {
            totalTaxForOrder = "zero";
        }
        else {
            totalTaxForOrder = "notZero";
        }
        assertThat(totalTaxForOrder).isEqualTo(totalTax);
    }

    @Then("^tax group is set to '(.*)'$")
    public void theTaxGroupIs(final String taxGroup) {
        final String taxGroupForOrder;
        if (CollectionUtils.isEmpty(importedOrderModel.getTotalTaxValues())) {
            taxGroupForOrder = "empty";
        }
        else {
            taxGroupForOrder = "notEmpty";
        }
        assertThat(taxGroupForOrder).isEqualTo(taxGroup);
    }

    @Then("^order store is set to '(.*)'$")
    public void theOrderStoreIs(final String orderStore) {
        if (StringUtils.equalsIgnoreCase(orderStore, "Null")) {
            assertThat(importedOrderModel.getCncStoreNumber()).isNull();
        }
        else {
            assertThat(importedOrderModel.getCncStoreNumber().toString()).isEqualTo(orderStore);
        }
    }


    @Then("^payment mode is '(.*)'$")
    public void thePaymentInfoTypeIs(final String paymentInfo) {
        assertThat(importedOrderModel.getPaymentInfo()).isNotNull();
        boolean success = false;
        if (StringUtils.equalsIgnoreCase(paymentInfo, "paypal")) {
            assertThat(importedOrderModel.getPaymentInfo()).isInstanceOf(PaypalPaymentInfoModel.class);
            success = true;
        }
        else if (StringUtils.equalsIgnoreCase(paymentInfo, "paynow")) {
            assertThat(importedOrderModel.getPaymentInfo()).isInstanceOf(PaynowPaymentInfoModel.class);
            success = true;
        }
        assertThat(success).isTrue();
    }

    @Then("^partner inventory update should be '(triggered|not triggered)'$")
    public void thePartnerInventoryIsUpdated(final String triggeredStatus) {
        final UpdateInventoryItemQuantityRequest updateInventoryItemQuantityRequest = MockTargetCaInventoryService
                .getUpdateInventoryItemQuantityRequest();

        if (StringUtils.equalsIgnoreCase(triggeredStatus, "triggered")) {
            assertThat(updateInventoryItemQuantityRequest.getProductList()).isNotEmpty();
            assertThat(importedOrderModel.getEntries().get(0).getProduct().getCode()).isEqualTo(
                    updateInventoryItemQuantityRequest.getProductList().get(0).getProductCode());

        }
        else {
            assertThat(updateInventoryItemQuantityRequest).isNull();
        }
    }

    @Then("^Shipment notification is '(sent|not sent)' to (eBay|TradeMe)$")
    public void thenShipmentNotificationSent(final String sentStatus, final String salesAppName) {
        final SubmitOrderShipmentRequest shipmentRequest = MockTargetCaShippingService.getShipmentRequest();
        if (StringUtils.equalsIgnoreCase(sentStatus, "not sent")) {
            assertThat(shipmentRequest).isNull();
        }
        else {
            assertThat(shipmentRequest).isNotNull();
            assertThat(shipmentRequest.getSalesChannel()).isNotEmpty().isEqualTo(salesAppName);
        }
    }

    @Then("^carrier code used in notification '(Australia Post|StarTrack|CPU|N/A)'$")
    public void thenCarrierCodeSentToEBay(final String carrierCode) {
        final SubmitOrderShipmentRequest shipmentRequest = MockTargetCaShippingService.getShipmentRequest();
        if (carrierCode.equalsIgnoreCase("N/A")) {
            assertThat(shipmentRequest).isNull();
        }
        else {
            assertThat(shipmentRequest.getShipmentList().get(0).getCarrierCode().equalsIgnoreCase(carrierCode))
                    .isTrue();
        }
    }

    @Then("^carrier class used in notification '(STD|EXP|CPU|N/A)'$")
    public void thenCarrierClassSentToEBay(final String carrierClass) {
        final SubmitOrderShipmentRequest shipmentRequest = MockTargetCaShippingService.getShipmentRequest();
        if (carrierClass.equalsIgnoreCase("N/A")) {
            assertThat(shipmentRequest).isNull();
        }
        else {
            assertThat(shipmentRequest.getShipmentList().get(0).getClassCode().equalsIgnoreCase(carrierClass))
                    .isTrue();
        }
    }

    @Then("^order email address is '(.*)'$")
    public void thenOrderEmailAddressIs(final String emailAddress) {
        assertThat(((TargetCustomerModel)importedOrderModel.getUser()).getContactEmail()).isEqualTo(emailAddress);
    }

    /**
     * @param addressLine1
     * @param postalCode
     * @param town
     * @param region
     * @param country
     * @param shippingClass
     */
    private void populateAddressesFromDetails(final String addressLine1, final String postalCode, final String town,
            final String region,
            final String country, final String shippingClass) {

        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        addressDto.setTitle("Mr");
        addressDto.setFirstName("CustomerFirstName");
        addressDto.setLastName("CustomerLastName");
        addressDto.setAddressLine1(addressLine1);
        addressDto.setPostcode(postalCode);
        addressDto.setTown(town);
        addressDto.setDistrict(region);
        addressDto.setCountry(country);
        addressDto.setShippingClass(shippingClass);
        orderDto.setPaymentAddress(addressDto);
        orderDto.setDeliveryAddress(addressDto);
    }

    /**
     * @param orderId
     * @param deliveryType
     */
    private void createOrderDtoWithDeliveryType(final String orderId, final String deliveryType) {
        if (orderDto == null) {
            orderDto = new EbayTargetOrderDTO();
        }
        orderDto.seteBayOrderNumber(orderId);
        orderDto.setBuyerEmail("test@email.com");

        if (orderDto.getEbayTargetOrderEntriesDTO() == null) {
            final EbayTargetOrderEntryDTO entry = new EbayTargetOrderEntryDTO();
            entry.setProductCode(CheckoutUtil.ANY_PRD_CODE);
            entry.setQuantity(String.valueOf(CheckoutUtil.ANY_PRD_QTY));
            entry.setBasePrice(String.valueOf(CheckoutUtil.ANY_PRD_PRICE));

            if ("Pickup".equalsIgnoreCase(deliveryType)) {
                entry.setFulfillmentType("Pickup");
                entry.setCncStoreNumber("7032");
            }
            else {
                entry.setFulfillmentType("Ship");
            }

            final EbayTargetOrderEntriesDTO entries = new EbayTargetOrderEntriesDTO();
            entries.setEntries(Collections.singletonList(entry));

            orderDto.setEbayTargetOrderEntriesDTO(entries);
        }
    }

    /**
     * @param orderDetail
     */
    private void updateOrderEntryDetails(final PartnerOrderFields orderDetail) {
        final EbayTargetOrderEntryDTO ebayTargetOrderEntryDTO = orderDto.getEbayTargetOrderEntriesDTO().getEntries()
                .get(0);
        final String fullfilmentType = StringUtils.equalsIgnoreCase(orderDetail.getFulfillmentType(), "Null") ? null
                : orderDetail.getFulfillmentType();
        final String storeNumber = StringUtils.equalsIgnoreCase(orderDetail.getStoreNumber(), "Null") ? null
                : orderDetail
                        .getStoreNumber();
        ebayTargetOrderEntryDTO
                .setCncStoreNumber(storeNumber);
        ebayTargetOrderEntryDTO.setFulfillmentType(fullfilmentType);

    }

    /**
     * @param orderDetails
     * @param eBayorderDTO
     */
    private void setOrderDetails(final List<PartnerOrderFields> orderDetails, final EbayTargetOrderDTO eBayorderDTO) {
        final PartnerOrderFields orderDetail = orderDetails.get(0);
        eBayorderDTO.setSalesChannel(orderDetail.getSalesChannel());
        eBayorderDTO.setCurrencyCode(orderDetail.getCurrency());
        eBayorderDTO.setDeliveryCost(orderDetail.getDeliveryCost());
        if (StringUtils.isNotEmpty(orderDetail.getDeliveryAddress())) {
            populateAddressDto(orderDetail.getDeliveryAddress(), orderDetail.getCountry());
        }
        else if (StringUtils.isNotEmpty(orderDetail.getCountry())) {
            givenCountry(orderDetail.getCountry());
        }
        givenPaymentType(orderDetail.getPaymentType());
        eBayorderDTO.setCreatedDate(orderDetail.getCreatedDate());
    }

    /**
     * @param deliveryAddress
     *            the delivery address
     * @param country
     *            the country
     */
    private void populateAddressDto(final String deliveryAddress, final String country) {
        final EbayTargetAddressDTO addressDto = new EbayTargetAddressDTO();
        final String[] address = deliveryAddress.split(",");
        assertThat(address).isNotEmpty();
        assertThat(address.length).isGreaterThanOrEqualTo(4);
        addressDto.setTitle("Mr");
        addressDto.setFirstName("CustomerFistName");
        addressDto.setLastName("CustomerLastName");
        addressDto.setAddressLine1(address[0].trim());
        addressDto.setAddressLine2(address[1].trim());
        addressDto.setTown(address[2].trim());
        addressDto.setPostcode(address[4].trim());
        addressDto.setDistrict(address[3].trim());
        addressDto.setCountry(country);
        orderDto.setPaymentAddress(addressDto);
        orderDto.setDeliveryAddress(addressDto);
    }

    private String getNextPositiveNumber() {
        return String.valueOf(random.nextInt(Integer.MAX_VALUE) + 1);
    }

}
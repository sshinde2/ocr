/**
 * 
 */
package au.com.target.tgttest.automation.cucumber.steps;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.junit.Assert;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.product.data.CustomerProductInfo;
import au.com.target.tgtfacades.product.data.TargetProductListerData;
import au.com.target.tgtfacades.product.data.TargetSelectedVariantData;
import au.com.target.tgtfacades.product.data.TargetVariantProductListerData;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.SessionUtil;
import au.com.target.tgttest.automation.facade.WishListUtil;
import au.com.target.tgttest.automation.facade.bean.WishListEntry;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.exception.AddToWishListException;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author rsamuel3
 *
 */
public class WishListSteps {

    private List<WishListEntry> localStorageEntries;
    private List<WishListEntry> savedStorageEntries;
    private String savedEmailAddress;

    private String shareWishListRecipientEmail;
    private String shareWishListRecipientName;
    private String shareWishListMessage;

    private List<TargetProductListerData> sharedProducts;


    @Given("^the local storage has:$")
    public void givenLocalStorageEntries(final List<WishListEntry> wishListEntries) {
        setLocalStorageEntries(wishListEntries);
    }

    @Given("^the local storage is empty$")
    public void givenEmptyLocalStorage() {
        setLocalStorageEntries(new ArrayList<WishListEntry>());
    }

    @Given("^customer '(.*)' having no favourites$")
    public void givenSignedInCustomerHasNoWishList(final String emailAddress) throws ParseException {
        SessionUtil.userLogin(emailAddress);
        setUpWishListData(emailAddress, false, ListUtils.EMPTY_LIST);
    }

    @Given("^customer '(.*)' with an empty favourites$")
    public void givenSignedInCustomerEmptyWishList(final String emailAddress) throws ParseException {
        SessionUtil.userLogin(emailAddress);
        setUpWishListData(emailAddress, true, ListUtils.EMPTY_LIST);
    }

    @Given("^the favourites for the customer '(.*)' is:$")
    public void givenCustomerSavedFavourites(final String emailAddress, final List<WishListEntry> wishListEntries)
            throws ParseException {
        SessionUtil.userLogin(emailAddress);
        setUpWishListData(emailAddress, true, wishListEntries);
    }

    @Given("^the max a user can add to his favourites is '(\\d+)'$")
    public void givenMaxFavouriteAllowed(final int maxAllowed) {
        if (maxAllowed > 0) {
            WishListUtil.setMaxEntries(Integer.valueOf(maxAllowed));
        }
    }

    @Given("the favourites for the customer '(.*)' to share has the following products:")
    public void favouriteForACustomerBeingShared(final String emailAddress,
            final List<WishListEntry> wishListEntryShared) {
        SessionUtil.userLogin(emailAddress);
        createProductsToShare(wishListEntryShared);
    }

    @Given("^webmethods returns '(SUCCESS|ERROR|UNAVAILABLE|success|error|unavailable)' for share favourites")
    public void setWebmethodsShareFavouritesResponse(final String status) {
        ServiceLookup.getShareFavouritesRestClient().setActive(true);
        ServiceLookup.getShareFavouritesRestClient().setResponseType(status.toLowerCase());
    }

    @When("^customer favourites the following product:$")
    public void customerFavouritesProduct(final List<WishListEntry> expectedWishListEntries)
            throws AddToWishListException, ParseException {
        final WishListEntry wishListEntry = expectedWishListEntries.get(0);
        ServiceLookup.getTargetWishListFacade().addProductToList(convertWishListEntryToProductInfo(wishListEntry),
                null, WishListTypeEnum.FAVOURITE);
    }

    @When("^the customer removes favourites from his favourites list:$")
    public void customerRemovesFavourites(final List<WishListEntry> removedWishListEntries) throws ParseException {
        final List<CustomerProductInfo> productInfos = convertWishListEntriesToProductInfo(removedWishListEntries);
        ServiceLookup.getTargetWishListFacade().removeProductsFromList(productInfos, StringUtils.EMPTY,
                WishListTypeEnum.FAVOURITE);
    }

    @When("the customer shares the favourites with the recipient '(.*)'$")
    public void givenShareWishListRecipientName(final String recipientName) throws ParseException {
        this.shareWishListRecipientName = recipientName;
    }

    @When("the recipient Email address is '(.*)'$")
    public void givenShareWishListRecipientEmail(final String recipientEmail) throws ParseException {
        this.shareWishListRecipientEmail = recipientEmail;
    }

    @When("the message to the recipient is '(.*)'$")
    public void givenShareWishListRecipientMessage(final String recipientMessage) throws ParseException {
        this.shareWishListMessage = recipientMessage;
    }

    @When("^the following variants are moved to a different style group:$")
    public void variantMovesToAnotherStyleGroup(final List<WishListEntry> wishListEntries) {
        final WishListEntry wishListEntry = wishListEntries.get(0);
        final String newBaseProductCode = wishListEntry.getBaseProductCode();
        final List<String> variantCodes = new ArrayList<>();
        variantCodes.add(wishListEntry.getSelectedVariantCode());
        ServiceLookup.getTargetWishListService().updateWishListsWithNewBaseProductCode(newBaseProductCode,
                variantCodes);
    }

    @Then("^favourites list will be created for the customer with type '(.*)' and visibility '(.*)'$")
    public void thenFavouritesListIsCreated(final String type, final String visibility)
            throws ParseException {
        final TargetWishListModel targetWishListModel = WishListUtil.retrieveWishList(savedEmailAddress, null,
                WishListTypeEnum.FAVOURITE);
        Assertions.assertThat(targetWishListModel.getEntries()).isNotEmpty().isNotEmpty();
        Assertions.assertThat(targetWishListModel.getType()).isNotNull();
        Assertions.assertThat(targetWishListModel.getType().getCode()).isEqualTo(type);
        Assertions.assertThat(targetWishListModel.getVisibility().getCode()).isEqualTo(visibility);
    }


    @Then("^the favourites for the customer in server has:$")
    public void thenFavouritesForCustomerInServer(final List<WishListEntry> expectedWishListEntries)
            throws ParseException {
        final TargetWishListModel targetWishListModel = WishListUtil.retrieveWishList(savedEmailAddress, null,
                WishListTypeEnum.FAVOURITE);
        Assertions.assertThat(targetWishListModel.getEntries()).isNotEmpty().isNotEmpty();
        verifyUpdatedWishListEntriesInServer(targetWishListModel.getEntries(), expectedWishListEntries);
    }

    @Then("^the favourites for the customer in server is empty$")
    public void thenFavouritesForCustomerInServer()
            throws ParseException {
        thenFavouritesForCustomer(Collections.EMPTY_LIST);
    }

    @Then("^the favourites for the customer has:$")
    public void thenFavouritesForCustomer(final List<WishListEntry> expectedWishListEntries)
            throws AddToWishListException, ParseException {
        final List<CustomerProductInfo> productInfos = convertWishListEntriesToProductInfo(localStorageEntries);
        final List<CustomerProductInfo> updatedWishList = ServiceLookup.getTargetWishListFacade().syncFavouritesList(
                productInfos);
        verifyUpdatedWishList(updatedWishList, expectedWishListEntries);
    }

    @Then("the email with the favourites is sent")
    public void shareWishListViaEmailSuccess() {
        Assertions.assertThat(ServiceLookup.getTargetWishListFacade().shareWishListForUser(sharedProducts,
                shareWishListRecipientEmail, shareWishListRecipientName, shareWishListMessage)).isTrue();
    }

    @Then("the email with the favourites is not sent")
    public void shareWishListViaEmailFail() {
        Assertions.assertThat(ServiceLookup.getTargetWishListFacade().shareWishListForUser(sharedProducts,
                shareWishListRecipientEmail, shareWishListRecipientName, shareWishListMessage)).isFalse();
    }

    @Given("^the product association for '(.*)' is removed$")
    public void removeProduct(final String productCode) {
        final ModelService modelService = ServiceLookup.getModelService();
        final TargetCustomerModel currentCustomer = (TargetCustomerModel)ServiceLookup.getUserService()
                .getCurrentUser();
        final TargetWishListModel targetWishListModel = ServiceLookup.getTargetWishListService()
                .verifyAndCreateList(StringUtils.EMPTY, currentCustomer, WishListTypeEnum.FAVOURITE);

        for (final Wishlist2EntryModel wishListEntry : targetWishListModel.getEntries()) {
            if (productCode.equals(wishListEntry.getProduct().getCode())) {
                modelService.remove(wishListEntry.getProduct());
                modelService.refresh(wishListEntry);
                break;
            }
        }
    }

    /**
     * @param updatedWishList
     * @param expectedWishListEntries
     * @throws ParseException
     */
    private void verifyUpdatedWishList(final List<CustomerProductInfo> updatedWishList,
            final List<WishListEntry> expectedWishListEntries) throws ParseException {
        if (CollectionUtils.isEmpty(expectedWishListEntries)) {
            Assertions.assertThat(updatedWishList).isNullOrEmpty();
        }
        else {
            Assertions.assertThat(updatedWishList).isNotNull().isNotEmpty().hasSize(expectedWishListEntries.size());
            for (final WishListEntry expectedEntry : expectedWishListEntries) {
                final CustomerProductInfo updatedEntry = find(updatedWishList, expectedEntry.getBaseProductCode());
                verifyUpdatedEntry(updatedEntry, expectedEntry);
            }
        }
    }

    /**
     * @param updatedEntry
     * @param expectedEntry
     * @throws ParseException
     */
    private void verifyUpdatedEntry(final CustomerProductInfo updatedEntry, final WishListEntry expectedEntry)
            throws ParseException {
        Assertions.assertThat(updatedEntry.getSelectedVariantCode()).isNotNull().isNotEmpty()
                .isEqualTo(expectedEntry.getSelectedVariantCode());
        Assertions.assertThat(updatedEntry.getTimeStamp()).isNotNull().isNotEmpty()
                .isEqualTo(expectedEntry.getTimestamp());
    }



    /**
     * @param updatedWishList
     * @param baseProductCode
     * @return customerProductInfo that is related to baseProductCode
     */
    private CustomerProductInfo find(final List<CustomerProductInfo> updatedWishList, final String baseProductCode) {
        for (final CustomerProductInfo updatedEntry : updatedWishList) {
            if (StringUtils.equals(updatedEntry.getBaseProductCode(), baseProductCode)) {
                return updatedEntry;
            }
        }
        return null;
    }

    /**
     * @param wishListEntriesModel
     * @param expectedWishListEntries
     * @throws ParseException
     */
    private void verifyUpdatedWishListEntriesInServer(final List<Wishlist2EntryModel> wishListEntriesModel,
            final List<WishListEntry> expectedWishListEntries) throws ParseException {
        Assertions.assertThat(wishListEntriesModel).isNotNull().isNotEmpty().hasSize(expectedWishListEntries.size());
        for (final WishListEntry expectedEntry : expectedWishListEntries) {
            verifyWishListEntryInServer(wishListEntriesModel, expectedEntry);
        }
    }

    /**
     * @param wishListEntriesModel
     * @param expectedEntry
     * @throws ParseException
     * 
     */
    private void verifyWishListEntryInServer(final List<Wishlist2EntryModel> wishListEntriesModel,
            final WishListEntry expectedEntry) throws ParseException {
        for (final Wishlist2EntryModel wishListEntryModel : wishListEntriesModel) {
            //Verifies all entries in the list are of the type TargetWishListEntryModel.
            Assertions.assertThat(wishListEntryModel).isInstanceOf(TargetWishListEntryModel.class);
            final TargetWishListEntryModel targetWishListEntryModel = (TargetWishListEntryModel)wishListEntryModel;
            //Verifies the details for a expected entry.
            if (expectedEntry.getBaseProductCode().equals(wishListEntryModel.getProduct().getCode())) {
                Assertions.assertThat(targetWishListEntryModel.getSelectedVariant())
                        .isEqualTo(expectedEntry.getSelectedVariantCode());
                Assertions.assertThat(String.valueOf(wishListEntryModel.getAddedDate().getTime()))
                        .isEqualTo(expectedEntry.getTimestamp());
                return;
            }
        }
        Assert.fail("Expected Entry Not Found" + expectedEntry.getBaseProductCode());
    }

    /**
     * @param storageEntries
     * @return List<CustomerProductInfo>
     * @throws ParseException
     */
    private List<CustomerProductInfo> convertWishListEntriesToProductInfo(final List<WishListEntry> storageEntries)
            throws ParseException {
        final List<CustomerProductInfo> wishLists = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(storageEntries)) {
            for (final WishListEntry entry : storageEntries) {
                final CustomerProductInfo wishListEntry = convertWishListEntryToProductInfo(entry);
                wishLists.add(wishListEntry);
            }
        }
        return wishLists;
    }

    /**
     * @param entry
     * @return CustomerProductInfo
     * @throws ParseException
     */
    protected CustomerProductInfo convertWishListEntryToProductInfo(final WishListEntry entry) throws ParseException {
        final CustomerProductInfo wishListEntry = new CustomerProductInfo();
        wishListEntry.setBaseProductCode(entry.getBaseProductCode());
        wishListEntry.setSelectedVariantCode(entry.getSelectedVariantCode());
        wishListEntry.setTimeStamp(entry.getTimestamp());
        return wishListEntry;
    }

    /**
     * @return the localStorageEntries
     */
    public List<WishListEntry> getLocalStorageEntries() {
        return localStorageEntries;
    }

    /**
     * @param localStorageEntries
     *            the localStorageEntries to set
     */
    public void setLocalStorageEntries(final List<WishListEntry> localStorageEntries) {
        this.localStorageEntries = localStorageEntries;
    }

    /**
     * @return the savedStorageEntries
     */
    public List<WishListEntry> getSavedStorageEntries() {
        return savedStorageEntries;
    }

    /**
     * @param savedStorageEntries
     *            the savedStorageEntries to set
     */
    public void setSavedStorageEntries(final List<WishListEntry> savedStorageEntries) {
        this.savedStorageEntries = savedStorageEntries;
    }

    /**
     * @return the savedEmailAddress
     */
    public String getSavedEmailAddress() {
        return savedEmailAddress;
    }

    /**
     * @param savedEmailAddress
     *            the savedEmailAddress to set
     */
    public void setSavedEmailAddress(final String savedEmailAddress) {
        this.savedEmailAddress = savedEmailAddress;
    }

    /**
     * 
     * @param emailAddress
     * @param createList
     * @param entries
     * @throws ParseException
     */
    private void setUpWishListData(final String emailAddress, final boolean createList,
            final List<WishListEntry> entries) throws ParseException {
        setSavedEmailAddress(emailAddress);
        final UserModel currentCustomer = ServiceLookup.getUserService().getUserForUID(emailAddress);
        WishListUtil.removeFavouritesForCustomer(currentCustomer);
        if (createList) {
            final TargetWishListModel targetWishListModel = ServiceLookup.getTargetWishListService()
                    .verifyAndCreateList(StringUtils.EMPTY, currentCustomer, WishListTypeEnum.FAVOURITE);
            if (CollectionUtils.isNotEmpty(entries)) {
                WishListUtil.setUpProductsForCustomerWishList(targetWishListModel, entries);
            }
        }
    }

    private void createProductsToShare(final List<WishListEntry> productsToShare) {
        sharedProducts = new ArrayList<>();
        for (final WishListEntry entry : productsToShare) {
            final TargetProductListerData targetProductListerData = new TargetProductListerData();
            targetProductListerData.setBaseProductCode(entry.getBaseProductCode());
            final TargetVariantProductListerData targetVariantProductListerData = new TargetVariantProductListerData();
            targetVariantProductListerData.setGridImageUrls(Arrays.asList(entry.getImageUrl()));
            targetProductListerData.setTargetVariantProductListerData(Arrays.asList(targetVariantProductListerData));
            if (StringUtils.isNotEmpty(entry.getPriceRange())) {
                final String[] priceRange = entry.getPriceRange().split("-");
                Assertions.assertThat(priceRange).hasSize(2);
                final PriceData minPrice = new PriceData();
                minPrice.setValue(new BigDecimal(priceRange[0]));
                minPrice.setFormattedValue(priceRange[0]);
                final PriceData maxPrice = new PriceData();
                maxPrice.setValue(new BigDecimal(priceRange[1]));
                maxPrice.setFormattedValue(priceRange[1]);

                final PriceRangeData priceRangeData = new PriceRangeData();
                priceRangeData.setMaxPrice(maxPrice);
                priceRangeData.setMinPrice(minPrice);

                targetProductListerData.setPriceRange(priceRangeData);
            }
            else {
                final PriceData priceData = new PriceData();
                priceData.setValue(new BigDecimal(entry.getPrice()));
                priceData.setFormattedValue(entry.getPrice());
                targetProductListerData.setPrice(priceData);
            }
            if (entry.getSelectedVariantCode() != null) {
                final TargetSelectedVariantData selectedVariantData = new TargetSelectedVariantData();
                selectedVariantData.setProductCode(entry.getSelectedVariantCode());
                selectedVariantData.setUrl("/productName/" + entry.getSelectedVariantCode());
                selectedVariantData.setPrice(targetProductListerData.getPrice());
                selectedVariantData.setColor(entry.getColour());
                selectedVariantData.setSize(entry.getSize());
            }
            targetProductListerData.setUrl("/productName/" + entry.getBaseProductCode());
            sharedProducts.add(targetProductListerData);
        }
    }
}

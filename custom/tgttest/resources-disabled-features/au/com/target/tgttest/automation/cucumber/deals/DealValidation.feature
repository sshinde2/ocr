@voucherData
Feature: performing partial cancellation during shortpick
  while preserving promotional value of remaining deal items

  Scenario: Refunding an order with no deals or discounts.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order
    And return entries
      | product       | qty |
      | P4025_black_S | 1   |
    Then order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 24.0  | 15.0     | 9.0          | 2.18     | 0.0            |
    And tlog total tender is -1500

#buy 2 and get one for 30 each
  Scenario: Refund an order with qtyBreakdeal items.
    Given cart entries:
      | product | qty | price |
      | CP7038  | 2   | 40    |
    And delivery mode is 'home-delivery'
    When place order
    And return entries
      | product | qty |
      | CP7038  | 1   |
    Then refund value should be '30.00'
    And order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 39.00 | 30.00    | 9.0          | 3.54     | 10.00          |
    And tlog entries are:
      | code   | qty | price | filePrice |
      | CP7038 | 1   | -3000 | 3000      |
    And tlog type is 'refund'
    And tlog user is 'system'
    And tlog shipping is 900
    And tlog total tender is -3000

#buy 4 and get one for 25 each
  Scenario: Partial cancel an order with qtyBreakdeal items.
    Given cart entries:
      | product | qty | price |
      | CP7039  | 4   | 40    |
    And delivery mode is 'home-delivery'
    When place order
    And cancel entries
      | product | qty |
      | CP7039  | 1   |
    Then refund value should be '25.00'
    And order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 84.00 | 75.00    | 9.0          | 7.63     | 45.00          |
    And tlog total tender is -800

#Buy 2 jeans get a tshirt half price
  Scenario: Partial Cancel the reward product of a order with buyGetDeal items.
    Given cart entries:
      | product     | qty | price |
      | CP4067      | 2   | 10    |
      | CP6018      | 1   | 10    |
      | P1010_RED_M | 1   | 20    |
    And delivery mode is 'home-delivery'
    When place order
    And cancel entries
      | product | qty |
      | CP6018  | 1   |
    Then refund value should be '5.00'
    And order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 49.00 | 40.00    | 9.00         | 4.45     | 0.00           |
    And tlog total tender is -500

# Buy 2 jeans get a third half price
  Scenario: Partial Cancel the reward product of a order with buyGetSameListDeal items.
    Given cart entries:
      | product     | qty | price |
      | CP4068      | 3   | 10    |
      | P1010_RED_M | 1   | 20    |
    And delivery mode is 'home-delivery'
    When place order
    And cancel entries
      | product | qty |
      | CP4068  | 1   |
    Then refund value should be '10.00'
    And order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 44.00 | 35.00    | 9.00         | 4.00     | 5.00           |
    And tlog total tender is -1000

# buy a bike and helmet and save $50
  Scenario: Partial Cancel an order with valueBundle items.
    Given cart entries:
      | product | qty | price |
      | CP7032  | 1   | 500   |
      | CP7033  | 1   | 100   |
    And delivery mode is 'home-delivery'
    When place order
    And cancel entries
      | product | qty |
      | CP7032  | 1   |
    Then refund value should be '458.33'
    And order is:
      | total  | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 100.67 | 91.67    | 9.00         | 9.15     | 8.33           |
    And tlog total tender is -45833

#Games spend 50 save 20
  Scenario: Partial Cancel an order with spendAndSave items.
    Given cart entries:
      | product | qty | price |
      | CP7036  | 2   | 20    |
      | CP7035  | 2   | 20    |
    When place order
    And cancel entries
      | product | qty |
      | CP2032  | 1   |
    Then refund value should be '7.50'
    And order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 24.00 | 15.00    | 9.00         | 2.18     | 2.50           |
    And tlog total tender is -750


# buy a bike and helmet and save $50
  Scenario: Partial Cancel an item from a valueBundle that got two instance
    Given cart entries:
      | product     | qty | price |
      | CP7033      | 2   | 100   |
      | CP7032      | 2   | 500   |
      | P4029_RED_S | 1   | 10    |
    And delivery mode is 'home-delivery'
    When place order
    And cancel entries
      | product | qty |
      | CP7033  | 1   |
    Then refund value should be '91.67'
    Then order is:
      | total   | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 1017.33 | 1008.33  | 9.00         | 92.48    | 41.67          |
    And tlog total tender is -9167

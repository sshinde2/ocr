Feature: Retrieving product information from POS store

  # Store 0000 is not in hybris
  # Store 7001 is the Geelong store
  Scenario: Getting product information from a store, but store not in hybris
    When get pos product 1111 from store 0000
    Then pos request is empty
    And result product info is:
      | itemcode | desc | price | was | errorCode                         |
      |          |      |       |     | Store number is not in the system |

  Scenario: Getting product information from a store, successful
    Given pos product data:
      | store | ean  | itemcode | desc       | price | was  |
      | 7001  | 1111 | P1000    | Baby shoes | 1000  | 1200 |
    When get pos product 1111 from store 7001
    Then pos request is
      | store | state | ean  |
      | 7001  | VIC   | 1111 |
    And result product info is:
      | itemcode | desc       | price | was  | errorCode |
      | P1000    | Baby shoes | 1000  | 1200 |           |

  Scenario: Getting product information from a store, product not found
    Given pos product data:
      | store | ean  | itemcode | desc       | price | was  |
      | 7001  | 1111 | P1000    | Baby shoes | 1000  | 1200 |
    When get pos product 2222 from store 7001
    Then pos request is
      | store | state | ean  |
      | 7001  | VIC   | 2222 |
    And result product info is:
      | itemcode | desc | price | was | errorCode          |
      |          |      |       |     | Itemcode not found |

  Scenario: Getting product information from a store, product is banned
    Given pos product data:
      | store | ean  | errorCode      |
      | 7001  | 1111 | Item is banned |
    When get pos product 1111 from store 7001
    Then pos request is
      | store | state | ean  |
      | 7001  | VIC   | 1111 |
    And result product info is:
      | itemcode | desc | price | was | errorCode      |
      |          |      |       |     | Item is banned |

Feature: Placing order with paypalhere creates order

  Scenario: Placing an order with paypalhere where transaction approved.
    Given store employee is 'store7001Employee'
    And cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order with paypalhere
    Then order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 39.0  | 30.0     | 9.0          |          | 0.0            |
    And order employee is 'store7001Employee'
    And tlog paypal payment info matches

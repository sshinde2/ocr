@pinpadData
Feature: Placing order with pinpad creates order but waits for update
  from the pinpad to the hybris web service before finishing the order. 
  Shortpick or zero pick is received from fastline

  Scenario: Placing an order with pinpad and then getting successful webservice update completes the order.
    Short pick file received from fastline

   Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    And start order with pinpad store 5098
    And pinpad sends payment update with success 'true'
    And finish order
    And business process completes
    When fastline returns pick entry data:
       | product       | qty|
       | P4025_black_S | 1  |
    Then csticket created
    And headline is 'Short or Zero pick received for an order from kiosk, but was unexpected'
    And subject is 'Short or Zero pick received for an order from kiosk, but was unexpected'
    And body is 'Short or Zero pick received for an order that does not allow them.'
    
   Scenario: Placing an order with deals.Short pick file received from fastline

    Given cart entries:
      | product  | qty | price |
      | 10000001 | 2   | 10    |
    And quantity break deal product '10000001'
    And quantity break deal break points:
      | qty | unitPrice |
      | 2   | 8         |
    And delivery mode is 'home-delivery'
    And start order with pinpad store 5098
    And pinpad sends payment update with success 'true'
    And finish order
    And business process completes
    When fastline returns pick entry data:
       | product       | qty|
       | 10000001 | 1  |
    Then csticket created
    And headline is 'Short pick on order with deal for an order from kiosk'
    And subject is 'Short pick on order with deal for an order from kiosk'
    And body is 'Short pick received for an order that contains deal items.'
    
    Scenario: Placing an order with pinpad and then getting successful webservice update completes the order.
    Zero pick file received from fastline

   Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    And start order with pinpad store 5098
    And pinpad sends payment update with success 'true'
    And finish order
    And business process completes
    When fastline returns pick entry data:
       | product       | qty|
       | P4025_black_S | 0  |
    Then csticket created
    And headline is 'Short or Zero pick received for an order from kiosk, but was unexpected'
    And subject is 'Short or Zero pick received for an order from kiosk, but was unexpected'
    And body is 'Short or Zero pick received for an order that does not allow them.'
    
    
    Scenario: Placing an order with deals.Zero pick file received from fastline

    Given cart entries:
      | product  | qty | price |
      | 10000001 | 2   | 10    |
    And quantity break deal product '10000001'
    And quantity break deal break points:
      | qty | unitPrice |
      | 2   | 8         |
    And delivery mode is 'home-delivery'
    And start order with pinpad store 5098
    And pinpad sends payment update with success 'true'
    And finish order
    And business process completes
    When fastline returns pick entry data:
       | product       | qty|
       | 10000001 | 0  |
    Then csticket created
    And headline is 'Short or Zero pick received for an order from kiosk, but was unexpected'
    And subject is 'Short or Zero pick received for an order from kiosk, but was unexpected'
    And body is 'Short or Zero pick received for an order that does not allow them.'
    
    Scenario: Placing an order.Full pick file received from fastline
    
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    And start order with pinpad store 5098
    And pinpad sends payment update with success 'true'
    And finish order
    And business process completes
    When fastline returns pick entry data:
       | product       | qty|
       | P4025_black_S | 2  |
    Then csticket not created
    And fastline pick process completes
    And consignment status is 'PICKED'
	    
    
    Scenario: Placing an order with deals.Full pick file received from fastline

    Given cart entries:
      | product  | qty | price |
      | 10000001 | 2   | 10    |
    And quantity break deal product '10000001'
    And quantity break deal break points:
      | qty | unitPrice |
      | 2   | 8         |
    And delivery mode is 'home-delivery'
    And start order with pinpad store 5098
    And pinpad sends payment update with success 'true'
    And finish order
    And business process completes
    When fastline returns pick entry data:
       | product       | qty|
       | 10000001 | 2  |
    Then csticket not created
    And pick process completes
    And consignment status is 'PICKED'
    

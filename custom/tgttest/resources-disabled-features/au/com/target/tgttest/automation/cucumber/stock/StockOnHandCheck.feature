@cartProductData 
Feature: Stock on hand check

  Scenario: Placing an order with stock on hand reduces but remains more than quantity in cart.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And reserve stock on product '10000011' amount 1
    And check the stock level of product '10000011' is great than 2
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39.0  | 30.0     | 9.0          |          |


@tlog @voucherData
Feature: Cancelling or refunding orders creates tlog refund entries
  In order to integrate with POS, REFUND TLOG entries should be created for cancellations and refunds.

  # delivery mode options are 'home-delivery' or 'click-and-collect'
  # Uses standard test user test.user1@target.com.au
  # Use 'valid' or 'invalid' for valid/invalid tmd numbers
  # Use code='tmd' for expected code in transDiscount
  Scenario: Placing an order with no deals or discounts and partial cancel.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order
    And cancel entries
      | product       | qty |
      | P4025_black_S | 1   |
    Then order total is 24.0
    And tlog entries are:
      | code          | qty | price | filePrice |
      | P4025_black_S | 1   | -1500 | 1500      |
    And tlog type is 'refund'
    And tlog user is 'system'
    And tlog shipping is 000
    And tlog total tender is -1500

  Scenario: Placing an order with no deals or discounts and full cancel.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order
    And the order is fully cancelled
    Then tlog entries are:
      | code          | qty | price | filePrice |
      | P4025_black_S | 1   | -1500 | 1500      |
      | P4025_black_S | 1   | -1500 | 1500      |
    And tlog type is 'refund'
    And tlog user is 'system'
    And tlog shipping is 000
    And tlog total tender is -3000

  Scenario: Placing an order with no deals or discounts and partial refund.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order
    And return entries
      | product       | qty |
      | P4025_black_S | 1   |
    Then order total is 24.0
    And tlog entries are:
      | code          | qty | price | filePrice |
      | P4025_black_S | 1   | -1500 | 1500      |
    And tlog type is 'refund'
    And tlog user is 'system'
    And tlog shipping is 000
    And tlog total tender is -1500

  Scenario: Placing an order with tmd partial cancel.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
    And delivery mode is 'home-delivery'
    When place order with tmd 'valid'
    And cancel entries
      | product       | qty |
      | P4025_black_S | 1   |
    Then order total is 21.75
    And tlog entries are:
      | code          | qty | price | filePrice |
      | P4025_black_S | 1   | -1275 | 1500      |
    And tlog type is 'refund'
    And tlog user is 'system'
    And tlog shipping is 000
    And tlog total tender is -1275

  Scenario: Placing an order with multiple items and tmd, partial cancel.
    Given cart entries:
      | product       | qty | price |
      | P4025_black_S | 2   | 15    |
      | P4025_black_M | 2   | 20    |
    And delivery mode is 'home-delivery'
    When place order with tmd 'valid'
    And cancel entries
      | product       | qty |
      | P4025_black_S | 1   |
      | P4025_black_M | 1   |
    Then order total is 38.75
    And tlog entries are:
      | code          | qty | price | filePrice |
      | P4025_black_S | 1   | -1275 | 1500      |
      | P4025_black_M | 1   | -1700 | 2000      |
    And tlog type is 'refund'
    And tlog user is 'system'
    And tlog shipping is 000
    And tlog total tender is -2975

  Scenario: Placing an order with vbd deal, extra reward product and tmd, partial cancel of one reward.
    Should get the refund on the item not in the deal.

    Given cart entries:
      | product | qty | price |
      | CP4067  | 2   | 50    |
      | CP4066  | 2   | 20    |
    And delivery mode is 'home-delivery'
    When place order with tmd 'valid'
    And cancel entries
      | product | qty |
      | CP4066  | 1   |
    Then order total is 99.0
    And tlog entries are:
      | code   | qty | price | filePrice |
      | CP4066 | 1   | -1800 | 2000      |
    And tlog type is 'refund'
    And tlog user is 'system'
    And tlog shipping is 000
    And tlog total tender is -1800

  Scenario: Placing an order with vbd deal, extra reward product and tmd, partial cancel of both rewards.
    Should get the refund on both items.

    Given cart entries:
      | product | qty | price |
      | CP4067  | 2   | 50    |
      | CP4066  | 2   | 20    |
    And delivery mode is 'home-delivery'
    When place order with tmd 'valid'
    And cancel entries
      | product | qty |
      | CP4066  | 2   |
    Then order total is 90.0
    And tlog entries are:
      | code   | qty | price | filePrice |
      | CP4066 | 1   | -1800 | 2000      |
      | CP4066 | 1   | -900  | 2000      |
    And tlog type is 'refund'
    And tlog user is 'system'
    And tlog shipping is 000
    And tlog total tender is -2700

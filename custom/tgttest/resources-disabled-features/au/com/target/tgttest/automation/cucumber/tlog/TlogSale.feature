@tlog @voucherData
Feature: Placing orders creates tlog sale entries
  In order to integrate with POS, as Target online store we want
  SALE TLOG entries to be created for placed orders.
  
  Uses standard registered test user test.user1@target.com.au
  Use 'valid' or 'invalid' for valid/invalid tmd numbers
  Use code='tmd' for expected code in transDiscount

  # Quantity break deal has code 5002
  Scenario: Placing an order with deal items.
    Given a cart with entries:
      | product  | qty | price |
      | 10000001 | 2   | 10    |
    And quantity break deal product '10000001'
    And quantity break deal break points:
      | qty | unitPrice |
      | 2   | 8         |
    And delivery mode is 'home-delivery'
    When order is placed
    Then order total is 25.0
    And tlog entries are:
      | code     | qty | price | filePrice | itemDealType | itemDealId | itemDealInstance | itemDealMarkdown |
      | 10000001 | 1   | 1000  | 1000      | R            | 5002       | 1                | 200              |
      | 10000001 | 1   | 1000  | 1000      | R            | 5002       | 1                | 200              |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 2500
    And transDeal entries are:
      | type | id   | instance | markdown |
      | 5    | 5002 | 1        | 400      |

  Scenario: Placing an order with vbd deal items.
    Given a cart with entries:
      | product | qty | price |
      | CP4067  | 2   | 50    |
      | CP4066  | 2   | 20    |
    And delivery mode is 'home-delivery'
    When order is placed
    Then order total is 130.0
    And tlog entries are:
      | code   | qty | price | filePrice | itemDealType | itemDealId | itemDealInstance | itemDealMarkdown |
      | CP4067 | 1   | 5000  | 5000      | Q            | 102        | 1                | 0                |
      | CP4067 | 1   | 5000  | 5000      | Q            | 102        | 1                | 0                |
      | CP4066 | 1   | 2000  | 2000      | R            | 102        | 1                | 1000             |
      | CP4066 | 1   | 2000  | 2000      |              |            |                  |                  |
    And tlog type is 'sale'
    And tlog shipping is 0
    And tlog total tender is 13000
    And transDeal entries are:
      | type | id  | instance | markdown |
      | 1    | 102 | 1        | 1000     |

  Scenario: Placing an order with vbd deal and tmd.
    Given a cart with entries:
      | product | qty | price |
      | CP4067  | 2   | 50    |
      | CP4066  | 2   | 20    |
    And delivery mode is 'home-delivery'
    When order is placed with tmd 'valid'
    Then order total is 117.0
    And tlog entries are:
      | code   | qty | price | filePrice | itemDealType | itemDealId | itemDealInstance | itemDealMarkdown | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | CP4067 | 1   | 5000  | 5000      | Q            | 102        | 1                | 0                | staff            | 10              | 500                |
      | CP4067 | 1   | 5000  | 5000      | Q            | 102        | 1                | 0                | staff            | 10              | 500                |
      | CP4066 | 1   | 2000  | 2000      | R            | 102        | 1                | 1000             | staff            | 10              | 100                |
      | CP4066 | 1   | 2000  | 2000      |              |            |                  |                  | staff            | 10              | 200                |
    And tlog type is 'sale'
    And tlog shipping is 0
    And tlog total tender is 11700
    And transDeal entries are:
      | type | id  | instance | markdown |
      | 1    | 102 | 1        | 1000     |
    And transDiscount entries are:
      | type  | code | pct | amount |
      | staff | tmd  | 10  | 1300   |

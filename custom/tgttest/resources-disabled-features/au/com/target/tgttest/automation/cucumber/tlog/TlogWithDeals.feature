@cartProductData 
Feature: Placing orders with deals
  Deals can be defined and applied to products, affecting order totals and tlog.

  Scenario: Placing an order with buy get deal items.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 50          | 1            |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 50    | buyget   | Q        |
      | 10000012 | 2   | 20    | buyget   | R        |
    And delivery mode is 'home-delivery'
    When order is placed
    Then order total is 130.0
    And tlog entries are:
      | code     | qty | price | filePrice | itemDealType | itemDealId | itemDealInstance | itemDealMarkdown |
      | 10000011 | 1   | 5000  | 5000      | Q            | 110        | 1                | 0                |
      | 10000011 | 1   | 5000  | 5000      | Q            | 110        | 1                | 0                |
      | 10000012 | 1   | 2000  | 2000      | R            | 110        | 1                | 1000             |
      | 10000012 | 1   | 2000  | 2000      |              |            |                  |                  |
    And tlog type is 'sale'
    And tlog shipping is 0
    And tlog total tender is 13000
    And transDeal entries are:
      | type | id  | instance | markdown |
      | 1    | 110 | 1        | 1000     |

  Scenario: Placing an order with buy get deal and tmd.
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 50          | 1            |
    And cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 50    | buyget   | Q        |
      | 10000012 | 2   | 20    | buyget   | R        |
    And delivery mode is 'home-delivery'
    When place order with tmd 'valid'
    Then order total is 110.5
    And tlog entries are:
      | code     | qty | price | filePrice | itemDealType | itemDealId | itemDealInstance | itemDealMarkdown | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000011 | 1   | 5000  | 5000      | Q            | 110        | 1                | 0                | staff            | 15              | 750                |
      | 10000011 | 1   | 5000  | 5000      | Q            | 110        | 1                | 0                | staff            | 15              | 750                |
      | 10000012 | 1   | 2000  | 2000      | R            | 110        | 1                | 1000             | staff            | 15              | 150                |
      | 10000012 | 1   | 2000  | 2000      |              |            |                  |                  | staff            | 15              | 300                |
    And tlog type is 'sale'
    And tlog shipping is 0
    And tlog total tender is 11050
    And transDeal entries are:
      | type | id  | instance | markdown |
      | 1    | 110 | 1        | 1000     |
    And transDiscount entries are:
      | type  | code | pct | amount |
      | staff | tmd  | 15  | 1950   |

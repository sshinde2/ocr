@cartProductData @initPreOrderProducts 
Feature: IPG - Accertify feed
  As a target online security team member
  I want the data feed for each transaction sent to Acertify
  So that I check whether there is a fraud risk for each transaction

  Background: 
    Given payment method is 'ipg'
    And feature 'accertify.include.billingAddress' is switched 'on'
    And giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | GC1       | Giftcard   | 62734500000000002 | 01/20      | 123456789 |             | 31  |
      | GC2       | Giftcard   | 62779575000000000 | 01/20      | 123456789 |             | 32  |
      | GC3       | Giftcard   | 62779575000000001 | 01/20      | 123456789 |             | 32  |
      | GC4       | Giftcard   | 62733500000000003 | 01/20      | 123456789 |             | 33  |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |
      | CC2       | MASTERCARD | 5587654321010013  | 02/20      | 123456789 | 01/02/2020  | 36  |
      | CC3       | AMEX       | 349876543210010   | 03/20      | 123456789 | 01/03/2020  | 37  |
      | CC4       | DINERS     | 36765432100028    | 04/20      | 123456789 | 01/04/2020  | 38  |

  Scenario: Accertify Feed successfull payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And the billing address is '14 Emert street,Wentworthville,NSW,2145'
    And customer pays with credit card
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 5236896578548965 | 01/2020    |
    When order is placed
    Then the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 5236896578548965 |                | visa     | 39                   | 2020/01        | 5236896578548965 | 78901234556 |
    And the accertify billing information entries:
      | addressLine1    | addressLine2 | town           | postalCode | region | country |
      | 14 Emert street |              | Wentworthville | 2145       | NSW    | AU      |
    And the failed credit card attempt is 0
    And the order type is sale
    
  Scenario: Accertify Feed successfull payment for Preorder product
    Given a cart with entries:
      | product  | qty | price |
      | V5555_preOrder_5 | 2   | 15    |
    And registered user checkout
    And the billing address is '14 Emert street,Wentworthville,NSW,2145'
    And customer pays with credit card
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 5236896578548965 | 01/2020    |
    When pre order is placed
    Then the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 5236896578548965 |                | visa     | 10                   | 2020/01        | 5236896578548965 | 78901234556 |
    And the accertify billing information entries:
      | addressLine1    | addressLine2 | town           | postalCode | region | country |
      | 14 Emert street |              | Wentworthville | 2145       | NSW    | AU      |
    And the failed credit card attempt is 0
    And the order type is preOrder
    
     
  Scenario: Accertify Feed successfull mixed payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And the billing address is '14 Emert street,Wentworthville,NSW,2145'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1, 10      |
      | GC2, 5.01    |
      | CC1, 20.99   |
      | CC3, 3       |
    When order is placed
    Then the accertify payment entries:
      | PaymentType | cardNumber        | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt       | cardToken |
      | GiftCard    | 62734500000000002 |                | giftcard | 10.00                |                | 62734500000000002 |           |
      | GiftCard    | 62779575000000000 |                | giftcard | 5.01                 |                | 62779575000000000 |           |
      | CreditCard  | 4987654321010012  |                | visa     | 20.99                | 20/01          | 4987654321010012  | 123456789 |
      | CreditCard  | 349876543210010   |                | amex     | 3.00                 | 20/03          | 349876543210010   | 123456789 |
    And the accertify billing information entries:
      | addressLine1    | addressLine2 | town           | postalCode | region | country |
      | 14 Emert street |              | Wentworthville | 2145       | NSW    | AU      |
    And the failed credit card attempt is 0
    And the order type is sale

  Scenario: Accertify Feed for sale with failed credit card transaction
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And the billing address is '14 Emert street,Wentworthville,NSW,2145'
    When order is placed with invalid creditcard
      | cardType | cardNumber       | cardExpiry |
      | MASTER   | 4242424242424242 | 01/2022    |
    And order is placed with valid creditcard
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 5896356978965423 | 01/2020    |
    Then the failed credit card attempt is 1
    And the failed credit card details are
      | cardType | cardNumber       | cardExpiry |
      | master   | 4242424242424242 | 01/2022    |
    And the failed billing information entries:
      | addressLine1    | addressLine2 | town           | postalCode | region | country |
      | 14 Emert street |              | Wentworthville | 2145       | NSW    | AU      |
    And the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 5896356978965423 |                | visa     | 39                   | 2020/01        | 5896356978965423 | 78901234556 |
    And the order type is sale

  Scenario: Accertify Feed for sale with failed gift card transaction
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And there are gift card payment attempts:
      | cardReceipt | cardAuthorizedAmount | isPaymentSuccess |
      | 65824152    | 20.65                | false            |
    When order is placed with valid creditcard
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 5896356978965423 | 01/2020    |
    Then the failed gift card attempt is 1
    And the failed gift card details are
      | cardType | cardNumber | amount |
      | giftcard | 65824152   | 20.65  |
    And the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 5896356978965423 |                | visa     | 39                   | 2020/01        | 5896356978965423 | 78901234556 |
    And the order type is sale
    And the accertify total amount is 39

  Scenario: Accertify Feed for sale with failed credit card transaction
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    When order is placed with invalid creditcard
      | cardType | cardNumber       | cardExpiry |
      | MASTER   | 4242424242424242 | 01/2022    |
    And order is placed with valid creditcard
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 5896356978965423 | 01/2020    |
    Then the failed credit card attempt is 1
    And the failed credit card details are
      | cardType | cardNumber       | cardExpiry |
      | master   | 4242424242424242 | 01/2022    |
    And the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 5896356978965423 |                | visa     | 39                   | 2020/01        | 5896356978965423 | 78901234556 |
    And the order type is sale
    And the accertify total amount is 39

  Scenario: Accertify Feed for refund
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 12    |
    When order is placed with valid creditcard
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 4545454545454545 | 01/2020    |
    And the IPG refund will 'succeed' for the order
    And the order is fully cancelled
    Then the failed credit card attempt is 0
    And the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 4545454545454545 |                | visa     | 33                   | 2020/01        | 4545454545454545 | 78901234556 |
    And the order type is refund
    And the accertify total amount is 33

  Scenario: Accertify Feed for refund with mixed payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 12    |
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1, 10      |
      | CC1, 23      |
    When order is placed
    And refund for gift card 'GC1' is declined
    And the order is fully cancelled
    Then the failed credit card attempt is 0
    Then the failed gift card attempt is 0
    And the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken |
      | CreditCard  | 4987654321010012 |                | visa     | 23                   | 20/01          | 4987654321010012 | 123456789 |
    And the order type is refund
    And the accertify total amount is 33

  Scenario: Accertify Feed for refund on Fraud
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 12    |
    And the order would trigger an Accertify response of 'REJECT'
    When order is placed with valid creditcard
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 4545454545454545 | 01/2020    |
    And the IPG refund will 'succeed' for the order
    Then the failed credit card attempt is 0
    And order is in status 'REJECTED'
    And the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 4545454545454545 |                | visa     | 33                   | 2020/01        | 4545454545454545 | 78901234556 |
    And the order type is refund
    And the accertify total amount is 33

@notAutomated
Feature: IPG - Change the method of selecting credit card on payment page
  In order to change the method of payment through IPG
  	As a target customer
   	I want to select the credit card for payment method

  Background: 
    Given IPG payment feature is on

  Scenario: Display Credit card Radio button on payment page
    Given I have created an order
    When I land on the Payment Page
    Then the Credit Card Radio button will be displayed on payment page

  Scenario: Selecting Credit Card Radio button highlights the Credit Card radio button
    Given the Credit Card radio button has been displayed
    When I click on the Credit Card radio button
    Then the Credit Card payment method will be selected
    And the continue button will read 'Review Order'

  Scenario: Do not display Enter Payment Details when Credit Card payment method is selected
    Given I have landed on the payment details page
    When I select the Credit Card payment method
    Then the Enter Payment Details section will not be displayed

  Scenario: Selecting PayPal Radio button will deselect the Credit Card radio button
    Given I have selected the Credit Card payment method
    When I click on an alternate radio button
    Then the Credit Card payment method will be deselected
    And the new selected payment method will become selected
    And the continue button will read 'Continue to PayPal'

  Scenario: Use Delivery Address as billing address
    Given I have already entered in my Shipping Address on the Delivery Page
    When I select the Credit Card payment method on the Payment Page
    Then my shipping address will displayed as the billing address
    And an Edit link and New link will be displayed

  Scenario: Tapping Edit opens pre-filled Address Fields
    Given I have selected the Credit Card payment method
    When click the Edit button
    Then the Billing Address fields will be displayed
    And pre-filled with the previously display address detail

  Scenario: Tapping New opens a blank Address Fields form
    Given I have selected the Credit Card payment method
    When click the New button
    Then The Billing Address fields will be displayed
    And The Billing Address fields will be empty

  Scenario: When Tapping Edit changes both links to just Use This Address
    Given I have selected the Credit Card payment method
    When I click the Edit
    Then Use This Address will be displayed

  Scenario: When Tapping New changes both links to just Use This Address
    Given I have selected the Credit Card payment method
    When I click the New link
    Then Use This Address will be displayed

  Scenario: Use this address reverts back to the defaulted address
    Given I have clicked New
    When I tap Use This Address link
    Then the billing address fields will be hidden
    And the defaulted billing address will be used

  Scenario: Use this address reverts back to the defaulted address
    Given I have clicked Edit
    When I tap Use This Address link
    Then the billing address fields will be hidden
    And the defaulted billing address will be used

  Scenario: Mandatory validations for billing address
    Given I have not entered data on all mandatory fields for billing address
    When I click continue
    Then I will see the field error state for each field
    And an error message displayed at the top of the page

  Scenario: Selecting PayPal Radio button hides billing address
    Given I have selected the Credit Card payment method
    When I select the PayPal payment method
    Then the billing address entry fields will be hidden
    And the contents will not be retained

  Scenario Outline: Field validations on Billing address
    Given the billing address input is <First Name>,<Last Name>,<Address Line1>,<City/Suburb>,<Postcode>
    When I click on Edit on the billing address on the payment page
    And I click Review Order
    Then I will see <Result>

    Examples: 
      | First Name   | Last Name    | Address Line1      | City/Suburb   | Postcode     | Result                             |
      | K            | 1            | gfgffgdf           | hjfg          | 897980       | Error, remains on the same Page    |
      | 33 charactes | 33 charactes | 41 charaters       | 65 characters | 10 charactes | Error, remains on the same Page    |
      | 32 charactes | 32 charactes | 40 charaters       | 64 characters | 4 characters | Error, remains on the same Page    |
      | abc          | xyzqw        | 12 Thompson street | North Geelong | 3215         | NO Error, proceed to the next Page |

  Scenario Outline: Field validations on Billing address
    Given the billing address input is <First Name>,<Last Name>,<Address Line1>,<City/Suburb>,<Postcode>
    When I click on New on the billing address on the payment page
    And I click Review Order
    Then I will see <Result>

    Examples: 
      | First Name   | Last Name    | Address Line1      | City/Suburb   | Postcode     | Result                             |
      | K            | 1            | gfgffgdf           | hjfg          | 897980       | Error, remains on the same Page    |
      | 33 charactes | 33 charactes | 41 charaters       | 65 characters | 10 charactes | Error, remains on the same Page    |
      | 32 charactes | 32 charactes | 40 charaters       | 64 characters | 4 characters | Error, remains on the same Page    |
      | abc          | xyzqw        | 12 Thompson street | North Geelong | 3215         | NO Error, proceed to the next Page |

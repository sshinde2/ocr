@notAutomated
Feature: IPG - Display Giftcard/Multiple Card Iframe in CSCockpit
  
  As a Target Customer Service Agent
  I want to be able to complete a gift card or Multiple Card payment in CS Cockpit
  so that I can place an order using Multiple Card/Gift Card for customer

  Background: 
    Given IPG payment feature is on
    And IPG payment gift card feature is on

  Scenario Outline: Multi Card IFrame in CS Cockpit
    Given a cart with entries
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And CS agent proceeds with checkout
    And the delivery mode is <deliveryMode>
    When the CS agent choose to add new multi card payment
    Then the multi card iframe is rendered

    Examples: 
      | deliveryMode    |
      | HomeDelivery    |
      | ClickandCollect |
      | ExpressDelivery |

  Scenario: Display Payment buttons in CS Cockpit
    Given a cart with entries
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When CS agent proceeds with checkout
    Then the 'Add Credit Card Details','Add Multi Card details' payment buttons are displayed under payment

  Scenario: Place order in CS Cockpit using gift card
    Given any cart with entries
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When the cs agent select to pay with gift card
      | cardNumber        | AccessPin | Amount |
      | 62734500000000002 | 1234      | 39.00  |
    Then order is created:
      | total | subtotal | deliveryCost | 
      | 39    | 30       | 9.0          |
    And order is in status 'INPROGRESS'
    And the payment details of the transaction will be recorded correctly

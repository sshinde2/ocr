@cartProductData
Feature: IPG - Card Management (Delete/ expiry)
  As an online customer
  I want any changes that I make to my credit cards in IPG to change and update in My Credit Cards
  So that I do not have to make changes twice in both My Credit Cards and IPG

  Scenario: Deleting a card deletes it from My Account
    Given customer has saved ipg credit cards
      | cardType | cardNumber      | cardExpiry | token     | tokenExpiry | defaultCard | cardOnFile | firstCredentialStorage |
      | VISA     | 425242*****4242 | 01/16      | 123456789 | 01/01/2017  | true        | true       | true                   |
      | AMEX     | 349876*****0044 | 01/17      | 123456780 | 01/01/2020  | false       | false      | false                  |
      | AMEX     | 349876*****0041 | 01/18      | 123456780 | 01/01/2020  | false       | false      | false                  |
    And any cart
    And registered user checkout
    And payment method is 'ipg'
    And ipg returns saved cards
      | cardType | cardNumber      | cardExpiry | token     | tokenExpiry | bin | defaultCard | cardOnFile | firstCredentialStorage |
      | VISA     | 424242*****4242 | 01/18      | 123456789 | 01/01/2018  | 34  | true        | false      | false                  |
      | AMEX     | 349876*****0044 | 01/16      | 123456780 | 01/01/2020  | 34  | false       | true       | true                   |
    When order is placed
    Then customer will have saved ipg credit cards
      | cardType | cardNumber      | cardExpiry | token     | tokenExpiry | defaultCard | cardOnFile | firstCredentialStorage |
      | VISA     | 424242*****4242 | 01/18      | 123456789 | 01/01/2018  | true        | false      | false                  |
      | AMEX     | 349876*****0044 | 01/16      | 123456780 | 01/01/2020  | false       | true       | true                   |

  @notAutomated
  Scenario: Remove expired card from My Account
    Given one of the customers saved cards is expired
    When the customer uses the iFrame to submit a payment
    Then the expired card will be removed from My Account

  Scenario: If Primary card is deleted then automatically assign remaining saved card as primary
    Given customer has saved ipg credit cards
      | cardType   | cardNumber      | cardExpiry | token     | tokenExpiry | defaultCard | cardOnFile | firstCredentialStorage |
      | VISA       | 425242*****4242 | 01/16      | 123456789 | 01/01/2017  | true        | true       | true                   |
      | MASTERCARD | 349876*****0044 | 01/17      | 123456780 | 01/01/2020  | false       | true       | true                   |
      | AMEX       | 349876*****0041 | 01/18      | 123456780 | 01/01/2020  | false       | true       | true                   |
    When the customer deletes the primary card
    Then customer will have saved ipg credit cards
      | cardType   | cardNumber      | cardExpiry | token     | tokenExpiry | defaultCard |cardOnFile | firstCredentialStorage |
      | MASTERCARD | 349876*****0044 | 01/17      | 123456780 | 01/01/2020  | true        |true       | true                   |
      | AMEX       | 349876*****0041 | 01/18      | 123456780 | 01/01/2020  | false       |true       | true                   |

  Scenario: If Primary card is deleted and there are no other saved cards the do not reassign Primary Card
    Given customer has saved ipg credit cards
      | cardType | cardNumber      | cardExpiry | token     | tokenExpiry | defaultCard | cardOnFile | firstCredentialStorage |
      | VISA     | 425242*****4242 | 01/16      | 123456789 | 01/01/2017  | true        | true       | true                   |
    When the customer deletes the primary card
    Then customer will have no saved cards

  @notAutomated
  Scenario: Remove Primary Card label
    Given I have at least 1 saved card
    When I land on the My Credit Card page (My Account)
    Then the primary card label will not be displayed with the saved credit card details

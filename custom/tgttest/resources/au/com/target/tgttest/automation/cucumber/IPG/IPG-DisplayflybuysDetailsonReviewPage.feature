@notAutomated
Feature: IPG - Display the ablility to view flybuys Details on the review page
  In order to provide better customer experience
  As target online customer
  I want to see the flybuys Details on the review page

  Background: 
  Given IPG is turned on

  Scenario: Do not display flybuys Details section on Review Page if flybuys member number is not available for new user
     Given User is on the payment page
     And user did not enter the flybuys Details
     When requesting the Review Page
     Then Do not display the flybuys Details section

   Scenario: Display flybuys Details section on Review Page for existing user
     Given User with saved flybuys details
     When requesting the Payment Page
     Then Display the flybuys details

   Scenario: Display flybuys Details section on Review Page if flybuys number is available
     Given User is on the payment page
     And flybuys Details is entered
     When requesting the Review Page
     Then Display the flybuys Details section
     And User should be able to change the flybuys details 

   Scenario: Change flybuys Details
     Given User is on Review page
     And flybuys Details is displayed
     When User request to change flybuys Details section
     Then Navigates back to payment page

    
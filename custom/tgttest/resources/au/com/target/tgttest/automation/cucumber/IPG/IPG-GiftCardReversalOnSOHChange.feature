@notAutomated
Feature: IPG - Gift Cards - Reversal
  As an online customer
  I want funds returned to my gift card if there was no stock left
  So that I can use my gift card for an alternate purchase

  Scenario: Perform reversal on partial out of stock and remain on Review Page with new iFrame displayed
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And customer submitted a payment to IPG with a Gift Card
    When the fastline stock changes
      | product  | reserved | available |
      | 10000011 | 0        | 1         |
    Then the customer will remain on the Review page
    And the out of stock items will be removed
    And a new iFrame will be loaded with the latest amount
    And any gift card charges will be reversed
    And the no SOH message will be displayed

  Scenario: Perform reversal on all products out of stock and remain on Review Page with new iFrame displayed
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And customer submitted a payment to IPG with a Gift Card
    When the fastline stock changes
      | product  | reserved | available |
      | 10000011 | 0        | 0         |
    Then the customer will land on the basket page
    And a stock is unavailable message will be displayed
    And any gift card charges will be reversed

@notAutomated
Feature: IPG - Change the method of selecting gift card on payment page
    As an online customer
    I want to be able to select the Gift Card payment method
    So that I can proceed to pay for my order using a gift card

  Background: 
    Given IPG payment feature is on
    And IPG payment gift card feature is on

  Scenario: Display gift card payment method on Payment Page
    Given the customer is proceeding through the checkout
    When the customer lands on the Payment Page
    Then the Gift Card payment method will be displayed

  Scenario: Tapping gift card payment method selects gift card payment method and displays the billing address fields and enables the "Review Order" button
    Given the customer has landed on the Payment Page
    When the customer taps the gift card payment method
    Then the payment method will become selected
    And the Gift Card message "Use your Target or Coles Group & Myer Gift Cards. Pay any balance with a credit card." will be displayed
    And the Billing Address will be displayed
    And the "Review Order" button will be enabled

  Scenario: Tapping on Review Order navigates to the Review page
    Given the customer has selected the Gift Card payment method
    And the customer has entered a Billing Address
    When the customer taps the Review Order button
    Then the customer will navigate to the Review Page

  Scenario: Use Delivery Address as billing address
    Given the customer has already entered in my Shipping Address on the Delivery Page
    When the customer selects the Gift Card payment method on the Payment Page
    Then the customer's shipping address will displayed as the billing address
    And an Edit link and New link will be displayed

  Scenario: Tapping Edit opens pre-filled Address Fields
    Given the customer has selected the Gift Card payment method
    When the customer clicks/taps the Edit button
    Then the Billing Address fields will be displayed
    And pre-filled with the previously display address details

  Scenario: Tapping New opens a blank Address Fields form
    Given the customer has selected the Gift Card payment method
    When the customer clicks/taps the New button
    Then The Billing Address fields will be displayed
    And the Billing Address Fields will be empty

  Scenario: Tapping Edit or New link changes both links to just Use This Address
    Given the customer has selected the Gift Card payment method
    When the customer taps/clicks the Edit or New link
    Then Use This Address will be displayed in it's place

  Scenario: Use this address reverts back to the defaulted address
    Given the customer has clicked Edit or New
    When the customer taps Use This Address link
    Then the billing address fields will be hidden
    And the defaulted billing address will be used

  Scenario: Mandatory validations for billing address
    Given the customer has not entered a mandatory field for billing address
    When the customer clicks continue
    Then the customer will see the field error state
    And an error message displayed at the top of the page

  Scenario: Selecting another Payment Method hides the Gift Card billing address
    Given the customer has selected the Gift Card payment method
    When the customer selects another payment method
    Then the billing address entry fields will be hidden
    And the contents will not be retained

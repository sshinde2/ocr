@notAutomated
Feature: IPG - Pay with IPG in mobile app
  As an online customer
  I want to pay for items through the mobile app
  So that I can use the mobile application to purchase a cart

  Background: 
    Given IPG payment feature is on

  Scenario: IPG iFrame is displayed for payment in mobile app
    Given customer proceeding through the checkout flow on the Mobile App
    And customer selects the credit card payment method
    When customer proceeds to review step
    Then the iFrame will be displayed in the review page

  Scenario: Submit payment details from iFrame to complete payment
    Given IPG iFrame is loaded
    When customer enters payment details
    And customer chooses to continue with payment from iFrame
    Then payment is submitted to IPG
    And order is created successfully

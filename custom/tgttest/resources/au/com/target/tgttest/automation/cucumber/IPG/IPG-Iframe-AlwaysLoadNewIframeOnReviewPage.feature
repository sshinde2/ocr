@notAutomated
Feature: IPG - Always Load New iFrame on Review Page
  
  As a Target customer
  I want to have the iFrame load on the review page
  So that if I refresh the page, navigate away from the review page, or my browser crashes I can still finish checking out

  Scenario: Load iFrame when landing on the review page
    Given the customer has clicked the Review Order button
    When the customer lands on the review page
    Then the iFrame will be displayed

  Scenario: If the review page is refreshed a new iFrame will be displayed
    Given the customer has landed on the review page
    When the customer clicks the refresh button or F5
    Then a new iFrame will be displayed

  Scenario: If the customer clicks the browser back button and then clicks the browser forward button a new iFrame will be displayed
    Given the customer has navigated back from the review page
    When the customer navigates forward to the review page
    Then a new iFrame will be displayed

  Scenario: If customer navigates to elsewhere in the Target website then click browser back button to the review page then a new iFrame will be displayed
    Given the customer has navigated elsewhere in the Target website
    When the customer click browser back button to the review page
    Then a new iFrame will be displayed

  Scenario: If taking payment is unsuccessful customer remains on the review page
    Given the customer has submitted their payment through ipg iframe on the review page
    When the payment is not successful
    Then the failed payment detail will be attached to the cart
    And the customer will remain on the review page
    And the a new iFrame will be displayed
    And the error message "We could not take payment using the card details provided. Please check your details and try again." will be displayed at the top of the page

  Scenario: Multiple Review Pages
    Given the customer opens review pages on multiple tabs
    When the customer clicks pay now through the iframe on previous pages (not the latest opened one)
    Then the customer will remain on the review page
    And the error message "We could not take payment using the card details provided. Please check your details and try again." will be displayed at the top of the page

  Scenario: New DL iFrame for Pre-order
	Given the Pre-order item in the Basket
	When customer is in the payment page
	And customer clicks credit card payment
	Then a new DL iFrame will be displayed
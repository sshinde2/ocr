@notAutomated
Feature: IPG - Display the split payment iFrame on the Review page
  As an online customer
  I want the Split Payment iFrame displayed on the Payment page when I select the Gift card payment details
  So that I know how much I have to pay in total

  Scenario Outline: If Gift Card payment method is selected display split payment iFrame on review page
    Given the customer has selected the Gift Card payment method
    And the customer has clicked the review order button
    When the customer lands on the Review page
    Then the Split Payment iFrame will be displayed
    And the Amount will be displayed in the iFrame
    And the Amount to Pay will be empty

@notAutomated
Feature: IPG - Display Tender Types on Tax Invoice for IPG
  
  As a Target customer
  I want to see all of the tender types that I used to pay with on the Tax invoice
  So that I can keep record of how I paid for my order

  Scenario: Display Payment Method on Tax Invoice as provided by IPG
    Given the customer has successfully paid for an online basket
    When the Tax Invoice is generated
    Then it will have the Payment Method displayed on it
    And it will have the Account ID(Masked Card information) displayed on it
    And it will have the Receipt Number displayed on it
    And it will have the Pay Date displayed on it


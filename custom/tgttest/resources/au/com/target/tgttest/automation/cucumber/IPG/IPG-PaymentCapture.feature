@cartProductData
Feature: IPG - Make single credit card payment
  In order to purchase products
  As an online customer
  I want to use my credit card to make payment on both single card and split payments

  @notAutomated
  Scenario: Populating relevant fields for single card capture
    Given IPG payment feature is on
    And the customer have landed on the review page
    When the IPG iFrame is rendered
    Then the single credit card capture fields 'CardNumber','CardExpiry','CVV' should be available for the user

  @notAutomated
  Scenario: Valid card details are provided for payment
    Given IPG payment feature is on
    And the IPG iFrame is rendered
    And customer creditcard details:
      | CardType   | CardNumber       | CardExpiry | CVV  |
      | MasterCard | 5587654321010013 | 12/2020    | 123  |
      | AMEX       | 349876543210010  | 12/2017    | 1234 |
      | Visa       | 4123456789010335 | 12/2017    | 123  |
    When the customer submits card details
    Then the card details will be sent to IPG for payment

  @notAutomated
  Scenario Outline: Validation for credit card details
    Given IPG payment feature is on
    And customer requested the review page
    And the IPG iFrame is rendered
    When user submit the form
    And <PossibleAction>
    Then <MessageDispalyed> message should be displayed

    Examples: 
      | PossibleAction                                                                  | MessageDispalyed                                                                                                               |
      | Credit card number did not pass the check digit algorithm                       | You must enter a valid credit card number                                                                                      |
      | Credit card number is blank                                                     | You must enter a valid credit card number                                                                                      |
      | Credit card number is Alphanumeric                                              | You must enter a valid credit card number                                                                                      |
      | Card in the exclusion list,cardnumber that are not allowed                      | Card number not permitted - Excluded card                                                                                      |
      | Credit card BIN not in CPAT list                                                | Card number not recognised                                                                                                     |
      | Both month and years are not selected                                           | Expiry Year requires a value                                                                                                   |
      | Only month selected                                                             | Expiry Year requires a value                                                                                                   |
      | Only year selected                                                              | Expiry Month requires a value                                                                                                  |
      | The date is in past                                                             | The expiry date you have entered appears invalid                                                                               |
      | CVV is blank                                                                    | CVV number requires a value                                                                                                    |
      | Wrong digits entered for Visa, MasterCard, Diners                               | The CVV must be 3 digits                                                                                                       |
      | Wrong digits entered for Amex                                                   | The CVV must be 4 digits                                                                                                       |
      | CVV is Alphanumeric                                                             | The CVV must be numeric                                                                                                        |
      | User selected to change primary card                                            | Are you sure that you want to make this your primary card?                                                                     |
      | User selected to delete default card/non default card                           | Are you sure you want to remove this card?                                                                                     |
      | User selected OK to delete primary card                                         | Please mark another card as primary or delete all other cards first before attempting again.                                   |
      | Saved card limit reached,trying to save fifth card                              | Saved card limit reached.Please delete an existing card or un-check the Save Card box prior to submitting a newly entered card |
      | Split Payment (Not 4th payment) Amount is blank                                 | You must enter a valid amount                                                                                                  |
      | Split Payment (Not 4th payment) Amount with 3 numbers after decimal point       | You must enter a valid amount - with two numbers after the decimal point                                                       |
      | Split Payment (Not 4th payment) Amount is 0.00                                  | Transaction declined.                                                                                                          |
      | Split Payment (Not 4th payment) Amount entered contains $                       | Please enter a valid amount                                                                                                    |
      | Split Payment (Not 4th payment) Amount entered is more than total amount to pay | Amount entered is more than the total owing                                                                                    |

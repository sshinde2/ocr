@cartProductData  @deliveryModeData @initPreOrderProducts
Feature: IPG - Refund for short pick/zero pick, cancel/partial cancel, refund after order complete, manual refund
  In order to have better shopping experience
  As an Online Store
  I want to refund the amount paid by credit card or gift card as quickly as possible if the customer order is valid for refund
  And raise a CS ticket for giftcard refund or any credit card refund failure

  Background: 
    Given payment method is 'ipg'
    And set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 25          | 2            |
    And giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | GC1       | Giftcard   | 62734500000000002 | 01/20      | 123456789 |             | 31  |
      | GC2       | Giftcard   | 62779575000000000 | 01/20      | 123456789 |             | 32  |
      | GC3       | Giftcard   | 62779575000000001 | 01/20      | 123456789 |             | 32  |
      | GC4       | Giftcard   | 62733500000000003 | 01/20      | 123456789 |             | 33  |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |
      | CC2       | MASTERCARD | 5587654321010013  | 02/20      | 123456789 | 01/02/2020  | 36  |
      | CC3       | AMEX       | 349876543210010   | 03/20      | 123456789 | 01/03/2020  | 37  |
      | CC4       | DINERS     | 36765432100028    | 04/20      | 123456789 | 01/04/2020  | 38  |

  Scenario: Refund for fastline short pick successfully
    Given a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000012 | 2   | 15    |
    And the IPG refund will 'succeed' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000012 | 1   |
    Then the IPG refund has been trigerred for the order with amount '15'

  Scenario: Refund for fastline short pick fully failed with CS ticket raised
    Given a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And the IPG refund will 'fail' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 1   |
    Then a CS ticket is created for the order

  Scenario: Refund for fastline short pick with deals successfully
    Given cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 25    | buyget   | Q        |
      | 10000012 | 2   | 25    | buyget   | R        |
    And the IPG refund will 'succeed' for the order
    And order is placed
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 2   |
      | 10000012 | 1   |
    Then the IPG refund has been trigerred for the order with amount '18.75'
    And a CS ticket is not created for the order

  Scenario: Refund for fastline short pick with deals fully failed
    Given cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 25    | buyget   | Q        |
      | 10000012 | 2   | 25    | buyget   | R        |
    And the IPG refund will 'fail' for the order
    And order is placed
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 2   |
      | 10000012 | 1   |
    Then a CS ticket is created for the order
    And the CS ticket group is refundfailures-csagentgroup

  Scenario: Refund for fastline Zero pick failed with CS ticket raised
    Given a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And the IPG refund will 'fail' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 0   |
    Then a CS ticket is created for the order
    And the CS ticket group is refundfailures-csagentgroup

  Scenario Outline: Refund for fastline Zero pick -- full success, full failure, partial success
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And refund for credit card 'CC2' is declined
    And refund for gift card 'GC2' is declined
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 0   |
    Then order will have successful refund:
      | PaymentEntry |
      | <refund1>    |
      | <refund2>    |
      | <refund3>    |
    And a CS ticket is <ticket_status> for the order
    And the order status is CANCELLED

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | refund1 | refund2 | refund3 | refund4 | ticket_status | ticketGroup                 |
      | CC1,35   |          |          |          | CC1,35  |         |         |         | not created   | N/A                         |
      | GC1,35   |          |          |          | GC1,35  |         |         |         | not created   | N/A                         |
      | GC1,20   | CC1,15   |          |          | GC1,20  | CC1,15  |         |         | not created   | N/A                         |
      | GC2,10   | CC2,25   |          |          |         |         |         |         | created       | refundfailures-csagentgroup |
      | GC2,35   |          |          |          |         |         |         |         | created       | refundfailures-csagentgroup |
      | CC2,35   |          |          |          |         |         |         |         | created       | refundfailures-csagentgroup |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | GC1,10  | GC3,10  | GC4,5   |         | created       | refundfailures-csagentgroup |
      | GC2,10   | GC3,10   | GC4,15   |          | GC3,10  | GC4,15  |         |         | created       | refundfailures-csagentgroup |
      | GC2,10   | CC1,10   | CC2,15   |          | CC1,10  |         |         |         | created       | refundfailures-csagentgroup |

  Scenario Outline: Refund for fastline Short pick -- full success, full failure, partial success
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And refund for credit card 'CC2' is declined
    And refund for gift card 'GC2' is declined
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 1   |
    Then order will have successful refund:
      | PaymentEntry |
      | <refund1>    |
      | <refund2>    |
      | <refund3>    |
    And a CS ticket is <ticket_status> for the order
    And the CS ticket group is <ticketGroup>
    And the order status is INVOICED

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | refund1 | refund2 | refund3 | refund4 | ticket_status | ticketGroup                 |
      | CC1,35   |          |          |          | CC1,15  |         |         |         | not created   | N/A                         |
      | GC1,20   | CC1,15   |          |          | CC1,15  |         |         |         | not created   | N/A                         |
      | GC1,35   |          |          |          |         |         |         |         | created       | refundfailures-csagentgroup |
      | GC2,10   | CC1,10   | CC2,15   |          |         |         |         |         | created       | refundfailures-csagentgroup |
      | GC2,10   | CC2,25   |          |          |         |         |         |         | created       | refundfailures-csagentgroup |
      | GC2,35   |          |          |          |         |         |         |         | created       | refundfailures-csagentgroup |
      | CC2,35   |          |          |          |         |         |         |         | created       | refundfailures-csagentgroup |
      | GC2,5    | CC1,10   | CC2,16   | GC1,4    |         |         |         |         | created       | refundfailures-csagentgroup |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | GC1,10  |         |         |         | created       | refundfailures-csagentgroup |

  Scenario Outline: Refund for full Cancel from CS -- full success, full failure, partial success
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And refund for credit card 'CC2' is declined
    And refund for gift card 'GC2' is declined
    When the order is fully cancelled
    Then order will have successful refund:
      | PaymentEntry |
      | <refund1>    |
      | <refund2>    |
      | <refund3>    |
    And a CS ticket is <ticket_status> for the order
    And the order status is <order_status>

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | refund1 | refund2 | refund3 | refund4 | ticket_status | order_status |
      | CC1,35   |          |          |          | CC1,35  |         |         |         | not created   | CANCELLED    |
      | GC1,35   |          |          |          | GC1,35  |         |         |         | not created   | CANCELLED    |
      | GC1,20   | CC1,15   |          |          | GC1,20  | CC1,15  |         |         | not created   | CANCELLED    |
      | GC2,10   | CC2,25   |          |          |         |         |         |         | not created   | INPROGRESS   |
      | GC2,35   |          |          |          |         |         |         |         | not created   | INPROGRESS   |
      | CC2,35   |          |          |          |         |         |         |         | not created   | INPROGRESS   |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | GC1,10  | GC3,10  | GC4,5   |         | created       | CANCELLED    |
      | GC2,10   | GC3,10   | GC4,15   |          | GC3,10  | GC4,15  |         |         | created       | CANCELLED    |
      | GC2,10   | CC1,10   | CC2,15   |          | CC1,10  |         |         |         | created       | CANCELLED    |

  Scenario Outline: Refund for partial Cancel from CS -- full success, full failure, partial success
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And refund for credit card 'CC2' is declined
    And refund for gift card 'GC2' is declined
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then order will have successful refund:
      | PaymentEntry |
      | <refund1>    |
      | <refund2>    |
      | <refund3>    |
    And a CS ticket is <ticket_status> for the order
    And the order status is <order_status>

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | refund1 | refund2 | refund3 | refund4 | ticket_status | order_status |
      | CC1,35   |          |          |          | CC1,15  |         |         |         | not created   | INPROGRESS   |
      | GC1,20   | CC1,15   |          |          | CC1,15  |         |         |         | not created   | INPROGRESS   |
      | GC1,35   |          |          |          |         |         |         |         | not created   | INPROGRESS   |
      | GC2,10   | CC1,10   | CC2,15   |          |         |         |         |         | not created   | INPROGRESS   |
      | GC2,10   | CC2,25   |          |          |         |         |         |         | not created   | INPROGRESS   |
      | GC2,35   |          |          |          |         |         |         |         | not created   | INPROGRESS   |
      | CC2,35   |          |          |          |         |         |         |         | not created   | INPROGRESS   |
      | GC2,5    | CC1,10   | CC2,16   | GC1,4    |         |         |         |         | not created   | INPROGRESS   |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | GC1,10  |         |         |         | created       | INPROGRESS   |

  Scenario Outline: Refund all products from CS after order completed -- full success, full failure, partial success
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And refund for credit card 'CC2' is declined
    And refund for gift card 'GC2' is declined
    And order has been completed
    When all products with delivery fee amount 5 are refunded
    Then order will have successful refund:
      | PaymentEntry |
      | <refund1>    |
      | <refund2>    |
      | <refund3>    |
    And a CS ticket is <ticket_status> for the order
    And the order status is <order_status>

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | refund1 | refund2 | refund3 | refund4 | ticket_status | order_status |
      | CC1,35   |          |          |          | CC1,35  |         |         |         | not created   | COMPLETED    |
      | GC1,35   |          |          |          | GC1,35  |         |         |         | not created   | COMPLETED    |
      | GC1,20   | CC1,15   |          |          | GC1,20  | CC1,15  |         |         | not created   | COMPLETED    |
      | GC2,10   | CC2,25   |          |          |         |         |         |         | not created   | COMPLETED    |
      | GC2,35   |          |          |          |         |         |         |         | not created   | COMPLETED    |
      | CC2,35   |          |          |          |         |         |         |         | not created   | COMPLETED    |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | GC1,10  | GC3,10  | GC4,5   |         | created       | COMPLETED    |
      | GC2,10   | GC3,10   | GC4,15   |          | GC3,10  | GC4,15  |         |         | created       | COMPLETED    |
      | GC2,10   | CC1,10   | CC2,15   |          | CC1,10  |         |         |         | created       | COMPLETED    |

  Scenario Outline: Refund partial products from CS after order completed -- full success, full failure, partial success
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And refund for credit card 'CC2' is declined
    And refund for gift card 'GC2' is declined
    And order has been completed
    When return entries
      | product  | qty |
      | 10000011 | 1   |
    Then order will have successful refund:
      | PaymentEntry |
      | <refund1>    |
      | <refund2>    |
      | <refund3>    |
    And a CS ticket is <ticket_status> for the order
    And the order status is <order_status>

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | refund1 | refund2 | refund3 | refund4 | ticket_status | order_status |
      | CC1,35   |          |          |          | CC1,15  |         |         |         | not created   | COMPLETED    |
      | GC1,20   | CC1,15   |          |          | CC1,15  |         |         |         | not created   | COMPLETED    |
      | GC1,35   |          |          |          |         |         |         |         | not created   | COMPLETED    |
      | GC2,10   | CC1,10   | CC2,15   |          |         |         |         |         | not created   | COMPLETED    |
      | GC2,10   | CC2,25   |          |          |         |         |         |         | not created   | COMPLETED    |
      | GC2,35   |          |          |          |         |         |         |         | not created   | COMPLETED    |
      | CC2,35   |          |          |          |         |         |         |         | not created   | COMPLETED    |
      | GC2,5    | CC1,10   | CC2,16   | GC1,4    |         |         |         |         | not created   | COMPLETED    |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | GC1,10  |         |         |         | created       | COMPLETED    |

  Scenario Outline: Manual Refund for partial Cancel from CS
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And a manual refund informaion is <manual_refund>
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then order will have successful manual refund <manual_refund_entry>
    And the order status is <order_status>
    And tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -1500
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 1   | -1500 | 1500      |
    And tlog tender entries are:
      | cardNumber | approved | amount | type |
      | null       | true     | -15    | cash |

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | manual_refund | manual_refund_entry | order_status |
      | CC1,35   |          |          |          | receipt1,15   | receipt1,15         | INPROGRESS   |
      | GC1,20   | CC1,15   |          |          | receipt1,15   | receipt1,15         | INPROGRESS   |
      | GC1,35   |          |          |          | receipt1,15   | receipt1,15         | INPROGRESS   |
      | GC2,10   | CC1,10   | CC2,15   |          | receipt1,15   | receipt1,15         | INPROGRESS   |
      | GC2,10   | CC2,25   |          |          | receipt1,15   | receipt1,15         | INPROGRESS   |
      | GC2,35   |          |          |          | receipt1,15   | receipt1,15         | INPROGRESS   |
      | CC2,35   |          |          |          | receipt1,15   | receipt1,15         | INPROGRESS   |
      | GC2,5    | CC1,10   | CC2,16   | GC1,4    | receipt1,15   | receipt1,15         | INPROGRESS   |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | receipt1,15   | receipt1,15         | INPROGRESS   |

  Scenario Outline: Manual Refund for full Cancel from CS
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And a manual refund informaion is <manual_refund>
    When the order is fully cancelled
    Then order will have successful manual refund <manual_refund_entry>
    And the order status is <order_status>
    And tlog type is 'refund'
    And tlog shipping is -500
    And tlog total tender is -3500
    And tlog tender entries are:
      | cardNumber | approved | amount | type |
      | null       | true     | -35    | cash |

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | manual_refund | manual_refund_entry | order_status |
      | CC1,35   |          |          |          | receipt1,35   | receipt1,35         | CANCELLED    |
      | GC1,20   | CC1,15   |          |          | receipt1,35   | receipt1,35         | CANCELLED    |
      | GC1,35   |          |          |          | receipt1,35   | receipt1,35         | CANCELLED    |
      | GC2,10   | CC1,10   | CC2,15   |          | receipt1,35   | receipt1,35         | CANCELLED    |
      | GC2,10   | CC2,25   |          |          | receipt1,35   | receipt1,35         | CANCELLED    |
      | GC2,35   |          |          |          | receipt1,35   | receipt1,35         | CANCELLED    |
      | CC2,35   |          |          |          | receipt1,35   | receipt1,35         | CANCELLED    |
      | GC2,5    | CC1,10   | CC2,16   | GC1,4    | receipt1,35   | receipt1,35         | CANCELLED    |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | receipt1,35   | receipt1,35         | CANCELLED    |

  Scenario Outline: Manual Refund for partial refund after order complete from CS
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order has been completed
    And a manual refund informaion is <manual_refund>
    When return entries
      | product  | qty |
      | 10000011 | 1   |
    Then order will have successful manual refund <manual_refund_entry>
    And the order status is <order_status>
    And tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -1500
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 1   | -1500 | 1500      |
    And tlog tender entries are:
      | cardNumber | approved | amount | type |
      | null       | true     | -15    | cash |

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | manual_refund | manual_refund_entry | order_status |
      | CC1,35   |          |          |          | receipt1,15   | receipt1,15         | COMPLETED    |
      | GC1,20   | CC1,15   |          |          | receipt1,15   | receipt1,15         | COMPLETED    |
      | GC1,35   |          |          |          | receipt1,15   | receipt1,15         | COMPLETED    |
      | GC2,10   | CC1,10   | CC2,15   |          | receipt1,15   | receipt1,15         | COMPLETED    |
      | GC2,10   | CC2,25   |          |          | receipt1,15   | receipt1,15         | COMPLETED    |
      | GC2,35   |          |          |          | receipt1,15   | receipt1,15         | COMPLETED    |
      | CC2,35   |          |          |          | receipt1,15   | receipt1,15         | COMPLETED    |
      | GC2,5    | CC1,10   | CC2,16   | GC1,4    | receipt1,15   | receipt1,15         | COMPLETED    |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | receipt1,15   | receipt1,15         | COMPLETED    |

  Scenario Outline: Manual Refund for full refund after order complete from CS
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order has been completed
    And a manual refund informaion is <manual_refund>
    When all products with delivery fee amount 5 are refunded
    Then order will have successful manual refund <manual_refund_entry>
    And the order status is <order_status>
    And tlog type is 'refund'
    And tlog shipping is -500
    And tlog total tender is -3500
    And tlog tender entries are:
      | cardNumber | approved | amount | type |
      | null       | true     | -35    | cash |

    Examples: 
      | payment1 | payment2 | payment3 | payment4 | manual_refund | manual_refund_entry | order_status |
      | CC1,35   |          |          |          | receipt1,35   | receipt1,35         | COMPLETED    |
      | GC1,20   | CC1,15   |          |          | receipt1,35   | receipt1,35         | COMPLETED    |
      | GC1,35   |          |          |          | receipt1,35   | receipt1,35         | COMPLETED    |
      | GC2,10   | CC1,10   | CC2,15   |          | receipt1,35   | receipt1,35         | COMPLETED    |
      | GC2,10   | CC2,25   |          |          | receipt1,35   | receipt1,35         | COMPLETED    |
      | GC2,35   |          |          |          | receipt1,35   | receipt1,35         | COMPLETED    |
      | CC2,35   |          |          |          | receipt1,35   | receipt1,35         | COMPLETED    |
      | GC2,5    | CC1,10   | CC2,16   | GC1,4    | receipt1,35   | receipt1,35         | COMPLETED    |
      | GC1,10   | GC2,10   | GC3,10   | GC4,5    | receipt1,35   | receipt1,35         | COMPLETED    |
      
   Scenario: Refund for Pre-order rejected of fraud
    Given set fastline stock:
    | product          | preOrder | maxPreOrder | reserved | available |
    | V1111_preOrder_1 | 2        | 100         |  10      |    50     | 
    And a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 2   | 300   |
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
        | PaymentEntry |
        | CC1,10   |
    And the order would trigger an Accertify response of 'REJECT'
    And the IPG refund will 'succeed' for the order
    When pre order is placed
    Then order is in status 'REJECTED'
    And the IPG refund has been trigerred for the order with amount '10'
    And fastline stock is:
      | product          | preOrder | maxPreOrder|reserved | available |
      | V1111_preOrder_1 | 2        | 100        | 10      | 50        |

  Scenario: CS ticket raised for Pre-order when refund failed
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 | 1   | 300   |
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
        | PaymentEntry |
        | CC1,10   | 
    And the order would trigger an Accertify response of 'REJECT'
    And the IPG refund will 'fail' for the order
    When pre order is placed
    Then a CS ticket is created for the order    

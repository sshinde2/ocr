@notAutomated
Feature: IPG - Pay with saved credit cards
  As a registered online customer
  I want to be able to pay with saved credit cards
  So that I do not have to spend additional time entering my credit card details again

  Background: 
    Given IPG payment feature is on

  Scenario Outline: Customer is able to select saved credit card for payment
    Given customer is a registered customer
    And customer has a saved card with details of card type <cardType>, masked card number <maskedCardNumber> and card expiry <expiryDate>
    And IPG iFrame is displayed
    When saved credit card with details <cardType> <maskedCardNumber> <expiryDate> is displayed
    Then customer is able to select card with details <cardType> <maskedCardNumber> <expiryDate> to proceed with payment

    Examples: 
      | cardType | maskedCardNumber | expiryDate |
      | VISA     | 424242******4521 | 03/17      |

  Scenario Outline: Customer is able to pay with saved credit card
    Given customer is a registered customer
    And customer has a saved card with details of card type <cardType>, masked card number <maskedCardNumber> and card expiry <expiryDate>
    And IPG iFrame is displayed
    And saved credit card with details <cardType> <maskedCardNumber> <expiryDate> is displayed
    And customer selects card with details <cardType> <maskedCardNumber> <expiryDate> to proceed with payment
    When customer submits the payment
    And order is created successfully

    Examples: 
      | cardType | maskedCardNumber | expiryDate |
      | VISA     | 424242******4521 | 03/17      |

  Scenario Outline: Saving credit card in IPG saves the card in My Account section
    Given customer is a registered customer
    And customer has a saved card with details of card type <cardType>, masked card number <maskedCardNumber> and card expiry <expiryDate>
      | cardType | maskedCardNumber | expiryDate |
      | VISA     | 424242******4521 | 03/17      |
    And IPG iFrame is displayed
    And customer adds a new credit card with card details of card type <newCardType>, card number <newCardNumber> and card expiry <newCardExpiryDate>
    And customer selects to save the credit card in iFrame
    And customer does not select to mark the credit card as primary card
    And customer submits the payment
    And order is created successfully
    When customer nagivates to My payment details in My Account
    Then newly saved credit card is displayed
    And logo for <newCardType> is displayed
    And masked <newCardNumber> is displayed
    And <newCardExpiry> is displayed
    And new credit card is not marked as primary card

    Examples: 
      | newCardType | newCardNumber   | newCardExpiryDate |
      | AMEX        | 349876*****0044 | 08/20             |

  Scenario Outline: Saving credit card as primary card in IPG saves the card in My Account section as primary card
    Given customer is a registered customer
    And customer has primary saved card with details of card type <cardType>, masked card number <maskedCardNumber> and card expiry <expiryDate>
      | cardType | maskedCardNumber | expiryDate |
      | VISA     | 424242******4521 | 03/17      |
    And IPG iFrame is displayed
    And customer adds a new credit card with card details of card type <newCardType>, card number <newCardNumber> and card expiry <newCardExpiryDate>
    And customer selects to save the credit card in iFrame
    And customer selects to mark the credit card as primary card
    And customer submits the payment
    And order is created successfully
    When customer nagivates to My payment details section in My Account
    Then newly saved credit card is displayed
    And logo for <newCardType> is displayed
    And masked <newCardNumber> is displayed
    And <newCardExpiry> is displayed
    And new credit card is marked as primary card
    And other card is not marked as primary card

    Examples: 
      | newCardType | newCardNumber   | newCardExpiryDate |
      | AMEX        | 349876*****0044 | 08/20             |

  Scenario Outline: Choosing to save 5th card displays an error in iFrame
    Given customer is a registered customer
    And customer has 4 saved credit cards
    And IPG iFrame is displayed
    And customer adds a new credit card with card details of card type <newCardType>, card number <newCardNumber> and card expiry <newCardExpiryDate>
    When customer selects to save the credit card in iFrame
    Then error message is displayed in the iframe
    And customer not able to procced with payment

    Examples: 
      | newCardType | newCardNumber   | newCardExpiryDate |
      | AMEX        | 349876*****0044 | 08/20             |

  Scenario: Hide expired saved credit card details in My Account
    Given a customer is a registered customer
    And current date is 12/2015
    And customer has a saved credit card
      | cardType | maskedCardNumber | expiryDate |
      | VISA     | 424242******4521 | 11/15      |
    When customer nagivates to My payment details section in My Account
    And no saved credit card will be displayed

@notAutomated
Feature: IPG - Payment information in tax invoice
  As on online customer
  I want to know details of payments I used on the order tax invoice
  So that I can be certain on payments I made

  Scenario Outline: Display PayPal payment details on tax invoice
    Given order with total <totalAmount>
    And customer paid with PayPal payment method <paypalAccount>
    And paypal receipt number is <receiptNumber>
    And customer receives tax invoice
    When customer opens tax invoice
    Then <paypalAccount> is displayed in Your Payment Details section
    And <totalAmount> is displayed in Your Payment Details section
    And <receiptNumber> is displayed in Your Payment Details section

    Examples: 
      | totalAmount | paypalAccount     | receiptNumber |
      | $10.00      | paypal@paypal.com | PP1346789     |

  Scenario Outline: Display credit card payment details on tax invoice
    Given order with total <totalAmount>
    And customer paid with credit card; <cardType>, <cardNumber>, <cardExpiry>
    And payment receipt number is <receiptNumber>
    And customer receives tax invoice
    When customer opens tax invoice
    Then <cardType> is displayed in Your Payment Details section
    And <maskedCreditCardNumber> is displayed in Your Payment Details section
    And <cardExpiry> is displayed in Your Payment Details section
    And <totalAmount> is displayed in Your Payment Details section
    And <receiptNumber> is displayed in Your Payment Details section

    Examples: 
      | totalAmount | cardType    | cardNumber       | maskedCardNumber | cardExpiry | receiptNumber |
      | $10.00      | Diners Club | 3676541234560036 | 367654******0036 | 02/17      | 123465789     |

  Scenario Outline: Display gift card payment details on tax invoice
    Given order with total <totalAmount>
    And customer paid with gift card <cardNumber>
    And payment receipt number is <receiptNumber>
    And customer receives tax invoice
    When customer opens tax invoice
    Then <maskedCardNumber> is displayed in Your Payment Details section
    And <totalAmount> is displayed in Your Payment Details section
    And <receiptNumber> is displayed in Your Payment Details section

    Examples: 
      | totalAmount | cardNumber       | maskedCardNumber | receiptNumber |
      | $10.00      | 6273451234568796 | 627345******8796 | 123465789     |

  Scenario Outline: Display split payment details on tax invoice
    Given customer paid with split payments
      | payment1   | payment2   |
      | <payment1> | <payment2> |
    And customer receives tax invoice
    When customer opens tax invoice
    Then payment information is displayed in Your Payment Details section
      | payment1   | payment2   |
      | <payment1> | <payment2> |

    Examples: 
      | payment1                             | payment2                             |
      | GC1,627345******8796,$10.00,receipt1 | CC1,424242******4242,$15.00,receipt2 |

  Scenario Outline: Display credit card refund details on tax invoice
    Given order with total <totalAmount>
    And customer paid with credit card; <cardType>, <cardNumber>, <cardExpiry>
    And payment receipt number is <paymentReceiptNumber>
    And partical refund of amount <refundAmount> was done to credit card
    And refund receipt number is <refundReceiptNumber>
    And customer receives tax invoice
    When customer opens tax invoice
    Then payment transaction details displayed as <cardType>,<maskedCardNumber>,<totalAmount>,<paymentReceiptNumber>
    And refund transaction details displayed as <cardType>,<maskedCardNumber>,<refundAmount>,<refundReceiptNumber>

    Examples: 
      | totalAmount | cardType    | cardNumber       | maskedCardNumber | cardExpiry | paymentReceiptNumber | refundAmount | refundReceiptNumber |
      | $10.00      | Diners Club | 3676541234560036 | 367654******0036 | 02/17      | 123465789            | $5.50        | 987654321           |

  Scenario Outline: Display manual refund details on tax invoice
    Given order with total <totalAmount>
    And customer paid with gift card; <cardNumber>
    And payment receipt number is <paymentReceiptNumber>
    And manual refund of <totalAmount> was performed
    And refund receipt number is <refundReceiptNumber>
    And customer receives tax invoice
    When customer opens tax invoice
    Then payment transaction details displayed as 'Gift Card',<maskedCardNumber>,<totalAmount>,<paymentReceiptNumber>
    And refund transaction details displayed as 'Credit Card',<totalAmount><refundReceiptNumber>

    Examples: 
      | totalAmount | cardNumber       | maskedCardNumber | paymentReceiptNumber | refundReceiptNumber |
      | $10.00      | 6273451234568796 | 627345******8796 | 123465789            | 987654321           |

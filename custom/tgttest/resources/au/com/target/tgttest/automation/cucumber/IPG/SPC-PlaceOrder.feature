@cartProductData @deliveryModeData
Feature: SPC - Place order successfully with IPG payment
  In order to purchase products
  As an online customer
  I want to use my credit card to make payment via ipg, and the order will be placed according to payment result and stock level.

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
    And a registered customer goes into the checkout process
    And user checkout via spc
    And giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | GC1       | Giftcard   | 62734500000000002 | 01/20      | 123456789 |             | 31  |
      | GC2       | Giftcard   | 62779575000000000 | 01/20      | 123456789 |             | 32  |
      | GC3       | Giftcard   | 62779575000000001 | 01/20      | 123456789 |             | 32  |
      | GC4       | Giftcard   | 62733500000000003 | 01/20      | 123456789 |             | 33  |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |
      | CC2       | MASTERCARD | 5587654321010013  | 02/20      | 123456789 | 01/02/2020  | 36  |
      | CC3       | AMEX       | 349876543210010   | 03/20      | 123456789 | 01/03/2020  | 37  |
      | CC4       | DINERS     | 36765432100028    | 04/20      | 123456789 | 01/04/2020  | 38  |
    And flybuys config max redeemable amount is 50
    And customer has valid flybuys number presented
    And the user entered flybuys details are 'valid'
    And Customer has available points '11000'

  Scenario: Placing an order with saved credit card.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And ipg returns saved cards
      | cardType | cardNumber       | cardExpiry | token     | tokenExpiry | bin | defaultCard |
      | VISA     | 4242424242424242 | 01/20      | 123456789 | 01/01/2020  | 34  | true        |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'

  Scenario: Placing an order with successful payment and product in stock.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And payment method is 'ipg'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |

  Scenario: Place order with partial stock
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
    And guest checkout
    And payment method is 'ipg'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |

  Scenario: Placing an Order with IPG Credit Card as Registered User
    Given a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
      | 10001030 | 2   | 15    |
      | 10000710 | 3   | 8     |
    And registered user checkout
    And payment method is 'ipg'
    And ipg returns saved cards
      | cardType | cardNumber       | cardExpiry | token     | tokenExpiry | bin | defaultCard |
      | VISA     | 5123450000000346 | 01/20      | 123456789 | 01/01/2020  | 34  | true        |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 73.0  | 64.0     | 9.0          |          |
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000911 | 1   | 1000  | 1000      |
      | 10001030 | 2   | 1500  | 1500      |
      | 10000710 | 3   | 800   | 800       |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 7300
    And tlog credit card payment info matches

  Scenario: Placing an order successfully with gift card
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And payment method is 'ipg'
    And payment type is giftcard
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |

  Scenario Outline: Make successful payment with combination of credit cards and gift cards
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | <payment1>   |
      | <payment2>   |
      | <payment3>   |
      | <payment4>   |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |
    And the payment details of the transaction will be recorded correctly

    Examples: 
      | payment1 | payment2 | payment3 | payment4 |
      | GC1,10   | GC2,10   | GC3,10   | GC4,9    |
      | GC2,10   | GC3,10   | GC4,19   |          |
      | GC3,20   | GC4,19   |          |          |
      | GC4,39   |          |          |          |
      | CC1,10   | CC2,10   | CC3,10   | CC4,9    |
      | CC2,10   | CC3,10   | CC4,19   |          |
      | CC3,10   | CC4,29   |          |          |
      | CC4,39   |          |          |          |
      | GC1,10   | CC1,29   |          |          |
      | GC1,10   | CC1,10   | CC2,19   |          |
      | GC1,10   | CC1,10   | CC2,10   | CC3,9    |
      | GC1,10   | GC2,10   | CC1,19   |          |
      | GC1,10   | GC2,10   | CC1,10   | CC2,9    |
      | GC1,10   | GC2,10   | GC3,10   | CC1,9    |

  Scenario: Place order with partial stock with flybuys discount applied
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
    And registered user checkout
    And the customer logins flybuys in spc
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE2'
    And payment method is 'ipg'
    And set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax | totalDiscounts |
      | 19    | 30       | 9.0          |          | 20             |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |

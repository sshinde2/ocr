@cartProductData @sessionCatalogVersion @storeFulfilmentCapabilitiesData
Feature: EGiftCards - Accertify feed
  As a target online Fraud team member
  I want the data  for each transaction sent to Acertify
  So that I check whether there is a fraud risk for each transaction

  Background: 
    Given payment method is 'ipg'

  Scenario: Sending Single Recipient Email Address to Accertify
    Given gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                        | messageText            |
      | PGC1000_iTunes_10 | FirstName | LastName | fname.lastname@target.com.au | This is a test message |
    And registered user checkout
    And delivery mode is 'digital-gift-card'
    And customer pays with credit card
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 5236896578548965 | 01/2020    |
    When order is placed
    Then the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 5236896578548965 |                | visa     | 10                   | 2020/01        | 5236896578548965 | 78901234556 |
    And the order item entries are
      | itemNumber        | Category   | quantity | recipientEmailAddresses      |
      | PGC1000_iTunes_10 | EGiftCards | 1        | fname.lastname@target.com.au |

  Scenario: Sending Multiple Recipient Email Address to Accertify
    Given gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                        | messageText            |
      | PGC1000_iTunes_10 | FirstName | LastName | fname.lastname@target.com.au | This is a test message |
      | PGC1000_iTunes_10 | Fname     | LName    | fname.lname@target.com.au    | This is a test message |
    And registered user checkout
    And delivery mode is 'digital-gift-card'
    And customer pays with credit card
      | cardType | cardNumber       | cardExpiry |
      | VISA     | 5236896578548965 | 01/2020    |
    When order is placed
    Then the accertify payment entries:
      | PaymentType | cardNumber       | cardHolderName | cardType | cardAuthorizedAmount | cardExpiryDate | cardReceipt      | cardToken   |
      | CreditCard  | 5236896578548965 |                | visa     | 20                   | 2020/01        | 5236896578548965 | 78901234556 |
    And the order item entries are
      | itemNumber        | Category   | quantity | recipientEmailAddresses                                |
      | PGC1000_iTunes_10 | EGiftCards | 2        | fname.lastname@target.com.au,fname.lname@target.com.au |

  Scenario Outline: Always include billing address to accertify for e-giftcard only orders
    Given feature 'accertify.include.billingAddress' is switched '<featureIncludeBillingAddress>'
    And gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                        | messageText            |
      | PGC1000_iTunes_10 | FirstName | LastName | fname.lastname@target.com.au | This is a test message |
    And registered user checkout
    And delivery mode is 'digital-gift-card'
    And the billing address is '14 Emert street,Wentworthville,NSW,2145'
    When order is placed
    Then the accertify billing information is included

    Examples: 
      | featureIncludeBillingAddress |
      | on                           |
      | off                          |

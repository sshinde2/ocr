@notAutomated
Feature: Afterpay - Payment information in tax invoice
  As on online customer
  I want to know details of payments I used on the order tax invoice
  So that I can be certain on payments I made

  Scenario: Display Afterpay payment details on tax invoice
    Given order with total $10.00
    And customer paid with Afterpay payment method
    And Afterpay order number is 1234567890
    And customer receives tax invoice
    When customer opens tax invoice
    Then the following information is displayed in Your Payment Details section:
      | type   | amount        | receiptNumber   |
      | Afterpay | $10.00 | 1234567890 |
      
  Scenario: Display Afterpay payment details on tax invoice after a refund
    Given order with total $100.00
    And customer paid with Afterpay payment method
    And Afterpay order number is 1234567890
    And a refund for $50.00 is processed
    And the refund receipt number is 4567
    And customer receives tax invoice
    When customer opens tax invoice
    Then the following information is displayed in Your Payment Details section:
      | type   | amount        | receiptNumber   |
      | Afterpay | $100.00 | 1234567890 |
      | Afterpay | $50.00 | 4567 |

Feature: As a target online customer
  I should be able use the Afterpay payment mode

  Scenario: select payment method Afterpay
    Given any cart
    And Afterpay createOrder api returns:
      | token                                                | expires              |
      | q54l9qd907m6iqqqlcrm5tpbjjsnfo47vsm59gqrfnd2rqefk9hu | 2016-05-10T13:14:01Z |
    When the payment mode is 'afterpay'
    Then the Afterpay reponse is successful

  Scenario: Afterpay unavailable
    Given any cart
    And Afterpay createOrder API is unavailable
    When the payment mode is 'afterpay'
    Then the Afterpay reponse is unsuccessful
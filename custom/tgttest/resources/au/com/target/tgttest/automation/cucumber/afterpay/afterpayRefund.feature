@cartProductData @deliveryModeData
Feature: Afterpay - Refund for short pick/zero pick, cancel, refund after order complete, manual refund
  In order to have better shopping experience
  As an Online Store
  I want to refund the amount paid by afterpay as quickly as possible if the customer order is valid for refund
  and raise a CS ticket for giftcard refund or any credit card refund failure

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
      | 10000012 | 0        | 10        |
    And payment method is 'afterpay'
    And user checkout via spc
    And set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 25          | 2            |

  Scenario: Refund for fastline short pick successfully
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And the afterpay refund will 'succeed' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 1   |
    Then the afterpay refund has been trigerred for the order with amount '15'
    And the afterpay refund has been 'succeeded'
    And the order status is INVOICED

  Scenario: Refund for fastline short pick failed with CS ticket raised
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And the afterpay refund will 'fail' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 1   |
    Then the afterpay refund has been trigerred for the order with amount '15'
    And the afterpay refund has been 'failed'
    And a CS ticket is created
    And CS ticket subject is 'Short or Zero pick refund failed'
    And the last CS ticket group is 'refundfailures-csagentgroup'
    And the order status is INVOICED

  Scenario: Refund for fastline short pick with deals successfully
    Given cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 25    | buyget   | Q        |
      | 10000012 | 2   | 25    | buyget   | R        |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 87.5  | 87.5     | 0            |          |
    And the afterpay refund will 'succeed' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 2   |
      | 10000012 | 1   |
    Then the afterpay refund has been trigerred for the order with amount '18.75'
    And the afterpay refund has been 'succeeded'
    And the order status is INVOICED

  Scenario: Refund for fastline short pick with deals failed
    Given cart entries with deals:
      | product  | qty | price | dealName | dealRole |
      | 10000011 | 2   | 25    | buyget   | Q        |
      | 10000012 | 2   | 25    | buyget   | R        |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 87.5  | 87.5     | 0            |          |
    And the afterpay refund will 'fail' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 2   |
      | 10000012 | 1   |
    Then the afterpay refund has been trigerred for the order with amount '18.75'
    And the afterpay refund has been 'failed'
    And a CS ticket is created
    And CS ticket subject is 'Short or Zero pick refund failed'
    And the last CS ticket group is 'refundfailures-csagentgroup'
    And the order status is INVOICED

  Scenario: Refund for fastline zero pick successfully
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And the afterpay refund will 'succeed' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 0   |
    Then the afterpay refund has been trigerred for the order with amount '39'
    And the afterpay refund has been 'succeeded'
    And the order status is CANCELLED

  Scenario: Refund for fastline zero pick failed with CS ticket raised
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And the afterpay refund will 'fail' for the order
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 0   |
    Then the afterpay refund has been trigerred for the order with amount '39'
    And the afterpay refund has been 'failed'
    And a CS ticket is created
    And CS ticket subject is 'Short or Zero pick refund failed'
    And the last CS ticket group is 'refundfailures-csagentgroup'
    And the order status is CANCELLED

  Scenario: Refund for full Cancel from CS successfully
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And the afterpay refund will 'succeed' for the order
    When the order is fully cancelled
    Then the afterpay refund has been trigerred for the order with amount '39'
    And the afterpay refund has been 'succeeded'
    And the order status is CANCELLED

  Scenario: Refund for full Cancel from CS failed when the fluent feature switch is OFF
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And the 'fluent' feature switch is disabled
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And the afterpay refund will 'fail' for the order
    When the order is fully cancelled
    Then the afterpay refund has been trigerred for the order with amount '39'
    And the afterpay refund has been 'failed'
    And the order status is INPROGRESS

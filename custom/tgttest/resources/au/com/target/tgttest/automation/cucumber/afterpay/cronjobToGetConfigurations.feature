Feature: As a Target online
  I want to be get the afterpay configuration
  So that I can diplay the afterpay message base on the configuration

  Background: 
    Given afterpay get configuration api returns the configuration:
      | type               | description   | max  | min |
      | PAY_BY_INSTALLMENT | Pay over time | 1000 | 30  |

  Scenario: create new afterpay configuration
    Given no afterpay configuration is in system
    When the afterpay configuration cronjob is triggered and is 'successful'
    Then afterpay configuration is:
      | type               | description   | max  | min |
      | PAY_BY_INSTALLMENT | Pay over time | 1000 | 30  |

  Scenario: update afterpay configuration
    Given an afterpay configuration is:
      | type | description | max  | min |
      | type | description | 2000 | 10  |
    When the afterpay configuration cronjob is triggered and is 'successful'
    Then afterpay configuration is:
      | type               | description   | max  | min |
      | PAY_BY_INSTALLMENT | Pay over time | 1000 | 30  |

  Scenario: remain the current configuration if get afterpay configuration fails
    Given an afterpay configuration is:
      | type | description | max  | min |
      | type | description | 2000 | 10  |
    And Afterpay configuration API is unavailable
    When the afterpay configuration cronjob is triggered and is 'failed'
    Then afterpay configuration is:
      | type | description | max  | min |
      | type | description | 2000 | 10  |

@notAutomated
Feature: Display afterpay payment details in cs cockpit
  As a CS team member
  I want to view the afterpay payment details
  So that I can verify that the transaction matches the customer

  Scenario: Display afterpay details in payment transaction
    Given payment has been attempted for an order using afterpay
    When the payment transaction entries is displayed in CS cockpit
    Then the afterpay payment transaction entry details are displayed with fields
      | payment provider | request id | Time           | Type    | Amount | Transaction status | Transaction status details |
      | afterpay         | 23871004   | 8/4/17 5:27 PM | CAPTURE | $49.00 | ACCEPTED           | SUCCESFULL                 |
    And the afterpay payment info is displayed with fields
      | Identifier                                              | Time created   | Afterpay Token                                       | Afterpay Order Id |
      | test@target.com.au_77d1c6ec-21eb-46c0-94f8-07e8439fe146 | 8/4/17 5:28 PM | l7evacmi9murcdeq0hbuid7qh4bvojmuoq8uthmr34qo3b89b60d | 23871004          |

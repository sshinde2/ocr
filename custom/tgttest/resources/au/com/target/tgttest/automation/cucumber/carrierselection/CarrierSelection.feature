@cartProductData @deliveryModeData @storeFulfilmentCapabilitiesData @cleanOrderData @cleanConsignmentData
Feature: Select carrier based on consignment weight and delivery area.
  As Fulfilment Department
  I want the cheaper carrier to perform home delivery
  So that I can minimize internal delivery costs
  
  Notes:
  * The product details are setup as:
  | product     | productType |
  | 10000011    | normal      |
  | 10000111    | normal      |
  | 10000211    | normal      |
  | 10000311    | normal      |
  | 10000312    | normal      |
  | 10000411    | bulky1      |
  | 10000510    | bulky2      |
  * Postcodes supported by StarTrack for home-delivery orders are: 3000, 3002, 3003, 3004
  * Max weight supported by StarTrack for home-delivery orders is configured as 3kg.

  Scenario: Use StarTrack for home-delivery order when consignment weight is less than 3kg and postcode is supported by StarTrack.
    Given a cart with entries with weight:
      | product  | qty | weight |
      | 10000011 | 2   | 0.50   |
    And delivery mode is 'home-delivery'
    And the delivery address is '1 Aberdeen Street, Newtown, VIC, 3003'
    When order is placed
    Then consignment sent to 'FastlineWarehouse'
    And carrier is 'StarTrackHD'
    And service type is 'TAR'

  Scenario: Use AustraliaPost for home-delivery order when consignment weight is less than 3kg and postcode is not supported by StarTrack.
    Given a cart with entries with weight:
      | product  | qty | weight |
      | 10000011 | 1   | 0.50   |
      | 10000311 | 2   | 0.78   |
    And delivery mode is 'home-delivery'
    And the delivery address is '112 Surf Parade, Broadbeach, QLD, 9945'
    When order is placed
    Then consignment sent to 'FastlineWarehouse'
    Then carrier is 'AustraliaPost'
    And service type is 'S1'

  Scenario: Use AustraliaPost for home-delivery order when consignment weight is more than 3kg.
    Given a cart with entries with weight:
      | product  | qty | weight |
      | 10000111 | 2   | 2.50   |
      | 10000211 | 2   | 3.99   |
    And delivery mode is 'home-delivery'
    And the delivery address is '1 Aberdeen Street, Newtown, VIC, 3003'
    When order is placed
    Then consignment sent to 'FastlineWarehouse'
    Then carrier is 'AustraliaPost'
    And service type is 'S1'

  Scenario: Use TOLL for home-delivery order when consignment contains B1/B2/MHD products.
    Given a cart with entries with weight:
      | product  | qty | weight |
      | 10000311 | 1   | 0.78   |
      | 10000411 | 1   | 10.20  |
    And delivery mode is 'home-delivery'
    And the delivery address is '1 Aberdeen Street, Newtown, VIC, 3003'
    When order is placed
    Then consignment sent to 'FastlineWarehouse'
    Then carrier is 'Toll'
    And service type is 'V'

  Scenario: Use AustraliaPost for home-delivery order when consignment weight is unknown.
    Given a cart with entries with weight:
      | product  | qty | weight |
      | 10000312 | 1   |        |
      | 10000311 | 1   | 0.78   |
    And delivery mode is 'home-delivery'
    And the delivery address is '1 Aberdeen Street, Newtown, VIC, 3003'
    When order is placed
    Then consignment sent to 'FastlineWarehouse'
    Then carrier is 'AustraliaPost'
    And service type is 'S1'

  Scenario: Use StarTrack for normal express-delivery order.
    Given a cart with entries with weight:
      | product  | qty | weight |
      | 10000011 | 2   | 0.50   |
      | 10000211 | 1   | 3.99   |
    And delivery mode is 'express-delivery'
    And the delivery address is '1 Aberdeen Street, Newtown, VIC, 3003'
    When order is placed
    Then consignment sent to 'FastlineWarehouse'
    Then carrier is 'StarTrack'
    And service type is 'FPP'

  Scenario: Use TOLL for normal click-and-collect order.
    Given a cart with entries with weight:
      | product  | qty | weight |
      | 10000011 | 2   | 0.50   |
      | 10000211 | 1   | 3.99   |
    And delivery mode is 'click-and-collect'
    And the delivery address is '112 Surf Parade, Broadbeach, QLD, 9000'
    When order is placed
    Then consignment sent to 'FastlineWarehouse'
    And carrier is 'TollCnC'
    And service type is 'X'

  Scenario: Customer delivery order rejected by store should be rerouted to Fastline and carrier should be StarTrack if the consignment weight is less than 3kg and postcode supported by StarTrack.
    Given a store '7032' with fulfilment capability and stock
    And a cart with entries with weight:
      | product  | qty | weight |
      | 10000311 | 2   | 0.78   |
    And delivery mode is 'home-delivery'
    And the delivery address is '35,Collins St, Melbourne,VIC,3000'
    And order is placed
    And consignment is assigned to 'Camberwell' and rejected by the store
    Then consignment redirected and sent to 'FastlineWarehouse'
    And carrier is 'StarTrackHD'
    And service type is 'TAR'

  Scenario: Customer delivery order rejected by store should be rerouted to Fastline and carrier should be AustraliaPost if the consignment weight is less than 3kg and postcode not supported by Startrack.
    Given a store '7032' with fulfilment capability and stock
    And a cart with entries with weight:
      | product  | qty | weight |
      | 10000311 | 2   | 0.78   |
    And delivery mode is 'home-delivery'
    And the delivery address is '35,Collins St, Melbourne,VIC,3001'
    And order is placed
    And consignment is assigned to 'Camberwell' and rejected by the store
    Then consignment redirected and sent to 'FastlineWarehouse'
    And carrier is 'AustraliaPost'
    And service type is 'S1'

  Scenario: Customer delivery order rejected by store should be rerouted to Fastline and carrier should be AustraliaPost if the consignment weight is more than 3kg.
    Given a store '7032' with fulfilment capability and stock
    And a cart with entries with weight:
      | product  | qty | weight |
      | 10000311 | 2   | 0.78   |
      | 10000211 | 2   | 3.99   |
    And delivery mode is 'home-delivery'
    And the delivery address is '35,Collins St, Melbourne,VIC,3000'
    And order is placed
    And consignment is assigned to 'Camberwell' and rejected by the store
    Then consignment redirected and sent to 'FastlineWarehouse'
    And carrier is 'AustraliaPost'
    And service type is 'S1'

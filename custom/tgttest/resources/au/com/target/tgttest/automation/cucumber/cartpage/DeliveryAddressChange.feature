@postCodeGroupData @cartProductData @deliveryModeData @clearSessionCartData
Feature: Restrict delivery fee value by post code catchment area
  
  In order to restrict delivery fee
  As a customer I want to enter post code in the text field to get the exact delivery fee value
  
  Notes:
  * Products are setup as:
  | product  | productType |
  | 10000011 | normal      |
  | 10000411 | bulky       |

  Background: 
    Given Postcodes allocated for the catchment area and also delivery fee set up based on product type and quantity.
      | postCode | catchmentArea | productType | qty       | deliveryFee |
      | 3002     | auto-Metro    | bulky1      | 1-2       | $15         |
      | 3003     | auto-Metro    | bulky1      | 3 or more | $30         |
      | 3222     | auto-Regional | bulky1      | 1-2       | $25         |
      | 3223     | auto-Regional | bulky1      | 3 or more | $50         |
      | 4875     | auto-Rural    | bulky1      | 1-2       | $45         |
      | 4876     | auto-Rural    | bulky1      | 3 or more | $80         |
      | n/a      | n/a           | normal      | 1 or more | $9          |
      | n/a      | n/a           | bulky1      | 1-2       | $59         |
      | n/a      | n/a           | bulky1      | 3 or more | $79         |
    And customer has saved addresses
      | 12 Thompson Road,North Geelong,3002 |
      | 13 Thompson Road,North Geelong,3003 |
      | 14 Thompson Road,North Geelong,3222 |
      | 15 Thompson Road,North Geelong,3223 |
      | 16 Thompson Road,North Geelong,4875 |
      | 17 Thompson Road,North Geelong,4876 |
      | 25 Thompson Road,North Geelong,0201 |
    And product delivery modes:
      | product  | deliveryMode                                     |
      | 10000011 | home-delivery,click-and-collect,express-delivery |

  Scenario Outline: : As a customer I want to know whether the delivery fee changed when I select a new address.
    Given a cart with product '<productsInCart>' and with quantity '<qty>'
    And delivery address selected in spc is '<originalAddress>' for delivery mode '<deliveryMode>'
    And delivery fee for cart in spc is '<originalDeliveryFee>'
    When delivery address selected in spc is '<selectedAddress>' for delivery mode '<deliveryMode>'
    Then delivery fee for cart is '<newDeliveryFee>' and it has changed '<deliveryFeeChanged>'

    Examples: 
      | productsInCart    | qty | deliveryMode  | originalAddress                     | originalDeliveryFee | selectedAddress                     | newDeliveryFee | deliveryFeeChanged |
      | 10000011          | 1   | home-delivery |                                     | $9                  | 12 Thompson Road,North Geelong,3002 | $9             | No                 |
      | 10000411          | 1   | home-delivery |                                     |                     | 12 Thompson Road,North Geelong,3002 | $15            | Yes                |
      | 10000411          | 1   | home-delivery |                                     |                     | 14 Thompson Road,North Geelong,3222 | $25            | Yes                |
      | 10000411          | 1   | home-delivery |                                     |                     | 16 Thompson Road,North Geelong,4875 | $45            | Yes                |
      | 10000411          | 1   | home-delivery | 12 Thompson Road,North Geelong,3002 | $15                 |                                     | $15            | Yes                |
      | 10000411          | 1   | home-delivery | 14 Thompson Road,North Geelong,3222 | $25                 |                                     | $25            | Yes                |
      | 10000411          | 1   | home-delivery | 16 Thompson Road,North Geelong,4875 | $45                 |                                     | $45            | Yes                |
      | 10000011          | 2   | home-delivery | 12 Thompson Road,North Geelong,3002 | $9                  | 13 Thompson Road,North Geelong,3003 | $9             | No                 |
      | 10000411          | 1   | home-delivery | 12 Thompson Road,North Geelong,3002 | $15                 | 13 Thompson Road,North Geelong,3003 | $15            | No                 |
      | 10000011          | 2   | home-delivery | 14 Thompson Road,North Geelong,3222 | $9                  | 15 Thompson Road,North Geelong,3223 | $9             | No                 |
      | 10000411          | 1   | home-delivery | 14 Thompson Road,North Geelong,3222 | $25                 | 15 Thompson Road,North Geelong,3223 | $25            | No                 |
      | 10000011          | 2   | home-delivery | 16 Thompson Road,North Geelong,4875 | $9                  | 17 Thompson Road,North Geelong,4876 | $9             | No                 |
      | 10000411          | 1   | home-delivery | 16 Thompson Road,North Geelong,4875 | $45                 | 17 Thompson Road,North Geelong,4876 | $45            | No                 |
      | 10000411          | 3   | home-delivery | 12 Thompson Road,North Geelong,3002 | $30                 | 17 Thompson Road,North Geelong,4876 | $80            | Yes                |
      | 10000411          | 3   | home-delivery | 17 Thompson Road,North Geelong,4876 | $80                 | 12 Thompson Road,North Geelong,3002 | $30            | Yes                |
      | 10000411          | 2   | home-delivery | 12 Thompson Road,North Geelong,3002 | $15                 | 14 Thompson Road,North Geelong,3222 | $25            | Yes                |
      | 10000411          | 2   | home-delivery | 14 Thompson Road,North Geelong,3222 | $25                 | 12 Thompson Road,North Geelong,3002 | $15            | Yes                |
      | 10000411          | 7   | home-delivery | 12 Thompson Road,North Geelong,3002 | $30                 | 15 Thompson Road,North Geelong,3223 | $50            | Yes                |
      | 10000411          | 7   | home-delivery | 15 Thompson Road,North Geelong,3223 | $50                 | 12 Thompson Road,North Geelong,3002 | $30            | Yes                |
      | 10000411          | 1   | home-delivery | 12 Thompson Road,North Geelong,3002 | $15                 | 16 Thompson Road,North Geelong,4875 | $45            | Yes                |
      | 10000411          | 1   | home-delivery | 16 Thompson Road,North Geelong,4875 | $45                 | 12 Thompson Road,North Geelong,3002 | $15            | Yes                |
      | 10000011,10000411 | 1,1 | home-delivery | 12 Thompson Road,North Geelong,3002 | $15                 | 13 Thompson Road,North Geelong,3003 | $15            | No                 |
      | 10000011,10000411 | 4,3 | home-delivery | 12 Thompson Road,North Geelong,3002 | $30                 | 13 Thompson Road,North Geelong,3003 | $30            | No                 |
      | 10000011,10000411 | 5,1 | home-delivery | 12 Thompson Road,North Geelong,3002 | $15                 | 14 Thompson Road,North Geelong,3222 | $25            | Yes                |
      | 10000011,10000411 | 5,1 | home-delivery | 14 Thompson Road,North Geelong,3222 | $25                 | 12 Thompson Road,North Geelong,3002 | $15            | Yes                |
      | 10000011,10000411 | 2,5 | home-delivery | 12 Thompson Road,North Geelong,3002 | $30                 | 15 Thompson Road,North Geelong,3223 | $50            | Yes                |
      | 10000011,10000411 | 2,5 | home-delivery | 15 Thompson Road,North Geelong,3223 | $50                 | 12 Thompson Road,North Geelong,3002 | $30            | Yes                |
      | 10000011,10000411 | 3,3 | home-delivery | 12 Thompson Road,North Geelong,3002 | $30                 | 16 Thompson Road,North Geelong,4875 | $80            | Yes                |
      | 10000011,10000411 | 3,3 | home-delivery | 16 Thompson Road,North Geelong,4875 | $80                 | 12 Thompson Road,North Geelong,3002 | $30            | Yes                |
      | 10000011,10000411 | 6,6 | home-delivery | 12 Thompson Road,North Geelong,3002 | $30                 | 17 Thompson Road,North Geelong,4876 | $80            | Yes                |
      | 10000011,10000411 | 6,6 | home-delivery | 17 Thompson Road,North Geelong,4876 | $80                 | 12 Thompson Road,North Geelong,3002 | $30            | Yes                |

  Scenario Outline: As a customer I want to know whether the selected address is applicable for express delivery
    Given a cart with product '<productsInCart>' and with quantity '<qty>'
    When delivery address selected in spc is '<selectedAddress>' for delivery mode 'express-delivery'
    Then the address '<selectedAddress>' is '<status>' to the cart
    And the error code is '<Code>'

    Examples: 
      | productsInCart | qty | selectedAddress                     | status    | Code                    |
      | 10000011       | 1   | 12 Thompson Road,North Geelong,3002 | Saved     | N/A                     |
      | 10000011       | 1   | 25 Thompson Road,North Geelong,0201 | Not Saved | ERR_ADDRESS_UNAVAILABLE |

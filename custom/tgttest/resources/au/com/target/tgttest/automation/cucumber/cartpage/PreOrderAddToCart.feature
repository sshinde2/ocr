@initPreOrderProducts
Feature: As a target online customer
    If I am trying to add the preOrder products into cart, I shall come across preOrder validations.

  Scenario: As a customer, while having an empty cart, I try to add a preOrder product in my cart and it get added successfully
    Given the cart is empty
    Then adding product 'V2222_preOrder_2' with quantity '1' returns 'success'

  Scenario Outline: As a customer I try to order mixed products while doing a preOrder.
    Given a cart with product '<productsInCart>' and with quantity '<qty>'
    Then adding product '<newProduct>' with quantity '<newProductQty>' returns '<expectedStatus>'

    Examples: 
      | productsInCart        | qty | newProduct            | newProductQty | expectedStatus             |
      |              10000011 |   1 | V2222_preOrder_2      |             1 | noMixedBasketWithPreOrders |
      | V1111_preOrder_1      |   1 | V2222_preOrder_2      |             1 | noMixedBasketWithPreOrders |
      | V1111_preOrder_1      |   1 |              10000011 |             1 | noMixedBasketWithPreOrders |
      |              10000012 |   1 |              10000011 |             1 | success                    |
      | PHYGC1003_skydive_100 |   1 | V1111_preOrder_1      |             1 | noMixedBasketWithPreOrders |
      | V1111_preOrder_1      |   1 | PHYGC1003_skydive_100 |             1 | noMixedBasketWithPreOrders |

  Scenario: As a customer I try to order mixed products(Digital gift card and Pre-order product) while doing a preOrder.
    Given a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText            |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    Then adding product 'V1111_preOrder_1' with quantity '1' returns 'noMixedBasketWithPreOrders'

  Scenario: As a customer I try to order mixed products(Pre-order product and Digital gift card) while doing a preOrder.
    Given a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 |   1 |    15 |
    Then a digital gift card with recipient details added to the cart returns 'noMixedBasketWithPreOrders':
      | productCode       | firstName | lastName | email                          | messageText            |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |

  Scenario Outline: As a customer, I try to add more than 1 quantity of my pre order item to cart.
    Given a cart with product '<productsInCart>' and with quantity '<qty>'
    Then adding product '<sameProduct>' with quantity '<newProductQtyToAdd>' returns '<expectedStatus>'

    Examples: 
      | productsInCart    			| qty | sameProduct       			| newProductQtyToAdd | expectedStatus    			  |
      | V2222_preOrder_2  			|   1 | V2222_preOrder_2  			|                  1 | success           			  |
      | V2222_preOrder_2  			|   2 | V2222_preOrder_2  			|                  4 | preOrder.itemsThresholdReached |
      | V2222_preOrder_2  			|   1 | V2222_preOrder_2  		  	|                  2 | preOrder.lowStock 			  |
      | V3333_preOrder_maxQuantity	|   2 | V3333_preOrder_maxQuantity	|                  2 | preOrder.noStock               |

  Scenario: As a customer, I try to add a preOrder product whose max preOrder quantity has reached
    Given the cart is empty
    And the product 'V3333_preOrder_maxQuantity' has reached max preOrder quantity
    Then adding product 'V3333_preOrder_maxQuantity' with quantity '1' returns 'preOrder.noStock'

  Scenario Outline: As a customer, I try to pre order a product, which is not allowed for pre-order
    Given the cart is empty
    Then adding product '<product>' fails

    Examples: 
      | product                                         |
      | V4444_preOrder_noStAndEndDate                   |
      | V4444_preOrder_endDatePassedEmbargoDtNotArrived |
      | V4444_preOrder_stDateNotArrived                 |

  Scenario: As a customer, I have preOrder product in cart and perform SOH with pre order stock still available.
    Given a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 |   2 |    10 |
    And set fastline stock:
      | product          | reserved | available | preOrder |
      | V1111_preOrder_1 |        0 |        10 |        2 |
    And a soh check is done on the cart
    Then the items in the cart are:
      | product          | qty | price |
      | V1111_preOrder_1 |   2 |    10 |

  Scenario: As a customer, I have preOrder product in cart and perform SOH with pre order stock still available, but no online normal stock.
    Given a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 |   2 |    10 |
    And set fastline stock:
      | product          | reserved | available | preOrder |
      | V1111_preOrder_1 |       10 |        10 |        2 |
    And a soh check is done on the cart
    Then the items in the cart are:
      | product          | qty | price |
      | V1111_preOrder_1 |   2 |    10 |

  Scenario: As a customer, I have preOrder product in cart and perform SOH with pre order stock less than the quantity in cart.
    Given a cart with entries:
      | product          | qty | price |
      | V2222_preOrder_2 |   2 |    10 |
    And set fastline stock:
      | product          | reserved | available | preOrder |
      | V2222_preOrder_2 |        0 |        10 |      999 |
    And a soh check is done on the cart
    Then the items in the cart are:
      | product          | qty | price |
      | V2222_preOrder_2 |   1 |    10 |

  Scenario: As a customer, I have preOrder product in cart and perform SOH with no pre order stock left.
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 |   2 |    10 |
    And set fastline stock:
      | product          | reserved | available | preOrder |
      | V5555_preOrder_5 |        0 |        10 |     1000 |
    And a soh check is done on the cart
    Then the cart is empty
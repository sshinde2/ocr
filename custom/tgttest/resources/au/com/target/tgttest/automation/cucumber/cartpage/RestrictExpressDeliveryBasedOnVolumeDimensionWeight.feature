@cartProductData @deliveryModeData
Feature: Restrict express delivery for cart with products within the configured dead weight, max dimension and volume.
  
  
  In order to restrict express delivery 
  As the online store
  I want to prevent express delivery for cart with products configured above dead weight, dimension and volume.
  
  Notes:
  * Products are setup in impex as:
      | product  | weight(kgs) | length(cms)  | width(cms) | height(cms) | volume(cm3) |
      | W1000001 | 5           | 25           | 30         | 49          | 36750       |
      | W1000011 | 4           | 40           | 50         | 35          | 70000       |
      | W1000021 | 3           | 45           | 10         | 25          | 11250       |
      | W1000031 | 6           | 55           | 45         | 20          | 49500       |
      | W1000041 | 2           | 30           | 25         | 15          | 11250       |
      | W1000051 | 10          | 40           | 60         | 30          | 72000       |
      | W1000061 | 1           | 10           | 50         | 40          | 20000       |
      | W1000071 | 11          | 40           | 45         | 100         | 180000      |
      | W1000081 | N/A         | N/A          | N/A        | N/A         | N/A         |
      | W1000091 | 0           | 30           | 0          | 45          | 0           |
      | W1000101 | -6          | 30           | 40         | -5          | N/A         |
      | W100002  | 5           | 10           | 10         | 1           | 100         |
      | W100003  | 5           | 11           | 1          | 1           | 11          |
   
   * All the Delivery modes are Enabled through configuration

  Background: 
    Given delivery value with dead weight restriction as 5kg, dimension restriction as 50cms and volume restriction as 70000 cubic cms for the express delivery

  Scenario Outline: Available delivery modes on cart page based on weight, dimension and volume of the cart
    Given a cart with entries '<cartEntries>'
    When the delivery modes for the cart is fetched
    Then delivery modes available for the cart are '<availableDeliveryModesForProduct>'

    Examples: 
      | cartEntries                                    | availableDeliveryModesForProduct                 |
      | 1 of 100010901                                 | express-delivery,home-delivery,click-and-collect |
      | 1 of 100010911                                 | express-delivery,home-delivery,click-and-collect |
      | 1 of 100010901, 1 of 100010911, 1 of 100010921 | home-delivery,click-and-collect                  |
      | 3 of 100010931                                 | home-delivery,click-and-collect                  |
      | 1 of 100010941                                 | express-delivery,home-delivery,click-and-collect |
      | 1 of 100010941, 2 of 100010951                 | home-delivery,click-and-collect                  |
      | 3 of 100010921, 1 of 100010941, 1 of 100010961 | home-delivery,click-and-collect                  |
      | 1 of 100010971                                 | home-delivery,click-and-collect                  |
      | 2 of 100010971, 2 of 100010981                 | home-delivery,click-and-collect                  |
      | 1 of 100010941, 1 of 100010991                 | express-delivery,home-delivery,click-and-collect |
      | 1 of 100011121                                 | home-delivery,click-and-collect                  |
      | 1 of 10000211                                  | express-delivery,home-delivery,click-and-collect |
      | 1 of 10000311                                  | express-delivery,home-delivery,click-and-collect |

  Scenario Outline: delivery promotion text on cart page based on weight, dimension and volume of the product
    Given a product with '<productCode>'
    And delivery promotion ranks order is '<deliveryPromotionRank>'
    And express delivery promo display settings are '<expressDeliveryPromo>'
    And home delivery promo display settings are '<homeDeliveryPromo>'
    And cnc delivery promo display settings are '<CnCPromo>'
    When product details are retrieved
    Then delivery promotion text is '<deliveryPromotionText>'

    Examples: 
      | productCode | deliveryPromotionRank          | expressDeliveryPromo             | homeDeliveryPromo                | CnCPromo                         | deliveryPromotionText  |
      | W1000001    | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | express or home or cnc |
      | W1000011    | click-and-collect,home,express | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | cnc or home or express |
      | W1000021    | express,home,click-and-collect | STICKER PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT | NONE                   |
      | W1000031    | express,click-and-collect,home | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT | home                   |
      | W1000041    | home,click-and-collect,express | STICKER PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT PRESENT     | cnc                    |
      | W1000051    | click-and-collect,express,home | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT | NONE                   |
      | W1000061    | home,express,click-and-collect | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT | home or express        |
      | W1000071    | home,express,click-and-collect | STICKER PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT | NONE                   |
      | W1000081    | click-and-collect,express,home | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT NOT-PRESENT | NONE                   |
      | W1000091    | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT | express or home        |
      | W1000101    | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT | home                   |
      | W100002     | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT | express or home        |
      | W100003     | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT | express or home        |

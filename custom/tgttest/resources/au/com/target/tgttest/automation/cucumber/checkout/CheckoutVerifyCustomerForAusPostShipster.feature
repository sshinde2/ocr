@cartProductData @customerData

Feature: As a Target customer
  when I enter my email address
  system should check if the email address is registered for AusPost Shipster 

  Background: 
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    
  Scenario: Target customer begins checkout as guest where email id is registered with AusPost Shipster
    Given a customer does not exist with the user name 'idontexist@target.com.au'
    And the customer email address is 'registered' for AusPost Shipster
    When the customer begins checkout as guest
    Then verify email api is invoked successfully
    And member is returned as 'verified'
    
    
  Scenario: Target customer begins checkout as guest where email id is not registered with AusPost Shipster
    Given a customer does not exist with the user name 'idontexist@target.com.au'
    And the customer email address is 'unregistered' for AusPost Shipster
    When the customer begins checkout as guest
    Then verify email api is invoked successfully
    And member is returned as 'not verified'
    
  Scenario: Target customer begins checkout as guest but call to AusPost verify email api fails
    Given a customer does not exist with the user name 'idontexist@target.com.au'
    And the customer email address is 'unregistered' for AusPost Shipster
    When the customer begins checkout as guest
    Then verify email api fails
    And Shipster status reponse is unsuccessful
    
    
  Scenario: Target customer begins checkout as a logged in user where email id is registered with AusPost Shipster
    Given a customer exists with the user name 'therealdeal@target.com.au'
    And the customer email address is 'registered' for AusPost Shipster
    When registered user proceed to checkout 
    Then verify email api is invoked successfully
    And member is returned as 'verified'
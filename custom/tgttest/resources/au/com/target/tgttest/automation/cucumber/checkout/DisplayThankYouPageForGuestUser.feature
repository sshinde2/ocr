@notAutomated
Feature: Display Thank You Page For Guest User
  In order to checkout faster
  As a online user
  I want to see the new spc thank you page for guest checkout

  Background: 
    Given feature 'spc.thankyou' is enabled

  Scenario: Checkout as a Guest User
    Given the Guest User is on the Target website, who has added items to basket for Checkout
    When the user completes the purchase of items
    Then the user gets navigated to the new SPC- Thank You Page
    And the user sees a message " Your order number 12345678 is currently being processed and you will receive a confirmation email at loggedin@emailaddress.com"
    And the same order number 12345678 gets displayed in the URL
    And the user sees a box to to enter the password, which has a placeholder text "Choose a password"

  Scenario: Refreshing of the Page (Test only)
    Given the user has executed the above scenario "Checkout as a Guest User"
    When the user refreshs the page
    Then the user lands to the same page

  Scenario: Moving away from Thank You Page (Test only)
    Given the user has executed the scenario "Checkout as a Guest User"
    And the user has copied the Thank You Page URL
    When the user moves away from Thank You Page to another page, within the website
    And the user tries to open the Thank You Page again, with the same URL copied before
    Then the user lands to an empty Basket Page

@cartProductData @deliveryModeData @initPreOrderProducts @cleanOrderData @cleanConsignmentData
Feature: Checkout
  In order to purchase products, as a customer I need a checkout process.
  Note defaults are: guest user, home delivery and a credit card payment.

  Scenario: Placing an order as a guest user.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39.0  | 30.0     | 9.0          |          |

  Scenario: Placing an order as a registered user.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    When order is placed
    Then order user is 'test.user1@target.com.au'

  Scenario: Placing an order with two products and free delivery.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000012 | 3   | 15    |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 75.0  | 75.0     | 0.0          |          |

  Scenario: Placing an order as a registered user with click-and-collect.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And registered user checkout
    And delivery mode is 'click-and-collect'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 35.0  | 30.0     | 5.0          |          |

  Scenario: Placing an order with POS price update occurring after product added to cart.
    The cart product price should not be affected.

    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And POS price update occurs in checkout
      | productCode | price |
      | 10000011    | 16    |
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |

  Scenario: Placing an Order with Pre Order product
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 | 1   | 400   |
    And registered user checkout
    And payment method is 'ipg'
    When pre order is placed
    And order is in status 'PARKED'
    Then no consignments are created
    And the normal sale start date is '2099-05-01T11:01:01'

  Scenario: Placing an Order with a preOrder product having SOH.
    Given set fastline stock:
      | product          | preOrder | maxPreOrder | reserved | available |
      | V1111_preOrder_1 | 0        | 100         | 0        | 0         |
    And a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 2   | 15    |
    And a registered customer goes into the checkout process
    And user checkout via spc
    And delivery mode is 'express-delivery'
    And payment method is 'ipg'
    When pre order is placed
    Then place order result is 'SUCCESS'
    And fastline stock is:
      | product          | preOrder | maxPreOrder | reserved | available |
      | V1111_preOrder_1 | 2        | 100         | 0        | 0         |

  Scenario: Placing an Order with a preOrder product having short-pick.
    Given set fastline stock:
      | product          | preOrder | maxPreOrder | reserved | available |
      | V2222_preOrder_2 | 99       | 100         | 10       | 50        |
    And a cart with entries:
      | product          | qty | price |
      | V2222_preOrder_2 | 2   | 15    |
    And a registered customer goes into the checkout process
    And user checkout via spc
    And delivery mode is 'express-delivery'
    And payment method is 'ipg'
    When pre order is placed
    Then place order result is 'SUCCESS'
    And fastline stock is:
      | product          | preOrder | maxPreOrder | reserved | available |
      | V2222_preOrder_2 | 100      | 100         | 10       | 50        |

  Scenario: Placing an Order with a preOrder product having no SOH.
    Given set fastline stock:
      | product                    | preOrder | maxPreOrder | reserved | available |
      | V3333_preOrder_maxQuantity | 100      | 100         | 0        | 0         |
    And a cart with entries:
      | product          | qty | price |
      | V2222_preOrder_2 | 2   | 15    |
    And a registered customer goes into the checkout process
    And user checkout via spc
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    When pre order is placed
    Then fastline stock is:
      | product                    | preOrder | maxPreOrder | reserved | available |
      | V3333_preOrder_maxQuantity | 100      | 100         | 0        | 0         |

  @fluentFeatureSwitch
  Scenario: Placing an Order should fail if stock insufficient from fluent
    Given products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000011 | 16 | 16 | 16 |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000011 | 16 | 1  | 16 |
    When order is submitted
    Then place order result is 'INSUFFICIENT_STOCK'

  @fluentFeatureSwitch
  Scenario: Placing an Order should pass if stock have sufficient from fluent
    Given products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000011 | 16 | 16 | 16 |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    When order is submitted
    Then place order result is 'SUCCESS'

  @fluentFeatureSwitch
  Scenario: Placing an Order with a single gift card to cart
    Given products with the following ats values in fluent:
      | product           | CC | HD | ED |
      | PGC1000_iTunes_10 | 16 | 16 | 16 |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText            |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    And guest checkout
    And payment method is 'ipg'
    When order is submitted
    Then place order result is 'SUCCESS'

  @fluentFeatureSwitch
  Scenario: Placing an Order with a single gift card to cart if insufficient from fluent.
    Given products with the following ats values in fluent:
      | product           | CC | HD | ED |
      | PGC1000_iTunes_10 | 16 | 16 | 16 |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText            |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    And guest checkout
    And payment method is 'ipg'
    And products with the following ats values in fluent:
      | product           | CC | HD | ED |
      | PGC1000_iTunes_10 | 16 | 0  | 16 |
    When order is submitted
    Then place order result is 'INSUFFICIENT_STOCK'

  @fluentFeatureSwitch
  Scenario: Placing an Order should fail if fluent API returns error
    Given products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000011 | 16 | 16 | 16 |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And the fluent client will respond with errorCode '500' and errorMessage 'Internal Server Error' for 'createFulfilmentOption'
    When order is submitted
    Then place order result is 'FLUENT_ORDER_EXCEPTION'

  Scenario: Placing an Order should fail if stock insufficient from default warehouse
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And products with the following stock exists against fastline warehouse:
      | product  | available | reserved |
      | 10000011 | 11        | 10       |
    When order is submitted
    Then place order result is 'INSUFFICIENT_STOCK'

  @notAutomated
  Scenario: Placing an Order with Pre Order product,approving order in review state by CS Agent
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 | 1   | 400   |
    And registered user checkout
    And payment method is 'ipg'
    And pre order is placed
    And the order is in 'REVIEW' state
    When the cs agent approves the order in CS Cockpit
    Then the order status is changed to 'PARKED'

  @fluentFeatureSwitch
  Scenario: Placing fluent Order status Pending with fulfiment and event response
    Given products with the following ats values in fluent:
      | product  | CC | HD | ED |
      | 10000011 | 16 | 16 | 16 |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And guest checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And fluent 'createOrder' api returns fluent id '456'
    And fluent order '456' status is 'PENDING'
    And the fluent fulfilment response is:
      | orderId | fulfilmentId | locationRef | fulfilmentType | deliveryType | status  |
      | 456     | auto234      | 7032        | HD_PFS         | STANDARD     | CREATED |
    And fluent resume order event is synced:
      | eventId | eventStatus | errorMessage |
      | 1       | SUCESS      |              |
    When order is submitted
    And fluent business process completes
    And consignment status is 'CREATED'
    Then place order result is 'SUCCESS'
    And order event sent to fluent is:
      | name         | entityType | entitySubtype |
      | RESUME_ORDER | ORDER      | HD            |


  @fluentFeatureSwitch
  Scenario: Placing an PreOrder item using fluent
    Given products with the following ats values in fluent:
      | product          | CC | HD | ED | PO  |
      | V1111_preOrder_1 | 0  | 0  | 0  | 100 |
    And a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 1   | 15    |
    And registered user checkout
    And delivery mode is 'express-delivery'
    And payment method is 'ipg'
    And ipg returns saved cards
      | cardType | cardNumber       | cardExpiry | token     | tokenExpiry | bin | defaultCard |
      | VISA     | 5123450000000346 | 01/20      | 123456789 | 01/01/2020  | 34  | true        |
    And fluent 'createPreOrder' api returns fluent id '456'
    And the fluent fulfilment response is:
      | orderId | fulfilmentId | locationRef | fulfilmentType | deliveryType | status  |
      | 456     | auto234      | 7032        | HD_PFS         | EXPRESS      | CREATED |
    When fluent preorder is placed
    Then order is in status 'PARKED'
    And place preorder result is 'SUCCESS'

  @fluentFeatureSwitch
  Scenario: Placing an PreOrder should fail if fluent API returns error
    Given products with the following ats values in fluent:
      | product          | CC | HD | ED | PO  |
      | V1111_preOrder_1 | 0  | 0  | 0  | 100 |
    And a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 1   | 15    |
    And guest checkout
    And delivery mode is 'express-delivery'
    And payment method is 'ipg'
    And the fluent client will respond with errorCode '500' and errorMessage 'Internal Server Error' for 'createFulfilmentOption'
    When fluent preorder is placed
    Then place preorder result is 'FLUENT_ORDER_EXCEPTION'
@cartProductData
Feature: Checkout - order creation fails
  In order to avoid multiple payments on carts
  We want user to be prevented from paying again when order creation fails

  Scenario Outline: Payment is captured successfully but order isn't created
    The cart with payment will remain in the system, but is removed from the session so the user
    cannot manipulate it further.
    Rather the user is sent to a new information page, and CS ticket and splunk alerts raised.

    Given a customer has provided '<Payment Method>' payment details for a cart
    And something is wrong with order creation
    When the customer tries to confirm the cart
    Then the payment is stored on the cart
    And the customer is directed to a failed transaction information page
    And a CS ticket is created
    And a Splunk alert is raised
    And the checkout cart is no longer in session
    But the checkout cart still exists in the system

    Examples: 
      | Payment Method |
      | Credit Card    |
      | PayPal         |
      | Pin Pad        |


  Scenario: Payment is captured successfully but order isn't created, and user tries to pay again
    This captures the situation where the cart has a payment but the user has managed to get back to
    the review page with the cart still in session.

    Given a customer has provided 'Credit Card' payment details for a cart
    And something is wrong with order creation
    When the customer tries to confirm the cart
    And the customer tries to confirm the cart again
    Then the payment is stored on the cart
    But a second payment is not stored on the cart
    And the customer is directed to a failed transaction information page

  Scenario: Payment is captured in REVIEW state but order isn't created, and user tries to pay again
    Given a customer has provided 'Credit Card' payment details for a cart
    And something is wrong with order creation
    And order payment goes into REVIEW state
    When the customer tries to confirm the cart
    And the customer tries to confirm the cart again
    Then the payment is stored on the cart
    But a second payment is not stored on the cart
    And the customer is directed to a failed transaction information page

  Scenario: Persistent cart exists with no payments when user logs in
    Given Persistent cart exists with no payments
    When user logs in to storefront
    Then persistent cart is loaded

  Scenario: Persistent cart exists with successful payment and order conversion failed
    Given Persistent cart exists with successful payment and order conversion failed
    When user logs in to storefront
    Then new cart is created

  Scenario: Persistent cart exists with an unsuccessful payment but no successful payment
    Given Persistent cart exists with an unsuccessful payment
    When user logs in to storefront
    Then persistent cart is loaded

@cartProductData
Feature: Pre-populate the user preferences during checkout to expedite checkout process
  In order to checkout faster 
  As a online customer I want user preferences should be pre-populated in order review page

  Scenario Outline: Customer with saved credit credit card details, Delivery mode and preferred CNC details
    Given a registered customer with a cart
    And saved preferences as
      | creditCard            | deliveryMode            | cncDetails        | defaultDeliveryAddress   |
      | <preferredCreditCard> | <preferredDeliveryMode> | <CNCDetailsSaved> | <defaultDeliveryAddress> |
    And have previous order with following details
      | paymentType   | previousDeliveryMode | deliveryAddress   |
      | <paymentType> | <deliveryMode>       | <deliveryAddress> |
    And the 'checkout.prepopulate.fields' feature switch is enabled
    When the customer navigates to checkout
    Then the customer cart is pre-populated with following details
      | creditCard           | deliveryMode         | deliveryDetails   | cNCDetails        |
      | <CreditCard-details> | <deliveryModeStatus> | <deliveryDetails> | <cncDetailsSaved> |

    Examples: 
      | preferredCreditCard | preferredDeliveryMode | CNCDetailsSaved  | paymentType | deliveryMode      | deliveryAddress                     | defaultDeliveryAddress              | CreditCard-details | deliveryModeStatus | deliveryDetails                     | cncDetailsSaved                  |
      | C00000001           | home-delivery         | Geelong,7001     | CreditCard  | home-delivery     | 8/15A,Hooker Road,Werribee,VIC,3030 | 8/15A,Hooker Road,Werribee,VIC,3030 | C00000001          | home-delivery      | 8/15A,Hooker Road,Werribee,VIC,3030 | Geelong,7001                     |
      | C00000002           | click-and-collect     | Mackay,7003      | CreditCard  | click-and-collect | N/A                                 | N/A                                 | C00000002          | click-and-collect  | N/A                                 | Mackay,7003                      |
      | C00000003           | express-delivery      | Ocean Grove,8593 | CreditCard  | express-delivery  | 180,Drake Street, Morley,WA,6062    | 180,Drake Street, Morley,WA,6062    | N/A                | N/A                | N/A                                 | N/A                              |
      | C00000001           | home-delivery         | Geelong,7001     | PayPal      | home-delivery     | 180,Drake Street, Morley,WA,6062    | N/A                                 | C00000001          | home-delivery      | N/A                                 | Geelong,7001                     |
      | C00000001           | home-delivery         | Geelong,7001     | PayPal      | home-delivery     | 180,Drake Street, Morley,WA,6062    | 180,Drake Street, Morley,WA,6062    | C00000001          | home-delivery      | 180,Drake Street, Morley,WA,6062    | Geelong,7001                     |
      | C00000002           | click-and-collect     | Mackay,7003      | PayPal      | click-and-collect | N/A                                 | N/A                                 | C00000002          | click-and-collect  | N/A                                 | 180,Drake Street, Morley,WA,6062 |
      | C00000003           | express-delivery      | Ocean Grove,8593 | PayPal      | express-delivery  | 180,Drake Street, Morley,WA,6062    | 180,Drake Street, Morley,WA,6062    | N/A                | N/A                | N/A                                 | N/A                              |
      | C00000001           | N/A                   | N/A              | PayPal      | click-and-collect | N/A                                 | N/A                                 | C00000001          | click-and-collect  | N/A                                 | N/A                              |
      | C00000002           | N/A                   | N/A              | CreditCard  | home-delivery     | 8/15A,Hooker Road,Werribee,VIC,3030 | 8/15A,Hooker Road,Werribee,VIC,3030 | C00000002          | home-delivery      | 8/15A,Hooker Road,Werribee,VIC,3030 | N/A                              |
      | N/A                 | N/A                   | N/A              | paypal      | home-delivery     | 8/15A,Hooker Road,Werribee,VIC,3030 | 8/15A,Hooker Road,Werribee,VIC,3030 | N/A                | home-delivery      | N/A                                 | N/A                              |

  Scenario: Customer with or with out saved credit credit card details and delivery address
    Given a registered customer with a cart
    And had no previous order
    And the 'checkout.prepopulate.fields' feature switch is enabled
    When the customer navigates to checkout
    Then the customer cart is not pre-populated

  Scenario: Even customers with saved details won't see any data pre-populated when the feature 'checkout.prepopulate.fields' is turned off 
    Given a registered customer with a cart
    And saved preferences as
      | creditCard | deliveryMode  | cncDetails   | defaultDeliveryAddress              |
      | C00000001  | home-delivery | Geelong,7001 | 8/15A,Hooker Road,Werribee,VIC,3030 |
    And have previous order with following details
      | paymentType   | previousDeliveryMode | deliveryAddress                     |
      | CreditCard    | home-delivery        | 8/15A,Hooker Road,Werribee,VIC,3030 |
    And the 'checkout.prepopulate.fields' feature switch is disabled
    When the customer navigates to checkout
    Then the customer cart is pre-populated with following details
      | creditCard | deliveryMode | deliveryDetails | cNCDetails |
      | N/A        | N/A          | N/A             | N/A        |
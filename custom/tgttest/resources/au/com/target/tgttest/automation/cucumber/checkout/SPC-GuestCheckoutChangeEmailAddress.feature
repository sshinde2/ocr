@cartProductData @sessionCatalogVersion
Feature: As a Target customer
  I want to be change the guest email address when I am doing the guest checkout on spc checkout page


  Scenario: Guest user change email address

    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And an anonymous user is doing checkout as guest
    And guest checkout is enabled
    When the customer changes the email to 'newguestemail@target.com.au'
    Then the customer begins checkout as guest
    And the customer will enter the checkout flow as an anonymous guest
    And the customer holds the same user details as before in checkout process


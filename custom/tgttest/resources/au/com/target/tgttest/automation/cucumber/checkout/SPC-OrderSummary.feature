@voucherData @rollback @cartProductData @sessionCatalogVersion
Feature: Single Page Checkout - Show Order Summary
  As a mobile user who is in the Single Page Checkout
  I want to see the Order Summary
  So that I know how much I have to pay

  Scenario: Voucher information displayed in cart summary
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |
    And apply voucher VOUCHER-TV1 to cart in spc
    When the order summary is displayed via spc
    Then voucher is 'VOUCHER-TV1'

  Scenario: Order summary with voucher
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |
    And apply voucher VOUCHER-TV1 to cart in spc
    When the order summary is displayed via spc
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts |
      | 10.0  | 20.0     | 0.91     | 10.0           |

  Scenario: Order summary without voucher
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |
    When the order summary is displayed via spc
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts |
      | 20.0  | 20.0     |          | 0.0            |

  Scenario: Voucher information displayed in cart detail
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |
    And apply voucher VOUCHER-TV1 to cart in spc
    When the order detail is displayed via spc
    Then voucher is 'VOUCHER-TV1'

  Scenario: Order detail with voucher
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |
    And apply voucher VOUCHER-TV1 to cart in spc
    When the order detail is displayed via spc
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts |
      | 10.0  | 20.0     | 0.91     | 10.0           |

  Scenario: Order detail without voucher
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |
    When the order detail is displayed via spc
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts |
      | 20.0  | 20.0     |          | 0.0            |

  Scenario: Order detail contains digital entries only
    Given a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                     | messageText   |
      | PGC1000_iTunes_10 | Test      | Target   | Test.Target@target.com.au | Test Checkout |
    When the order detail is displayed via spc
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts | containsDigitalEntriesOnly |
      | 10.0  | 10.0     |          | 0.0            | true                       |

  Scenario: Order detail contains both digital product and physical product
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                     | messageText   |
      | PGC1000_iTunes_10 | Test      | Target   | Test.Target@target.com.au | Test Checkout |
    When the order detail is displayed via spc
    Then cart summary is:
      | total | subtotal | totalTax | totalDiscounts | containsDigitalEntriesOnly |
      | 30.0  | 30.0     |          | 0.0            | false                      |

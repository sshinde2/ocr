Feature: Single Page Checkout - Choose credit card / gift card (IPG)
  As a mobile customer 
  I want to select the Gift Card or Credit Card payment method and see the relevant IPG payment iFrame 
  So that I can proceed with my preferred mode of payment

  @notAutomated
  Scenario Outline: 1. Select IPG payment method
    Given a target.com.au user is in the Single Page Checkout
    And the available payment methods have been displayed (as per story [OCR-14890])
    When the user chooses payment method <paymentMethod>
    Then a Google Analytics event is recorded
    And the payment method selection is saved on the cart
    And the payment information is passed to IPG (and visible in through the IPG portal)
    And the IPG session token and iframe URL are obtained
    And the other payment methods are hidden
    And the <iframe> iframe is displayed

    Examples: 
      | paymentMethod | iframe                  |
      | Gift Card     | Gift Card + Credit Card |
      | Credit Card   | Single Credit Card only |

  @notAutomated
  Scenario Outline: 2. Error obtaining the IPG session token
    Given a target.com.au user is in the Single Page Checkout
    And the available payment methods have been displayed (as per story [OCR-14890])
    And the user has chosen payment method <paymentMethod>
    And a Google Analytics event is recorded
    And the payment method selection is saved on the cart
    When there is an error obtaining the IPG session token
    Then payment method selection is cleared on the cart (including the case when there was a previously selected payment method)
    And no payment method is displayed as selected (and all available payment methods are still displayed and enabled, including the one that just had an issue)
    And no iFrame is displayed
    And error message is displayed in the Feedback style: "There was an error saving your payment method. Please try again."

    Examples: 
      | paymentMethod |
      | Gift Card     |
      | Credit Card   |

  @notAutomated
  Scenario Outline: 3. Unexpected error in back-end when attempt to save payment method
    Given a target.com.au user is in the Single Page Checkout
    And the available payment methods have been displayed (as per story [OCR-14890])
    And the user chooses payment method <paymentMethod>
    When there is an unexpected error while attempting to save the payment method in the back-end (eg. Delivery details not on the cart.)
    Then an error message is displayed in the Feedback style: "There was an error saving your payment method. Please try again."

    Examples: 
      | paymentMethod |
      | Gift Card     |
      | Credit Card   |

  Scenario Outline: 4. Select IPG payment method succesfully via spc
    Given a registered customer with a cart
    When the user chooses payment method '<paymentMethod>' via spc
    Then the payment method selection is saved on the cart
    And the IPG session token and iframe URL are obtained

    Examples: 
      | paymentMethod |
      | giftcard      |
      | ipg           |

  Scenario Outline: 5. Fail to select IPG payment method via spc
    Given a registered customer with a cart
    And the user chooses payment method '<existingPayment>' via spc
    When the user chooses payment method '<paymentMethod>' via spc and failed
    Then payment method selection is empty on the cart

    Examples: 
      | existingPayment | paymentMethod |
      |                 | giftcard      |
      | giftcard        | ipg           |

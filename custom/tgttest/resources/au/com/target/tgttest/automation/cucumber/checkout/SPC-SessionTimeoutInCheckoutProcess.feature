@notAutomated
Feature: Session timeout in checkout process
  As a target online store
  I want to keep the user on the checkout page when session timeout
  
  checkout details - Detail information user input in the checkout process like delivery mode, 
  delivery address, payment address, vouchers/flybuys, TMD card

  Scenario: Login user stays on the SPC page after session timeout
    Given a soft login user
    And the user goes into checkout process with password as a login user
    When use interacts with the SPC page after user session timeout
    Then the user stays on the SPC page
    And the user is asked to enter the password
    And the user has the cart of soft login user
    And the user has all previous items in the cart
    And the checkout details are cleaned up

  Scenario: Soft login user checkout as guest stays on the SPC page after session timeout
    Given a soft login user
    And the user goes into checkout process without password as a guest user
    And the user has the cart with a temporary guest user
    When use interacts with the SPC page after user session timeout
    Then the user stays on the SPC page
    And the user is asked to enter the password
    And the user has the cart of soft login user
    And the user has all previous items in the cart
    And the checkout details are cleaned up
    And the temporary guest user for checkout is removed

  Scenario: Anonymous user guest checkout stays on the SPC page after session timeout
    Given an anonymous user
    And the user goes into checkout process without password as a guest user
    When use interacts with the SPC page after user session timeout
    Then the user stays on the SPC page
    And the user is asked to enter the password
    And the user has the cart of anonymous user
    And the user has all previous items in the cart
    And the checkout details are cleaned up
    And the temporary guest user for checkout is removed

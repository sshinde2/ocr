@notAutomated
Feature: Single Page Checkout - Segment customers as Mobile or Desktop
  As a online customer I want to go to single checkout page if I meet the segmentation requirement

Background:
Given feature 'spc.login.and.checkout' is enabled

  Scenario Outline: Navigate to checkout
    Given the user is on the Basket page
    And has products in their Basket
    And the device they are using is <device>
    And the Sales Channel is <salesChannel>
    When the user chooses to checkout securely
    Then the version of Checkout that the user is taken to is <checkOut>

    Examples: 
      | device  | salesChannel | checkOut             |
      | mobile  | Web          | Single Page Checkout |
      | mobile  | MobileApp    | Single Page Checkout |
      | desktop | Web          | Legacy               |
      | tablet  | Web          | Legacy               |
      | tablet  | MobileApp    | Legacy               |

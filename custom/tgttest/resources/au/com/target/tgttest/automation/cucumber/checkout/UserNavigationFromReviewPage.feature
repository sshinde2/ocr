@notAutomated
Feature: User navigation from order review page to other checkout pages during checkout
  In order to provide better navigation
  A customer can able to navigate to the previous checkout pages from order review page

  Scenario: Customer is on order review page and can navigate to payment details page by clicking on flybuys button
    Given A registered user on order review page through standard checkout process
    When user clicks on flybuys button
    Then user should navigate to payment details page

  Scenario: Customer is on order review page and can navigate to payment details page by clicking on promo/discount button
    Given A registered user on order review page through standard checkout process
    When user clicks on promo/discount button
    Then user should navigate to payment details page

  Scenario: Customer is on order review page and can navigate to payment details page by clicking on flybuys button
    Given A registered user on order review page through expedite checkout process
    When user clicks on flybuys button
    Then user should navigate to payment details page

  Scenario: Customer is on order review page and can navigate to payment details page by clicking on promo/discount button
    Given A registered user on order review page through expedite checkout process
    When user clicks on promo/discount button
    Then user should navigate to payment details page

  Scenario: Customer is on order review page and can navigate to payment details page by clicking on promo/discount button
    Given A guest user on order review page through standard checkout process
    When user clicks on promo/discount button
    Then user should navigate to payment details page

  Scenario: Customer is on order review page and can navigate to payment details page by clicking on flybuys button
    Given A guest user on order review page through standard checkout process
    When user clicks on flybuys button
    Then user should navigate to payment details page

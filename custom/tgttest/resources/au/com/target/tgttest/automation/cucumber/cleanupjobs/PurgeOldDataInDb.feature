@notAutomated
Feature: Clean up jobs to purge old data that is filling up the data base.
  
  In order to have a cleaned environment 
  As a online store 
  I want to clean up old log entries in batch.

  Background: 
    Given email clean up job threshold time is set to 10 minutes
    And stock Level History clean up job threshold time is set to 15 minutes
    And Order Process clean up job threshold is set to 20 minutes
    And the number of items per batch is set to 10

  Scenario: Purging the sent email message log entries
    Given sent email message log entries
      | sentEmailMessageId | createdTime | attachments |
      | 1111               | now         | Yes         |
      | 1112               | now-10 mins | Yes         |
      | 1113               | now-11 mins | No          |
      | 1114               | now-19 mins | No          |
      | 1115               | now-9 mins  | Yes         |
      | 1116               | now-15 mins | Yes         |
      | 1117               | now-20 mins | Yes         |
      | 1118               | now-2 mins  | Yes         |
      | 1119               | now-1 mins  | Yes         |
      | 1120               | now-15 mins | Yes         |
      | 1121               | now         | No          |
    When email message clean job is initiated
    Then email message log entries are
      | sentEmailMessageId | createdTime | attachments |
      | 1111               | now         | Yes         |
      | 1115               | now-9 mins  | Yes         |
      | 1118               | now-2 mins  | Yes         |
      | 1119               | now-1 mins  | Yes         |
      | 1121               | now         | No          |
    And any attachments related to the entries should be removed

  Scenario: Cleaning up stock level History Entries
    Given stock Level History entries
      | stocklevelHistoryEntryId | createdTime |
      | 1122                     | now         |
      | 1123                     | now-10 mins |
      | 1124                     | now-11 mins |
      | 1125                     | now-9 mins  |
      | 1126                     | now-9 mins  |
      | 1127                     | now-15 mins |
      | 1128                     | now-20 mins |
      | 1129                     | now-21 mins |
      | 1130                     | now-21 mins |
      | 1131                     | now-25 mins |
    When stock Level History clean up job in initiated
    Then stock Level History entries are
      | stocklevelHistoryEntryId | createdTime |
      | 1122                     | now         |
      | 1123                     | now-10 mins |
      | 1124                     | now-11 mins |
      | 1125                     | now-9 mins  |
      | 1126                     | now-9 mins  |

  Scenario: cleaning up the Order process attachments
    Given Order process entries with order status
      | orderProcessCode | createdTime | orderStatus    | orderProcessTasksLogs |
      | auto0000001      | now-10 mins | Completed      | 2                     |
      | auto0000002      | now-10 mins | Rejected       | 4                     |
      | auto0000003      | now-21 mins | Review         | 1                     |
      | auto0000004      | now-22 mins | Cancel         | 2                     |
      | auto0000005      | now-20 mins | Invoiced       | 3                     |
      | auto0000006      | now-10 mins | Review on hold | 2                     |
      | auto0000007      | now-14 mins | In progress    | 2                     |
      | auto0000008      | now-30 mins | Pending        | 1                     |
      | auto0000009      | now-23 mins | Created        | 4                     |
      | auto0000010      | now-24 mins | In progress    | 0                     |
      | auto0000011      | now-25 mins | Completed      | 3                     |
      | auto0000012      | now-25 mins | Rejected       | 2                     |
    When the Order process cleanup job is initiated
    Then Order process entries
      | orderProcessCode | createdTime | orderStatus    | orderProcessTasksLogs |
      | auto0000001      | now-10 mins | Completed      | 2                     |
      | auto0000002      | now-10 mins | Rejected       | 4                     |
      | auto0000003      | now-21 mins | Review         | 1                     |
      | auto0000005      | now-20 mins | Invoiced       | 3                     |
      | auto0000006      | now-10 mins | Review on hold | 2                     |
      | auto0000007      | now-14 mins | In progress    | 2                     |
      | auto0000008      | now-30 mins | Pending        | 1                     |
      | auto0000009      | now-23 mins | Created        | 4                     |
      | auto0000010      | now-24 mins | In progress    | 2                     |
    And Order Process Tasks logs should be removed

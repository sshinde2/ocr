@notAutomated
Feature: Showing display-only products within the Recently Viewed corousel
Scenario: Show find in store for display only products
	Given that customer is in listing/category/search results/PDP/checkout pages
	When customer has recently viewed a display only product
	Then display the find in store icon on the product card in the recently viewed section

Scenario: Show find in store when the product has no online stock but has consolidated store stock
	Given that customer is in listing/category/search results/PDP/checkout pages
	When customer has recently viewed a product that has no online stock (for any of its variants) but has consolidated store stock
	Then display the find in store icon on the product card in the recently viewed section

Scenario: Redirect to PDP
	Given that customer is viewing a product in recently viewed component
	When the find in store icon is selected
	Then redirect customer to store tab in PDP
	And if a preferred store is set, load variants based on the store stock else display find in store icon
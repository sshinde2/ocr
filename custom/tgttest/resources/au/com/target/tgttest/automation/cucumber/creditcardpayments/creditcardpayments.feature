@wip
Feature: Credit Card Payments
In order to purchase products
As a customer
I need to use my credit card in the checkout process

  Scenario: Placing an Order with Credit Card as Registered User
    Given a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
      | 10001030 | 2   | 15    |
      | 10000710 | 3   | 8     |
    And registered user checkout
    And payment method is 'credit card'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 73.0  | 64.0     | 9.0          | 6.64     |
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000911 | 1   | 1000  | 1000      |
      | 10001030 | 2   | 1500  | 1500      |
      | 10000710 | 3   | 800   | 800       |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 7300
    And tlog tender type is 'eft'
    And tlog credit card payment info matches
    And Accertify feed has :
    | paymentType | cardAuthorizedAmount |
    | CreditCard  | 73.00                |


  Scenario: When placing an Order with Credit Card as registered user, payment details are saved in Customer's account by default
    Given any cart
    And registered user checkout
    And payment method is 'credit card'
    When order is placed
    Then payment details are stored in customer account

  Scenario: When placing an Order with Credit Card as registered user, payment details are not saved in Customer's account when checkbox is not checked
    Given any cart
    And registered user checkout
    And payment method is 'credit card'
    When customer does not want to save the payment details
    When order is placed
    Then payment details are not stored in customer account


  Scenario: Placing an Order with Credit Card using the saved credit card
    Given any cart
    And registered user checkout
    And saved credit card in customer account
    When customer selects saved credit card
    And order was placed
    Then place order result is 'SUCCESS'

  Scenario: Placing an Order with Credit Card Payment when Payment Gateway is down
    Given payment gateway is down
    And a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
    And registered user checkout
    And payment method is 'credit-card'
    When order is placed
    Then place order result is 'PAYMENT_FAILURE'

  Scenario Outline: Placing an Order with Credit Card Payment when TNS Response is TimeOut/Review
    Given payment gateway response is <response>
    And a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
    And registered user checkout
    And payment method is 'credit-card'
    When order is placed
    Then place order result is 'Pending'
    Examples:
    | response |
    | Timeout  |
    | Review   |

  Scenario: Placing an Order after retry of Pending transaction
    Given a credit card Order in Pending status
    And payment gateway is up
    When payment request retries
    Then place order result is 'SUCCESS'

  Scenario: Order Cancelled after retry of Pending transaction
    Given a credit card Order in Pending status
    And payment gateway is down
    When payment request retries
    Then order staus is 'Cancelled'


  Scenario: Fulfill a home delivery order with credit card payment
    Given an order with 'credit-card'
    When Order is fulfilled
    Then order is:
    | orderStatus | consignmentStatus | invoice   |
    | Completed   | Shipped           | generated |

Scenario: Placing an order with Credit Card after a payment failure
    Given a cart with failed 'creditcard' payment
    When order is placed
    Then place order result is 'SUCCESS'
    And Accertify feed has :
      | totalFailedCCAttempts | paymentType |
      | 1                     | CreditCard  |

@notAutomated
Feature: Customer FeedBack  CMS Component

  Background: 
    Given the customer feedback container has <feedbackItem>
    And the feedback has visibility set to <visible>
    And has the component restriction <prodRestriction> ,<timeRestriction> ,<catRestriction>,<brandRestriction>
      | feedbackItem | visible | prodRestriction | timeRestriction | catRestriction  | brandRestriction |
      | FB01         | yes     | none            | none            | none            | none             |
      | FB02         | yes     | none            | none            | none            | none             |
      | FB03         | No      | none            | none            | none            | none             |
      | FB04         | yes     | Product A only  | none            | none            | none             |
      | FB05         | yes     | none            | applied         | none            | none             |
      | FB06         | yes     | none            | none            | Category B only | none             |
      | FB07         | yes     | none            | none            | none            | Brand C only     |

  Scenario: Add generic feedback component to Landing page template
    Given a CMS user
    And the user is editing the home page
    When the user adds the generic feedback (TargetFeedbackContainer) component to the Customer Feedback slot on the page
    Then the component is saved on the template in the CMS

  Scenario: Add feedback item to feedback container component
    Given the feedbackcontainer component is added in the slot
    When the user adds a feedbackItem to the feedback component
    Then the feedback item has the following attributes
      | attribute         | type   |
      | title             | String |
      | description       | String |
      | exiturl           | String |
      | featureKey        | String |
      | minDelay          | Number |
      | maxDelay          | Number |
      | trafficPercentage | Number |
      | nextEligibility   | Number |
      | minUse            | Number |
      | includeUrls       | String |
      | excludeUrls       | String |
      | minTime           | Number |
    And the feedback item has been added

  Scenario: Place multiple feedback items in the feedback component
    Given the user add more feedback items to the feedback component
    When the user moves the feedbackitems
    Then the user will be able to place the feedbackitems based on the priority

  Scenario: Edit feedback item
    Given the customerfeedbackcontainer has feedback item
    When the user click on the edit button on the feedbackitem
    Then the user is able to edit the attributes of an existing feedback item

  Scenario: Remove feedbackItem
    Given the customerfeedbackcontainer has feedback item
    When the user clicks on the delete button
    Then the feedbackItem should be removed from the customerfeedbackcontainer component

  Scenario: Hide feedbackItem
    Given the customerfeedbackcontainer has feedback item
    When the user changes the visible attribute of the item to 'no'
    Then the feedbackItem is hidden from the customer

  Scenario Outline: Display the feedback prompt
    Given the customer feedback slot has configured with <feedbackItems>
    When customer navigates to the page <page>
    Then the feedbackitem displayed is <displayedItems>
    And the feedback items array in the div tag displays <divItems>
    And the feedback prompt has title,description,'Give feedback' button and close button

    Examples: 
      | feedbackItems | page                                             | divItems   | displayedItem |
      | FB01          | any                                              | FB01       | FB01          |
      | FB01,FB02     | any                                              | FB01,FB02  | FB01          |
      | FB02,FB01     | any                                              | FB02,FB01  | FB01          |
      | None          | any                                              | none       | none          |
      | FB03          | any                                              | none       | none          |
      | Fb03,FB02     | any                                              | FB02       | FB02          |
      | FB04,FB01     | PDP for product A                                | FB01       | FB01          |
      | FB05,FB01     | any page and within FB05 time restriction        | FB05, FB01 | FB05          |
      | FB05,FB01     | any page and outside FB05 time restriction       | FB01       | FB01          |
      | FB06, FB01    | Category page for category B                     | FB06, FB01 | FB06          |
      | FB06, FB01    | any page other than Category page for category B | FB01       | FB01          |
      | FB07, FB01    | PDP for product with Brand C                     | FB01,FB01  | FB07          |
      | FB07, FB01    | any page other than brand c                      | FB01       | FB01          |

@notAutomated
Feature: Customer FeedBack prompt displayed in webpage

  Scenario: Displayed feedback prompt on target site
    Given a Target Online Store user
    And the customer  user agent is <userAgent>
    When they have navigated to a page
    And feedback item <itemName> is the highest priority feedback items that qualifies for the user on that page
    Then the  prompt for feedback item  is <promptDisplay>
    And the feedback prompt has title,description,'Give Feedback' button , 'Id rather not link' link and close button
      | userAgent           | promptDisplay |
      | Desktop web browser | displayed     |
      | Kiosk               | not displayed |
      | iOsApp              | not displayed |
      | AndroidApp          | not displayed |

  Scenario: customer clicks on Give Feedback
    Given the feedback prompt  has been displayed  customer in webpage
    When the customer clicks 'Give feedback' button
    Then the user should be taken to the survey page (Exit URL) for feedback item A in a different tab
    And a Participate Google Analytics event should be tracked for the feedback item

  Scenario: customer dismisses the feedback prompt
    Given the feedback prompt  has been displayed for customer in webpage
    When the user selects either the Close button or I'd rather not link
    Then the feedback prompt is hidden
    And a Participate Google Analytics event should be tracked for the feedback item

  Scenario: customer does not action feedback prompt
    Given the feedback prompt  has been displayed for customer in webpage
    And the feedback prompt or icon (on mobile web) for feedback item has been displayed
    When the customer does not interact with the prompt
    And scrolls the page
    Then the prompt or icon (on mobile web) should continue to display at the bottom of the screen

  Scenario Outline: Check global exclusion period before displaying prompt
    Given a Target Online customer landed on target website
    And the customer feedback container is configured with feedback items
    And the highest priority feedback item for that user on that page is <priorityItem>
    And the the configured 'Global exclusion (days)' to display feedback prompts is <globalExclusion>
    And the most recent interaction by this customer with any other feedback prompt was <mostRecent>
    When the page is rendered
    And the feedback item is NOT excluded by any other rules
    Then the feedback item is <display>

    Examples: 
      | priorityItem | globalExclusion | mostRecent | display       |
      | FB01         | 0               | any        | displayed     |
      | FB01         | blank           | any        | displayed     |
      | FB01         | any             | none       | displayed     |
      | FB01         | 2               | <2 days    | not displayed |
      | FB01         | 2               | >2 days    | displayed     |

  Scenario Outline: Only prompt once for a particular feedback survey
    Given a Target Online Store user
    And they have navigated to a page
    And the highest priority feedback item for that user on that page is <priorityItem>
    And the user has previously seen the prompt for that particular feedback item
    And at that time the action taken by the user was <prevAction>
    When the page is rendered
    And the feedback item is NOT excluded by any other rules
    Then the feedback item is <display>

    Examples: 
      | priorityItem | prevAction     | display       |
      | FB01         | Give feedback  | not displayed |
      | FB02         | dismiss        | not displayed |
      | FB03         | I'd rather not | not displayed |
      | FB04         | ignore         | displayed     |

  Scenario Outline: Check minimum session age before displaying prompt
    Given a Target Online customer landed on target website
    And the customer feedback container is configured with feedback items
    And the highest priority feedback item for that user on that page is <priorityItem>
    And the the configured Min session age to display feedback prompts is <minAge>
    And the users tab has been active for <sessionLength>
    When the page is rendered
    And the feedback item is NOT excluded by any other rules
    Then the feedback item is <displayStatus>

    Examples: 
      | priorityItem | minAge | sessionLength | displayStatus |
      | FB01         | 0      | any           | displayed     |
      | FB01         | blank  | any           | displayed     |
      | FB01         | 5      | <5 minutes    | not displayed |
      | FB01         | 5      | >5 minutes    | displayed     |

  Scenario Outline: Feedback prompt page inclusions and exclusions
    Given a Target Online Store user
    And they have navigated to page <page>
    And the highest priority feedback item for that user on that page is <priorityItem>
    And the user has NOT previously interacted with that particular feedback item
    And the feedback item has an 'Include URLs' value of <includeURLs>
    And the feedback item has an 'Exclude URLs' value of <excludeURLs>
    When the page is rendered
    And the feedback item is NOT excluded by any other rules
    Then the feedback item is <display>

    Examples: 
      | page                     | priorityItem | includeURLs | excludeURLs | display       |
      | any page                 | FB01         | blank       | blank       | displayed     |
      | any product page         | FB02         | /p          | blank       | displayed     |
      | any other page           | FB02         | /p          | blank       | not displayed |
      | any product page         | FB03         | blank       | /p          | not displayed |
      | any other page           | FB03         | blank       | /p          | displayed     |
      | any product page         | FB04         | /p,/c       | blank       | displayed     |
      | any category page        | FB04         | /p,/c       | blank       | displayed     |
      | any other page           | FB04         | /p,/c       | blank       | not displayed |
      | any product page         | FB05         | blank       | /p,/c       | not displayed |
      | any category page        | FB05         | blank       | /p,/c       | not displayed |
      | any other page           | FB05         | blank       | /p,/c       | displayed     |
      | any womens category page | FB06         | /c          | /c/women    | not displayed |
      | any other category page  | FB06         | /c          | /c/women    | displayed     |
      | any other page           | FB06         | /c          | /c/women    | not displayed |

  Scenario Outline: Apply traffic percentage for a feedback survey prompt
    Given a Target Online Store user
    And they have navigated to a page
    And the highest priority feedback item for that user on that page is <priorityItem>
    And that feedback item has a configured TrafficPercentage of <trafficpercentage>
    When the page is rendered
    And the feedback item is NOT excluded by any other rules
    And a decision HAS NOT previously been made for this user re whether to display this feedback item
    Then the feedback item is <display>

    Examples: 
      | priorityItem | trafficpercentage | display                               |
      | FB01         | 100               | displayed                             |
      | FB02         | 0                 | not displayed                         |
      | FB03         | 50                | displayed with 50 percent probability |

  Scenario Outline: Retain previous decision of percentage when display feedback item
    Given a Target Online Store user
    And they have navigated to a page
    And the highest priority feedback item for that user on that page is <priorityItem>
    And that feedback item has a configured TrafficPercentage of <percentage>
    When the page is rendered
    And the feedback item is NOT excluded by any other rules
    And a decision HAS previously been made for this user re whether to display this feedback item
    And that decision was to <prevDisplayDecision> the feedback item
    And if it was displayed then user previously ignored the prompt
    Then the feedback item is <display>

    Examples: 
      | priorityItem | percentage | prevDisplayDecision | display       |
      | FB03         | 50         | display             | displayed     |
      | FB03         | 50         | NOT display         | NOT displayed |

  Scenario Outline: Feedback prompt page device restrictions
    Given a Target Online Store user
    And the user agent is <userAgent>
    And they have navigated to a page
    And the highest priority feedback item for that user on that page is <priorityItem>
    And the user has NOT previously interacted with that particular feedback item
    And the feedback item has an device attribute with  value of <devices>
    When the page is rendered
    And the feedback item is NOT excluded by any other rules
    Then the feedback item is <display>

    Examples: 
      | userAgent       | priorityItem | devices | display       |
      | mobile browser  | FB01         | blank   | displayed     |
      | desktop browser | FB01         | blank   | displayed     |
      | Mobile app      | FB01         | mobile  | not displayed |
      | Mobile app      | FB01         | Desktop | not displayed |
      | desktop browser | FB02         | Desktop | displayed     |
      | mobile browser  | FB02         | Desktop | not dispalyed |
      | mobile browser  | FB02         | mobile  | dispalyed     |
      | ipad browser    | FB02         | Desktop | displayed     |
      | ipad browser    | FB02         | Mobile  | not displayed |

  Scenario Outline: Prompt again for a particular feedback survey after a period
    Given a Target Online Store user
    And they have navigated to a page
    And the highest priority feedback item for that user on that page is <priorityItem>
    And the NextEligibility figure configured for the feedback item is <nextEligibility> days
    And the period since the user last interacted with the feedback item is <lastSeenDays> days
    When the page is rendered
    And the feedback item is NOT excluded by any other rules
    Then the feedback item is <display>

    Examples: 
      | priorityItem | nextEligibility | lastSeenDays | display       |
      | FB01         | blank           | any          | not displayed |
      | FB02         | 2               | < 2          | not displayed |
      | FB02         | 2               | >= 2         | displayed     |
      | FB02         | 2               | never        | displayed     |
      | FB03         | 0               | any          | not displayed |

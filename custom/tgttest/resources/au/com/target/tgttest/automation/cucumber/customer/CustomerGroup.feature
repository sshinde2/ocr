@customerData
Feature: Customer Account Group Activities
  In order to capture the customers using different sales channel
  , as Target I want to add them to be added to configured customer group.

  Scenario: Customer doesnt have target account and register for one using mobile app
    Given that the customer doesn't have a Target account
    When the customer creates a new account and logs in through 'MobileApp'
    Then the customer account is created and is assigned to the 'mobileappcustomergroup'

  Scenario: Customer has a Target customer account but not associated to mobile app group and logs in 
    Given that the customer is already a Target customer
    When the customer logs in through 'MobileApp'
    Then the customer account is associated to the 'mobileappcustomergroup'


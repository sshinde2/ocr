 @notAutomated
Feature: 
  As a member of the Target Marketing team
  I want to know the original source of a record in ExactTarget
  so that I can ascertain the channel this customer used to sign-up

  Scenario: New customer signing up for newsletter using quick signup
    Given that a new customer with the subscription details
      | Email          |
      | John@gmail.com |
    And sales application is 'web'
    And the customer record does not exist in ExactTarget records
    When the customer subscribes to newsletter
    Then the source field in ExactTarget should be populated with 'newsletter'

  Scenario: New customer signing up to newsletter using quick signup
    Given that a new customer  with the subscription details
      | Title | First Name | Email          |
      | Mr    | John       | John@gmail.com |
    And sales application is 'web'
    And the customer record does not exist in ExactTarget records
    When the customer subscribes to newsletter
    Then the source field in ExactTarget should be populated with 'newsletter'

  Scenario: Existing customer signing up for newsletter
    Given that an existing customer  with the subscription details
      | Email          |
      | John@gmail.com |
    And sales application is 'web'
    When the customer subscribes to newsletter
    Then the source field in ExactTarget should not be changed

  Scenario: Existing customer signing up for newsletter
    Given that an existing customer  with the subscription details
      | Title | First Name | Email          |
      | Mr    | John       | John@gmail.com |
    And sales application is 'web'
    When the customer subscribes to newsletter
    Then the source field in ExactTarget should not be changed

  Scenario: New customer signing up to newsletter using wifi
    Given that a new customer with the subscription details
      | Email          |
      | John@gmail.com |
    And sales application is 'web'
    When the customer subscribes to newsletter using 'instore wifi'
    Then the source field in ExactTarget should be populated with 'wifi'

  Scenario: New customer signing up to new account while checkout
    Given that a new customer is signing up for new account while online checkout
    And this customer does not exist in ET records
    And the sales application is 'web'
    When the customer subscribes
    Then the source field in ExactTarget should be populated with 'webCheckout'

  Scenario: New customer signing up to new account via Account creation
    Given that a new customer is signing up for newsletter while creating an account
    And this customer does not exist in ET records
    And the sales application is 'web'
    When the customer subscribes
    Then the source field in ExactTarget should be populated with 'webAccount'

  Scenario: New customer signing up to new account while checkout using kiosk
    Given that a new customer is signing up for new account while online checkout
    And this customer does not exist in ET records
    And the sales application is 'kiosk'
    When the customer subscribes
    Then the source field in ExactTarget should be populated with 'kiosk'

  Scenario: New customer signing up to new account via Account creation using kiosk
    Given that a new customer is signing up for newsletter while creating an account
    And this customer does not exist in ET records
    And the sales application is 'kiosk'
    When the customer subscribes
    Then the source field in ExactTarget should be populated with 'kiosk'

  Scenario: New customer signing up to quick newsletter using kiosk
    Given that new customer with the subscription details
      | title | First Name | Email          |
      | Mr    | John       | John@gmail.com |
    And this customer does not exist in ET records
    And the sales application is 'kiosk'
    When the customer subscribes
    Then the source field in ExactTarget should be populated with 'kiosk'

  Scenario: New customer signing up to newsletter using kiosk
    Given that new customer with the subscription details
      | Email          |
      | John@gmail.com |
    And this customer does not exist in ET records
    And the sales application is 'kiosk'
    When the customer subscribes
    Then the source field in ExactTarget should be populated with 'kiosk'

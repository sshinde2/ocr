@wip
Feature: 
  As a Digital customer who has not joined MumsHub 
  I want to provide some of my personal details
  so that I can get a personalized mums hub experience

  Background: 
    Given customer personal details subscription retry interval is set to 2 times every 20000 milliseconds

  Scenario: Existing customer signs up to Mumshub
    Given an exact target account exists for customer 'test@gmail.com'
    And customer with personal details
      | Title | FirstName | LastName | Email | Duedate | Birthday   | Gender | BabyGender |
      | Mr    | John      | James    |       |         | 05/05/1989 | Female |            |
    And customer child details
      | ChildName | ChildBirthday | ChildGender |
      | Charles   | 20/03/2011    | Boy         |
    When customer subscribes to Mumshub
    Then customer personal details subscription process outcome is 'success'

  Scenario: New customer signs up to Mumshub
    Given an exact target account does not exists for customer 'test2@gmail.com'
    And customer with personal details
      | Title | FirstName | LastName | Email | Duedate    | Birthday   | Gender | BabyGender |
      | Mr    | John      | James    |       | 05/05/2016 | 05/05/1989 | Female | Girl       |
    And customer child details
      | ChildName | ChildBirthday | ChildGender |
      |           |               |             |
    When customer subscribes to Mumshub
    Then customer personal details subscription process outcome is 'success'

  Scenario: Customer subscribes to Mumshub when webmethod or ExactTarget is offline
    Given webmethods is offline for 20 seconds
    And an exact target account exists for customer 'John@gmail.com'
    And customer with personal details
      | Title | FirstName | LastName | Email | Duedate | Birthday   | Gender | BabyGender |
      | Mr    | John      |          |       |         | 05/05/1989 | Female |            |
    And customer child details
      | ChildName | ChildBirthday | ChildGender |
      |           |               |             |
    When customer subscribes to Mumshub
    Then customer personal details subscription process outcome is 'success'

 @wip
  Scenario: Customer subscription retry entries exceeded when webmethod is offline
    Given webmethods is offline for 100 seconds
    And an exact target account exists for customer 'John@gmail.com'
    And customer with personal details
      | Title | FirstName | LastName | Email          | Duedate | Birthday   | Gender | BabyGender |
      | Mr    | John      |          | John@gmail.com | N/A     | 05/05/1989 | Female | N/A        |
    And customer child details
      | ChildName | ChildBirthday | ChildGender |
      |           |               |             |
    When customer subscribes to Mumshub
    And customer personal details subscription process fails with retry exceeded

  @notAutomated
  Scenario: Invalid session or session expired
    Given the customer is on the update page for longer than 30 minutes
    When the user submits the page
    Then user is redirected to the MumsHub homepage
    And customer gets the message 'For security reasons, your session has expired. You should receive a welcome email with a link to complete your profile'

  @notAutomated
  Scenario: Splunk Alert when user information does not reach ET
    Given the system contains valid data that failed to reach ExactTarget
    When the system has had 5 unsuccessful retry attempts
    Then a Splunk alert is triggered
    And a P3 incident is raised to Production Support
    And the Email Address and Business Process ID is contained within the subject line

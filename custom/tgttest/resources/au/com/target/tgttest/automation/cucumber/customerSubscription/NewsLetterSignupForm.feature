@wip
Feature: Newsletter signup form
  In order to get provide better shopping experience
  As Marketing
  I want to be able to capture customer details when they sign up to newsletter   
  The form will be hosted on Target infrastructure to replace the iframe to ExactTarget 
  Kiosks will be updated as well
  Exact Target integration failure scenarios are mocked using TgtTinker

  Background: 
    Given customer subscription retry interval is set to 2 times every 15000 milliseconds

  Scenario: Valid new email address used to signup to newsletter
    Given customer with subscription details:
      | Email           | Title | FirstName | LastName |
      | ankit@gmail.com | Mr    | Ankit     | Kumar    |
    When customer subscribes to newsletter 
    And customer gets message for subscription 'success'

  @notAutomated
  Scenario: customer subscribes to Newsletter only with email address
    Given customer with subscription details:
      | Email           | Title | FirstName | LastName |
      | ankit@gmail.com | Mr    |           |          |
    When customer subscribes to newsletter
    Then customer gets message for subscription 'error'

  Scenario: Valid new email address used and webmethod is down then Hybris business process captures the email address successfully
    Given customer with subscription details:
      | Email        | Title | FirstName | LastName |
      | mj@gmail.com | Mrs   | Mary      | Jones    |
    And webmethods is offline for 20 seconds
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
    And customer subscription process outcome is 'success'

  Scenario: Valid new email address used and provider is offline then Hybris business process captures the email address successfully
    Given customer with subscription details:
      | Email        | Title | FirstName | LastName |
      | mj@gmail.com | Mrs   | Mary      | Jones    |
    And provider is offline for 10 seconds
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
    And customer subscription process outcome is 'success'

  Scenario: existing subscriber tries to subsribe to enews
    Given customer is already subsribed
    When customer subscribes to newsletter
    And customer gets message for subscription 'success'

  @wip
  Scenario: hybris retry entries exceeded
    Given webmethods is offline for 100 seconds
    When customer subscribes to newsletter
    Then customer subscription business process fails with retry exceeded
    And customer gets message for subscription 'success'

  @notAutomated
  Scenario: Valid new email address used to signup to newsletter creates UID in ExactTarget DB and Hybris DB
    Given customer details are:
      | Email        | Title | First Name | Last Name |
      | ak@gmail.com | Ms    | Jemma      | Machin    |
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
    And this record will contain a unique subscriber key in ExactTarget Consolidated customer database
    And this new record will mark this email address as subscribed to newsletter in ExactTarget Consolidated customer database

  @notAutomated
  Scenario: Existing email address which is not subscribed to newsletter used to signup to newsletter
    Given customer with details:
      | Email        | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | ak@gmail.com | Ms    | Jemma      | Machin    | N                  | N                     |
    When user subscribes to newsletter
    Then the ExactTarget data will be updated to:
      | Email        | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | ak@gmail.com | Ms    | Jemma      | Machin    | N                  | Y                     |
    And customer subscription process outcome is 'success'
    And customer gets message for subscription 'success'

  @notAutomated
  Scenario: Existing email address which is already subscribed to newsletter used to signup to newsletter with a different first name, last name and title
    Given customer with details:
      | Email        | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | mj@gmail.com | Mrs   | Mary       | Jones     | N                  | Y                     |
    When the user submits details:
      | Email        | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | mj@gmail.com | Mrs   | Mary-Ann   | Jonas     | N                  | Y                     |
    Then the database will be updated to:
      | Email        | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | mj@gmail.com | Mrs   | Mary       | Jones     | N                  | Y                     |
    And customer gets message for subscription 'success'

  @notAutomated
  Scenario: Existing email address which not subscribed to newsletter used to signup to newsletter with a different first name, last name and title
    Given customer with details:
      | Email        | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | mj@gmail.com | Mrs   | Mary       | Jones     | N                  | N                     |
    When the customer submits details:
      | Email        | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | mj@gmail.com | Mrs   | Mary-Ann   | Jonas     | N                  | Y                     |
    Then the database will be updated to:
      | Email        | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | mj@gmail.com | Mrs   | Mary       | Jones     | N                  | Y                     |
    And customer gets message for subscription 'success'

  @notAutomated
  Scenario Outline: Invalid email address is used to sign to newsletter
    Given customer with <Email Address> details
    When the customer submits <Email Address> details
    Then customer gets subscription <Error Message>

    Examples: 
      | Email      | Title | First Name | Last Name | Error Message |
      | @gmail.com | Mr    | Ankit      | Kumar     | error         |
      | mj@.com    | Mrs   | Mary       | Jones     | error         |
      | gmail.com  | Ms    | Jemma      | Machin    | error         |

  @notAutomated
  Scenario Outline: Exact Target integration failure scenarios using TgtTinker
    Given customer with details:
      | Email            | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | test@example.com | Mr    | TestFirst  | TestLast  | Y                  | N                     |
    And tgttinker mock response is <Response Type>
    When customer subscribes to newletter
    Then customer subscription process is <outcome>

    Examples: 
      | Responce code | Response Message | Response Type | outcome |
      | 1             | Test unavailable | unavailable   | error   |
      | 1             | Test invalid     | invalid       | error   |
      | 1             | Test success     | success       | success |

  @notAutomated
  Scenario: Splunk alerts are correctly displayed
    Given WM is offline
    When Hybris request to WM fails
    Then Splunk log has alert event
      | Put the alert log format text here |

  @notAutomated
  Scenario: Splunk logging messages are correctly displayed
    When hybris sent request to WM
    Then Splunk log has event message
      | Put the event log format text here |

  @notAutomated
  Scenario: Front end validation on each input field of form
    Given newsletter form submitted
    And data entered in the form
    When customer submits form
    Then mandatory field check validation will be done

  @notAutomated
  Scenario: T&C's will be updated
    When newsletter form displayed to the customer
    Then updated T&C will be displayed to the customer

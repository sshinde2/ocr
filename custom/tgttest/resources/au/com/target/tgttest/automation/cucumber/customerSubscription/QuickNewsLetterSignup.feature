@notAutomated
Feature: Quick Newsletter signup
  In order to get provide better shopping experience
  As Marketing
  I want to be able to capture customer email details when they sign up to newsletter
  
  Kiosks will be updated as well
  Exact Target integration scenarios are mocked using TgtTinker

  Scenario: Valid new email address used to signup to newsletter
   Given customer with subscription details:
      | Email                    |
      | targetTest56@example.com |
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'

  Scenario: Valid new email address used to signup to newsletter creates UID in WM and ExactTarget DB
   Given customer with subscription details:
      | Email                    | Newsletter subscriber | Subscription Message |
      | targetTest57@example.com | N                     | success              |
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
    And this record will contain a unique subscriber key in ExactTarget Consolidated customer database
    And this new record will mark this email address as subscribed to newsletter in ExactTarget Consolidated customer database

  Scenario: Existing email address that has signedup flag set to yes in ET is used to signup to newsletter
    Given customer with details:
      | Email                    | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | targetTest55@example.com | Mr    | Ankit      | Kumar     | Y                  | Y                     |
    When user subscribes to newsletter
    Then the ExactTarget data will be not be updated:
      | Email                    | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | targetTest55@example.com | Mr    | Ankit      | Kumar     | Y                  | Y                     |
    And customer subscription process is created
    And customer gets message for subscription 'success'

  Scenario: Existing email address that has not signed up used to signup to newsletter
    Given customer with details:
      | Email                    | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | targetTest58@example.com | Mr    | Ankit      | Kumar     | Y                  | N                     |
    When user subscribes to newsletter
    Then the ExactTarget data will be updated:
      | Email                    | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | targetTest58@example.com | Mr    | Ankit      | Kumar     | Y                  | Y                     |
    And customer subscription process created
    And customer gets message for subscription 'success'

  Scenario Outline: Invalid email address is used to sign to newsletter
    Given customer with <Email Address> details
    When the customer submits <Email Address> details
    Then customer gets subscription <Error Message>

    Examples: 
      | Email Address | Error Message |
      | @gmail.com    | error         |
      | mj@.com       | error         |
      | gmail.com     | error         |

  Scenario: Valid new email address used and WM is down then Hybris business process captures the email address successfully
    Given customer email address is:
      | Email Address | Title | First Name | Last Name | Newsletter subscriber |
      | mj@gmail.com  | Mrs   | Mary       | Jones     | N                     |
    And WM is offline
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
    And customer subscription process is created

  Scenario: Valid new email address used and ET is down then Hybris business process captures the email address successfully
    Given customer email address is:
      | Email Address | Title | First Name | Last Name | Newsletter subscriber |
      | mj@gmail.com  | Mrs   | Mary       | Jones     | N                     |
    And ET is offline
    When customer subscribes to newsletter
    Then customer gets message for subscription 'success'
    And customer subscription process is created

  Scenario Outline: Exact Target integration scenarios using TgtTinker
    Given customer with details:
      | Email            | Title | First Name | Last Name | MumsHub subscriber | Newsletter subscriber |
      | test@example.com | Mr    | TestFirst  | TestLast  | Y                  | N                     |
    And tgttinker mock response is <Response Type>
    When customer subscribes to newletter
    Then customer subscription process is <outcome>

    Examples: 
      | Response code | Response Message | Response Type | outcome |
      | 1             | Test unavailable | unavailable   | error   |
      | 1             | Test invalid     | invalid       | error   |
      | 1             | Test success     | success       | success |

  Scenario: Splunk alerts are correctly displayed
    Given WM is offline
    When Hybris request to WM fails
    Then Splunk log has alert event
      | Put the alert log format text here |

  Scenario: Splunk logging messages are correctly displayed
    When hybris sent request to WM
    Then Splunk log has event message
      | Put the event log format text here |

  Scenario: Front end validation on each input field of form
    Given newsletter form submitted
    And data entered in the form
    When customer submits form
    Then mandatory field check validation will be done

  Scenario: T&C's will be updated
    When newsletter form displayed to the customer
    Then updated T&C will be displayed to the customer

@notAutomated
Feature: Display payment details in cs cockpit
  As a CS team member
  I want to view the payment details
  So that I can verify that the transaction matches the customer

  Scenario: Display credit card details in payment transaction
    Given payment has been attempted for an order using credit card
    When the payment transaction entry is displayed in CS cockpit
    Then the credit card details will be displayed

  Scenario: Display gift card details in payment transaction
    Given payment has been attempted for an order using giftcard
    When the payment transaction entry is displayed in CS cockpit
    Then the giftcard details will be displayed (cart type, masked number and billing address)

  Scenario: Display credit card details and billing address on Refund
    Given a refund has been attempted
    When the refund is displayed in CS cockpit
    Then the credit card details will be displayed (refer notes below for details)

  Scenario: Display giftcard card details and billing address on Refund
    Given a refund has been attempted
    When the refund is displayed in CS cockpit
    Then the gift card details will be displayed (cart type, masked number and billing address)

  Scenario: Display payment details in payment details box
    Given payment has been attempted for an order via credit cards and gift cards (multi-card)
    When the payment details box is displayed in CS cockpit
    Then no details are displayed (cart type, masked number and billing address)

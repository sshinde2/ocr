Feature: Deals Available for Mobile
  In order to provide customers with better visibility of deals and offers on mobile
   As a Customer
  	I want to retrieve all the mobile deals, to get a consolidated and detailed view of applicable deals on mobile.

  Scenario: Fetching all Deals which are available for Mobile
    Given the following deals are setup in the system with following details:
      | dealId | availableForMobile | longTitle             | offerHeading  | enabled | startDate |featured|
      | 9102   | true               | "Long Title for 9102" | baby          | false   | past      | true   |
      | 101    | true               | "Long Title for 101"  | entertainment | true    | past      | false  |
      | 9104   | true               |                       | toys,womens   | false   | past      | true   |
      | 103    | false              | "Long Title for 103"  | kids          | true    | past      | false  |
      | 104    | true               | "Long Title for 104"  | lifestyle     | true    | past      | false  |
      | 9107   | true               | "Long Title for 9107" | lifestyle     | false   | future    | false  |
    When the deals applicable for mobile are being fetched
    Then the retrieved data will contain the following deals:
      | dealId | availableForMobile  | longTitle             | offerHeading  | enabled | startDate | featured |
      | 9102   | true                | "Long Title for 9102" | baby          | false   | past      | true     |
      | 101    | true                | "Long Title for 101"  | entertainment | true    | past      | false    | 
      | 9104   | true                |                       | toys,womens   | false   | past      | true     |
      | 104    | true                | "Long Title for 104"  | lifestyle     | true    | past      | false    |
    But will not contain the following deals with id:
      | 103    |
      | 9107   |

  Scenario: Fetching all the mobile offer headings for customers so that they can filter and view based on different offer headings
    Given the following mobile offer headings are setup in the system with following details:
      | womens        |
      | mens          |
      | kids          |
      | baby          |
      | home          |
      | entertainment |
      | toys          |
      | lifestyle     |
      | beauty        |
    When the deals applicable for mobile are being fetched
    Then the retrieved data will contain the following mobile offer headings:
      | code          | name          | colour    |
      | womens        | WOMEN         | '#F74552' |
      | mens          | MEN           | '#69CCE6' |
      | kids          | KIDS          | '#F27D00' |
      | baby          | BABY          | '#C782D1' |
      | home          | HOME          | '#7878B0' |
      | entertainment | ENTERTAINMENT | '#57D4CC' |
      | toys          | TOYS          | '#F0CA0F' |
      | lifestyle     | LIFESTYLE     | '#BAD405' |
      | beauty        | BEAUTY        | '#FC7AB0' |

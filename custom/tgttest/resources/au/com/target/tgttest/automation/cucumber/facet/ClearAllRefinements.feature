@notAutomated 
Feature: "Clear All" button to clear refinements
  As a target online user
  I need a clear all call to action
  so that I can clear all selected facets/refinements in one go.

  Scenario: Display of "Clear All" button in the Refine menu in Mobile 
    Given I'm a target online user browsing a product listing page on mobile
    When I select a refinement from the refine menu
    Then I can see the "Clear All" button at the bottom of the refine menu
    And it is STICKY

  Scenario: Display of "Clear All" button in the Desktop
    Given I'm a target online user browsing a product listing page on desktop
    When I select a refinement from the left facets menu
    Then I can see the "Clear All" button below all the left facet options
    And it is NOT STICKY 

  Scenario: "Clear All" button action
    Given I'm a target online user browsing a product listing page on desktop
    And I select a refinement from the left facets menu
    When I tap on the "Clear All" button
    Then all the selections in the left facets menu are cleared
    And the accordions which were open for selection, remains open
    And the facet counts are updated
    And "Clear All" button is no longer visible

  Scenario: "Clear All" button is not shown unless any refinements are selected
    Given I'm a target online user browsing a product listing page on desktop
    When no refinements are selected
    Then the "Clear All" button is not shown
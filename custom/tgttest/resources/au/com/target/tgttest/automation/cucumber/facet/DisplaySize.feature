@notAutomated
Feature: Display correct size groups and order on product listing page

  Scenario: Multiple Size Group
    Given Product size group data as:
      | sizeGroupPosition | sizeGroup | sizePosition | size   |
      | 2                 | men       | 1            | small  |
      | 1                 | women     | 3            | large  |
      | 3                 | kids      | 2            | medium |
      | 3                 | kids      | 1            | small  |
    And The size ordering feature switch is on
    When Navigate to the product listing page
    Then Results should contain a displaySize facet
    And The first size group is 'women'
    And The second size group is 'men'
    And The third size group is 'kids'
    And The size group  should have a label
    And The sizes within the group should be displayed in the correct order:
      | sizeGroup | orderedSizes |
      | women     | large        |
      | men       | small        |
      | kids      | small,medium |

  Scenario: Single Size Groups
    Given Product size group data as:
      | sizeGroup | sizePosition | size   |
      | kids       | 2            | medium |
      | kids       | 1            | small  |
      | kids       | 3            | large  |
    And The size ordering feature switch is on
    When Navigate to the product listing page
    Then Results should contain a displaySize facet
    And The sizes within the group should be displayed in the correct order:
      | sizeGroup | sizes              |
      | kids      | small,medium,large |
    And the size group will not be displayed

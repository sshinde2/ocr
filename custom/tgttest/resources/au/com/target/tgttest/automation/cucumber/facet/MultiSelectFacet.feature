@notAutomated 
Feature: Selected facets should be shown along with available facets 
    As a target online user
    I need to see selected facets on a product listing page along with the available facets
    so that the search experience is easier for selecting and unselecting factes

  Scenario: checking items in facet in Desktop
    Given I'm browsing a product listing page as a target online user
    When I check multiple refinements in a multiselect facet
    Then all selected items stays along with the available refinements
    And the selected items DO NOT appear under the breadcrumb

  Scenario: Un-checking of items in the Facet in Desktop
    Given I'm browsing a product listing page as a target online user
    And refinements have been selected from multiselect facets
    When I click on the selected refinements
    Then it gets un-checked

  Scenario: Multi-selecting the attributes in facets 
    When I am browsing a product listing page as a target online user 
    Then I can see the option to multi-select certain facets such as 'colour' using check-boxes 

  Scenario: Behavior of the Check-boxes in Desktop only 
    Given I am browsing a product listing page as a target online user 
    And I can see the option to multi-select certain facets such as 'colour' using check-boxes 
    When I click on a facet value such as 'red' 
    Then the product list is filtered approriately 
    And the selected value's checkbox becomes checked 
    And clicking on it again removes the filter 

  Scenario: empty facets are dropped off on selection 
    Given I am browsing a product listing page as a target online user 
    And I can see the option to multi-select certain facets such as 'colour' using check-boxes 
    When I click on a facet value such as 'red' 
    Then facets with empty listings are dropped off 
    And Only the <Matching Attributes> exists 
        | Existing facet options | Selection    | Matching Attributes |
        | Categories             |              | Dropped off         |
        | Brands                 | Mickey Mouse | Brands              |
        | Colour                 |              | Colour              |
        | Size                   |              | Size                |
        | Price                  |              | Dropped off         |
        | Offers                 |              | Dropped off         |
        | Rating                 |              | Dropped off         |
        | Range                  |              | Dropped off         |
        | Delivery Options       |              | Dropped off         |

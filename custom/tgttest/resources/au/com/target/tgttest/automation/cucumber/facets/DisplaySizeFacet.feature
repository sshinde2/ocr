@notAutomated
Feature: Neat view of selected facets from a display size dimension on the product list page
  As a target online customer 
  I want to view the selected facets neatly with my size selection on the product listing page
  So that customer can view the selected facets clearly

  Background: 
    Given user is on product list page for a Category or Brand Page or Search term
    And 'Display Size' facet is enabled for that Category or Brand Page or Search term

  Scenario: Choose the 'Display Size' filter with single size
    Given 'Display Size' facet is displayed
    When user chooses a size within that facet
    Then the selected size will appear in the top left of the page
    And the name of the selected facets will be only the size name and will not include size group

  Scenario: Choose the 'Display Size' filter with multiple sizes
    Given 'Display Size' facet is displayed
    When user chooses multiple sizes within that facet
    Then those selected sizes will appear in the top left of the page
    And the name of the selected facets will be only the size name and will not include size group

  Scenario: De-select one of the sizes of Display Size facet from the Selected Facets 
    Given 'Display Size' facet is displayed
    And one size is selected by the user from 'Display Size' facet
    When user deselects a size from Selected Facets
    Then size will not be displayed in the top left of the page

  Scenario: De-select multiple sizes of Display Size facet from the Selected Facets 
    Given 'Display Size' facet is displayed
    And multiple size is selected by the user from 'Display Size' facet
    When user deselects multiple sizes from Selected Facets
    Then those sizes will disappear in the top left of the page

  Scenario: Choose the mulitple filters
    Given 'Display Size' facet is displayed
    And 'Colour' facet is displayed
    When user chooses a size within the facet 'Display Size' and a colour within the facet 'Colour'
    Then both the facets should be updated
    And the size and colour will appear in the top left of the page
    And the size name of the selected facets will be only the size name and will not include size group
    And the colour look the same as the colour that user selected
    And other facets should not be affected

@notAutomated
Feature: Listing Page Design

  Scenario Outline: Listing Page Select Box
    Given A Target Customer is browsing the Target website
    And feature switch "ui.newFacetDisplay" is set to <featureStatus>
    When I am using <device>
    Then The element 'refine-form' has the class <className> applied
    And The element 'refine-form' does not have the class <excludedClass> applied

    Examples: 
      | featureStatus | device  | className            | excludedClass        |
      | enabled       | desktop | 'sort-options-multi' | 'chosen-select'      |
      | enabled       | mobile  | 'sort-options-multi' | 'chosen-select'      |
      | enabled       | kiosk   | 'chosen-select'      | 'sort-options-multi' |
      | disabled      | desktop | 'chosen-select'      | 'sort-options-multi' |
      | disabled      | mobile  | 'chosen-select'      | 'sort-options-multi' |
      | disabled      | kiosk   | 'chosen-select'      | 'sort-options-multi' |
  
  Scenario Outline: Pre-Order Guided Navigation Menu option is displayed successfully
  	Given there are products present in the system
  	And a customer logged into the Taget online system
  	When this customer lands on the Refinement screen
	Then the 'Pre-order' facet should be on the top after the 'Available Online'


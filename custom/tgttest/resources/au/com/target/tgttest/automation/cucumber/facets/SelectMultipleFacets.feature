@notAutomated
Feature: 
  Display of Subcategories & Headings as an accordion

  Scenario Outline: Display of Subcategories & Headings as an accordion
    Given A Target Customer is browsing the Target website
    When The customer looks at all the fonts in the <section>
    Then The font is "Cocogoose"

    Examples: 
      | Section                          |
      | Left Facet Category              |
      | Page Title                       |
      | Number of items under page title |

  Scenario: Sub categories to display on left, and not scroll
    Given A Target Customer browsing the Target Website
    When The customer clicks on a top-level category
    Then The customer can see all sub-categories of that top-level category sitting on the left
    And The sub-categories are bold
    And There is no overflow

  Scenario: Facets that are not category display in an accordion
    Given A Target Customer browsing the Target Website
    When The customer clicks on a top-level category
    Then The left-hand Facets that are not category are displayed in an Accordion

  Scenario: Accordion is able to be expanded
    Given A Target Customer browsing a Product Listing Page
    And The Target Customer sees an accordion in the left hand navigation
    When The customer clicks on a closed accordion heading
    Then The accordion will expand to display its content

  Scenario: Accordion is able to be collapsed
    Given A Target Customer browsing a Product Listing Page
    And The Target Customer sees an accordion in the left hand navigation
    When The customer clicks on an opened accordion heading
    Then The accordion will collapse hiding its content

  Scenario: Refinements are remembered when the popup is closed on mobile
    Given A Target Customer is on the Listing Page of the Target website
    And The customer is browsing via Mobile
    And The customer has gone into the Refine section
    And The customer has refined their listings via either Colour; Size or Price
    When The customer goes back into the Refine section
    Then The same refinements are already open for them

  Scenario: Facets that have a refinement applied are open when page loads
    Given A Target Customer is on the Listing Page of the Target website
    And The customer applies a refinement to my selection
    When The customer refresh the page
    Then The accordions that have a refinement selected are opened
    And Accordions that do not have a refinement are closed

  Scenario: Accordion with an accordion remains open, by default
    Given A Target Customer is browsing a High Level Category at the Target website
    When The customer sees into an Accordion, which has multiple sub-Accordions
    Then That particular Accordion is already open with sub-Accordions, by default

  Scenario: Refreshing of the Product screen in a Desktop
    Given I'm a target online user browsing a product listing page on desktop
    And The customer has opened the Left Accordion
    When The customer selects an item in the Left Accordion
    Then The products in the product grid are reloaded
    And The page is not reloaded

  Scenario: Refreshing of the Product screen in a Mobile
    Given I'm a target online user browsing a product listing page on mobile
    And I have tapped the refine button
    When I select a refinement from the refine menu
    Then The products in the product grid are reloaded
    When I click on the Red Rondel for View Items
    Then I can see the new filtered products

  Scenario: Selecting of items in Facet in Desktop
    Given I'm a target online user browsing a product listing page on desktop
    And I select a refinement from the left facets menu
    When I select multiple options in the facet
    Then The total count of available options in each facet is animated
    And The number of items gets updated underneath the Page Category

  Scenario: Deselecting of items in the Facet in Desktop
    Given I'm a target online user browsing a product listing page on desktop
    And I have opened a facet
    And The facet <facet> has multiple options already selected
    When I click on the selected option <option>
    Then The option <option> is deselected
    And The total count of available options in each facet is animated
    And The number of items gets updated underneath the Page Category

  Scenario: Checking of items in the Facet in Mobile
    Given I'm a target online user browsing a product listing page on mobile
    And I have opened a facet
    When I select multiple options in the facet
    Then The total count of available options in each facet is animated
    Then The products in the product grid are reloaded
    And The products do not appear on top of the Facet
    And The number of items gets updated in the Red Rondel

  Scenario: Un-Checking of items in the Facet in Mobile
    Given I'm a target online user browsing a product listing page on mobile
    And I have an opened facet <facet>
    And The facet <facet> has multiple options already selected
    When The customer click on the selected option <option>
    Then The option <option> is deselected
    And The total count of available options in each facet is animated
    And The number of items gets updated in the Red Rondel

Feature: As the Target Online Store 
    I want to send up-to-date details for all stores to Fluent each day
    So that the Locations details used in Fluent for Available To Sell (ATS) and In Store Fulfilment (ISF) are up-to-date
    and for use in initially loading Locations into Fluent

Scenario: Sending stores update to Fluent as a batch succeeds 
    Given the job to update store details in Fluent has been configured in the batch/chron scheduler 
    And the fluent client will respond with responseId '1' for 'createJob'
    And the fluent client will respond with responseId '2' for 'createBatch'
    When the locations feed job is initiated 
    Then fluent createJob api is invoked with 
        | name      | retailerId |
        | LOCATION_ | 1          |
    And fluent createBatch api is invoked with 
        | action | entityType |
        | UPSERT | LOCATION   |
    And the job result is 'SUCCESS' 
    And the job status is 'FINISHED' 
    And the jobId '1' & batchId '2' are logged in 'LocationFluentBatchResponse' 

Scenario: Sending stores update to Fluent as a batch fails 
    Given the job to update store details in Fluent has been configured in the batch/chron scheduler 
    And the fluent client will respond with errorCode '500' and errorMessage 'Internal Server Error' for 'createJob'
    When the locations feed job is initiated 
    Then fluent createJob api is invoked with 
        | name      | retailerId |
        | LOCATION_ | 1          |
    And the job result is 'FAILURE' 
    And the job status is 'FINISHED' 

Scenario Outline: Check status of locations batch 
    Given there exists a jobResponse with jobId '1', batchId '1' and status '<oldStatus>' 
    And batchView api will respond with status '<newStatus>' 
    When the location feed status check job is initiated 
    And the jobResponse status is updated to '<newStatus>' for jobId '1', batchId '1'
    And the job result is 'SUCCESS' 
    And the job status is 'FINISHED' 

    Examples: 
        | oldStatus | newStatus |
        | PENDING   | PENDING   |
        | PENDING   | RUNNING   |
        | PENDING   | COMPLETE  |
        | RUNNING   | RUNNING   |
        | RUNNING   | COMPLETE  |

@rollback @cartProductData
Feature: Flybuys - Applying flybuys discount
  In order to redeem points, as a customer I want to apply a flybuys discount to my cart.
  story for the tofix is OCR-13773

  @toFix
  Scenario: customer selects option for 2000 points
    Given Customer has available points '120000'
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |
    When the customer selects option for 2000 points
    Then summary shows flybuys discount:
      | dollerAmt | points |
      | 10        | 2000   |

  Scenario: customer lowers the cart value and add more items increase cart value
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 5   | 5     |
    And the customer redeems 2000 flybuys points
    And the customer decreases cart value below 10 dollars
    And customer adds items increasing cart value to 25 dollars
    When the customer redeems 2000 flybuys points
    Then cart total is 30.0

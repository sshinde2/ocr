@rollback @cartProductData
Feature: Flybuys - remove Flybuys card
  In order to provide better customer satisfaction
  As Online Store
  I want to provide means to remove flybuys points

  Background: 
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 35    |
    When customer presents flybuys card '6008943218616910' in spc
    Then cart contains flybuys number '6008943218616910'

  Scenario: Customer removes flybuys number from cart
    When customer removes flybuys in spc
    Then cart does not contain flybuys number

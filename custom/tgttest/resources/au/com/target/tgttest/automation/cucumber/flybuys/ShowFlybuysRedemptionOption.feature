@rollback @cartProductData
Feature: Flybuys - View flybuys redemption options
  In order to control the flybuys redemption options, 
  as Target online store we can set configurable max redeemable amount
  and options are limited by order value and points balance.

  Background: 
    Given flybuys config max redeemable amount is 50
    And a registered customer goes into the checkout process
    And customer has valid flybuys number presented
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 8   | 6     |
    And Customer has available points '11000'
    And the customer logins flybuys in spc
    And the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 40        | 8000   | DUMMYREDEEMCODE4 |
      | 30        | 6000   | DUMMYREDEEMCODE3 |
      | 20        | 4000   | DUMMYREDEEMCODE2 |
      | 10        | 2000   | DUMMYREDEEMCODE1 |
    And the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE1'

  Scenario: Customer clicks change button to fetch the redemption options
    When the customer selects change button to fetch the flybuys redemption options
    Then the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 40        | 8000   | DUMMYREDEEMCODE4 |
      | 30        | 6000   | DUMMYREDEEMCODE3 |
      | 20        | 4000   | DUMMYREDEEMCODE2 |
      | 10        | 2000   | DUMMYREDEEMCODE1 |

  Scenario: Customer clicks change button to fetch the redemption options after decreases cart value below 10 dollars
    Given the customer decreases cart value below 10 dollars
    When the customer selects change button to fetch the flybuys redemption options
    Then the change flybuys redemption response constains flybuys canRedeemPoints 'false'

  Scenario: Customer clicks change button to fetch the redemption options after decreases cart value below 20 dollars
    Given the customer decreases cart value below 20 dollars
    When the customer selects change button to fetch the flybuys redemption options
    Then the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 10        | 2000   | DUMMYREDEEMCODE1 |

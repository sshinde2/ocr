@notAutomated
Feature: Masking of flybuys number for security 
    As a Target Online user
    I want to see my Flybuys number masked in tax invoice and order pages
    so that my flybuys number is not compromised

  Scenario: Mask Flybuys code in the Tax Invoice
    Given as a target online user I place an order
    And I have entered my Flybuys number while placing the order
    When my order is ready for shipment
    Then I receive an email which has the tax invoice attached
    And my flybuys number in the tax invoice is masked

  Scenario: Mask Flybuys code in Review Your Order page (Legacy/Kiosk)
    Given as a target online user I place an order
    And I have entered my Flybuys number while placing the order
    When I am on the Review Your Order Page
    And my flybuys number in the page is masked

  Scenario: Mask Flybuys code in Thank You page
    Given as a target online user I place an order
    And I have entered my Flybuys number while placing the order
    When I am on the Thank You Page
    Then my flybuys number in the page is masked

  Scenario: Mask Flybuys code in My Order Details page
    Given as a target online user I place an order
    And I have entered my Flybuys number while placing the order
    And I've completed placing the order
    When I browse to My Account Page > My Order Details
    Then my flybuys number in the page is masked

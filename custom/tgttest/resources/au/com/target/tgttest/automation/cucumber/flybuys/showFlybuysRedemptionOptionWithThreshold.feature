@rollback @cartProductData
Feature: Flybuys - View flybuys redemption options
  In order to control the flybuys redemption options, 
  as Target online store we can set configurable max redeemable amount
  and options are limited by order value and points balance.
  There is one flybuysCartThresholdLimit properties with default value 1,
  which means the order sub total plus threshould limit should be great than flybuys redemption dollar amount

  Scenario: show redemption options up to order value - flybuys 20 dollar option is excluded because of 1 dollar threshold
    Given flybuys config max redeemable amount is 50
    And a registered customer goes into the checkout process
    And customer has valid flybuys number presented
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 10    |
    And selected delivery mode in spc is 'home-delivery'
    And cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      | 29.0  | 20.0     | 0              | 9            |
    And Customer has available points '11000'
    And the customer logins flybuys in spc
    And the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 10        | 2000   | DUMMYREDEEMCODE1 |
    When the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE1'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      | 19.0  | 20.0     | 10.0           | 9.0          |

  Scenario: show redemption options up to order value - flybuys 20 dollar option is displayed
    Given flybuys config max redeemable amount is 50
    And a registered customer goes into the checkout process
    And customer has valid flybuys number presented
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 11    |
    And selected delivery mode in spc is 'home-delivery'
    And cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      | 31.0  | 22.0     | 0              | 9            |
    And Customer has available points '11000'
    And the customer logins flybuys in spc
    And the customer will be presented with the redemption options in spc
      | dollarAmt | points | code             |
      | 20        | 4000   | DUMMYREDEEMCODE2 |
      | 10        | 2000   | DUMMYREDEEMCODE1 |
    When the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE2'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      | 11.0  | 22.0     | 20.0           | 9.0          |


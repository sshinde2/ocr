@cleanOrderData @cleanConsignmentData @cartProductData
Feature: Fulfilment for CNP consignments

  Scenario: When cancel recieved for a consignments which are confirmed by warehouse
    Given consignments for warehouse 'CnpWarehouse' are:
      | consignmentCode | consignmentStatus      | createdDate        | deliveryMethod | bulky |
      | autocnp000011   | CONFIRMED_BY_WAREHOUSE | yesterday at 08:00 | home-delivery  | true  |
    When cancel is recieved from warehouse for the consignment 'autocnp000011'
    Then consignment status is 'CANCELLED'
    And reject reason is 'Zero_Pick_By_Warehouse'

  Scenario: When cancel recieved for a consignments which are sent to warehouse and not confirmed
    Given consignments for warehouse 'CnpWarehouse' are:
      | consignmentCode | consignmentStatus | createdDate        | deliveryMethod | bulky |
      | autocnp000012   | SENT_TO_WAREHOUSE | yesterday at 08:00 | home-delivery  | true  |
    When cancel is recieved from warehouse for the consignment 'autocnp000012'
    Then consignment status is 'CANCELLED'
    And reject reason is 'Zero_Pick_By_Warehouse'

  Scenario: When consignment complete is recieved for a consignments which are sent to warehouse and not confirmed
    Given consignments for warehouse 'CnpWarehouse' are:
      | consignmentCode | consignmentStatus      | createdDate        | deliveryMethod | bulky |
      | autocnp000012   | CONFIRMED_BY_WAREHOUSE | yesterday at 08:00 | home-delivery  | true  |
    When complete is recieved from warehouse for the consignment:
      | consignmentCode | shipDate           | trackingId | boxes | carrier |
      | autocnp000012   | yesterday at 12:00 | dummyTrkId | 2     | TOLL    |
    Then consignment status is 'SHIPPED'

  Scenario: ResendNotAcknowledgedOrderExtractsCronjob resends the consignment when the consignment was sent to warehouse failed and reach the RESENDORDEXTRACTTIMEOUT which is 60 mins.
    Given consignments for warehouse 'CnpWarehouse' are:
      | consignmentCode | consignmentStatus | createdDate        | deliveryMethod | bulky |
      | autocnp000013   | CREATED           | yesterday at 08:00 | home-delivery  | true  |
    When cronjob resendNotAcknowledgedOrderExtractsJob is trigged
    And webmethods returns 'SUCCESS' for send to warehouse
    Then consignment status is 'SENT_TO_WAREHOUSE'

  Scenario: ResendNotAcknowledgedOrderExtractsCronjob does not resend the consignment when the consignment was sent to warehouse failed but does not reach the RESENDORDEXTRACTTIMEOUT which is 60 mins.
    Given consignments for warehouse 'CnpWarehouse' are:
      | consignmentCode | consignmentStatus | createdDate | deliveryMethod | bulky |
      | autocnp000013   | CREATED           | now         | home-delivery  | true  |
    When cronjob resendNotAcknowledgedOrderExtractsJob is trigged
    And webmethods returns 'SUCCESS' for send to warehouse
    Then consignment status is 'CREATED'
    
  Scenario: Customer Service agent fully cancels an order assigned to CNP Warehouse
    Given consignments for warehouse 'CnpWarehouse' are:
      | consignmentCode | consignmentStatus      | createdDate        | deliveryMethod | bulky |
      | autocnp000014   | CONFIRMED_BY_WAREHOUSE | yesterday at 08:00 | home-delivery  | true  |
    When the order is fully cancelled with the reason 'CUSTOMERREQUEST'
    Then consignment status is 'CANCELLED'
    And reject reason is 'Cancelled_By_Customer'
    And extract is not sent to warehouse

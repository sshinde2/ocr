@cartProductData @ntlFulfilmentData
Feature: Assign consignments to the correct warehouse
  
  As the target online trading manager 
  
  I want to include the CNP dropship warehouse into the routing rules 
  
  So that when an order is received from CNP and created in Hybris the consignment is assigned to the correct warehouse
  
  Notes: Following data is setup via impex:
    NTLs with status and stock:
      | ntl          | enabled | stock                                       |
      | NSWONLINEBNB | yes     | 2 of 10000411, 0 of 10000510, 5 of 10000021 |
      | QLDONLINEBNB | yes     | 1 of 10000411, 1 of 10000510, 1 of 10000021 |

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 100       |
      | 10000012 | 0        | 100       |
      | 10000021 | 0        | 100       |
      | 10000022 | 0        | 100       |
      | 10000111 | 0        | 100       |
      | 10000112 | 0        | 100       |
      | 10000121 | 0        | 100       |
    And set CNP stock:
      | product  | reserved | available |
      | 10000323 | 0        | 100       |
      | 10000324 | 0        | 100       |
      | 10000325 | 0        | 100       |
      | 10000328 | 0        | 100       |
    And set Incomm gift cards in stock:
      | product                | available |
      | PGC1000_iTunes_10      | 100       |
      | PGC1014_venue_coffee   | 100       |
      | PGC1014_venue_cocktail | 100       |

  Scenario: consignment successfully sent to cnp
    Given a cart with entries:
      | product  | qty | price |
      | 10000324 | 4   | 15    |
      | 10000328 | 2   | 15    |
    And delivery mode is 'home-delivery'
    And the delivery address is '160, Terrace Place, Murarrie, QLD, 4172'
    When order is placed
    Then a single consignment is created
    And the assigned warehouse will be 'Cnp'
    And consignment carrier is 'Toll'
    And consignment is sent to webmethods with:
      | consignmentId      | warehouseId  | orderId      | customerFirstName | customerLastName |
      | NEW_CONSIGNMENT_ID | CnpWarehouse | NEW_ORDER_ID | firstname         | lastname         |
    And dropship consignment entries are sent to webmethods with:
      | productId | name                         | ean          |
      | 10000324  | Test bulky product 4 black M | 00357357     |
      | 10000328  | Test bulky product 4 grey M  | 817980010536 |

  Scenario: Stock available for cart with Home Delivery at fulfilment in CNP warehouse
    Given a cart with entries:
      | product  | qty | price |
      | 10000323 | 2   | 15    |
      | 10000324 | 4   | 15    |
    And delivery mode is 'home-delivery'
    And the delivery address is '160, Terrace Place, Murarrie, QLD, 4172'
    When order is placed
    Then a single consignment is created
    And the assigned warehouse will be 'Cnp'
    And CNP stock is:
      | product  | reserved | available |
      | 10000323 | 2        | 100       |
      | 10000324 | 4        | 100       |

  Scenario: Stock available for cart with Home Delivery at fulfilment in fastline and CNP warehouse
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000325 | 4   | 15    |
    And delivery mode is 'home-delivery'
    And the delivery address is '180, Drake Street, Morley, WA, 6062'
    When order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'fastline'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 100       |
    And a consignment is assigned to warehouse 'CNP'
    And CNP stock is:
      | product  | reserved | available |
      | 10000325 | 4        | 100       |

  Scenario: Mixed cart with giftcard and CNP Product
    Given a cart with entries:
      | product  | qty | price |
      | 10000324 | 3   | 12    |
    And a gift card is added to cart with recipient details:
      | productCode          | firstName | lastName | email                         | messageText   |
      | PGC1014_venue_coffee | Debashish | Sethy    | debashish.sethy@uxcoxygen.com | Test Checkout |
    And delivery mode is 'home-delivery'
    When order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And a consignment is assigned to warehouse 'Cnp'
    And CNP stock is:
      | product  | reserved | available |
      | 10000324 | 3        | 100       |
    And incomm stock is:
      | product              | reserved | available | warehouseCode   |
      | PGC1014_venue_coffee | 0        | 100       | IncommWarehouse |

  Scenario: Mix cart, Stock available for cart with HD at fulfilment enabled NTL
    Given a cart with entries:
      | product  | qty | price |
      | 10000323 | 4   | 12    |
      | 10000411 | 2   | 15    |
      | 10000021 | 2   | 10    |
    And delivery mode is 'home-delivery'
    And the delivery address is '8/15, Helles Ave, Moorebank, NSW, 2170'
    When order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'CNP'
    And a consignment is assigned to warehouse 'NSWONLINEBNB'
    And CNP stock is:
      | product  | reserved | available |
      | 10000323 | 4        | 100       |
    And NTL stock is:
      | product  | reserved | available | warehouseCode |
      | 10000411 | 2        | 2         | TollNSW       |
      | 10000021 | 2        | 5         | TollNSW       |
    And fastline stock is:
      | product  | reserved | available |
      | 10000411 | 0        | 100       |
      | 10000021 | 0        | 100       |
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 100       |
      | 10000012 | 0        | 100       |
      | 10000021 | 0        | 100       |
      | 10000022 | 0        | 100       |
      | 10000111 | 0        | 100       |
      | 10000112 | 0        | 100       |
      | 10000121 | 0        | 100       |
    And set CNP stock:
      | product  | reserved | available |
      | 10000323 | 0        | 100       |
      | 10000324 | 0        | 100       |
      | 10000325 | 0        | 100       |
    And set Incomm gift cards in stock:
      | product                | available |
      | PGC1000_iTunes_10      | 100       |
      | PGC1014_venue_coffee   | 100       |
      | PGC1014_venue_cocktail | 100       |

  Scenario: Mixed cart with a Giftcard, CNP Product and Fastline Product and Accertify response is Review.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
      | 10000323 | 1   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText   |
      | PGC1000_iTunes_10 | Debashish | Sethy    | debashish.sethy@uxcoxygen.com | Test Checkout |
    And delivery mode is 'home-delivery'
    And the order would trigger an Accertify response of 'REVIEW'
    When order is placed
    Then order is in status 'REVIEW'
    And 0 consignments are created
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 1        | 100       |
    And CNP stock is:
      | product  | reserved | available |
      | 10000323 | 1        | 100       |
    And incomm stock is:
      | product           | reserved | available | warehouseCode   |
      | PGC1000_iTunes_10 | 0        | 100       | IncommWarehouse |

  Scenario: Mixed cart with a Giftcard, CNP Product and Store Product and Accertify response is Reject.
    Given stores with fulfilment capability and stock:
      | store | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | 7126  | Yes     | QLD   | Yes            | Yes                      | Yes                         |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000012 | 4   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode            | firstName | lastName | email                         | messageText   |
      | PGC1014_venue_cocktail | Debashish | Sethy    | debashish.sethy@uxcoxygen.com | Test Checkout |
    And delivery mode is 'home-delivery'
    And the order would trigger an Accertify response of 'REJECT'
    When order is placed
    Then order is in status 'REJECTED'
    And 0 consignments are created
    And fastline stock is:
      | product  | reserved | available |
      | 10000012 | 0        | 100       |
      | 10000011 | 0        | 100       |
    And incomm stock is:
      | product                | reserved | available | warehouseCode   |
      | PGC1014_venue_cocktail | 0        | 100       | IncommWarehouse |

  Scenario: Mix cart, Stock not available at fulfilment enabled NTL
    Given a cart with entries:
      | product  | qty | price |
      | 10000324 | 2   | 15    |
      | 10000510 | 2   | 15    |
    And delivery mode is 'home-delivery'
    And the delivery address is '160, Terrace Place, Murarrie, QLD, 4172'
    When order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'CNP'
    And a consignment is assigned to warehouse 'fastline'
    And CNP stock is:
      | product  | reserved | available |
      | 10000324 | 2        | 100       |
    And NTL stock is:
      | product  | reserved | available | warehouseCode |
      | 10000510 | 0        | 1         | TollQLD       |
    And fastline stock is:
      | product  | reserved | available |
      | 10000510 | 2        | 100       |

  Scenario: Mixed cart with a Giftcard, CNP Product and Store Product
    Given stores with fulfilment capability and stock:
      | store | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | 7126  | Yes     | QLD   | Yes            | Yes                      | Yes                         |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000324 | 4   | 15    |
    And a gift card is added to cart with recipient details:
      | productCode            | firstName | lastName | email                         | messageText   |
      | PGC1014_venue_cocktail | Debashish | Sethy    | debashish.sethy@uxcoxygen.com | Test Checkout |
    And delivery mode is 'home-delivery'
    And the delivery address is '19,Robina Town Centre Drive,Robina,QLD,4226'
    When order is placed
    Then 3 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And a consignment is assigned to warehouse 'CNP'
    And a consignment is assigned to warehouse 'Robina'
    And incomm stock is:
      | product                | reserved | available | warehouseCode   |
      | PGC1014_venue_cocktail | 0        | 100       | IncommWarehouse |
    And CNP stock is:
      | product  | reserved | available |
      | 10000324 | 4        | 100       |

  Scenario: Mixed cart with a Giftcard, CNP Product and Fastline Product
    Given a cart with entries:
      | product  | qty | price |
      | 10000021 | 2   | 15    |
      | 10000323 | 4   | 12    |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText   |
      | PGC1000_iTunes_10 | Debashish | Sethy    | debashish.sethy@uxcoxygen.com | Test Checkout |
    And delivery mode is 'home-delivery'
    When order is placed
    Then 3 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And a consignment is assigned to warehouse 'fastline'
    And a consignment is assigned to warehouse 'CNP'
    And fastline stock is:
      | product  | reserved | available |
      | 10000021 | 2        | 100       |
    And CNP stock is:
      | product  | reserved | available |
      | 10000323 | 4        | 100       |
    And incomm stock is:
      | product           | reserved | available | warehouseCode   |
      | PGC1000_iTunes_10 | 0        | 100       | IncommWarehouse |

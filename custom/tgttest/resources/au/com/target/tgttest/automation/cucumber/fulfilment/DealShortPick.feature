@cartProductData
Feature: Fulfilment Process: set "Allows short pick" to "Y" for deal items

  Background: 
    Given global order routing is Disabled
    And a buy-get deal:
    And cart entries with buy-get deal(s):
      | product  | qty | price | deal |
      | 10000011 | 2   | 50    | Y    |
      | 10000012 | 2   | 20    | N    |

  Scenario: Place an order with deal items
    When order is placed
    Then all items in order extract will have the Allows short pick flag set to 'Y'

  Scenario: Cancel an order with deal items
    Given order is placed
    When cancel entries
      | product  | qty |
      | 10000012 | 1   |
      | 10000011 | 1   |
    Then all items in order extract will have the Allows short pick flag set to 'Y'

  Scenario: Resend order extract for an order with deal items
    Given order is placed
    When order extract is resent
    Then all items in order extract will have the Allows short pick flag set to 'Y'

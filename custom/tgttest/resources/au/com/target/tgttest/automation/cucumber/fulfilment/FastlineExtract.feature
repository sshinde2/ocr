@cartProductData @deliveryModeData @currencyConversionFactorsData
Feature: Fastline extract for fulfilment
  
  Notes:Following currency conversion factors have been setup via impex:
      | sourceCurrency | targetCurrency | currencyConversionFactor | dateTimeStamp       |
      | NZD            | AUD            | 1.08900                  | 2015-12-08 04:00:00 |
      | NZD            | AUD            | 1.08971                  | 2015-12-08 05:00:00 |
      | NZD            | AUD            | 1.08890                  | 2015-12-09 05:00:00 |
      | NZD            | AUD            | 1.08257                  | 2015-12-10 05:00:00 |
      | NZD            | AUD            | 1.07439                  | 2015-12-11 05:00:00 |
    And Following products have been setup with weights via impex:
      | product  | weight(kgs) | producttype |
      | 10000011 | 0.5         | normal      |
      | 10000012 | 1.0         | normal      |
      | 10000021 | 0.0         | normal      |
      | 10000022 |             | normal      |
      | 10000411 | 10.20       | bulky       |
    And feature 'fastline.include.item.order.values' is set to ON
    And home delivery fee is $9 for orders containing normal products below $75
    And home delivery fee is $19 for big and bulky products up to 2 quantities

  Background: 
    Given Postcodes allocated for the catchment area and also delivery fee set up based on product type and quantity.
      | postCode | catchmentArea | productType | qty       | deliveryFee | lowestDeliveryFee |
      | n/a      | n/a           | normal      | 1 or more | $9          |                   |
      | n/a      | n/a           | bulky1      | 1 or more | $19         |                   |

  Scenario: TradeMe Single item before exchange rate change
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 1   | 20    |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | TradeMe       | TradeMe      | NZD      | NZ      | 10            | 2015-12-08T05:05:00 |
    And the order is imported in hybris
    And a new order in hybris is created
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 21.79      | 21.79            | 0.5        | N            | 10.9        |

  Scenario: TradeMe Single item after exchange rate change
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 1   | 20    |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | TradeMe       | TradeMe      | NZD      | NZ      | 10            | 2015-12-10T05:05:00 |
    And the order is imported in hybris
    And a new order in hybris is created
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 21.65      | 21.65            | 0.5        | N            | 10.83       |

  Scenario: TradeMe Multiple items before exchange rate change
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | TradeMe       | TradeMe      | NZD      | NZ      | 10            | 2015-12-08T05:05:00 |
    And the order is imported in hybris
    And a new order in hybris is created
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 21.79      | 32.69            | 0.5        | N            | 10.9        |
      | 10000012 | 2               | 10.90      | 32.69            | 1.0        | N            | 10.9        |

  Scenario: TradeMe Multiple items after exchange rate change
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | TradeMe       | TradeMe      | NZD      | NZ      | 10            | 2015-12-10T05:05:00 |
    And the order is imported in hybris
    And a new order in hybris is created
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 21.65      | 32.48            | 0.5        | N            | 10.83       |
      | 10000012 | 2               | 10.83      | 32.48            | 1.0        | N            | 10.83       |

  Scenario: eBay Single item before exchange rate change
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 1   | 20    |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | eBay          | PayPal       | AUD      | AU      | 10            | 2015-12-08T05:05:00 |
    And the order is imported in hybris
    And a new order in hybris is created
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 20.00            | 0.5        | N            | 10          |

  Scenario: eBay Single item after exchange rate change
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 1   | 20    |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | eBay          | PayPal       | AUD      | AU      | 10            | 2015-12-10T05:05:00 |
    And the order is imported in hybris
    And a new order in hybris is created
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 20.00            | 0.5        | N            | 10          |

  Scenario: eBay Multiple items before exchange rate change
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | eBay          | PayPal       | AUD      | AU      | 10            | 2015-12-08T05:05:00 |
    And the order is imported in hybris
    And a new order in hybris is created
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 10          |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 10          |

  Scenario: eBay Multiple items after exchange rate change
    Given a new order in webmethods with product entries
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And a new order in webmethods with:
      | Sales Channel | Payment Type | Currency | Country | Delivery Cost | created Date        |
      | eBay          | PayPal       | AUD      | AU      | 10            | 2015-12-10T05:05:00 |
    And the order is imported in hybris
    And a new order in hybris is created
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 10          |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 10          |

  Scenario: Web Multiple items, mixed product types i.e. with non-zero weight, with zero weight and without weight.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
      | 10000021 | 1   | 15    |
      | 10000022 | 1   | 25    |
    And order is placed
    When business process completes
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 70.00            | 0.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 70.00            | 1.0        | N            | 9           |
      | 10000021 | 1               | 15.00      | 70.00            | 0.0        | N            | 9           |
      | 10000022 | 1               | 25.00      | 70.00            |            | N            | 9           |

  Scenario: Web Multiple items, weights are not updated and extract is resent
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And order is placed
    And business process completes
    And the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 9           |
    When order extract is resent
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 9           |

  Scenario: Web Multiple items, weights are updated and extract is resent
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And order is placed
    And business process completes
    And the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 9           |
    And product weights are updated as:
      | productId | weight |
      | 10000011  | 1.5    |
      | 10000012  | 2.0    |
    When order extract is resent
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 1.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 30.00            | 2.0        | N            | 9           |

  Scenario: Web Multiple items, weights are not updated and order is updated
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And order is placed
    And business process completes
    And the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 9           |
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000012 | 2               | 10.00      | 10.00            | 1.0        | U            | 9           |

  Scenario: Web Multiple items, weights are updated and order is updated
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And order is placed
    And business process completes
    And the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 9           |
    And product weights are updated as:
      | productId | weight |
      | 10000011  | 1.5    |
      | 10000012  | 2.0    |
    When cancel entries
      | product  | qty |
      | 10000012 | 1   |
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 25.00            | 1.5        | U            | 9           |
      | 10000012 | 1               | 5.00       | 25.00            | 2.0        | U            | 9           |

  Scenario: Web Multiple items, weights are not updated and order is cancelled
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And order is placed
    And business process completes
    And the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 9           |
    When the order is fully cancelled
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 11111111 | 1               |            |                  |            | C            |             |

  Scenario: Web Multiple items, weights are updated and order is cancelled
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000012 | 2   | 5     |
    And order is placed
    And business process completes
    And the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 30.00            | 0.5        | N            | 9           |
      | 10000012 | 2               | 10.00      | 30.00            | 1.0        | N            | 9           |
    And product weights are updated as:
      | productId | weight |
      | 10000011  | 1.5    |
      | 10000012  | 2.0    |
    When the order is fully cancelled
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 11111111 | 1               |            |                  |            | C            |             |

  Scenario: Web Multiple containing bulky and normal items, order is updated and delivery fee is affected
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 20    |
      | 10000411 | 1   | 15    |
    And order is placed
    And business process completes
    And the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 35.00            | 0.5        | N            | 19          |
      | 10000411 | 1               | 15.00      | 35.00            | 10.20      | N            | 19          |
    When cancel entries
      | product  | qty |
      | 10000411 | 1   |
    Then the fastline extract contains the records
      | itemCode | quantityOrdered | itemAmount | orderGoodsAmount | itemWeight | orderPurpose | deliveryFee |
      | 10000011 | 1               | 20.00      | 20.00            | 0.5        | U            | 9           |

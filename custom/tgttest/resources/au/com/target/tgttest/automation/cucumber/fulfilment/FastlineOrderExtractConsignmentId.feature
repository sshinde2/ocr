@cartProductData @sessionCatalogVersion
Feature: Send Correct Identifier to Fastline based on the feature switch.
     As Target Online
     I want to ensure that Fastline is able to receive and process consignment entries identified by consignment id
     So that Fastline is able to fulfill just part of an order (but cnc barcode still generated from ordernumber)
     
     I want to know which identifier (order id or consignment id) is used  in an order extract to Fastline for a given order
     So that I can process consignment updates during the transition of using order id to consignment id

  Scenario: When feature switch is off then order number is used for new consignment
    Given feature 'fastline.orderextract.consignmentid' is switched 'off'
    When a consignment is sent to fastline
    Then ConsignmentExtractRecord has consignmentId used 'false'
    And the following fastline extracts are sent:
      | consignment id | order purpose |
      | ORDER_NUMBER   | NEW           |

  Scenario: When feature switch is off then order number is used for new and resent consignment
    Given feature 'fastline.orderextract.consignmentid' is switched 'off'
    And a consignment is sent to fastline
    When order extract is resent
    Then ConsignmentExtractRecord has consignmentId used 'false'
    And the following fastline extracts are sent:
      | consignment id | order purpose |
      | ORDER_NUMBER   | NEW           |
      | ORDER_NUMBER   | NEW           |

  Scenario: When feature switch is on then consignment id is used for new consignment
    Given feature 'fastline.orderextract.consignmentid' is switched 'on'
    When a consignment is sent to fastline
    Then ConsignmentExtractRecord has consignmentId used 'true'
    And the following fastline extracts are sent:
      | consignment id     | order purpose |
      | CONSIGNMENT_NUMBER | NEW           |

  Scenario: When feature switch is on then consignment id is used for new and resent consignment
    Given feature 'fastline.orderextract.consignmentid' is switched 'on'
    And a consignment is sent to fastline
    When order extract is resent
    Then ConsignmentExtractRecord has consignmentId used 'true'
    And the following fastline extracts are sent:
      | consignment id     | order purpose |
      | CONSIGNMENT_NUMBER | NEW           |
      | CONSIGNMENT_NUMBER | NEW           |
      
Scenario: During transition updated extract uses order id even after switch is on
    Given feature 'fastline.orderextract.consignmentid' is switched 'off'
    And a cart with entries:
      | product  | qty |
      | 10000011 | 2   |
    And order is placed
    And feature 'fastline.orderextract.consignmentid' is switched 'on'
    When cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then ConsignmentExtractRecord has consignmentId used 'false'
    And the following fastline extracts are sent:
      | consignment id | order purpose |
      | ORDER_NUMBER   | NEW           |
      | ORDER_NUMBER   | UPDATED       |

  Scenario: During transition cancelled extract uses order id even after switch is on
    Given feature 'fastline.orderextract.consignmentid' is switched 'off'
    And a consignment is sent to fastline
    When feature 'fastline.orderextract.consignmentid' is switched 'on'
    And the order is fully cancelled
    Then ConsignmentExtractRecord has consignmentId used 'false'
    And the following fastline extracts are sent:
      | consignment id | order purpose |
      | ORDER_NUMBER   | NEW           |
      | ORDER_NUMBER   | CANCELLED     |

  Scenario: During reverse transition cancelled extract uses order id even after switch is on
    Given feature 'fastline.orderextract.consignmentid' is switched 'on'
    And a consignment is sent to fastline
    When feature 'fastline.orderextract.consignmentid' is switched 'off'
    And the order is fully cancelled
    Then ConsignmentExtractRecord has consignmentId used 'true'
    And the following fastline extracts are sent:
      | consignment id     | order purpose |
      | CONSIGNMENT_NUMBER | NEW           |
      | CONSIGNMENT_NUMBER | CANCELLED     |

  Scenario: During transition resent extract uses order id even after switch is on
    Given feature 'fastline.orderextract.consignmentid' is switched 'off'
    And a consignment is sent to fastline
    When feature 'fastline.orderextract.consignmentid' is switched 'on'
    And order extract is resent
    Then ConsignmentExtractRecord has consignmentId used 'false'
    And the following fastline extracts are sent:
      | consignment id | order purpose |
      | ORDER_NUMBER   | NEW           |
      | ORDER_NUMBER   | NEW           |

@notAutomated
Feature: Make Toll Warehouses a Non-Trading Location
  As the supply chain team
  I want the Toll DCs to be set up as non-trading locations
  So that the stores are aligned with Merchandise system and they do not appear in the results of the store locator

  Background: 
    Given a list of non-trading locations
      | ntl          | number | address                            |
      | NSWONLINEBNB | 5600   | 5 Helles Ave Moorebank NSW 2170    |
      | QLDONLINEBNB | 5601   | 16 Terrace Place Murarrie QLD 4172 |
      | WAONLINEBNB  | 5602   | 22 Tomah Rd Welshpool WA 6106      |

  Scenario: Ensure Non-trading locations do not appear in store locator both in Web and Mobile
    When user search for stores in <state>
    Then <ntl> should not be visible
      | state | ntl          |
      | NSW   | NSWONLINEBNB |
      | QLD   | QLDONLINEBNB |
      | WA    | WAONLINEBNB  |

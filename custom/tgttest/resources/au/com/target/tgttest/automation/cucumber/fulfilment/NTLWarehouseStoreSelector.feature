@cartProductData @ntlFulfilmentData
Feature: Assign order to NTL Warehouse after Lookup for Order Stock Level
  As a supply chain team member
  I want the order to be fulfilled, where possible, by the NTL in the same state as the delivery destination
  So that the delivery costs are minimised
  
  
  Notes: Following data is setup via impex:
    NTLs with status and stock:
      | ntl          | enabled | stock                                       |
      | NSWONLINEBNB | yes     | 2 of 10000411, 0 of 10000510, 5 of 10000021 |
      | QLDONLINEBNB | yes     | 1 of 10000411, 1 of 10000510, 1 of 10000021 |
      | WAONLINEBNB  | no      | 5 of 10000411, 0 of 10000510, 3 of 10000021 |

  Background: 
    Given global order routing is Disabled
    And set fastline stock:
      | product  | reserved | available |
      | 10000411 | 0        | 10        |
      | 10000510 | 0        | 10        |
      | 10000021 | 0        | 10        |

  Scenario: Stock available for cart with HD at fulfilment enabled NTL
    Given a cart with entries:
      | product  | qty | price |
      | 10000411 | 2   | 15    |
      | 10000021 | 2   | 10    |
    And delivery mode is 'home-delivery'
    And the delivery address is '8/15, Helles Ave, Moorebank, NSW, 2170'
    When order is placed
    Then a single consignment is created
    And the assigned warehouse will be 'NSWONLINEBNB'
    And NTL stock is:
      | product  | reserved | available | warehouseCode |
      | 10000411 | 2        | 2         | TollNSW       |
      | 10000021 | 2        | 5         | TollNSW       |
    And fastline stock is:
      | product  | reserved | available |
      | 10000411 | 0        | 10        |
      | 10000021 | 0        | 10        |

  Scenario: Stock available for cart with Express Delivery at fulfilment enabled NTL
    Given a cart with entries:
      | product  | qty | price |
      | 10000021 | 1   | 15    |
    And delivery mode is 'express-delivery'
    And the delivery address is '29, Firestone Court, Robina, QLD, 4226'
    When order is placed
    Then a single consignment is created
    And the assigned warehouse will be 'QLDONLINEBNB'
    And NTL stock is:
      | product  | reserved | available | warehouseCode |
      | 10000021 | 1        | 3         | TollQLD       |
    And fastline stock is:
      | product  | reserved | available |
      | 10000021 | 0        | 10        |

  Scenario: Stock available for cart with CNC at fulfilment enabled NTL
    Given a cart with entries:
      | product  | qty | price |
      | 10000411 | 1   | 15    |
      | 10000510 | 1   | 15    |
    And delivery mode is 'click-and-collect'
    And the delivery address is '160, Terrace Place, Murarrie, QLD, 4172'
    When order is placed
    Then a single consignment is created
    And the assigned warehouse will be 'Fastline'
    And NTL stock is:
      | product  | reserved | available | warehouseCode |
      | 10000411 | 0        | 1         | TollQLD       |
      | 10000510 | 0        | 1         | TollQLD       |
    And fastline stock is:
      | product  | reserved | available |
      | 10000411 | 1        | 10        |
      | 10000510 | 1        | 10        |

  Scenario: Stock not available at fulfilment enabled NTL
    Given a cart with entries:
      | product  | qty | price |
      | 10000510 | 2   | 15    |
      | 10000411 | 4   | 12    |
    And delivery mode is 'home-delivery'
    And the delivery address is '160, Terrace Place, Murarrie, QLD, 4172'
    When order is placed
    Then a single consignment is created
    And the assigned warehouse will be 'Fastline'
    And NTL stock is:
      | product  | reserved | available | warehouseCode |
      | 10000510 | 0        | 1         | TollQLD       |
      | 10000411 | 0        | 1         | TollQLD       |
    And fastline stock is:
      | product  | reserved | available |
      | 10000510 | 2        | 10        |
      | 10000411 | 4        | 10        |

  Scenario: Stock available at fulfilment disabled NTL
    Given a cart with entries:
      | product  | qty | price |
      | 10000021 | 3   | 15    |
      | 10000411 | 2   | 10    |
    And delivery mode is 'home-delivery'
    And the delivery address is '180, Drake Street, Morley, WA, 6062'
    When order is placed
    Then a single consignment is created
    And the assigned warehouse will be 'Fastline'
    And NTL stock is:
      | product  | reserved | available | warehouseCode |
      | 10000021 | 0        | 3         | TollWA        |
      | 10000411 | 0        | 5         | TollWA        |
    And fastline stock is:
      | product  | reserved | available |
      | 10000021 | 3        | 10        |
      | 10000411 | 2        | 10        |

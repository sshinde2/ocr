@cartProductData @cleanOrderData @cleanConsignmentData @storeFulfilmentCapabilitiesData @mockStoreSelectorStrategy
Feature: verify order and consignment statuses when order is fulfilled by multiple warehouses
  
  As a Target Online
  I want to place an order with multiple items in basket
  So that consignments are fulfilled by Incomm and Ofc warehouses
  
   Scenario: Cart contains physical giftcard and a fastline/normal product where physical giftcard shipped first
   Given a cart with entries:
      | product               | qty | price |
      | 10000011              | 2   | 15    |
      | PHYGC1003_skydive_100 | 1   |   100 |
    And registered user checkout
    And customer checkout is started
    When order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Fastline'
    And a consignment is assigned to warehouse 'Incomm'
    When complete consignment is recieved for 'Incomm'
    And auto pick process completes
    And order complete process completes
    Then 'Incomm' consignment status is 'SHIPPED'
    And order is in status 'INPROGRESS'
    When order ack is received from fastline
    Then 'fastline' consignment status is 'CONFIRMED_BY_WAREHOUSE'
    And order is in status 'INPROGRESS'
    When the consignment is full picked by fastline
    Then fastline pick process completes
    And 'fastline' consignment status is 'PICKED'
    And order is in status 'INVOICED'
    When ship conf is received from fastline
    Then order complete process completes
    And 'fastline' consignment status is 'SHIPPED'
    And order is in status 'COMPLETED'
    
   Scenario: Cart contains physical giftcard and a fastline/normal product where physical is shipped after pick from fastline
   Given a cart with entries:
      | product               | qty | price |
      | 10000011              | 2   | 15    |
      | PHYGC1003_skydive_100 | 1   |   100 |
    And registered user checkout
    And customer checkout is started
    When order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Fastline'
    And a consignment is assigned to warehouse 'Incomm'
    When order ack is received from fastline
    Then 'fastline' consignment status is 'CONFIRMED_BY_WAREHOUSE'
    And order is in status 'INPROGRESS'
    When the consignment is full picked by fastline
    Then fastline pick process completes
    And 'fastline' consignment status is 'PICKED'
    And order is in status 'INPROGRESS'
    When complete consignment is recieved for 'Incomm'
    And auto pick process completes
    And order complete process completes
    Then 'Incomm' consignment status is 'SHIPPED'
    And order is in status 'INVOICED'
    When ship conf is received from fastline
    Then order complete process completes
    And 'fastline' consignment status is 'SHIPPED'
    And order is in status 'COMPLETED'
    
   Scenario: Cart contains physical giftcard and a fastline/normal product where physical giftcard shipped first after ACK from fastline
   Given a cart with entries:
      | product               | qty | price |
      | 10000011              | 2   | 15    |
      | PHYGC1003_skydive_100 | 1   |   100 |
    And registered user checkout
    And customer checkout is started
    When order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Fastline'
    And a consignment is assigned to warehouse 'Incomm'
    When order ack is received from fastline
    Then 'fastline' consignment status is 'CONFIRMED_BY_WAREHOUSE'
    And order is in status 'INPROGRESS'
    When complete consignment is recieved for 'Incomm'
    And auto pick process completes
    And order complete process completes
    Then 'Incomm' consignment status is 'SHIPPED'
    And order is in status 'INPROGRESS'
    When the consignment is full picked by fastline
    Then fastline pick process completes
    And 'fastline' consignment status is 'PICKED'
    And order is in status 'INVOICED'
    When ship conf is received from fastline
    Then order complete process completes
    And 'fastline' consignment status is 'SHIPPED'
    And order is in status 'COMPLETED'
    
   Scenario: Cart contains physical giftcard and a fastline/normal product where fastline consignment is shipped first
   Given a cart with entries:
      | product               | qty | price |
      | 10000011              | 2   | 15    |
      | PHYGC1003_skydive_100 | 1   |   100 |
    And registered user checkout
    And customer checkout is started
    When order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Fastline'
    And a consignment is assigned to warehouse 'Incomm'
    When order ack is received from fastline
    Then 'fastline' consignment status is 'CONFIRMED_BY_WAREHOUSE'
    And order is in status 'INPROGRESS'
    When the consignment is full picked by fastline
    Then fastline pick process completes
    And 'fastline' consignment status is 'PICKED'
    And order is in status 'INPROGRESS'
    When ship conf is received from fastline
    Then order complete process completes
    And 'fastline' consignment status is 'SHIPPED'
    When complete consignment is recieved for 'Incomm'
    And auto pick process completes
    And order complete process completes
    Then 'Incomm' consignment status is 'SHIPPED'
    And order is in status 'COMPLETED'
    
   
   Scenario: Cart contains physical giftcard and a product with instore fulfillment, where physical giftcard is shipped first
   Given a cart with entries:
      | product               | qty | price |
      | 10000021              | 2   | 15    |
      | PHYGC1003_skydive_100 | 1   |   100 |
    And store number for ofc portal is '7049' 
    And the delivery address is '11,Canning Street,Rockhampton,QLD,4700'
    And registered user proceed to checkout
    And order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And a consignment is assigned to warehouse 'Rockhampton'
    When complete consignment is recieved for 'Incomm'
    And auto pick process completes
    And order complete process completes
    Then 'Incomm' consignment status is 'SHIPPED'
    And order is in status 'INPROGRESS'
    When the consignment is accepted by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'WAVED'
    And order is in status 'INPROGRESS'
    When the consignment is picked by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'PICKED'
    And order is in status 'INPROGRESS'
    When the consignment is packed by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'PACKED'
    And order is in status 'INVOICED'
    When ofc transmit function is run
    And order complete process completes
    And 'Rockhampton' consignment status is 'SHIPPED'
    And order is in status 'COMPLETED'
    
   Scenario: Cart contains physical giftcard and a product with instore fulfillment, where physical giftcard is shipped after OFC accepts the consignment
   Given a cart with entries:
      | product               | qty | price |
      | 10000021              | 2   | 15    |
      | PHYGC1003_skydive_100 | 1   |   100 |
    And store number for ofc portal is '7049' 
    And the delivery address is '11,Canning Street,Rockhampton,QLD,4700'
    And registered user proceed to checkout
    And order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And a consignment is assigned to warehouse 'Rockhampton'
    When the consignment is accepted by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'WAVED'
    And order is in status 'INPROGRESS'
    When complete consignment is recieved for 'Incomm'
    And auto pick process completes
    And order complete process completes
    Then 'Incomm' consignment status is 'SHIPPED'
    And order is in status 'INPROGRESS'
    When the consignment is picked by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'PICKED'
    And order is in status 'INPROGRESS'
    When the consignment is packed by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'PACKED'
    And order is in status 'INVOICED'
    When ofc transmit function is run
    And order complete process completes
    And 'Rockhampton' consignment status is 'SHIPPED'
    And order is in status 'COMPLETED'
    
   Scenario: Cart contains physical giftcard and a product with instore fulfillment, where physical giftcard is shipped after OFC picks the consignment
   Given a cart with entries:
      | product               | qty | price |
      | 10000021              | 2   | 15    |
      | PHYGC1003_skydive_100 | 1   |   100 |
    And store number for ofc portal is '7049' 
    And the delivery address is '11,Canning Street,Rockhampton,QLD,4700'
    And registered user proceed to checkout
    And order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And a consignment is assigned to warehouse 'Rockhampton'
    When the consignment is accepted by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'WAVED'
    And order is in status 'INPROGRESS'
    When the consignment is picked by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'PICKED'
    And order is in status 'INPROGRESS'
    When complete consignment is recieved for 'Incomm'
    And auto pick process completes
    And order complete process completes
    Then 'Incomm' consignment status is 'SHIPPED'
    And order is in status 'INPROGRESS'
    When the consignment is packed by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'PACKED'
    And order is in status 'INVOICED'
    When ofc transmit function is run
    And order complete process completes
    And 'Rockhampton' consignment status is 'SHIPPED'
    And order is in status 'COMPLETED'
    
   Scenario: Cart contains physical giftcard and a product with instore fulfillment, where physical giftcard is shipped after OFC packs the consignment
   Given a cart with entries:
      | product               | qty | price |
      | 10000021              | 2   | 15    |
      | PHYGC1003_skydive_100 | 1   |   100 |
    And store number for ofc portal is '7049' 
    And the delivery address is '11,Canning Street,Rockhampton,QLD,4700'
    And registered user proceed to checkout
    And order is placed
    Then 2 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And a consignment is assigned to warehouse 'Rockhampton'
    When the consignment is accepted by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'WAVED'
    And order is in status 'INPROGRESS'
    When the consignment is picked by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'PICKED'
    And order is in status 'INPROGRESS'
    When the consignment is packed by store 'Rockhampton'
    Then 'Rockhampton' consignment status is 'PACKED'
    And order is in status 'INPROGRESS'
    When complete consignment is recieved for 'Incomm'
    And auto pick process completes
    And order complete process completes
    Then 'Incomm' consignment status is 'SHIPPED'
    And order is in status 'INVOICED'
    When ofc transmit function is run
    And order complete process completes
    And 'Rockhampton' consignment status is 'SHIPPED'
    And order is in status 'COMPLETED'
    
    
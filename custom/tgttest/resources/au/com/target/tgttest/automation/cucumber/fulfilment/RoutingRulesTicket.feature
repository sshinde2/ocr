@cartProductData @stockLevelExceptionMock
Feature: Analyse the behaviour of fulfiment routing rules when failures occurs
  
  As the target online trading manager 
  
  So that when failures occurs, analysing the behaviour of fulfilment routing rules by raising cs ticket

  Scenario: Exception handling for products when stock level is not available
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 20    |
      | 10000012 | 3   | 20    |
    And registered user checkout
    And customer checkout is started
    And delivery mode is 'home-delivery'
    When order is placed which might fail
    Then a CS ticket is created

@sessionCatalogVersion
Feature: Gift Cards - adding to basket
  
  As an Online customer
  I want to specify different delivery details for each e-gift I add to my cart 
  So that i can purchase multiple e-gifts for different people in a single purchase

  Background: 
    Given gift card data are set up as:
      | BrandID        | MaxOrderQuantity | MaxOrderValue |
      | appletest1234  | 2                | 50            |
      | test1adrenalin | 2                | 50            |

  Scenario: Add a single gift card to cart
    When a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText            |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    Then the gift card validation is 'SUCCESS'
    And gift card product details are:
      | productCode       | giftCardFlag | deliveryModes     | cardValue | cardDesign | quantity |
      | PGC1000_iTunes_10 | true         | digital-gift-card | 10        | nocolour   | 1        |
    And gift recipient details for the product 'PGC1000_iTunes_10' are:
      | firstName | lastName | email                          | messageText            |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    And only the digital-gift-card delivery mode is available on the cart

  Scenario: Add a two gift cards to cart of the same brand and same denomination
    When gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    Then the gift card validation is 'SUCCESS'
    And gift card product details are:
      | productCode       | giftCardFlag | deliveryModes     | cardValue | cardDesign | quantity |
      | PGC1000_iTunes_10 | true         | digital-gift-card | 10        | nocolour   | 2        |
    And gift recipient details for the product 'PGC1000_iTunes_10' are:
      | firstName | lastName | email                          | messageText             |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    And only the digital-gift-card delivery mode is available on the cart

  Scenario: Add a two gift cards to cart of the same brand and different denomination
    When gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1001_iTunes_20 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    Then the gift card validation is 'SUCCESS'
    And gift card product details are:
      | productCode       | giftCardFlag | deliveryModes     | cardValue | cardDesign | quantity |
      | PGC1000_iTunes_10 | true         | digital-gift-card | 10        | nocolour   | 1        |
      | PGC1001_iTunes_20 | true         | digital-gift-card | 20        | nocolour   | 1        |
    And gift recipient details for the product 'PGC1000_iTunes_10' are:
      | firstName | lastName | email                          | messageText            |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    And gift recipient details for the product 'PGC1001_iTunes_20' are:
      | firstName | lastName | email                          | messageText             |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    And only the digital-gift-card delivery mode is available on the cart

  Scenario: Violate the quantity rule on adding the product to basket using products with the same brand
    When gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1001_iTunes_20 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
      | PGC1001_iTunes_20 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message3 |
    Then the gift card validation is 'QUANTITY'

  Scenario: Add products from different brand to ensure the rules of quantity don't apply
    When gift cards are added to cart with recipient details:
      | productCode          | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10    | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1001_iTunes_20    | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
      | PGC1003_adventure_50 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message3 |
    Then the gift card validation is 'SUCCESS'

  Scenario: Violate the value rule on adding the product to basket using products with the same brand
    When gift cards are added to cart with recipient details:
      | productCode          | firstName | lastName | email                          | messageText             |
      | PGC1003_adventure_50 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1003_adventure_50 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    Then the gift card validation is 'VALUE'

  Scenario: Add products from different brand to ensure the rules of quantity don't apply
    When gift cards are added to cart with recipient details:
      | productCode          | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10    | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1003_adventure_50 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message3 |
    Then the gift card validation is 'SUCCESS'

  Scenario: Add a one gift cards get another free
    Given set deal parameters:
      | name   | rewardType     | rewardValue | rewardMaxQty |
      | buyget | PercentOffEach | 50          | 1            |
    When gift cards are added to cart with recipient details with deals:
      | productCode       | firstName | lastName | email                          | messageText             | price | dealName | dealRole | minQty |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  | 10    | buyget   | Q        | 1      |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 | 10    | buyget   | R        | 0      |
    Then the gift card validation is 'SUCCESS'
    And gift card product details are:
      | productCode       | giftCardFlag | deliveryModes     | cardValue | cardDesign | quantity | discountPrice | basePrice | totalPrice |
      | PGC1000_iTunes_10 | true         | digital-gift-card | 10        | nocolour   | 2        | 5             | 10        | 15         |
    And gift recipient details for the product 'PGC1000_iTunes_10' are:
      | firstName | lastName | email                          | messageText             |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    And only the digital-gift-card delivery mode is available on the cart

  Scenario: mixed basket
    Given any home-delivery cart
    When a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText            |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    Then the gift card validation is 'SUCCESS'
    And digital cart delivery modes are:
      | name              | available |
      | digital-gift-card | true      |
    And physical cart delivery modes are:
      | name              | available |
      | home-delivery     | true      |
      | click-and-collect | true      |

  Scenario: Add a two gift cards to cart of the same brand and same denomination and remove one recipient
    Given gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    When recipient entry '1' of the product 'PGC1000_iTunes_10' is removed from cart
    Then gift recipient details for the product 'PGC1000_iTunes_10' are:
      | firstName | lastName | email                          | messageText            |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    And only the digital-gift-card delivery mode is available on the cart
    And cart has subtotals:
      | total | subtotal | deliveryCost |
      | 10.0  | 10.0     | 0.0          |

  Scenario: Add a one gift cards to cart and remove one recipient
    Given a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText            |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    When recipient entry '0' of the product 'PGC1000_iTunes_10' is removed from cart
    Then cart is empty

  Scenario: Add a giftcard with 2 recipients and another with 1 recipient and removing the one with 1 recipient
    Given gift cards are added to cart with recipient details:
      | productCode          | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10    | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1000_iTunes_10    | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
      | PGC1003_adventure_50 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message3 |
    When recipient entry '0' of the product 'PGC1003_adventure_50' is removed from cart
    Then the cart contains:
      | PGC1000_iTunes_10 |
    And gift recipient details for the product 'PGC1000_iTunes_10' are:
      | firstName | lastName | email                          | messageText             |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    And only the digital-gift-card delivery mode is available on the cart
    And cart has subtotals:
      | total | subtotal | deliveryCost |
      | 20.0  | 20.0     | 0.0          |

  Scenario: Add a giftcard with 2 recipients and another with 1 recipient and removing one from the one with 2 recipients
    Given gift cards are added to cart with recipient details:
      | productCode          | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10    | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1000_iTunes_10    | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
      | PGC1003_adventure_50 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message3 |
    When recipient entry '1' of the product 'PGC1000_iTunes_10' is removed from cart
    Then the cart contains:
      | PGC1000_iTunes_10    |
      | PGC1003_adventure_50 |
    And gift recipient details for the product 'PGC1000_iTunes_10' are:
      | firstName | lastName | email                          | messageText            |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    And gift recipient details for the product 'PGC1003_adventure_50' are:
      | firstName | lastName | email                          | messageText             |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message3 |
    And only the digital-gift-card delivery mode is available on the cart
    And cart has subtotals:
      | total | subtotal | deliveryCost |
      | 60.0  | 60.0     | 0.0          |

  Scenario: Mixed basket with one gift card multiple recipients
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText             |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message  |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    When recipient entry '0' of the product 'PGC1000_iTunes_10' is removed from cart
    Then the cart contains:
      | PGC1000_iTunes_10 |
      | 10000011          |
    And gift recipient details for the product 'PGC1000_iTunes_10' are:
      | firstName | lastName | email                          | messageText             |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message2 |
    And cart has subtotals:
      | total | subtotal | deliveryCost |
      | 40.0  | 40.0     | 0.0          |

  Scenario: Mixed basket with one gift card multiple recipients
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                          | messageText            |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    When recipient entry '0' of the product 'PGC1000_iTunes_10' is removed from cart
    Then the cart contains:
      | 10000011 |
    And cart has subtotals:
      | total | subtotal | deliveryCost |
      | 30.0  | 30.0     | 0.0          |

  Scenario: Stock check on add to cart
    Given set Incomm gift cards in stock:
      | product               | available |
      | PGC1003_adventure_100 | NONE      |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When gift cards are added to cart with recipient details:
      | productCode           | firstName | lastName | email                          | messageText            |
      | PGC1003_adventure_100 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au | This is a test message |
    Then the cart contains:
      | 10000011 |

@sessionCatalogVersion
Feature: Error handling for failed order requests sent to Incomm warehouse
  As online support 
  I want to recognise specific error messages coming back from Incomm on a failed order request 
  So that I can make the relevant updates to order status

  Scenario Outline: Errors from Incomm when request sent for fulfilment
    Given incomm returns '<incommOrderStatus>' for order
    When a consignment is sent to incomm
    Then consignment status is '<consignmentStatus>'
    And error logged with '<errorCode>' as error code

    Examples: 
      | incommOrderStatus     | consignmentStatus | errorCode |
      | AlreadyExists         | Shipped           | 103       |
      | InsufficientInventory | Created           | 400208    |
      | BrandNotFound         | Created           | 404201    |
      | ServiceUnavailable    | Created           | na        |

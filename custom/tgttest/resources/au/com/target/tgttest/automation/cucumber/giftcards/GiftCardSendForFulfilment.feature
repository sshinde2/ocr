@cartProductData @sessionCatalogVersion @storeFulfilmentCapabilitiesData
Feature: Gift cards - split and send to warehouse
  
  As Target online
  I want to send selected details for an Incomm / gift card consignment to webMethods
  So that webMethods has all the details required to generate the request to Incomm to fulfil the consignment
  
  As Target Online
  I want to be able to identify e-gift card items in the order and create a consignment for those items
  So that I ensure the items are sent to the correct fulfilment location
  
  As Target Online
  I want to process pick and ship notifications for 3rd party e-gift card orders once details have been successfully accepted by Incomm
  So that I can update the consignment status in Hybris and trigger the relevant business processes
  
  Notes
   * Non-Denominated giftcard Products are setup in impex as:
      | product                | price | denomination | brandId                | giftCardStyle     | giftCardProductId | name                                     |
      | PGC1014_venue_coffee   | 5.50  | 5.50         | nondenominatedvenueaus | CIR_000064_00.png | 613366            | e-Gift Card - Incomm Venue 5 Fine Dining |
      | PGC1014_venue_cocktail | 10    | 10           | nondenominatedvenueaus | CIR_000064_00.png | 613367            | e-Gift Card - Incomm Venue 6 Fine Dining |

  Background: 
    Given set fastline stock:
      | product  | available | reserved |
      | 10000011 |        10 |        0 |
    And set Incomm gift cards in stock:
      | product                | available |
      | PGC1000_iTunes_10      |       100 |
      | PGC1014_venue_coffee   |       100 |
      | PGC1014_venue_cocktail |       100 |

  Scenario: Gift cards successfully sent to incomm
    Given webmethods returns 'success' for send to warehouse
    And gift cards are added to cart with recipient details:
      | productCode       | firstName | lastName | email                           | messageText             |
      | PGC1000_iTunes_10 | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au  | This is a test message  |
      | PGC1000_iTunes_10 | Navarose  | Haneefa2 | Navarose.Haneefa2@target.com.au | This is a test message2 |
    And registered user checkout
    And customer checkout is started
    And delivery mode is 'not changed'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      |  20.0 |     20.0 |          0.0 |          |
    And incomm stock is:
      | product           | reserved | available | warehouseCode   |
      | PGC1000_iTunes_10 |        0 |       100 | IncommWarehouse |
    And a single consignment is created
    And a consignment is assigned to warehouse 'Incomm'
    And consignment contains products:
      | product           | qty |
      | PGC1000_iTunes_10 |   2 |
    And consignment is not sent to Fastline
    And consignment carrier is 'NullDigGift'
    And consignment deliveryMode is 'digital-gift-card'
    And consignment deliveryAddress is 'NULL'
    And consignment is sent to webmethods with:
      | consignmentId      | warehouseId     | orderId      | customerFirstName | customerLastName | shippingMethod |
      | NEW_CONSIGNMENT_ID | IncommWarehouse | NEW_ORDER_ID | test              | user             | Email          |
    And consignment entries are sent to webmethods with:
      | productId         | name            | denomination | brandId       | giftCardStyle     |
      | PGC1000_iTunes_10 | $10 iTunes Code |         10.0 | appletest1234 | CIR_000153_04.png |
    And gift recipient details are sent to webmethods for product 'PGC1000_iTunes_10' with:
      | firstName | lastName | email                           | messageText             |
      | Navarose  | Haneefa  | Navarose.Haneefa@target.com.au  | This is a test message  |
      | Navarose  | Haneefa2 | Navarose.Haneefa2@target.com.au | This is a test message2 |
    And auto pick process completes
    And order complete process completes
    And this consignments status is 'SHIPPED'

  Scenario: Mixed cart with giftcard and physical items
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText   |
      | PGC1000_iTunes_10 | Shreeram  | Mishra   | Shreeram.Mishra@target.com.au | Test Checkout |
    And registered user checkout
    And customer checkout is started
    And delivery mode is 'home-delivery'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      |  49.0 |     40.0 |          9.0 |          |
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 |        2 |        10 |
    And incomm stock is:
      | product           | reserved | available | warehouseCode   |
      | PGC1000_iTunes_10 |        0 |       100 | IncommWarehouse |
    And 2 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And consignment carrier is 'NullDigGift'
    And consignment deliveryMode is 'digital-gift-card'
    And consignment deliveryAddress is 'NULL'
    And consignment contains products:
      | product           | qty |
      | PGC1000_iTunes_10 |   1 |
    And auto pick process completes
    And order complete process completes
    And this consignments status is 'SHIPPED'
    And a consignment is assigned to warehouse 'Fastline'
    And consignment carrier is 'AustraliaPost'
    And consignment deliveryMode is 'home-delivery'
    And consignment deliveryAddress is 'ORDER-DELIVERY-ADDRESS'
    And consignment contains products:
      | product  | qty |
      | 10000011 |   2 |
    And this consignments status is 'SENT_TO_WAREHOUSE'

  Scenario: Mixed cart with giftcard and physical items sent to store
    Given stores with fulfilment capability and stock:
      | store | instoreEnabled | inStock | deliveryModesAllowed | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      |  7126 | Yes            | Yes     | click-and-collect    | Yes                      | No                          |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 |   2 |    15 |
    And a gift card is added to cart with recipient details:
      | productCode       | firstName | lastName | email                         | messageText   |
      | PGC1000_iTunes_10 | Shreeram  | Mishra   | Shreeram.Mishra@target.com.au | Test Checkout |
    And delivery mode is 'click-and-collect'
    And order cnc store is '7126'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      |  40.0 |     40.0 |          0.0 |          |
    And 2 consignments are created
    And a consignment is assigned to warehouse 'Incomm'
    And consignment carrier is 'NullDigGift'
    And consignment deliveryMode is 'digital-gift-card'
    And consignment deliveryAddress is 'NULL'
    And consignment contains products:
      | product           | qty |
      | PGC1000_iTunes_10 |   1 |
    And auto pick process completes
    And order complete process completes
    And this consignments status is 'SHIPPED'
    And a consignment is assigned to warehouse 'Robina'
    And consignment carrier is 'NullCnC'
    And consignment deliveryMode is 'click-and-collect'
    And consignment deliveryAddress is 'ORDER-DELIVERY-ADDRESS'
    And consignment contains products:
      | product  | qty |
      | 10000011 |   2 |
    And this consignments status is 'CONFIRMED_BY_WAREHOUSE'

  Scenario: Non-Denominated Gift cards successfully sent to incomm
    Given webmethods returns 'success' for send to warehouse
    And gift cards are added to cart with recipient details:
      | productCode            | firstName | lastName | email                           | messageText            |
      | PGC1014_venue_coffee   | Siddharam | Rathod   | Siddharam.Rathod@target.com.au  | This is a test message |
      | PGC1014_venue_cocktail | Siddharam | Rathod2  | Siddharam.Rathod2@target.com.au | This is a test message |
    And registered user checkout
    And customer checkout is started
    And delivery mode is 'not changed'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 15.50 |    15.50 |          0.0 |          |
    And a consignment is assigned to warehouse 'Incomm'
    And consignment contains products:
      | product                | qty |
      | PGC1014_venue_coffee   |   1 |
      | PGC1014_venue_cocktail |   1 |
    And consignment carrier is 'NullDigGift'
    And consignment deliveryMode is 'digital-gift-card'
    And consignment deliveryAddress is 'NULL'
    And consignment is sent to webmethods with:
      | consignmentId      | warehouseId     | orderId      | customerFirstName | customerLastName | shippingMethod |
      | NEW_CONSIGNMENT_ID | IncommWarehouse | NEW_ORDER_ID | test              | user             | Email          |
    And consignment entries are sent to webmethods with:
      | productId              | name                                     | denomination | brandId                | giftCardStyle     | giftCardProductId |
      | PGC1014_venue_coffee   | e-Gift Card - Incomm Venue 5 Fine Dining |          5.5 | nondenominatedvenueaus | CIR_000064_00.png |            613366 |
      | PGC1014_venue_cocktail | e-Gift Card - Incomm Venue 6 Fine Dining |         10.0 | nondenominatedvenueaus | CIR_000064_00.png |            613367 |
    And gift recipient details are sent to webmethods for product 'PGC1014_venue_coffee' with:
      | firstName | lastName | email                          | messageText            |
      | Siddharam | Rathod   | Siddharam.Rathod@target.com.au | This is a test message |
    And gift recipient details are sent to webmethods for product 'PGC1014_venue_cocktail' with:
      | firstName | lastName | email                           | messageText            |
      | Siddharam | Rathod2  | Siddharam.Rathod2@target.com.au | This is a test message |
    And auto pick process completes
    And order complete process completes
    And this consignments status is 'SHIPPED'

  Scenario: Physical Gift cards successfully sent to incomm with small parcel as shipping method
    Given webmethods returns 'success' for send to warehouse
    And a cart with entries:
      | product               | qty | price |
      | PHYGC1003_skydive_100 |   2 |   100 |
    And registered user checkout
    And customer checkout is started
    And delivery mode is 'home-delivery'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 200.0 |    200.0 |          0.0 |          |
    And a consignment is assigned to warehouse 'Incomm'
    And consignment contains products:
      | product               | qty |
      | PHYGC1003_skydive_100 |   2 |
    And consignment deliveryMode is 'home-delivery'
    And consignment is sent to webmethods with:
      | consignmentId      | warehouseId     | orderId      | customerFirstName | customerLastName | shippingMethod               |
      | NEW_CONSIGNMENT_ID | IncommWarehouse | NEW_ORDER_ID | test              | user             | AusPostSmallNormalRegistered |
    And consignment entries are sent to webmethods with:
      | productId             | name                                 | denomination | brandId        | giftCardStyle     |
      | PHYGC1003_skydive_100 | $100 e-Gift Card - Adrenalin Skydive |        100.0 | test1adrenalin | CIR_000475_03.png |         

  Scenario: Physical Gift cards successfully sent to incomm with large parcel as shipping method
    Given webmethods returns 'success' for send to warehouse
    And a cart with entries:
      | product               | qty | price |
      | PHYGC1003_skydive_100 |   4 |   100 |
    And registered user checkout
    And customer checkout is started
    And delivery mode is 'home-delivery'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 400.0 |    400.0 |          0.0 |          |
    And a consignment is assigned to warehouse 'Incomm'
    And consignment contains products:
      | product               | qty |
      | PHYGC1003_skydive_100 |   4 |
    And consignment deliveryMode is 'home-delivery'
    And consignment is sent to webmethods with:
      | consignmentId      | warehouseId     | orderId      | customerFirstName | customerLastName | shippingMethod               |
      | NEW_CONSIGNMENT_ID | IncommWarehouse | NEW_ORDER_ID | test              | user             | AusPostLargeRegularRegistered |
    And consignment entries are sent to webmethods with:
      | productId             | name                                 | denomination | brandId        | giftCardStyle     |
      | PHYGC1003_skydive_100 | $100 e-Gift Card - Adrenalin Skydive |        100.0 | test1adrenalin | CIR_000475_03.png |        

@notAutomated
Feature: Google product feed
  In order to provide error less product feed to Google
  As Target online
  I want to provide appropriate data in the product feed

  Scenario Outline: Formatting of data values in Google product feed.
    Given a product in hybris with title as <title> and description as <description>
    When google product feed is run
    Then feed contains the title as <formattedTitle> and description as <formattedDescription>

    Examples: 
      | title                          | description                                                     | formattedTitle                     | formattedDescription                                            |
      | Play-Doh Makin' Station        | Use your "frosting" tool to add "frosting"                      | Play-Doh Makin' Station            | "Use your ""frosting"" tool to add ""frosting"""                |
      | John Frieda                    | ""Silkening"" moisture to ""quench"" parched hair               | John Frieda                        | """""Silkening"""" moisture to """"quench"""" parched hair"     |
      | Hunter Studio 18" x 24" Studio | Ready to showcase your next masterpiece                         | "Hunter Studio 18"" x 24"" Studio" | Ready to showcase your next masterpiece                         |
      | Galaxy Tab 3 10.1" Cover       | Slim and lightweight design constructed with a premium material | "Galaxy Tab 3 10.1"" Cover"        | Slim and lightweight design constructed with a premium material |
      | Target 7" Digital              | Vivid colour on the 7" screen                                   | "Target 7"" Digital"               | "Vivid colour on the 7"" screen"                                |
      | Sony Digital Camera DSCW810S   | A stylish camera with an impressive 2.7" LCD screen             | Sony Digital Camera DSCW810S       | "A stylish camera with an impressive 2.7"" LCD screen"          |
      | Electric Guitar 32"            | Electric Guitar                                                 | "Electric Guitar 32"""             | Electric Guitar                                                 |
      | Kicking Leo Figure 12"         | Mega Mutant Leo has multiple moves, sound effects, and phrases  | "Kicking Leo Figure 12"""          | Mega Mutant Leo has multiple moves, sound effects, and phrases  |

  Scenario Outline: Include sellable product URL in Google feed for each product
    Given a product with size variant code <sizeVariantCode>
    And colour variant code <colourVariantCode>
    And base product code <baseProductCode>
    And title <title>
    When google product feed is run
    Then the product URL in the feed is <URL>

    Examples: 
      | sizeVariantCode | colourVariantCode | baseProductCode | title                       | URL                                                                                                                                                                                                                                       |
      | 57351162        | 57351094          | W774690         | 5 Pack S/S Polo Tops        | http://www.target.com.au/p/5-pack-s-s-polo-tops/57351162?utm_term=57351162&utm_content=5-pack-s-s-polo-tops&utm_source=google&utm_medium=merchant-site&utm_campaign=merchant-site                                                         |
      | 57351193        | 57351179          | W774690         | 5 Pack S/S Polo Tops        | http://www.target.com.au/p/5-pack-s-s-polo-tops/57351193?utm_term=57351193&utm_content=5-pack-s-s-polo-tops&utm_source=google&utm_medium=merchant-site&utm_campaign=merchant-site                                                         |
      | 57351223        | 57351179          | W774690         | 5 Pack S/S Polo Tops        | http://www.target.com.au/p/5-pack-s-s-polo-tops/57351223?utm_term=57351223&utm_content=5-pack-s-s-polo-tops&utm_source=google&utm_medium=merchant-site&utm_campaign=merchant-site                                                         |
      | 54736771        | 54736740          | P54736740       | MODA Bikini Brief - Black   | http://www.target.com.au/p/moda-bikini-brief-black/54736771?utm_term=54736771&utm_content=moda-bikini-brief-black&utm_source=google&utm_medium=merchant-site&utm_campaign=merchant-site                                                   |
      | 54736788        | 54736740          | P54736740       | MODA Bikini Brief - Black   | http://www.target.com.au/p/moda-bikini-brief-black/54736788?utm_term=54736788&utm_content=moda-bikini-brief-black&utm_source=google&utm_medium=merchant-site&utm_campaign=merchant-site                                                   |
      | N/A             | 56094268          | W224512         | Grandeur Bath Towel         | http://www.target.com.au/p/grandeur-bath-towel/56094268?utm_term=56094268&utm_content=grandeur-bath-towel&utm_source=google&utm_medium=merchant-site&utm_campaign=merchant-site                                                           |
      | N/A             | 56094732          | W224512         | Grandeur Bath Towel         | http://www.target.com.au/p/grandeur-bath-towel/56094732?utm_term=56094732&utm_content=grandeur-bath-towel&utm_source=google&utm_medium=merchant-site&utm_campaign=merchant-site                                                           |
      | N/A             | 56357653          | P56357653       | Target Bluetooth Headphones | http://www.target.com.au/p/target-bluetooth-headphones-with-carry-pouch-883/56357653?utm_term=56357653&utm_content=target-bluetooth-headphones-with-carry-pouch-883&utm_source=google&utm_medium=merchant-site&utm_campaign=merchant-site |

  Scenario Outline: Do not include product in Google feed if all stock is reserved.
    Given a product has stock available as <stockAvailable>
    And the product has stock reserved as <stockReserved>
    And show when out of stock flag is <showWhenOutOfStock>
    When google product feed is run
    Then the product is <prodOnFeed> the feed

    Examples: 
      | stockAvailable | stockReserved | showWhenOutOfStock | prodOnFeed    |
      | 2              | 0             | N/A                | included on   |
      | 2              | 1             | N/A                | included on   |
      | 2              | 2             | true               | included on   |
      | 2              | 2             | false              | excluded from |
      | 0              | 0             | true               | included on   |
      | 0              | 0             | false              | excluded from |
      | 2              | 4             | true               | included on   |
      | 2              | 4             | false              | excluded from |

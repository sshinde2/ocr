@notAutomated
Feature: InStore Products Import
  As a Target Web Coordinator
  I want to set a product as display-only in STEP
  So that downstream systems know that the product is not available for sale online

  Scenario Outline: Edit In Store Only product in STEP
    Given a STEP user
    When the user is editing a product at the level <level>
    Then there is a 'Display Only' attribute in the 'Web Description'
    And the attribute is editable
    And the  user is able to set it to 'Y' or 'N'

    Examples: 
      | level              |
      | hierarchy          |
      | Sub hierarchy      |
      | non sellable color |
      | sellable color     |
      | size               |

  Scenario Outline: Inheritance of In Store Only attribute
    Given a STEP user
    And the user is editing a product at level <level>
    And the product has children at levels <childrenLevels>
    When the user sets the 'Display Only' attribute on the product
    And the attribute has NOT set at children levels
    Then all children level product inherit the "Display Only' value set

    Examples: 
      | level               | childrenLevels                                  |
      | hierarchy           | Subhierarchy ,Sellable color,non-Sellable color |
      | non-sellable colour | size (single or multiple)                       |

  Scenario Outline: Override In Store Only attribute on variant
    Given a STEP user
    And the user is editing a product at level <level>
    And the 'Display Only' attribute is currently inherited from level <inheritedFrom>
    When the user sets the 'Display Only' attribute on the level
    Then it is considered to be overridden and is no longer inherited
    And changes to the parent are no longer inherited by the product

    Examples: 
      | level               | inheritedFrom                 |
      | sellable colour     | hierarchy                     |
      | non-sellable colour | hierarchy                     |
      | size                | non-sellable colour,hierarchy |

  Scenario Outline: Viewing the product in HMC
    Given a HMC user
    When the user is viewing a product at level <level>
    Then there  'Display Only' attribute is <displayStatus>
    And the attribute is writable and the value of the attribute is <value>

    Examples: 
      | level               | displayStatus | value          |
      | non-sellable colour | displayed     | n/a            |
      | sellable colour     | displayed     | Yes or No      |
      | size                | displayed     | Yes or No      |
      | base                | not displayed | Not Applicable |

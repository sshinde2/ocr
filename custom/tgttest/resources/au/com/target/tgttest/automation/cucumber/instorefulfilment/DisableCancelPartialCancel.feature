@dummyStoreWarehouseData @cleanOrderData @cleanConsignmentData
Feature: Instore Fulfilment - Disable Cancel and Partial cancel
  In order not to impact the pick, pack and ship activities in store
  As online
  I want the partial cancel and cancel functions in CS cockpit disabled for any consignment assigned to a store with status waved, picked or shipped

  Background: 
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      |
      | auto11000001    | WAVED                  |
      | auto11000002    | SHIPPED                |
      | auto11000003    | PICKED                 |
      | auto11000004    | SENT_TO_WAREHOUSE      |
      | auto11000005    | CONFIRMED_BY_WAREHOUSE |
      | auto11000006    | PACKED                 |

  Scenario Outline: Disable Cancel and Partial Cancel
    When CS cockpit checks if the consignment '<consignmentCode>' can be cancelled
    Then the cancelled response is '<response>'

    Examples: 
      | consignmentCode | response |
      | auto11000001    | NO       |
      | auto11000002    | NO       |
      | auto11000003    | NO       |
      | auto11000004    | YES      |
      | auto11000005    | YES      |
      | auto11000006    | NO       |

@cartProductData @cleanOrderData @cleanConsignmentData @storeFulfilmentCapabilitiesData
Feature: Instore Fulfilment - Process Instore Completion and adjust fastline reserve
  
  As the online store I want to re-adjust Fastline stock if the order is fulfilled from store so that the website accurately reflects the stock available to sell.
  As online, I want a method to transition a consignment into a 'packed' status, so that a consignment can proceed through the workflow in the OFC.
  As the online store I want to be able to process confirmation of complete for INSTORE_PICKUP consignments routed to stores so that the consignment can be processed through Hybris

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |

  Scenario: Order fulfilled by fastline
    Given a consignment sent to fastline with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When the consignment is full picked by fastline
    Then fastline pick process completes
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 8         |

  Scenario: Order fulfilled by store
    Given a consignment sent to a store '7032' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When the consignment is completed instore
    Then fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |

  Scenario Outline: Instore pickup order fulfilled by store with Parcel Count from OFC
    Given a consignment of type 'INSTORE_PICKUP' is set up for store '7032'
    When the consignment is completed instore with parcel count as <parcelCount>
    Then the CnC extract is '<cncOutcome>' with parcel count <parcelCount>
    And consignment status is '<status>'
    And the ofc error message is '<errorMessage>'

    Examples: 
      | parcelCount | cncOutcome  | status  | errorMessage      |
      | 2           | created     | SHIPPED |                   |
      | 0           | Not Created | PICKED  | ERR_INVALID_PARAM |

  Scenario Outline: Interstore or home delivery orders fulfilled by store
    Given a consignment of type '<ofcOrderType>' is set up for store '7032'
    When the consignment is completed instore with parcel list:
      | height | width | length | weight |
      | 1      | 2     | 3      | 4      |
      | 5      | 6     | 7      | 8      |
    Then consignment status is 'PACKED'
    And consignment parcel count is 2
    And consignment parcels are:
      | height | width | length | weight |
      | 1      | 2     | 3      | 4      |
      | 5      | 6     | 7      | 8      |

    Examples: 
      | ofcOrderType        |
      | INTERSTORE_DELIVERY |
      | CUSTOMER_DELIVERY   |

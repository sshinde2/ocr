@cartProductData
Feature: Instore Fulfilment - Stock Lookup For Consignment
  As the online store 
  I want to know the stock levels for all the products in a consignment 
  So that I can determine if an order could be successfully picked by a nominated store
  
  StockStatus is determined based on following range
   StockStatus	Range		
   HIGH_STOCK	>=25		
   MEDIUM_STOCK	>=10 & <25	
   LOW_STOCK	>=0 & <10	
   NO_STOCK		0

  Background: 
    Given stores with stock:
      | store | stock                                                        |
      | 7001  | 20 of 10000011, 0 of 10000012, 30 of 10000021, 5 of 10000022 |

  Scenario: consignment is sent to store and stock check is performed
    Given a consignment sent to store '7001' with existing stocks and products:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
      | 10000021 | 4   | 15    |
      | 10000022 | 4   | 15    |
      | 10000012 | 4   | 15    |
    When the stock levels for the consignment is checked for store '7001'
    Then the stock levels at the store is:
      | productCode | stockLevel | stockStatus  |
      | 10000011    | 20         | MEDIUM_STOCK |
      | 10000021    | 30         | HIGH_STOCK   |
      | 10000022    | 5          | LOW_STOCK    |
      | 10000012    | 0          | NO_STOCK     |
  
  Scenario: consignment is sent to store and stock check is performed
    Given a consignment sent to store '7001' with existing stocks and products:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
      | 10000021 | 4   | 15    |
      | 10000022 | 4   | 15    |
      | 10000012 | 4   | 15    |
    And feature 'useCacheStockForFulfillment' is enabled
    When the stock levels for the consignment is checked for store '7001'
    Then the stock levels at the store is:
      | productCode | stockLevel | stockStatus  |
      | 10000011    | 20         | MEDIUM_STOCK |
      | 10000021    | 30         | HIGH_STOCK   |
      | 10000022    | 5          | LOW_STOCK    |
      | 10000012    | 0          | NO_STOCK     |
      
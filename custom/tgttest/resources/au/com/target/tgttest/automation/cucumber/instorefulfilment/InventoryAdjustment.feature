@cartProductData @cleanOrderData @cleanConsignmentData @storeFulfilmentCapabilitiesData
Feature: Instore Fulfilment - Processing instore fulfilment - Sends an inventory adjustment to cache via webmethods
  
  As the online store I want to ensure inventory adjustment is sent to keep the stock positions accurate in cache.
  
  Scenario: Order fulfilled by store
    Given a consignment sent to a store '7032' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And the 'inventoryAdjustmentUsingWM' feature switch is enabled
    When the consignment is completed instore
    Then inventory adjustment is sent to webmethods with the following entries:
      | productCode  | store    | adjustmentQuantity |
      | 10000011     | 7032     | -2        		     |
      | 10000011     | 5599     | 2    		     |


@cleanOrderData @cleanConsignmentData
Feature: Instore Fulfilment - Automatic re-routing of non accepted orders depending on pull back tolerance.
  
  In order to timely fulfil the orders
  As the online store
  I want to automatically re-route an "Open" order that has not been accepted by a store at a defined time of the day
  
  As part of this feature if order is not acknowledged by the store with in the pull back tolerance.
  a) The consignment associated with the store needs to be Cancelled.
  b) A reason needs to be set on the consignment
  c) A new consignment needs to be created with warehouse as fastline

  Scenario Outline: Non Accepted orders are re routed to another warehouse if they are not accepted by a store with in the defined tolerance time
    Given some existing orders routed to store:
      | order       | warehouse   | consignmentStatus      | minutesSinceCreated |
      | auto1000001 | Robina      | CONFIRMED_BY_WAREHOUSE | 100                 |
      | auto1000002 | Robina      | Waved                  | 80                  |
      | auto1000003 | Rockhampton | CONFIRMED_BY_WAREHOUSE | 90                  |
      | auto1000004 | Fastline    | SENT_TO_WAREHOUSE      | 79                  |
      | auto1000005 | Fastline    | Picked                 | 81                  |
      | auto1000006 | Fastline    | Shipped                | 90                  |
      | auto1000007 | Robina      | Shipped                | 200                 |
      | auto1000008 | Rockhampton | Waved                  | 60                  |
      | auto1000009 | Rockhampton | Shipped                | 20                  |
      | auto1000010 | Fastline    | CONFIRMED_BY_WAREHOUSE | 81                  |
      | auto1000011 | Robina      | CONFIRMED_BY_WAREHOUSE | 80                  |
      | auto1000012 | Rockhampton | CONFIRMED_BY_WAREHOUSE | 78                  |
      | auto1000013 | Morley      | CONFIRMED_BY_WAREHOUSE | 90                  |
    And order age tolerance is set to 80 Minutes
    And the excludedStates are '<excludedStates>' and includedStates are '<includedStates>' for the pullback job
    When the open orders pull back process is initiated
    Then orders are updated as follows
      | order       | consignmentOneStatus   | consignmentOneReason | consignmentTwoStatus | consignmentTwoWarehouse |
      | auto1000001 | <nonWaOneStatus>       | <nonWaReason>        | <nonWaTwoStatus>     | <nonWaUpdatedWarehouse> |
      | auto1000002 | Waved                  | NULL                 | N/A                  | Robina                  |
      | auto1000003 | <nonWaOneStatus>       | <nonWaReason>        | <nonWaTwoStatus>     | <nonWaUpdatedWarehouse> |
      | auto1000004 | SENT_TO_WAREHOUSE      | NULL                 | N/A                  | Fastline                |
      | auto1000005 | Picked                 | NULL                 | N/A                  | Robina                  |
      | auto1000006 | Shipped                | NULL                 | N/A                  | Fastline                |
      | auto1000007 | Shipped                | NULL                 | N/A                  | Robina                  |
      | auto1000008 | Waved                  | NULL                 | N/A                  | Rockhampton             |
      | auto1000009 | Shipped                | NULL                 | N/A                  | Rockhampton             |
      | auto1000010 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Fastline                |
      | auto1000011 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Robina                  |
      | auto1000012 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Rockhampton             |
      | auto1000013 | <waOneStatus>          | <waReason>           | <waTwoStatus>        | <waUpdatedWarehouse>    |
    And clear states config in the pullback job

    Examples:
      | Title                                 | excludedStates | includedStates | waReason         | waOneStatus            | waTwoStatus         | nonWaReason      | nonWaOneStatus         | nonWaTwoStatus      | waUpdatedWarehouse | nonWaUpdatedWarehouse |
      | pullback job for all states           | NULL           | NULL           | ACCEPT_TIMEOUT   | Cancelled              | SENT_TO_WAREHOUSE   | ACCEPT_TIMEOUT   | Cancelled              | SENT_TO_WAREHOUSE   | Fastline           | Fastline              |
      | pullback job for all states except WA | WA             | NULL           | NULL             | CONFIRMED_BY_WAREHOUSE | N/A                 | ACCEPT_TIMEOUT   | Cancelled              | SENT_TO_WAREHOUSE   | NO_CHANGE          | Fastline              |
      | pullback job for WA                   | NULL           | WA             | ACCEPT_TIMEOUT   | Cancelled              | SENT_TO_WAREHOUSE   | NULL             | CONFIRMED_BY_WAREHOUSE | N/A                 | Fastline           | NO_CHANGE             |

  Scenario: Non Accepted orders are re routed to another warehouse if they are not accepted by a store and tolerance time is not set
    Given some existing orders routed to store:
      | order       | warehouse   | consignmentStatus      | minutesSinceCreated |
      | auto1000001 | Robina      | CONFIRMED_BY_WAREHOUSE | 1                   |
      | auto1000002 | Robina      | Waved                  | 0                   |
      | auto1000003 | Rockhampton | CONFIRMED_BY_WAREHOUSE | 30                  |
      | auto1000004 | Fastline    | SENT_TO_WAREHOUSE      | 0                   |
      | auto1000005 | Fastline    | Picked                 | 1                   |
      | auto1000006 | Fastline    | Shipped                | 2                   |
      | auto1000007 | Robina      | Shipped                | 3                   |
      | auto1000008 | Rockhampton | Waved                  | 0                   |
      | auto1000009 | Rockhampton | Shipped                | 0                   |
      | auto1000010 | Fastline    | CONFIRMED_BY_WAREHOUSE | 1                   |
      | auto1000011 | Robina      | CONFIRMED_BY_WAREHOUSE | 0                   |
      | auto1000012 | Rockhampton | CONFIRMED_BY_WAREHOUSE | 0                   |
    And order age tolerance is not set
    When the open orders pull back process is initiated
    Then orders are updated as follows
      | order       | consignmentOneStatus   | consignmentOneReason | consignmentTwoStatus | consignmentTwoWarehouse |
      | auto1000001 | Cancelled              | ACCEPT_TIMEOUT       | SENT_TO_WAREHOUSE    | Fastline                |
      | auto1000002 | Waved                  | NULL                 | N/A                  | Robina                  |
      | auto1000003 | Cancelled              | ACCEPT_TIMEOUT       | SENT_TO_WAREHOUSE    | Fastline                |
      | auto1000004 | SENT_TO_WAREHOUSE      | NULL                 | N/A                  | Fastline                |
      | auto1000005 | Picked                 | NULL                 | N/A                  | Robina                  |
      | auto1000006 | Shipped                | NULL                 | N/A                  | Fastline                |
      | auto1000007 | Shipped                | NULL                 | N/A                  | Robina                  |
      | auto1000008 | Waved                  | NULL                 | N/A                  | Rockhampton             |
      | auto1000009 | Shipped                | NULL                 | N/A                  | Rockhampton             |
      | auto1000010 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Fastline                |
      | auto1000011 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Robina                  |
      | auto1000012 | CONFIRMED_BY_WAREHOUSE | NULL                 | N/A                  | Rockhampton             |

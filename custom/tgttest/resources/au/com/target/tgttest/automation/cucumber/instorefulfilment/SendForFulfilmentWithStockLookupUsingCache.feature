@cartProductData @deliveryModeData @storeFulfilmentCapabilitiesData
Feature: Instore Fulfilment - Send consignment for fulfilment and auto acknowledge for instore fulfilment
  As the online store I want to be able to assign a consignment to a store for fulfilment so that I can provide the store with instructions to fulfil the consignment
  As the online store I want to be able to automatically acknowledge a consignment for orders sent to stores for fulfilment so that the store can proceed to pick the consignment without needing to send an acknowledgement
  As the online store I want to assign AusPost carrier to consignments for orders sent to stores for fulfilment that will be manifested so that the consignment has the correct carrier assigned
  
  Robina store 7126 is enabled for all types of instore fulfilment
  Any cart with weight < 3kg takes StarTrackHD as carrier for home-delivery orders

  Background: 
    Given stores with fulfilment capability and stock:
      | store  | inStock | instoreEnabled | deliveryModesAllowed            | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | Robina | Yes     | Yes            | click-and-collect,home-delivery | Yes                      | yes                         |
    And feature 'useCacheStockForFulfillment' is enabled


  Scenario: Consignment for store with instore pickup
    When a consignment sent to a store '7126'
    Then a single consignment is created
    And consignment is not sent to Fastline
    And consignment status is 'CONFIRMED_BY_WAREHOUSE'
    And consignment carrier is 'NullCnC'
    And the assigned warehouse will be 'Robina'
    And the OFC order type in portal for store '7126' is 'INSTORE_PICKUP'

  Scenario: Consignment for interstore delivery to Maroochydore
    Given any click-and-collect cart for pickup at '7038'
    When order is placed
    Then a single consignment is created
    And consignment is not sent to Fastline
    And consignment status is 'CONFIRMED_BY_WAREHOUSE'
    And consignment carrier is 'AustraliaPostInstoreCNC'
    And the assigned warehouse will be 'Robina'
    And the OFC order type in portal for store '7126' is 'INTERSTORE_DELIVERY'

  Scenario: Consignment for customer delivery near Robina
    Given any home-delivery cart
    And the delivery address is '19,Robina Town Centre Drive,Robina,QLD,4226'
    When order is placed
    Then a single consignment is created
    And consignment is not sent to Fastline
    And consignment status is 'CONFIRMED_BY_WAREHOUSE'
    And consignment carrier is 'AustraliaPostInstoreHD'
    And the assigned warehouse will be 'Robina'
    And the OFC order type in portal for store '7126' is 'CUSTOMER_DELIVERY'

@cleanOrderData @cleanConsignmentData
Feature: Instore Fulfilment - Manifest - Resend Manifest

  Background: 
    Given store number for ofc portal is '7032'
    And list of consignments for the store is:
      | consignmentCode | deliveryMethod    | cncStoreNumber | consignmentStatus | manifestID   | packDate    | boxes | customer | destination        |
      | autox11000001   | home-delivery     |                | Shipped           | autox0000123 | 25 days ago |       | BB BBBB  | Seaford, VIC, 3198 |
      | autox11000002   | home-delivery     |                | Shipped           | autox0000124 | 10 days ago |       | DD DDDD  | Tarneit, VIC, 3029 |
      | autox11000003   | home-delivery     |                | Shipped           | autox0000125 | 8 days ago  | 2     | GG GGGG  | Seaford, VIC, 3198 |
      | autox11000004   | home-delivery     |                | Shipped           | autox0000126 | 7 days ago  | 3     | GG GGGG  | Seaford, VIC, 3198 |
      | autox11000005   | home-delivery     |                | Shipped           | autox0000127 | 7 days ago  | 2     | DD DDDD  | Seaford, VIC, 3198 |
      | autox11000006   | home-delivery     |                | Shipped           | autox0000128 | yesterday   | 1     | NN NNNN  | Seaford, VIC, 3198 |
      | autox11000012   | click-and-collect | 2000           | Shipped           | autox0000129 | today       | 1     | AA AAAA  | Seaford, VIC, 3198 |
      | autox11000013   | click-and-collect | 2000           | Shipped           | autox0000130 | today       |       | CC CCCC  | Tarneit, VIC, 3198 |

  Scenario: Manifests that are not transmitted are transmitted in a cron job.
    Given list of manifests for the store is:
      | manifestID   | manifestDate   | manifestTime | transmitted | webmethodsResponse |
      | autox0000123 | 25 days before |              | true        |                    |
      | autox0000124 | 10 days before |              | true        |                    |
      | autox0000125 | 8 days before  |              | false       | success            |
      | autox0000126 | 7 days before  | 23:59        | true        |                    |
      | autox0000127 | 7 days before  | 00:01        | false       | error              |
      | autox0000128 | yesterday      |              | false       | unavailable        |
      | autox0000129 | today          |              | true        |                    |
      | autox0000130 | today          |              | false       | success            |
    When retransmit of the manifests is initiated
    Then the manifests after retransmit are:
      | manifestID   | transmitted |
      | autox0000123 | true        |
      | autox0000124 | true        |
      | autox0000125 | true        |
      | autox0000126 | true        |
      | autox0000127 | false       |
      | autox0000128 | false       |
      | autox0000129 | true        |
      | autox0000130 | true        |

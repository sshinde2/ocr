@cleanOrderData @cleanConsignmentData
Feature: Instore Fulfilment - Manifests - Display Hard Copy Manifest
  
  In Order to print manifest details as Store Agent I want to print a hard copy manifest
  
  Notes
  * Hard copy is retrived after manifest transmit is run
  * Hoppers store is number 7064, at 50 Old Geelong Road, Hoppers Crossing, VIC, 3030
  * Consignments sorted by chargeZone, consignmentId

  Background: 
    Given store number for ofc portal is '7032'

  Scenario: View a manifest hard copy when user selects a manifest.
    Given list of manifests for the store is:
      | manifestID | manifestDate | manifestTime |
      | x0000100   | yesterday    | 10:00        |
    And list of consignments for the store is:
      | consignmentCode | manifestID | deliveryMethod    | cncStoreNumber | consignmentStatus | customer    | destination                                      | trackingId |
      | autox0000100    | x0000100   | home-delivery     |                | Shipped           | John Smith  | Seaford, VIC, 3198                               | 123456     |
      | autox0000101    | x0000100   | click-and-collect | 7064           | Shipped           | Jane Doe    | 50 Old Geelong Road, Hoppers Crossing, VIC, 3030 | 123457     |
      | autox0000102    | x0000100   | home-delivery     |                | Shipped           | Steve Bryan | 1 Aberdeen St, Newtown, VIC, 3220                | 123458     |
    And consignments have parcels:
      | consignmentCode | height | width | length | weight |
      | autox0000100    | 1      | 2     | 3      | 4.27   |
      | autox0000100    | 5      | 6     | 7      | 8      |
      | autox0000101    | 5      | 6     | 7      | 5.29   |
      | autox0000102    | 5      | 6     | 7      | 99.999 |
      | autox0000102    | 5      | 6     | 7      | 00.999 |
    When ofc retrieves hard copy manifest 'x0000100' to view
    Then hard copy manifest has core details:
      | mlid | storeAddress                          | manifestDate       | manifestId | totalArticles | totalWeight |
      | JDQ  | Station Street, Camberwell, VIC, 3124 | yesterday at 10:00 | x0000100   | 5             | 118.558     |
    And hard copy manifest has consignments:
      | consignmentId | chargeZone | chargeZoneDescription | chargeCode | deliveryName              | deliveryPostcode | deliverySuburb   | articles | weight  |
      | 123458        | GL         | Geelong               | S1         | Steve Bryan               | 3220             | Newtown          | 2        | 100.998 |
      | 123456        | V1         | Melbourne Metro       | S1         | John Smith                | 3198             | Seaford          | 2        | 12.27   |
      | 123457        | V1         | Melbourne Metro       | S1         | Target - Hoppers Crossing | 3030             | Hoppers Crossing | 1        | 5.29    |

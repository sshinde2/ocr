@cartProductData @storeFulfilmentCapabilitiesData
Feature: Instore Fulfilment - Global Routing Rules for category exclusion
  
    The test product and category relation have been set in the cartProductData:
    
    | Super categories | Target product | Size Variant Product |
    | W93746,355       | W100000        | 10000011             |
    | W93747,330       | W100001        | 10000111             |
  
    Test category structure:
    
    Category: 
    
    AP01--W93741--W93745--W93746
                       |--W93747
    Merch department:  
    
    TMDApparel--355
             |--330

  Background: 
    Given stores with fulfilment capability and stock:
      | store  | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | Robina | Yes     | QLD   | Yes            | Yes                      | Yes                         |

  Scenario: Global category exclusion rule exit and consignment was routing to the store.
    Given the global category exclusion is
      | categoryCode | excludeStartDate | excludeEndDate |
      | 920          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Robina'

  Scenario: Global category exclusion deny routing to store with super category W93746 of 10000011 in the category exclusion list.
    Given the global category exclusion is
      | categoryCode | excludeStartDate | excludeEndDate |
      | W93746       | NOT POPULATED    | NOT POPULATED  |
      | 920          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Global category exclusion deny routing to store with second level super category W93745 of 10000011 in the category exclusion list.
    Given the global category exclusion is
      | categoryCode | excludeStartDate | excludeEndDate |
      | W93745       | NOT POPULATED    | NOT POPULATED  |
      | 920          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Global category exclusion deny routing to store with super merch department 355 of 10000011 in the category exclusion list.
    Given the global category exclusion is
      | categoryCode | excludeStartDate | excludeEndDate |
      | 355          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Global category exclusion deny routing to store with second level super merch department TMDApparel of 10000011 in the category exclusion list.
    Given the global category exclusion is
      | categoryCode | excludeStartDate | excludeEndDate |
      | TMDApparel   | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario Outline: Global category exclusion for date range
    Given the global category exclusion is '<categoryCode>' with start date '<exclusionStartDate>' and end date '<exclusionEndDate>'
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000111 | 2   | 15    |
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | categoryCode | exclusionStartDate | exclusionEndDate | warehouse | comment                                                                                    |
      | W93746       | NOT POPULATED      | NOT POPULATED    | Fastline  | global category exclusion is active without start date and end date                        |
      | W93746       | NOT POPULATED      | BEFORE NOW       | Robina    | global category exclusion is not active with no start date and end date before now         |
      | W93746       | NOT POPULATED      | AFTER NOW        | Fastline  | global category exclusion is active with no start date and end date after now              |
      | W93746       | BEFORE NOW         | NOT POPULATED    | Fastline  | global category exclusion is active with start date before now and no end date             |
      | W93746       | BEFORE NOW         | AFTER NOW        | Fastline  | global category exclusion is active with start date before now and end date after now      |
      | W93746       | BEFORE NOW         | BEFORE NOW       | Robina    | global category exclusion is not active with start date before now and end date before now |
      | W93746       | AFTER NOW          | NOT POPULATED    | Robina    | global category exclusion is not active with start date after now and no end date          |
      | W93746       | AFTER NOW          | AFTER NOW        | Robina    | global category exclusion is not active with start date after now and end date after now   |

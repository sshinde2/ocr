@cartProductData @storeFulfilmentCapabilitiesData
Feature: Instore Fulfilment - Ability to turn off interstore CnC orders at a global level
  
  As an administrator, I want the ability to restrict any interstore transfers from being assigned to stores, 
  so that I can easily switch off the capability across the board without needing to configure each individual store.

  Background: 
    Given stores with fulfilment capability:
      | store       | instoreEnabled | deliveryModesAllowed            | allowDeliveryToSameStore | allowDeliveryToAnotherStore | inStock |
      | Camberwell  | Yes            | click-and-collect,home-delivery | Yes                      | Yes                         | Yes     |
      | Robina      | Yes            | click-and-collect,home-delivery | Yes                      | No                          | Yes     |
      | Rockhampton | Yes            | click-and-collect,home-delivery | No                       | Yes                         | Yes     |
      | Morley      | Yes            | click-and-collect,home-delivery | No                       | No                          | Yes     |

  Scenario Outline: Orders sent to store based on global and store specific interstore flag
    Given the global interstore routing capability is '<allowInterstore>'
    And any click-and-collect cart for pickup at '<cncStore>'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | allowInterstore | cncStore         | warehouse   |
      | Yes             | Camberwell       | Camberwell  |
      | No              | Camberwell       | Camberwell  |
      | Yes             | Hoppers Crossing | Camberwell  |
      | No              | Hoppers Crossing | Fastline    |
      | Yes             | Perth            | Fastline    |
      | Yes             | Brisbane         | Rockhampton |

  Scenario Outline: Order sent to store based on delivery mode supported
    Given the global interstore routing capability is '<allowInterstore>'
    And any home-delivery cart
    And the delivery address is '1 Aberdeen Street, Newtown, VIC, 3218'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | allowInterstore | warehouse  |
      | Yes             | Camberwell |
      | No              | Camberwell |

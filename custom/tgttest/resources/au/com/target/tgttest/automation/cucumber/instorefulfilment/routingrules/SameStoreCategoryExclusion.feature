@cartProductData @storeFulfilmentCapabilitiesData
Feature: Instore Fulfilment - Same Store and PPD Routing Rules for category exclusion
  
    The test product and category relation have been set in the cartProductData:
    
    | Super categories | Target product | Size Variant Product |
    | W93748,355       | W100002        | 10000211             |
    | W93749,335       | W100003        | 10000311             |
  
    Test category structure:
    
    Category: 
    
    AP01--W93741--W93745--W93748
                       |--W93749
    Merch department:  
    
    TMDApparel--355
             |--330

  Background: 
    Given stores with fulfilment capability and stock:
      | store | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore |
      | 7126  | Yes     | QLD   | Yes            | Yes                      | Yes                         |
      | 7049  | No      | QLD   | Yes            | Yes                      | Yes                         |

  Scenario: Robina store category exclusion rule exist and consignment was routing to the store.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | 920          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Robina'

  Scenario: Robina store category exclusion deny routing to store with super category W93748 of 10000211 in the Robina store category exclusion list.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | W93748       | NOT POPULATED    | NOT POPULATED  |
      | 920          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Robina store category exclusion deny routing to store with second level super category W93745 of 10000211 in the Robina store category exclusion list.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | W93745       | NOT POPULATED    | NOT POPULATED  |
      | 920          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Robina store category exclusion deny routing to store with super merch department 355 of 10000211 in the Robina store category exclusion list.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | 355          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Robina store category exclusion deny routing to store with second level super merch department TMDApparel of 10000211 in the category exclusion list.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | TMDApparel   | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Robina store category exclusion deny routing to store with second level super category W93745 with start date and end date of 10000211 in the Robina store category exclusion list.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | W93745       | BEFORE NOW       | AFTER NOW      |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario: Robina store category exclusion deny routing to store with second level super category W93745 with future date of 10000211 in the Robina store category exclusion list.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | W93745       | AFTER NOW        | AFTER NOW      |
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Robina'

  Scenario: Robina store category exclusion rule exist and consignment was routing to the interstore.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | 920          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7049' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Robina'

  Scenario: Robina store category exclusion deny routing to store with super category W93748 of 10000211 in the Robina store category exclusion list.
    Given category exclusion for store '7126':
      | categoryCode | excludeStartDate | excludeEndDate |
      | W93748       | EXACTLY NOW      | NOT POPULATED  |
      | 920          | NOT POPULATED    | NOT POPULATED  |
      | 980          | NOT POPULATED    | NOT POPULATED  |
    When a consignment sent to a store '7049' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be 'Fastline'

  Scenario Outline: same store category exclusion for date range
    Given the same store category exclusion is '<categoryCode>' with start date '<exclusionStartDate>' and end date '<exclusionEndDate>' for store '7126'
    When a consignment sent to a store '7126' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | categoryCode | exclusionStartDate | exclusionEndDate | warehouse | comment                                                                                        |
      | W93741       | NOT POPULATED      | NOT POPULATED    | Fastline  | same store category exclusion is active without start date and end date                        |
      | W93745       | NOT POPULATED      | BEFORE NOW       | Robina    | same store category exclusion is not active with no start date and end date before now         |
      | W93748       | NOT POPULATED      | AFTER NOW        | Fastline  | same store category exclusion is active with no start date and end date after now              |
      | W93741       | BEFORE NOW         | NOT POPULATED    | Fastline  | same store category exclusion is active with start date before now and no end date             |
      | W93741       | BEFORE NOW         | AFTER NOW        | Fastline  | same store category exclusion is active with start date before now and end date after now      |
      | W93748       | BEFORE NOW         | BEFORE NOW       | Robina    | same store category exclusion is not active with start date before now and end date before now |
      | W93745       | AFTER NOW          | NOT POPULATED    | Robina    | same store category exclusion is not active with start date after now and no end date          |
      | W93748       | AFTER NOW          | AFTER NOW        | Robina    | same store category exclusion is not active with start date after now and end date after now   |

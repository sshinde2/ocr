@cartProductData @storeFulfilmentCapabilitiesData @cleanOrderData @cleanConsignmentData
Feature: Instore Fulfilment - Routing Rules for home delivery and interstore fulfilment
  As the online store, I want to be able to apply routing rules to a consignment, so that I can identify which warehouse(s) could fulfil a given consignment
  
  If instore pickup is not available for an order, then store fulfilment capabilities are checked 
  to remove stores that cannot fulfil orders.
  
  Note: by default the global rules allow instore pickup orders.
  Normal Products - 10000211,10000311
  Bulky2 Products - 10000510
  Store details :
       store Name   store number
       Robina       7038
       Camberwell   7064
       Morley       7043
       Rockhampton  8235

  Background: 
    Given stores with fulfilment capability:
      | store       | instoreEnabled | deliveryModesAllowed                                                 | allowDeliveryToSameStore | allowDeliveryToAnotherStore | inStock | clearProductExclusions | clearCategoryExclusions |
      | Robina      | Yes            | click-and-collect,home-delivery,ebay-click-and-collect,eBay-delivery | Yes                      | yes                         | yes     | true                   | true                    |
      | Camberwell  | Yes            | click-and-collect                                                    | Yes                      | No                          | yes     | true                   | true                    |
      | Morley      | No             | na                                                                   | na                       | na                          | yes     | true                   | true                    |
      | Rockhampton | Yes            | click-and-collect,home-delivery                                      | Yes                      | yes                         | no      | true                   | true                    |
    And delivery modes allowed for instore fulfilment are 'click-and-collect,ebay-click-and-collect,home-delivery,eBay-delivery'

  Scenario Outline: Orders are only sent to store if store has the right capability.
    Given any cart
    And delivery mode is '<delivery-mode>'
    And the delivery address is '<address>'
    And order cnc store is '<cnc_store>'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | delivery-mode          | address                                 | cnc_store | warehouse | comment                                             |
      | home-delivery          | 446,Ann Street,Brisbane City,QLD,4000   | na        | Robina    | Robina can do home-delivery                         |
      | click-and-collect      | na                                      | 7038      | Robina    | Robina can do cnc to Maroochydore                   |
      | home-delivery          | 8/15A,Hooker Road,Werribee,VIC,3030     | na        | Fastline  | Camberwell cannot do home-delivery                  |
      | click-and-collect      | na                                      | 7064      | Fastline  | Camberwell cannot do instore cnc to Hoppers         |
      | click-and-collect      | na                                      | 7043      | Fastline  | Morley cannot do anything                           |
      | home-delivery          | 180,Drake Street, Morley,WA,6062        | na        | Fastline  | Morley cannot do anything                           |
      | home-delivery          | 11,Canning Street, Rockhampton,QLD,4700 | na        | Robina    | Rockhampton has no stock                            |
      | click-and-collect      | na                                      | 8235      | Robina    | CNC store is Gladstone but Rockhampton has no stock |
      | eBay-delivery          | 446,Ann Street,Brisbane City,QLD,4000   | na        | Robina    | Robina can do eBay-delivery                         |
      | ebay-click-and-collect | na                                      | 7038      | Robina    | Robina can do cnc to Maroochydore                   |
      | eBay-delivery          | 8/15A,Hooker Road,Werribee,VIC,3030     | na        | Fastline  | Camberwell cannot do eBay-delivery                  |
      | ebay-click-and-collect | na                                      | 7064      | Fastline  | Camberwell cannot do instore cnc to Hoppers         |
      | ebay-click-and-collect | na                                      | 7043      | Fastline  | Morley cannot do anything                           |
      | eBay-delivery          | 180,Drake Street, Morley,WA,6062        | na        | Fastline  | Morley cannot do anything                           |
      | eBay-delivery          | 11,Canning Street, Rockhampton,QLD,4700 | na        | Robina    | Rockhampton has no stock                            |
      | ebay-click-and-collect | na                                      | 8235      | Robina    | CNC store is Gladstone but Rockhampton has no stock |

  Scenario Outline: order which would normally go to store, with products of different sizes
    Given product weight limit for store delivery for 'Robina' is <limit> kg
    And a cart with entries with weight:
      | product  | weight    | qty | price |
      | 10000011 | <weight1> | 1   | 5     |
      | 10000012 | <weight2> | 1   | 5     |
    And delivery mode is '<deliveryMode>'
    And the delivery address is '446,Ann Street,Brisbane City,QLD,4000'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | limit | weight1 | weight2 | deliveryMode  | warehouse | comment                      |
      | 2     | 1       | 1       | home-delivery | Robina    | Under limit                  |
      | 2     | 2       | 1       | home-delivery | Robina    | Under limit                  |
      | 2     | 3       | 1       | home-delivery | Fastline  | Over limit                   |
      | 3     | 3       | 1       | home-delivery | Robina    | Under limit                  |
      | 2     | 2.01    | 1       | home-delivery | Fastline  | Over limit                   |
      | 2     | 0       | 1       | home-delivery | Robina    | 0 considered as under limit  |
      | 2     | 1       | 3       | home-delivery | Fastline  | Second product over limit    |
      | 0     | 1       | 1       | home-delivery | Robina    | 0 means rule turned off      |
      | 0     | 3       | 1       | home-delivery | Robina    | 0 means rule turned off      |
      | 2     |         | 1       | home-delivery | Fastline  | Cannot read weight so reject |
      | null  | 22      | 1       | home-delivery | Robina    | Not set, default to 22       |
      | null  | 23      | 1       | home-delivery | Fastline  | Not set, default to 22       |
      | 3     | 2.99    | 1       | home-delivery | Robina    | Under limit                  |
      | 2     | 1       | 1       | eBay-delivery | Robina    | Under limit                  |
      | 2     | 3       | 1       | eBay-delivery | Fastline  | Over limit                   |
   

  Scenario Outline: Orders will not be sent to store if the number of orders exceeds maximum orders for the day for a given store.
    Given any cart
    And the daily maximum consignments for Robina is '<orderLimit>'
    And the global start time for the order limit is '<offset> plus 2 hours ago'
    And list of consignments for store '7126' are:
      | consignmentCode | deliveryMethod    | cncStoreNumber | createdDate               | comment        |
      | autox11000124   | click-and-collect | 7126           | <offset> plus 0 hours ago | today at 12:00 |
      | autox11000125   | click-and-collect | 7140           | <offset> plus 3 hours ago | today at 09:00 |
      | autox11000126   | click-and-collect | 7140           | <offset> plus 2 hours ago | today at 10:00 |
      | autox11000127   | click-and-collect | 7140           | <offset> plus 1 hours ago | today at 11:00 |
      | autox11000128   | home-delivery     |                | <offset> plus 3 hours ago | today at 09:00 |
      | autox11000129   | home-delivery     |                | <offset> plus 0 hours ago | today at 12:00 |
    And delivery mode is '<deliveryMode>'
    And the delivery address is '446,Ann Street,Brisbane City,QLD,4000'
    And order cnc store is '<cncStoreNumber>'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | orderLimit | orderTimeExample  | offset | warehouse | deliveryMode      | cncStoreNumber | comment                                                     |
      | 5          | today at 13:00    | 01     | Robina    | home-delivery     |                | less than the max number of orders and afterstart time      |
      | 2          | today at 13:00    | 01     | Fastline  | home-delivery     |                | Limit exceeded for the day                                  |
      | 2          | today at 13:00    | 01     | Robina    | click-and-collect | 7126           | sameStore CNC should not get rejected even if limit exceeds |
      | 2          | tomorrow at 10:01 | 23     | Robina    | click-and-collect | 7140           | Outside 24 hr period                                        |
      | 2          | tomorrow at 09:59 | 21     | Fastline  | home-delivery     |                | Limit exceeded for the day                                  |
      | 2          | today at 13:00    | 01     | Fastline  | click-and-collect | 7140           | Limit exceeded for the day                                  |
      | null       | today at 13:00    | 01     | Robina    | home-delivery     |                | no order limit found, do not reject the order               |
      | 0          | today at 13:00    | 01     | Robina    | home-delivery     |                | feature turned off                                          |

  Scenario Outline: Orders will not be sent to store if the order contains product of type which is restricted for the store.
    Given product type '<productTypes>' restricted for store 'Robina'
    And delivery mode is '<deliveryMode>'
    And product weight limit for store delivery for 'Robina' is 100 kg
    When a consignment sent to a store '7049' with product:
      | product  | qty | price |
      | 10000211 | 2   | 15    |
      | 10000311 | 2   | 15    |
      | 10000510 | 1   | 30    |
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | productTypes | deliveryMode           | warehouse |
      |              | click-and-collect      | Robina    |
      | normal       | click-and-collect      | Fastline  |
      | bulky2       | click-and-collect      | Fastline  |
      |              | ebay-click-and-collect | Robina    |
      | normal       | ebay-click-and-collect | Fastline  |
      | bulky2       | ebay-click-and-collect | Fastline  |

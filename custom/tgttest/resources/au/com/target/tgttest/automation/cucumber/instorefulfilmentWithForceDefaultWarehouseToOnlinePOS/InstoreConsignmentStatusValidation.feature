@dummyStoreWarehouseData @cleanOrderData @cleanConsignmentData @storeFulfilmentCapabilitiesData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Process of validation of the status of the consignments and orders when consignments are fulfilled
  This is the set of scenarios where the consignments are processed in store and taken into the different status
  Confirmed_by_warehouse -> waved -> picked -> complete and rejected scenarios

  Background: 
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      | createdDate | shipDate | cancelDate |
      | auto11000001    | CONFIRMED_BY_WAREHOUSE | today       | null     | null       |
      | auto11000002    | SENT_TO_WAREHOUSE      | today       | null     | null       |
      | auto11000003    | WAVED                  | today       | null     | null       |
      | auto11000004    | SHIPPED                | today       | null     | null       |
      | auto11000005    | CANCELLED              | today       | null     | null       |
      | auto11000006    | CREATED                | today       | null     | null       |
      | auto11000007    | PICKED                 | today       | null     | null       |

  Scenario Outline: A consignment is accepted by store user
    When the consignment '<consignmentCode>' is accepted
    Then consignment status for '<consignmentCode>' is '<consignmentStatus>'

    Examples: 
      | consignmentCode | consignmentStatus |
      | auto11000001    | WAVED             |
      | auto11000002    | WAVED             |
      | auto11000003    | WAVED             |
      | auto11000004    | SHIPPED           |
      | auto11000005    | CANCELLED         |
      | auto11000006    | CREATED           |
      | auto11000007    | PICKED            |

  Scenario Outline: A consignment is rejected by store user
    When the consignment '<consignmentCode>' is rejected
    Then consignment status for '<consignmentCode>' is '<consignmentStatus>'

    Examples: 
      | consignmentCode | consignmentStatus |
      | auto11000001    | CANCELLED         |
      | auto11000002    | CANCELLED         |
      | auto11000003    | CANCELLED         |
      | auto11000004    | SHIPPED           |
      | auto11000005    | CANCELLED         |
      | auto11000006    | CREATED           |
      | auto11000007    | CANCELLED         |

  Scenario Outline: A consignment is completed by store user
    When the consignment '<consignmentCode>' is completed
    Then consignment status for '<consignmentCode>' is '<consignmentStatus>'

    Examples: 
      | consignmentCode | consignmentStatus      |
      | auto11000001    | CONFIRMED_BY_WAREHOUSE |
      | auto11000002    | SENT_TO_WAREHOUSE      |
      | auto11000003    | WAVED                  |
      | auto11000004    | SHIPPED                |
      | auto11000005    | CANCELLED              |
      | auto11000006    | CREATED                |
      | auto11000007    | PACKED                 |

  Scenario Outline: Begin pack process for store
    When the consignment '<consignmentCode>' is picked
    Then consignment '<consignmentCode>' has a status of '<consignmentStatus>' and order status of '<orderStatus>'

    Examples: 
      | consignmentCode | consignmentStatus      | orderStatus |
      | auto11000001    | CONFIRMED_BY_WAREHOUSE | INPROGRESS  |
      | auto11000002    | SENT_TO_WAREHOUSE      | INPROGRESS  |
      | auto11000003    | PICKED                 | INPROGRESS  |
      | auto11000004    | SHIPPED                | COMPLETED   |
      | auto11000005    | CANCELLED              | CANCELLED   |
      | auto11000006    | CREATED                | CREATED     |
      | auto11000007    | PICKED                 | INPROGRESS  |

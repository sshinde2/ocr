@cartProductData @storeFulfilmentCapabilitiesData @cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Process Instore Reject of a consignment
  As the online store, I want to be able to apply re-routing a consignment to fastline if the store cannot fulfil the order
  
  As part of this feature, once rejected by store 
  a) the consignment associated with the store needs to be cancelled
  b) a reason needs to be set on the consignment
  c) a new consignment needs to be created with warehouse as fastline
  d) all the business process for fastline will get executed
  e) when stock reported at store is zero, reject state is set to ZERO_STOCK_REPORTED and in store reason is null

  Scenario Outline: reject a waved consignment in store goes to Fastline
    Given a consignment sent to a store
    And the consignment is accepted
    When the consignment is rejected with instore reject reason '<reason>' and reject state is '<rejectState>'
    Then consignment status is 'CANCELLED'
    And consignment reject state is '<rejectState>'
    And re-routing reason is 'PICK_FAILED'
    And a new consignment created with the same consignment details
    And the assigned warehouse will be 'Fastline'
    And consignment carrier is 'AustraliaPostInstoreCNC'
    And consignment is picked by Fastline without order extract

    Examples:
    | reason             | rejectState         |
    | Pick Not Attempted |                     |
    | none               | ZERO_STOCK_REPORTED |

  Scenario Outline: consignment is not waved but rejected by store
    Given a consignment sent to a store with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And the consignment is rejected with instore reject reason '<reason>' and reject state is '<rejectState>'
    When run ofc list consignments for store
    Then ofc list of consignments is:
      | items | consignmentStatus | customer |
      | 2     | REJECTED          | cnc      |
    And re-routing reason is 'REJECTED_BY_STORE'
    And consignment reject state is '<rejectState>'

    Examples:
    | reason             | rejectState         |
    | Pick Not Attempted |                     |
    | none               | ZERO_STOCK_REPORTED |

  Scenario: consignment is of status picked but then rejected by store
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
    And a consignment sent to a store '7032' with products:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
    When the consignment is waved and picked instore
    And the consignment is rejected
    Then consignment status is 'CANCELLED'
    And re-routing reason is 'PICK_FAILED'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 4        | 10        |

  Scenario: consignment is rejected by store because Insufficient Time To Pick then the same instore reject reason should exist in consignment
    Given a consignment sent to a store with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When the consignment is rejected with instore reject reason 'Insufficient Time To Pick'
    Then instore reject reason is 'Insufficient Time To Pick'

  Scenario: consignment is rejected by store because Pick Not Attempted then the same instore reject reason should exist in consignment
    Given a consignment sent to a store with product:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    When the consignment is rejected with instore reject reason 'Pick Not Attempted'
    Then instore reject reason is 'Pick Not Attempted'

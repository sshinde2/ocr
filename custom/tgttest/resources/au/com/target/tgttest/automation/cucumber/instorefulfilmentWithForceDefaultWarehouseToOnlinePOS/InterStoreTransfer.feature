@cartProductData @forceDefaultWarehouseToOnlinePOS
Feature: Inter Store Transfer - Info regarding adjustment of stock levels between online store and fulfilling store
  
  As the online store I want to send stock levels adjustment info to POS between Fastline and the fulfilment store for items fulfilled by a store.

  Scenario: Do not process inter store transfer when order is not fulfilled by store.
    Given a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
      | 10000012 | 2   | 15    |
    When the consignment is full picked by fastline
    Then fastline pick process completes
    And inter store transfer is not triggered

  @wip
  Scenario: Process inter store transfer when order fulfilled by store.
    Given a consignment sent to a store '7032' with products:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
      | 10000012 | 2   | 15    |
    When the consignment is completed instore
    Then inter store transfer contains fromStore as 7032
    And inter store transfer contains items as:
      | product  | qty |
      | 10000011 | 4   |
      | 10000012 | 2   |

  @wip
  Scenario: Check that no IST happens and the stock is not released to fastline on the status change to picked.
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
    And a consignment sent to a store '7032' with products:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
    When the consignment is waved and picked instore
    Then inter store transfer is not triggered
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 4        | 10        |

  @wip
  Scenario: process complete and test IST and stock being released to fastline
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
    And a consignment sent to a store '7032' with products:
      | product  | qty | price |
      | 10000011 | 4   | 15    |
    When the consignment is completed instore
    Then inter store transfer contains fromStore as 7032
    And inter store transfer contains items as:
      | product  | qty |
      | 10000011 | 4   |
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |

@toFix @cleanOrderData @cleanConsignmentData @cleanSmsBusinessProcessData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - SMS notification for open orders
  
  In Order to notify the store manager about all open orders
  As online store
  I want to send SMS notification to store manager
  so that I can process my open orders

  Scenario Outline: Alert sent to store manager for open orders
    Given store for which sms notification will be sent <store>
    And mobile number for store is '<mobileNumber>'
    And sms notification flag is '<notificationEnabled>'
    And sms notification start time is '<notificationStartTime>'
    And sms notification end time is '<notificationEndTime>'
    And number of open orders at the store are '<openOrders>'
    When send sms notification to store job is run
    Then send sms to store status is '<smsStatus>'

    Examples: 
      | store | mobileNumber | notificationEnabled | notificationStartTime | notificationEndTime | openOrders | smsStatus |
      | 7032  | +61430011120 | yes                 | now + 2 hours         | now + 9 hours       | 2          | no        |
      | 7049  | +61430022222 | yes                 | now - 10 hours        | now - 1 hours       | 1          | no        |
      | 7043  | +61430033333 | no                  | now - 2 hours         | now + 12 hours      | 2          | no        |
      | 7126  |              | yes                 | now - 2 hours         | now + 7 hours       | 4          | no        |
      | 7032  | +61430011222 | yes                 | now - 10 hours        | now + 2 hours       | 2          | yes       |
      | 7049  | +61430022222 | yes                 | now - 5 hours         | now + 3 hours       | 0          | no        |
      | 7032  | +61430011333 | yes                 | now - 3 hours         | now + 4 hours       | 4          | yes       |

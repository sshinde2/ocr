@cartProductData @storeFulfilmentCapabilitiesData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Stock Lookup For Order
  As the online store 
  I want to know if a given store has available stock to fulfil an order 
  So that I can determine if an order could be successfully picked by a nominated store

  Background: 
    Given stores with stock:
      | store | stock                                          |
      | 7001  | 10 of 10000011, 10 of 10000012, 10 of 10000021 |
      | 7100  | 10 of 10000011, 0 of 10000012, 10 of 10000021  |
      | 8593  | 0 of 10000011, 0 of 10000012, 0 of 10000021    |
      | 7064  | 3 of 10000011, 1 of 10000012, 0 of 10000021    |
      | 7269  | 3 of 10000011, 10 of 10000012, 10 of 10000021  |
      | 7120  | 4 of 10000011, 2 of 10000012, 1 of 10000021    |
      | 7128  | 7 of 10000011, 5 of 10000012, 4 of 10000021    |
      | 7125  | 6 of 10000011, 4 of 10000012, 3 of 10000021    |
      | 7160  | 4 of 10000022, 2 of 10000025, 1 of 10000023    |
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 4   | 100   |
      | 10000012 | 2   | 200   |
      | 10000021 | 1   | 100   |

  Scenario Outline: Cart SOH check for a given store without stock buffer.
    Given store SOH buffer is reset for '<Store>'
    When SOH check is performed for <Store>
    Then the result of the store SOH check is <Result>

    Examples: 
      | Store | Result | Comment                                                                                                     |
      | 7001  | true   | stock on hand available to fulfil item quantities for entire cart (stock available for all item quantities) |
      | 7100  | false  | stock on hand available to only partially fulfil item quantities in cart (could not fulfil entire cart)     |
      | 8593  | false  | stock on hand unavailable to fulfil any item in cart (none could be fulfilled)                              |
      | 7064  | false  | not enough stock on hand unavailable to fulfil any item in cart (none could be fulfilled)                   |
      | 7269  | false  | not enough stock on hand unavailable to fulfil some items in cart (could only be partially fulfilled)       |
      | 7120  | true   | exactly enough stock on hand unavailable to fulfil all items in cart (could be fulfilled)                   |
      | 7128  | true   | enough stock on hand available to fulfil all items in cart (could be fulfilled)                             |
      | 7125  | true   | enough stock on hand available to fulfil all items in cart (could be fulfilled)                             |
      | 7160  | false  | store does not have the products (products not available)                                                   |

  Scenario Outline: Cart SOH check for a given store with stock buffer.
    Given store SOH buffer is set to '3' for '<Store>'
    When SOH check is performed for <Store>
    Then the result of the store SOH check is <Result>

    Examples: 
      | Store | Result | Comment                                                                                                     |
      | 7001  | true   | stock on hand available to fulfil item quantities for entire cart (stock available for all item quantities) |
      | 7100  | false  | stock on hand available to only partially fulfil item quantities in cart (could not fulfil entire cart)     |
      | 8593  | false  | stock on hand unavailable to fulfil any item in cart (none could be fulfilled)                              |
      | 7064  | false  | not enough stock on hand unavailable to fulfil any item in cart (none could be fulfilled)                   |
      | 7269  | false  | not enough stock on hand unavailable to fulfil some items in cart (could only be partially fulfilled)       |
      | 7120  | true   | no store stock buffer (could be fulfilled)                                                                  |
      | 7128  | true   | exactly enough stock on hand available to fulfil all items in cart (could be fulfilled)                     |
      | 7125  | true   | no store stock buffer (could be fulfilled)                                                                  |
      | 7160  | false  | store does not have the products (products not available)                                                   |

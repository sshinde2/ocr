@wip @cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - product count committed to consignments for store
  
  As online
  I want to add safeguards to the stock on hand check when verifying if a store could fulfil an order based on the stock they have available
  So that I can ensure the order can be fulfilled without selling out of stock in-store
  
  Buffer per item for the day is made up of:
  * quantity of items from successfully completed orders within that day for the store (shipped date is within the current day)
  * quantity of items currently in orders with a 'packed' status (regardless of date packed)
  * quantity of items currently in orders with a 'picked' status (regardless of date picked)
  * Exclude cancelled/rejected orders
  * Exclude open / confirmed by warehouse
  * Exclude in progress / waved
  * Exclude completed orders from the previous day

  Background: 
    Given committed product count start time is '4 hours ago'
    And list of consignments for store 'Camberwell' are:
      | consignmentCode | products                     | consignmentStatus      | createdDate  | waveDate     | pickDate     | packDate    | shipDate    |
      | autox11000121   | 1 of 10000011, 1 of 10000012 | shipped                | 6 hours ago  | 6 hours ago  | 6 hours ago  | 6 hours ago | 6 hours ago |
      | autox11000122   | 1 of 10000011, 2 of 10000021 | shipped                | 4 hours ago  | 4 hours ago  | 4 hours ago  | 4 hours ago | 4 hours ago |
      | autox11000123   | 1 of 10000011, 3 of 10000021 | shipped                | 6 hours ago  | 6 hours ago  | 6 hours ago  | 6 hours ago | 4 hours ago |
      | autox11000124   | 1 of 10000011, 4 of 10000021 | packed                 | 6 hours ago  | 6 hours ago  | 6 hours ago  | 6 hours ago | n/a         |
      | autox11000125   | 1 of 10000011, 5 of 10000021 | packed                 | 6 hours ago  | 6 hours ago  | 6 hours ago  | 4 hours ago | n/a         |
      | autox11000126   | 1 of 10000011, 6 of 10000021 | picked                 | 12 hours ago | 12 hours ago | 12 hours ago | n/a         | n/a         |
      | autox11000127   | 1 of 10000011, 1 of 10000012 | waved                  | 2 hours ago  | 2 hours ago  | n/a          | n/a         | n/a         |
      | autox11000128   | 1 of 10000011, 1 of 10000012 | confirmed_by_warehouse | 2 hours ago  | n/a          | n/a          | n/a         | n/a         |
      | autox11000129   | 1 of 10000011, 1 of 10000012 | cancelled              | 2 hours ago  | n/a          | n/a          | n/a         | n/a         |

  Scenario Outline: 
    When get committed product count for store 'Camberwell' for product '<product>'
    Then committed product count is <count>

    Examples: 
      | product  | count |
      | 10000011 | 5     |
      | 10000012 | 0     |
      | 10000021 | 20    |

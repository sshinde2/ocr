@cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Manifests - Generate Manifest
  
  In Order to send details to Carrier as Store Agent I want to Generate manifest
  
  Notes: 
  * 7032 is Camberwell.
  * Hoppers store is number 7064

  Background: 
    Given store number for ofc portal is '7032'
    And list of consignments for the store is:
      | consignmentCode | deliveryMethod    | cncStoreNumber | consignmentStatus | manifestID | packDate       | boxes | customer | destination                 | trackingId |
      | autox11000001   | home-delivery     |                | Waved             |            |                |       | BB BBBB  | Seaford, VIC, 3198          | xxx        |
      | autox11000002   | home-delivery     |                | Picked            |            |                |       | DD DDDD  | Tarneit, VIC, 3029          | xxx        |
      | autox11000003   | home-delivery     |                | Packed            |            | Today at 16:00 | 2     | GG GGGG  | Seaford, VIC, 3198          | 123457     |
      | autox11000004   | home-delivery     |                | Packed            |            | Yesterday      | 3     | GG GGGG  | Seaford, VIC, 3198          | 123455     |
      | autox11000005   | home-delivery     |                | Packed            |            | 2 days ago     | 2     | DD DDDD  | Seaford, VIC, 3198          | 123454     |
      | autox11000006   | home-delivery     |                | Shipped           | 7110001    | Yesterday      | 1     | NN NNNN  | Seaford, VIC, 3198          | xxx        |
      | autox11000012   | click-and-collect | 7064           | Waved             |            |                |       | AA AAAA  | Hoppers Crossing, VIC, 3030 | xxx        |
      | autox11000013   | click-and-collect | 7064           | Picked            |            |                |       | CC CCCC  | Hoppers Crossing, VIC, 3030 | xxx        |
      | autox11000014   | click-and-collect | 7064           | Packed            |            | 3 days ago     | 1     | EE EEEE  | Hoppers Crossing, VIC, 3030 | 123453     |
      | autox11000015   | click-and-collect | 7064           | Packed            |            | Today at 09:00 | 2     | FF FFFF  | Hoppers Crossing, VIC, 3030 | 123456     |
      | autox11000016   | click-and-collect | 7064           | Shipped           | 7110003    | Yesterday      | 2     | JJ JJJJ  | Hoppers Crossing, VIC, 3030 | xxx        |

  Scenario Outline: Create a manifest for HD or inter-store CNC order, with various responses from webmethods.
    Given webmethods returns '<webmethodsResponse>' for auspost manifest transmission
    When ofc transmit function is run
    Then list of consignments in generated manifest is:
      | consignmentCode | orderType           | packDate       | customer | destination           | boxes |
      | autox11000014   | INTERSTORE_DELIVERY | 3 days ago     | EE EEEE  | Hoppers Crossing, VIC | 1     |
      | autox11000005   | CUSTOMER_DELIVERY   | 2 days ago     | DD DDDD  | Seaford, VIC          | 2     |
      | autox11000004   | CUSTOMER_DELIVERY   | Yesterday      | GG GGGG  | Seaford, VIC          | 3     |
      | autox11000015   | INTERSTORE_DELIVERY | Today at 09:00 | FF FFFF  | Hoppers Crossing, VIC | 2     |
      | autox11000003   | CUSTOMER_DELIVERY   | Today at 16:00 | GG GGGG  | Seaford, VIC          | 2     |
    And unique manifestId is generated
    And manifest dateTime stamp is current time
    And number of parcels in generated manifest is '10'
    And status of consignments in generated manifest is shipped
    And manifest request is sent to AusPost with:
      | store      | merchantLocationId | storeAddress                          |
      | Camberwell | JDQ                | Station Street, Camberwell, VIC, 3124 |
    And manifest request consignments are:
      | trackingId | deliveryName              | deliveryAddress             |
      | 123453     | Target - Hoppers Crossing | Hoppers Crossing, VIC, 3030 |
      | 123454     | DD DDDD                   | Seaford, VIC, 3198          |
      | 123455     | GG GGGG                   | Seaford, VIC, 3198          |
      | 123456     | Target - Hoppers Crossing | Hoppers Crossing, VIC, 3030 |
      | 123457     | GG GGGG                   | Seaford, VIC, 3198          |
    And the manifest sent flag is set to '<manifestSentFlag>'
    And status of orders in generated manifest is '<ordersStatus>'

    Examples: 
      | webmethodsResponse | manifestSentFlag | ordersStatus |
      | success            | true             | completed    |
      | unavailable        | false            | invoiced     |
      | error              | false            | invoiced     |

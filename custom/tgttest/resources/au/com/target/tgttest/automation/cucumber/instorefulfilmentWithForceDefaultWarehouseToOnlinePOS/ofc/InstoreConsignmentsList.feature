@cartProductData @dummyStoreWarehouseData @cleanOrderData @cleanConsignmentData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Instore Consignment list
  As a store user, I want to see the orders filtered out for reject reason as cancelled by customer 
  Pagination of results.
  Default sorting by order creation date ascending
  
  Note: Assuming product 10000022 has clearance=true, 10000121 has clearance=false and 10000111 has clearance=false. Need to set the values while implementing Step definition.

  Background: 
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      | createdDate          | cancelDate           | rejectReason          | customer        |  deliveryMethod      |
      | autox11000011   | CANCELLED              | 7 days ago at 23:30  | 7 days ago at 23:35  | PICK_FAILED           | firstname Guest |  click-and-collect   |
      | autox11000012   | CANCELLED              | 8 days ago at 13:30  | 8 days ago at 13:35  | PICK_TIMEOUT          | firstname Guest |  click-and-collect   |
      | autox11000013   | CANCELLED              | 20 days ago at 14:26 | 20 days ago at 14:28 | REJECTED_BY_STORE     | firstname Guest |  click-and-collect   |
      | autox11000014   | CONFIRMED_BY_WAREHOUSE | yesterday at 08:00   | null                 | null                  | firstname Guest |  home-delivery       |
      | autox11000015   | CANCELLED              | yesterday at 23:00   | yesterday at 23:15   | CANCELLED_BY_CUSTOMER | firstname Guest |  home-delivery       |
      | autox11000016   | WAVED                  | today                | null                 | null                  | firstname Guest |  home-delivery       |

  Scenario Outline: Show consignments for store '2000'
    When run ofc list consignments for store '2000' with offset as <Offset> and records per page as <PerPage>
    Then consignment list is ordered as <Result>

    Examples: 
      | Offset | PerPage | Result                                                                | Comment                                                                      |
      | 0      | 0       | autox11000011,autox11000012,autox11000013,autox11000014,autox11000016 | Will use default page size for retrieving records, and exclude autox11000015 |
      | 0      | 3       | autox11000011,autox11000012,autox11000013                             | From offset value 0 3 records will be retrieved                              |
      | 1      | 2       | autox11000012,autox11000013                                           | From offset value 1 2 records will be retrieved                              |

   Scenario: consignments with clearance products
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      | products                     |
      | autox11000101   | CONFIRMED_BY_WAREHOUSE | 1 of 10000022, 1 of 10000121 |
      | autox11000102   | CONFIRMED_BY_WAREHOUSE | 1 of 10000121                |
      | autox11000104   | CONFIRMED_BY_WAREHOUSE | 1 of 10000121, 1 of 10000111 |
    When run ofc list consignments for store '2000' with offset as 0 and records per page as 0
    Then consignment list contains:
      | consignmentCode | clearance |
      | autox11000101   | true      |
      | autox11000102   | false     |
      | autox11000104   | false     |

  Scenario: A single consignment with product details including clearance status
    Given list of consignments for store '2000' are:
      | consignmentCode | consignmentStatus      | products                     |
      | autox11000105   | CONFIRMED_BY_WAREHOUSE | 1 of 10000022, 1 of 10000111 |
    When run ofc list consignments for store '2000' with offset as 0 and records per page as 0
    Then consignment list contains:
      | consignmentCode | clearance |
      | autox11000105   | true      |
    And consignment 'autox11000105' contains products:
      | productCode | clearance |
      | 10000022    | true      |
      | 10000111    | false     |

  Scenario Outline: Show consignments for store '2000' with text search (consignment code)
    When run ofc list consignments for store '2000' with offset as <Offset> and records per page as <PerPage> with search text as <SearchText> and ofc status as <OfcStatus>
    Then consignment list is ordered as <Result>

    Examples: 
      | Offset | PerPage | Result        | Comment                                                    | SearchText    | OfcStatus  |
      | 0      | 25      | autox11000011 | Default Page size search with consignment code             | autox11000011 |            |
      | 0      | 25      | autox11000016 | Default Page size search with consignment code with status | autox11000016 | INPROGRESS |

  Scenario Outline: Show consignments for store '2000' with text search Customer Name AND/OR Ofc Status
    When run ofc list consignments for store '2000' with offset as <Offset> and records per page as <PerPage> with search text as <SearchText> and ofc status as <OfcStatus>
    Then consignment list result contains <Result>

    Examples: 
      | Offset | PerPage | Result                                                                | Comment                                             | SearchText      | OfcStatus |
      | 0      | 0       | autox11000011,autox11000012,autox11000013,autox11000014,autox11000016 | Default Page size search with name                  | firstname Guest |           |
      | 0      | 0       | autox11000011,autox11000012,autox11000013,autox11000014,autox11000016 | Default Page size search with last name             | Guest           |           |
      | 0      | 0       | autox11000011,autox11000012,autox11000013,autox11000014,autox11000016 | Default Page size search with firstname             | firstname       |           |
      | 0      | 0       | autox11000014                                                         | Default Page size search with name with status      | firstname Guest | OPEN      |
      | 0      | 0       | autox11000014                                                         | Default Page size search with lastname with status  | Guest           | OPEN      |
      | 0      | 0       | autox11000014                                                         | Default Page size search with firstname with status | firstname       | OPEN      |
      | 0      | 0       | autox11000014                                                         | Default Page size search with status                |                 | OPEN      |
      
   Scenario Outline: Show consignments for store '2000' with Sort by Order Date
    When run ofc list consignments for store '2000' with offset as <Offset> and records per page as <PerPage> with ofc status as <OfcStatus> and sort by key as <ofcSortKey> and direction as <ofcSortDirection> 
    Then consignment list is ordered as <Result>

    Examples: 
      | Offset | PerPage | Result                                                                | Comment                                      | OfcStatus  | ofcSortKey         | ofcSortDirection |
      | 0      | 0       | autox11000011,autox11000012,autox11000013,autox11000014,autox11000016 | Default Page size sort by default order date |            |                    | ASC              |
      | 0      | 0       | autox11000016,autox11000014,autox11000013,autox11000012,autox11000011 | Sort by order date DESC                      |            | orderCreationDate  | DESC             |
      | 0      | 0       | autox11000011,autox11000012,autox11000013,autox11000014,autox11000016 | Sort by order date ASC                       |            | orderCreationDate  | ASC              |
      
   Scenario Outline: Show consignments for store '2000' with Sort by Consignment Date
    When run ofc list consignments for store '2000' with offset as <Offset> and records per page as <PerPage> with ofc status as <OfcStatus> and sort by key as <ofcSortKey> and direction as <ofcSortDirection> 
    Then consignment list is ordered as <Result>

    Examples: 
      | Offset | PerPage | Result                                                                | Comment                          | OfcStatus  | ofcSortKey                 | ofcSortDirection |
      | 0      | 0       | autox11000013,autox11000012,autox11000011,autox11000014,autox11000016 | Sort by consignment date ASC     |            | consignmentCreationDate    | ASC              |
      | 0      | 0       | autox11000016,autox11000014,autox11000011,autox11000012,autox11000013 | Sort by consignment date DESC    |            | consignmentCreationDate    | DESC             |
      
   Scenario Outline: Show consignments for store '2000' with Sort by Delivery Type
    When run ofc list consignments for store '2000' with offset as <Offset> and records per page as <PerPage> with ofc status as <OfcStatus> and sort by key as <ofcSortKey> and direction as <ofcSortDirection> 
    Then consignment list is ordered as <Result>

    Examples: 
      | Offset | PerPage | Result                                                                | Comment                    | OfcStatus  | ofcSortKey      | ofcSortDirection |
      | 0      | 0       | autox11000011,autox11000012,autox11000013,autox11000014,autox11000016 | Sort by delivery type ASC  |            | deliveryType    | ASC              |
      | 0      | 0       | autox11000014,autox11000016,autox11000011,autox11000012,autox11000013 | Sort by delivery type DESC |            | deliveryType    | DESC             |                  
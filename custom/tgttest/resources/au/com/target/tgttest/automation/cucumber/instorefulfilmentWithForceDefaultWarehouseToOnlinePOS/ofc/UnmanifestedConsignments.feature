@dummyStoreWarehouseData @cleanOrderData @cleanConsignmentData @storeFulfilmentCapabilitiesData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - Manifests - Unmanifested consignments
  As a Target store member who fulfils consignments from store
  I want to view a list of un-manifested consignments
  So that I can create a manifest to send to the delivery carrier
  cleanOrderData and cleanConsignmentData tags will clear any existing consignments before the scenario runs.

  Background: 
    Given store number for ofc portal is '1000'

  Scenario: No consignments to be manifested and ofc un-manifested consignments function is run.
    When ofc un-manifested consignments function is run
    Then list of un-manifested consignments for the store is empty

  Scenario: Some consignments to be manifested and ofc un-manifested consignments function is run.
    Given list of consignments for the store is:
      | consignmentCode | deliveryMethod    | cncStoreNumber | consignmentStatus | manifestID | packDate       | boxes | customer | destination  |
      | auto11000001   | home-delivery     |                | Waved             |            |                |       | BB BBBB  | Seaford, VIC |
      | auto11000002   | home-delivery     |                | Picked            |            |                |       | DD DDDD  | Tarneit, VIC |
      | auto11000003   | home-delivery     |                | Packed            |            | Today at 16:00 | 2     | GG GGGG  | Seaford, VIC |
      | auto11000004   | home-delivery     |                | Packed            |            | Yesterday      | 3     | GG GGGG  | Seaford, VIC |
      | auto11000005   | home-delivery     |                | Packed            |            | 2 days ago     | 2     | DD DDDD  | Seaford, VIC |
      | auto11000006   | home-delivery     |                | Shipped           | 7110001    | Yesterday      | 1     | NN NNNN  | Seaford, VIC |
      | auto11000012   | click-and-collect | 2000           | Waved             |            |                |       | AA AAAA  | Seaford, VIC |
      | auto11000013   | click-and-collect | 2000           | Picked            |            |                |       | CC CCCC  | Tarneit, VIC |
      | auto11000014   | click-and-collect | 2000           | Packed            |            | 3 days ago     | 1     | EE EEEE  | Seaford, VIC |
      | auto11000015   | click-and-collect | 2000           | Packed            |            | Today at 09:00 | 2     | FF FFFF  | Tarneit, VIC |
      | auto11000016   | click-and-collect | 2000           | Shipped           | 7110003    | Yesterday      | 2     | JJ JJJJ  | Tarneit, VIC |
    When ofc un-manifested consignments function is run
    Then list of un-manifested consignments for the store is:
      | consignmentCode | orderType           | packDate       | customer | destination  | boxes |
      | auto11000014   | INTERSTORE_DELIVERY | 3 days ago     | EE EEEE  | Seaford, VIC | 1     |
      | auto11000005   | CUSTOMER_DELIVERY   | 2 days ago     | DD DDDD  | Seaford, VIC | 2     |
      | auto11000004   | CUSTOMER_DELIVERY   | Yesterday      | GG GGGG  | Seaford, VIC | 3     |
      | auto11000015   | INTERSTORE_DELIVERY | Today at 09:00 | FF FFFF  | Tarneit, VIC | 2     |
      | auto11000003   | CUSTOMER_DELIVERY   | Today at 16:00 | GG GGGG  | Seaford, VIC | 2     |

@storeFulfilmentCapabilitiesData @cartProductData @forceDefaultWarehouseToOnlinePOS
Feature: Instore Fulfilment - home and interstore delivery
  Select warehouse based on:
  * geographical state
  * closeness of delivery address
  * number of allowed routing attempts
  
  Note: by default global and store rules allow all orders

  Background: 
    Given stores with fulfilment capability and stock:
      | store       | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore | clearProductExclusions | clearCategoryExclusions |
      | Robina      | Yes     | QLD   | Yes            | Yes                      | Yes                         | true                   | true                    |
      | Rockhampton | Yes     | QLD   | Yes            | Yes                      | Yes                         | true                   | true                    |
      | Morley      | Yes     | WA    | No             | Yes                      | Yes                         | true                   | true                    |
      | Camberwell  | Yes     | VIC   | Yes            | Yes                      | Yes                         | true                   | true                    |
    And postcode chargezone mappings are:
      | postcode | chargeZone |
      | 0002     |            |

  Scenario: No store routing as max number of attempts is 0 even though store could take it
    Given System-wide max number of routing attempts is '0'
    And any home-delivery cart
    And the delivery address is '19,Robina Town Centre Drive,Robina,QLD,4226'
    When order is placed
    Then consignment is picked by Fastline without order extract

  Scenario Outline: Select warehouse for home delivery based on state and closest store
    Given System-wide max number of routing attempts is '1'
    And any home-delivery cart
    And the delivery address is '<Address>'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | Address                                      | warehouse   | comment                  |
      | 11,Coleman Street,Koonibba,SA,5690           | Fastline    | No enabled store in SA   |
      | 180,Drake Street, Morley,WA,6062             | Fastline    | No enabled store in WA   |
      | 8/15A, Hooker Road, Werribee,VIC,3030        | Camberwell  | One enabled store in VIC |
      | 11,Canning Street, Rockhampton,QLD,4700      | Rockhampton | Closest to Rockhampton   |
      | 19,Robina Town Centre Drive, Robina,QLD,4226 | Robina      | Closest to Robina        |
      | 446,Ann Street, Brisbane City,QLD,4000       | Robina      | Closest to Robina        |
      | 6,McCann Street, South Gladstone,QLD,4680    | Rockhampton | Closest to Rockhampton   |

  Scenario: Consignment sent for instore pickup and then rejected, 2 routing attempts goes to next closest store.
    Given System-wide max number of routing attempts is '2'
    And a consignment sent to a store 'Rockhampton'
    And the consignment is rejected
    Then the assigned warehouse will be 'Robina'

  Scenario: Consignment sent for instore pickup and then rejected, 1 routing attempts
    Given System-wide max number of routing attempts is '1'
    And a consignment sent to a store 'Rockhampton'
    And the consignment is rejected
    Then consignment is picked by Fastline without order extract

  Scenario: Consignment sent to store for customer delivery is rejected, single store in state.
    Given System-wide max number of routing attempts is '2'
    And any home-delivery cart
    And the delivery address is '35,Collins St, Melbourne,VIC,3000'
    When order is placed
    And consignment is assigned to 'Camberwell' and rejected by the store
    Then consignment is picked by Fastline without order extract

  Scenario Outline: Consignment sent to Fastline for customer delivery since the postcode in the delivery address is not a known postcode.
    Given System-wide max number of routing attempts is '1'
    And any home-delivery cart
    And the delivery address is '<Address>'
    When order is placed
    Then the assigned warehouse will be '<warehouse>'

    Examples: 
      | Address                           | warehouse  | comment                                            |
      | 35,Collins St, Melbourne,VIC,0001 | Fastline   | Postcode not found in system                       |
      | 35,Collins St, Melbourne,VIC,0002 | Fastline   | postcode exists but no chargezone for the postcode |
      | 35,Collins St, Melbourne,VIC,3000 | Camberwell | have a valid postcode and chargezone               |
      | 35,Collins St, Melbourne,,3000    | Fastline   | missing state in the delivery address              |

@lookDetailsData @cartProductData @sessionCatalogVersion
Feature: Shop the Look Feature on Mobile Apps
  As a Target online user I want to see Shop the Look products.

  Background:
    Given the 'step.shop.the.look.app' feature switch is enabled
    And the shop the look are setup in the system running the mentioned impex

  Scenario: View Shop the Look products on mobile apps. Only active looks should be displayed.
    Given the 'step.shop.the.look.app.limit' feature switch is disabled
    When social media products are retrieved for mobile apps
    Then the social media product instagram URLs are 'https://www.instagram.com/p/LOOK3/,https://www.instagram.com/p/BJe_OnxgMmP/,https://www.instagram.com/p/LOOK6/,https://www.instagram.com/p/BLXil5KgZ2c/,https://www.instagram.com/p/LOOK1/,https://www.instagram.com/p/BHgx_FDAAmH/'

  Scenario: View Shop the Look products on mobile apps while honouring limit.
    Given the 'step.shop.the.look.app.limit' feature switch is enabled
    When social media products are retrieved for mobile apps
    Then the social media product instagram URLs are 'https://www.instagram.com/p/LOOK3/,https://www.instagram.com/p/BJe_OnxgMmP/'

@notAutomated
Feature: Contextual Megamenu - Menu tap to take the user to the Top Category (Mobile only)



  Scenario: Mega Menu to take me back to my parent-level Category(Mobile Browser only, no Apps)
    Given I have tapped on the Menu in a Mobile
    And I have drilled down to any of the Categories e.g. Women> Bottoms
    And I have tapped on any of the items in the Accordion e.g Casual Pants
    When I tap back again on the Menu
    Then As per the Breadcrumb on the page, the top Category is open for me e.g Women in this case
    And All the other Accordion's if they we're originally opened, will be closed
    And I will be auto-focused to the Parent Category e.g. Women sitting at the top position in the Menu 
    And Scrolling up in the Menu Bar I see a link to All Categories

  Scenario: Performing a search for a product-PDP Page(Mobile Browser only, no Apps)
    Given I have searched for Kahuna Thongs in the Target search and tapped the drop-down to be on the PDP Page
    And The Breadcrumb on the page would be Kids > Shoes> Thongs> Kahuna Thongs
    When I click the Menu button on the Target website
    Then As per the Breadcrumb on the page, the top Category is open for me e.g Kids in this case
    And I will be auto-focused to the Top Category e.g. Kids sitting at the top position in the Menu 
    And Scrolling up in the Menu Bar I see a link to All Categories

  Scenario: Performing a search for a list of products -Listing Page_(Mobile Browser only, no Apps)
    Given I am on the Target website
    And I have performed a search to be on a Listing Page say Kids Shoes
    When I click the Menu button on the Target website
    Then I see NO change in the behavior to what it is currently. I am at the root Menu level (No drilling down to the parent Category in the Menu)
    And Scrolling up in the Menu Bar I see a link to All Categories

  Scenario: Searching for a Brand in a search engine_(Mobile Browser only, no Apps)
    Given I have searched for a Brand e.g. Lego
    When I click the Menu button on the Target website
    Then I see NO change in the behavior to what it is currently. I am at the root Menu level (No drilling down to the parent Category in the Menu)
    And Scrolling up in the Menu Bar I see a link to All Categories
    
    
  Scenario: Performing a search for a Landing Page(Mobile Browser only, no Apps)
    Given I have performed a search for a Landing Page e.g. Kids
    When I click the Menu button on the Target website
    Then As per the Breadcrumb on the page, the top Category is open for me e.g Kids in this case
    And All the other Accordion's if they we're originally opened, will be closed
    And I will be auto-focused to the Parent Category e.g. Kids sitting at the top position in the Menu 
    And Scrolling up in the Menu Bar I see a link to All Categories

  Scenario: No Change if I have made no selections(Mobile Browser only, no Apps)
    Given I am on the Target website
    And I have tapped the Menu button
    And I have opened up the few Accordions
    When I tap on the Cross (X) button on the Menu to be back on the Target site
    And I tap the Menu button again to be on the Menu Listings
    Then I see NO change in the behavior to what it is currently. I am at the root Menu level (No drilling down to the parent Category in the Menu)
    And Scrolling up in the Menu Bar I see a link to All Categories
  


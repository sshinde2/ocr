@wip
Feature: Applying sales channel
  As a Mobile app customer, 
  I only want to receive the search menu and mega menu when I request the target.com.au homepage,

  Scenario Outline: Specific version of the home page for mobility app
    Given hybris received a request
    When the request header contain <salesHeader>
    Then <version> homepage is displayed

    Examples: 
      | salesHeader       | version                     |
      | mobile app        | mobileapp                   |
      | kiosk             | web with kiosk restrictions |
      | none              | web                         |

@notAutomated
Feature: The target mobile apps should get assortment flag in product list response

  Scenario: new 'assorted' flag in mobile gateway
    Given the products in hybris are:
        | productCode | assorted |
        | a0001       | true     |
        | a0002       | false    |
    When the product list end-point is invoked
    Then I receive a product list JSON response
    And each product has the "assorted"
        | productCode | assorted |
        | a0001       | true     |
        | a0002       | false    |

  Scenario: new localised message for assortment
    When the resources end-point (https://app.target.com.au/ios/resources) in the Mobile Gateway is invoked
    Then the localised message for assorted products read "This item will be randomly selected. If you want a specific option, please visit your local Target store"

    
  Scenario: Mini Basket with an Assorted Product
    Given I have added some Assorted & Non-Assorted products for Checkout
    When I point to the Basket
    Then I can see the Mini Basket appearing 
    And I see an Assorted text message in the Product Description under Order Summary in orange "This item will be randomly selected", for only the products which are assorted
  
  Scenario: Thank You page with an Assorted product description
    Given I have added some Assorted & Non-Assorted products for Checkout
    When I complete my Checkout
    And I am on the Thank You page
    Then I see an Assorted text message in the Product Description under Order Summary in orange "This item will be randomly selected", for only the products which are assorted


  Scenario: My Account-Order Summary to have an assorted product description
    Given I have completed my Checkout with some Assorted & Non-Assorted products
    When I go to MyOrder Details in My Account
    Then I can see my recently placed order
    And Under the Order Summary, I can see an Assorted text in orange "This item will be randomly selected", for the ones which were Assorted

  Scenario: Assorted Text message sits between the Variants & the Qty
    Given I have added some Assorted product for Checkout
    And The added product has Colour & Size variants
    When I perform a checkout via SPC
    Then I see an Assorted text message in the Order Summary Page in orange "This item will be randomly selected"
    And I see the Assorted Text message in between the Variants (Colour, Size) and Qty
    
  Scenario: Assorted Text message sits right down bottom in the Mini Basket
    Given I have added some Assorted product for Checkout
    And the added product has Colour & Size variants
    When I point to the Basket
    Then I see an Assorted text message in the Order Summary Page in orange "This item will be randomly selected"
    And I see the Assorted Text message in the order: Item Desc; ITEM CODE; QTY; COLOUR; Assorted Text Msg
    
  Scenario: Add to Basket button in orange, on an iOS App
    Given I browsing the Target website, on an iOS App
    When I select an Assorted product, on an iOS App
    And I am viewing the PDP page
    Then I see the button "I Agree, Add to Basket" in orange
  
  Scenario: Selecting a Non-Assorted product, on an iOS App
    Given I browsing the Target website, on an iOS App
    When I select a Non-Assorted product, on an iOS App
    And I am viewing the PDP page
    Then I DO NOT see any message
    And I see the button "Add to Basket" in green

  Scenario: Clicking Target Store link
    Given I am on the PDP page in iOS App
    And I have selected an assorted product
    When I click on the link "Target Store"
    Then I am taken to the Find Store tab    
    
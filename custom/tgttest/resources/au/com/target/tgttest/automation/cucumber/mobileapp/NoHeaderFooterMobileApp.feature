@notAutomated
Feature: Show content without header and footer for mobile app
In order to provide better experience on moblie app
As a Mobile app customer
I do not want to see the header and footer

Scenario Outline: Showing header and footer depending on the request header
Given hybris received a request
When the request header contain <salesHeader>
Then display header <headerDisplayStatus>
And display footer <footerDisplayStatus>

Examples:
| salesHeader | headerDisplayStatus | footerDisplayStatus |
| mobileApp   | no                  | no                  |
| kiosk       | yes                 | yes                 |
| callcenter  | yes                 | yes                 |
| ebay        | yes                 | yes                 |
| web         | yes                 | yes                 |
| none        | yes                 | yes                 |
 
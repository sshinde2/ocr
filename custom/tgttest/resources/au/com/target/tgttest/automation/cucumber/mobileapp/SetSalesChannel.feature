@notAutomated

Feature: Setting sales channel
  In order to provide better visiblily
  As Target Online
  I want to specify the sales channel based on the sales header

  Scenario Outline: Setting sales channel depending on the request header
    Given hybris received a request
    When the request header contain <salesHeader>
    Then sales channel is <salesChannel>

    Examples: 
      | salesHeader | salesChannel |
      | MobileApp   | mobileApp    |
      | Kiosk       | kiosk        |
      | CallCenter  | callcenter   |
      | eBay        | ebay         |
      | Web         | web          |
      | none        | web          |

@notAutomated
Feature: Departments navigation component (Mobile only)
  	In order to get better conversion
  	As a Marketing
  	I want department navigation on the mobile homepage

  Scenario Outline: Configure department navigation in CMS
    Given CMS has department <department>
    When department navigation component is configured
    Then department <department> is available in department navigation

    Examples: 
      | department |
      | Women      |
      | Men        |
      | Kids       |

@notAutomated
Feature: Displaying Menu item on Mobile and Tab
  In order to increase conversion
  As a marketing 
  I want to display Menu text under the hamburger menu icon

  Scenario Outline: Display text Menu under the menu icon
    Given customer views target web site
    And the device is <deviceType>
    When mega menu is loaded
    Then under the menu icon 'MENU' text should be displayed

    Examples: 
      | deviceType |
      | Mobile     |
      | tablet     |

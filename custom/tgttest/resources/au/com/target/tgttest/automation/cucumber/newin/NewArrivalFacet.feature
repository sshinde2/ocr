@notAutomated
Feature: Facet by a newly arrived products dimension on the product list page
  As a target online customer 
  I want to view products that are newly arrived on the product list page
  So that customer can differentiate new arrivals

  Background: 
    Given user is on product list page for a Category or Brand Page or Search term
    And 'Shop New Arrivals' facet is enabled for that Category or Brand Page or Search term

  Scenario Outline: Display of 'Shop New Arrivals' facet
    Given <numberOfProducts> products are in the current Product List
    And <numberOfNew> products are for 'New Arrivals'
    And <numberInFilter> are in the currently displayed filtered set of products
    And <numberOfNewInFilter> are in the currently displayed filtered set of products
    When the 'Shop by' facet dimensions are displayed
    Then 'Shop New Arrivals' facet is <facetDisplayStatus>
    And 'New Arrivals X' filter is <filterDisplayStatus>

    Examples:
      | numberOfProducts | numberOfNew | numberInFilter | numberOfNewInFilter | facetDisplayStatus | filterDisplayStatus |
      | 5                | 0           | 5              | 0                   | not displayed      | not displayed       |
      | 5                | 3           | 5              | 3                   | displayed          | not displayed       |
      | 5                | 3           | 3              | 3                   | not displayed      | displayed           |
      | 5                | 3           | 4              | 2                   | displayed          | not displayed       |
      | 5                | 3           | 2              | 2                   | not displayed      | displayed           |
      | 5                | 3           | 2              | 0                   | not displayed      | not displayed       |

  Scenario: Choose the 'New Arrivals' filter
    Given 'Shop New Arrivals' facet is displayed
    And therefore 'New Arrivals X' filter is not displayed
    When user chooses 'New Arrivals' within that facet
    Then only 'New Arrivals' products are displayed
    And 'Shop New Arrivals' facet is no longer displayed
    And 'New Arrivals X' filter is displayed

  Scenario: Remove the 'New Arrivals X' filter
    Given 'New Arrivals' filter has been applied
    And therefore 'Shop New Arrivals' facet is NOT displayed
    And 'New Arrivals X' filter is displayed
    When user chooses to remove the 'New Arrivals' filter
    Then both 'New Arrivals' AND products that are NOT 'New Arrivals' are displayed
    And 'Shop New Arrivals' facet is displayed
    And 'New Arrivals X' filter is not displayed

  Scenario: Non-display of 'Shop New Arrivals' facet
    Given user is on the product list page for a Category or Brand Pgae or Search term
    And 'Shop New Arrivals' facet is NOT enabled for that Category or Brand Pgae or Search term
    When 'Shop by' facet dimensions are displayed
    Then 'Shop New Arrivals' facet is NOT displayed
    And 'New Arrivals X' filter is NOT displayed

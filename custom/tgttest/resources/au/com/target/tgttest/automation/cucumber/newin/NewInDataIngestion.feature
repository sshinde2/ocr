@notAutomated
 
Feature: Ingesting data for new arrivals in Endeca
In order to view products with 'new arrivals' tag on the site
As Target Online
I want to ingest data for new arrivals in Endeca

Scenario Outline: Ingesting new arrival data.
Given categories details as:
| category | newToOnlineConfigDays | topLevelCategoryCode |
| men      | N/A                   | w1211                |
| kids     | 14                    | w1222                |
| baby     | 28                    | w1233                |
| women    | 7                     | w1244                |
And product is <productId>
And top level Category is <topLevelCategoryName>
And product creation date is <daysSinceCreated>
And newToOnlineConfigDays at the all products level is <allProductsDays>
When baseline is run
Then the product new arrival value is <newArrival>
And Top level category code display as <topLevelCategoryCode>
 
Examples:
| productId | topLevelCategoryName | daysSinceCreated | allProductsDays | newArrival | topLevelCategoryCode |
| SKU101    | women                | <7               | any             | yes        | W1244                |
| SKU102    | women                | =7               | any             | yes        | W1244                |
| SKU103    | women                | >7               | any             | no         | W1244                |
| SKU201    | kids                 | <14              | any             | yes        | W1222                |
| SKU202    | kids                 | =14              | any             | yes        | W1222                |
| SKU203    | kids                 | >14              | any             | no         | W1222                |
| SKU301    | men                  | <21              | 21              | yes        | W1211                |
| SKU302    | men                  | =21              | 21              | yes        | W1211                |
| SKU303    | men                  | >21              | 21              | no         | W1211                |
| SKU304    | men                  | <21              | blank           | no         | W1211                |
| SKU305    | men                  | =21              | blank           | no         | W1211                |
| SKU306    | men                  | >21              | blank           | no         | W1211                |
| SKU401    | baby                 | <21              | 21              | yes        | W1233                |
| SKU402    | baby                 | =21              | 21              | yes        | W1233                |
| SKU403    | baby                 | >21              | 21              | yes        | W1233                |
| SKU404    | baby                 | <28              | 21              | yes        | W1233                |
| SKU403    | baby                 | =28              | 21              | yes        | W1233                |
| SKU404    | baby                 | >28              | 21              | No         | W1233                |

Scenario: Change of new arrival config days 
Given top level Category is 'Home'
And newToOnlineConfigDays is '7' days
And product 'SKU501' creation date under that category is '<7' days
And new arrival value is 'yes'
When newToOnlineConfigDays is changed to '5' days
And baseline is run
Then the product new arrival value is 'no'

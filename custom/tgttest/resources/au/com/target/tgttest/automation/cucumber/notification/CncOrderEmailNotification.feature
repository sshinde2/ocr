@notAutomated
Feature: notifications for cnc orders
  In order to notify the customer through email when a cnc order is ready to pick up as soon as its ready
  As online store
  I would like to send email notifications to the customer

  Background: 
    Given cnc email notification retry interval is set to 8 times every 7200000 milliseconds

  Scenario Outline: cnc order is ready for pickup during blackout time
    Given cnc order is ready for collection in <store>
    And blackout time is set from 21:00 to 7:00 store time
    When hybris receives a cnc collection notification from pos at <receivedbyHybris>
    Then email notification is sent to customer at <sentTime>

    Examples: 
      | store   | receivedbyHybris | sentTime |
      | Geelong | 20:00            | now      |
      | Geelong | 22:00            | 7am      |
      | Geelong | 8:00             | now      |
      | Perth   | 18:00            | now      |
      | Perth   | 22:30            | 7:00     |
      | Perth   | 8:30             | now      |
      | Perth   | 6:30             | 7:00     |

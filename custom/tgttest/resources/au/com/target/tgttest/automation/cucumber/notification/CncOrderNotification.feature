@cartProductData @deliveryModeData
Feature: notifications for cnc orders
  In order to notify the customer when a cnc order is ready to pick up as soon as its ready
  As online store
  I would like to send Sms notifications to the customer

  Background: 
    Given cnc sms notification retry interval is set to 2 times every 20000 milliseconds

  Scenario Outline: notifications for cnc order
    Given a new cnc order with sales channel as '<salesChannel>'
    And consignment status date for 'ReadyForPickup' is already set to '<ReadyForPickupDay>'
    And consignment status date for 'Pickedup' is already set to '<PickedupDate>'
    When cnc notification received for '<notificationType>'
    Then consignment status date for 'ReadyForPickup' is set to '<ReadyForPickupDayAfterNotification>'
    Then consignment status date for 'Pickedup' is set to '<pickedupDayAfterNotification>'
    And notification '<notificationSentStatus>' sent to eBay
    And carrier code used in notification '<carrierCode>'
    And carrier class used in notification '<carrierClass>'

    Examples: 
      | salesChannel | ReadyForPickupDay | PickedupDate | notificationType | ReadyForPickupDayAfterNotification | pickedupDayAfterNotification | notificationSentStatus | carrierCode | carrierClass | comments                                 |
      | Web          | null              | N/A          | ReadyForPickup   | TODAY                              | N/A                          | N/A                    | N/A         | N/A          | non-partner order                        |
      | Web          | YESTERDAY         | N/A          | ReadyForPickup   | YESTERDAY                          | N/A                          | N/A                    | N/A         | N/A          | non-partner order                        |
      | Web          | YESTERDAY         | YESTERDAY    | Pickedup         | YESTERDAY                          | YESTERDAY                    | N/A                    | N/A         | N/A          | non-partner order                        |
      | Web          | YESTERDAY         | null         | Pickedup         | YESTERDAY                          | TODAY                        | N/A                    | N/A         | N/A          | non-partner order                        |
      | Web          | TODAY             | null         | Pickedup         | TODAY                              | TODAY                        | N/A                    | N/A         | N/A          | non-partner order                        |
      | eBay         | null              | N/A          | ReadyForPickup   | TODAY                              | N/A                          | ReadyForPickup         | CPU         | CPU          | eBay order notified as ReadyForPickup    |
      | eBay         | YESTERDAY         | N/A          | ReadyForPickup   | YESTERDAY                          | N/A                          | N/A                    | N/A         | N/A          | eBay order re-notified as ReadyForPickup |
      | eBay         | YESTERDAY         | null         | Pickedup         | YESTERDAY                          | TODAY                        | Complete               | CPU         | CPU          | eBay order notified as Pickedup          |
      | eBay         | TODAY             | null         | Pickedup         | TODAY                              | TODAY                        | Complete               | CPU         | CPU          | eBay order notified as Pickedup          |
      | eBay         | YESTERDAY         | YESTERDAY    | Pickedup         | YESTERDAY                          | YESTERDAY                    | N/A                    | N/A         | N/A          | eBay order re-notified as Pickedup       |

  Scenario Outline: Auto picked up notifications for eBay cnc order
    Given a new cnc order with sales channel as '<salesChannel>'
    And consignment 'ReadyForPickup' date is '<ReadyForPickupDate>'
    And consignment 'Pickedup' date is '<PickedupDate>'
    And consignment 'AutoPickedup' date is '<AutoPickedupDate>'
    And consignment auto pickedup days are configured as '5'
    When autoPickedupCronJob runs
    Then consignment 'AutoPickedupDate' is '<AutoPickedupDateStatus>'
    And notification '<notificationSentStatus>' sent to eBay
    And carrier code used in notification '<carrierCode>'
    And carrier class used in notification '<carrierClass>'

    Examples: 
      | salesChannel | ReadyForPickupDate | PickedupDate | AutoPickedupDate | AutoPickedupDateStatus | notificationSentStatus | carrierCode | carrierClass | comments                                                  |
      | Web          | 6 days ago         | TODAY        | not saved        | not saved              | N/A                    | N/A         | N/A          | non-partner order                                         |
      | Web          | 6 days ago         | not saved    | not saved        | not saved              | N/A                    | N/A         | N/A          | non-partner order                                         |
      | Web          | not saved          | not saved    | not saved        | not saved              | N/A                    | N/A         | N/A          | non-partner order                                         |
      | eBay         | not saved          | not saved    | not saved        | not saved              | N/A                    | N/A         | N/A          | eBay order not ReadyForPickup                             |
      | eBay         | 6 days ago         | TODAY        | not saved        | not saved              | N/A                    | N/A         | N/A          | eBay order already pickedup                               |
      | eBay         | 6 days ago         | not saved    | 2 days ago       | 2 days ago             | N/A                    | N/A         | N/A          | eBay order already auto-pickedup                          |
      | eBay         | 5 days ago         | not saved    | not saved        | not saved              | N/A                    | N/A         | N/A          | eBay order ReadyforPickup, not applicable for auto-pickup |
      | eBay         | 6 days ago         | not saved    | not saved        | TODAY                  | Complete               | CPU         | CPU          | eBay order ReadyForPickup, applicable for auto-pickup     |

  Scenario Outline: Send CNC Order Ready for Pickup Notification to Fluent
    Given a new cnc order with sales channel as '<salesChannel>'
    And consignment status date for 'ReadyForPickup' is already set to '<ReadyForPickupDay>'
    And consignment status date for 'Pickedup' is already set to '<PickedupDate>'
    And fluent event response is:
      | eventId | entityId | eventStatus | errorMessage |
      | 1       | 1        | SUCCESS     |              |
    When fluent cnc notification received for '<notificationType>' and fluent order id '123'
    Then consignment status date for 'ReadyForPickup' is set to '<ReadyForPickupDayAfterNotification>'
    And consignment status date for 'Pickedup' is set to '<pickedupDayAfterNotification>'
    And notification '<notificationSentStatus>' sent to eBay
    And carrier code used in notification '<carrierCode>'
    And carrier class used in notification '<carrierClass>'
    And consignment event sent to fluent is:
      | name        | entityType   | entitySubtype   |
      | <eventName> | <FULFILMENT> | <entitySubtype> |

    Examples: 
      | salesChannel | ReadyForPickupDay | PickedupDate | notificationType | ReadyForPickupDayAfterNotification | pickedupDayAfterNotification | notificationSentStatus | carrierCode | carrierClass | comments                                 | eventName        | FULFILMENT | entitySubtype |
      | Web          | null              | N/A          | ReadyForPickup   | TODAY                              | N/A                          | N/A                    | N/A         | N/A          | non-partner order                        | READY_FOR_PICKUP | FULFILMENT | CC            |
      | eBay         | YESTERDAY         | N/A          | ReadyForPickup   | YESTERDAY                          | N/A                          | N/A                    | N/A         | N/A          | eBay order re-notified as ReadyForPickup | READY_FOR_PICKUP | FULFILMENT | CC            |

 Scenario Outline: Do not send Fluent CNC Order Ready for Pickup Notification for old order
    Given a new cnc order with sales channel as '<salesChannel>'
    When consignment status date for 'ReadyForPickup' is already set to '<ReadyForPickupDay>'
    And consignment status date for 'Pickedup' is already set to '<PickedupDate>'
    Then consignment status date for 'ReadyForPickup' is set to '<ReadyForPickupDayAfterNotification>'
    And consignment status date for 'Pickedup' is set to '<pickedupDayAfterNotification>'
    And notification '<notificationSentStatus>' sent to eBay
    And carrier code used in notification '<carrierCode>'
    And carrier class used in notification '<carrierClass>'
    And consignment event is not sent to fluent

    Examples:
      | salesChannel | ReadyForPickupDay | PickedupDate | ReadyForPickupDayAfterNotification | pickedupDayAfterNotification | notificationSentStatus | carrierCode | carrierClass |
      | eBay         | YESTERDAY         | N/A          | YESTERDAY                          | N/A                          | N/A                    | N/A         | N/A          |
  
  Scenario Outline: Send CNC Order Picked Up Notification to Fluent
    Given a new cnc order with sales channel as '<salesChannel>'
    And consignment status date for 'ReadyForPickup' is already set to '<ReadyForPickupDay>'
    And consignment status date for 'Pickedup' is already set to '<PickedupDate>'
    And fluent event response is:
      | eventId | entityId | eventStatus | errorMessage |
      | 1       | 1        | SUCCESS     |              |
    When fluent cnc notification received for '<notificationType>' and fluent order id '123'
    Then consignment event sent to fluent is:
      | name        | entityType   | entitySubtype   |
      | <eventName> | <FULFILMENT> | <entitySubtype> |

    Examples: 
      | salesChannel | ReadyForPickupDay | PickedupDate | notificationType | ReadyForPickupDayAfterNotification | pickedupDayAfterNotification | notificationSentStatus | carrierCode | carrierClass | comments                                 | eventName        | FULFILMENT | entitySubtype |
      | Web          | null              | N/A          | ReadyForPickup   | TODAY                              | N/A                          | N/A                    | N/A         | N/A          | non-partner order                        | READY_FOR_PICKUP | FULFILMENT | CC            |
      | eBay         | YESTERDAY         | N/A          | ReadyForPickup   | YESTERDAY                          | N/A                          | N/A                    | N/A         | N/A          | eBay order re-notified as ReadyForPickup | READY_FOR_PICKUP | FULFILMENT | CC            |

  Scenario Outline: Send CNC Order Return To Floor Notification to Fluent
    Given a new cnc order with sales channel as '<salesChannel>'
    And consignment status date for 'ReadyForPickup' is already set to '<ReadyForPickupDay>'
    And consignment status date for 'Pickedup' is already set to '<PickedupDate>'
    And fluent event response is:
      | eventId | entityId | eventStatus | errorMessage |
      | 1       | 1        | SUCCESS     |              |
    When fluent cnc notification received for '<notificationType>' and fluent order id '123'
    Then consignment event sent to fluent is:
      | name        | entityType   | entitySubtype   |
      | <eventName> | <FULFILMENT> | <entitySubtype> |

    Examples: 
      | salesChannel | ReadyForPickupDay | PickedupDate | notificationType | ReadyForPickupDayAfterNotification | pickedupDayAfterNotification | notificationSentStatus | carrierCode | carrierClass | comments                                 | eventName        | FULFILMENT | entitySubtype |
      | Web          | null              | N/A          | ReadyForPickup   | TODAY                              | N/A                          | N/A                    | N/A         | N/A          | non-partner order                        | READY_FOR_PICKUP | FULFILMENT | CC            |
      | eBay         | YESTERDAY         | N/A          | ReadyForPickup   | YESTERDAY                          | N/A                          | N/A                    | N/A         | N/A          | eBay order re-notified as ReadyForPickup | READY_FOR_PICKUP | FULFILMENT | CC            |

  Scenario: cnc order is unpacked and returned to floor
    Given cnc order is ready for collection
    When cnc notification received for 'ReturnedToFloor'
    Then consignment status date for 'ReturnedToFloor' is set to 'TODAY'

  Scenario: multiple calls for returned to floor notification
    Given cnc order is ready for collection
    And consignment status date for 'ReturnedToFloor' is already set to 'YESTERDAY'
    When cnc notification received for 'ReturnedToFloor'
    Then consignment status date for 'ReturnedToFloor' is set to 'YESTERDAY'

  @notAutomated
  Scenario Outline: cnc order is ready for pickup during blackout window
    This scenario is not automoated as the blackout window is configured in webmethods.

    Given cnc order is ready for collection in <store>
    And blackout window is set from 9pm to 7am store time
    And the Perth time is 3 hours behind Melbourne time
    When hybris receives a cnc collection notification from pos at <received by Hybris>
    Then sms notification is sent to customer <sent Time>

    Examples: 
      | store   | received by Hybris | sent Time |
      | Geelong | 8pm                | now       |
      | Geelong | 10pm               | 7am       |
      | Geelong | 8am                | now       |
      | Perth   | 8pm                | now       |
      | Perth   | 10pm               | now       |
      | Perth   | 8am                | 10am      |

  Scenario: Try to send cnc sms notification when webmethods is offline
    Given cnc order is ready for collection
    When hybris receives a cnc collection notification from pos
    And webmethod is offline
    Then hybris retries to send sms notification

  Scenario: Try to send cnc sms notifications when receiving errors from the sms notification provider
    Given cnc order is ready for collection
    When hybris receives a cnc collection notification from pos
    And sms service provider is not able to process SMS request
    Then hybris retries to send sms notification

  Scenario: Try to send cnc sms notifications when receiving errors from the sms notification provider
    Given sms service provider is not able to process SMS request
    And cnc order is ready for collection
    When hybris receives a cnc collection notification from pos
    Then hybris will not send sms notification

  Scenario: cnc order is ready for pickup on site and mobile number provided is correct and with +
    Given cnc order is ready for collection
    And mobile number in the order is '+61411111111'
    When hybris receives a cnc collection notification from pos
    Then cnc sms notification for onsite is sent to the customer

  Scenario: cnc order is ready for pickup on site and mobile number saved in the order is correct but without +
    Given cnc order is ready for collection
    And mobile number in the order is '61411111111'
    When hybris receives a cnc collection notification from pos
    Then cnc sms notification for onsite is sent to the customer

  Scenario: cnc order is ready for pickup on site however mobile number provided is not a good AU mobile number
    Given cnc order is ready for collection
    And mobile number in the order is '+61352462000'
    When hybris receives a cnc collection notification from pos
    Then hybris will not send sms notification

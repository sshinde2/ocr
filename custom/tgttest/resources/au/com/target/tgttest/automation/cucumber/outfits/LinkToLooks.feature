@lookDetailsData @cartProductData @sessionCatalogVersion
Feature: Link to Looks that a product belongs to
  In order to give more looks for a product
  As a Target online user I want to link to Looks that a Product belongs to.

  Notes:
   * Products with single Colour Variant are setup in impex as:
       look     status   productsInLook
       LOOK001  visible  W100007,W100008,W1000071,W1000081,W1000091
       LOOK002  visible  W100008,W1000071,W1000081,W1000091
       LOOK003  visible  W1000071,W1000081,W1000091
       LOOK004  visible  W1000091,W1000051
       LOOK005  visible  W1000091,W1000061
       LOOK006  visible  W1000081,W1000051

   * Products with multiple Colour Variants are setup in impex as:
       baseProduct    cvProducts
       W100001        10000110,10000120
       W100010        10001010,10001020
       W1000011       100010011
       W1000021       100010021
       W1000031       100010031

    * Products with multiple Colour Variants with/without size variants are setup as:
       baseProduct  cvProducts  svProduct          Looks Displayed
       W100008      10000810    N/A                LOOK001,LOOK002
       W100007      10000710    N/A                LOOK001
       W100001      10000110    10000111,10000112  LOOK010,LOOK011,LOOK014,LOOK015
       W1000061     100010061   100010961          LOOK005
       W1000051     100010051   100010951          LOOK004,LOOK006
       W1000071     100010071   100010971          LOOK001,LOOK002,LOOK003
       W1000081     100010081   100010981          LOOK001,LOOK002,LOOK003,LOOK006

   * Products with multiple Colour Variants,and across multiple Collections are setup as:
       look     status   productsInLook            collection  approvalStatus
       LOOK010  visible  W100001,W100010,W1000011  COLL001     approved
       LOOK011  visible  W100001,W100010,W1000011  COLL001     approved
       LOOK012  visible  W100001,W1000021          COLL002     unapproved
       LOOK013  visible  W100010,W1000021          COLL003     approved
       LOOK014  visible  W100001,W1000021          COLL002     approved
       LOOK015  visible  W100001,W1000031          COLL001     approved
       LOOK016  hidden   W100003                   COLL001     approved
       LOOK017  hidden   W100002                   COLL002     unapproved

  Background:
    Given the shop the look are setup in the system running the mentioned impex

  Scenario Outline: View Product Details Page for a Colour Variant product.
  The number of looks returned will be limited to value set for tgtstorefront.product.looks.maximium.size which is 4.
    Given the 'step.shop.the.look' feature switch is <switch>
    When product details is retrieved for colour variant product '<productId>'
    Then the looks in the grid are '<looksDisplayed>'

    Examples: 
      | productId | looksDisplayed                  | switch   |
      | W100007   | LOOK001                         | disabled |
      | W100008   | LOOK001,LOOK002                 | disabled |
      | W1000071  | LOOK001,LOOK002,LOOK003         | disabled |
      | W1000081  | LOOK001,LOOK002,LOOK003,LOOK006 | disabled |
      | W1000091  | LOOK001,LOOK002,LOOK003,LOOK004 | disabled |
      | W1000051  | LOOK004,LOOK006                 | disabled |
      | W1000061  | LOOK005                         | disabled |
      | P6004     | LOOK1,casualWearLook1           | enabled  |

  Scenario Outline: View Product Details Page for a Colour Variant product with multiple colour variants.
    Given the 'step.shop.the.look' feature switch is <switch>
    When product details is retrieved for colour variant product '<productId>'
    Then the looks in the grid are '<looksDisplayed>'

    Examples: 
      | productId | looksDisplayed                                      | switch   |
      | W100001   | LOOK010,LOOK011,LOOK014,LOOK015                     | disabled |
      | W100010   | LOOK010,LOOK011                                     | disabled |
      | W1000011  | LOOK010 ,LOOK011                                    | disabled |
      | W1000021  | LOOK012,LOOK014                                     | disabled |
      | W1000031  | LOOK015                                             | disabled |
      | P8069     | womenSpringCasualLook6,LOOK6,womenSpringCasualLook4 | enabled  |

  Scenario Outline: View Product Details Page for a Size Variant product.
    Given the 'step.shop.the.look' feature switch is <switch>
    When product details is retrieved for size variant product '<productId>'
    Then the looks in the grid are '<looksDisplayed>'

    Examples: 
      | productId | looksDisplayed                  | switch   |
      | 100010961 | LOOK005                         | disabled |
      | 100010951 | LOOK004,LOOK006                 | disabled |
      | 100010971 | LOOK001,LOOK002,LOOK003         | disabled |
      | 100010981 | LOOK001,LOOK002,LOOK003,LOOK006 | disabled |
      | 100010951 | womenSpringCasualLook4          | enabled  |

  @notAutomated
  Scenario: View Product Details Page for multiple Colour Variants and across multiple collections
    Given the 'step.shop.the.look' feature switch is <switch>
    When product details is retrieved for colour variant product '<productId>'
    Then the looks in the grid are:
      | productId | looksGrid     | looksDisplayed                                | switch   |
      | W100001   | displayed 2x2 | LOOK010,LOOK011,LOOK014,LOOK015               | disabled |
      | W100010   | displayed 2x2 | LOOK010,LOOK011,LOOK013                       | disabled |
      | W1000011  | displayed 2x1 | LOOK010 ,LOOK011                              | disabled |
      | W1000021  | displayed 2x1 | LOOK012,LOOK014                               | disabled |
      | W1000031  | displayed 1x1 | LOOK015                                       | disabled |
      | P6004     | displayed 1x1 | casualWearLook1                               | enabled  |
      | P8069     | displayed 2x1 | womenSpringCasualLook6,womenSpringCasualLook4 | enabled  |

  @notAutomated
  Scenario: View Product Details Page for all types of products for front end purpose
    Given the 'step.shop.the.look' feature switch is <switch>
    When product details is retrieved for colour variant product '<productId>'
    Then the looks in the grid are:
      | Product  | Looks Displayed                 | looks Grid    | switch   |
      | W100008  | LOOK001,LOOK002                 | Displayed 2x1 | disabled |
      | W100007  | LOOK001                         | Displayed 1x1 | disabled |
      | W100001  | LOOK010,LOOK011,LOOK014,LOOK015 | Displayed 2x2 | disabled |
      | W1000071 | LOOK001,LOOK002,LOOK003         | Displayed 2x2 | disabled |
      | W1000081 | LOOK001,LOOK002,LOOK003,LOOK006 | Displayed 2x2 | disabled |
      | W1000091 | LOOK001,LOOK002,LOOK003,LOOK004 | Displayed 2x2 | disabled |
      | W1000051 | LOOK004,LOOK006                 | Displayed 2x1 | disabled |
      | W1000061 | LOOK005                         | Displayed 1x1 | disabled |
      | 10000810 | LOOK001,LOOK002                 | Displayed 2x1 | disabled |
      | 10000110 | LOOK010,LOOK011,LOOK014,LOOK015 | Displayed 2x2 | disabled |
      | 10000710 | LOOK001                         | Displayed 1x1 | disabled |
      | 10000111 | LOOK010,LOOK011,LOOK014,LOOK015 | Displayed 2x2 | disabled |
      | 10000112 | LOOK010,LOOK011,LOOK014,LOOK015 | Displayed 2x2 | disabled |
      | W100003  | N/A                             | N/A           | disabled |
      | W100002  | N/A                             | N/A           | disabled |
      | W100002  | N/A                             | N/A           | enabled  |

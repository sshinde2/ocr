@notAutomated
Feature: Outfit product details - store front
  	In order to view, customise collection of products as a whole outfit
    As an online customer 
    I want to see product details in Look page

  Background: Online customer is viewing the "Casual Look" page of "Summer Collection" collection

  Scenario Outline: Online customer can see product details in look page
    Given color variant products are displayed in "Summer Collection" look page
    When customer chooses displayMethod
      | displayMethod                                                  |
      | choosing "Get the look" from the Look Summary                  |
      | clicking on the small image of the Product in the Look Summary |
      | clicking on the product in the Product List                    |
      | navigating to "Next product" from any product except the last  |
    Then product details are displayed for product <product>
    And hero image <heroImage> is displayed for default color variant product
    And small secondary images <smallSecondaryImages> are displayed for default color variant product
    And "OPTIONS" tab is shown and content is displayed
    And "DESCRIPTION" tab is is displayed when product has non-blank description
    And "FEATURES" tab is is displayed when product has non-blank features

    Examples: 
      | product  | name     | heroImage  | smallSecondaryImages     |
      | 10000011 | product1 | heroImage1 | image11,image12, image13 |
      | 10000121 | product2 | heroImage2 | image21, image22         |
      | 10000211 | product3 | heroImage3 | N/A                      |
      | 10001010 | product4 | heroImage4 | image41                  |
      | 10000912 | product5 | heroImage5 | N/A                      |

  Scenario Outline: Online customer can see product configuration information in the look page
    Given Online customer chooses to view details of product <product> from look page
    When "OPTIONS" tab is displayed
    Then default color varient is displayed as selected
    And size availability <sizeAvailability> for default color varient is displayed
    And size guide <sizeGuide> is displayed
    And color availability <colorAvailability> for default color varient is displayed
    And stock unavailability <stockUnavailability> is displayed
    And quantity counter is displayed with default value 1
    And price <price> is displayed
    And wasPrice <wasPrice> is displayed
    And Add to basket button <addToBasketButton> is displayed
    And product is added to user "Recently Viewed Items"

    Examples: 
      | product  | name     | sizeAvailability | sizeGuide    | colorAvailability   | stockUnavailability   | price | wasPrice | addToBasketButton |
      | 10000011 | product1 | 30, 32, 34, 38   | "Size Guide" | 'Cherries', 'Cream' | N/A                   | 29.90 | N/A      | "Add to basket"   |
      | 10000011 | product1 | N/A              | N/A          | 'Cream'             | N/A                   | 29.90 | 39.90    | "Add to basket"   |
      | 10000011 | product1 | XS, S, M, L, XL  | "Size Guide" | N/A                 | N/A                   | 50    | N/A      | "Add to basket"   |
      | 10000011 | product1 | 1, 2, 3          | "Size Guide" | 'Sand', 'Navy blue' | 'Product unavailable' | 15    | 20       | N/A               |

  Scenario: Online customer can navigate forward on products
    Given Online customer chooses to view details of product from look page
    When product is not the last product of the list
    Then "Next product" button is displayed

  Scenario: Online customer can finish product navigation
    Given Online customer chooses to view details of product from look page
    When product is the last product of the list
    Then "Finish" button is displayed

  Scenario: Online customer can select an available color variant product
    Given Online customer chooses to view details of product from look page
    And color varients are available for product
    When customer selects a color varient
    And color varient has stock available
    Then hero image is updated
    And secondary images are updated
    And size availability is updated
    And price is updated
    And "Add to basket" button is displayed

  Scenario: Online customer can select an unavailable color variant product
    Given Online customer chooses to view details of product from look page
    And color varients are available for product
    When customer selects a color varient
    And color varient has stock unavailable
    Then hero image is updated
    And secondary images are updated
    And size availability is updated
    And price is updated
    And "Product unavailble" message is displayed
    And "Add to basket" button is not displayed

  Scenario: Online customer can select an available size varient
    Given Online customer chooses to view details of product from look page
    And size varients are available for product
    When customer selects a size varient
    And size varient has stock available
    Then price is updated
    And "Add to basket" button is displayed

  Scenario: Online customer can select an unavailable size varient
    Given Online customer chooses to view details of product from look page
    And size varients are available for product
    When customer selects a size varient
    And size varient has stock unavailable
    Then price is updated
    And "Product unavailble" message is displayed
    And "Add to basket" button is not displayed

  Scenario: Online customer can see product description
    Given Online customer chooses to view details of product from look page
    And customer is viewing "OPTIONS" or "FEATURES" information
    And product has non-blank description
    When customer chooses to view product description
    Then "DESCRIPTION" section replaces current view section
    And product description is displayed

  Scenario: Online customer can see product features
    Given Online customer chooses to view details of product from look page
    And customer is viewing "OPTIONS" or "DESCRIPTION" information
    And product has non-blank features information
    When customer chooses to view product features
    Then "FEATURES" section replaces current view section
    And product features information is displayed

  Scenario: Online customer can see product options
    Given Online customer chooses to view details of product from look page
    And customer is viewing "FEATURES" or "DESCRIPTION" information
    When customer chooses to view product options
    Then "OPTIONS" section replaces current view section
    And product options are displayed with selections as per when the user was last on the "OPTIONS" section
    
  Scenario: Online customer can see select a secondary image of a product
    Given Online customer chooses to view details of product from look page
    When customer chooses a secondary image
    Then hero image is updated with hero version of secondary image    

  Scenario: Online customer can see a zoomed view of product hero image (desktop only)
    Given Online customer chooses to view details of product from look page
    And customer is using a desktop
    When customer hover or touch to zoom
    Then hero image is zoomed    

  Scenario: Online customer can see a zoomed view of product hero image (desktop only)
    Given Online customer chooses to view details of product from look page
    And customer is using a desktop
    And customer hovered or touched to zoom the hero image
    When customer stops hovering or touching the hero image
    Then hero image is unzoomed  

  Scenario: Online customer can see a zoomed view of product hero image (desktop only)
    Given Online customer chooses to view details of product from look page
    And customer is using a desktop
    When customer chooses to enlarge the hero image
    Then the hero image is displayed enlarged in a new window
   
  Scenario: Online customer can see a zoomed view of product hero image (desktop only)
    Given Online customer chooses to view details of product from look page
    When customer selects '+' or '-'
    Then the quantity is updated   
    
  Scenario: Online customer can navigate forward on products
    Given Online customer chooses to view details of product from look page
    When product is not the last product of the list
    And "Next product" button is displayed
    When customer chooses "Next product" button
    Then the product details of current product is closed
    And product details of next product is displayed

  Scenario: Online customer can finish product navigation
    Given Online customer chooses to view details of product from look page
    When product is the last product of the list
    And "Finish" button is displayed
    When customer chooses "Finish" button
    Then product details of current product is closed
    And look summary is displayed
     
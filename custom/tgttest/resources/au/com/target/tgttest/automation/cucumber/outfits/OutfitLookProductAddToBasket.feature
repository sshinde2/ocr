@notAutomated
Feature: Outfit product add to basket - store front
  	In order to purchase collection of products as a whole outfit
    As an online customer 
    I want to add product to basket in Look page

  Scenario Outline: Online customer can add to basket when stock available
    Given Online customer has navigated to look <look> page
    And opened the product details section for product <product>
    And selected the color variant <colorVariant>
    And selected the size variant <sizeVariant>
    And selected the quantity <quantity>
    And the product price is <price>
    And product has stock <stock>
    And product <product> is not the last product of the look <look>
    And "Add to basket" button is visible
    When customer chooses to add to basket
    Then feedback provided is <feedback>
    And the quantity added to basket is <addedQuantity>
    And tally of items added from the look is increased by <addedQuantity>
    And "Your look" price is increased by product of <price> and <addedQuantity>
    And the small image of the product is displayed in the Look Summary
    And "View basket" button is available in Look Summary
    And product details for product <product> is closed
    And prodcut details for next product <nextProduct> is opened

    Examples: 
      | look        | product  | colorVariant | sizeVariant | quantity | stock | price | addedQuantity | addedPrice | nextProduct | feedback                                                                                                                 |
      | Casual Look | 10000011 | Cream        | 12          | 2        | 10    | 20    | 2             | 40         | 10000121    | "Added to basket"                                                                                                        |
      | Casual Look | 10000121 | Navy blue    | 14          | 5        | 3     | 15    | 3             | 45         | 10000211    | "This product isn't available in the requested quantity. An amended quantity of 3 item/s has been added to your basket." |

  Scenario Outline: Online customer can not add to basket when stock is not availble for size variant
    Given Online customer has navigated to look <look> page
    And opened the product details section for product <product>
    And selected the color variant <colorVariant>
    When customer chooses a size variant <sizeVariant>
    And size variant is unavailable
    Then feedback <feedback> is displayed

    Examples: 
      | look        | product  | colorVariant | sizeVariant | feedback                                                                        |
      | Casual Look | 10000121 | Navy blue    | 14          | "We're sorry. This product isn't available in the quantity that you've chosen." |

  Scenario Outline: Online customer can not add to basket when size is not selected
    Given Online customer has navigated to look <look> page
    And opened the product details section for product <product>
    And selected the color variant <colorVariant>
    And "Add to basket" button is visible
    When customer chooses to add to basket
    Then feedback <feedback> is displayed

    Examples: 
      | look        | product  | colorVariant | quantity | feedback                      |
      | Casual Look | 10000121 | Navy blue    | 5        | "Please select a size option" |

  Scenario: Online customer can see product details of the look from selecting product images in look summary
    Given Online customer has navigated to "Casual Look" page
    And added a product of the look to basket
    And small image for product is displayed in look summary
    When customer selects small image for added product
    Then product details section for product in look page opens

  Scenario: Online customer can view basket
    Given Online customer has navigated to "Casual Look" page
    And added a product to basket
    And "View basket" button is visible
    When customer selects "View basket"
    Then basket page is displayed

  Scenario: Online customer can see details of product added to basket
    Given Online customer has navigated to "Casual Look" page
    And added a product to basket
    When customer choses to see details of product added to basket
    Then product details section is displayed
    And "Add to basket" button is not displayed
    And "Added to basket" message is displayed

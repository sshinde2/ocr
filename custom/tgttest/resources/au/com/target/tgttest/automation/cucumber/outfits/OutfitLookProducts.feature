@notAutomated
Feature: Outfit Look page - store front
  	In order to view, customise collection of products as a whole outfit
    As an online customer 
    I want to see products in Look page

  Scenario Outline: Online customer can see list of products in look page
    Given Online customer navigates to collection <collection> page
    And look <look> is visible
    When customer chooses to view the look <look>
    Then look <look> page is displayed
    And products <productsInLook> are displayed in configured order

    Examples: 
      | look               | collection       | productsInLook                                   |
      | Summer Office Look | SummerCollection | 10000011, 10000121, 10000211, 10001010, 10000912 |
      | Summer Party Look  | SummerCollection | 10000011, 10000121, 10000211                     |
      | Summer Casual Look | SummerCollection | 10001010, 10000121, 10000011, 10000912           |
      | Winder Casual Look | WinterCollection | N/A                                              |

  Scenario Outline: Online customer can see product information summary
    Given Online customer navigates to collection "Summer Collection" page
    And look "Casual Look" is visible
    When customer chooses to view the look
    Then look "Casual Look" page is displayed
    And products are displayed in configured order
    And a small image <image> is for product <product> displayed
    And product name <name> is displayed
    And price <price> for product <product> is displayed
    And was price <wasPrice> for product <product> is displayed
    And product rating <rating> is displayed
    And out of stock <outOfStockMessage> for product <product> is displayed

    Examples: 
      | product  | name     | image  | price   | wasPrice | rating | outOfStockMessage   |
      | 10000011 | product1 | image1 | 50      | 59.90    | 5      | N/A                 |
      | 10000121 | product2 | image2 | 49.90   | N/A      | 3      | Product unavailable |
      | 10000211 | product3 | image3 | 15 - 16 | N/A      | 1      | N/A                 |
      | 10001010 | product4 | image4 | 24.90   | 29.90    | N/A    | Product unavailable |
      | 10000912 | product5 | image5 | 9.90    | N/A      | N/A    | N/A                 |

@cartProductData @deliveryModeData
Feature: PayPal Payments
  In order to purchase products
  As a customer
  I need to use my paypal account in the checkout process

  Scenario: Placing an Order with PayPal
    Given a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
      | 10001030 | 2   | 15    |
      | 10000710 | 3   | 8     |
    And payment method is 'paypal'
    When order is placed
    Then order is:
      | total | subtotal | deliveryCost | totalTax |
      | 73.0  | 64.0     | 9.0          |          |
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000911 | 1   | 1000  | 1000      |
      | 10001030 | 2   | 1500  | 1500      |
      | 10000710 | 3   | 800   | 800       |
    And tlog type is 'sale'
    And tlog shipping is 900
    And tlog total tender is 7300
    And tlog tender type is 'PAYPAL'
    And the accertify payment entries:
      | paymentType | paypalAmount | paypalStatus |
      | PayPal      | 73.00        | ACCEPTED     |

  Scenario: Placing an Order with PayPal Payment when PayPal rejects the payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
    And payment method is 'paypal'
    And paypal rejects the payment
    When order is placed
    Then place order result is 'PAYMENT_FAILURE'

  Scenario: Placing an Order with PayPal Payment when paypal is down and retry successful
    Given paypal is down during payment capture
    And a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
    And payment method is 'paypal'
    When order is placed
    Then order is in status 'INPROGRESS'

  Scenario: placing an Order with PayPal Payment when paypal is down and retry failed
    Given paypal is down
    And a cart with entries:
      | product  | qty | price |
      | 10000911 | 1   | 10    |
    And payment method is 'paypal'
    When order is placed
    Then order is in status 'CANCELLED'

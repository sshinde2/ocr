@initPreOrderProducts @cartProductData
Feature: IPG - Use ipg payment to pay for preOrder initial deposit
  In order to purchase pre order product
  As an online customer
  I want to use make payment via ipg using creditcard for initial deposit
  
  Background:
    Given a registered customer goes into the checkout process
    And user checkout via spc
    And giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |

  Scenario: Placing an order with credit card.
    Given set fastline stock:
      | product          | preOrder | maxPreOrder | reserved | available |
      | V1111_preOrder_1 | 0        | 100         |  10      |    50     |
    And a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 2   | 300   |
    And registered user checkout
    And delivery mode is 'express-delivery'    
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
        | PaymentEntry |
        | CC1,10   |
    When pre order is placed
    And order is in status 'PARKED'
    And the payment details of the transaction will be recorded correctly
    And fastline stock is:
      | product          | preOrder | maxPreOrder|reserved | available |
      | V1111_preOrder_1 | 2        | 100        | 10      | 50        |
      
  @notAutomated
  Scenario: IPG is down while palcing order and max retries attempt is reached
    Given a cart with entries:
      | product          | qty | price |
      | V5555_preOrder_5 | 1   | 300   |
    And registered user checkout
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
        | PaymentEntry |
        | CC1,10   |
    And ipg capture payment is down
    When pre order is placed
    And place order result is 'PAYMENT_FAILURE'
    And order is in status 'CANCELLED'
    And order payment is in status 'REJECTED'
    And a CS ticket is created for the order
    And CS ticket subject is 'Payment retries has exceeded allowed retries'
@cartProductData @deliveryModeData
Feature: Display reduced or free delivery - Product details page and quick view
  
  In order to promote reduced or free delivery
  As the online store
  I want to display appropriate delivery text and sticker on product details page and quick view

  Background: 
    Given product delivery modes:
      | product | deliveryMode                                     |
      | W100000 | express-delivery,home-delivery,click-and-collect |
      | W100001 | express-delivery,home-delivery,click-and-collect |
      | W100002 | express-delivery,home-delivery,click-and-collect |
      | W100003 | express-delivery,home-delivery,click-and-collect |
      | W100004 | express-delivery,home-delivery,click-and-collect |
      | W100005 | express-delivery,home-delivery,click-and-collect |
      | W100007 | express-delivery,home-delivery,click-and-collect |
      | W100008 | express-delivery,home-delivery                   |
      | W100009 | home-delivery,click-and-collect                  |
      | W100010 | express-delivery,home-delivery                   |
      | W100011 | home-delivery,click-and-collect                  |
      | W100012 | home-delivery                                    |
      | W100013 | click-and-collect                                |
      | W100014 | express-delivery                                 |
      | W100015 | express-delivery,home-delivery                   |

  Scenario Outline: delivery promotion text on product details page and quick view Page
    Given a product with '<productCode>'
    And delivery promotion ranks order is '<deliveryPromotionRank>'
    And express delivery promo display settings are '<expressDeliveryPromo>'
    And home delivery promo display settings are '<homeDeliveryPromo>'
    And cnc delivery promo display settings are '<CnCPromo>'
    When product details are retrieved
    Then delivery promotion sticker is '<deliveryPromotionSticker>'
    And delivery promotion text is '<deliveryPromotionText>'

    Examples: 
      | productCode | deliveryPromotionRank          | expressDeliveryPromo                 | homeDeliveryPromo                    | CnCPromo                             | deliveryPromotionSticker | deliveryPromotionText  |
      | W100000     | express,home,click-and-collect | STICKER PRESENT,TEXT NOT-PRESENT     | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT         | express sticker          | home or cnc            |
      | W100001     | home,express,click-and-collect | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT PRESENT         | STICKER PRESENT,TEXT PRESENT         | home sticker             | home or cnc            |
      | W100002     | express,home,click-and-collect | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT         | express sticker          | express or home or cnc |
      | W100003     | click-and-collect,home,express | STICKER PRESENT,TEXT NOT-PRESENT     | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT         | cnc sticker              | cnc or home            |
      | W100004     | click-and-collect,home,express | STICKER PRESENT,TEXT PRESENT         | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT PRESENT         | cnc sticker              | cnc or home or express |
      | W100005     | home,express,click-and-collect | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER PRESENT,TEXT NOT-PRESENT     | cnc sticker              | home                   |
      | W100007     | express,home,click-and-collect | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | NONE                   |
      | W100008     | express,home                   | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT PRESENT         | N/A                                  | home sticker             | home                   |
      | W100009     | home,click-and-collect         | N/A                                  | STICKER NOT-PRESENT,TEXT NOT-PRESENT | STICKER PRESENT,TEXT PRESENT         | cnc sticker              | cnc                    |
      | W100010     | express,home                   | STICKER PRESENT,TEXT NOT-PRESENT     | STICKER NOT-PRESENT,TEXT NOT-PRESENT | N/A                                  | express sticker          | NONE                   |
      | W100011     | home,click-and-collect         | N/A                                  | STICKER NOT-PRESENT,TEXT PRESENT     | STICKER NOT-PRESENT,TEXT NOT-PRESENT | NONE                     | home                   |
      | W100012     | home                           | N/A                                  | STICKER PRESENT,TEXT NOT-PRESENT     | N/A                                  | home sticker             | NONE                   |
      | W100013     | click-and-collect              | N/A                                  | N/A                                  | STICKER PRESENT,TEXT NOT-PRESENT     | cnc sticker              | NONE                   |
      | W100014     | express                        | STICKER NOT-PRESENT,TEXT NOT-PRESENT | N/A                                  | N/A                                  | NONE                     | NONE                   |
      | W100015     | express,home                   | STICKER PRESENT,TEXT PRESENT         | STICKER PRESENT,TEXT PRESENT         | N/A                                  | express sticker          | express or home        |

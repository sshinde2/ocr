@notAutomated
Feature: Display reduced or free delivery - Product list page

In order to promote reduced or free delivery
As the online store
I want to display appropriate delivery sticker on product list page

Scenario Outline: delivery promotion sticker on product list Page

Given product delivery modes:
| productCode | deliveryModesAllowed                             |
| 10000011    | express-delivery,home-delivery,click-and-collect |
| 10000012    | express-delivery,home-delivery,click-and-collect |
| 10000013    | express-delivery,home-delivery,click-and-collect |
| 10000014    | express-delivery,home-delivery,click-and-collect |
| 10000015    | express-delivery,home-delivery,click-and-collect |
| 10000016    | express-delivery,home-delivery,click-and-collect |
| 10000017    | express-delivery,home-delivery,click-and-collect |
| 10000018    | express-delivery,home-delivery                   |
| 10000019    | home-delivery,click-and-collect                  |
| 10000020    | express-delivery,home-delivery                   |
| 10000021    | home-delivery,click-and-collect                  |
| 10000022    | home-delivery                                    |
| 10000023    | click-and-collect                                |
| 10000024    | express-delivery                                 |
| 10000025    | express-delivery,home-delivery                   |
And product <ProductCode>
And delivery promotion ranks order is <deliveryPromotionRank>
And express delivery promo sticker settings are <expressDeliveryPromo>
And home delivery promo sticker settings are <homeDeliveryPromo>
And cnc delivery promo sticker settings are <CnCPromo>
When product is displayed on the product list page
Then delivery promotion sticker is <deliveryPromotionSticker>

Examples:
| productCode | deliveryPromotionRank | expressDeliveryPromo | homeDeliveryPromo   | CnCPromo            | deliveryPromotionSticker |
| 10000011    | express,home,cnc      | STICKER PRESENT      | STICKER NOT-PRESENT | STICKER PRESENT     | express                  |
| 10000012    | home,express,cnc      | STICKER PRESENT      | STICKER NOT-PRESENT | STICKER PRESENT     | express                  |
| 10000013    | express,home,cnc      | STICKER PRESENT      | STICKER NOT-PRESENT | STICKER PRESENT     | express                  |
| 10000014    | cnc,home,express      | STICKER PRESENT      | STICKER NOT-PRESENT | STICKER PRESENT     | cnc                      |
| 10000015    | express,home,cnc      | STICKER NOT-PRESENT  | STICKER PRESENT     | STICKER PRESENT     | home                     |
| 10000016    | home,express,cnc      | STICKER NOT-PRESENT  | STICKER NOT-PRESENT | STICKER PRESENT     | cnc                      |
| 10000017    | express,home,cnc      | STICKER NOT-PRESENT  | STICKER NOT-PRESENT | STICKER NOT-PRESENT | NONE                     |
| 10000018    | express,home          | N/A                  | STICKER PRESENT     | STICKER NOT-PRESENT | home                     |
| 10000019    | home,cnc              | STICKER PRESENT      | N/A                 | STICKER PRESENT     | cnc                      |
| 10000020    | express,home          | N/A                  | STICKER NOT-PRESENT | STICKER PRESENT     | NONE                     |
| 10000021    | home,cnc              | STICKER NOT-PRESENT  | N/A                 | STICKER NOT-PRESENT | NONE                     |
| 10000022    | home                  | N/A                  | N/A                 | STICKER PRESENT     | NONE                     |
| 10000023    | cnc                   | N/A                  | N/A                 | STICKER PRESENT     | cnc                      |
| 10000024    | express               | N/A                  | STICKER PRESENT     | STICKER PRESENT     | NONE                     |
| 10000025    | express,home          | STICKER PRESENT      | STICKER PRESENT     | STICKER PRESENT     | express                  |

@sessionCatalogVersion
Feature: Fetching Product deal information
  In order to see what the current deal state is for a product
  As a Customer
  I want to retrieve the product deal information
  
  Description: The products used for testing the scenarios has been setup during 
  initialization process using the following impex-
  /tgtinitialdata/import/productCatalogs/targetProductCatalog/automationTest/automation-fetch-products-setup.impex

  Scenario: Fetch details of a product that is part of a deal where no part of the deal has been fulfilled
    When the product data with deal message is retrieved using the variant code 80100100
    Then the product should be retrieved with the following deal details:
      | productCode | dealDescription          | couldFireMessage |
      | 80100100    | Buy get percent off deal | null             |

  Scenario: Fetch details of a product that is the reward for a deal where the qualifier is already in the cart
    Given a cart with entries:
      | product  | qty |
      | 80100100 | 1   |
    When the product data with deal message is retrieved using the variant code 81100100
    Then the product should be retrieved with the following deal details:
      | productCode | dealDescription          | couldFireMessage                |
      | 81100100    | Buy get percent off deal | Buy Get Could Have More Rewards |

  Scenario: Fetch details of a product that is the qualifier for a deal where the reward is already in the cart
    Given a cart with entries:
      | product  | qty |
      | 81100100 | 1   |
    When the product data with deal message is retrieved using the variant code 80100100
    Then the product should be retrieved with the following deal details:
      | productCode | dealDescription          | couldFireMessage   |
      | 80100100    | Buy get percent off deal | Buy Get could fire |

  Scenario: Fetch details of a size variant that is the qualifier of a deal
    When the product data with deal message is retrieved using the variant code 84102101
    Then the product should be retrieved with the following deal details:
      | productCode | dealDescription          | couldFireMessage |
      | 84102101    | Buy get percent off deal | null             |

  Scenario: Fetch details of a size variant that is the reward of a deal
    When the product data with deal message is retrieved using the variant code 83100101
    Then the product should be retrieved with the following deal details:
      | productCode | dealDescription          | couldFireMessage |
      | 83100101    | Buy get percent off deal | null             |

  Scenario: Fetch details of a colour variant with a size variant that is the qualifier of a deal
    When the product data with deal message is retrieved using the variant code 84102100
    Then the product should be retrieved with the following deal details:
      | productCode | dealDescription          | couldFireMessage |
      | 84102100    | Buy get percent off deal | null             |

  Scenario: Fetch details of a colour variant with multiple size variants where only one is the reward of a deal
    When the product data with deal message is retrieved using the variant code 83100100
    Then the product should be retrieved with the following deal details:
      | productCode | dealDescription             | couldFireMessage |
      | 83100100    | This item is part of a deal | null             |

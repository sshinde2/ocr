@autoFetchProductData
Feature: Fetching Product For Mobile
  In order to get a view of products natively on mobile 
  As Customer
  I want to retrieve the product details for different combination of products which are present online.
  
  Description: The products used for testing the scenarios has been setup during 
  initialization process using the following impex-
  /tgtinitialdata/import/productCatalogs/targetProductCatalog/automationTest/automation-fetch-products-setup.impex

  Scenario: Fetching details for Product with one colour variant and no sizes.
    Given the product P80100100 exists with 1 colour variant and 0 size variants
    When the product is fetched using the colourvariant code 80100100
    Then the product and the colour variant should be retrieved with the following details:
      | productCode | colourVariantCode | sizeVariantCount | name                     | displayPrice | displayWasPrice | swatchColour | colour     | imageUrlsPresent | videoUrlPresent |
      | P80100100   | 80100100          | 0                | Spider Man Action Figure | $20          | $30             | Blue         | Light Blue | true             | true			|

  Scenario: Fetching details for Product with multiple colour variants and no sizes.
    Given the product P81100100 exists with 3 colour variants and 0 size variants
    When the product is fetched using the colourvariant code 81101100
    Then the product and the colour variant should be retrieved with the following details:
      | productCode | colourVariantCode | sizeVariantCount | name                   | displayPrice | displayWasPrice | swatchColour | colour      |
      | P81100100   | 81100100          | 0                | Granduer Towel - Blue  | $15          | $20             | Blue         | Light Blue  |
      | P81100100   | 81101100          | 0                | Granduer Towel - Green | $20          | $30             | Green        | Light Green |
      | P81100100   | 81102100          | 0                | Granduer Towel - Blue  | $30          | null            | Blue         | Cyan        |

  Scenario: Fetching details for Product with single size variant under a single colour variant
    Given the product P82100100 exists with 1 colour variant and 1 size variant
    When the product is fetched using the colourvariant code P82100100
    Then the product and the colour variant should be retrieved with the following details:
      | productCode | colourVariantCode | sizeVariantCount | sizeVariantCode | name            | displayPrice | displayWasPrice | size | swatchColour | colour | inStock |
      | P82100100   | 82100100          | 1                | 82100101        | Apple Ipad 16GB | $20          | $30             | 16GB | Blue         | Blue   | true    |

  Scenario: Fetching details for Product with multiple size variants under a colour variant
    Given the product P83100100 exists with 1 colour variant and 3 size variant
    When the product is fetched using the colourvariant code 83100103
    Then the product and the colour variant should be retrieved with the following details:
      | productCode | colourVariantCode | sizeVariantCount | sizeVariantCode | name                  | displayPrice | displayWasPrice | size | swatchColour | colour | inStock |
      | P83100100   | 83100100          | 3                | 83100101        | Flannelette Sheet Set | $20          | $30             | L    | Blue         | Blue   | true    |
      | P83100100   | 83100100          | 3                | 83100102        | Flannelette Sheet Set | $20          | $30             | XL   | Blue         | Blue   | true    |
      | P83100100   | 83100100          | 3                | 83100103        | Flannelette Sheet Set | $20          | $30             | XXL  | Blue         | Blue   | true    |

  Scenario: Fetching details for Product with multiple size variants under multiple colour variants
    Given the product P84100100 exists with 3 colour variants and 5 size variants
    When the product is fetched using the colourvariant code 84102100
    Then the product and the colour variant should be retrieved with the following details:
      | productCode | colourVariantCode | sizeVariantCount | sizeVariantCode | name                    | displayPrice | displayWasPrice | swatchColour | colour      | size | displayPriceAtColourLevel | displayWasPriceAtColourLevel | inStock |
      | P84100100   | 84100100          | 2                | 84100101        | Mens Polo Shirt - Blue  | $20          | $30             | Blue         | Light Blue  | L    | $20 - $25                 | $30 - $35                    | true    |
      | P84100100   | 84100100          | 2                | 84100102        | Mens Polo Shirt - Blue  | $25          | $35             | Blue         | Light Blue  | XL   | $20 - $25                 | $30 - $35                    | true    |
      | P84100100   | 84101100          | 1                | 84101102        | Mens Polo Shirt - Green | $20          | $50             | Green        | Light Green | XL   | $20                       | $50                          | false   |
      | P84100100   | 84102100          | 2                | 84102101        | Mens Polo Shirt - Black | $40          | $45             | Black        | Black       | M    | $20 - $40                 | $30 - $45                    | true    |
      | P84100100   | 84102100          | 2                | 84102102        | Mens Polo Shirt - Black | $20          | $30             | Black        | Black       | XL   | $20 - $40                 | $30 - $45                    | true    |
    And the size variant with code 84101101 is not fetched as it is unapproved

 Scenario: Fetching details for Product which are gift cards
    Given the product PGC1000 exists with 1 colour variant and 1 size variants
    When the product is fetched using the colourvariant code PGC1000_iTunes
    Then the product and the colour variant should be retrieved with the following details:
      | productCode | colourVariantCode 	| sizeVariantCount | sizeVariantCode   | name                     | displayPrice | imageUrlsPresent | giftCard | size | inStock |
      | PGC1000     | PGC1000_iTunes        | 1                | PGC1000_iTunes_10 | $10 iTunes Code 		  | $10          | true             | true	   | $10  | true    |
 
  Scenario: Selecting an Assorted product when retrieving details for mobile
	Given an assorted product with code '82100100'
	When the product is fetched using the colourvariant code 82100100
    Then the product and the colour variant should be retrieved with the following details:
     | productCode | colourVariantCode | sizeVariantCount | sizeVariantCode | name            | displayPrice | displayWasPrice | size | swatchColour | colour | inStock |assorted |
     | P82100100   | 82100100          | 1                | 82100101        | Apple Ipad 16GB | $20          | $30             | 16GB | Blue         | Blue   | true    | true    |
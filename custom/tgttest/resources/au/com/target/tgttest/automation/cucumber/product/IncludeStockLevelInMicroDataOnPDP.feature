@notAutomated
Feature: Schema.org stock level entries on Product Detail Page should consider both online and display-only products

  Scenario Outline: Navigate directly to Size Variant product
    Given the user has navigated to the PDP
    And they have navigated directly to size variant <svProductID>
    And whether the size variant is display-only is <displayOnly>
    And that size variant has price <svPrice>
    And that size variant has online stock <svOnlineStock>
    And that size variant has store stock <svStoreStock>
    When the PDP is rendered
    Then the microdata has a "schema.org/Product" microdata section
    And that section has a "name" element with content the product name
    And there is a nested "schema.org/Offer" microdata section
    And that nested section has a "price" element with content <svPrice> as a decimal number
    And that nested section has a "priceCurrency" element with content "AUD"
    And that nested section has an "availability" element with content <stockContent>
    And that nested section has an "itemCondition" element with content "http://schema.org/NewCondition"

    Examples: 
      | svProductId | displayOnly | svPrice | svOnlineStock | svStoreStock | stockContent                  |
      | sv001       | no          | 20.00   | > 0           | > 0          | http://schema.org/InStock     |
      | sv002       | no          | 20.00   | > 0           | = 0          | http://schema.org/OnlineOnly  |
      | sv003       | no          | 20.00   | = 0           | > 0          | http://schema.org/InStoreOnly |
      | sv004       | no          | 20.00   | = 0           | = 0          | http://schema.org/OutOfStock  |
      | sv005       | yes         | 20.00   | n/a           | > 0          | http://schema.org/InStoreOnly |
      | sv006       | yes         | 20.00   | n/a           | = 0          | http://schema.org/OutOfStock  |

  Scenario Outline: Navigate to Colour Variant product with no Size Variants
    Given the user has navigated to the PDP
    And they have navigated to colour variant <cvProductID>
    And whether the colour variant is display-only is <displayOnly>
    And that colour variant has price <cvPrice>
    And that colour variant has online stock <cvOnlineStock>
    And that colour variant has store stock <cvStoreStock>
    And that colour variant has no size variants
    When the PDP is rendered
    Then the microdata has a "schema.org/Product" microdata section
    And that section has a "name" element with content the product name
    And there is a nested "schema.org/Offer" microdata section
    And that nested section has a "price" element with content <cvPrice> as a decimal number
    And that nested section has a "priceCurrency" element with content "AUD"
    And that nested section has an "availability" element with content <stockContent>
    And that nested section has an "itemCondition" element with content "http://schema.org/NewCondition"

    Examples: 
      | cvProductId | displayOnly | cvPrice | cvOnlineStock | cvStoreStock | stockContent                  |
      | cv001       | no          | 20.00   | > 0           | > 0          | http://schema.org/InStock     |
      | cv002       | no          | 20.00   | > 0           | = 0          | http://schema.org/OnlineOnly  |
      | cv003       | no          | 20.00   | = 0           | > 0          | http://schema.org/InStoreOnly |
      | cv004       | no          | 20.00   | = 0           | = 0          | http://schema.org/OutOfStock  |
      | cv005       | yes         | 20.00   | n/a           | > 0          | http://schema.org/InStoreOnly |
      | cv006       | yes         | 20.00   | n/a           | = 0          | http://schema.org/OutOfStock  |

  Scenario Outline: Navigate to Colour Variant product with Size Variants; all size variants either display only or not
    Given the user has navigated to the PDP
    And they have navigated to colour variant <cvProductID>
    And that colour variant has size variants
    And the size variants that are display only are <svDisplayOnly>
    And there are <svWithOnlineStock> size variants with online stock
    And there are <svWithStoreStock> size variants with store stock
    And the lowest priced size variant has price <svLowPrice>
    And the highest priced size variant has price <svHighPrice>
    When the PDP is rendered
    Then the microdata has a "schema.org/Product" microdata section
    And that section has a "name" element with content the product name
    And there is a nested <offerSection> microdata section
    And that nested section has <priceElements> price element(s)
    And that nested section has a "priceCurrency" element with content "AUD"
    And that nested section <availElement> an "availability" element
    And where required, that "availability" element has content <stockContent>
    And that nested section has an "itemCondition" element with content "http://schema.org/NewCondition"

    Examples: 
      | cvProductId | svDisplayOnly | svWithOnlineStock | svWithStoreStock | svLowPrice | svHighPrice | offerSection              | priceElements                   | availElement  | stockContent                  |
      | cv011       | none          | > 0               | > 0              | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/InStock     |
      | cv012       | none          | > 0               | 0                | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/OnlineOnly  |
      | cv013       | none          | 0                 | > 0              | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/InStoreOnly |
      | cv014       | none          | 0                 | 0                | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/OutOfStock  |
      | cv015       | none          | > 0               | > 0              | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |
      | cv016       | none          | > 0               | 0                | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |
      | cv017       | none          | 0                 | > 0              | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |
      | cv018       | none          | 0                 | 0                | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |
      | cv019       | all           | n/a               | > 0              | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/InStoreOnly |
      | cv020       | all           | n/a               | 0                | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/OutOfStock  |
      | cv021       | all           | n/a               | > 0              | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |
      | cv022       | all           | n/a               | 0                | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |

  Scenario Outline: Navigate to Colour Variant product with Size Variants; size variants are mixture of display only or not
    Given the user has navigated to the PDP
    And they have navigated to colour variant <cvProductID>
    And that colour variant has size variants
    And the size variants that are display only are <svDisplayOnly>
    And there are <svWithOnlineStock> *non-display only* size variants with online stock
    And there are <svWithStoreStock> size variants with store stock
    And the lowest priced size variant has price <svLowPrice>
    And the highest priced size variant has price <svHighPrice>
    When the PDP is rendered
    Then the microdata has a "schema.org/Product" microdata section
    And that section has a "name" element with content the product name
    And there is a nested <offerSection> microdata section
    And that nested section has <priceElements> price element(s)
    And that nested section has a "priceCurrency" element with content "AUD"
    And that nested section <availElement> an "availability" element
    And where required, that "availability" element has content <stockContent>
    And that nested section has an "itemCondition" element with content "http://schema.org/NewCondition"

    Examples: 
      | cvProductId | svDisplayOnly | svWithOnlineStock | svWithStoreStock | svLowPrice | svHighPrice | offerSection              | priceElements                   | availElement  | stockContent                  |
      | cv031       | some          | > 0               | > 0              | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/InStock     |
      | cv032       | some          | > 0               | 0                | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/OnlineOnly  |
      | cv033       | some          | 0                 | > 0              | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/InStoreOnly |
      | cv034       | some          | 0                 | 0                | 20.00      | 20.00       | schema.org/Offer          | price 20.00                     | does have     | http://schema.org/OutOfStock  |
      | cv035       | some          | > 0               | > 0              | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |
      | cv036       | some          | > 0               | 0                | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |
      | cv037       | some          | 0                 | > 0              | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |
      | cv038       | some          | 0                 | 0                | 15.00      | 25.00       | schema.org/AggregateOffer | lowprice 15.00, highprice 25.00 | does NOT have | N/A                           |

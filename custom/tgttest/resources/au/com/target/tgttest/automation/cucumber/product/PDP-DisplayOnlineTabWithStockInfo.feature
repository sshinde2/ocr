@notAutomated
Feature: Product Detail Page: Update variant display logic for online tab in PDP
  As a online trading manager,
  I need to ensure that product information including price and stock is loaded on pageload rather than asking user to wait to retrieve data.

  Scenario Outline: Product is available for both online and instore
    Given a product is available on target website
    And available to be sold online and find in store
    And user is able to search and land on the PDP of the selected product
    When user lands on PDP
    Then the real-time online stock data (not the consolidated stock) of all the sellable variants of the product is fetched on pageload (PDP)
    And if the product is not available online but available in any of the stores, then fetch the store consolidated stock so that the product can be displayed as available in network
    And if the product is not available online and in consolidated store stock, then retrieve no stock indication
      | baseProduct | sellableVariants | onlineStockStatus | consolidatedStoreStock | stock data retrieved |
      | P1000       | CP1,CP2          |             20,10 |                     10 | High, Low            |
      | P1000       | CP1,CP2          |             20,50 |                     10 | High, High           |
      | P1000       | CP1,CP2          |              0,50 |                      0 | No, High             |
      | P1000       | CP1,CP2          |              0,50 |                     10 | Network, High        |
      | P1000       | CP1,CP2          |               0,0 |                     70 | Network, Network     |
      | P1000       | CP1,CP2          |               0,0 |                      0 | No, No               |

  Scenario Outline: Map the fetched stock data with stock levels to be displayed in online tab
    Given a base product has colour variants which is sold both online and in store
    And has base product as <baseProduct> and variants as <sellableVariants>
    When the online stock levels are retrieved and available as <stockDataRetrieved>
    And selected variant is <selectedVariant>
    And the selected variant has online stock as <onlineStockStatus> and consolidated store stock as <consolidatedStoreStock>
    Then display the stock levels <UIMessage> as per the below table.

    Examples: 
      | baseProduct | sellableVariants | selectedVariant | onlineStockStatus | consolidatedStoreStock | stockDataRetrieved | UIMessage                    |
      | P1000       | CP1,CP2          | CP1             | >=16              | Any                    | High               | In stock                     |
      | P1000       | CP1,CP2          | CP1             | between 1 and 15  | Any                    | Low                | Limited Stock                |
      | P1000       | CP1,CP2          | CP1             |                 0 | >0                     | Network            | Find in store icon           |
      | P1000       | CP1,CP2          | CP1             |                 0 |                      0 | No                 | Sold out online and in store |

  Scenario Outline: UI Message - icons vs actions
    Given that user is in PDP and in online tab
    When user selects icons, <UIMessage> on page
    And has a preferred store set as <preferredStoreSet>
    Then redirect user to <nextState>

    Examples: 
      | UIMessage          | preferredStoreSet | nextState                                                                  |
      | Find in store icon | false             | navigate to in store tab and open the Find in store pop up                 |
      | Find in store icon | true              | navigate to in store tab and display the variants, price etc as applicable |

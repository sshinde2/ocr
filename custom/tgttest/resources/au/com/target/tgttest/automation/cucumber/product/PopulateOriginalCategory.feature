@sessionStagedCatalogVersion @adminUser
Feature: Checks original category field of product is getting populated or not

  Scenario: Execute job with 0 batch size
    Given batch limit is '0'
    And sub batch limit is '0'
    When job runs for populating original category
    Then the original should be
      | productCode | originalCategory | approvedStatus | catalogVersionName |
      | N5001       | N/A              | approved       | Staged             |
      | N5002       | N/A              | unapproved     | Staged             |
      | N5003       | N/A              | check          | Online             |
      | N5004       | N/A              | approved       | Online             |
      | N5005       | N/A              | unapproved     | Online             |
      | N5013       | N/A              | approved       | Staged             |
      | N5016       | N/A              | unapproved     | Staged             |

  Scenario: populate original category for batchSize no of products
    Given batch limit is '10000'
    And sub batch limit is '100'
    And clearance category code is set to 'W676714'
    When job runs for populating original category
    Then the original should be
      | productCode | originalCategory | approvedStatus | catalogVersionName |
      # originalcategory should be populated as this product is in approved, staged and originalCatgory is empty
      | N5001       | W133034          | approved       | Staged             |
      # originalcategory should be populated as this product is in unapproved, staged and originalCatgory is empty
      | N5002       | W133034          | unapproved     | Staged             |
      # originalcategory should not be populated as this product is in check, online and originalCatgory is empty
      | N5003       | N/A              | check          | Online             |
      # originalcategory should not be populated as this product is in approved, online and originalCatgory is empty
      | N5004       | N/A              | approved       | Online             |
      # originalcategory should not be populated as this product is in unapproved, online and originalCatgory is empty
      | N5005       | N/A              | unapproved     | Online             |
      # originalcategory should not be changed as this product is in approved, staged and originalCatgory is not empty
      | N5007       | W133029          | approved       | Staged             |
      # originalcategory should be populated as this product is in approved, staged and originalCatgory is empty and belongs to clearance category
      | N5013       | W93743           | approved       | Staged             |
      # originalcategory could not be populated as the super category has no relevant other top category
      | N5016       | N/A              | unapproved     | Staged             |

  Scenario: Dont set original category if it is already set
    When job runs for populating original category
    Then original category should not be changed
      | productCode | originalCategory | approvedStatus |
      #Normal product
      | N5007       | W133029          | approved       |
      #Normal product
      | N5008       | W133029          | unapproved     |
      #Clearance product approved
      | N5014       | N330009          | approved       |
      #Clearance product unapproved
      | N5015       | N330009          | unapproved     |

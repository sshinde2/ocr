@initShortLeadTime @initCncLocationData
Feature: As a Target customer I want to know my nearest home or click & collect delivery location details

  Background: 
    Given geolocation setup for certain store locations are:
      | locationText | latitude   | longitude  | locality     | postcode | state |
      | Maroochydore | -26.654025 | 153.088514 | Maroochydore |     4558 | QLD   |
      | Shepparton   | -36.381062 | 145.402138 | Shepparton   |     3630 | VIC   |
      | Bega         | -36.674304 | 149.842456 | Bega         |     2550 | NSW   |
      | Hobart       | -42.881812 | 147.328016 | Hobart       |     7000 | TAS   |
      | Tuggeranong  |    -35.417 |    149.066 | Tuggeranong  |     2900 | ACT   |
      | Palmerston   |   -12.4817 | 130.985051 | Palmerston   |     0830 | NT    |
      | Unley        | -34.949846 | 138.606814 | Unley        |     5061 | SA    |
      | Broome       |    -17.953 |    122.229 | Broome       |     6725 | WA    |

  @notAutomated
  Scenario Outline: Finding nearest store location for hd(home delivery) type
    Given the delivery type 'hd'
    When the user searches for the nearest store location '<locationTextInput>'
    Then the home delivery store location details are:
      | suburb   | postcode   | hdShortLeadTime   | edShortLeadTime   | bulky1ShortLeadTime   | bulky2ShortLeadTime   |
      | <suburb> | <postcode> | <hdShortLeadTime> | <edShortLeadTime> | <bulky1ShortLeadTime> | <bulky2ShortLeadTime> |

    Examples: 
      | locationTextInput | suburb       | postcode | hdShortLeadTime | edShortLeadTime | bulky1ShortLeadTime | bulky2ShortLeadTime |
      | Maroochydore      | Maroochydore |     4558 |               3 |               4 |                   5 |                   6 |
      | Shepparton        | Shepparton   |     3630 |               3 |               3 |                   3 |                   3 |
      | Bega              | Bega         |     2550 |               3 |               3 |                   3 |                   3 |
      | Hobart            | Hobart       |     7000 |               3 |               3 |                   3 |                   3 |
      | Tuggeranong       | Tuggeranong  |     2900 |               3 |               3 |                   3 |                   3 |
      | Palmerston        | Palmerston   |     0830 |               3 |               3 |                   3 |                   3 |
      | Unley             | Unley        |     5061 |               3 |               3 |                   3 |                   3 |
      | Broome            | Broome       |     6725 |               3 |              10 |                  15 |                   9 |

  Scenario Outline: Finding nearest store location for cnc(click & collect) delivery type
    Given the delivery type 'cnc'
    When the user searches for the nearest store location '<locationTextInput>'
    Then the click and collect store location details are:
      | state   | storeName   | storeNumber   | targetOpeningHours   | formattedAddress   | phone   | latitude   | longitude   | timeZone   | postcode   | shortLeadTime   | closed   | acceptCNC   | acceptLayBy   | url   | type   |
      | <state> | <storeName> | <storeNumber> | <targetOpeningHours> | <formattedAddress> | <phone> | <latitude> | <longitude> | <timeZone> | <postcode> | <shortLeadTime> | <closed> | <acceptCNC> | <acceptLayBy> | <url> | <type> |

    Examples: 
      | locationTextInput | storeNumber | state | storeName    | targetOpeningHours | formattedAddress                   | phone          | latitude   | longitude  | timeZone             | postcode | shortLeadTime | longLeadTime | closed | acceptCNC | acceptLayBy | url                          | type   |
      | Maroochydore      |        7038 | QLD   | Maroochydore | target-hours-0     | Plaza Parade, Maroochydore, 4558   | (07) 5479 2949 | -26.654025 | 153.088514 | Australia/Queensland |     4558 |             8 |            1 | false  | true      | true        | /store/qld/maroochydore/7038 | Target |
      | Shepparton        |        7007 | VIC   | Shepparton   | target-hours-0     | High Street, Shepparton, 3630      | (03) 5821 9255 | -36.381062 | 145.402138 | Australia/Victoria   |     3630 |             3 |            2 | false  | true      | true        | /store/vic/shepparton/7007   | Target |
      | Bega              |        8331 | NSW   | Bega         | target-hours-0     | 168 Carp Street, Bega, 2550        | (02) 6492 2365 | -36.674304 | 149.842456 | Australia/NSW        |     2550 |             3 |            3 | false  | true      | true        | /store/nsw/bega/8331         | Target |
      | Hobart            |        7161 | TAS   | Hobart       | target-hours-0     | 52 Elizabeth Street, Hobart, 7000  | (03) 6235 2555 | -42.881812 | 147.328016 | Australia/Tasmania   |     7000 |             5 |            5 | false  | true      | true        | /store/tas/hobart/7161       | Target |
      | Tuggeranong       |        7162 | ACT   | Tuggeranong  | target-hours-0     | Pitman Street, Tuggeranong, 2900   | (02) 6293 5200 |    -35.417 |    149.066 | Australia/ACT        |     2900 |             5 |            9 | false  | true      | true        | /store/act/tuggeranong/7162  | Target |
      | Palmerston        |        7146 | NT    | Palmerston   | target-hours-0     | Temple Terrace, Palmerston, 0830   | (08) 8935 0600 |   -12.4817 | 130.985051 | Australia/North      |     0830 |             5 |            9 | false  | true      | true        | /store/nt/palmerston/7146    | Target |
      | Unley             |        7197 | SA    | Unley        | target-hours-0     | 170 Unley Street, Unley, 5061      | (08) 8291 3777 | -34.949846 | 138.606814 | Australia/South      |     5061 |             5 |            9 | false  | true      | true        | /store/sa/unley/7197         | Target |
      | Broome            |        7291 | WA    | Broome       | target-hours-0     | 106 Frederick Street, Broome, 6725 | (08) 9158 3700 |    -17.953 |    122.229 | Australia/West       |     6725 |             5 |            9 | false  | true      | true        | /store/wa/broome/7291        | Target |

  Scenario Outline: Nearest store location details not found
    Given the delivery type '<deliveryType>'
    When the user searches for the nearest store location '<locationTextInput>'
    Then result will return an error: 'STORE_LOCATION_NOTFOUND_OR_FOUND_MORE_THAN_ONE_LOCATION', 'Sorry, there was a problem locating stores. Please try again.'

    Examples: 
      | deliveryType | locationTextInput |
      | cnc          |                   |

  @notAutomated
  Scenario Outline: Displaying suburb location details with lead times
    Given the customer current delivery location '<location>' or co-ordinates '<latitude>', '<longitude>'
    When suburb API gets called
    Then the suburb search details with location and lead times are:
      | status   | searchTerm   | state   | suburb   | postalCode   | hdShortLeadTime   | edShortLeadTime   | bulky1ShortLeadTime   | bulky2ShortLeadTime   |
      | <status> | <searchTerm> | <state> | <suburb> | <postalCode> | <hdShortLeadTime> | <edShortLeadTime> | <bulky1ShortLeadTime> | <bulky2ShortLeadTime> |

    Examples: 
      | location         | latitude      | longitude     | status  | searchTerm       | state | suburb           | postalCode | hdShortLeadTime | edShortLeadTime | bulky1ShortLeadTime | bulky2ShortLeadTime |
      | Carnegie         |               |               | success | Carnegie         | vic   | Carnegie         |       3163 |               3 |               2 |                   4 |                   5 |
      |             3220 |               |               | success |             3220 | vic   |                  |       3220 |               3 |               3 |                   5 |                   3 |
      |             3220 |               |               | success |             3220 | vic   |                  |       3220 |               3 |               3 |                   5 |                   3 |
      |             3220 |               |               | success |             3220 | vic   |                  |       3220 |               3 |               3 |                   5 |                   3 |
      |             3220 |               |               | success |             3220 | vic   |                  |       3220 |               3 |               3 |                   5 |                   3 |
      |                  |    -36.757786 |    144.278702 | success |                  | vic   | Bendigo          |       3550 |               3 |               3 |                   5 |                   3 |
      |                  |    -34.206841 |    142.136490 | success |                  | vic   | Mildura          |       3500 |               3 |               3 |                   5 |                   3 |
      | Launceston       |               |               | success | Launceston       | tas   | Launceston       |       7250 |               3 |               2 |                   4 |                   5 |
      |             7000 |               |               | success |             7000 | tas   | Glebe            |       7000 |               3 |               3 |                   3 |                   3 |
      |             7000 |               |               | success |             7000 | tas   | Hobart           |       7000 |               3 |               3 |                   3 |                   3 |
      |             7000 |               |               | success |             7000 | tas   | Mount Stuart     |       7000 |               3 |               3 |                   3 |                   3 |
      |             7000 |               |               | success |             7000 | tas   | North Hobart     |       7000 |               5 |               3 |                   3 |                   8 |
      |             7000 |               |               | success |             7000 | tas   | Queens Domain    |       7000 |               3 |               3 |                   3 |                   3 |
      |             7000 |               |               | success |             7000 | tas   | West Hobart      |       7000 |               3 |               9 |                   3 |                   5 |
      |                  | -41.058166434 | 145.888496446 | success |                  | tas   | Park Grove       |       7320 |               3 |               3 |                   5 |                   3 |
      | Barton           |               |               | success | Barton           | act   | Barton           |       2600 |               3 |               3 |                   3 |                   3 |
      |             2604 |               |               | success |             2604 | act   | Kingston         |       2604 |               3 |               3 |                   3 |                   3 |
      |             2604 |               |               | success |             2604 | act   | Causeway         |       2604 |               3 |               3 |                   3 |                   3 |
      |             2604 |               |               | success |             2604 | act   | Narrabundah      |       2604 |               3 |               3 |                   3 |                   3 |
      |                  |      -35.3051 |      149.0981 | success |                  | act   | Yarralumla       |       2600 |               3 |               3 |                   5 |                   3 |
      | Parramatta       |               |               | success | Parramatta       | nsw   | Parramatta       |       2150 |               3 |               3 |                   3 |                   3 |
      |             2148 |               |               | success |             2148 | nsw   | Arndell Park     |       2148 |               3 |               3 |                   3 |                   3 |
      |             2148 |               |               | success |             2148 | nsw   | Blacktown        |       2148 |               3 |               3 |                   3 |                   3 |
      |             2148 |               |               | success |             2148 | nsw   | Huntingwood      |       2148 |               3 |               3 |                   3 |                   3 |
      |             2148 |               |               | success |             2148 | nsw   | King Park        |       2148 |               3 |               3 |                   3 |                   3 |
      |             2148 |               |               | success |             2148 | nsw   | Marayong         |       2148 |               3 |               3 |                   3 |                   3 |
      |             2148 |               |               | success |             2148 | nsw   | Prospect         |       2148 |               3 |               3 |                   3 |                   3 |
      |                  |         -33.8 |     151.18333 | success |                  | nsw   | Chatswood        |       2067 |               3 |               3 |                   5 |                   3 |
      | Berrimah         |               |               | success | Berrimah         | nt    | Berrimah         |       0828 |               3 |               3 |                   3 |                   3 |
      |             0850 |               |               | success |             0850 | nt    | Katherine        |       0850 |               3 |               3 |                   3 |                   3 |
      |             0850 |               |               | success |             0850 | nt    | Cossack          |       0850 |               3 |               3 |                   3 |                   3 |
      |             0850 |               |               | success |             0850 | nt    | Emungalan        |       0850 |               3 |               3 |                   3 |                   3 |
      |             0850 |               |               | success |             0850 | nt    | Katherine East   |       0850 |               3 |               3 |                   3 |                   3 |
      |             0850 |               |               | success |             0850 | nt    | Katherine South  |       0850 |               3 |               3 |                   3 |                   3 |
      |             0850 |               |               | success |             0850 | nt    | Lansdowne        |       0850 |               3 |               3 |                   3 |                   3 |
      |                  |    -12.462827 |    130.841782 | success |                  | nt    | Darwin City      |       0800 |               3 |               3 |                   5 |                   3 |
      | Surfers Paradise |               |               | success | Surfers Paradise | qld   | Surfers Paradise |       4217 |               3 |               3 |                   3 |                   3 |
      |             4000 |               |               | success |             4000 | qld   | Emungalan        |       4000 |               3 |               3 |                   3 |                   3 |
      |             4000 |               |               | success |             4000 | qld   | Katherine East   |       4000 |               3 |               3 |                   3 |                   3 |
      |             4000 |               |               | success |             4000 | qld   | Katherine South  |       4000 |               3 |               3 |                   3 |                   3 |
      |                  |     -27.56056 |     151.95386 | success |                  | qld   | Toowoomba City   |       4350 |               3 |               3 |                   5 |                   3 |
      | Norwood          |               |               | success | Norwood          | sa    | Norwood          |       5067 |               3 |               3 |                   3 |                   3 |
      |             5009 |               |               | success |             5009 | sa    | Allenby Gardens  |       5009 |               3 |               3 |                   3 |                   3 |
      |             5009 |               |               | success |             5009 | sa    | Beverley         |       5009 |               3 |               3 |                   3 |                   3 |
      |             5009 |               |               | success |             5009 | sa    | Kilkenny         |       5009 |               3 |               3 |                   3 |                   3 |
      |                  |     -35.06667 |     138.86667 | success |                  | sa    | Mount Barker     |       5251 |               3 |               3 |                   3 |                   3 |
      | North Perth      |               |               | success | North Perth      | wa    | North Perth      |       6006 |               3 |               3 |                   3 |                   3 |
      |             6054 |               |               | success |             6054 | wa    | Ashfield         |       6054 |               3 |               3 |                   3 |                   3 |
      |             6054 |               |               | success |             6054 | wa    | Bassendean       |       6054 |               3 |               3 |                   3 |                   3 |
      |             6054 |               |               | success |             6054 | wa    | Eden Hill        |       6054 |               3 |               3 |                   3 |                   3 |
      |             6054 |               |               | success |             6054 | wa    | Kiara            |       6054 |               3 |               3 |                   3 |                   3 |
      |                  |      -31.7090 |      115.7240 | success |                  | wa    | Tamala Park      |       6030 |               3 |               3 |                   3 |                   3 |

  @notAutomated
  Scenario Outline: Customer current location details not found
    Given the customer current delivery location '<location>' or co-ordinates '<latitude>', '<longitude>'
    And Google Geocode API returns
      | status   | errorCode   | errorMessage   |
      | <status> | <errorCode> | <errorMessage> |
    When the google map geocode API gets called
    Then result will returns an error: '<code>', '<errorMessage>'

    Examples: 
      | status | errorCode                    | errorMessage                                                            |
      | false  | ERROR_DELIVERY_LOCATION_CODE | Sorry, there was a problem finding delivery location. Please try again. |
      | false  | ERROR_DELIVERY_LOCATION_CODE | Sorry, there was a problem finding delivery location. Please try again. |

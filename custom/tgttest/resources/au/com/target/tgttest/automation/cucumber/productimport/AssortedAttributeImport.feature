Feature: Product Import - import products from STEP with assorted attribute
  As a Target administrative user
  I want to verify that the assorted flag is imported as configured in STEP
  so that I can merchandise assorted products appropriately on target.com.au

  Scenario Outline: Import of products from STEP to Hybris to verify assorted flag
    Given a product with assorted attribute set to '<flagInStep>' in STEP
    When it is imported into Hybris through ESB
    Then the assorted flag is set to '<flagInHybris>' against the product in Hybris

    Examples:
    |flagInStep |flagInHybris |
    |Y          |true         |
    |N          |false        |
    |           |false        |

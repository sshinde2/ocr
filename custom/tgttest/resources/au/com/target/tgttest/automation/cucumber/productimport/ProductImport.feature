Feature: Product import
  Test normal imports
  As Target Online
  I want to do import of products from STEP

  Scenario: product Import and check department,express delivery
    Given the 'productimport.expressbyflag' feature switch is disabled
    And a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | department | availableExpressDelivery |
      |        160 | added                    |

  Scenario: product Import and check express delivery through express flag
    Given the 'productimport.expressbyflag' feature switch is enabled
    And a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | availableExpressDelivery |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Y                        |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | availableExpressDelivery |
      | added                    |

  Scenario: product import and check express not set for bulky products
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | Y     | bulky1      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | availableExpressDelivery |
      | not added                |

  Scenario: product import and check express not set for ineligible department
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | Y     | bulky1      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     | women    |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | department | availableExpressDelivery |
      |        300 | not added                |

  @notAutomated
  Scenario: product import and verify attributes of base product
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | materials                | careInstructions                     | webExtraInfo                      | webFeatures                         | youTube                | license | genders | ageFrom |
      | P11112222   | Test Product | Y     | bulky1      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     | women    | Sample product materials | Sample care instructions for product | Sample web extra info for product | Sample web web features for product | http://youtu.be/ABC123 | Disney  | Boy     |       1 |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | productCode | variantCode | department | availableExpressDelivery | materials                | careInstructions                     | webExtraInfo                      | webFeatures                         | youTube                | license | genders | ageFrom |
      | P11112222   | V987654321  |        300 | not added                | Sample product materials | Sample care instructions for product | Sample web extra info for product | Sample web web features for product | http://youtu.be/ABC123 | Disney  | Boy     |       1 |

  Scenario: physical giftcard product import and verify attributes of base product
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | availableCnc | sizeType | warehouse        | giftcardBrandId | denominaton|
      | P11112223   | Test Product | N     | normal      | V987654324  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     |              | women    | IncommWarehouse  |   itunes        |     20     |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | excludeForAfterpay | availableHomeDelivery | productType |
      |        yes         |  added                |  giftCard   |

   Scenario: import product and verify merchProductStatus
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | merchProductStatus |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Active             |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | department | merchProductStatus |
      |        160 | Active             |

  Scenario: import product with variants and verify merchProductStatus
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | merchProductStatus |
      | W100000     | Test Product | N     | normal      | 10000330    | W93746          |                   | target | Active         | N                 | N                      |        355 | test               | Y                     | babyshoe | Active             |
    And variants:
      | code     | approvalStatus | size     | merchProductStatus |
      | 10000331 | active         | BabySize | Active             |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | department | merchProductStatus |
      |        355 | Active             |
      
  Scenario: import product and verify preorder attributes
    Given a product:
    | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | merchProductStatus | preOrderStartDate   | preOrderEndDate     | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate  |
    | P11112225   | Test Product | N     | normal      | V987654329  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Active             | 2018-02-02 09:30:00 | 2018-03-01 11:30:00 |           20            | 2018-03-03 11:30:00         |
    When run STEP product import process
    Then product import response is successful
    And product has:
      |   preOrderStartDate |    preOrderEndDate  | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate |
      | 2018-02-02 09:30:00 | 2018-03-01 11:30:00 |   20                    |     2018-03-03 11:30:00    | 
  
  Scenario: import preOrder product where EmbargoReleaseDate has format of HH:MM as 00:00
    Given a product:
    | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | merchProductStatus | preOrderStartDate   | preOrderEndDate     | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate  |
    | P11112225   | Test Product | N     | normal      | V987654329  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Active             | 2018-02-02 09:30:00 | 2018-03-01 00:00:00 |           20            | 2018-03-03 00:00:00         |
    When run STEP product import process
    Then product import response is successful
    And product has:
      |   preOrderStartDate |    preOrderEndDate  | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate |
      | 2018-02-02 09:30:00 | 2018-03-01 00:00:00 |   20                    |     2018-03-03 03:00:00    |
      
  Scenario: import preOrder product where EmbargoReleaseDate has format of HH:MM as 00:00:01
    Given a product:
    | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | merchProductStatus | preOrderStartDate   | preOrderEndDate     | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate  |
    | P11112225   | Test Product | N     | normal      | V987654329  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Active             | 2018-02-02 09:30:00 | 2018-03-01 00:00:00 |           20            | 2018-03-03 00:00:01         |
    When run STEP product import process
    Then product import response is successful
    And product has:
     | preOrderEmbargoReleaseDate |preOrderOnlineQuantity|
     |     2018-03-03 03:00:01    |                    20|
      
  Scenario: import preOrder product where EmbargoReleaseDate has non zero HH as 01:00:01
    Given a product:
    | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | merchProductStatus | preOrderStartDate   | preOrderEndDate     | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate  |
    | P11112225   | Test Product | N     | normal      | V987654329  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Active             | 2018-02-02 09:30:00 | 2018-03-01 00:00:00 |           20            | 2018-03-03 01:00:01         |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | preOrderEmbargoReleaseDate |preOrderOnlineQuantity|
      |     2018-03-03 01:00:01    |                    20|
      
  Scenario: import preOrder product where EmbargoReleaseDate has non zero MM as 00:29:01
    Given a product:
    | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | merchProductStatus | preOrderStartDate   | preOrderEndDate     | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate  |
    | P11112225   | Test Product | N     | normal      | V987654329  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Active             | 2018-02-02 09:30:00 | 2018-03-01 00:00:00 |           20            | 2018-03-03 00:29:01         |
    When run STEP product import process
    Then product import response is successful
    And product has:
      | preOrderEmbargoReleaseDate |preOrderOnlineQuantity|
      |     2018-03-03 00:29:01    |                    20|

      
  Scenario: import product and verify preorder attributes without end date
    Given a product:
    | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | merchProductStatus | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate  |
    | P11112225   | Test Product | N     | normal      | V987654329  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | Active             |     20                  | 2018-03-03 11:30:00         |
    When run STEP product import process
    Then product import response is successful
    And product has:
     | preOrderOnlineQuantity  | preOrderEmbargoReleaseDate |
     |                         |     2018-03-03 11:30:00    | 
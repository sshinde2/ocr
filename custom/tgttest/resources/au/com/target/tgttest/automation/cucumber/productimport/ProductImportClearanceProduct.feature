Feature: Import Clearance product from STEP
  In order to ensure clearance option available on products
  As Target Online
  I want to specify in STEP whether a product can be clearance product or not

  Scenario Outline: Import Clearance product as part of STEP import.
    Given a product in STEP with code '<productCode>'
    And clearance flag in STEP is '<clearanceFlag>'
    When run STEP product import process
    Then product import response is successful
    And clearance flag in hybris is '<clearance>'

    Examples: 
      | productCode | clearanceFlag | clearance |
      | 1110001     | Y             | added     |
      | 1110002     | N             | not added |
      | 1110003     | Y             | added     |
      | 1110004     | N             | not added |

  Scenario: Import a new product in STEP
    Given a new colour variant product in STEP
    And clearance flag in STEP is 'N'
    When run STEP product import process
    Then product import response is successful
    And clearance flag in hybris is 'not added'

   Scenario: Import a new product in STEP
    Given a new colour variant product in STEP
    And clearance flag in STEP is 'Y'
    When run STEP product import process
    Then product import response is successful
    And clearance flag in hybris is 'added'
    
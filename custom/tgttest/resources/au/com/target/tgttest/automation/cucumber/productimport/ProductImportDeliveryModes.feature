Feature: Product import - delivery modes validation.
  Test normal imports
  As Target Online
  I want to do import of products from STEP and validate the delivery modes
  
  Scenario: product import and check only home delivery set
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | availableCnc | sizeType |
      | P11112222   | Test Product | Y     | bulky1      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     |  N           |women    |
    When run STEP product import process
    Then product import response is successful
    And imported product delivery modes are 'home-delivery'

  Scenario: product import and ebay express delivery
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | availableCnc | sizeType | availableEbayExpressDelivery | availableOnEbay |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | N                     | N            | women    | Y                            | Y               |
    When run STEP product import process
    Then product import response is successful
    And imported product delivery modes are 'eBay-delivery,ebay-express-delivery'

  Scenario: product import and check ebay home delivery
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |  availableCnc | sizeType | availableEbayExpressDelivery | availableOnEbay |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | N                     |  N            | women    | N                            | Y               |
    When run STEP product import process
    Then product import response is successful
    And imported product delivery modes are 'eBay-delivery'

  Scenario: product import and check home delivery only when not available on ebay
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |  availableCnc | sizeType | availableEbayExpressDelivery | availableOnEbay |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     |  N            | women    | Y                            | N               |
    When run STEP product import process
    Then product import response is successful
    And imported product delivery modes are 'home-delivery'

  Scenario: product import and check home delivery and cnc only when not available on ebay
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | availableCnc | sizeType | availableEbayExpressDelivery | availableOnEbay |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     | Y            | women    | Y                            | N               |
    When run STEP product import process
    Then product import response is successful
    And imported product delivery modes are 'home-delivery,click-and-collect'
    
  Scenario: product import and check home delivery and cnc only when not available on ebay
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | availableCnc | sizeType | availableEbayExpressDelivery | availableOnEbay |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     | Y            | women    | N                            | N               |
    When run STEP product import process
    Then product import response is successful
    And imported product delivery modes are 'home-delivery,click-and-collect'
    
  Scenario: physical gift card product import and check only home delivery available
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | availableCnc | sizeType | availableEbayExpressDelivery | availableOnEbay | warehouse        | giftcardBrandId | denominaton|
      | P11112223   | Test Product | N     | normal      | V987654324  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     |              | women    | N                            | N               | IncommWarehouse  |   itunes        |     20     |
    When run STEP product import process
    Then product import response is successful
    And imported product delivery modes are 'home-delivery'

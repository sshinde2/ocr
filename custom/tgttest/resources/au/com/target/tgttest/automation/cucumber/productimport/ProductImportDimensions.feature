Feature: Product Import - imports products from STEP with dimension data. Verify also the stocklevel creation for products except giftcards
  
  Dimension data format below is (height, width, length, weight)

  Scenario: Import a new colour variant product with no dimension data
    Given a new colour variant product in STEP
    When run STEP product import process
    Then product import response is successful
    And fastline stock object is created
    And imported product delivery modes are 'home-delivery,click-and-collect'

  Scenario: Import a new colour variant product with all dimension data
    Given a new colour variant product in STEP
    And dimension data (10, 11, 12, 13)
    When run STEP product import process
    Then product import response is successful
    And product dimensions are updated
    And fastline stock object is created

  Scenario: Import a new colour variant product with partial dimension data weight missing
    Given a new colour variant product in STEP
    And dimension data (10, 11, 12, )
    When run STEP product import process
    Then product import response is not successful

  Scenario: Update an existing colour variant product with no dimension data
    Given an existing colour variant product in STEP
    When run STEP product import process
    Then product import response is successful

  Scenario: Update an existing colour variant product with all dimension data
    Given an existing colour variant product in STEP
    And dimension data (10, 11, 12, 13)
    When run STEP product import process
    Then product import response is successful
    And product dimensions are updated

  Scenario: Update an existing colour variant product with partial dimension data weight missing
    Given an existing colour variant product in STEP
    And dimension data (10, 11, 12, )
    When run STEP product import process
    Then product import response is not successful

  Scenario: Import a new colour variant product with size variant and no dimension data
    Given a new colour variant with size variant in STEP
    When run STEP product import process
    Then product import response is successful

  Scenario: Import a new colour variant with size variant with all dimension data
    Given a new colour variant with size variant in STEP
    And dimension data (10, 11, 12, 13)
    When run STEP product import process
    Then product import response is successful
    And product dimensions are updated

  Scenario: Import a new colour variant with size variant with partial dimension data weight missing
    Given a new colour variant with size variant in STEP
    And dimension data (10, 11, 12, )
    When run STEP product import process
    Then product import response is not successful
    And no fastline stock object is created

  Scenario: Import a new size variant product with no dimension data
    Given a new size variant product in STEP
    When run STEP product import process
    Then product import response is successful
    And fastline stock object is created

  Scenario: Import a new size variant product with all dimension data
    Given a new size variant product in STEP
    And dimension data (10, 11, 12, 13)
    When run STEP product import process
    Then product import response is successful
    And product dimensions are updated
    And fastline stock object is created

  Scenario: Import a new size variant product with partial dimension data weight missing
    Given a new size variant product in STEP
    And dimension data (10, 11, 12, )
    When run STEP product import process
    Then product import response is not successful
    And no fastline stock object is created

  Scenario: Import a new giftcard product with existing gift card brand
    Given a new gift card product in STEP with gift card data:
      | BrandID       | ProductID | Denomination | GiftCardStyle     |
      | venue1golfaus | PRD01     | 50           | CIR_000153_04.png |
    And giftcard record exists for 'venue1golfaus'
    When run STEP product import process
    Then product import response is successful
    And imported product type is 'Digital'
    And imported giftcard denomination value in size variant level is '50.0'
    And imported giftcard style value in colour variant level is 'CIR_000153_04.png'
    And imported giftcard productID value in colour variant level is 'PRD01'
    And imported giftcard brandID value in base product is 'venue1golfaus'
    And incomm stock object is created with available 100
    And no fastline stock object is created
    And imported product delivery modes are 'digital-gift-card'

  Scenario: Import a new giftcard product with new gift card brand
    Given a new gift card product in STEP with gift card data:
      | BrandID       | ProductID | Denomination | GiftCardStyle     |
      | venue1golfaus | PRD02     | 50           | CIR_000153_04.png |
    And giftcard record does not exist for 'venue1golfaus'
    When run STEP product import process
    Then product import response is successful
    And imported product type is 'Digital'
    And imported giftcard denomination value in size variant level is '50.0'
    And imported giftcard style value in colour variant level is 'CIR_000153_04.png'
    And imported giftcard productID value in colour variant level is 'PRD02'
    And imported giftcard brandID value in base product is 'venue1golfaus'
    And incomm stock object is created with available 100
    And no fastline stock object is created
    And imported product delivery modes are 'digital-gift-card'
    And a new giftcard is created for 'venue1golfaus' with:
      | BrandID       | Brand         | MaxOrderQuantity | MaxOrderValue |
      | venue1golfaus | venue1golfaus | 10               | 500           |

  Scenario: Import a new giftcard product with missing brandId
    Given a new gift card product in STEP with gift card data:
      | BrandID | ProductID | Denomination | GiftCardStyle     |
      |         | PRD01     | 50           | CIR_000153_04.png |
    When run STEP product import process
    Then product import response is not successful

  Scenario: Import a new giftcard product with missing denomination
    Given a new gift card product in STEP with gift card data:
      | BrandID       | ProductID | Denomination | GiftCardStyle     |
      | venue1golfaus | PRD01     |              | CIR_000153_04.png |
    When run STEP product import process
    Then product import response is not successful

  Scenario Outline: Import ebay delivery modes for products
    Given a product in STEP with code '<productCode>'
    And available for cnc flag in STEP is '<clickAndCollectFlag>'
    And available on ebay flag in STEP is '<availableOnEbay>'
    When run STEP product import process
    Then product import response is successful
    And imported product delivery modes are '<expectedModes>'

    Examples: 
      | productCode | availableOnEbay | clickAndCollectFlag | expectedModes                                                        |
      | 1110001     | Y               | Y                   | home-delivery,click-and-collect,eBay-delivery,ebay-click-and-collect |
      | 1110002     | Y               | N                   | home-delivery,eBay-delivery                                          |
      | 1110003     | N               | Y                   | home-delivery,click-and-collect                                      |
      | 1110004     | N               | N                   | home-delivery                                                        |

@stepImportData
Feature: Import Express Delivery option from STEP
  In order to ensure Express option available on apparel and non apparel products
  As Target Online
  I want to specify in STEP whether a product can be delivered via the Express method or not
  
  Notes:
  * Feature-switch(productimport.expressbyflag) to start using express-delivery flag from STEP has been turned-on via impex.

  Scenario Outline: Import Express Delivery option as part of STEP import.
    Given a product in STEP with code '<productCode>'
    And product type in STEP is '<productType>'
    And express delivery flag in STEP is '<expressDeliveryFlag>'
    When run STEP product import process
    Then product import response is '<importStatus>'
    And error message after import is "<errorMessage>"
    And express delivery mode in hybris is '<expressDeliveryMode>'

    Examples: 
      | productCode | productType | expressDeliveryFlag | importStatus   | errorMessage                                                                                      | expressDeliveryMode |
      | 1110001     | normal      | Y                   | successful     | N/A                                                                                               | added               |
      | 1110002     | normal      | N                   | successful     | N/A                                                                                               | not added           |
      | 1110003     | normal      | Blank               | successful     | N/A                                                                                               | not added           |
      | 1110004     | bulky1      | Y                   | not successful | It is a bulky product with express-delivery enabled, aborting product/variant import for: 1110004 | N/A                 |
      | 1110005     | bulky1      | N                   | successful     | N/A                                                                                               | not added           |
      | 1110006     | bulky1      | Blank               | successful     | N/A                                                                                               | not added           |
      | 1110007     | bulky2      | Y                   | not successful | It is a bulky product with express-delivery enabled, aborting product/variant import for: 1110007 | N/A                 |
      | 1110008     | bulky2      | N                   | successful     | N/A                                                                                               | not added           |
      | 1110009     | bulky2      | Blank               | successful     | N/A                                                                                               | not added           |
      | 1110010     | mhd         | Y                   | not successful | It is a bulky product with express-delivery enabled, aborting product/variant import for: 1110010 | N/A                 |
      | 1110011     | mhd         | N                   | successful     | N/A                                                                                               | not added           |
      | 1110012     | mhd         | Blank               | successful     | N/A                                                                                               | not added           |

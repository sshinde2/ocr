Feature: Product import original category - validation.
  Test normal imports
  As Target Online
  I want to do import of products from STEP

  Scenario: product Import and check original category
    Given a product contains original category:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | originalCategory |
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        160 | test               | Y                     | women    | W123             |
    When run STEP product import process
    Then product import response is successful
    And product has originalCatogry:
      | department | originalCategory |
      |        160 | W123             |

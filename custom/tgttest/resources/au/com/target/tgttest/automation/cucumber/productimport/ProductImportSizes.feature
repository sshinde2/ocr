@notAutomated
Feature: Product Size from ODBMS to Hybris

  Scenario Outline: Size2 should be imported to step
    Given a product in ODBMS with size values:
      | <size1> | <size2> |
    And the override_size for the product is empty in step
    When the product is imported to step
    Then the transformed_size is <transformed_size>
    And the "size,display" is <hybris_size>

    Examples: 
      | size1 | size2 | transformed_size | hybris_size |
      | 82    | A     | 82               | 82A         |
      | 08    | C     | 8                | 8C          |
      | 16+   | A     | 16               | 16A         |
      | 082   | B     | 082              | 082B        |
      | 82    | N/A   | 82               | 82          |
      | N/A   | 82    | N/A              | 82          |
      | N/A   | N/A   | N/A              | N/A         |

  Scenario Outline: Size 2 should be passed to hybris
    Given a product in step with size values:
      | <size1> | <size2> | <override_size> | <transformed_size> |
    When the product is exported to hybris
    Then size in hybris is <hybris_size>

    Examples: 
      | size1 | size2 | override_size | transformed_size | hybris_size |
      | 82    | A     | N/A           | 82               | 82A         |
      | 08    | C     | N/A           | 8                | 8C          |
      | 16+   | A     | N/A           | 16               | 16A         |
      | 082   | B     | 82A           | 082              | 82A         |
      | 82    | N/A   | N/A           | 82               | 82          |
      | N/A   | 82    | N/A           | N/A              | 82          |
      | N/A   | N/A   | N/A           | N/A              | N/A         |

Feature: Product import - validation.
  Test all validation
  As Target Online
  I want to do validation on products imported from STEP

  Scenario: Product Import validation with no product code
    Given a product:
      | productCode | productName  | primaryCategory |
      |             | Test product |                 |
    When run STEP product import process
    Then error message after import is "One of required values is missing [Base Product Code, Variation Code, Product Name]"

  Scenario: Product Import validation with no product name
    Given a product:
      | productCode | productName | primaryCategory |
      | P11112222   |             |                 |
    When run STEP product import process
    Then error message after import is "One of required values is missing [Base Product Code, Variation Code, Product Name]"

  Scenario: product Import validation when primary category is null
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |
      | P11112222   | Test Product | V987654321  |                 | target | Active         | N                 | N                      | 160        | test               | Y                     |
    When run STEP product import process
    Then error message after import is "Missing PRIMARY CATEGORY (web storefront classification) for product, aborting product/variant import for: V987654321"

  Scenario: product Import validation when primary category is nonExistant in hybris
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |
      | P11112222   | Test Product | V987654321  | XYZ123          | target | Active         | N                 | N                      | 160        | test               | Y                     |
    When run STEP product import process
    Then error message after import is "Primary category XYZ123 does not exist in Hybris, aborting product/variant import for: V987654321"

  Scenario: product Import validation bulky product marked as normal productType
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | Y     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "Bulky product with incorrect product type, aborting product/variant import for: V987654321"

  Scenario: product Import validation non bulky product marked as bulky productType
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | N     | bulky1      | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "Bulky product type set for a bulky flag that is not Y, aborting product/variant import for: V987654321"

  Scenario: product Import validation bulky productType with empty bulky flag
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product |       | bulky1      | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "Bulky product type set for a bulky flag that is not Y, aborting product/variant import for: V987654321"

  Scenario: product Import validation with unknown productType
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | N     | abcde       | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "ProductType specified is not defined in hybris, aborting product/variant import for: V987654321"

  Scenario: product Import validation with empty productType
    Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | Y     |             | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "Bulky product with incorrect product type, aborting product/variant import for: V987654321"

  Scenario: product Import validation express delivery set for bulky products
    Given a product:
      | productCode | productName  | bulky | productType | availableExpressDelivery | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | Y     | bulky1      | Y                        | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "It is a bulky product with express-delivery enabled, aborting product/variant import for: V987654321"

  Scenario: product Import validation with empty available for layby flag
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         |                   | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "Missing AVAILABLE FOR LAYBY flag (138956 - Toysale 7), aborting product/variant import for: V987654321"

  Scenario: product Import validation with empty long term layby flag
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 |                        | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "Missing AVAILABLE FOR LONGTERM LAYBY flag (Attr_311962), aborting product/variant import for: V987654321"

  Scenario: product Import validation when secondary category is nonExistant in hybris
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |
      | P11112222   | Test Product | V987654321  | W93746          | XY123             | target | Active         | N                 | N                      | 160        | test               | Y                     |
    When run STEP product import process
    Then error message after import is "Secondary category [XY123] does not exist in Hybris, aborting product/variant import for: V987654321"

  Scenario: product Import validation when brand is missing
    Given a product:
      | productCode | productName  | brand | variantCode | primaryCategory | secondaryCategory | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |
      | P11112222   | Test Product |       | V987654321  | W93746          |                   | Active         | N                 | N                      | 160        | test               | Y                     |
    When run STEP product import process
    Then error message after import is "Missing product/variant BRAND (49135), aborting product/variant import for: V987654321"

  Scenario: product Import validation when department is missing
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 | N                      |            | test               | Y                     |
    When run STEP product import process
    Then error message after import is "Missing DEPARTMENT NUMBER (Attr_308459), aborting product/variant import for: V987654321"

  Scenario: product Import validation with Invalid Department
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 777        | test               | Y                     |
    When run STEP product import process
    Then error message after import is "Department 777 does not exists in hybris, aborting product/variant import for V987654321"

  Scenario: product Import validation with missing product Description
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        |                    | Y                     |
    When run STEP product import process
    Then error message after import is "Missing PRODUCT DESCRIPTION (49142), aborting product/variant import for: V987654321"

  Scenario: product Import validation with no approval Status
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target |                | N                 | N                      | 160        | test               | Y                     |
    When run STEP product import process
    Then error message after import is "Missing product/variant APPROVAL STATUS (94267), aborting product/variant import for: V987654321"

  Scenario: product Import validation with no approval Status on variants
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    And variants:
      | code     | approvalStatus | size |
      | 53123123 |                | 10   |
    When run STEP product import process
    Then error message after import is "Missing variant APPROVAL STATUS (94267), aborting product/variant import for: 53123123"

  Scenario: product Import validation with empty HD flag
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               |                       | women    |
    When run STEP product import process
    Then error message after import is "Missing AVAILABLE FOR HOME DELIVERY flag (279381), aborting product/variant import for: V987654321"

  Scenario: product Import validation with empty size type for a size only products
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | sizeOnly | size |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     |          | true     | 10   |
    And variants:
      | code     | approvalStatus | size |
      | 53123123 | Active         | 10   |
    When run STEP product import process
    Then error message after import is "Missing SIZE TYPE definition (49129), aborting product/variant import for: V987654321"

  Scenario: product Import validation with empty size value for a size only products
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType | sizeOnly | size |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    | true     |      |
    When run STEP product import process
    Then error message after import is "Missing SIZE value (49128), aborting product/variant import for: V987654321"

  Scenario: product Import validation with size variant that has no size
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeOnly | sizeType |
      | P11112222   | Test Product | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | false    | women    |
    And variants:
      | code     | approvalStatus | size |
      | 53123123 | Active         |      |
    When run STEP product import process
    Then error message after import is "Missing SIZE value (49128) for size variant, aborting product/variant import for: 53123123"

  Scenario: product Import validation for a not sourced product
    Given a product:
      | productCode | productName  | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P64654566   | Test Product | V424343434  | W93746          |                   | target | NOT SOURCED    | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "'NOT SOURCED' product/variant status found, aborting product/variant import for: V424343434"

  Scenario: product Import validation offline date before online date
    Given a product:
      | productCode | productName  | onlineDate          | offlineDate         | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | 2017-01-10 08:00:00 | 2017-01-01 08:00:00 | V987654321  | W93746          |                   | target | Not Sourced    | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "Product offline date is before online date, aborting product/variant import for: V987654321"

  Scenario: product Import validation offline date and online date invalid
    Given a product:
      | productCode | productName  | onlineDate | offlineDate         | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | invalid    | 2017-01-10 08:00:00 | V987654321  | W93746          |                   | target | Not Sourced    | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "Product online date has incorrect format, aborting product/variant import for: V987654321"

  Scenario Outline: removing leading zeroes from the product code
    Given a base product code '<baseProductCode>' and the colour variant code '<colourVariantCode>'
    And variant code '<variantCode>'
    When run STEP product import process
    Then the base product code is '<actualBaseproductCode>' and colour variant code is '<actualColourVariantCode>'
    And the variant code is '<actualVariantCode>'

    Examples: 
      | baseProductCode | colourVariantCode | variantCode | actualBaseproductCode | actualColourVariantCode | actualVariantCode |
      | 00011223300     | 0099887700        | 0033445500  | 11223300              | 99887700                | 33445500          |
      | 00011223300     | 0099887700        | 0066778800  | 11223300              | 99887700                | 66778800          |

  Scenario: product import validation with media missing
    Given a product:
      | productCode | productName  | primaryImage | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | IMG999       | V987654321  | W93746          |                   | target | Not Sourced    | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error messages after import are:
      | ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /hero/IMG999 not found in hybris      |
      | ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /thumb/IMG999 not found in hybris     |
      | ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /large/IMG999 not found in hybris     |
      | ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /list/IMG999 not found in hybris      |
      | ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /grid/IMG999 not found in hybris      |
      | ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier /full/IMG999 not found in hybris      |
      | ERR-PRODUCTMEDIA-NOMEDIACONTAINER: No media container was found in hybris with the qualifier IMG999 |

  Scenario: product import validation when no media
    Given a product:
      | productCode | productName  | primaryImage | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | NA           | V987654321  | W93746          |                   | target | Not Sourced    | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error message after import is "ERR-PRODUCTIMPORT-MEDIAMISSING : There was no media found on hybris for product with ID V987654321"

  Scenario: product Import validation with no show when out of stock flag
    Given a product:
      | productCode | productName  | showWhenOutOfStock | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | NA                 | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then product import response is successful
   
   Scenario: physical giftcard product Import validation with no brand id for giftcard 
     Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | availableCnc | sizeType | warehouse        | giftcardBrandId | denominaton|
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     |              | women    | IncommWarehouse  |                 |     20     |
     When run STEP product import process
     Then error message after import is "Missing Brand ID for gift card product, aborting product/variant import for: V987654321"
     
  Scenario: physical giftcard product Import validation with denomination for giftcard 
     Given a product:
      | productCode | productName  | bulky | productType | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | availableCnc | sizeType | warehouse        | giftcardBrandId | denominaton|
      | P11112222   | Test Product | N     | normal      | V987654321  | W93746          |                   | target | Active         | N                 | N                      |        300 | test               | Y                     |              | women    | IncommWarehouse  |   itunes        |            |
     When run STEP product import process
     Then error message after import is "Missing Denomination for gift card product, aborting product / variant import for: V987654321"
  

  Scenario Outline: EAN validation during product import from STEP.
    Given a sellable variant product in STEP with code 'P11112222' and with name 'Test Product'
    And EAN of product is given as '<EAN>'
    When run STEP product import process
    Then EAN of product imported is '<updatedEAN>'
    And error message after import is "<message>"

    Examples: 
      | EAN            | response | updatedEAN    | message                                                                                                                             |
      | 357357         | Valid    | 00357357      | N/A                                                                                                                                 |
      | 9475915        | Valid    | 09475915      | N/A                                                                                                                                 |
      | 772059817      | Valid    | 0000772059817 | N/A                                                                                                                                 |
      | 4405976120     | Valid    | 0004405976120 | N/A                                                                                                                                 |
      | 65541802292    | Valid    | 0065541802292 | N/A                                                                                                                                 |
      | 817980010536   | Valid    | 817980010536  | N/A                                                                                                                                 |
      | 9300633499815  | Valid    | 9300633499815 | N/A                                                                                                                                 |
      | 93006334998159 | Invalid  | Empty         | EAN validation failed for EAN: 93006334998159 for product with code: P11112222, name: Test Product hence EAN has been set to empty. |
      | 9300633499816  | Invalid  | Empty         | EAN validation failed for EAN: 9300633499816 for product with code: P11112222, name: Test Product hence EAN has been set to empty.  |
      | 817980010533   | Invalid  | Empty         | EAN validation failed for EAN: 817980010533 for product with code: P11112222, name: Test Product hence EAN has been set to empty.   |
      | 357 357        | Invalid  | Empty         | EAN validation failed for EAN: 357 357 for product with code: P11112222, name: Test Product hence EAN has been set to empty.        |
      | 357EA357       | Invalid  | Empty         | EAN validation failed for EAN: 357EA357 for product with code: P11112222, name: Test Product hence EAN has been set to empty.       |
      | 357E$357       | Invalid  | Empty         | EAN validation failed for EAN: 357E$357 for product with code: P11112222, name: Test Product hence EAN has been set to empty.       |
      | 357357         | Valid    | 00357357      | N/A                                                                                                                                 |
      | Empty          | Valid    | Empty         | N/A                                                                                                                                 |
      
      
  Scenario: product Import validation for pre order product
    Given a product:
      | productCode | productName  | showWhenOutOfStock | preOrderStartDate   | preOrderEndDate     | preOrderEmbargoReleaseDate | preOrderOnlineQuantity | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | NA                 | 2017/03/28 12:23:56 | 2017/01/28 12:23:56 | 2017/05/28 12:23:56        |       -5               | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error messages after import are:
      | Product pre order start date has incorrect format, aborting product/variant import for: V987654321              |
      | Product pre order end date has incorrect format, aborting product/variant import for: V987654321                |
      | Product pre order embargo release date has incorrect format, aborting product/variant import for: V987654321    |
      | Product pre order online quantity cannot be less than zero, aborting product/variant import for: V987654321     |
      
  Scenario: product Import validation for pre order product when Pre order end date is before pre order start date
    Given a product:
      | productCode | productName  | showWhenOutOfStock | preOrderStartDate   | preOrderEndDate     | preOrderEmbargoReleaseDate | preOrderOnlineQuantity | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | NA                 | 2018-03-01 12:00:00 | 2018-02-28 11:01:01 |                            |       8                | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error messages after import are:
      | Product pre order end date is before pre order start date, aborting product/variant import for: V987654321 |
      
  Scenario: product Import validation for pre order product when difference between preOrderEndDate and EmbargoReleaseDate is less than 48 hours
    Given a product:
      | productCode | productName  | showWhenOutOfStock | preOrderStartDate   | preOrderEndDate     | preOrderEmbargoReleaseDate | preOrderOnlineQuantity | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | NA                 |                     | 2018-03-01 11:01:01 | 2018-03-02 12:00:00        |       8                | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then error messages after import are:
      | Pre-Order End Date/Time has to finish >48 hours before ODBMS Embargo Release Date/Time, aborting product/variant import for: V987654321   |
      
  Scenario: product Import validation for pre order product when difference between preOrderEndDate and EmbargoReleaseDate is more than 48 hours
    Given a product:
      | productCode | productName  | showWhenOutOfStock | preOrderStartDate   | preOrderEndDate     | preOrderEmbargoReleaseDate | preOrderOnlineQuantity | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | NA                 |                     | 2018-03-01 11:01:01 | 2018-03-04 12:00:00        |       8                | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then product import response is successful
    
  Scenario: product Import validation for pre order product when difference between preOrderEndDate and EmbargoReleaseDate is exactly 48 hours
    Given a product:
      | productCode | productName  | showWhenOutOfStock | preOrderStartDate   | preOrderEndDate     | preOrderEmbargoReleaseDate | preOrderOnlineQuantity | variantCode | primaryCategory | secondaryCategory | brand  | approvalStatus | availableForLayby | availableLongTermLayby | department | productDescription | availableHomeDelivery | sizeType |
      | P11112222   | Test Product | NA                 |                     | 2018-03-01 11:01:01 | 2018-03-03 11:01:01        |       8                | V987654321  | W93746          |                   | target | Active         | N                 | N                      | 160        | test               | Y                     | women    |
    When run STEP product import process
    Then product import response is successful

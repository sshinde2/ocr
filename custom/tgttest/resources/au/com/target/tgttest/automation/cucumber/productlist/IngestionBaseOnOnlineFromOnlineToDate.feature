@notAutomated
 
Feature: Consider Online From Date and Online To Date in Endeca ingestion
In order to ingest products in the Endeca
As Target Online
I want to consider Online To and Online From date

Scenario Outline:Ingesting product in Endeca based on Online From and Online To Date

Given product <productId> is eligible for Endeca ingestion
And the Online From Date of the product is <onlineFromDate>
And the Online To Date of the product is <onlineToDate>
When Baseline is run
Then the product ingested in Endeca is <ingested>
And reason is <reason>

Examples:
| productId | onlineFromDate | onlineToDate | ingested | reason            |
| SKU101    | past           | future       | yes      | within date range |
| SKU102    | past           | blank        | yes      | within date range |
| SKU103    | future         | future       | no       | not in date range |
| SKU104    | future         | blank        | no       | not in date range |
| SKU105    | past           | past         | no       | not in date range |
| SKU106    | blank          | blank        | yes      | within date range |
| SKU107    | blank          | future       | yes      | within date range |
| SKU108    | blank          | past         | no       | not in date range |


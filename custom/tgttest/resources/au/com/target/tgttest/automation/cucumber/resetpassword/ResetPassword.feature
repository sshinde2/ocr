@notAutomated
Feature: Reset Password in SPC

  Scenario Outline: Forgotten password screen from Reset Password link
    Given the customer is on the Checkout Page
    And feature switch "spa.reset.password" is set to <featureStatus>
    And the customer is <userType>
    When the customer clicks on the "Reset password"
    Then the <screen> page is rendered
    And An email is <sentStatus> to the customers registered email address

    Examples: 
      | featureStatus | userType       | screen                | sentStatus |
      | enabled       | registered     | spc reset password    | sent       |
      | enabled       | soft-signed in | spc reset password    | sent       |
      | disabled      | registered     | legacy reset password | not sent   |
      | disabled      | soft-signed in | legacy reset password | not sent   |

  Scenario Outline: Set the new password via the correct screen
    Given the customer has reset their password
    And feature switch "spa.reset.password" is set to <featureStatus>
    When the customer clicks the link in their email
    And the generated token is "abc++"
    Then the <landingPage> is rendered

    Examples: 
      | featureStatus | landingPage                                     |
      | enabled       | /spc/set-password?token=abc%2B@2B               |
      | disabled      | /login/forgotten-password/reset?token=abc%2B@2B |

  Scenario Outline: Setting the new password will take you to where you belong
    Given the customer has reset their password
    And the previous page was <previousPage>
    When the customer sets their new password successfully
    And the cart is <cartStatus>
    Then the customer will hit the set-password endpoint
    And will land on <landingPage>

    Examples: 
      | previousPage | cartStatus | landingPage |
      | checkout     | populated  | checkout    |
      | checkout     | empty      | homepage    |
      | favourites   | N/A        | favourites  |
      | PDP          | N/A        | my-account  |
      | homepage     | N/A        | my-account  |

  Scenario Outline: Returning to favourites will merge your favourites
    Given the customer has items <itemList1> in their favourites
    And their respective account has <itemList2> saved in its favourites
    When the customer set their new password successfully
    Then the new favourites list will be <itemList3>

    Examples: 
      | itemList1    | itemList2    | itemList3                  |
      | P1000, P1001 | P1003, P1004 | P1000, P1001, P1003, P1004 |
      | P1000, P1001 |              | P1000, P1001               |
      |              | P1003, P1004 | P1003, P1004               |
      | P1000, P1001 | P1000        | P1000, P1001               |
      | P1000        | P1000, P1001 | P1000, P1001               |

  Scenario Outline: Resetting password after token has expired
    Given The customer has reset their password
    And their previous page was <previousPage>
    And their reset token has expired
    And they are taken to the reset password page
    And they request a new password
    When they set their new password successfully
    And their cart is <cartStatus>
    Then they will land on <landingPage>

    Examples: 
      | previousPage | cartStatus | landingPage |
      | checkout     | populated  | checkout    |
      | checkout     | empty      | homepage    |
      | favourites   | N/A        | favourites  |
      | PDP          | N/A        | my-account  |
      | homepage     | N/A        | my-account  |

  Scenario Outline: Customer is notified when their password does not meet criteria
    Given The customer has reset their password
    And they are on the new reset page
    When they enter password <password>
    Then they get the message <message>
    And their password reset is <success>

    Examples: 
      | password                          | message                                              | success |
      | abc~defg                          | Please enter a password without ~ or spaces.         | false   |
      | abc defg                          | Please enter a password without ~ or spaces.         | false   |
      | ab                                | Please enter a password between 6 and 32 characters. | false   |
      | password                          | The supplied password is insecure.                   | false   |
      | abcdefghijklmnopqrstuvwxyz1234567 | Please enter a password between 6 and 32 characters. | false   |
      | SaFe!VaLuE$141                    |                                                      | true    |

  Scenario: Consumed token will give the customer an error
    Given The customer has requested to reset their password
    And they have successfully reset their password
    And they click the link generated in the email again
    When they attempt to reset their password again
    Then they are taken to the page to reset password again
    And they receive an error message
@cartProductData @deliveryModeData @clearSessionCartData @postCodeGroupData @shipsterConfigData @voucherData @preOrderDeliveryFee
Feature: Calculate Delivery fee value for shipster member
  
  In order to calculate delivery fee
  As a customer I want to get free delivery fee as a shipster member
    
    Default shipster configuration:
    | deliveryMode    | active | maxShippingThresholdValue | minOrderTotalValue |
    | home-delivery   | true   | 20                        | 25                 |
    | express-delivery| false  | 20                        | 25                 |
    
    Default delivery fee
    | deliveryMode     | fee   |
    | home-delivery    | 9     |
    | express-delivery | 12.95 |
    
    Default PreOrder delivery fee
    | deliveryMode     | fee   |
    | express-delivery | 4.0   |
    
    Flybuys redeem code 'DUMMYREDEEMCODE2'values 20 dollar
    Voucher code 'VOUCHER-TV1' values 10 dollar

  Background: 
    Given Postcodes allocated for the catchment area and also delivery fee set up based on product type and quantity.
      | postCode | catchmentArea | productType | qty       | deliveryFee | lowestDeliveryFee |
      |     3002 | auto-Metro    | bulky1      | 1-2       | $15         |                   |
      |     3003 | auto-Metro    | bulky1      | 3 or more | $30         |                   |
      |     3222 | auto-Regional | bulky1      | 1-2       | $25         |                   |
      |     3223 | auto-Regional | bulky1      | 3 or more | $50         |                   |
      |     4875 | auto-Rural    | bulky1      | 1-2       | $45         |                   |
      |     4876 | auto-Rural    | bulky1      | 3 or more | $80         |                   |
      | n/a      | n/a           | normal      | 1 or more | $9          |                   |
      | n/a      | n/a           | bulky1      | 1-2       | $59         | $15               |
      | n/a      | n/a           | bulky1      | 3 or more | $79         | $30               |
    And flybuys config max redeemable amount is 50
    And customer has valid flybuys number presented
    And the user entered flybuys details are 'valid'
    And Customer has available points '11000'
    And Shipster is enabled for 'home-delivery'
    And Shipster is disabled for 'express-delivery'

  Scenario Outline: Calculate the delivery fee for shipster
    Given a registered customer goes into the checkout process
    And a cart with entries:
      | product       | qty   | price   |
      | <productCode> | <qty> | <price> |
    And the postcode allocated for the cart is '<postCode>'
    When shipster membership status is '<shipstermemberStatus>'
    And delivery fee displayed for the '<deliveryMode1>' is '<deliveryFee1>'
    And delivery fee displayed for the '<deliveryMode2>' is '<deliveryFee2>'

    Examples: 
      | productCode | qty | price | postCode | shipstermemberStatus | deliveryMode1 | deliveryFee1 | deliveryMode2    | deliveryFee2 | comment                                         |
      |   100010901 |   2 |    15 |     3001 | true                 | home-delivery |            0 | express-delivery |        12.95 | home delivery eligible for shipster             |
      |   100010901 |   2 |    15 |     3001 | false                | home-delivery |            9 | express-delivery |        12.95 | User is not a shipster member                   |
      |   100010901 |   1 |    15 |     3001 | true                 | home-delivery |            9 | express-delivery |        12.95 | Order total under 25                            |
      |    10000411 |   3 |    15 |     3003 | true                 | home-delivery |           30 | Not available    | N/A          | bulky item delivery cost is over threshold      |
      |    10000411 |   1 |    15 |     3002 | true                 | home-delivery |           15 | Not available    | N/A          | bulky item order total under 25                 |
      |    10000411 |   1 |    30 |     3002 | true                 | home-delivery |            0 | Not available    | N/A          | bulky item available for shipster free delivery |

  Scenario: Recalculate the delivery fee for shipster when user selects the delivery mode and changes the delivery mode
    Given a registered customer goes into the checkout process
    And a cart with entries:
      | product   | qty | price |
      | 100010901 |   2 |    15 |
    And the customer email address is 'registered' for AusPost Shipster
    And verify email api is invoked successfully
    And the postcode allocated for the cart is '3001'
    And delivery fee displayed for the 'home-delivery' is '0'
    And delivery fee displayed for the 'express-delivery' is '12.95'
    When selected delivery mode in spc is 'home-delivery'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  30.0 |     30.0 |            0.0 |          0.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | true                            | true              |
    When selected delivery mode in spc is changed to 'express-delivery'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      | 42.95 |     30.0 |            0.0 |        12.95 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                12.95 | false                           | true              |

  Scenario: Recalculate the delivery fee for shipster when user changes the email address
    Given a registered customer goes into the checkout process
    And a cart with entries:
      | product   | qty | price |
      | 100010901 |   2 |    15 |
    And the customer email address is 'registered' for AusPost Shipster
    And verify email api is invoked successfully
    And the postcode allocated for the cart is '3001'
    And delivery fee displayed for the 'home-delivery' is '0'
    And delivery fee displayed for the 'express-delivery' is '12.95'
    When selected delivery mode in spc is 'home-delivery'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  30.0 |     30.0 |            0.0 |          0.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | true                            | true              |
    When the customer changes the email address is 'unregistered' for Shipster
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  39.0 |     30.0 |            0.0 |          9.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | false                           | false             |

  Scenario: Recalculate the delivery fee for shipster when user applies the TMD card
    Given a registered customer goes into the checkout process
    And a cart with entries:
      | product   | qty | price |
      | 100010901 |   2 |    13 |
    And the customer email address is 'registered' for AusPost Shipster
    And verify email api is invoked successfully
    And the postcode allocated for the cart is '3001'
    And delivery fee displayed for the 'home-delivery' is '0'
    And delivery fee displayed for the 'express-delivery' is '12.95'
    When selected delivery mode in spc is 'home-delivery'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  26.0 |     26.0 |            0.0 |          0.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | true                            | true              |
    When customer apply tmd 'valid'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  33.7 |     24.7 |            0.0 |          9.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | false                           | true              |

  Scenario: Recalculate the delivery fee for shipster when user applies the flybuys
    Given a registered customer goes into the checkout process
    And a cart with entries:
      | product   | qty | price |
      | 100010901 |   2 |    15 |
    And the customer email address is 'registered' for AusPost Shipster
    And verify email api is invoked successfully
    And the postcode allocated for the cart is '3001'
    And delivery fee displayed for the 'home-delivery' is '0'
    And delivery fee displayed for the 'express-delivery' is '12.95'
    When selected delivery mode in spc is 'home-delivery'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  30.0 |     30.0 |            0.0 |          0.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | true                            | true              |
    And the customer logins flybuys in spc
    When the customer redeems flybuys points with redeem code 'DUMMYREDEEMCODE2'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  19.0 |     30.0 |           20.0 |          9.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | false                           | true              |

  Scenario: Recalculate the delivery fee for shipster when user applies the voucher
    Given a registered customer goes into the checkout process
    And a cart with entries:
      | product   | qty | price |
      | 100010901 |   2 |    15 |
    And the customer email address is 'registered' for AusPost Shipster
    And verify email api is invoked successfully
    And the postcode allocated for the cart is '3001'
    And delivery fee displayed for the 'home-delivery' is '0'
    And delivery fee displayed for the 'express-delivery' is '12.95'
    When selected delivery mode in spc is 'home-delivery'
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  30.0 |     30.0 |            0.0 |          0.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | true                            | true              |
    When apply voucher VOUCHER-TV1 to cart in spc
    Then cart summary is:
      | total | subtotal | totalDiscounts | deliveryCost |
      |  29.0 |     30.0 |           10.0 |          9.0 |
    And shipster attributes on cart are:
      | originalDeliveryCost | ausPostDeliveryClubFreeDelivery | ausPostClubMember |
      |                  9.0 | false                           | true              |

  Scenario: Calculate the delivery fee for preOrder, when (express delivery fee > preOrder delivery fee)
    Given a registered customer goes into the checkout process
    When a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 |   2 |    30 |
    And the postcode allocated for the cart is '3001'
    Then delivery fee displayed for the 'express-delivery' is '4.0'
    
  Scenario: For postcode where express delivery is not avaliable.
    Given a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 | 1   | 15    |
    When post code is '1405'
    Then express delivery available 'false'
    
  Scenario: Calculate the delivery fee for preOrder, when (express delivery fee < preOrder delivery fee)
    Given a registered customer goes into the checkout process
    When a cart with entries:
      | product          | qty | price |
      | V1111_preOrder_1 |   2 |    30 |
    And the postcode allocated for the cart is '3001'
    And express delivery mode fee is '3.0'
    Then delivery fee displayed for the 'express-delivery' is '3.0'

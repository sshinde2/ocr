Feature: Display Look Page
    As a customer, I want to see all the products under a look page
  
  Description: The look and products are going to be setup using an impex (/tgttest/automation-impex/shopTheLook/test-looks.impex)

  Scenario: Online customer are able to browse the products going to look page
    Given the shop the look are setup in the system running the mentioned impex
    When the valid looks are being retrieved for id 'casualWearLook1'
    Then the look is retrieved with the following details:
      | id              | name               | description        | collectionId | collectionName |
      | casualWearLook1 | Casual Wear Look 1 | Casual Wear Look 1 | womenCasual  | Women's Casual |
    And the look is retrieved with the following images:
      | gridImageUrl | thumbImageUrl                                 | largeImageUrl | swatchImageUrl | heroImageUrl                                 | listImageUrl |
      |              | /medias/shopTheLook/casualWearLook1_thumb.jpg |               |                | /medias/shopTheLook/casualWearLook1_hero.jpg |              |
    And the look contains the products:
      | P8101_black |
      | CP6004      |
      | P5066_black |

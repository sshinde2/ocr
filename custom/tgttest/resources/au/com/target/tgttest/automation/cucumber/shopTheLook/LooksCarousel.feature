@notAutomated 
Feature: LooksCarousel 
    As a Target Online user I want to see other Looks in the same Collection on a Look page in a carousel

  Background:
    Given the test data is setup as below:
    |collection  |topLevelCategory    |looksInCollection                                                     |
    |Coll10      |Women               |Look11, Look12, Look13, Look14, Look15, Look16, Look17, Look18, Look19|
    |Coll20      |Women               |Look21                                                                |
    |Coll30      |Women               |Look31, Look32 (hidden)                                               |
    |Coll40      |Women               |NIL                                                                   |
    |Coll45      |Women               |Look41, Look42                                                        |
    |Coll50      |Home                |Look51, Look52, Look53, Look54                                        |
    |Coll60      |Women               |Look61, Look62, Look63 (hidden), Look64, Look65                       |
    |Coll70      |Home                |Look71, Look72, Look73, Look74, Look75                                |

  Scenario Outline: Display "More Looks to Inspire You" Carousel on Look pages
    Given a target.com.au user has navigated to the Look page for Look <look>
    And the "More Looks to Inspire You" carousel is configured to appear on the Looks template (eg. below the Product List and above Recently Viewed Items)
    When the Look page is displayed
    Then the "Other Looks in Collection" section is <otherLooksDisplay>
    And the Looks displayed, in order, are <looksDisplayed>
    And on Desktop the section <operateAsCarousel> operates as a Carousel
    And on Mobile the section swipes right and left
    And if the section is displayed, then there is also a link to the Collection page

    Examples: 
        |look    |otherLooksDisplay   |operateAsCarousel   |looksDisplayed                                                |
        |Look11  |displayed           |does                |Look12, Look13, Look14, Look15, Look16, Look17, Look18, Look19|
        |Look12  |displayed           |does                |Look11, Look13, Look14, Look15, Look16, Look17, Look18, Look19|
        |Look13  |displayed           |does                |Look11, Look12, Look14, Look15, Look16, Look17, Look18, Look19|
        |Look14  |displayed           |does                |Look12, Look13, Look15, Look16, Look17, Look18, Look19, Look11|
        |Look15  |displayed           |does                |Look13, Look14, Look16, Look17, Look18, Look19, Look11, Look12|
        |Look16  |displayed           |does                |Look14, Look15, Look17, Look18, Look19, Look11, Look12, Look13|
        |Look17  |displayed           |does                |Look15, Look16, Look18, Look19, Look11, Look12, Look13, Look14|
        |Look18  |displayed           |does                |Look16, Look17, Look19, Look11, Look12, Look13, Look14, Look15|
        |Look19  |displayed           |does                |Look17, Look18, Look11, Look12, Look13, Look14, Look15, Look16|
        |Look21  |not displayed       |N/A                 |N/A                                                           |
        |Look31  |not displayed       |N/A                 |N/A                                                           |
        |Look41  |displayed           |does not            |Look42                                                        |
        |Look51  |displayed           |does not            |Look52, Look53, Look54                                        |
        |Look61  |displayed           |does not            |Look62, Look64, Look65                                        |
        |Look71  |displayed           |does                |Look72, Look73, Look74, Look75                                |

  Scenario: Choose to view a Look
    Given a target online user is on a Look page
    And the Collection that the Look belongs to has at least one other visible Look
    And therefore there are Looks displayed in the Looks Carousel
    When the user chooses to navigate to a Look (by clicking anywhere on the tile for the Look)
    Then the Look page is displayed

  Scenario Outline: Display "Looks in Collection" Carousel on pages other than Look pages
    Given a target.com.au user has navigated to a page other than a Look page
    And the "Looks in Collection" carousel is configured to appear for Collection <collection>
    When the page is displayed
    Then the "Looks in Collection" section is <sectionDisplay>
    And the section <operateAsCarousel> operates as a Carousel
    And the Looks displayed, in CMS configured order, are <looksDisplayed>
    And on Desktop the section <operateAsCarousel> operates as a Carousel*
    And on Mobile the section swipes right and left
    And if the section is displayed, then there is also a link to the Collection page

    Examples: 
        |look    |sectionDisplay |operateAsCarousel |looksDisplayed                                                        |
        |Coll10  |displayed      |does              |Look11, Look12, Look13, Look14, Look15, Look16, Look17, Look18, Look19|
        |Coll20  |displayed      |does not          |Look21                                                                |
        |Coll30  |displayed      |does not          |Look31                                                                |
        |Coll40  |not displayed  |n/a               |n/a                                                                   |
        |Coll45  |not displayed  |n/a               |n/a                                                                   |
        |Coll50  |displayed      |does not          |Look51, Look52, Look53, Look54                                        |
        |Coll60  |displayed      |does not          |Look61, Look62, Look64, Look65                                        |
        |Coll70  |displayed      |does              |Look71, Look72, Look73, Look74, Look75                                |
        
  Scenario: Choose to view a Look
    Given a target online user is on a page that is not a Look page
    And the page is configured to show the "Looks in Collection" carousel for a Collection
    And that Collection has at least one visible Look
    And therefore there are Looks displayed in the "Looks in Collection" carousel
    When the user chooses to navigate to a Look (by clicking anywhere on the tile for the Look)
    Then the Look page is displayed

  Scenario: StartDate attribute is Mandatory for Looks created in STEP
    Given a web coordinator has opened STEP
    When they add a look in STEP
    Then "StartDate" attribute is a Mandatory field

  Scenario: Sorting the Looks via Start Date
    Given a web coordinator have added Look in STEP
    And they have added the Date & Time in the StartDate (Date Time) attribute
    When they browse the webpage "www.target.com.au"
    Then the latest look appears on the webpage

  Scenario: Looks to be sorted via StartDate in a descending order
    Given a web coordinator has opened STEP
    When they add looks
        |Looks   |StartDate          |
        |Look_A  |2016-10-25 10:30:10|
        |Look_B  |2016-10-25 09:40:10|
        |Look_C  |2016-10-24 16:40:10|
        |Look_D  |2016-10-23 16:35:10|
    Then looks appear on the web in descending order Look_A; Look_B; Look_C; Look_D

  Scenario: Adding Looks to STEP with a future Start Date
    Given a web coordinator has opened STEP
    When they add future Date & Time in the StartDate (Date Time) attribute
    Then the Look does not appear currently on the Web
    And look appears only appears on the Web at the Future Date&Time specified

  Scenario: Look stop appearing past End Date
    Given a web coordinator has added a Look in STEP
    When they have entered the details in the attribute End Date
    Then look stops appearing on the web past the EndDate (Timestamp)

  Scenario: Enabled attribute Active
    Given a web coordinator has opened STEP
    When they have added a Look in STEP with an Enabled attribute set to 'Y'
    Then the look appears on the web (Provided it's within the StartDate & EndDate time range)

  Scenario: Enabled attribute Non-Active
    Given a web coordinator has opened STEP
    When they have added a Look in STEP with an Enabled attribute set to 'N'
    Then the look does NOT appear on the web (Irrespective it's within the StartDate & EndDate time range)

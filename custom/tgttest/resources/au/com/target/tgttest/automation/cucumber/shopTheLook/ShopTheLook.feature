Feature: Shop The Look Page
    As a customer, I want to see all the looks under a department in a single page for a shop the look
    and the looks should be filterable based on collection

 Description: The looks are going to be setup using an impex (/tgttest/automation-impex/shopTheLook/test-looks.impex)
    The impex contains a shop the look which has 2 collections and 8 looks
    Out of the all the looks only 3 looks are valid to be shown

  Scenario: Online customer are able to browse the looks going to shop the look page
    Given the shop the look are setup in the system running the mentioned impex
    When the valid looks are being retrieved for shop the look having id 'STL02'
    Then the shop the look is retrieved with the following details:
      | title               |  countOfLooks |
      | Women's Inspiration |    3          |
    And the retrieved shop the look will have the following collections:
      | colletionId | collectionName        | countOfLooks |
      | LC01        | Women's Casual        |   1          |
      | LC02        | Women's Spring Casual |   2          |
    And the retrieved shop the look will have the following looks:
      | lookId | lookName             | url                              | collectionId | collectionName        | description                  |
      | LOOK3  | Spring Casual Look 1 | /look/spring-casual-look-1/LOOK3 | LC02         | Women's Spring Casual | Women's Spring Casual Look 1 |
      | LOOK6  | Spring Casual Look 4 | /look/spring-casual-look-4/LOOK6 | LC02         | Women's Spring Casual | Womens Spring Casual Look 4  |
      | LOOK1  | Casual Wear Look 1   | /look/casual-wear-look-1/LOOK1   | LC01         | Women's Casual        | Casual Wear Look 1           |
    But will not contain the following looks:
      | LOOK2 |
      | LOOK4 |
      | LOOK5 |
      | LOOK7 |

  @notAutomated
  Scenario: ShopTheLook being populated on the Mega Menu
    Given I have opened the webpage "target.com.au" as a Target online user
    When I click on Mega Menu-Category
    Then I see the link for Shop The Look

  @notAutomated
  Scenario: No Collection Heading Title appears, if not selected
    Given I am on the ShopTheLook Page as a Target online user
    When I have not selected any Collection Filter
    Then no heading title appears on ShopTheLook page

  @notAutomated
  Scenario: Collection Heading Title appears, if selected
    Given I am on the ShopTheLook Page as a Target online user
    When I have selected a Collection Filter
    Then heading title appears on ShopTheLook page

  @notAutomated
  Scenario: Number of Results to appear
    When I am on the ShopTheLook Page as a Target online user
    Then the total number of looks in the page is displayed

  @notAutomated
  Scenario: Look has a Look Name
    When I am on the ShopTheLook Page as a Target online user
    Then I see the look name(rather than a general name like Look 1, Look 2..) below the look image

  @notAutomated
  Scenario: Every Look has a Collection Filter Title, if not selected
    When I am on the ShopTheLook Page as a Target online user
    And I have not selected any collection filter
    Then I see the collection title below the look name

  @notAutomated
  Scenario: Look's Filter Title goes off, once Collection Filter is selected
    When I am on the ShopTheLook Page as a Target online user
    And I have selected a collection filter
    Then I DO NOT see the collection title below the look name

  @notAutomated
  Scenario: Two Looks adjacent to each other (Mobile Only)
    When I have opened the ShopTheLook page in Mobile as a Target online user
    Then I see rows of 2 Looks each

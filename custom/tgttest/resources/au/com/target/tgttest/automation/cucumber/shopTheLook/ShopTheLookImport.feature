@stlImport
Feature: Import Looks & Collection into Hybris
    As a target support staff
    I need to ensure shop the look entries are imported from STEP through ESB
    so that shop the look is available on target.com.au

  Scenario: Collection with a single Look, which has Multiple Products 
    Given a shopTheLook with category '2000', '1' collections each with '1' looks and '2' products each from STEP
    When XML export from STEP is passed to hybris through ESB
    Then '1' collection is created
    And there is '1' look in each collection
    And there are '2' products in each look

  Scenario: 2 Collections with 2 Looks each, which has 2 Products each 
    Given a shopTheLook with category '2000', '2' collections each with '2' looks and '2' products each from STEP
    When XML export from STEP is passed to hybris through ESB
    Then '2' collections are created
    And there are '2' looks in each collection
    And there are '2' products in each look

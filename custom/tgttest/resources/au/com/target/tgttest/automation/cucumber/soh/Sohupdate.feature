@cartProductData
Feature: Stock on Hand - Stock Lookup For Product in Hybris for CNP warehouse
  
  As the trading team 
  I want to assign the dropship inventory from the CNP warehouse to the CNP dropship product in Hybris 
  So that there is visibility of the SOH for the CNP products

  Background: 
    Given hybris has products set up:
      | product  | warehouseCode     | available |
      | 10000324 | CnpWarehouse      | 4         |
      | 10000325 | CnpWarehouse      | 7         |
      | 10000326 | CnpWarehouse      | 6         |
      | 10000327 | CnpWarehouse      | 10        |
      | 10000328 | CnpWarehouse      | 4         |
      | 10000011 | FastlineWarehouse | 4         |
      | 10000012 | FastlineWarehouse | 4         |
      | 10000022 | FastlineWarehouse | 4         |
      | 10000111 | FastlineWarehouse | 4         |
    And the max batch size for stock update is 10

  Scenario Outline: Update SOH for individual product against warehouses
    Given a SOH file is received for a product
      | productCode      | warehouse      | ean      | totalQuantityAvailable      |
      | <sohProductCode> | <sohWarehouse> | <sohEan> | <sohTotalQuantityAvailable> |
    When the SOH is assigned to an individual product
    Then stock is updated in Hybris against the warehouse
      | product   | available          | warehouseCode |
      | <product> | <actualStockLevel> | <warehouse>   |

    Examples: 
      | sohProductCode | sohEan        | sohWarehouse      | sohTotalQuantityAvailable | product  | warehouse         | actualStockLevel | comment                                                            |
      |                | 00357357      | CnpWarehouse      | 10                        | 10000324 | CnpWarehouse      | 10               | stock will be updated                                              |
      |                | 09475915      | CnpWarehouse      | 3                         | 10000325 | CnpWarehouse      | 3                | stock will be updated                                              |
      |                | 0000772059817 | CnpWarehouse      | 0                         | 10000326 | CnpWarehouse      | 0                | stock will be updated                                              |
      |                | 0004405976120 | CnpWarehouse      | -1                        | 10000327 | CnpWarehouse      | 0                | stock will be updated                                              |
      |                | 817980010536  | CnpWarehouse      | 12                        |          |                   |                  | stock will not be updated as the product does not exists in Hybris |
      | 10000012       |               | FastlineWarehouse | 10                        | 10000012 | FastlineWarehouse | 10               | stock will be updated                                              |
      | 10000022       |               | FastlineWarehouse | 0                         | 10000022 | FastlineWarehouse | 0                | stock will be updated                                              |
      | 10000111       |               | FastlineWarehouse | -1                        | 10000111 | FastlineWarehouse | 0                | stock will be updated                                              |
      | 10000323       |               | CnpWarehouse      | 4                         | 10000323 | CnpWarehouse      | 4                | stock will be updated                                              |

  Scenario Outline: Adjust Stock for individual product against warehouses
    Given a SOH file is received for a product
      | productCode      | warehouse      | ean      | adjustmentQuantity   |
      | <sohProductCode> | <sohWarehouse> | <sohEan> | <adjustmentQuantity> |
    When the stock adjustment is assigned to an individual product
    Then stock is adjusted in Hybris against the warehouse
      | product   | available          | warehouseCode | responseStatus   |
      | <product> | <actualStockLevel> | <warehouse>   | <responseStatus> |

    Examples: 
      | sohProductCode | sohEan        | sohWarehouse      | adjustmentQuantity | product  | warehouse         | actualStockLevel | responseStatus | comment                                                                     |
      |                | 00357357      | CnpWarehouse      | 6                  | 10000324 | CnpWarehouse      | 10               | OK             | stock will be updated                                                       |
      |                | 09475915      | CnpWarehouse      | -4                 | 10000325 | CnpWarehouse      | 3                | OK             | stock will be updated                                                       |
      |                | 0000772059817 | CnpWarehouse      | -6                 | 10000326 | CnpWarehouse      | 0                | OK             | stock will be updated                                                       |
      |                | 0004405976120 | CnpWarehouse      | -11                | 10000327 | CnpWarehouse      | -1               | OK             | stock will be updated                                                       |
      | 10000324       |               | CnpWarehouse      | 6                  | 10000324 | CnpWarehouse      | 10               | OK             | stock will be updated                                                       |
      |                | 9300633499815 | CnpWarehouse      | -1                 | 10000011 | FastlineWarehouse | 4                | NOT_FOUND      | stock will not be updated as the product does not belongs to CNP ware house |
      |                | 8888888888888 | CnpWarehouse      | 10                 |          |                   |                  | NOT_FOUND      | stock will not be updated as the product does not exists in Hybris          |
      | 10000012       |               | FastlineWarehouse | 15                 | 10000012 | FastlineWarehouse | 19               | OK             | stock will be updated                                                       |
      | 10000022       |               | FastlineWarehouse | 0                  | 10000022 | FastlineWarehouse | 4                | OK             | stock will be updated                                                       |
      | 10000111       |               | FastlineWarehouse | -4                 | 10000111 | FastlineWarehouse | 0                | OK             | stock will be updated                                                       |
      |                | 9300633499815 | FastlineWarehouse | 4                  | 10000011 | FastlineWarehouse | 8                | OK             | stock will be updated                                                       |

  Scenario: Update SOH for multiple products against warehouse
    Given a SOH file is received for a list of products
      | productCode | warehouse    | ean           | totalQuantityAvailable |
      |             | CnpWarehouse | 00357357      | 10                     |
      |             | CnpWarehouse | 09475915      | 3                      |
      |             | CnpWarehouse | 0000772059817 | 0                      |
      |             | CnpWarehouse | 0004405976120 | -1                     |
      |             | CnpWarehouse | 9300633499815 | 12                     |
      |             | CnpWarehouse | 8888888888888 | 12                     |
    When the SOH is assigned to a list of product
    Then the stock of the products are updated in Hybris against the warehouse
      | product  | available | warehouseCode |
      | 10000324 | 10        | CnpWarehouse  |
      | 10000325 | 3         | CnpWarehouse  |
      | 10000326 | 0         | CnpWarehouse  |
      | 10000327 | 0         | CnpWarehouse  |
    And the SOH response status is 'OK'

  Scenario: Update SOH for multiple products against warehouse with exceed max batch size error
    Given the max batch size for stock update is 5
    And a SOH file is received for a list of products
      | productCode | warehouse    | ean           | totalQuantityAvailable |
      |             | CnpWarehouse | 00357357      | 10                     |
      |             | CnpWarehouse | 09475915      | 3                      |
      |             | CnpWarehouse | 0000772059817 | 0                      |
      |             | CnpWarehouse | 0004405976120 | -1                     |
      |             | CnpWarehouse | 9300633499815 | 12                     |
      |             | CnpWarehouse | 8888888888888 | 12                     |
    When the SOH is assigned to a list of product
    Then the SOH response status is 'EXCEED_MAX_BATCH_SIZE'

  Scenario: Adjust SOH for multiple products against warehouse
    Given a SOH file is received for a list of products
      | productCode | warehouse    | ean           | adjustmentQuantity |
      |             | CnpWarehouse | 00357357      | 6                  |
      |             | CnpWarehouse | 09475915      | -4                 |
      |             | CnpWarehouse | 0000772059817 | -6                 |
      |             | CnpWarehouse | 0004405976120 | -11                |
      | 10000324    | CnpWarehouse |               | 6                  |
      |             | CnpWarehouse | 9300633499815 | -1                 |
      |             | CnpWarehouse | 8888888888888 | 10                 |
    When the stock adjustment is assigned to a list of product
    Then the stock of the products are updated in Hybris against the warehouse
      | product  | available | warehouseCode     |
      | 10000324 | 16        | CnpWarehouse      |
      | 10000325 | 3         | CnpWarehouse      |
      | 10000326 | 0         | CnpWarehouse      |
      | 10000327 | -1        | CnpWarehouse      |
      | 10000011 | 4         | FastlineWarehouse |
    And the SOH response status is 'OK'

  Scenario: Adjust SOH for multiple products against warehouse with exceed max batch size error
    Given the max batch size for stock update is 5
    And a SOH file is received for a list of products
      | productCode | warehouse    | ean           | adjustmentQuantity |
      |             | CnpWarehouse | 00357357      | 6                  |
      |             | CnpWarehouse | 09475915      | -4                 |
      |             | CnpWarehouse | 0000772059817 | -6                 |
      |             | CnpWarehouse | 0004405976120 | -11                |
      | 10000324    | CnpWarehouse |               | 6                  |
      |             | CnpWarehouse | 9300633499815 | -1                 |
      |             | CnpWarehouse | 8888888888888 | 10                 |
    When the stock adjustment is assigned to a list of product
    Then the SOH response status is 'EXCEED_MAX_BATCH_SIZE'

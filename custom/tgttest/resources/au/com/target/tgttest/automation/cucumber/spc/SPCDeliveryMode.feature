@postCodeGroupData @cartProductData @deliveryModeData @shipsterConfigData @initPreOrderProducts
Feature: Delivery mode - Single Page Checkout
  
  Scenarios use cases for delivery modes in single page checkout.

  Scenario Outline: User should see applicable delivery modes based on products in basket subject to ats when using fluent
    Given the 'fluent' feature switch is <fluentEnabled>
    And products with the following ats values in fluent:
      | product          | CC | HD | ED  | PO  |
      | 10000211         | 3  | 2  | 0   | 0   |
      | 10000212         | 0  | 2  | 3   | 0   |
      | V1111_preOrder_1 | 0  | 0  | 0   | 100 |
      | V2222_preOrder_2 | 0  | 0  | 100 | 0   |
    And a cart with product '<productsInCart>' and with quantity '<qty>'
    And pre-order message for express-delivery is '<longDescription>'
    When delivery modes for the cart is fetched
    Then applicable deliveryModes are '<deliveryModes>'
    And the express delivery mode values for the pre-order are :
      | name             | longDescription                                         | code             |
      | Express Delivery | Testing pre-order product express-delivery long message | express-delivery |

    Examples: 
      | productsInCart    | qty | fluentEnabled | deliveryModes                                    | longDescription                                         |
      | 10000211,10000212 | 2,2 | enabled       | home-delivery                                    |                                                         |
      | 10000211,10000212 | 2,2 | disabled      | click-and-collect,home-delivery,express-delivery |                                                         |
      | 10000211          | 2   | enabled       | click-and-collect,home-delivery                  |                                                         |
      | 10000212          | 2   | enabled       | home-delivery,express-delivery                   |                                                         |
      | 10000211          | 2   | disabled      | click-and-collect,home-delivery,express-delivery |                                                         |
      | 10000212          | 2   | disabled      | click-and-collect,home-delivery,express-delivery |                                                         |
      | V1111_preOrder_1  | 2   | enabled       | express-delivery                                 | Testing pre-order product express-delivery long message |
      | V2222_preOrder_2  | 2   | enabled       | express-delivery                                 | Testing pre-order product express-delivery long message |
      | V1111_preOrder_1  | 2   | disabled      | express-delivery                                 | Testing pre-order product express-delivery long message |

  Scenario Outline: : As a customer with products in basket I want to select a delivery mode applicable to my basket.
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And registered user checkout
    When selected delivery mode in spc is '<selectedDeliveryMode>'
    Then cart delivery mode is '<cartDeliveryMode>'

    Examples: 
      | selectedDeliveryMode | cartDeliveryMode  |
      | click-and-collect    | click-and-collect |
      | express-delivery     |                   |

  Scenario Outline: : As a customer with products in basket I want to change the delivery modes and have my address preselected provided it is valid.
    Given a cart with entries:
      | product   | qty | price |
      | 100010901 | 1   | 15    |
    And registered user checkout
    And customer has saved addresses
      | 12 Thompson Road,North Geelong,3002 |
      | 25 Thompson Road,North Geelong,0201 |
    And delivery address selected in spc is '<selectedAddress>' for delivery mode '<selectedDeliveryMode>'
    When selected delivery mode in spc is changed to '<newDeliveryMode>'
    Then cart delivery mode is '<cartDeliveryMode>'
    And saved cart delivery address is '<savedDeliveryAddress>'

    Examples: 
      | selectedDeliveryMode | selectedAddress                     | newDeliveryMode  | cartDeliveryMode | savedDeliveryAddress                |
      | home-delivery        | 25 Thompson Road,North Geelong,0201 | express-delivery | express-delivery |                                     |
      | home-delivery        | 12 Thompson Road,North Geelong,3002 | express-delivery | express-delivery | 12 Thompson Road,North Geelong,3002 |

  Scenario Outline: As a target customer begins checkout with Shipster feature enabled
    Given a cart with entries:
      | product       | qty   | price   |
      | <productCode> | <qty> | <price> |
    When shipster membership status is '<shipstermemberStatus>'
    Then validated each delivery modes based on the order value and shipping value
    And return shipster available for each '<deliveryMode>' as '<shipsterAvailable>' with '<reasonCode>'

    Examples: 
      | productCode | qty | price | deliveryMode      | shipstermemberStatus | reasonCode                  | shipsterAvailable |
      | 10000011    | 2   | 15    | home-delivery     | true                 |                             | true              |
      | 10000011    | 2   | 15    | express-delivery  | true                 |                             | true              |
      | 10000011    | 2   | 15    | click-and-collect | true                 | ERR_SHIPSTER_NOT_CONFIGURED | false             |
      | 10000011    | 2   | 15    | home-delivery     | false                | ERR_SHIPSTER_NOT_SUBSCRIBED | false             |
      | 10000011    | 2   | 15    | express-delivery  | false                | ERR_SHIPSTER_NOT_SUBSCRIBED | false             |
      | 10000011    | 1   | 15    | home-delivery     | true                 | ERR_SHIPSTER_LOW_VALUE      | false             |
      | 10000011    | 1   | 15    | express-delivery  | true                 | ERR_SHIPSTER_LOW_VALUE      | false             |
      | 10000011    | 1   | 15    | home-delivery     | false                | ERR_SHIPSTER_NOT_SUBSCRIBED | false             |
      | 10000011    | 1   | 15    | express-delivery  | false                | ERR_SHIPSTER_NOT_SUBSCRIBED | false             |

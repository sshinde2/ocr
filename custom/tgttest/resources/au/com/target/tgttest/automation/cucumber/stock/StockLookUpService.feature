@initPreOrderProducts
Feature: Stock Lookup Service
  In order to provide third party vendors and customers with better visibility of stock from warehouse
  As a Customer/third party vendor
  I want to have visibility of the stock in warehouse

  Scenario: Fetching stock for products from default warehouse
    Given products with the following stock exists against fastline warehouse:
      | product       | available | reserved |
      | 10000011      | 100       | 10       |
      | 10000012      | 200       | 20       |
      | P1001_black_L | 150       | 50       |
    And the 'fluent' feature switch is disabled
    When the stock level is looked up:
      | itemCode          | requestedQuantity |
      | 10000011          | 10                |
      | 10000012          | 20                |
      | P1001_black_L     | 101               |
      | PGC1000_iTunes_10 | 1                 |
    Then the retrieved stock response will contain the following details:
      | itemCode          | isAvailable | totalAvailableQuantity |
      | 10000011          | true        | 90                     |
      | 10000012          | true        | 180                    |
      | P1001_black_L     | false       | 100                    |
      | PGC1000_iTunes_10 | false       | 0                      |

  Scenario: Fetching stock for products using fluent
    Given products with the following ats values in fluent:
      | product       | CC  | HD | ED |
      | 10000011      | 90  | 0  | 0  |
      | 10000012      | 180 | 0  | 0  |
      | P1001_black_L | 100 | 0  | 0  |
    And the 'fluent' feature switch is enabled
    When the stock level is looked up:
      | itemCode          | requestedQuantity |
      | 10000011          | 10                |
      | 10000012          | 20                |
      | P1001_black_L     | 101               |
      | PGC1000_iTunes_10 | 1                 |
    Then the retrieved stock response will contain the following details:
      | itemCode          | isAvailable | totalAvailableQuantity |
      | 10000011          | true        | 90                     |
      | 10000012          | true        | 180                    |
      | P1001_black_L     | false       | 100                    |
      | PGC1000_iTunes_10 | false       | 0                      |

  Scenario Outline: Fetching ATS, consolidatedStoreStock and storeSoh using new stock endpoint
    Given products with the following stock exists against fastline warehouse:
      | product  | available | reserved |
      | 10000011 | 16        | 0        |
      | 10000012 | 15        | 0        |
      | 10000021 | 1         | 0        |
      | 10000022 | 0         | 0        |
    And products with the following ats values in fluent:
      | product  | CC | HD | ED | PO |
      | 10000011 | 16 | 15 | 1  | 20 |
      | 10000012 | 1  | 0  | 15 | 0  |
      | 10000021 | 15 | 16 | 15 | 0  |
      | 10000022 | 15 | 1  | 0  | 0  |
    And products with the following consolidated stock exists:
      | product  | stock |
      | 10000011 | 0     |
      | 10000012 | 1     |
      | 10000021 | 1     |
      | 10000022 | 0     |
    And the following stock levels are configured:
      | code     | storeNumber | soh |
      | 10000011 | 7001        | 9   |
      | 10000011 | 7002        | 8   |
      | 10000012 | 7001        | 1   |
      | 10000012 | 7002        | 0   |
    And the 'fluent' feature switch is <fluentEnabled>
    When stock level is looked up with:
      | variants   | deliveryTypes   | locations   |
      | <variants> | <deliveryTypes> | <locations> |
    Then stock response has:
      | variants   | atsCc   | atsHd   | atsEd   |  atsPo   | consolidatedStoreStock   | storeSoh   | error   |
      | <variants> | <atsCc> | <atsHd> | <atsEd> |  <atsPo> | <consolidatedStoreStock> | <storeSoh> | <error> |

    Examples:
      | fluentEnabled | variants          | deliveryTypes                       | locations | atsCc               | atsHd               | atsEd               |  atsPo                 | consolidatedStoreStock | storeSoh                                                 | error              |
      | enabled       | 10000011,10000012 | CC,HD,ED,PO,CONSOLIDATED_STORES_SOH | 7001,7002 | inStock,lowStock    | lowStock,outOfStock | lowStock,lowStock   |  instock,outOfStock    | outOfStock,inStock     | 7001:inStock 7002:lowStock,7001:lowStock 7002:outOfStock | none               |
      | enabled       | 10000021,10000022 | CC,HD,ED,PO,CONSOLIDATED_STORES_SOH | none      | lowStock,lowStock   | inStock,lowStock    | lowStock,outOfStock |  outOfStock,outOfStock | inStock,outOfStock     | none                                                     | none               |
      | disabled      | 10000011,10000012 | CC,HD,ED,CONSOLIDATED_STORES_SOH    | 7001,7002 | inStock,lowStock    | inStock,lowStock    | inStock,lowStock    |  none                  | outOfStock,inStock     | 7001:inStock 7002:lowStock,7001:lowStock 7002:outOfStock | none               |
      | disabled      | 10000021,10000022 | CC,HD,ED,CONSOLIDATED_STORES_SOH    | none      | lowStock,outOfStock | lowStock,outOfStock | lowStock,outOfStock |  none                  | inStock,outOfStock     | none                                                     | none               |
      | enabled       | 10000011          | CC,CONSOLIDATED_STORES_SOH          | none      | inStock             | none                | none                |  none                  | outOfStock             | none                                                     | none               |
      | disabled      | 10000011          | CC,CONSOLIDATED_STORES_SOH          | none      | inStock             | none                | none                |  none                  | outOfStock             | none                                                     | none               |
      | enabled       | 10000011          | none                                | none      | none                | none                | none                |  none                  | none                   | none                                                     | ERR_EMPTY_OPTIONS  |
      | disabled      | 10000011          | none                                | none      | none                | none                | none                |  none                  | none                   | none                                                     | ERR_EMPTY_OPTIONS  |
      | enabled       | none              | CC,HD,ED,CONSOLIDATED_STORES_SOH    | 7001,7002 | none                | none                | none                |  none                  | none                   | none                                                     | ERR_EMPTY_VARIANTS |
      | disabled      | none              | CC,HD,ED,CONSOLIDATED_STORES_SOH    | 7001,7002 | none                | none                | none                |  none                  | none                   | none                                                     | ERR_EMPTY_VARIANTS |

  Scenario Outline: Fetching stock level status for the pre-order products from warehouse
    Given products with the following stock exists against fastline warehouse:
      | product                    | maxPreOrder | preOrder | reserved | available |
      | V1111_preOrder_1           | 90          | 1        | 0        | 0         |
      | V3333_preOrder_maxQuantity | 1000        | 1000     | 0        | 0         |
      | V003_PreOrder_03_L         | 90          | 1        | 0        | 0         |
      | V004_PreOrder_04_M         | 100         | 100      | 0        | 0         |
    And the 'fluent' feature switch is disabled
    When stock level is looked up with:
      | variants   | deliveryTypes   |
      | <variants> | <deliveryTypes> |
    Then stock response has:
      | variants   | atsCc   | atsHd   | atsEd   | atsPo   |
      | <variants> | <atsCc> | <atsHd> | <atsEd> | <atsPo> |

    Examples:
      | variants                   | deliveryTypes | atsCc      | atsHd      | atsEd      | atsPo      | 
      | V1111_preOrder_1           | CC,HD,ED,PO   | inStock    | inStock    | inStock    | inStock    |
      | V3333_preOrder_maxQuantity | CC,HD,ED,PO   | outOfStock | outOfStock | outOfStock | outOfStock |
      | V003_PreOrder_03_L         | CC,HD,ED,PO   | inStock    | inStock    | inStock    | inStock    |
      | V004_PreOrder_04_M         | CC,HD,ED,PO   | outOfStock | outOfStock | outOfStock | outOfStock |

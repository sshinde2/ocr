Feature: Fetching  Stores
  In order to provide a way for external systems and online  with better visibility of stores and details
  As Online operations
  I want to retrieve all Target stores and details

  Scenario: Fetching all the Target Stores
    Given The following target Stores are present in Hybris:
      | Belconnen        |
      | Melbourne        |
      | Picton           |
      | Bundaberg        |
      | Caroline Springs |
    When all the stores are fetched from Hybris
    Then response should contain the following stores and details:
      | state | storeNumber | storeName        | targetOpeningHours     | formattedAddress                      | phone          | latitude   | longitude  | timeZone             |
      | ACT   | 7123        | Belconnen        | target-hours-0         | 18 Benjamin Way, Belconnen, 2617      | (02) 6256 4000 | -35.238385 | 149.065848 | Australia/ACT        |
      | VIC   | 7131        | Melbourne        | target-hours-0         | 236 Bourke Street, Melbourne, 3000    | (03) 9653 4000 | -37.812961 | 144.966522 | Australia/Victoria   |
      | NSW   | 7381        | Picton           | target-country-hours-0 | 9-13 Margaret Street, Picton, 2571    | (02) 4640 0600 | -34.168192 | 150.613509 | Australia/NSW        |
      | QLD   | 7048        | Bundaberg        | target-hours-0         | Bourbong Street, Bundaberg, 4670      | (07) 4152 2499 | -24.865    | 152.352    | Australia/Queensland |
      | VIC   | 7120        | Caroline Springs | target-no-hours        | 13-19 Lake St, Caroline Springs, 3023 | (03) 8361 0600 | -37.733292 | 144.74321  | Australia/Victoria   |
    But the following closed stores will not be present:
      | Kingaroy |
      | Renmark  |

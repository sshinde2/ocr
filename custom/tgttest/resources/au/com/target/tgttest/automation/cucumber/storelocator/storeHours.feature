Feature: Store hours
  In order to provide customers with better visibility over store opening hours
  As a Online operations
  I want to display store hours in online site

  Scenario: Creating store hours for a new store
    Given Target store with number '1999' does not exist
    When Excel spreadsheet containing store opening hours for the store is processed
      | openingDay | openingTime | closingTime |
      | Today      | 9.00AM      | 11.00PM     |
    Then the hours for non-existing store will be ignored

  Scenario: Updating the opening time for an existing store
    Given Target store with number '7001' exists
    And opening hours for the store are:
      | openingDay | openingTime | closingTime |
      | yesterday  | 8.00AM      | 07.00PM     |
      | today      | 8.00AM      | 07.00PM     |
      | tomorrow   | 8.00AM      | 07.00PM     |
    When Excel spreadsheet containing store opening hours for the store is processed
      | openingDay         | openingTime | closingTime |
      | today              | 9.00AM      | 08.00PM     |
      | tomorrow           | 9.00AM      | 07.00PM     |
      | day after tomorrow | 8.00AM      | 06.00PM     |
    Then the opening schedule will be updated
    And unused opening days deleted for the store are:
      | openingDay | openingTime | closingTime |
      | yesterday  | 8.00AM      | 07.00PM     |

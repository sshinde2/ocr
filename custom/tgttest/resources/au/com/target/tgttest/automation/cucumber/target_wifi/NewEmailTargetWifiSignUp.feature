@notAutomated
Feature: New email on Target wi-fi internet sign-up for users

  Scenario: New ExactTarget enews welcome email for Target wifi sign-up
    Given a user is in the Target Store
    And the user has Mobile device
    When the user opts for free Target store Wireless internet
    Then the user signed up for new e-news
    And the user receives an email - which is a wifi specific enew welcome email(Email ID 21410)
    And the email user received is from ExactTarget, not Hybris

  Scenario: Regression for normal ExactTarget enews welcome email for Target online user
    Given a Target online user explores the Target website
    When the user subscribes the enews online
    Then the user receives the normal enews welcome email(Email ID 696)
    And The email user received is from ExactTarget, not Hybris

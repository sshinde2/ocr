@tlog @cartProductData @deliveryModeData
Feature: TLOG - Multi-card payments
    In order to integrate with POS As online operations I want REFUND TLOG entries to be created for cancellations and refunds to multiple cards

  Background: 
    Given giftcard/creditcard details:
      | paymentId | cardType   | cardNumber        | cardExpiry | token     | tokenExpiry | bin |
      | GC1       | Giftcard   | 62734500000000002 | 01/20      | 123456789 |             | 31  |
      | CC1       | VISA       | 4987654321010012  | 01/20      | 123456789 | 01/01/2020  | 35  |
      | CC2       | MASTERCARD | 5587654321010013  | 02/20      | 123456789 | 01/02/2020  | 36  |

  Scenario: Full cancel on order with multi card payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 50    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,40       |
      | CC1,60       |
    When order is placed
    And the order is fully cancelled
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -10000
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 1   | -5000 | 5000      |
      | 10000011 | 1   | -5000 | 5000      |
    And tlog multi card payment info matches

  Scenario: Partial cancel on order with multi card payment, where the tender amount fits in the first card
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 50    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,40       |
      | CC1,60       |
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -5000
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 1   | -5000 | 5000      |
    And tlog multi card payment info matches

  @toFix
  Scenario: Partial cancel on order with multi card payment, where the tender amount crosses two cards
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 70    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,80       |
      | CC1,60       |
    When order is placed
    And cancel entries
      | product  | qty |
      | 10000011 | 1   |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -7000
    And tlog entries are:
      | code     | qty | price | filePrice |
      | 10000011 | 1   | -7000 | 7000      |
    And tlog tender entries are:
      | cardNumber       | approved | amount | type |
      | 4987654321010012 | true     | -60    | eft  |
      | null             | true     | -10    | cash |

  Scenario: Full cancel on order with multi card payment, where one refund to credit card fails
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 70    |
    And registered user checkout
    And delivery mode is 'home-delivery'
    And payment method is 'ipg'
    And customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | GC1,80       |
      | CC1,40       |
      | CC2,20       |
    And refund for credit card '4987654321010012' is declined
    When order is placed
    And the order is fully cancelled
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -14000
    And tlog tender entries are:
      | cardNumber        | approved | amount | type |
      | 62734500000000002 | true     | -80    | eft  |
      | 5587654321010013  | true     | -20    | eft  |
      | null              | true     | -40    | cash |


  Scenario: Short pick on order with multi card payment, where one refund to credit card fails
    Given customer submits payment for a combination of cards with amount:
      | PaymentEntry |
      | CC1,20       |
      | GC1,40       |
    And a consignment sent to fastline with products:
      | product  | qty | price |
      | 10000011 | 3   | 20    |
    And refund for credit card '4987654321010012' is declined
    When fastline returns pick entry data:
      | product  | qty |
      | 10000011 | 1   |
    Then tlog type is 'refund'
    And tlog shipping is 000
    And tlog total tender is -4000
    And tlog tender entries are:
      | cardNumber       | approved | amount | type |
      | null             | true     | -40   | cash |

@wip
Feature: Team Member Discount
  In order to utilize team member discount
  As a Team Member
  I need to use my team member discount card in the checkout process

  Scenario Outline: Placing an Order with Target TMD Card with only apparal items
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 10    |
      | 10000111 | 2   | 15    |
      | 10000211 | 3   | 8     |
    And customer presents tmd 'targettmd'
    And payment method is '<payment_method>'
    When order is placed
    Then order entries are :
      | product  | qty | basePrice | totalPrice |
      | 10000011 | 1   | 10        | 8          |
      | 10000111 | 2   | 15        | 24         |
      | 10000211 | 3   | 8         | 19.2       |
    And Order is :
      | total | subtotal | deliveryCost | totalTax |
      | 60.2  | 51.2     | 9.0          | 5.47     |
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000011 | 1   | 1000  | 1000      | staff            | 20              | 200                |
      | 10000111 | 2   | 1500  | 1500      | staff            | 20              | 600                |
      | 10000211 | 3   | 800   | 800       | staff            | 20              | 480                |
    And tlog type is 'sale'
    And transDiscount entries are:
      | type  | code      | pct | amount |
      | staff | targettmd | 20  | 1280   |
    And tlog total tender is 6020
    And Accertify feed has :
      | promotionAmount | targetStaffDiscountCard |
      | 12.80           | targettmd               |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an Order with Target TMD Card with only non-apparal items
    Given a cart with entries:
      | product  | qty | price |
      | 10000911 | 3   | 12    |
      | 10000912 | 3   | 49    |
      | 10001010 | 3   | 29    |
    And customer presents tmd 'targettmd'
    And payment method is '<payment_method>'
    When order is placed
    Then order entries are :
      | product  | qty | basePrice | totalPrice |
      | 10000911 | 3   | 12        | 34.20      |
      | 10000912 | 3   | 49        | 139.65     |
      | 10001010 | 3   | 29        | 82.65      |
    And Order is :
      | total  | subtotal | deliveryCost | totalTax |
      | 256.50 | 256.50   | 0.0          | 23.32    |
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000911 | 3   | 1200  | 1200      | staff            | 5               | 180                |
      | 10000912 | 3   | 4900  | 4900      | staff            | 5               | 735                |
      | 10001010 | 3   | 2900  | 2900      | staff            | 5               | 435                |
    And tlog type is 'sale'
    And transDiscount entries are:
      | type  | code      | pct | amount |
      | staff | targettmd | 5   | 1350   |
    And tlog total tender is 25650
    And Accertify feed has :
      | promotionAmount | targetStaffDiscountCard |
      | 13.5            | targettmd               |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an Order with Target TMD Card with both apparal and no-apparal items
    Given a cart with entries:
      | product  | qty | price |
      | 10000112 | 2   | 25    |
      | 10000122 | 3   | 30    |
      | 10000913 | 3   | 39    |
      | 10000914 | 2   | 29    |
    And customer presents tmd 'targettmd'
    And payment method is '<payment_method>'
    When order is placed
    Then order entries are :
      | product  | qty | basePrice | totalPrice |
      | 10000112 | 2   | 25        | 40.00      |
      | 10000122 | 3   | 30        | 72.00      |
      | 10000913 | 3   | 39        | 111.15     |
      | 10000914 | 2   | 29        | 55.10      |
    And Order is :
      | total  | subtotal | deliveryCost | totalTax |
      | 278.25 | 278.25   | 0.0          | 25.30    |
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000112 | 2   | 2500  | 2500      | staff            | 20              | 1000               |
      | 10000122 | 3   | 3000  | 3000      | staff            | 20              | 1800               |
      | 10000913 | 3   | 3900  | 3900      | staff            | 5               | 585                |
      | 10000914 | 2   | 2900  | 2900      | staff            | 5               | 290                |
    And tlog type is 'sale'
    And transDiscount entries are:
      | type  | code      | pct | amount |
      | staff | targettmd | 5   | 875    |
      | staff | targettmd | 20  | 2800   |
    And tlog total tender is 27825
    And Accertify feed has :
      | promotionAmount | targetStaffDiscountCard |
      | 36.75           | targettmd               |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an Order with Non-Target TMD Card with only apparal items
    Given a cart with entries:
      | product  | qty | price |
      | 10000211 | 1   | 10    |
      | 10000212 | 2   | 15    |
      | 10000111 | 3   | 8     |
    And customer presents tmd 'nontargettmd'
    And payment method is '<payment_method>'
    When order is placed
    Then order entries are :
      | product  | qty | basePrice | totalPrice |
      | 10000211 | 1   | 10        | 9.50       |
      | 10000212 | 2   | 15        | 28.50      |
      | 10000111 | 3   | 8         | 22.8       |
    And Order is :
      | total | subtotal | deliveryCost | totalTax |
      | 69.8  | 60.8     | 9.0          | 6.35     |
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000211 | 1   | 1000  | 1000      | staff            | 5               | 50                 |
      | 10000212 | 2   | 1500  | 1500      | staff            | 5               | 150                |
      | 10000111 | 3   | 800   | 800       | staff            | 5               | 120                |
    And tlog type is 'sale'
    And transDiscount entries are:
      | type  | code         | pct | amount |
      | staff | nontargettmd | 5   | 320    |
    And tlog total tender is 6980
    And Accertify feed has :
      | promotionAmount | targetStaffDiscountCard |
      | 3.20            | nontargettmd            |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an Order with Non-Target TMD Card with only non-apparal items
    Given a cart with entries:
      | product  | qty | price |
      | 10000912 | 3   | 12    |
      | 10000913 | 3   | 49    |
      | 10000914 | 3   | 29    |
    And customer presents tmd 'nontargettmd'
    And payment method is '<payment_method>'
    When order is placed
    Then order entries are :
      | product  | qty | basePrice | totalPrice |
      | 10000912 | 3   | 12        | 34.20      |
      | 10000913 | 3   | 49        | 139.65     |
      | 10000914 | 3   | 29        | 82.65      |
    And Order is :
      | total  | subtotal | deliveryCost | totalTax |
      | 256.50 | 256.50   | 0.0          | 23.32    |
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000912 | 3   | 1200  | 1200      | staff            | 5               | 180                |
      | 10000913 | 3   | 4900  | 4900      | staff            | 5               | 735                |
      | 10000914 | 3   | 2900  | 2900      | staff            | 5               | 435                |
    And tlog type is 'sale'
    And transDiscount entries are:
      | type  | code      | pct | amount |
      | staff | targettmd | 5   | 1350   |
    And tlog total tender is 25650
    And Accertify feed has :
      | promotionAmount | targetStaffDiscountCard |
      | 13.5            | nontargettmd            |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: Placing an Order with Non-Target TMD Card with both apparal and no-apparal items
    Given a cart with entries:
      | product  | qty | price |
      | 10000211 | 2   | 25    |
      | 10000212 | 3   | 30    |
      | 10001010 | 3   | 39    |
      | 10001020 | 2   | 29    |
    And customer presents tmd 'nontargettmd'
    And payment method is '<payment_method>'
    When order is placed
    Then order entries are :
      | product  | qty | basePrice | totalPrice |
      | 10000211 | 2   | 25        | 47.50      |
      | 10000212 | 3   | 30        | 85.50      |
      | 10001010 | 3   | 39        | 111.15     |
      | 10001020 | 2   | 29        | 55.10      |
    And Order is :
      | total  | subtotal | deliveryCost | totalTax |
      | 299.25 | 299.25   | 0.0          | 27.20    |
    And tlog entries are:
      | code     | qty | price | filePrice | itemDiscountType | itemDiscountPct | itemDiscountAmount |
      | 10000211 | 2   | 2500  | 2500      | staff            | 5               | 250                |
      | 10000212 | 3   | 3000  | 3000      | staff            | 5               | 450                |
      | 10001010 | 3   | 3900  | 3900      | staff            | 5               | 585                |
      | 10001020 | 2   | 2900  | 2900      | staff            | 5               | 290                |
    And tlog type is 'sale'
    And transDiscount entries are:
      | type  | code         | pct | amount |
      | staff | nontargettmd | 5   | 1575   |
    And tlog total tender is 29925
    And Accertify feed has :
      | promotionAmount | targetStaffDiscountCard |
      | 15.75           | nontargettmd            |

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario Outline: TMD card number in the hot card list are not applied to the order
    Given a cart with entries:
      | product  | qty | price |
      | 10000911 | 2   | 25    |
      | 10001030 | 3   | 30    |
    And customer presents tmd 'hottmdcard'
    And payment method is '<payment_method>'
    When order is placed
    Then no discount is applied to order

    Examples: 
      | payment_method |
      | creditcard     |
      | ipg            |

  Scenario: Fulfill an order with team member discount
    Given an order with tmd
    When Order is fulfilled
    Then order is:
      | orderStatus | consignmentStatus | invoice   |
      | Completed   | Shipped           | generated |

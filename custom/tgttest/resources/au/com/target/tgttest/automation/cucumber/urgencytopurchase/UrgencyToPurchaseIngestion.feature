@notAutomated
Feature: Ingest Favourite Interest, Cart Interest and Total Interests
  As a customer, I want to see the number of people who are interested in the product which I am looking at.

  Background: 
    Given Following configuration is set up in Target Shared Config:
      | configuration                       | value     | ruleAppliedDuringIngestion |
      | Minimum level of interest           |         5 | No                         |
      | Maximum displayed level of interest |        99 | No                         |
      | Minimum price threshold             |     20.00 | No                         |
      | Excluded Categories                 | Clearance | No                         |
      | Maximum for sale online period      |        90 | Yes                        |
      | Demonstration of interest period    |        10 | Yes                        |
      | Excluded Product Types              | Digital   | Yes                        |
    And Products with following data:
      | SKU    | baseProd | price | primaryCategory | productType | periodOnline | fav_10days | fav_11days | cart_10days | cart_11days | stock | comment                                   |
      | SKU101 | SKU100   | $25   | Womens          | normal      |           30 |         12 |         13 |          14 |          15 | Yes   | Single sellable variant                   |
      | SKU111 | SKU110   | $25   | Womens          | normal      |           30 |          3 |          5 |           4 |           6 | Yes   | Multiple sellable variants                |
      | SKU112 | SKU110   | $25   | Womens          | normal      |           30 |          5 |          7 |           6 |           8 | Yes   | Multiple sellable variants                |
      | SKU121 | SKU120   | $25   | Womens          | normal      |           30 |          1 |          3 |           2 |           4 | Yes   | Still ingested if low interest            |
      | SKU131 | SKU130   | $25   | Womens          | normal      |           30 |          0 |          1 |           0 |           0 | Yes   | Still ingested if no interest             |
      | SKU141 | SKU140   | $25   | Womens          | normal      |           30 |        110 |        111 |         120 |         121 | Yes   | Still ingested if high interest           |
      | SKU151 | SKU150   | $15   | Womens          | normal      |           30 |         12 |         13 |          14 |          15 | Yes   | Still ingested if low price               |
      | SKU161 | SKU160   | $15   | Womens          | normal      |           30 |          3 |          5 |           4 |           6 | Yes   | Price range on variants                   |
      | SKU162 | SKU160   | $25   | Womens          | normal      |           30 |          5 |          7 |           6 |           8 | Yes   | Price range on variants                   |
      | SKU171 | SKU170   | $25   | Clearance       | normal      |           30 |         12 |         13 |          14 |          15 | Yes   | Ingested even if Clearance                |
      | SKU181 | SKU180   | $25   | Womens          | normal      |           91 |         12 |         13 |          14 |          15 | Yes   | Ingested with 0s if online too long       |
      | SKU191 | SKU190   | $25   | Womens          | normal      |           91 |          3 |          5 |           4 |           6 | Yes   | One variant old, others not               |
      | SKU192 | SKU190   | $25   | Womens          | normal      |           30 |          5 |          7 |           6 |           8 | Yes   | One variant old, others not               |
      | SKU193 | SKU190   | $25   | Womens          | normal      |           30 |          7 |          9 |           8 |          10 | Yes   | One variant old, others not               |
      | SKU201 | SKU200   | $25   | Gift Cards      | digital     |           30 |         12 |         13 |          14 |          15 | Yes   | Ingested with 0s if digital product       |
      | SKU211 | SKU210   | $25   | Gift Cards      | digital     |           30 |          0 |          2 |           0 |           3 | Yes   | Ingested with 0s if all interest historic |
      | SKU221 | SKU220   | $25   | Womens          | normal      |           30 |          0 |          0 |           0 |           0 | Yes   | A variant with no interest                |
      | SKU222 | SKU220   | $25   | Womens          | normal      |           30 |          5 |          7 |           6 |           8 | Yes   | A variant with no interest                |
      | SKU231 | SKU230   | $25   | Womens          | normal      |           30 |          3 |          5 |           4 |           6 | No    | A variant with no stock                   |
      | SKU232 | SKU230   | $25   | Womens          | normal      |           30 |          5 |          7 |           6 |           8 | Yes   | A variant with no stock                   |
      | SKU241 | SKU240   | $25   | Womens          | normal      |           30 |          3 |          5 |           4 |           6 | No    | All variants no stock                     |
      | SKU242 | SKU240   | $25   | Womens          | normal      |           30 |          5 |          7 |           6 |           8 | No    | All variants no stock                     |

  Scenario Outline: Ingest urgency to purchase
    Given daily baseline is run
    When the data is analysed for product <sellableVariantProduct>
    Then the Endeca favouriteInterest value stored for the product is <favouriteInterest>
    And the Endeca cartInterest value stored for the product is <cartInterest>
    And the Endeca totalInterest value stored for the product is <totalInterest>

    Examples: 
      | sellableVariantProduct | favouriteInterest | cartInterest | totalInterest | comment                                   |
      | SKU101                 |                12 |           14 |            26 | Single sellable variant                   |
      | SKU111                 |                 8 |           10 |            18 | Multiple sellable variants                |
      | SKU112                 |                 8 |           10 |            18 | Multiple sellable variants                |
      | SKU121                 |                 1 |            2 |             3 | Still ingested if low interest            |
      | SKU131                 |                 0 |            0 |             0 | Still ingested if no interest             |
      | SKU141                 |               110 |          120 |           230 | Still ingested if high interest           |
      | SKU151                 |                12 |           14 |            26 | Still ingested if low price               |
      | SKU161                 |                 8 |           10 |            18 | Price range on variants                   |
      | SKU162                 |                 8 |           10 |            18 | Price range on variants                   |
      | SKU171                 |                12 |           14 |            26 | Ingested even if Clearance                |
      | SKU181                 |                 0 |            0 |             0 | Ingested with 0s if online too long       |
      | SKU191                 |                15 |           18 |            33 | One variant old, others not               |
      | SKU192                 |                15 |           18 |            33 | One variant old, others not               |
      | SKU193                 |                15 |           18 |            33 | One variant old, others not               |
      | SKU201                 |                 0 |            0 |             0 | Ingested with 0s if digital product       |
      | SKU211                 |                 0 |            0 |             0 | Ingested with 0s if all interest historic |
      | SKU221                 |                 0 |            0 |             0 | A variant with no interest                |
      | SKU222                 |                 5 |            6 |            11 | A variant with no interest                |
      | SKU231                 |                 8 |           10 |            18 | A variant with no stock                   |
      | SKU232                 |                 8 |           10 |            18 | A variant with no stock                   |
      | SKU241                 |                 8 |           10 |            18 | All variants no stock                     |
      | SKU242                 |                 8 |           10 |            18 | All variants no stock                     |

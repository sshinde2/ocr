@notAutomated
Feature: Display the total level of interest message in product display page
  As a Target customer, I want to see the number of people who are interested in the product that I'm viewing

  Background: 
    Given the endeca is ingested with the following data
      | baseProduct | sellableVariantProduct | favouriteInterest | cartInterest | totalInterest | instockFlag | price             | primaryCategory |
      | P100001     | 100001                 | 12                | 14           | 26            | 0           | 10.00             | Beauty          |
      | P100002     | 100002,100012          | 8                 | 10           | 18            | 1           | 10.00,15.00       | Tops            |
      | P100003     | 100003                 | 8                 | 10           | 18            | 1           | 29.00             | Home            |
      | P100004     | 100004,100014          | 11                | 20           | 31            | 1           | 30.00,30.00       | Clearance       |
      | P100005     | 100005                 | 0                 | 0            | 0             | 0           | 30.00             | Entertainment   |
      | P100006     | 100006,100016          | 110               | 120          | 230           | 1           | 25.00             | Toys            |
      | P100007     | 100007,100017,100027   | 12                | 14           | 26            | 1           | 10.00,20.00,36.00 | Kids            |
    And the configuration in hybris
      | configuration                        | value     |
      | urgencyToPurchase.minLevelOfInterest | 5         |
      | urgencyToPurchase.maxLevelOfInterest | 99        |
      | urgencyToPurchase.minPriceThreshold  | 20.00     |
      | urgencyToPurchase.excludedCategory   | Clearance |
    And the digital products in hybris
      | sellableVariant | productType | category   | price |
      | 100055          | Digital     | Gift Cards | 50.00 |
      | 100067          | Digital     | Gift Cards | 50.00 |
      | 100077          | Digital     | Gift Cards | 50.00 |

  Scenario Outline: Display of urgency to purchase message on the PDP
    Given the customer has navigated to the Product Details Page (PDP)
    And the customer is viewing the details of the product <productCode>
    When the PDP is displayed
    Then the  the urgency to purchase message is shown as <message>

    Examples: 
      | productCode | message                                                              |
      | 100001      | N/A                                                                  |
      | 100002      | N/A                                                                  |
      | 100012      | N/A                                                                  |
      | 100003      | 18 customers have this product in their favourites or cart right now |
      | P100003     | 18 customers have this product in their favourites or cart right now |
      | 100004      | N/A                                                                  |
      | 100014      | N/A                                                                  |
      | 100005      | N/A                                                                  |
      | 100006      | This is a top trending product                                       |
      | 100016      | This is a top trending product                                       |
      | P100006     | This is a top trending product                                       |
      | 100007      | 26 customers have this product in their favourites or cart right now |
      | 100017      | 26 customers have this product in their favourites or cart right now |
      | 100027      | 26 customers have this product in their favourites or cart right now |
      | P100007     | 26 customers have this product in their favourites or cart right now |
      | 100055      | N/A                                                                  |
      | 100067      | N/A                                                                  |
      | 100077      | N/A                                                                  |

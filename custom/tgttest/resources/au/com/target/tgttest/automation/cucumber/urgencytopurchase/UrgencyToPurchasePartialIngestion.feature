@notAutomated
Feature: In order to ingest Favourite Interest, Cart Interest and Total Interests in Partial
  As a customer, I want to update the number of people who are interested in the product which I am looking at.

  Background: 
    Given Following configuration is set up in Target Shared Config:
      | configuration                       | value     | ruleAppliedDuringIngestion |
      | Minimum level of interest           | 5         | No                         |
      | Maximum displayed level of interest | 99        | No                         |
      | Minimum price threshold             | 20.00     | No                         |
      | Excluded Categories                 | Clearance | No                         |
      | Maximum for sale online period      | 90        | Yes                        |
      | Demonstration of interest period    | 10        | Yes                        |
      | Excluded Product Types              | Digital   | Yes                        |
    And Products with following data:
      | ProductCode | BaseProduct | Price | PrimaryCategory | ProductType | PeriodOnline | Fav_10days | Fav_11days | Cart_10days | Cart_11days | Stock | comment                                   |
      | SKU101      | SKU100      | $25   | Womens          | normal      | 30           | 12         | 13         | 14          | 15          | Yes   | Single sellable variant                   |
      | SKU111      | SKU110      | $25   | Womens          | normal      | 30           | 3          | 5          | 4           | 6           | Yes   | Multiple sellable variants                |
      | SKU112      | SKU110      | $25   | Womens          | normal      | 30           | 5          | 7          | 6           | 8           | Yes   | Multiple sellable variants                |
      | SKU121      | SKU120      | $25   | Womens          | normal      | 30           | 1          | 3          | 2           | 4           | Yes   | Still ingested if low interest            |
      | SKU131      | SKU130      | $25   | Womens          | normal      | 30           | 0          | 1          | 0           | 0           | Yes   | Still ingested if no interest             |
      | SKU141      | SKU140      | $25   | Womens          | normal      | 30           | 110        | 111        | 120         | 121         | Yes   | Still ingested if high interest           |
      | SKU151      | SKU150      | $15   | Womens          | normal      | 30           | 12         | 13         | 14          | 15          | Yes   | Still ingested if low price               |
      | SKU156      | SKU155      | $10   | Womens          | normal      | 30           | 3          | 5          | 4           | 6           | Yes   | Low price range on variants               |
      | SKU157      | SKU155      | $12   | Womens          | normal      | 30           | 5          | 7          | 6           | 8           | Yes   | Low price range on variants               |
      | SKU161      | SKU160      | $15   | Womens          | normal      | 30           | 3          | 5          | 4           | 6           | Yes   | Price range on variants                   |
      | SKU162      | SKU160      | $25   | Womens          | normal      | 30           | 5          | 7          | 6           | 8           | Yes   | Price range on variants                   |
      | SKU171      | SKU170      | $25   | Clearance       | normal      | 30           | 12         | 13         | 14          | 15          | Yes   | Ingested even if Clearance                |
      | SKU181      | SKU180      | $25   | Womens          | normal      | 91           | 12         | 13         | 14          | 15          | Yes   | Ingested with 0s if online too long       |
      | SKU191      | SKU190      | $25   | Womens          | normal      | 91           | 3          | 5          | 4           | 6           | Yes   | One variant old, others not               |
      | SKU192      | SKU190      | $25   | Womens          | normal      | 30           | 5          | 7          | 6           | 8           | Yes   | One variant old, others not               |
      | SKU193      | SKU190      | $25   | Womens          | normal      | 30           | 7          | 9          | 8           | 10          | Yes   | One variant old, others not               |
      | SKU201      | SKU200      | $25   | Gift Cards      | digital     | 30           | 12         | 13         | 14          | 15          | Yes   | Ingested with 0s if digital product       |
      | SKU211      | SKU210      | $25   | Womens          | normal      | 30           | 0          | 2          | 0           | 3           | Yes   | Ingested with 0s if all interest historic |
      | SKU221      | SKU220      | $25   | Womens          | normal      | 30           | 0          | 0          | 0           | 0           | Yes   | A variant with no interest                |
      | SKU222      | SKU220      | $25   | Womens          | normal      | 30           | 5          | 7          | 6           | 8           | Yes   | A variant with no interest                |
      | SKU231      | SKU230      | $25   | Womens          | normal      | 30           | 3          | 5          | 4           | 6           | No    | A variant with no stock                   |
      | SKU232      | SKU230      | $25   | Womens          | normal      | 30           | 5          | 7          | 6           | 8           | Yes   | A variant with no stock                   |
      | SKU241      | SKU240      | $25   | Womens          | normal      | 30           | 3          | 5          | 4           | 6           | No    | All variants no stock                     |
      | SKU242      | SKU240      | $25   | Womens          | normal      | 30           | 5          | 7          | 6           | 8           | No    | All variants no stock                     |
      | SKU251      | SKU250      | $25   | Toys            | normal      | 30           | 12         | 13         | 14          | 15          | Yes   | CNP Drop Ship product                     |

  Scenario Outline: Run partial after some products are newly favourited
    Given baseline has previously been run, including the setting of urgency to purchase attributes
    And since the most recent baseline or partial, the following products have been favourited once each
      | favourited |
      | SKU101     |
      | SKU111     |
    When partial is run
    Then the products that are updated in endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest | totalInterest   | comment                    |
      | SKU101                 | 13 (up from 12)   | 14           | 27 (up from 26) | Single sellable variant    |
      | SKU111                 | 9 (up from 8)     | 10           | 19 (up from 18) | Multiple sellable variants |
      | SKU112                 | 9 (up from 8)     | 10           | 19 (up from 18) | Multiple sellable variants |

  Scenario Outline: Run partial after some products are removed from favourites
    Given baseline has previously been run, including the setting of urgency to purchase attributes
    And since the most recent baseline or partial, the following products have been un-favourited once each
      | un-favourited |
      | SKU101        |
      | SKU111        |
    When partial is run
    Then the products that are updated in endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest | totalInterest     | comment                    |
      | SKU101                 | 11 (down from 12) | 14           | 25 (down from 26) | Single sellable variant    |
      | SKU111                 | 7 (down from 8)   | 10           | 17 (down from 18) | Multiple sellable variants |
      | SKU112                 | 7 (down from 8)   | 10           | 17 (down from 18) | Multiple sellable variants |

  Scenario Outline: Run partial after some products are added to cart
    Given baseline has previously been run, including the setting of urgency to purchase attributes
    And since the most recent baseline or partial, the following products have been added to cart once each
      | addedToCart |
      | SKU101      |
      | SKU111      |
    When partial is run
    Then the products that are updated in endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest    | totalInterest   | comment                    |
      | SKU101                 | 12                | 15 (up from 14) | 27 (up from 26) | Single sellable variant    |
      | SKU111                 | 8                 | 11 (up from 10) | 19 (up from 18) | Multiple sellable variants |
      | SKU112                 | 8                 | 11 (up from 10) | 19 (up from 18) | Multiple sellable variants |

  Scenario Outline: Run partial after some products are removed from cart
    Given baseline has previously been run, including the setting of urgency to purchase attributes
    And since the most recent baseline or partial, the following products have been removed from cart once each
      | removedFromCart |
      | SKU101          |
      | SKU111          |
    When partial is run
    Then the products that are updated in endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest      | totalInterest     | comment                    |
      | SKU101                 | 12                | 13 (down from 14) | 25 (down from 26) | Single sellable variant    |
      | SKU111                 | 8                 | 9 (down from 10)  | 17 (down from 18) | Multiple sellable variants |
      | SKU112                 | 8                 | 9 (down from 10)  | 17 (down from 18) | Multiple sellable variants |

  Scenario Outline: Run partial after cart is transformed into order
    Given baseline has previously been run, including the setting of urgency to purchase attributes
    And since the most recent baseline or partial, the following cart has been converted into an order following successful purchase
      | cartConvertedToOrder |
      | SKU101               |
      | SKU111               |
    When partial is run
    Then the products that are updated in endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest      | totalInterest     | comment                    |
      | SKU101                 | 12                | 13 (down from 14) | 25 (down from 26) | Single sellable variant    |
      | SKU111                 | 8                 | 9 (down from 10)  | 17 (down from 18) | Multiple sellable variants |
      | SKU112                 | 8                 | 9 (down from 10)  | 17 (down from 18) | Multiple sellable variants |

  Scenario Outline: Run partial after a cart is removed as expired
    Given baseline has previously been run including the setting of urgency to purchase attributes
    And since the most recent baseline or partial the following cart has been purged due to being expired
      | cartPurgedAsExpired |
      | SKU101              |
      | SKU111              |
    When partial is run
    Then the only sellable products with updates to interest attributes in Endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest      | totalInterest     | comment                    |
      | SKU101                 | 12                | 13 (down from 14) | 25 (down from 26) | Single sellable variant    |
      | SKU111                 | 8                 | 9 (down from 10)  | 17 (down from 18) | Multiple sellable variants |
      | SKU112                 | 8                 | 9 (down from 10)  | 17 (down from 18) | Multiple sellable variants |

  Scenario Outline: Run partial after product has exceeded maximum for sale online period
    Given baseline has previously been run, including the setting of urgency to purchase attributes
    And since the most recent baseline or partial, the following products now exceed the maximum for sale online period
      | productExceedsMaxForSaleOnlinePeriod |
      | SKU101                               |
      | SKU111                               |
      | SKU112                               |
    When partial is run
    Then the only sellable products with updates to interest attributes in Endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest     | totalInterest    | comment                    |
      | SKU101                 | 0 (down from 12)  | 0 (down from 14) | 0 (down from 26) | Single sellable variant    |
      | SKU111                 | 0 (down from 8)   | 0 (down from 10) | 0 (down from 18) | Multiple sellable variants |
      | SKU112                 | 0 (down from 8)   | 0 (down from 10) | 0 (down from 18) | Multiple sellable variants |

  Scenario Outline: Run partial after some favourites on a product have become historic
    Given baseline has previously been run, including the setting of urgency to purchase attributes
    And since the most recent baseline or partial, the following products have had one favourite each go beyond the demonstration of interest period
      | favouriteGoBeyondInterestPeriod |
      | SKU101                          |
      | SKU111                          |
    When partial is run
    Then the only sellable products with updates to interest attributes in Endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest | totalInterest     | comment                    |
      | SKU101                 | 11 (down from 12) | 14           | 25 (down from 26) | Single sellable variant    |
      | SKU111                 | 7 (down from 8)   | 10           | 17 (down from 18) | Multiple sellable variants |
      | SKU112                 | 7 (down from 8)   | 10           | 17 (down from 18) | Multiple sellable variants |

  Scenario Outline: Run partial after some cart entries on a product have become historic
    Given baseline has previously been run, including the setting of urgency to purchase attributes
    And since the most recent baseline or partial, the following products have had one cart entry each go beyond the demonstration of interest period
      | cartEntryGoBeyondInterestPeriod |
      | SKU101                          |
      | SKU111                          |
    When partial is run
    Then the only sellable products with updates to interest attributes in Endeca are:
      | sellableVariantProduct | favouriteInterest | cartInterest      | totalInterest     | comment                    |
      | SKU101                 | 12                | 13 (down from 14) | 25 (down from 26) | Single sellable variant    |
      | SKU111                 | 8                 | 9 (down from 10)  | 17 (down from 18) | Multiple sellable variants |
      | SKU112                 | 8                 | 9 (down from 10)  | 17 (down from 18) | Multiple sellable variants |

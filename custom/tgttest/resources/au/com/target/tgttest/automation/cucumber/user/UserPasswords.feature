@registeredCustomers
Feature: Making passwords more secure
  As a customer
  I want my password to be even more secure

  Background: 
    Given the 'bcrypt.password.encoding' feature switch is enabled

  Scenario: new user created
    Given a user with uid 'john.smith@auto.test' does not exist
    When the user registers with the following details:
      | username             | password                 | firstName | lastName |
      | john.smith@auto.test | muuda6vdmbfrIyIhec4t4EG4 | John      | Smith    |
    Then the user password is encrypted with bcrypt

  Scenario: existing user with a password stored bcrypt signs in with correct password
    Given a user exists with the following details:
      | username             | password                 | passwordEncoding |
      | jane.smith@auto.test | N5pP0SL9BNk6adnB5fjjvlwG | bcrypt           |
    When the user signs in with the correct password
    Then the user is allowed to sign in
    And no attempt is made to update password encryption

  Scenario: existing user with a password stored bcrypt attempts to sign in with incorrect password
    Given a user exists with the following details:
      | username               | password                 | passwordEncoding |
      | john.citizen@auto.test | F7C3aX6attxNfArG2UHfZAv8 | bcrypt           |
    When the user attempts to sign in with an incorrect password
    Then the user is not allowed to sign in
    And no attempt is made to update password encryption

  Scenario: existing user with a password stored targetmd5 signs in with correct password
    Given a user exists with the following details:
      | username               | password                 | passwordEncoding |
      | jane.citizen@auto.test | BNVMwRQZ3tqK5xmHnc4ATPoz | targetmd5        |
    When the user signs in with the correct password
    Then the user is allowed to sign in
    And the user password is encrypted with bcrypt

  Scenario: existing user with a password stored targetmd5 attempts to sign in with incorrect password
    Given a user exists with the following details:
      | username           | password                 | passwordEncoding |
      | john.doe@auto.test | Y2qptS4Aq7zpRycPxPcghp2C | targetmd5        |
    When the user attempts to sign in with an incorrect password
    Then the user is not allowed to sign in
    And no attempt is made to update password encryption

  Scenario: user resets their password
    Given a user exists with the following details:
      | username           | password                 | passwordEncoding |
      | jane.doe@auto.test | ADr8Gb1zbX3LOhJWqbWPO6mQ | targetmd5        |
    And the user submits a password reset request
    When the user resets their password to 'lcgm4jQjOeP8So88XT6AXMMt'
    Then the user password is encrypted with bcrypt

  Scenario: user resets their password with same password with old password using md5 encryption
    Given a user exists with the following details:
      | username            | password                 | passwordEncoding |
      | ting.tong@auto.test | ADr8Gb1zbX3LOhJWqbWPO6mQ | targetmd5        |
    And the user submits a password reset request
    When the user resets their password to 'ADr8Gb1zbX3LOhJWqbWPO6mQ'
    Then the user is presented with a re-use same password error

  Scenario: user resets their password using same password as the old password which is based of bcrypt encryption
    Given a user exists with the following details:
      | username            | password                 | passwordEncoding |
      | ping.pong@auto.test | F7C3aX6attxNfArG2UHfZAv8 | bcrypt           |
    And the user submits a password reset request
    When the user resets their password to 'F7C3aX6attxNfArG2UHfZAv8'
    Then the user is presented with a re-use same password error

  Scenario: user with password stored with MD5 changes their password
    Given a user exists with the following details:
      | username           | password                 | passwordEncoding |
      | jane.doe@auto.test | ADr8Gb1zbX3LOhJWqbWPO6mQ | targetmd5        |
    When the user changes their password to 'lcgm4jQjOeP8So88XT6AXMMt'
    Then the user password is encrypted with bcrypt

  Scenario: user with password stored with bcrypt changes their password
    Given a user exists with the following details:
      | username               | password                 | passwordEncoding |
      | john.citizen@auto.test | F7C3aX6attxNfArG2UHfZAv8 | bcrypt           |
    When the user changes their password to 'lcgm4jQjOeP8So88XT6AXMMt'
    Then the user password is encrypted with bcrypt

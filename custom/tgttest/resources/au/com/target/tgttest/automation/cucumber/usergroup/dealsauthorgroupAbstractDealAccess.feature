@rollback @dealsauthor @toFix
Feature: Deals authors can only modify certain Deal fields

  # This is marked as toFix because I wasn't able to get the test working correctly. There are two issues:
  # - The dealsauthor user can't login without disabling search restrictions (due to no session catalog version), and
  # - The dealsauthor user shouldn't be able to modify then save the enabled flag, but when done as part of the test it can.

  Scenario: dealsauthor attempts to modify the featured flag
    Given user 'dealsauthor' exists in 'dealsauthorgroup' user group
    And deal with ID '9102' exists
    When the 'featured' value is set to 'true'
    Then the deal can be saved

  Scenario: dealsauthor attempts to modify the enabled flag
    Given user 'dealsauthor' exists in 'dealsauthorgroup' user group
    And deal with ID '9102' exists
    When the 'enabled' value is set to 'true'
    Then the deal can not be saved

@devsupport
Feature: New devsupport group to extend employee read only access to all datatypes and can login to hmc
  
  In order to restrict access
  As a admin I want to extends employee access to read only and login into hmc

  Scenario: Restrict access for devsupport group to read only
    Given user 'dev1' exists in 'devsupport' user group
    When access an existing customer record
    Then can read email and name
    And can not update name

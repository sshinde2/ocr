@notAutomated
Feature: Handle QAS call failed or unavailable when attempt to call

  Background: 
    Given single line address lookup feature is enabled

  Scenario Outline: Initial display of the Delivery Address page
    Given <entrySituation>
    When the "Delivery Address" page is displayed
    Then the single-line address is displayed as a blank field and Placeholder text: "eg. 10 Thompsons Road, North Geelong, VIC 3215"
    And the single line address field has label "Address"
    And the "Save Address" button is displayed.

    Examples: 
      | entrySituation                                                                                                        |
      | a guest user has chosen "Home delivery" in the Checkout and selected "Continue Securely >"                            |
      | a registered user with no saved address has chosen "Home delivery" in the Checkout and selected "Continue Securely >" |
      | a registered user with saved address(es) has chosen "Home delivery" and then chosen "Add new address" in the Checkout |
      | a registered user has chosen "Add new address" in "My Account"                                                        |

  Scenario: Hide Placeholder text when focus placed in empty single line address field
    Given the single line address field is displayed
    And the field is blank and has Placeholder text
    And the field does NOT have the focus
    When the field is given the focus
    Then the Placeholder text is removed

  Scenario: Display Placeholder text when lose focus from empty single line address field
    Given the single line address field is displayed
    And the field is blank
    And the field has the focus
    When the field loses the focus
    Then the Placeholder text is displayed

  Scenario: Leave typing in single line address field when it has >=4 characters
    Given the single line address field is displayed
    And the field has the focus
    And the field has >=4 characters
    When the field loses the focus
    Then make a pretend unsuccessful QAS call
    And if the address selection box is already displayed, leave it displayed.
    And if the address selection box is NOT displayed, display it.
    And display the message "Unable to verify address."
    And include a "Manually enter my address" link below the message.

  Scenario: Stop typing in single line address field when it has >=4 characters
    Given the single line address field is displayed
    And the field has the focus
    And the field has >=4 characters
    When the user stops entering or removing characters for 0.n seconds (n to be determined)
    Then make a pretend unsuccessful QAS call
    And if the address selection box is already displayed, leave it displayed.
    And if the address selection box is NOT displayed, display it.
    And display the message "Unable to verify address."
    And include a "Manually enter my address" link below the message.

  Scenario: Leave or stop typing in single line address field when it has <4 characters
    Given the single line address field is displayed
    And the field has the focus
    And the field has <4 characters
    When the field loses the focus
    Then if the address selection box is displayed, hide it.

  Scenario: Leave or stop typing in single line address field when it has <4 characters
    Given the single line address field is displayed
    And the field has the focus
    And the field has <4 characters
    When the user stops entering or removing characters for 0.n seconds (n to be determined)
    Then if the address selection box is displayed, hide it.

  Scenario: Choose the "Manually enter my address" link
    Given the single line address field is displayed
    And the address selection box has been displayed, including the "Unable to find address" link
    When the user chooses the "Manually enter my address" link
    Then hide the single line address field
    And hide the address selection box
    And display the separate fields for Address line 1, Address line 2, Suburb, State and Postcode
    And display all of those fields as blank
    And provide a "Return to address lookup" link

  Scenario: Choose the "Return to address lookup" link
    Given the separate fields for Address line 1, Address line 2, Suburb, State and Postcode are displayed
    And the "Return to address lookup" link is displayed
    When the user chooses the "Return to address lookup" link
    Then hide the separate fields for Address line 1, Address line 2, Suburb, State and Postcode.
    And hide the "Return to address lookup" link.
    And display the single line address field.
    And display that field as blank with Placeholder text.

@notAutomated
Feature: Single line address lookup is disabled.

  Background: 
    Given the QAS feature is turned off

  Scenario Outline: Add New Address
    Given <entrySituation>
    When the "Delivery Address" page is displayed
    Then display the separate fields for Address line 1, Address line 2, Suburb, State and Postcode
    And display all of those fields as blank
    And DON'T provide a "Return to address lookup" link
    And display the "Save address" button.

    Examples: 
      | entrySituation                                                                                                        |
      | a guest user has chosen "Home delivery" in the Checkout and selected "Continue Securely >"                            |
      | a registered user with no saved address has chosen "Home delivery" in the Checkout and selected "Continue Securely >" |
      | a registered user with saved address(es) has chosen "Home delivery" and then chosen "Add new address" in the Checkout |
      | a registered user has chosen "Add new address" in "My Account"                                                        |

  Scenario: Save Address
    Given the user is presented with the separated address fields
    And the single line address field is NOT able to be displayed
    When the user chooses to "Save Address"
    And has entered valid values in all the fields
    # No change to current behaviour
    Then the address validation modal pop-up is displayed

@notAutomated
Feature: Handle "Save Address" including silent validation and no longer showing the validation pop-up

  Background: 
    Given single line address lookup feature is enabled

  Scenario Outline: QAS feature is on
    Given the user is on the <page> page
    And the delivery mode selected is <deliveryMode>
    And the delivery address post code is <expressPostCode>
    And the values in all the separated address fields are <fieldValues>
    And the address field(s) currently displayed is/are the <fieldsDisplayed>
    When the user chooses to "Save Address"
    Then the separated address fields are validated
    And the error displayed is <errorDisplayed>
    And the address is <outcome>
    And the page displayed is <pageDisplayed>

    Examples: 
      | page                      | deliveryMode    | expressPostCode | fieldValues | fieldsDisplayed     | errorDisplayed          | outcome   | pageDisplayed                            |
      | Checkout Delivery Address | Home            | N/A             | valid       | separated fields    | none                    | saved     | Payment Details                          |
      | Checkout Delivery Address | Express         | supported       | valid       | separated fields    | none                    | saved     | Payment Details                          |
      | Checkout Delivery Address | Express         | not supported   | valid       | separated fields    | Post code not supported | not saved | unchanged                                |
      | Checkout Delivery Address | Home or Express | N/A             | not valid   | separated fields    | field level error(s)    | not saved | unchanged                                |
      | Checkout Delivery Address | Home or Express | N/A             | not valid   | single line address | field level error(s)    | not saved | unchanged, with separated address fields |
      | My Account                | N/A             | N/A             | valid       | separated fields    | none                    | saved     | My Addresses list                        |
      | My Account                | N/A             | N/A             | not valid   | separated fields    | field level error(s)    | not saved | unchanged                                |
      | My Account                | N/A             | N/A             | not valid   | single line address | field level error(s)    | not saved | unchanged, with separated address fields |

  # Note: It is not possible to have valid field values in the case when the single line address field is displayed. This is because the fields are cleared if the user chooses to "Return to address lookup"
  Scenario Outline: 3. Silent QAS call when save address to record address verification status
    Given the QAS feature is turned on
    And the user is on the <page> page
    And the address entered in the separated fields <addressStatus>
    When the user chooses "Save Address"
    And the address fields are validated successfully (including Postcode check if Express)
    Then a silent QAS call is made to verify the address being saved
    And the system sets the address verified flag to <flagSetting>

    Examples: 
      | page                      | addressStatus | flagSetting  |
      | Checkout Delivery Address | exists in QAS | verified     |
      | Checkout Delivery Address | is not in QAS | not verified |
      | My Account                | exists in QAS | verified     |
      | My Account                | is not in QAS | not verified |

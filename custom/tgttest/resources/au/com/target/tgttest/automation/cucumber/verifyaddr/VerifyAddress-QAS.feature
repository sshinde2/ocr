@notAutomated
Feature: Provide single-line delivery address look-up on the Check-out and My Account, new address case

  Background: 
    Given single line address lookup feature is enabled
    And the "Delivery Address" page is displayed with no pre-selected address
    And the single-line address is displayed as a blank field and Placeholder text: "eg. 10 Thompsons Road, North Geelong, VIC 3215"

  Scenario: Leave or stop typing in single line address field when it has <4 characters - don't call QAS
    Given the single line address field is displayed
    And the field has the focus
    And the field has <4 characters
    When the field loses the focus Or the user stops entering or removing characters for 0.n seconds (n to be determined)
    Then any existing QAS call is cancelled
    And if the address selection box is displayed, hide it.

  Scenario: Leave or stop typing in single line address field when it has >=4 characters - call QAS
    Given the single line address field is displayed
    And the field has the focus
    And the field has >=4 characters
    When the field loses the focus Or the user stops entering or removing characters for 0.n seconds (n to be determined)
    Then any existing QAS call is cancelled
    And a new QAS call is made
    And the processing spinner is displayed at the right hand end of the field
    And if the address selection box is already displayed, leave it displayed.

  Scenario Outline: Handle response from QAS call
    Given a QAS call has been made and not cancelled
    And the single line address field is still displayed
    When the QAS response has been received as <response>
    Then the address selection box is <boxDisplay>
    And the address selection box contains <boxContains>
    And the "Unable to verify address" message is <unableMessage>
    And the "Manually enter my address" link is <manualMessage>
    And the processing spinner is hidden
    And the contents of the single line address field is not changed

    Examples: 
      | response          | boxDisplay | boxContains  | unableMessage | manualMessage | comment                   |
      | 1 result          | displayed  | 1 address    | not displayed | displayed     | Single result             |
      | 10 results        | displayed  | 10 addresses | not displayed | displayed     | 10 results                |
      | max results       | displayed  | 10 addresses | not displayed | displayed     | > 10  results             |
      | >max results      | unchanged  | unchanged    | unchanged     | unchanged     | Too many results          |
      | 0 results         | displayed  | no addresses | displayed     | displayed     | No addresses found        |
      | QAS not available | displayed  | no addresses | displayed     | displayed     | Integration not available |
      | QAS error         | displayed  | no addresses | displayed     | displayed     | Integration error         |

  Scenario: Select a listed address
    Given the address selection box is displayed
    And 1 or more addresses are listed
    When the user selects one of the listed addresses
    Then hide the single line address field
    And cancel any existing QAS call
    And hide the address selection box
    And display the address split across separate fields for Address, Address line 2, Suburb, State and Postcode.
    And populate those fields with details from the selected address
    And provide a "Return to address lookup" option

  Scenario: Choose the "Manually enter my address" link
    Given the single line address field is displayed
    And the address selection box has been displayed, including the "Manually enter my address" link
    When the user chooses the "Manually enter my address" link
    Then hide the single line address field
    And cancel any existing QAS call
    And hide the address selection box
    And display the separate fields for Address line 1, Address line 2, Suburb, State and Postcode
    And display all of those fields as blank (even if they had previously been populated)
    And provide a "Return to address lookup" link

  Scenario: Choose the "Return to address lookup" link
    Given the separate fields for Address line 1, Address line 2, Suburb, State and Postcode are displayed
    And the "Return to address lookup" link is displayed
    When the user chooses the "Return to address lookup" link
    Then hide the separate fields for Address line 1, Address line 2, Suburb, State and Postcode
    And clear the values in the (no longer visible) separate address fields
    And hide the "Return to address lookup" link
    And display the single line address field
    And display that field as blank with Placeholder text

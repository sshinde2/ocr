@esbPriceUpdate @cartProductData
Feature: Was now prices - Capture previous permanent prices 
  In order to build-up recent product price history
  As Online Operations
  I want to capture product prices to build-up recent product price history
  
  Previous permanent prices are captured for products on runs of POS price update.
  
  Background: 
    Given 'approved' status in hybris for variant '10000011'

  Scenario: Initial permanent price update
    Given that a previous permanent price record does not exist for product '10000011'
    And POS updates price for product '10000011':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 10        | 10        | false      |
    When Hybris POS price update runs
    Then a previous permanent price record is created:
      | VariantCode | StartDate | EndDate | Price |
      | 10000011    | now       | null    | 10    |

  Scenario: Initial promo price update
    Given that a previous permanent price record does not exist for product '10000011'
    And POS updates price for product '10000011':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 10        | 10        | true       |
    When Hybris POS price update runs
    Then a previous permanent price is not created for product '10000011'

  Scenario: First time price update with future permanent price
    Given that a previous permanent price record does not exist for product '10000011'
    And POS updates price for product '10000011':
      | Date               | SellPrice | PermPrice | promoEvent |
      | null               | 10        | 10        | false      |
      | day after tomorrow | 8         | 8         | false      |
    When Hybris POS price update runs
    Then a previous permanent price record is created:
      | VariantCode | StartDate | EndDate | Price |
      | 10000011    | now       | null    | 10    |

  Scenario: Price update with new permanent price
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | null    | 10    |
    And POS updates price for product '10000011':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 8         | 8         | false      |
    When Hybris POS price update runs
    Then previous permanent price record is updated with:
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | now     | 10    |
      | 10000011    | now        | null    | 8     |

  Scenario: Banned product ends the previous permanent price
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | null    | 10    |
    And POS updates price for product '10000011':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 10        | 10        | false      |
    And POS sets the product as 'banned'
    When Hybris POS price update runs
    Then previous permanent price record is updated with:
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | now     | 10    |

  Scenario: Permanent price same as existing previous price value
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | null    | 10    |
    And POS updates price for product '10000011':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 10        | 10        | false      |
    When Hybris POS price update runs
    Then a previous permanent price record is created:
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | null    | 10    |

  Scenario: Current price is promotional price, un-ended previous permanent price exists
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | null    | 10    |
    And POS updates price for product '10000011':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 8         | 8         | true       |
    When Hybris POS price update runs
    Then previous permanent price record is updated with:
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | now     | 10    |

  Scenario: Product in hybris has a status that is unapproved
    Given that a previous permanent price record does not exist for product '10000011'
    And 'unapproved' status in hybris for variant '10000011'
    And POS updates price for product '10000011':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 8         | 8         | false      |
    When Hybris POS price update runs
    Then a previous permanent price is not created for product '10000011'

  Scenario: Product in hybris has a status that is check
    Given that a previous permanent price record does not exist for product '10000011'
    And 'check' status in hybris for variant '10000011'
    And POS updates price for product '10000011':
      | Date | SellPrice | PermPrice | promoEvent |
      | null | 8         | 8         | false      |
    When Hybris POS price update runs
    Then a previous permanent price is not created for product '10000011'

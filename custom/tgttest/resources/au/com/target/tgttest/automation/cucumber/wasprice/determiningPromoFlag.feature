@notAutomated
Feature: Was now prices - Promotional event flag passed from POS to Hybris by ESB
  In order to update a new TargetPriceRow promoEvent flag depending on promotional markdowns from POS
  As Online Operations
  I want to update Fuse price update process

  Note:
  This will be driven from the POS price flags promo, global
  This functionality is in ESB
  
  Scenario Outline: Promotional markdown from POS
		Given POS price update for product with promotional flag <Promo> global flag <Global>
		When POS price update runs
		Then product promoevent flag is <promoEvent>

		Examples:
		| Promo | Global | promoEvent |
		| false | false  | false      |
		| true  | false  | true       |
		| false | true   | true       |
		| true  | true   | true       |





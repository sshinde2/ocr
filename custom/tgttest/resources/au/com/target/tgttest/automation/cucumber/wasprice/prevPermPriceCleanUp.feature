Feature: Was now prices - Automatic previous permanent price clean-up job for permanent price history table
  
  delete records which have ended but are too short in duration - date range is shorter then 14 days.
  delete records with end date older the 14 or more days ago
  don't delete records which are not ended.
  
  NOTE: Due to the Endeca index job running only once a day, we will reduce the number of days product is available at a price during which we can display the was price to 13 or less

  Background: 
    Given price has to be advertised continuously for 14 or more days in order to qualify to be shown as was price.
    And the ACCC number of days product is available at a price during which we can display the was price is 14 or less
    And initially there are no previous permanent price records

  Scenario: A previous permanent price record that is no longer required because it ended too long ago
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate    | EndDate                    | Price |
      | 10000011    | 100 days ago | 14 days ago minus 1 minute | 10    |
    When the previous permanent price clean-up job is run
    Then the previous permanent price record is deleted

  Scenario: A previous permanent price record that is still required
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000011    | 18 days ago | 2 days ago | 10    |
    When the previous permanent price clean-up job is run
    Then the previous permanent price record is not deleted

  Scenario: A previous permanent price record that is still required
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate    | EndDate                   | Price |
      | 10000011    | 100 days ago | 14 days ago plus 1 minute | 10    |
    When the previous permanent price clean-up job is run
    Then the previous permanent price record is not deleted

  Scenario: A previous permanent price record that is no longer required because didn't last long enough
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate   | EndDate                   | Price |
      | 10000011    | 16 days ago | 2 days ago minus 1 minute | 10    |
    When the previous permanent price clean-up job is run
    Then the previous permanent price record is deleted

  Scenario: A previous permanent price record that has not ended
    Given that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 2 days ago | Not set | 10    |
    When the previous permanent price clean-up job is run
    Then the previous permanent price record is not deleted

@cartProductData @adminUser
Feature: Was now prices - Update previous permanent price when product status updated from STEP
  In order compliance with price guidelines
  As Online Operations
  I want to update previous permanent price when product becomes inactive

  Scenario: Setting product to inactive through STEP
    Given 'approved' status in hybris for variant '10000011'
    And that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | null    | 10    |
    And product '10000011' is set to 'inactive' in STEP
    When STEP product import runs and updates approval status
    Then previous permanent price record is updated with:
      | VariantCode | StartDate  | EndDate | Price |
      | 10000011    | 5 days ago | today   | 10    |

  Scenario: Setting product to active through STEP with a current permanent price
    Given 'unapproved' status in hybris for variant '10000011'
    And that a previous permanent price record does not exist for product '10000011'
    And product '10000011' has current price
      | Date      | sellPrice | promoEvent |
      | 1 day ago | 10        | false      |
    And product '10000011' is set to 'active' in STEP
    When STEP product import runs and updates approval status
    Then a previous permanent price record is created:
      | VariantCode | StartDate | EndDate | Price |
      | 10000011    | today     | null    | 10    |

  Scenario: Setting product to active through STEP with a current permanent price and a previous permanent price
    Given 'unapproved' status in hybris for variant '10000011'
    And that a previous permanent price record exists for product '10000011'
      | VariantCode | StartDate  | EndDate   | Price |
      | 10000011    | 5 days ago | 1 day ago | 10    |
    And product '10000011' has current price
      | Date      | sellPrice | promoEvent |
      | 1 day ago | 10        | false      |
    And product '10000011' is set to 'active' in STEP
    When STEP product import runs and updates approval status
    Then a previous permanent price record is created:
      | VariantCode | StartDate | EndDate | Price |
      | 10000011    | today     | null    | 10    |

  Scenario: Setting product to active through STEP with a current promotional price
    Given 'unapproved' status in hybris for variant '10000011'
    And that a previous permanent price record does not exist for product '10000011'
    And product '10000011' has current price
      | Date      | sellPrice | promoEvent |
      | 1 day ago | 10        | true       |
    And product '10000011' is set to 'active' in STEP
    When STEP product import runs and updates approval status
    Then a previous permanent price is not created for product '10000011'

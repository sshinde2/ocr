@cartProductData @adminUser
Feature: Was now prices - Product was prices on PDP
  In order increase sales by providing better visibility on the price with accordance to the ACCC
  As Marketing
  I want to show was price on product details page

  Background: 
    Given price has to be advertised continuously for 14 or more days in order to qualify to be shown as was price.
    And the ACCC number of days product is available at a price during which we can display the was price is 13 or less

  Scenario: Previous permanent price was advertised for 14 days or more
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate | Price |
      | 10000121    | 15 days ago | today   | 20    |
      | 10000121    | today       | null    | 15    |
    And product '10000121' has current price
      | Date  | sellPrice | promoEvent |
      | today | 15        | false      |
    When product details are requested
    Then price details:
      | was price | now price |
      | 20        | 15        |

  Scenario: Previous permanent price was advertised for 14 days or more and the current price is advertised for less than 13 days
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate     | Price |
      | 10000121    | 30 days ago | 12 days ago | 20    |
      | 10000121    | 12 days ago | null        | 15    |
    And product '10000121' has current price
      | Date  | sellPrice | promoEvent |
      | today | 15        | false      |
    When product details are requested
    Then price details:
      | was price | now price |
      | 20        | 15        |

  Scenario: Previous permanent price was advertised for less then 14 days
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate | Price |
      | 10000121    | 12 days ago | today   | 20    |
      | 10000121    | today       | null    | 15    |
    And product '10000121' has current price
      | Date  | sellPrice | promoEvent |
      | today | 15        | false      |
    When product details are requested
    Then price details:
      | was price | now price |
      |           | 15        |

  Scenario: Previous permanent price advertised for less then 14 days and Current permanent price advertised for 13 days or more
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate     | Price |
      | 10000121    | 20 days ago | 13 days ago | 20    |
      | 10000121    | 13 days ago | null        | 15    |
    And product '10000121' has current price
      | Date        | sellPrice | promoEvent |
      | 13 days ago | 15        | false      |
    When product details are requested
    Then price details:
      | was price | now price |
      |           | 15        |

  Scenario: Previous permanent price advertised for 14 days or more and Current permanent price advertised for 13 days or more
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate     | Price |
      | 10000121    | 30 days ago | 14 days ago | 20    |
      | 10000121    | 14 days ago | null        | 15    |
    And product '10000121' has current price
      | Date        | sellPrice | promoEvent |
      | 14 days ago | 15        | false      |
    When product details are requested
    Then price details:
      | was price | now price |
      |           | 15        |

  Scenario: Previous permanent price advertised for 14 days or more and Current permanent price is a promotional price
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate     | Price |
      | 10000121    | 30 days ago | 13 days ago | 20    |
    And product '10000121' has current price
      | Date        | sellPrice | permPrice | promoEvent |
      | 13 days ago | 15        |           | true       |
    When product details are requested
    Then price details:
      | was price | now price |
      |           | 15        |

  Scenario: Promotional price update with a WAS price
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000121    | 20 days ago | 2 days ago | 20    |
    And product '10000121' has current price
      | Date        | sellPrice | permPrice | promoEvent |
      | 13 days ago | 15        | 20        | true       |
    When product details are requested
    Then price details:
      | was price | now price |
      | 20        | 15        |

  Scenario: Promotional price update with a WAS price and without promoEvent
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000121    | 20 days ago | 2 days ago | 20    |
    And product '10000121' has current price
      | Date        | sellPrice | permPrice | promoEvent |
      | 13 days ago | 15        | 20        |            |
    When product details are requested
    Then price details:
      | was price | now price |
      | 20        | 15        |

  Scenario: Promotional price update with no WAS price
    Given that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000121    | 20 days ago | 2 days ago | 25    |
    And product '10000121' has current price
      | Date        | sellPrice | permPrice | promoEvent |
      | 13 days ago | 15        |           | true       |
    When product details are requested
    Then price details:
      | was price | now price |
      |           | 15        |

  Scenario: Was price range is invalid due to previous permanent price not meeting the criteria and current price being a promotional price
    Given a colour variant product '10000120' has size variants:
      | VariantCode |
      | 10000121    |
      | 10000122    |
    And that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate     | Price |
      | 10000121    | 20 days ago | 15 days ago | 35    |
      | 10000121    | 15 days ago | null        | 26    |
    And product '10000121' has current price
      | Date        | sellPrice | promoEvent |
      | 15 days ago | 26        | false      |
    And that a previous permanent price record exists for product '10000122'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000122    | 20 days ago | 2 days ago | 46    |
    And product '10000122' has current price
      | Date        | sellPrice | permPrice | promoEvent |
      | 13 days ago | 15        |           | true       |
    When product details are requested for '10000120'
    Then price details:
      | from price | to price | from was price | to was price | was price |
      | 15         | 26       |                |              |           |

  Scenario: Was price range for all variants is invalid due to one variant without wasprice
    Given a colour variant product '10000120' has size variants:
      | VariantCode |
      | 10000121    |
      | 10000122    |
    And that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000121    | 20 days ago | 2 days ago | 35    |
      | 10000121    | 2 days ago  | null       | 35    |
    And product '10000121' has current price
      | Date       | sellPrice | promoEvent |
      | 2 days ago | 35        | false      |
    And that a previous permanent price record exists for product '10000122'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000122    | 20 days ago | 2 days ago | 46    |
    And product '10000122' has current price
      | Date        | sellPrice | permPrice | promoEvent |
      | 13 days ago | 15        |           | true       |
    When product details are requested for '10000120'
    Then price details:
      | from price | to price | from was price | to was price | was price |
      | 15         | 35       |                |              |           |

  Scenario: Was price range is valid for all variant products both have valid was price
    Given a colour variant product '10000120' has size variants:
      | VariantCode |
      | 10000121    |
      | 10000122    |
    And that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000121    | 20 days ago | 2 days ago | 35    |
      | 10000121    | 2 days ago  | null       | 33    |
    And product '10000121' has current price
      | Date       | sellPrice | promoEvent |
      | 2 days ago | 33        | false      |
    And that a previous permanent price record exists for product '10000122'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000122    | 20 days ago | 2 days ago | 46    |
      | 10000122    | 2 days ago  | null       | 20    |
    And product '10000122' has current price
      | Date       | sellPrice | promoEvent |
      | 2 days ago | 20        | false      |
    When product details are requested for '10000120'
    Then price details:
      | from price | to price | from was price | to was price |
      | 20         | 33       | 35             | 46           |

  Scenario: Was price range are valid for all variant products one is from previous perm price, another one is from target price row
    Given a colour variant product '10000120' has size variants:
      | VariantCode |
      | 10000121    |
      | 10000122    |
    And that a previous permanent price record exists for product '10000121'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000121    | 20 days ago | 2 days ago | 35    |
      | 10000121    | 2 days ago  | null       | 26    |
    And product '10000121' has current price
      | Date       | sellPrice | promoEvent |
      | 2 days ago | 26        | false      |
    And that a previous permanent price record exists for product '10000122'
      | VariantCode | StartDate   | EndDate    | Price |
      | 10000122    | 20 days ago | 2 days ago | 46    |
    And product '10000122' has current price
      | Date       | sellPrice | permPrice | promoEvent |
      | 1 days ago | 15        | 20        | true       |
    When product details are requested for '10000120'
    Then price details:
      | from price | to price | from was price | to was price |
      | 15         | 26       | 20             | 35           |

Feature: Fulfilment of orders received from channel advisor
  As a vendor for different marketplaces
  I want to notify order being shipped
  So that I can provide update to various marketplaces.

  Scenario Outline: Fulfilments at Fastline
    Given an <saleschannel> '<deliveryType>' webmethods order
    When the consignment is '<pickedupStatus>' by fastline
    And fastline pick process completes
    And ship conf is received from fastline
    Then order complete process completes
    And consignment status is 'SHIPPED'
    And Shipment notification is '<sentStatus>' to <saleschannel>
    And carrier code used in notification '<carrierCode>'
    And carrier class used in notification '<carrierClass>'
    And the CnC extract is '<cncOutcome>' with parcel count <parcelCount>

    Examples: 
      | saleschannel | deliveryType     | pickedupStatus   | sentStatus | cncOutcome  | parcelCount | carrierCode    | carrierClass |
      | eBay         | express delivery | full picked      | sent       | not created |           0 | StarTrack      | EXP          |
      | eBay         | delivery         | full picked      | sent       | not created |           0 | Australia Post | STD          |
      | eBay         | pickup           | full picked      | not sent   | created     |           1 | N/A            | N/A          |
      | eBay         | express delivery | partially picked | sent       | not created |           0 | StrarTrack     | EXP          |
      | eBay         | delivery         | partially picked | sent       | not created |           0 | Australia Post | STD          |
      | eBay         | pickup           | partially picked | not sent   | created     |           1 | N/A            | N/A          |
      | TradeMe      | delivery         | full picked      | sent       | not created |           0 | Australia Post | STD          |

  Scenario Outline: Fulfil Trademe orders at Fastline.
    Given stores with fulfilment capability and stock:
      | store  | inStock | state | instoreEnabled | allowDeliveryToSameStore | allowDeliveryToAnotherStore | deliveryModesAllowed |
      | Robina | Yes     | QLD   | Yes            | Yes                      | Yes                         | home-delivery        |
    And a new order in webmethods
    And webmethods order with entries:
      | product  | qty | price |
      | 10000011 |   5 |    20 |
      | 10000012 |   3 |    10 |
    And payment type is '<paymentType>'
    And currency as '<currency>'
    And country as '<country>'
    And global order routing is Enabled
    And global blackout period is Not in Effect
    And delivery mode is 'home-delivery'
    And the delivery address is '19,Robina Town Centre Drive,Robina,QLD,4226'
    And delivery modes allowed for instore fulfilment are 'home-delivery'
    And global max items per instore fulfillment order is 10
    And sales channel as '<salesChannel>'
    When the order is imported in hybris
    Then a new order in hybris is created
    And a single consignment is created
    And the assigned warehouse will be 'Fastline'

    Examples: 
      | paymentType | currency | country | salesChannel | comment                                                   |
      | PayPal      | AUD      | AU      | eBay         | eBay orders are NOT assessed for in-store fulfillment     |
      | TradeMe     | NZD      | NZ      | TradeMe      | Trade Me orders are NOT assessed for in-store fulfillment |

  Scenario: Zero pick for TradeMe order.
    Given a TradeMe order in webmethods
    When the order is zero picked
    Then the order status is Cancelled
    And consignment status is 'Cancelled'
    And assigned cockpit group is 'trademe-csagentgroup'

  Scenario: Zero pick for eBay order.
    Given an eBay delivery order in webmethods
    When the order is zero picked
    Then the order status is Cancelled
    And consignment status is 'Cancelled'
    And assigned cockpit group is 'ebay-csagentgroup'

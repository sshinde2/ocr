@notAutomated
Feature: Adding favourites to basket from the favourites page
  
  In order to give the customer better experience and to increase  the saless
  As a Target Online store
  I want the customer to provide the ability  to add the products from favourites to basket

  Scenario Outline: 
    Given the customer is on the target.com.au
    And customer has favourited the product <product> at <favouriteLevel> level
    And the stock level for the product is <stockstatus>
    And the selected variant  is <sellableVariantFlag> sellable variant
    And the baseProduct of the favourite does have <onlySellableVariantFlag> one sellable variant
    When the customer views that item on the favourites page
    Then the favourite item displayed has <colour> <size>
    And the add to basket button is <addToBasketEnabled>

    Examples: 
      | productCode    | productName         | stockStatus | favouriteLevel | selectedColour | selectedSize | addToBasketEnabled | sellableVariantFlag | onlySellableVariantFlag |
      | P1000          | Long Sleeve Shirt   | Available   | baseProduct    |                |              | Disabled           | Not                 | Not                     |
      | P1000_Blue     | Long Sleeve Shirt   | Available   | colorVariant   | Blue           |              | Disabled           | Not                 | Not                     |
      | P1000_Purple_S | Long Sleeve Shirt   | Available   | sizeVariant    | Purple         | S            | Enabled            |                     | Not                     |
      | P1000_Red_S    | Long Sleeve Shirt   | Unavailable | sizeVariant    | Red            | S            | Disabled           |                     | Not                     |
      | W224512        | Grandeur Towel      | Available   | baseProduct    |                |              | Disabled           |                     | Not                     |
      | 58483565       | Grandeur Towel      | Available   | colorVariant   | Berry          |              | Enabled            |                     | Not                     |
      | 58483564       | Grandeur Towel      | Unavailable | colorVariant   | Red            |              | Disabled           |                     | Not                     |
      | P5848350       | Fit and Flare dress | Available   | baseProduct    |                |              | Disabled           | Not                 | Not                     |
      | 58483501       | Fit and Flare dress | Available   | colorVariant   | No Colour      |              | Disabled           | Not                 | Not                     |
      | 58483502       | Fit and Flare dress | Available   | sizeVariant    | No Colour      | 12           | Enabled            |                     | Not                     |
      | 58483503       | Fit and Flare dress | Unavailable | sizeVariant    | No Colour      | 10           | Disabled           | Not                 | Not                     |
      | P58305430      | Star wars           | Available   | baseProduct    | No colour      |              | Enabled            | Not                 |                         |
      | 58305430       | Star wars           | Available   | colorVariant   | No colour      |              | Enabled            |                     |                         |
      | P57527789      | Frozen Karoke       | Unavailable | baseproduct    | No colour      |              | Disabled           | Not                 |                         |
      | 57527789       | Frozen Karoke       | Unavailable | colorVariant   | No colour      |              | Disabled           |                     |                         |

  Scenario: Add to basket successfully where have product availability
    Given the add to basket button is enabled in favourites page for the product
    And there is stock availability of the sellable variant
    When the user chooses to add to basket
    And the add to basket is successful
    Then a single item of the sellable variant is added to the basket
    And a message fades in and disappears after 2 to 3 seconds
    And the message displayed is 'Item has been added to your basket - View basket'

  Scenario: Add to basket failure due to no product availability
    Given the add to basket button is displayed in favourites page for the product
    And there is NO stock availability of the sellable variant
    When the user chooses to add to basket
    Then the add to basket is NOT successful
    And a message fades in and disappears after 2 to 3 seconds
    And the message is 'We are sorry. This product is not available'

  Scenario: Add to basket back-end failure
    Given the add to basket button is enabled in favourites page for the product
    And there is stock availability of the sellable variant
    When the user chooses to add to basket
    And there is a back-end failure when attempting to add to basket
    Then the add to basket is NOT successful
    And a message fades in and disappears after 2 to 3 seconds
    And the message is 'There was an error adding to basket. Please try again.'

  Scenario Outline: Attempt to add to basket when button disabled
    And the user has previously favourited <favouritedProduct> <level>
    And it is a case where the Add to basket button is NOT enabled
    When the user chooses to add to basket
    Then the add to basket is not initiated
    And a message fades in and disappears after 2 to 3 seconds
    And the message is <message>

    Examples: 
      | favouritedProduct | level  | selectedColour | selectedSize | message                                           | comment                                   |
      | W906076           | base   | none           | none         | Please select option Edit                         | Base product favourited (on Product List) |
      | 58539729          | colour | blue           | none         | Please select size option Edit                    | Other colour variant favourited           |
      | 58540022          | size   | blue           | L            | We are sorry. This product is not available. Edit | Out of stock size variant favourited      |
      | 58539743          | colour | red            | none         | We are sorry. This product is not available. Edit | Out of stock colour variant favourited    |
      | 58540138          | size   | red            | M            | We are sorry. This product is not available. Edit | Out of stock size variant favourited      |
      | W224512           | base   | none           | none         | Please select option Edit                         | Base product favourited (on Product List) |
      | 58483541          | colour | coral          | none         | We are sorry. This product is not available. Edit | Out of stock colour variant               |
      | P58687369         | base   | peacoat/grey   | none         | Please select option Edit                         | Base product favourited (on Product List) |
      | 58687369          | colour | peacoat/grey   | none         | Please select size option Edit                    | Only colour variant is favourited         |
      | 58687413          | size   | peacoat/grey   | 12           | We are sorry. This product is not available. Edit | Out of stock size variant                 |
      | W923615           | base   | none           | none         | We are sorry. This product is not available. Edit | Base product favourited (on Product List) |
      | 58773208          | colour | blue           | none         | We are sorry. This product is not available. Edit | Colour variant favourited                 |
      | 58773314          | size   | blue           | M            | We are sorry. This product is not available. Edit | Size variant favourited                   |
      | 58980934          | colour | red            | none         | We are sorry. This product is not available. Edit | Colour variant favourited                 |
      | P58965603         | base   | burgundy       | none         | We are sorry. This product is not available. Edit | Base product favourited (on Product List) |
      | 58965603          | colour | burgundy       | none         | We are sorry. This product is not available. Edit | Colour variant favourited                 |
      | 58965641          | size   | burgundy       | 10           | We are sorry. This product is not available. Edit | Size variant favourited                   |
      | P57527789         | base   | none           | none         | We are sorry. This product is not available. Edit | Base product favourited (on Product List) |
      | 57527789          | colour | none           | none         | We are sorry. This product is not available. Edit | Only colour variant is favourited         |

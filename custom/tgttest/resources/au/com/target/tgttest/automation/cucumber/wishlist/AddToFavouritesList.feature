Feature: Adding Products to Favourites List
  
  In order have products in my favourites
  As a registered Target Customer
  I want the ability to favourite products

  Scenario: Registered target customer without favourites list favouriting a product
    Given customer 'test.user1@target.com.au' having no favourites
    When customer favourites the following product:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
    Then favourites list will be created for the customer with type 'FAVOURITE' and visibility 'PRIVATE'
    And the favourites for the customer in server has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |

  Scenario: Registered target customer having favourites list favouriting a product
    Given customer 'test.user1@target.com.au' with an empty favourites
    When customer favourites the following product:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
    Then the favourites for the customer in server has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |

  Scenario: Registered target customer having favourites list favouriting a sellable variant
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
    When customer favourites the following product:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |
    Then the favourites for the customer in server has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |

  Scenario: Registered target customer having favourites list favouriting a different sellable variant of an existing product
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
    When customer favourites the following product:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000012            | W100000         | 28-11-2015 09:05:00 |
    Then the favourites for the customer in server has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000012            | W100000         | 28-11-2015 09:05:00 |

  Scenario: As a user who already has saved max favourites, and user adds another one.
    Given the max a user can add to his favourites is '4'
    And the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |
      | 10000211            | W100002         | 06-11-2015 18:20:35 |
      | P4000_blue          | P4000           | 14-03-2015 11:15:00 |
    When customer favourites the following product:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000322            | W100003         | 28-11-2015 09:05:00 |
    Then the favourites for the customer in server has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 10:30:00 |
      | 10000211            | W100002         | 06-11-2015 18:20:35 |
      | 10000322            | W100003         | 28-11-2015 09:05:00 |

  Scenario: When a product that is already favourited is moved from one style group to another
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 15-05-2017 16:10:00 |
      | 10000111            | W100001         | 15-05-2017 16:11:00 |
      | P4000_blue          | P4000           | 15-05-2017 16:12:00 |
    When the following variants are moved to a different style group:
      | selectedVariantCode | baseProductCode | timestamp           |
      | P4000_blue          | W100002         | 15-05-2017 16:12:00 |
    Then the favourites for the customer in server has:  
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 15-05-2017 16:10:00 |
      | 10000111            | W100001         | 15-05-2017 16:11:00 |
      | P4000_blue          | W100002         | 15-05-2017 16:12:00 |         
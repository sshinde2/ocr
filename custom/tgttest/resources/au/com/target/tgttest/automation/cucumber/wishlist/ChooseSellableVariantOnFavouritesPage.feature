@notAutomated
Feature: Choose a sellable variant on the Favourites page
  As a Target customer
  I want to be able to choose a sellable variant from a favourited item
  so that I can easily purchase it.

  Background: 
    Given the products details
      | Base SKU  | Name                              | Colour SKU   | Colour         | image                 | Size SKU | Size         | Stock | comment |
      |           |                                   | 58539682     | Charcoal       | !charcoal shirt.jpg   |          |              |       |         |
      |           |                                   |              |                |                       | 58539774 | M            | Yes   |         |
      |           |                                   |              |                |                       | 58539781 | L            | Yes   |         |
      |           |                                   | 58539729     | Blue           | !blue shirt.jpg       |          |              |       |         |
      |           |                                   |              |                |                       | 58540015 | M            | Yes   |         |
      |           |                                   |              |                |                       | 58540022 | L            | No    |         |
      |           |                                   | 58539743     | Red            | !red shirt.jpg        |          |              |       |         |
      |           |                                   |              |                |                       | 58540138 | M            | No    |         |
      |           |                                   |              |                |                       | 58540145 | L            | No    |         |
      |           |                                   | 48892674     | Steel Grey     | !steel grey towel.jpg |          |              |       | Yes     |
      |           |                                   | 58483565     | Berry          | !berry towel.jpg      |          |              |       | Yes     |
      |           |                                   | 58483541     | Coral          | !coral towel.jpg      |          |              |       | No      |
      |           |                                   | 58687369     | Peacoat / Grey | !dress.jpg            |          |              |       |         |
      |           |                                   |              |                |                       | 58687406 | 10           | Yes   |         |
      |           |                                   |              |                |                       | 58687413 | 12           | No    |         |
      |           |                                   | 58305430     | nocolour       | !monopoly.jpg         |          |              |       | Yes     |
      | W923615   | Active Fleece Hoodie              | Eg, 58773208 |                | !blue hoodie.jpg      |          | Eg. 58773314 |       |         |
      | W927006   | Check Wool Blend Tote             | Eg. 58980934 |                | !bag red.jpg          |          |              |       |         |
      | P58965603 | Lily Loves Split Back Check Shirt | Eg. 58965603 |                | !check shirt.jpg      |          | Eg. 58965641 |       |         |
      | P57527789 | Remote Control Quad Copter        | Eg. 57527789 |                | !copter.jpg           |          |              |       |         |

  Scenario Outline: Display of the link to choose a variant
    Given the user is on target.com.au
    And the user has previously favourited <favouritedProduct> (at level <level>)
    When the user views that item on their favourites page
    Then the favourite item displayed has:Image <image>,<selectedColour>,<selectedSize>
    And the colour and size edit link is <linkDisplay>
    And the text on the link is <linkText>

    Examples: 
      | favouritedProduct | level  | image                 | selectedColour | selectedSize | linkDisplay   | linkText              |
      | W906076           | base   | !charcoal shirt.jpg   | none           | none         | displayed     | Need to select option |
      | 58539729          | colour | !blue shirt.jpg       | blue           | none         | displayed     | Need to select size   |
      | 58540015          | size   | !blue shirt.jpg       | blue           | M            | displayed     | Edit                  |
      | 58540022          | size   | !blue shirt.jpg       | blue           | L            | displayed     | Edit                  |
      | 58539743          | colour | !red shirt.jpg        | red            | none         | displayed     | Need to select size   |
      | 58540138          | size   | !red shirt.jpg        | red            | M            | displayed     | Edit                  |
      | 58540145          | size   | !red shirt.jpg        | red            | L            | displayed     | Edit                  |
      | W224512           | base   | !steel grey towel.jpg | none           | none         | displayed     | Need to select option |
      | 58483565          | colour | !berry towel.jpg      | berry          | none         | displayed     | Edit                  |
      | 58483541          | colour | !coral towel.jpg      | coral          | none         | displayed     | Edit                  |
      | P58687369         | base   | !dress.jpg            | peacoat/grey ? | none         | displayed     | Need to select option |
      | 58687369          | colour | !dress.jpg            | peacoat/grey   | none         | displayed     | Need to select size   |
      | 58687406          | size   | !dress.jpg            | peacoat/grey   | 10           | displayed     | Edit                  |
      | 58687413          | size   | !dress.jpg            | peacoat/grey   | 12           | displayed     | Edit                  |
      | P58305430         | base   | !monopoly.jpg         | none           | none         | NOT displayed | N/A                   |
      | 58305430          | colour | !monopoly.jpg         | none           | none         | NOT displayed | N/A                   |
      | W923615           | base   | !blue hoodie.jpg      | none           | none         | displayed     | Need to select option |
      | 58773208          | colour | !blue hoodie.jpg      | blue           | none         | displayed     | Need to select size   |
      | 58773314          | size   | !blue hoodie.jpg      | blue           | M            | displayed     | Edit                  |
      | W927006           | base   | !bag red.jpg          | none           | none         | displayed     | Need to select option |
      | 58980934          | colour | !bag red.jpg          | red check      | none         | displayed     | Edit                  |
      | P58965603         | base   | !check shirt.jpg      | burgundy ?     | none         | displayed     | Need to select option |
      | 58965603          | colour | !check shirt.jpg      | burgundy       | none         | displayed     | Need to select size   |
      | 58965641          | size   | !check shirt.jpg      | burgundy       | 10           | displayed     | Edit                  |
      | P57527789         | base   | !copter.jpg           | none           | none         | NOT displayed | N/A                   |
      | 57527789          | colour | !copter.jpg           | none           | none         | NOT displayed | N/A                   |

  Scenario Outline: Display of the Choose Option(s) panel
    Given the user has previously favourited <favouritedProduct> (at level <level>)
    When the user chooses to select a variant
    Then that item on the Favourites page is replaced by a Choose Option(s) panel for that item
    And the panel displays with: Heading as <heading>
    And Selected size as <selectedColour>
    And Selected size as <selectedSize>
    And Done button that is <doneButton>
    And there is a Cancel button
    And there is a Close \(x\) button

    Examples: 
      | favouritedProduct | level  | heading        | image                 | selectedColour | selectedSize | doneButton |
      | W906076           | base   | Choose Options | !charcoal shirt.jpg   | none           | none         | grey       |
      | 58539729          | colour | Choose Options | !blue shirt.jpg       | blue           | none         | grey       |
      | 58540015          | size   | Choose Options | !blue shirt.jpg       | blue           | M            | blue       |
      | 58540022          | size   | Choose Options | !blue shirt.jpg       | blue           | L            | blue       |
      | 58539743          | colour | Choose Options | !red shirt.jpg        | red            | none         | grey       |
      | 58540138          | size   | Choose Options | !red shirt.jpg        | red            | M            | blue       |
      | 58540145          | size   | Choose Options | !red shirt.jpg        | red            | L            | blue       |
      | W224512           | base   | Choose Option  | !steel grey towel.jpg | none           | none         | grey       |
      | 58483565          | colour | Choose Option  | !berry towel.jpg      | berry          | none         | blue       |
      | 58483541          | colour | Choose Option  | !coral towel.jpg      | coral          | none         | blue       |
      | P58687369         | base   | Choose Option  | !dress.jpg            | n/a            | none         | grey       |
      | 58687369          | colour | Choose Option  | !dress.jpg            | n/a            | none         | grey       |
      | 58687406          | size   | Choose Option  | !dress.jpg            | n/a            | 10           | blue       |
      | 58687413          | size   | Choose Option  | !dress.jpg            | n/a            | 12           | blue       |
      | W923615           | base   | Choose Options | !blue hoodie.jpg      | none           | none         | grey       |
      | 58773208          | colour | Choose Options | !blue hoodie.jpg      | blue           | none         | grey       |
      | 58773314          | size   | Choose Options | !blue hoodie.jpg      | blue           | M            | blue       |
      | W927006           | base   | Choose Option  | !bag red.jpg          | none           | none         | grey       |
      | 58980934          | colour | Choose Option  | !bag red.jpg          | red check      | none         | blue       |
      | P58965603         | base   | Choose Option  | !check shirt.jpg      | n/a            | none         | grey       |
      | 58965603          | colour | Choose Option  | !check shirt.jpg      | n/a            | none         | grey       |
      | 58965641          | size   | Choose Option  | !check shirt.jpg      | n/a            | 10           | blue       |

  Scenario: Select colour
    Given the product has colour variants
    When the user selects a colour
    Then the selected colour is highlighted
    And the label is updated to: "Selected Colour: \{colour-name\}"
    And if either a size is already selected, or there are no size variants, then the Done button is enabled

  Scenario: Select size
    Given the product has size variants
    When the user selects a size
    Then the selected size is highlighted
    And the Done button is enabled

  Scenario Outline: Select Done on the Choose Option(s) panel when done button is blue
    Given the user has selected <favouritedProduct> (at level <level>), with:
      | image   | colour           | size           |
      | <image> | <selectedColour> | <selectedSize> |
    And the Done button is blue
    When the user chooses that they are "Done" on the Choose Option(s) panel
    Then The favourite is refined to the selected variant.
    And The Choose Option(s) panel is removed.
    And The favourited item is re-displayed, with the selected variant displayed.

    Examples: 
      | favouritedProduct | level  | image                 | selectedColour | selectedSize | doneButton | message                       |
      | W906076           | base   | !charcoal shirt.jpg   | none           | none         | grey       | Please select colour and size |
      | 58539729          | colour | !blue shirt.jpg       | blue           | none         | grey       | Please select a size option   |
      | 58540015          | size   | !blue shirt.jpg       | blue           | M            | blue       | N/A                           |
      | 58540022          | size   | !blue shirt.jpg       | blue           | L            | blue       | N/A                           |
      | 58539743          | colour | !red shirt.jpg        | red            | none         | grey       | Please select a size option   |
      | 58540138          | size   | !red shirt.jpg        | red            | M            | blue       | N/A                           |
      | 58540145          | size   | !red shirt.jpg        | red            | L            | blue       | N/A                           |
      | W224512           | base   | !steel grey towel.jpg | none           | none         | grey       | Please select a colour option |
      | 58483565          | colour | !berry towel.jpg      | berry          | none         | blue       | N/A                           |
      | 58483541          | colour | !coral towel.jpg      | coral          | none         | blue       | N/A                           |
      | P58687369         | base   | !dress.jpg            | n/a            | none         | grey       | Please select a size option   |
      | 58687369          | colour | !dress.jpg            | n/a            | none         | grey       | Please select a size option   |
      | 58687406          | size   | !dress.jpg            | n/a            | 10           | blue       | N/A                           |
      | 58687413          | size   | !dress.jpg            | n/a            | 12           | blue       | N/A                           |
      | 58773208          | colour | !blue hoodie.jpg      | blue           | none         | grey       | Please select a size option   |
      | 58773314          | size   | !blue hoodie.jpg      | blue           | M            | blue       | N/A                           |
      | W927006           | base   | !bag red.jpg          | none           | none         | grey       | Please select a colour option |
      | 58980934          | colour | !bag red.jpg          | red check      | none         | blue       | N/A                           |
      | P58965603         | base   | !check shirt.jpg      | n/a            | none         | grey       | Please select a size option   |
      | 58965603          | colour | !check shirt.jpg      | n/a            | none         | grey       | Please select a size option   |
      | 58965641          | size   | !check shirt.jpg      | n/a            | 10           | blue       | N/A                           |

  Scenario: Close or Cancel from the Choose Option(s) panel
    Given the user chooses to close \(x\) or Cancel
    Then the favourite is NOT refined at all
    And the Choose Option(s) panel is removed
    And the unchanged favourited item is re-displayed

  Scenario: Update favourite back-end failure
    Given it is a case where the Done button is blue (enabled) (ie. the user has selected a sellable variant)
    When the user chooses Done to update the favourite
    And there is a back-end failure when attempting to update the favourite
    Then the update to favourite is NOT successful
    And a message fades in and disappears after 2 to 3 seconds
    And the message is: "There was an error updating the Favourite. Please try again."

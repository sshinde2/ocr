@notAutomated
Feature: D isplay find in store icon on Favourites page - Display only product
  As a Target online coordinator
  I want to display find in store option in Favourites page
  So that customers can easily view store stock availability for their favourited products
  Please note display only products means all variants under the same base product are to be display only. 
  For example, even user favourite a variant which is display only, when there is other variants sold online for the same base product, 
  the display only variant will be dispalyed as non-display only variants.

  Scenario: Display find in store for display only product
    Given that customer has favourited a display only product (all variants under the base product are display-only)
    When customer views the display only product in favourites page
    Then display the product card with Find in store icon instead of basket
    And remove the edit options for customer in the product card
    And also display the find in store option below the icon as per design

  Scenario: Redirect customer to PDP
    Given customer views a product in favourites page
    When customer selects find in store icon or  find in store text or find in store stock messages below variants within edit screen or product image
    Then redirect user to in store tab in PDP (store tab is active)

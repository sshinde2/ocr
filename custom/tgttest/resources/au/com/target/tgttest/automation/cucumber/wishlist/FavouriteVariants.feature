@notAutomated
Feature: Displaying the variants in the Favourites list
  
  Inorder to view the favourites 
  As a Target Online user 
  I want to  favourite a particular variant, and to see that variant on the Favourites page, including stock position of that variant
  So that I have a better information  of the product that I am after, and also to  give me an indication of the stock level

  Background: 
    Given the products details
      | baseProduct | productName                       | colorvariant    | sizeVariant       | stockAvailable | Price |
      | P906076     | Long Sleeve Flannelette Shirt     | P906076_blue    | P906076_blue_S    | yes            | $10   |
      | P906076     | Long Sleeve Flannelette Shirt     | P906076_blue    | P906076_blue_M    | yes            | $20   |
      | P906076     | Long Sleeve Flannelette Shirt     | P906076_red     | P906076_red_S     | yes            | $10   |
      | P906076     | Long Sleeve Flannelette Shirt     | P906076_red     | P906076_red_M     | no             | $20   |
      | P224512     | Grandeur Bath Towel               | P224512_blue    |                   | yes            | $15   |
      | P224512     | Grandeur Bath Towel               | P224512_green   |                   | no             | $15   |
      | P58687369   | Tie Front Jersey Dress            | P58687369_black | P58687369_black_S | yes            | $20   |
      | P58687369   | Tie Front Jersey Dress            | P58687369_black | P58687369_black_L | no             | $20   |
      | P58305430   | Star Wars MonoPoly                | P58305430_black |                   | yes            | $20   |
      | W923615     | Active Fleece Hoodie              | W923615_black   | W923615_black_S   | no             | $20   |
      | W923615     | Active Fleece Hoodie              | W923615_black   | W923615_black_L   | no             | $20   |
      | W923615     | Active Fleece Hoodie              | W923615_blue    | W923615_blue_S    | no             | $20   |
      | W923615     | Active Fleece Hoodie              | W923615_blue    | W923615_blue_L    | no             | $20   |
      | P57527789   | Remote Control Helicopter         | P57527789_blue  |                   | no             | $20   |
      | P58965603   | Lily Loves Split Back Check Shirt | P58965603_red   | P58965603_red_S   | no             | $20   |
      | P58965603   | Lily Loves Split Back Check Shirt | P58965603_red   | P58965603_red_L   | no             | $20   |
      | P58965603   | Lily Loves Split Back Check Shirt | P58965603_blue  | P58965603_blue_S  | yes            | $20   |
      | P58965603   | Lily Loves Split Back Check Shirt | P58965603_blue  | P58965603_blue_L  | yes            | $20   |

  Scenario Outline: View selected variant on Favourites page
    Given the customer is on the target online
    And has favorited the product <productCode>
    When the customer retrieves the favourite details
    Then the favourite information displayed are <productCode>,<outofStockFlag>

    Examples: 
      | productCode       | outofStockFlag | Price     |
      | P906076           | Instock        | $10 - $20 |
      | P906076_blue      | Instock        | $10 - $20 |
      | P906076_blue_S    | Instock        | $10       |
      | P906076_red       | Instock        | $10 - $20 |
      | P906076_red_M     | Unavailable    | $20       |
      | P224512           | Instock        | $15       |
      | P224512_green     | Unavailable    | $15       |
      | P58687369_black_L | Unavailable    | $20       |
      | W923615           | Unavailable    | $20       |
      | W923615_black     | Unavailable    | $20       |
      | W923615_blue_S    | Unavailable    | $20       |
      | W923615_black_S   | Unavailable    | $20       |
      | P57527789         | Unavailable    | $20       |
      | P58965603         | Instock        | $20       |
      | P58965603_red     | Unavailable    | $20       |
      | P58965603_red_S   | Unavailable    | $20       |
      | P58965603_blue    | Instock        | $20       |
      | P58965603_blue_L  | Instock        | $20       |

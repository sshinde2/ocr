Feature: Removing Products from Favourites List
  
  As a registered Target Customer
  I want the ability to remove products from my favourites list

  Scenario: Registered target customer having favourites list removing a product
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |
    When the customer removes favourites from his favourites list:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |
    Then the favourites for the customer in server has:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |

  Scenario: Registered target customer having favourites list removing all sellable variant
    Given the favourites for the customer 'test.user1@target.com.au' is:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |
    When the customer removes favourites from his favourites list:
      | selectedVariantCode | baseProductCode | timestamp           |
      | 10000111            | W100001         | 28-11-2015 08:05:00 |
      | 10000011            | W100000         | 28-11-2015 08:05:00 |
    Then the favourites for the customer in server is empty

Feature: Sharing of Favourites List
  
  As a registered Target Customer
  I want the ability to share my favourite products

  Scenario: As a user who as signed in and share the favourites.
    Given webmethods returns 'success' for share favourites
    And the favourites for the customer 'test.user1@target.com.au' to share has the following products:
      | selectedVariantCode | baseProductCode | imageUrl                   | Colour | Size | price | priceRange |
      | 10000211            | W100002         | 01**/grid/10000211.jpg     |        |      | 20    |            |
      | P4000_blue          | P4000           | 01**/grid/P4000_blue.jpg   | blue   |      |       | 10-39.99   |
      | P4001_blue_L        | P4001           | 01**/grid/P4001_blue_L.jpg | blue   | L    | 10    |            |
    When the customer shares the favourites with the recipient 'Jane Doe'
    And the recipient Email address is 'jane.doe@test.com'
    And the message to the recipient is 'These are my favourites'
    Then the email with the favourites is sent

  Scenario: As a user who as signed in and share the favourites, an error occurs
    Given webmethods returns 'error' for share favourites
    And the favourites for the customer 'test.user1@target.com.au' to share has the following products:
      | selectedVariantCode | baseProductCode | imageUrl                 | price |
      | 10000211            | W100002         | 01**/grid/10000211.jpg   | 30    |
      | P4000_blue          | P4000           | 01**/grid/P4000_blue.jpg | 20    |
    When the customer shares the favourites with the recipient 'Jane Doe'
    And the recipient Email address is 'jane.doe@test.com'
    And the message to the recipient is 'These are my favourites'
    Then the email with the favourites is not sent

  @notAutomated
  Scenario Outline: Share selected variants on Favorites page
    Given the user has favourited <favouritedProduct>
    When the user attempts to share their favorites list via email
    Then for each favourited product the following information is passed to Exact Target via WebMethods:
      | imageUrl   | name          | url          | colour           | size           |
      | <imageUrl> | <productName> | <productUrl> | <selectedColour> | <selectedSize> |

    Examples: 
      | favouritedProduct | baseProductCode | imageUrl                   | productUrl     | name                  | selectedColour | selectedSize |
      | P4000             | P4000           | 01**/grid/P4000_blue.jpg   | p/P4000        | P4000_BaseProductName |                |              |
      | P4000_blue        | P4000           | 01**/grid/P4000_blue.jpg   | p/P4000_blue   | P4000_BaseProductName | blue           |              |
      | P4001_blue_L      | P4001           | 01**/grid/P4001_blue_L.jpg | p/P4001_blue_L | P4000_BaseProductName | blue           | L            |

  @notAutomated
  Scenario Outline: Share selected variants on Favorites page - content of email
    Given the user has favourited <favouritedProduct>
    When the user attempts to share their favorites list via email
    Then only <imageUrl>, <productName> and <productUrl> are displayed in the email generated
    And price, colour, size and stock would NOT be displayed in the email generated.

    Examples: 
      | favouritedProduct | baseProductCode | imageUrl                   | productUrl     | name                  | selectedColour |
      | P4000             | P4000           | 01**/grid/P4000_blue.jpg   | p/P4000        | P4000_BaseProductName |                |
      | P4000_blue        | P4000           | 01**/grid/P4000_blue.jpg   | p/P4000_blue   | P4000_BaseProductName | blue           |
      | P4001_blue_L      | P4001           | 01**/grid/P4001_blue_L.jpg | p/P4001_blue_L | P4000_BaseProductName | blue           |

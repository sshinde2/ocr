@cartProductData @deliveryModeData
Feature: Zippay - Refund for cancel and refund after order complete
  In order to have better shopping experience
  As an Online Store
  I want to refund the amount paid by zip as quickly as possible if the customer order is valid for refund

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
      | 10000012 | 0        | 10        |
    And payment method is 'zippay'
    And user checkout via spc

  Scenario: Refund for full Cancel from CS successfully
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And zip create refund api returns:
      | id                        |
      | rf_H5J24ZnqpKMByItvF2Jh85 |    
    And the zip refund will 'succeed' for the order
    When the order is fully cancelled
    Then the zip refund has been trigerred for the order with amount '39'
    And the zip refund has been 'succeeded'
    And the order status is CANCELLED

  Scenario: Refund for full Cancel from CS failed
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And the zip refund will 'fail' for the order
    When the order is fully cancelled
    Then the zip refund has been trigerred for the order with amount '39'
    And the zip refund has been 'failed'
    And the order status is INPROGRESS

   Scenario: Refund is successful from CS after order has been completed
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order has been completed
    And zip create refund api returns:
      | id                        |
      | rf_H5J24ZnqpKMByItvF2Jh85 |
    And the zip refund will 'succeed' for the order
    When all products with delivery fee amount 9 are refunded
    And the zip refund has been trigerred for the order with amount '39'
    And the zip refund has been 'succeeded'
    Then the refund for the order is:
      | 39 |

   Scenario: Refund has failed from CS after order has been completed
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And order is placed
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order has been completed
    And the zip refund will 'fail' for the order
    When return entries
      | product  | qty |
      | 10000011 | 2   |    
    And the zip refund has been 'failed'
    Then the refund for the order is:
      |  |      
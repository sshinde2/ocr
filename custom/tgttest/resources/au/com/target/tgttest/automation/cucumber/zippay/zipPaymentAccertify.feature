@cartProductData @deliveryModeData
Feature: zippay Payments
  In order to purchase products
  As a customer
  I need to use my zippay account in the checkout process

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 10        |
      | 10000012 | 0        | 10        |
    And a registered customer goes into the checkout process
    And user checkout via spc
    And delivery mode is 'home-delivery'
    And set product stock:
      | product  | available | warehouseCode              |
      | 10000011 | 30        | ConsolidatedStoreWarehouse |
      | 10000012 | 20        | ConsolidatedStoreWarehouse |

  Scenario: Placing an Order with zippay - success and verify payment entries
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And payment method is 'zippay'
    When order is placed
    Then place order result is 'SUCCESS'
    Then the accertify payment entries:
      | PaymentType | zipAmount | zipStatus |
      | Zippay      | 39.00     | ACCEPTED  |

  Scenario: Placing an Order with zippay and verify failedPaymentInformation - payment failure
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And payment method is 'zippay'
    And set zip CaptureTransactionError 'true'
    When order is placed
    Then place order result is 'PAYMENT_FAILURE'
    When set zip CaptureTransactionError 'false'
    And order is placed
    Then place order result is 'SUCCESS'
    Then the accertify payment entries:
      | PaymentType | zipAmount | zipStatus |
      | Zippay      | 39.00     | ACCEPTED  |
    And verify accertify failedPaymentInformation:
      | failedAmount | failedStatus | totalFailedZIPAttempts |
      | 39.00        | ERROR        | 1                      |

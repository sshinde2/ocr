@cartProductData @deliveryModeData
Feature: zippay Payments
  In order to purchase products
  As a customer
  I need to use my zippay account in the checkout process

  Background: 
    Given set fastline stock:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
      | 10000012 | 0        | 2         |
    And a registered customer goes into the checkout process
    And user checkout via spc
    And delivery mode is 'home-delivery'

  Scenario: Placing an Order with zippay - success
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And payment method is 'zippay'
    When order is placed
    Then place order result is 'SUCCESS'
    And order is:
      | total | subtotal | deliveryCost | totalTax |
      | 39    | 30       | 9.0          |          |
    And order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 2        | 2         |
      
  Scenario: placing an Order with zippay Payment - zippay retrieve checkout api is down
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And payment method is 'zippay'
    And zippay retrieve checkout api is down
    When order is placed which might fail
    Then place order result is 'SERVICE_UNAVAILABLE'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

  Scenario: placing an Order with zippay Payment - order total does not match capture value
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And payment method is 'zippay'
    And the cart entries are updated before place order:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    When order is placed which might fail
    Then place order result is 'PAYMENT_FAILURE_DUETO_MISSMATCH_TOTALS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

  Scenario: placing an Order with zippay Payment - order items does not match the cart items
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And payment method is 'zippay'
    And the cart entries are updated before place order:
      | product  | qty | price |
      | 10000011 | 1   | 15    |
    And the cart entries are added before place order:
      | product  | qty | price |
      | 10000012 | 1   | 15    |
    When order is placed which might fail
    Then place order result is 'ITEM_MISMATCH_IN_CART_AFTER_PAYMENT'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |
      | 10000012 | 0        | 2         |

  Scenario: placing an Order with zippay Payment - some stock not available
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
      | 10000012 | 2   | 15    |
    And payment method is 'zippay'
    And set fastline stock before place order:
      | product  | reserved | available |
      | 10000011 | 1        | 2         |
      | 10000012 | 1        | 2         |
    When order is placed which might fail
    Then place order result is 'INSUFFICIENT_STOCK'

  Scenario: Placing an Order with zippay Payment - when zippay rejects the payment
    Given a cart with entries:
      | product  | qty | price |
      | 10000011 | 2   | 15    |
    And payment method is 'zippay'
    And zippay rejects the payment
    When order is placed which might fail
    Then place order result is 'PAYMENT_FAILURE'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |

  Scenario: placing an Order with zippay Payment - when zippay capture payment api is down and retry failed
    Given zippay capture payment api is down
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 10    |
    And payment method is 'zippay'
    When order is placed which might fail
    Then order is in status 'CANCELLED'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 0        | 2         |


  Scenario: Placing an Order with zippay Payment - when zippay capture api and get payment is down during first try and capture payment succeeds on retry
    Given zippay capture payment api is down
    And a cart with entries:
      | product  | qty | price |
      | 10000011 | 1   | 10    |
    And payment method is 'zippay'
    When order is submitted
    And zippay capture payment api succeeds on retry
    Then order is in status 'INPROGRESS'
    And fastline stock is:
      | product  | reserved | available |
      | 10000011 | 1        | 2         |

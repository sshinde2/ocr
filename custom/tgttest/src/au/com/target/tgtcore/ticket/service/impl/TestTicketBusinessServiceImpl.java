/**
 * 
 */
package au.com.target.tgtcore.ticket.service.impl;

import de.hybris.platform.core.model.order.OrderModel;


/**
 * @author sbryan6
 * 
 */
public class TestTicketBusinessServiceImpl extends TargetTicketBusinessServiceImpl {

    private static String ticketId = null;

    @Override
    public String createCsAgentTicket(final String headline, final String subject, final String text,
            final OrderModel order) {

        // For testing, capture the ticket id
        ticketId = super.createCsAgentTicket(headline, subject, text, order);

        return ticketId;
    }

    @Override
    public String createCsAgentTicket(final String headline, final String subject, final String text,
            final OrderModel order, final String group) {
        ticketId = super.createCsAgentTicket(headline, subject, text, order, group);

        return ticketId;
    }



    /**
     * Get data for the last ticket sent
     * 
     * @returnTicketData
     */
    public static String getTicketId() {
        return ticketId;
    }

    /**
     * clear remembered ticket id
     */
    public static void clearTicketId() {
        ticketId = null;
    }
}

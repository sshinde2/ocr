/**
 * 
 */
package au.com.target.tgtfraud.order.impl;

import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.order.InvalidCartException;


/**
 * @author sbryan6
 * 
 */
public class TestCommerceCheckoutServiceImpl extends TargetFraudCommerceCheckoutServiceImpl {

    /* 
     * For faking multiple payment issue 
     */
    @Override
    public CommerceOrderResult placeOrder(final CommerceCheckoutParameter checkoutParameter)
            throws InvalidCartException {

        return null;
    }

}

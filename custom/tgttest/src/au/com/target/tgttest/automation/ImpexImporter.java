/**
 * 
 */
package au.com.target.tgttest.automation;



/**
 * Utility for importing impex during automated test.
 * 
 */
public final class ImpexImporter {

    private ImpexImporter() {
        // Utility class
    }

    public static void importCsv(final String csvFile)
    {
        ServiceLookup.getSetupImpexService().importImpexFile(csvFile, true);
    }
}

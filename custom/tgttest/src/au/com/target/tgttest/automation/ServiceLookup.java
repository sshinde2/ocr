/**
 * 
 */
package au.com.target.tgttest.automation;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.interceptor.impl.InterceptorMapping;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.impl.DefaultBaseSiteService;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.store.services.impl.DefaultBaseStoreService;
import de.hybris.platform.ticket.service.TicketService;
import de.hybris.platform.voucher.VoucherModelService;
import de.hybris.platform.voucher.VoucherService;

import au.com.target.tgtcore.actions.GenerateTaxInvoiceAction;
import au.com.target.tgtcore.actions.ReverseGiftCardAction;
import au.com.target.tgtcore.actions.SendClickAndCollectSmsNotificationAction;
import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.customer.TargetBcryptPasswordEncoder;
import au.com.target.tgtcore.customer.TargetCustomerAccountService;
import au.com.target.tgtcore.customer.TargetMD5PasswordEncoder;
import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.featureswitch.dao.TargetFeatureSwitchDao;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.flybuys.service.FlybuysRedeemConfigService;
import au.com.target.tgtcore.giftcards.GiftCardService;
import au.com.target.tgtcore.order.FindOrderRefundedShippingStrategy;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtcore.order.TargetCommerceCheckoutService;
import au.com.target.tgtcore.order.TargetFindDeliveryCostStrategy;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.orderEntry.TargetOrderEntryService;
import au.com.target.tgtcore.ordercancel.impl.TargetOrderCancelService;
import au.com.target.tgtcore.orderreturn.TargetOrderReturnService;
import au.com.target.tgtcore.postcode.service.TargetPostCodeService;
import au.com.target.tgtcore.price.TargetPreviousPermanentPriceService;
import au.com.target.tgtcore.product.ProductTypeService;
import au.com.target.tgtcore.product.TargetProductPriceService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.impl.TargetOriginalCategoryPopulatorServiceImpl;
import au.com.target.tgtcore.refund.TargetRefundService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtcore.shipster.ShipsterConfigService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.storefinder.impl.TargetStoreFinderServiceImpl;
import au.com.target.tgtcore.storelocator.TargetOpeningDayService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.ticket.service.impl.TargetTicketBusinessServiceImpl;
import au.com.target.tgtcore.ticket.service.impl.TestTicketBusinessServiceImpl;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtcs.service.checkout.impl.TargetCsCheckoutService;
import au.com.target.tgtebay.actions.UpdateInventoryToPartnerAction;
import au.com.target.tgtebay.actions.UpdatePartnerWithPickedupStatusAction;
import au.com.target.tgtebay.actions.UpdatePartnerWithReadyForPickupStatusAction;
import au.com.target.tgtebay.actions.UpdatePartnerWithShipmentInfoAction;
import au.com.target.tgtebay.jobs.AutoUpdatePartnerPickedupJob;
import au.com.target.tgtfacades.cart.impl.TargetOrderErrorHandlerFacadeImpl;
import au.com.target.tgtfacades.config.CheckoutConfigurationFacade;
import au.com.target.tgtfacades.customer.impl.DefaultTargetCustomerFacade;
import au.com.target.tgtfacades.customer.impl.TargetCustomerSubscriptionFacadeImpl;
import au.com.target.tgtfacades.deals.TargetDealsFacade;
import au.com.target.tgtfacades.delivery.TargetDeliveryFacade;
import au.com.target.tgtfacades.flybuys.impl.FlybuysDiscountFacadeImpl;
import au.com.target.tgtfacades.login.TargetAuthenticationFacade;
import au.com.target.tgtfacades.login.TargetCheckoutLoginResponseFacade;
import au.com.target.tgtfacades.login.impl.TargetAuthenticationFacadeImpl;
import au.com.target.tgtfacades.login.impl.TargetCheckoutLoginResponseFacadeImpl;
import au.com.target.tgtfacades.look.TargetLookPageFacade;
import au.com.target.tgtfacades.marketing.TargetSocialMediaProductsFacade;
import au.com.target.tgtfacades.order.TargetCheckoutResponseFacade;
import au.com.target.tgtfacades.order.TargetOrderFacade;
import au.com.target.tgtfacades.order.impl.TargetCartFacadeImpl;
import au.com.target.tgtfacades.order.impl.TargetCheckoutFacadeImpl;
import au.com.target.tgtfacades.order.impl.TargetCheckoutResponseFacadeImpl;
import au.com.target.tgtfacades.order.impl.TargetPlaceOrderFacadeImpl;
import au.com.target.tgtfacades.prepopulatecart.TargetPrepopulateCheckoutCartFacade;
import au.com.target.tgtfacades.product.TargetProductFacade;
import au.com.target.tgtfacades.product.TargetProductListerFacade;
import au.com.target.tgtfacades.stl.TargetShopTheLookFacade;
import au.com.target.tgtfacades.stock.impl.DefaultTargetStockLookUpFacade;
import au.com.target.tgtfacades.storefinder.TargetStoreFinderStockFacade;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.impl.TargetStoreLocatorFacadeImpl;
import au.com.target.tgtfacades.user.impl.TargetUserFacadeImpl;
import au.com.target.tgtfacades.voucher.TargetVoucherFacade;
import au.com.target.tgtfacades.wishlist.TargetWishListFacade;
import au.com.target.tgtfluent.jobs.LocationFeedJob;
import au.com.target.tgtfluent.jobs.LocationFeedStatusCheckJob;
import au.com.target.tgtfluent.service.FluentBatchResponseService;
import au.com.target.tgtfluent.service.FluentFulfilmentService;
import au.com.target.tgtfluent.service.FluentUpdateStatusService;
import au.com.target.tgtfraud.order.impl.TargetFraudCommerceCheckoutServiceImpl;
import au.com.target.tgtfraud.order.impl.TestCommerceCheckoutServiceImpl;
import au.com.target.tgtfraud.provider.AccertifyFraudServiceProvider;
import au.com.target.tgtfulfilment.dao.TargetGlobalStoreFulfilmentCapabilitiesDao;
import au.com.target.tgtfulfilment.dao.impl.TargetConsignmentDaoImpl;
import au.com.target.tgtfulfilment.dao.impl.TargetManifestDaoImpl;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.jobs.AutoRejectInStoreAckByWarehouse;
import au.com.target.tgtfulfilment.jobs.AutoRejectInStoreWavedJob;
import au.com.target.tgtfulfilment.jobs.ResendNotAcknowledgedOrderExtractsJob;
import au.com.target.tgtfulfilment.jobs.RetransmitManifestsJob;
import au.com.target.tgtfulfilment.jobs.SendSmsToStoreForOpenOrdersJob;
import au.com.target.tgtfulfilment.ordersplitting.strategy.impl.ExternalWarehouseSelectorStrategy;
import au.com.target.tgtfulfilment.ordersplitting.strategy.impl.TargetSplitWithRoutingStrategy;
import au.com.target.tgtfulfilment.service.SendToWarehouseProtocol;
import au.com.target.tgtfulfilment.service.TargetCarrierSelectionService;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtfulfilment.service.TargetGlobalStoreFulfilmentService;
import au.com.target.tgtfulfilment.service.TargetManifestService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtjms.jms.converter.TargetMessageConverter;
import au.com.target.tgtlayby.cart.TargetCommerceCartService;
import au.com.target.tgtlayby.cart.TargetLaybyCartService;
import au.com.target.tgtlayby.order.TargetLaybyCommerceCheckoutService;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtlayby.purchase.impl.PurchaseOptionConfigServiceImpl;
import au.com.target.tgtmail.actions.SendCustomerSubscriptionAction;
import au.com.target.tgtpayment.dao.impl.AfterpayConfigDaoImpl;
import au.com.target.tgtpayment.jobs.TargetAfterpayConfigurationJob;
import au.com.target.tgtpayment.service.impl.AfterpayConfigServiceImpl;
import au.com.target.tgtpaymentprovider.afterpay.util.TargetAfterpayHelper;
import au.com.target.tgtsale.ist.service.TargetInterStoreTransferService;
import au.com.target.tgtsale.pos.service.TargetPOSExtractsService;
import au.com.target.tgtsale.product.TargetPOSProductService;
import au.com.target.tgtsale.product.impl.TargetPOSProductServiceImpl;
import au.com.target.tgtsale.tlog.TargetTransactionLogService;
import au.com.target.tgttest.automation.datasetup.ConsignmentsCreationHelper;
import au.com.target.tgttest.automation.datasetup.OrderCreationHelper;
import au.com.target.tgttest.automation.mock.MockTargetBusinessProcessService;
import au.com.target.tgttest.automation.mock.MockTargetStoreStockService;
import au.com.target.tgttest.cart.impl.TestTargetCommerceCartFactory;
import au.com.target.tgttest.instore.ManifestCreationHelper;
import au.com.target.tgttest.order.impl.TestTargetPartnerOrderServiceImpl;
import au.com.target.tgttinker.client.impl.MockSendToWarehouseRestClient;
import au.com.target.tgttinker.mock.auspost.MockAusPostShipsterClient;
import au.com.target.tgttinker.mock.auspost.MockDispatchLabelClient;
import au.com.target.tgttinker.mock.auspost.MockTransmitManifestClient;
import au.com.target.tgttinker.mock.customer.TargetCustomerSubscriptionClientMock;
import au.com.target.tgttinker.mock.customer.sharewishlist.TargetShareWishlistClientMock;
import au.com.target.tgttinker.mock.endeca.infront.querybuilder.EndecaProductQueryBuilderMock;
import au.com.target.tgttinker.mock.exception.MockExceptionThrowingTargetStockService;
import au.com.target.tgttinker.mock.fluent.FluentClientMock;
import au.com.target.tgttinker.mock.flybuys.FlybuysClientMock;
import au.com.target.tgttinker.mock.fraud.provider.AccertifyFraudServiceProviderMock;
import au.com.target.tgttinker.mock.instore.MockTargetStockUpdateClient;
import au.com.target.tgttinker.mock.logger.MockWMSendToWarehouseLogger;
import au.com.target.tgttinker.mock.payment.IpgClientMock;
import au.com.target.tgttinker.mock.payment.PayPalServiceMock;
import au.com.target.tgttinker.mock.payment.TargetAfterpayClientMock;
import au.com.target.tgttinker.mock.payment.TargetCreditCardPaymentMethodMock;
import au.com.target.tgttinker.mock.payment.TargetZippayClientMock;
import au.com.target.tgttinker.mock.sendSms.TargetSendSmsClientMock;
import au.com.target.tgttinker.mock.stockvisibility.StockVisibilityClientMock;
import au.com.target.tgtwebmethods.stock.client.impl.TargetInventoryAdjustmentClientImpl;
import au.com.target.tgtwishlist.service.impl.TargetWishListServiceImpl;
import au.com.target.tgtwsfacades.activation.impl.TargetProductActivationIntegrationFacade;
import au.com.target.tgtwsfacades.cnc.impl.ClickAndCollectNotificationFacadeImpl;
import au.com.target.tgtwsfacades.consignment.ConsignmentIntegrationFacade;
import au.com.target.tgtwsfacades.instore.TargetInStoreIntegrationFacade;
import au.com.target.tgtwsfacades.orderAck.OrderAckIntegrationFacade;
import au.com.target.tgtwsfacades.pickconfirm.impl.PickConfirmIntegrationFacadeImpl;
import au.com.target.tgtwsfacades.pinpad.impl.PinPadPaymentFacadeImpl;
import au.com.target.tgtwsfacades.pos.PosPriceImportIntegrationFacade;
import au.com.target.tgtwsfacades.productimport.impl.TargetCategoryImportIntegrationFacade;
import au.com.target.tgtwsfacades.productimport.impl.TargetProductImportIntegrationFacade;
import au.com.target.tgtwsfacades.shipconfirm.ShipConfirmIntegrationFacade;
import au.com.target.tgtwsfacades.stlimport.ShopTheLookImportIntegrationFacade;
import au.com.target.tgtwsfacades.stockupdate.impl.StockUpdateIntegrationFacadeImpl;
import au.com.target.tgtwsfacades.storelocator.TargetStoreLocatorIntegrationFacade;


/**
 * Lookup utility for services
 * 
 */
public final class ServiceLookup {

    private static final SetupImpexService SETUP_IMPEX_SERVICE = (SetupImpexService)Registry.getApplicationContext()
            .getBean("setupImpexService");

    private static final ModelService MODEL_SERVICE = (ModelService)Registry.getApplicationContext()
            .getBean("modelService");

    private static final FlexibleSearchService FLEXIBLE_SEARCH_SERVICE = (FlexibleSearchService)Registry
            .getApplicationContext()
            .getBean("flexibleSearchService");

    private static final PurchaseOptionService PURCHASE_OPTION_SERVICE = (PurchaseOptionService)Registry
            .getApplicationContext()
            .getBean("purchaseOptionService");

    private static final ProductService PRODUCT_SERVICE = (ProductService)Registry.getApplicationContext()
            .getBean("productService");

    private static final TargetCommerceCartService TARGET_COMMERCE_CART_SERVICE = (TargetCommerceCartService)Registry
            .getApplicationContext()
            .getBean("targetCommerceCartService");

    private static final TargetLaybyCartService TARGET_LAYBY_CART_SERVICE = (TargetLaybyCartService)Registry
            .getApplicationContext()
            .getBean("targetLaybyCartService");

    private static final TargetCommerceCheckoutService TARGET_COMMERCE_CHECKOUT_SERVICE = (TargetCommerceCheckoutService)Registry
            .getApplicationContext()
            .getBean("targetCommerceCheckoutService");

    private static final TargetTransactionLogService TARGET_TRANSACTION_LOG_SERVICE = (TargetTransactionLogService)Registry
            .getApplicationContext()
            .getBean("targetTransactionLogService");

    private static final TargetPOSExtractsService TARGET_POS_EXTRACT_SERVICE = (TargetPOSExtractsService)Registry
            .getApplicationContext()
            .getBean("posExtractsService");

    private static final TargetMessageConverter TARGET_MESSAGE_CONVERTER_SERVICE = (TargetMessageConverter)Registry
            .getApplicationContext()
            .getBean("targetMessageConverter");

    private static final TargetOrderService TARGET_ORDER_SERVICE = (TargetOrderService)Registry
            .getApplicationContext()
            .getBean("targetOrderService");

    private static final TargetOrderFacade TARGET_ORDER_FACADE = (TargetOrderFacade)Registry
            .getApplicationContext()
            .getBean("targetOrderFacade");

    private static final TargetDeliveryService TARGET_DELIVERY_SERVICE = (TargetDeliveryService)Registry
            .getApplicationContext()
            .getBean("targetDeliveryService");

    private static final TargetDeliveryFacade TARGET_DELIVERY_FACADE = (TargetDeliveryFacade)Registry
            .getApplicationContext()
            .getBean("targetDeliveryFacade");

    private static final TargetPostCodeService TARGET_POSTCODE_SERVICE = (TargetPostCodeService)Registry
            .getApplicationContext()
            .getBean("targetPostCodeService");

    private static final CatalogVersionService CATALOG_VERSION_SERVICE = (CatalogVersionService)Registry
            .getApplicationContext()
            .getBean("catalogVersionService");

    private static final VoucherService VOUCHER_SERVICE = (VoucherService)Registry
            .getApplicationContext()
            .getBean("voucherService");

    private static final VoucherModelService VOUCHER_MODEL_SERVICE = (VoucherModelService)Registry
            .getApplicationContext()
            .getBean("voucherModelService");

    private static final TargetFulfilmentService TARGET_FULFILMENT_SERVICE = (TargetFulfilmentService)Registry
            .getApplicationContext()
            .getBean("targetFulfilmentService");

    private static final SendToWarehouseProtocol ESB_FULFILMENT_PROCESS_SERVICE = (SendToWarehouseProtocol)Registry
            .getApplicationContext()
            .getBean("esbSendToWarehouseProtocol");

    private static final TicketService TICKET_SERVICE = (TicketService)Registry
            .getApplicationContext()
            .getBean("ticketService");

    private static final TargetProductService TARGET_PRODUCT_SERVICE = (TargetProductService)Registry
            .getApplicationContext().getBean("targetProductService");

    private static final TargetProductPriceService TARGET_PRODUCT_PRICE_SERVICE = (TargetProductPriceService)Registry
            .getApplicationContext()
            .getBean("targetProductPriceService");

    private static final TargetCreditCardPaymentMethodMock TARGET_MOCK_CCPAYMENT = (TargetCreditCardPaymentMethodMock)Registry
            .getApplicationContext().getBean("mockCreditCardPaymentMethod");

    private static final AccertifyFraudServiceProvider TARGET_ACCERTIFY = (AccertifyFraudServiceProvider)Registry
            .getApplicationContext().getBean("accertifyFraudServiceProvider");

    private static final AccertifyFraudServiceProviderMock TARGET_MOCK_ACCERTIFY = (AccertifyFraudServiceProviderMock)Registry
            .getApplicationContext().getBean("mockAccertifyFraudServiceProvider");

    private static final TargetCustomerAccountService TARGET_CUSTOMER_ACCOUNT_SERVICE = (TargetCustomerAccountService)Registry
            .getApplicationContext().getBean("targetCustomerAccountService");

    private static final TargetOrderCancelService TARGET_ORDER_CANCEL_SERVICE = (TargetOrderCancelService)Registry
            .getApplicationContext().getBean("targetOrderCancelService");

    private static final UserService USER_SERVICE = (UserService)Registry
            .getApplicationContext().getBean("userService");

    private static final TargetOrderReturnService TARGET_ORDER_RETURN_SERVICE = (TargetOrderReturnService)Registry
            .getApplicationContext().getBean("returnService");

    private static final TargetRefundService TARGET_REFUND_SERVICE = (TargetRefundService)Registry
            .getApplicationContext().getBean("targetRefundService");

    private static final PaymentModeService PAYMENT_MODE_SERVICE = (PaymentModeService)Registry
            .getApplicationContext()
            .getBean("paymentModeService");

    private static final CategoryService CATEGORY_SERVICE = (CategoryService)Registry
            .getApplicationContext()
            .getBean("categoryService");

    private static final StockService STOCK_SERVICE = (StockService)Registry
            .getApplicationContext()
            .getBean("stockService");

    private static final TargetWarehouseService TARGET_WAREHOUSE_SERVICE = (TargetWarehouseService)Registry
            .getApplicationContext()
            .getBean("targetWarehouseService");

    private static final TargetPointOfServiceService TARGET_POINT_OF_SERVICE_SERVICE = (TargetPointOfServiceService)Registry
            .getApplicationContext()
            .getBean("targetPointOfServiceService");

    private static final MockTargetStoreStockService TARGET_STORE_STOCK_SERVICE = (MockTargetStoreStockService)Registry
            .getApplicationContext()
            .getBean("targetStoreStockService");

    private static final TargetPOSProductServiceImpl TARGET_POS_PRODUCT_SERVICE = (TargetPOSProductServiceImpl)Registry
            .getApplicationContext()
            .getBean("targetPOSProductService");

    private static final PinPadPaymentFacadeImpl PINPAD_PAYMENT_FACADE = (PinPadPaymentFacadeImpl)Registry
            .getApplicationContext()
            .getBean("pinPadPaymentFacade");

    private static final TargetPlaceOrderFacadeImpl TARGET_PLACE_ORDER_FACADE = (TargetPlaceOrderFacadeImpl)Registry
            .getApplicationContext()
            .getBean("targetPlaceOrderFacade");

    private static final TargetCartFacadeImpl TARGET_CART_FACADE = (TargetCartFacadeImpl)Registry
            .getApplicationContext()
            .getBean("targetCartFacade");

    private static final TargetCheckoutFacadeImpl TARGET_CHECKOUT_FACADE = (TargetCheckoutFacadeImpl)Registry
            .getApplicationContext()
            .getBean("targetCheckoutFacade");

    private static final TargetCheckoutResponseFacade TARGET_CHECKOUT_RESPONSE_FACADE = (TargetCheckoutResponseFacadeImpl)Registry
            .getApplicationContext()
            .getBean("targetCheckoutResponseFacade");

    private static final SessionService SESSION_SERVICE = (SessionService)Registry
            .getApplicationContext()
            .getBean("sessionService");

    private static final TargetSalesApplicationService TARGET_SALES_APPLICATION_SERVICE = (TargetSalesApplicationService)Registry
            .getApplicationContext()
            .getBean("targetSalesApplicationService");

    private static final TargetCarrierSelectionService TARGET_CARRIER_SELECTION_SERVICE = (TargetCarrierSelectionService)Registry
            .getApplicationContext()
            .getBean("targetCarrierSelectionService");

    private static final FlybuysClientMock FLYBUYS_MOCK_CLIENT = (FlybuysClientMock)Registry
            .getApplicationContext().getBean("mockFlybuysClient");

    private static final FlybuysDiscountFacadeImpl FLYBUYS_DISCOUNT_FACADE = (FlybuysDiscountFacadeImpl)Registry
            .getApplicationContext().getBean("flybuysDiscountFacade");

    private static final FlybuysRedeemConfigService FLYBUYS_REDEEM_CONFIG_SERVICE = (FlybuysRedeemConfigService)Registry
            .getApplicationContext().getBean("flybuysRedeemConfigService");

    private static final TargetStockService TARGET_STOCK_SERVICE = (TargetStockService)Registry
            .getApplicationContext().getBean("targetStockService");

    private static final MockTargetStockUpdateClient TARGET_STOCK_UPDATE_CLIENT = (MockTargetStockUpdateClient)Registry
            .getApplicationContext().getBean("targetStockUpdateClient");

    private static final MockExceptionThrowingTargetStockService MOCK_EXCEPTION_TARGET_STOCK_SERVICE = (MockExceptionThrowingTargetStockService)Registry
            .getApplicationContext().getBean("targetStockService");

    private static final MockTransmitManifestClient TRANSMIT_MANIFEST_CLIENT = (MockTransmitManifestClient)Registry
            .getApplicationContext().getBean("transmitManifestClient");

    private static final MockDispatchLabelClient DISPATCH_LABEL_CLIENT = (MockDispatchLabelClient)Registry
            .getApplicationContext().getBean("dispatchLabelClient");

    private static final MockSendToWarehouseRestClient SEND_TO_WAREHOUSE_REST_CLIENT = (MockSendToWarehouseRestClient)Registry
            .getApplicationContext().getBean("sendToWarehouseRestClient");

    private static final TargetWarehouseService WAREHOUSE_SERVICE = (TargetWarehouseService)Registry
            .getApplicationContext().getBean("warehouseService");

    private static final DefaultBaseSiteService BASE_SITE_SERVICE = (DefaultBaseSiteService)Registry
            .getApplicationContext()
            .getBean("baseSiteService");

    private static final DefaultTargetCustomerFacade TARGET_CUSTOMER_FACADE = (DefaultTargetCustomerFacade)Registry
            .getApplicationContext()
            .getBean("defaultTargetCustomerFacade");


    private static final TargetLaybyCommerceCheckoutService TARGET_LAYBY_COMMERCE_CHECKOUT_SERVICE = (TargetLaybyCommerceCheckoutService)Registry
            .getApplicationContext().getBean("targetLaybyCommerceCheckoutService");

    private static final ZoneDeliveryModeService ZONE_DELIVERY_MODE_SERVICE = (ZoneDeliveryModeService)Registry
            .getApplicationContext().getBean("zoneDeliveryModeService");


    private static final TargetUserFacadeImpl TARGET_USER_FACADE = (TargetUserFacadeImpl)Registry
            .getApplicationContext()
            .getBean("targetUserFacade");

    private static final BaseStoreService BASE_STORE_SERVICE = (DefaultBaseStoreService)Registry
            .getApplicationContext().getBean("baseStoreService");

    private static final PosPriceImportIntegrationFacade POS_PRICE_IMPORT_INTEGRATION_FACADE = (PosPriceImportIntegrationFacade)Registry
            .getApplicationContext().getBean("posPriceImportIntegrationFacade");

    private static final TargetProductActivationIntegrationFacade TARGET_PRODUCT_ACTIVATION_INTEGRATION_FACADE = (TargetProductActivationIntegrationFacade)Registry
            .getApplicationContext()
            .getBean("activationIntegrationFacade");

    private static final TargetPreviousPermanentPriceService TGT_PREVIOUSPERM_PRICE_SERVICE = (TargetPreviousPermanentPriceService)Registry
            .getApplicationContext().getBean("targetPreviousPermanentPriceService");

    private static final TargetSharedConfigService TARGET_SHARED_CONFIG_SERVICE = Registry.getApplicationContext()
            .getBean("targetSharedConfigService", TargetSharedConfigService.class);

    private static final TargetProductFacade TARGET_PRODUCT_FACADE = (TargetProductFacade)Registry
            .getApplicationContext().getBean("productFacade");

    private static final TestCommerceCheckoutServiceImpl TEST_COMMERCE_CHECKOUT_SERVICE = (TestCommerceCheckoutServiceImpl)Registry
            .getApplicationContext().getBean("testCommerceCheckoutServiceImpl");

    private static final TargetFraudCommerceCheckoutServiceImpl FRAUD_COMMERCE_CHECKOUT_SERVICE = (TargetFraudCommerceCheckoutServiceImpl)Registry
            .getApplicationContext().getBean("targetFraudCommerceCheckoutServiceImpl");

    private static final TestTicketBusinessServiceImpl TEST_TICKET_SERVICE = (TestTicketBusinessServiceImpl)Registry
            .getApplicationContext().getBean("testTicketBusinessService");

    private static final TargetTicketBusinessServiceImpl TARGET_TICKET_SERVICE = (TargetTicketBusinessServiceImpl)Registry
            .getApplicationContext().getBean("targetTicketBusinessService");


    private static final TargetOrderErrorHandlerFacadeImpl TARGET_ORDER_ERROR_HANDLER_FACADE = (TargetOrderErrorHandlerFacadeImpl)Registry
            .getApplicationContext().getBean("targetOrderErrorHandlerFacade");

    private static final TargetSendSmsClientMock TARGET_SEND_SMS_CLIENT = (TargetSendSmsClientMock)Registry
            .getApplicationContext().getBean("mockSendSmsCncNotificationClient");

    private static final ClickAndCollectNotificationFacadeImpl CLICK_AND_COLLECTNOTIFICATION_FACADE = (ClickAndCollectNotificationFacadeImpl)Registry
            .getApplicationContext().getBean("clickAndCollectNotificationFacadeImpl");

    private static final SendClickAndCollectSmsNotificationAction SEND_CLICK_AND_COLLECT_SMS_NOTIFICATIONACTION = (SendClickAndCollectSmsNotificationAction)Registry
            .getApplicationContext().getBean("sendClickAndCollectSmsNotificationAction");

    private static final GenerateTaxInvoiceAction GENERATE_TAX_INVOICE_ACTION = (GenerateTaxInvoiceAction)Registry
            .getApplicationContext()
            .getBean("generateTaxInvoiceAction");

    private static final TargetProductImportIntegrationFacade TARGET_PRODUCT_IMPORT_FACADE = (TargetProductImportIntegrationFacade)Registry
            .getApplicationContext().getBean("productImportIntegrationFacade");

    private static final TargetCategoryImportIntegrationFacade TARGET_CATEGORY_IMPORT_FACADE = (TargetCategoryImportIntegrationFacade)Registry
            .getApplicationContext().getBean("targetCategoryImportIntegrationFacade");

    private static final MockTargetBusinessProcessService TARGET_BUSINESS_PROCESS_SERVICE = (MockTargetBusinessProcessService)Registry
            .getApplicationContext().getBean("targetBusinessProcessService");

    private static final TargetCustomerSubscriptionFacadeImpl TARGET_CUSTOMER_SUBSCRIPTION_FACADE = (TargetCustomerSubscriptionFacadeImpl)Registry
            .getApplicationContext().getBean("targetCustomerSubscriptionFacade");

    private static final TargetCustomerSubscriptionClientMock TARGET_CUSTOMER_SUBSCRIPTION_CLIENT_MOCK = (TargetCustomerSubscriptionClientMock)Registry
            .getApplicationContext().getBean("targetCustomerSubscriptionClient");

    private static final SendCustomerSubscriptionAction SEND_CUSTOMER_SUBSCRIPTION_ACTION = (SendCustomerSubscriptionAction)Registry
            .getApplicationContext().getBean("sendCustomerSubscriptionAction");


    private static final SendCustomerSubscriptionAction SEND_CUSTOMER_NEWSLETTER_SUBSCRIPTION_ACTION = (SendCustomerSubscriptionAction)Registry
            .getApplicationContext().getBean("sendCustomerNewsletterSubscriptionAction");


    private static final SendCustomerSubscriptionAction SEND_CUSTOMER_PERSONALDETAILS_SUBSCRIPTION_ACTION = (SendCustomerSubscriptionAction)Registry
            .getApplicationContext().getBean("sendCustomerPersonalDetailsSubscriptionAction");
    private static final TargetStoreLocatorFacade TARGET_STORE_LOCATOR_FACADE = (TargetStoreLocatorFacadeImpl)Registry
            .getApplicationContext().getBean("targetStoreLocatorFacade");

    private static final TargetStoreFinderServiceImpl TARGET_STORE_FINDER_SERVICE = (TargetStoreFinderServiceImpl)Registry
            .getApplicationContext().getBean("storeFinderService");

    private static final TargetInStoreIntegrationFacade TARGET_INSTORE_INTEGRATION_FACADE = (TargetInStoreIntegrationFacade)Registry
            .getApplicationContext().getBean("targetInStoreIntegrationFacade");

    private static final ConsignmentsCreationHelper CONSIGNMENT_CREATION_HELPER = (ConsignmentsCreationHelper)Registry
            .getApplicationContext().getBean("consignmentsCreationHelper");

    private static final ConsignmentIntegrationFacade CONSIGNMENT_INTEGRATION_FACADE = (ConsignmentIntegrationFacade)Registry
            .getApplicationContext().getBean("consignmentIntegrationFacade");

    private static final OrderCreationHelper ORDER_CREATION_HELPER = (OrderCreationHelper)Registry
            .getApplicationContext().getBean("orderCreationHelper");

    private static final TargetConsignmentService TARGET_CONSIGNMENT_SERVICE = (TargetConsignmentService)Registry
            .getApplicationContext().getBean("targetConsignmentService");

    private static final TargetStoreConsignmentService TARGET_STORE_CONSIGNMENT_SERVICE = (TargetStoreConsignmentService)Registry
            .getApplicationContext().getBean("targetStoreConsignmentService");

    private static final TargetInterStoreTransferService TARGET_INTER_STORE_TRANSFER_SERVICE = (TargetInterStoreTransferService)Registry
            .getApplicationContext().getBean("targetInterStoreTransferService");

    private static final InterceptorMapping GLOBAL_STORE_FULFILMENT_INTERCEPTOR_MAPPING = (InterceptorMapping)Registry
            .getApplicationContext().getBean("globalStoreFulfilmentCapabilitiesValidateInterceptorMapping");

    private static final TargetGlobalStoreFulfilmentCapabilitiesDao TARGET_GLOBAL_STORE_FULFILMENT_DAO = (TargetGlobalStoreFulfilmentCapabilitiesDao)Registry
            .getApplicationContext().getBean("targetGlobalStoreFulfilmentCapabilitiesDao");

    private static final InterceptorMapping STORE_FULFILLMENT_CAPABILITIES_INTERCEPTOR = (InterceptorMapping)Registry
            .getApplicationContext().getBean("storeFulfilmentCapabilitiesValidateInterceptorMapping");

    private static final TargetStoreLocatorIntegrationFacade TARGET_STORE_LOCATOR_INTEGRATION_FACADE = Registry
            .getApplicationContext().getBean("targetStoreLocatorIntegrationFacade",
                    TargetStoreLocatorIntegrationFacade.class);

    private static final TargetManifestService TARGET_MANIFEST_SERVICE = (TargetManifestService)Registry
            .getApplicationContext().getBean("targetManifestService");

    private static final AutoRejectInStoreAckByWarehouse AUTO_REJECT_INSTORE_ACK_BY_WAREHOUSE = (AutoRejectInStoreAckByWarehouse)Registry
            .getApplicationContext().getBean("autoRejectInStoreAckByWarehouse");

    private static final AutoRejectInStoreWavedJob AUTO_REJECT_NOPICKED_BY_WAREHOUSE = (AutoRejectInStoreWavedJob)Registry
            .getApplicationContext().getBean("autoRejectInStoreWavedJob");

    private static final PurchaseOptionConfigServiceImpl PURCHASE_OPTION_CONFIG = (PurchaseOptionConfigServiceImpl)Registry
            .getApplicationContext().getBean("purchaseOptionConfigService");

    private static final TargetManifestDaoImpl TARGET_MANIFEST_DAO = (TargetManifestDaoImpl)Registry
            .getApplicationContext().getBean("targetManifestDao");

    private static final ManifestCreationHelper MANIFEST_CREATION_HELPER = (ManifestCreationHelper)Registry
            .getApplicationContext().getBean("manifestCreationHelper");

    private static final TestTargetCommerceCartFactory TARGET_COMMERCE_CART_FACTORY = (TestTargetCommerceCartFactory)Registry
            .getApplicationContext().getBean("cartFactory");

    private static final TargetOpeningDayService TARGET_OPENING_DAY_SERVICE = (TargetOpeningDayService)Registry
            .getApplicationContext().getBean("targetOpeningDayService");

    private static final TargetGlobalStoreFulfilmentService TARGET_GLOBAL_STORE_FULFILMENT_SERVICE = (TargetGlobalStoreFulfilmentService)Registry
            .getApplicationContext().getBean("targetGlobalStoreFulfilmentService");

    private static final TargetFindDeliveryCostStrategy TARGET_FIND_DELIVERY_STRATEGY = (TargetFindDeliveryCostStrategy)Registry
            .getApplicationContext().getBean("findDeliveryCostStrategy");

    private static final FindOrderRefundedShippingStrategy FIND_ORDER_REFUNDED_SHIPPING_STRATEGY = (FindOrderRefundedShippingStrategy)Registry
            .getApplicationContext().getBean("findOrderRefundedShippingStrategy");

    private static final SendSmsToStoreForOpenOrdersJob SEND_SMS_TO_STORE_FOR_OPEN_ORDERS_JOB = (SendSmsToStoreForOpenOrdersJob)Registry
            .getApplicationContext().getBean("sendSmsToStoreForOpenOrdersJob");

    private static final TargetDealsFacade TARGET_DEALS_FACADE = (TargetDealsFacade)Registry
            .getApplicationContext().getBean("targetDealsFacade");

    private static final TargetVoucherFacade TARGET_VOUCHER_FACADE = (TargetVoucherFacade)Registry
            .getApplicationContext().getBean("targetVoucherFacade");

    private static final TargetProductListerFacade TARGET_PRODUCT_LISTER_FACADE = Registry.getApplicationContext()
            .getBean("targetProductListerFacade", TargetProductListerFacade.class);

    private static final CommonI18NService COMMON_I18N_SERVICE = Registry.getApplicationContext().getBean(
            "commonI18NService", CommonI18NService.class);

    private static final TargetPrepopulateCheckoutCartFacade TARGET_PREPOPULATE_CHECKOUT_FACADE = (TargetPrepopulateCheckoutCartFacade)Registry
            .getApplicationContext().getBean("targetPrepopulateCheckoutCartFacade");

    private static final RetransmitManifestsJob RETRANSMIT_MANIFEST_JOB = (RetransmitManifestsJob)Registry
            .getApplicationContext().getBean("retransmitManifestsJob");

    private static final TargetConsignmentDaoImpl TARGET_CONSIGNMENT_DAO = (TargetConsignmentDaoImpl)Registry
            .getApplicationContext().getBean("targetConsignmentDao");

    private static final ProductTypeService PRODUCT_TYPE_SERVICE = (ProductTypeService)Registry
            .getApplicationContext().getBean("productTypeService");


    private static final IpgClientMock IPG_CLIENT_MOCK = (IpgClientMock)Registry
            .getApplicationContext().getBean("mockIpgClient");

    private static final PayPalServiceMock PAYPAL_SERVICE_MOCK = (PayPalServiceMock)Registry
            .getApplicationContext().getBean("mockPayPalService");

    private static final TargetLookPageFacade TARGET_LOOK_PAGE_FACADE = (TargetLookPageFacade)Registry
            .getApplicationContext()
            .getBean("targetLookPageFacade");

    private static final GiftCardService GIFT_CARD_SERVICE = (GiftCardService)Registry
            .getApplicationContext().getBean("giftCardService");

    private static final TargetSplitWithRoutingStrategy TARGET_SPLIT_WITH_ROUTING_STRATEGY = (TargetSplitWithRoutingStrategy)Registry
            .getApplicationContext().getBean("targetSplitWithRoutingStrategy");

    private static final ExternalWarehouseSelectorStrategy EXTERNAL_WAREHOUSE_SELECTOR = (ExternalWarehouseSelectorStrategy)Registry
            .getApplicationContext().getBean("externalWarehouseSelectorStrategy");

    private static final TestTargetPartnerOrderServiceImpl TARGET_PARTNER_ORDER_SERVICE = (TestTargetPartnerOrderServiceImpl)Registry
            .getApplicationContext().getBean("targetPartnerOrderService");

    private static final StockUpdateIntegrationFacadeImpl STOCK_UPDATE_INT_FACADE = (StockUpdateIntegrationFacadeImpl)Registry
            .getApplicationContext()
            .getBean("stockUpdateIntegrationFacade");

    private static final OrderAckIntegrationFacade ORDER_ACK_INT_FACADE = (OrderAckIntegrationFacade)Registry
            .getApplicationContext()
            .getBean("orderAckIntegrationFacade");

    private static final ShipConfirmIntegrationFacade ORDER_SHIP_INT_FACADE = (ShipConfirmIntegrationFacade)Registry
            .getApplicationContext()
            .getBean("shipConfirmIntegrationFacade");

    private static final PickConfirmIntegrationFacadeImpl PICK_CONF_INTEGRATION_FACADE = (PickConfirmIntegrationFacadeImpl)Registry
            .getApplicationContext()
            .getBean("pickConfirmIntegrationFacade");

    private static final ReverseGiftCardAction REVERSE_GIFT_CARD_ACTION = (ReverseGiftCardAction)Registry
            .getApplicationContext()
            .getBean("reverseGiftCardAction");

    private static final TargetWishListFacade WISH_LIST_FACADE = (TargetWishListFacade)Registry
            .getApplicationContext()
            .getBean("targetWishListFacade");

    private static final TargetWishListServiceImpl TARGET_WISHLIST_SERVICE = (TargetWishListServiceImpl)Registry
            .getApplicationContext()
            .getBean("targetWishListService");

    private static final TargetCsCheckoutService TARGET_CS_COCKPIT_SERVICE = (TargetCsCheckoutService)Registry
            .getApplicationContext().getBean("targetCsCheckoutService");

    private static final TargetFeatureSwitchService TARGET_FEATURE_SWITCH_SERVICES = (TargetFeatureSwitchService)Registry
            .getApplicationContext().getBean("targetFeatureSwitchService");

    private static final TargetShareWishlistClientMock SHARE_WISHLIST_REST_CLIENT = (TargetShareWishlistClientMock)Registry
            .getApplicationContext().getBean("shareWishListClient");

    private static final UpdatePartnerWithShipmentInfoAction UPDATE_PARTNER_WITH_SHIPMENT_INFO_ACTION = (UpdatePartnerWithShipmentInfoAction)Registry
            .getApplicationContext().getBean("updatePartnerWithShipmentInfoAction");

    private static final UpdateInventoryToPartnerAction UPDATE_INVENTORY_TO_PARTNER_ACTION = (UpdateInventoryToPartnerAction)Registry
            .getApplicationContext().getBean("updateInventoryToPartnerAction");

    private static final TargetDealService TARGET_DEAL_SERVICE = Registry.getApplicationContext()
            .getBean("targetDealService", TargetDealService.class);

    private static final ConfigurationService CONFIG_SERVICE = Registry.getApplicationContext()
            .getBean("configurationService", ConfigurationService.class);

    private static final CronJobService CRONJOB_SERVICE = Registry.getApplicationContext()
            .getBean("cronJobService", CronJobService.class);

    private static final AutoUpdatePartnerPickedupJob AUTOUPDATEPICKEDUPJOB = Registry.getApplicationContext()
            .getBean("autoUpdatePartnerPickedupJob", AutoUpdatePartnerPickedupJob.class);

    private static final UpdatePartnerWithPickedupStatusAction UPDATE_PARTNER_WITH_PICKEDUP_STATUS_ACTION = (UpdatePartnerWithPickedupStatusAction)Registry
            .getApplicationContext().getBean("updatePartnerWithPickedupStatusAction");

    private static final UpdatePartnerWithReadyForPickupStatusAction UPDATE_PARTNER_WITH_READY_FOR_PICKEDUP_STATUS_ACTION = (UpdatePartnerWithReadyForPickupStatusAction)Registry
            .getApplicationContext().getBean("updatePartnerWithReadyForPickupStatusAction");

    private static final TargetCheckoutLoginResponseFacade TARGET_CHECKOUT_LOGIN_RESPONSE_FACADE = Registry
            .getApplicationContext()
            .getBean("targetCheckoutLoginResponseFacade", TargetCheckoutLoginResponseFacadeImpl.class);

    private static final TargetAuthenticationFacade TARGET_AUTHENTICATION_FACADE = Registry.getApplicationContext()
            .getBean("targetAuthenticationFacade", TargetAuthenticationFacadeImpl.class);

    private static final MockWMSendToWarehouseLogger TARGET_WM_SEND_TO_WAREHOUSE_LOGGER = Registry
            .getApplicationContext()
            .getBean("wmSendToWarehouseLogger", MockWMSendToWarehouseLogger.class);

    private static final CheckoutConfigurationFacade CHECKOUT_CONFIGURATION_FACADE = Registry.getApplicationContext()
            .getBean("checkoutConfigurationFacade", CheckoutConfigurationFacade.class);

    private static final ResendNotAcknowledgedOrderExtractsJob RESEND_NOT_ACKNOWLEDGED_ORDER_EXTRACTS_JOB = Registry
            .getApplicationContext()
            .getBean("resendNotAcknowledgedOrderExtractsJob", ResendNotAcknowledgedOrderExtractsJob.class);

    private static final TargetBcryptPasswordEncoder TARGET_BCRYPT_PASSWORD_ENCODER = Registry.getApplicationContext()
            .getBean("tgtcore.targetBcryptPasswordEncoder", TargetBcryptPasswordEncoder.class);

    private static final TargetMD5PasswordEncoder TARGET_MD5_PASSWORD_ENCODER = Registry.getApplicationContext()
            .getBean("tgtcore.targetMD5PasswordEncoder", TargetMD5PasswordEncoder.class);

    private static final StockVisibilityClientMock STOCK_VISIBILITY_CLIENT_MOCK = Registry.getApplicationContext()
            .getBean("mockStockVisibilityClient", StockVisibilityClientMock.class);

    private static final TargetStoreFinderStockFacade TARGET_STORE_FINDER_STOCK_FACADE = Registry
            .getApplicationContext().getBean("targetStoreFinderStockFacade", TargetStoreFinderStockFacade.class);

    private static final DefaultTargetStockLookUpFacade TARGET_STOCKLOOKUP_FACADE = Registry
            .getApplicationContext().getBean("targetStockLookupFacade", DefaultTargetStockLookUpFacade.class);

    private static final TargetShopTheLookFacade TARGET_SHOP_THE_LOOK_FACADE = Registry
            .getApplicationContext().getBean("targetShopTheLookFacade", TargetShopTheLookFacade.class);

    private static final TargetSocialMediaProductsFacade TARGET_SOCIAL_MEDIA_PRODUCTS_FACADE = Registry
            .getApplicationContext().getBean("targetSocialMediaProductsFacade", TargetSocialMediaProductsFacade.class);

    private static final ShopTheLookImportIntegrationFacade SHOP_THE_LOOK_IMPORT_INTEGRATION_FACADE = Registry
            .getApplicationContext()
            .getBean("shopTheLookImportIntegrationFacade", ShopTheLookImportIntegrationFacade.class);

    private static final ClassificationService TARGET_CLASSIFICATION_SERVICE = Registry
            .getApplicationContext().getBean("classificationService", ClassificationService.class);

    private static final TargetFeatureSwitchDao TARGET_FEATURE_SWITCH_DAO = Registry
            .getApplicationContext().getBean("targetFeatureSwitchDao", TargetFeatureSwitchDao.class);

    private static final TargetInventoryAdjustmentClientImpl TARGET_INVENTORY_ADJUSTMENT_CLIENT = (TargetInventoryAdjustmentClientImpl)Registry
            .getApplicationContext().getBean("inventoryAdjustmentClient");


    private static final TargetAfterpayClientMock TARGET_AFTERPAY_CLIENT_MOCK = (TargetAfterpayClientMock)Registry
            .getApplicationContext().getBean("targetAfterpayClient");

    private static final TargetZippayClientMock TARGET_ZIPPAY_CLIENT_MOCK = (TargetZippayClientMock)Registry
            .getApplicationContext().getBean("targetZippayClient");

    private static final AfterpayConfigDaoImpl AFTERPAY_CONFIG_DAO_IMPL = (AfterpayConfigDaoImpl)Registry
            .getApplicationContext().getBean("afterpayConfigDao");

    private static final TargetAfterpayConfigurationJob TARGET_AFTERPAY_CONFIGURATION_JOB = (TargetAfterpayConfigurationJob)Registry
            .getApplicationContext().getBean("targetAfterpayConfigurationJob");

    private static final AfterpayConfigServiceImpl AFTERPAY_CONFIG_SERVICE_IMPL = (AfterpayConfigServiceImpl)Registry
            .getApplicationContext().getBean("afterpayConfigService");

    private static final TargetOriginalCategoryPopulatorServiceImpl TARGET_ORIGINAL_CATEGORY_PRODUCT_SERVICE = Registry
            .getApplicationContext()
            .getBean("targetOriginalCategoryPopulatorService", TargetOriginalCategoryPopulatorServiceImpl.class);

    private static final TargetAfterpayHelper TARGET_AFTERPAY_HELPER = (TargetAfterpayHelper)Registry
            .getApplicationContext().getBean("targetAfterpayHelper");

    private static final MockAusPostShipsterClient MOCK_AUSPOST_DELIVERY_CLUB_CLIENT = (MockAusPostShipsterClient)Registry
            .getApplicationContext().getBean("ausPostShipsterClient");

    private static final ShipsterConfigService SHIPSTER_CONFIG_SERVICE = Registry.getApplicationContext().getBean(
            "shipsterConfigService",
            ShipsterConfigService.class);

    private static final FluentClientMock FLUENT_CLIENT_MOCK = (FluentClientMock)Registry.getApplicationContext()
            .getBean("fluentClient");

    private static final LocationFeedJob LOCATION_FEED_JOB = (LocationFeedJob)Registry.getApplicationContext()
            .getBean("locationFeedJob");

    private static final FluentBatchResponseService FLUENT_BATCH_RESPONSE_SERVICE = (FluentBatchResponseService)Registry
            .getApplicationContext().getBean("fluentBatchResponseService");


    private static final FluentUpdateStatusService FLUENT_UPDATE_STATUS_SERVICE = (FluentUpdateStatusService)Registry
            .getApplicationContext().getBean("fluentUpdateStatusService");

    private static final LocationFeedStatusCheckJob LOCATION_FEED_STATUS_CHECK_JOB = (LocationFeedStatusCheckJob)Registry
            .getApplicationContext().getBean("locationFeedStatusCheckJob");

    private static final EndecaProductQueryBuilderMock ENDECA_PRODUCT_QUERY_BUILDER = (EndecaProductQueryBuilderMock)Registry
            .getApplicationContext().getBean("endecaProductQueryBuilder");

    private static final FluentFulfilmentService FLUENT_FULFILMENT_SERVICE = (FluentFulfilmentService)Registry
            .getApplicationContext().getBean("fluentFulfilmentService");

    private static final TargetOrderEntryService TARGET_ORDER_ENTRY_SERVICE = (TargetOrderEntryService)Registry
            .getApplicationContext().getBean("targetOrderEntryService");

    private static final FluentOrderService FLUENT_ORDER_SERVICE = (FluentOrderService)Registry
            .getApplicationContext().getBean("fluentOrderService");

    private ServiceLookup() {
        // utility
    }

    /**
     * @return the resendNotAcknowledgedOrderExtractsJob
     */
    public static ResendNotAcknowledgedOrderExtractsJob getResendNotAcknowledgedOrderExtractsJob() {
        return RESEND_NOT_ACKNOWLEDGED_ORDER_EXTRACTS_JOB;
    }

    public static ModelService getModelService() {
        return MODEL_SERVICE;
    }

    public static FlexibleSearchService getFlexibleSearchService() {
        return FLEXIBLE_SEARCH_SERVICE;
    }

    public static PurchaseOptionService getPurchaseOptionService() {
        return PURCHASE_OPTION_SERVICE;
    }

    public static ProductService getProductService() {
        return PRODUCT_SERVICE;
    }

    public static TargetCommerceCartService getTargetCommerceCartService() {
        return TARGET_COMMERCE_CART_SERVICE;
    }

    public static CatalogVersionService getCatalogVersionService() {
        return CATALOG_VERSION_SERVICE;
    }

    public static VoucherService getVoucherService() {
        return VOUCHER_SERVICE;
    }

    public static VoucherModelService getVoucherModelService() {
        return VOUCHER_MODEL_SERVICE;
    }

    public static SetupImpexService getSetupImpexService() {
        return SETUP_IMPEX_SERVICE;
    }

    public static TargetCommerceCheckoutService getTargetCommerceCheckoutService() {
        return TARGET_COMMERCE_CHECKOUT_SERVICE;
    }

    public static TargetOrderService getTargetOrderService() {
        return TARGET_ORDER_SERVICE;
    }

    public static TargetTransactionLogService getTargetTransactionLogService() {
        return TARGET_TRANSACTION_LOG_SERVICE;
    }

    public static TargetPOSExtractsService getTargetPosExtractService() {
        return TARGET_POS_EXTRACT_SERVICE;
    }

    public static TargetMessageConverter getTargetMessageConverterService() {
        return TARGET_MESSAGE_CONVERTER_SERVICE;
    }

    public static TargetLaybyCartService getTargetLaybyCartService() {
        return TARGET_LAYBY_CART_SERVICE;
    }

    public static TargetDeliveryService getTargetDeliveryService() {
        return TARGET_DELIVERY_SERVICE;
    }

    public static TargetDeliveryFacade getTargetDeliveryFacade() {
        return TARGET_DELIVERY_FACADE;
    }

    public static TargetProductService getTargetProductService() {
        return TARGET_PRODUCT_SERVICE;
    }

    public static TargetProductPriceService getTargetProductPriceService() {
        return TARGET_PRODUCT_PRICE_SERVICE;
    }

    public static TargetCreditCardPaymentMethodMock getTargetMockCcpayment() {
        return TARGET_MOCK_CCPAYMENT;
    }

    public static TargetCustomerAccountService getTargetCustomerAccountService() {
        return TARGET_CUSTOMER_ACCOUNT_SERVICE;
    }

    public static TargetOrderCancelService getTargetOrderCancelService() {
        return TARGET_ORDER_CANCEL_SERVICE;
    }

    public static UserService getUserService() {
        return USER_SERVICE;
    }

    public static AccertifyFraudServiceProviderMock getTargetMockAccertify() {
        return TARGET_MOCK_ACCERTIFY;
    }

    public static TargetOrderReturnService getTargetOrderReturnService() {
        return TARGET_ORDER_RETURN_SERVICE;
    }

    public static TargetRefundService getTargetRefundService() {
        return TARGET_REFUND_SERVICE;
    }

    public static PaymentModeService getPaymentModeService() {
        return PAYMENT_MODE_SERVICE;
    }

    public static CategoryService getCategoryService() {
        return CATEGORY_SERVICE;
    }

    public static StockService getStockService() {
        return STOCK_SERVICE;
    }

    public static TargetWarehouseService getTargetWarehouseService() {
        return TARGET_WAREHOUSE_SERVICE;
    }

    public static TargetPOSProductService getTargetPOSProductService() {
        return TARGET_POS_PRODUCT_SERVICE;
    }

    public static TargetPointOfServiceService getTargetPointOfServiceService() {
        return TARGET_POINT_OF_SERVICE_SERVICE;
    }

    public static MockTargetStoreStockService getTargetStoreStockService() {
        return TARGET_STORE_STOCK_SERVICE;
    }

    public static PinPadPaymentFacadeImpl getPinpadPaymentFacade() {
        return PINPAD_PAYMENT_FACADE;
    }

    public static MockTargetStockUpdateClient getTargetStockUpdateClient() {
        return TARGET_STOCK_UPDATE_CLIENT;
    }

    public static MockExceptionThrowingTargetStockService getMockTargetService() {
        return MOCK_EXCEPTION_TARGET_STOCK_SERVICE;
    }

    public static MockTransmitManifestClient getTransmitManifestClient() {
        return TRANSMIT_MANIFEST_CLIENT;
    }

    public static MockDispatchLabelClient getDispatchLabelClient() {
        return DISPATCH_LABEL_CLIENT;
    }

    public static MockSendToWarehouseRestClient getSendToWarehouseRestClient() {
        return SEND_TO_WAREHOUSE_REST_CLIENT;
    }

    public static TargetPlaceOrderFacadeImpl getTargetPlaceOrderFacade() {
        return TARGET_PLACE_ORDER_FACADE;
    }

    public static SessionService getSessionService() {
        return SESSION_SERVICE;
    }

    public static TargetCartFacadeImpl getTargetCartFacade() {
        return TARGET_CART_FACADE;
    }

    public static TargetCheckoutFacadeImpl getTargetCheckoutFacade() {
        return TARGET_CHECKOUT_FACADE;
    }

    public static AccertifyFraudServiceProvider getTargetAccertify() {
        return TARGET_ACCERTIFY;
    }

    public static TargetFulfilmentService getTargetFulfilmentService() {
        return TARGET_FULFILMENT_SERVICE;
    }

    public static SendToWarehouseProtocol getEsbFulfilmentProcessService() {
        return ESB_FULFILMENT_PROCESS_SERVICE;
    }

    public static TicketService getTicketService() {
        return TICKET_SERVICE;
    }

    /**
     *
     */
    public static TargetSalesApplicationService getTargetSalesApplicationService() {
        return TARGET_SALES_APPLICATION_SERVICE;

    }

    public static FlybuysClientMock getFlybuysclientMockService() {
        return FLYBUYS_MOCK_CLIENT;
    }

    public static FlybuysDiscountFacadeImpl getFlybuysDiscountFacade() {
        return FLYBUYS_DISCOUNT_FACADE;
    }



    public static FlybuysRedeemConfigService getFlybuysRedeemConfigService() {
        return FLYBUYS_REDEEM_CONFIG_SERVICE;
    }

    public static TargetStockService getTargetStockService() {
        return TARGET_STOCK_SERVICE;
    }

    public static TargetWarehouseService getWarehouseService() {
        return WAREHOUSE_SERVICE;
    }

    /**
     * @return the baseSiteService
     */
    public static DefaultBaseSiteService getBaseSiteService() {
        return BASE_SITE_SERVICE;
    }

    /**
     * @return the targetCustomerFacade
     */
    public static DefaultTargetCustomerFacade getTargetCustomerFacade() {
        return TARGET_CUSTOMER_FACADE;
    }


    public static TargetLaybyCommerceCheckoutService getTargetLaybyCommerceCheckoutService() {
        return TARGET_LAYBY_COMMERCE_CHECKOUT_SERVICE;
    }

    public static ZoneDeliveryModeService getZoneDeliveryModeService() {
        return ZONE_DELIVERY_MODE_SERVICE;
    }

    /**
     * @return the targetUserFacade
     */
    public static TargetUserFacadeImpl getTargetUserFacade() {
        return TARGET_USER_FACADE;
    }

    /**
     * 
     * @return BaseStoreService
     */
    public static BaseStoreService getBaseStoreService() {
        return BASE_STORE_SERVICE;

    }

    /**
     * @return the posPriceImportIntegrationFacade
     */
    public static PosPriceImportIntegrationFacade getPosPriceImportIntegrationFacade() {
        return POS_PRICE_IMPORT_INTEGRATION_FACADE;
    }

    /**
     * @return the tgtPreviouspermPriceService
     */
    public static TargetPreviousPermanentPriceService getTgtPreviouspermPriceService() {
        return TGT_PREVIOUSPERM_PRICE_SERVICE;
    }

    /**
     * @return the targetProductActivationIntegrationFacade
     */
    public static TargetProductActivationIntegrationFacade getTargetProductActivationIntegrationFacade() {
        return TARGET_PRODUCT_ACTIVATION_INTEGRATION_FACADE;
    }

    /**
     * 
     * @return TARGET_SHARED_CONFIG_SERVICE
     */
    public static TargetSharedConfigService getTargetSharedConfigService() {
        return TARGET_SHARED_CONFIG_SERVICE;
    }

    /**
     * 
     * @return TARGET_PRODUCT_FACADE
     */
    public static TargetProductFacade getTargetProductFacade() {
        return TARGET_PRODUCT_FACADE;
    }

    /**
     * @return TEST_COMMERCE_CHECKOUT_SERVICE
     */
    public static TestCommerceCheckoutServiceImpl getTestCommerceCheckoutServiceImpl() {
        return TEST_COMMERCE_CHECKOUT_SERVICE;
    }

    /**
     * @return FRAUD_COMMERCE_CHECKOUT_SERVICE
     */
    public static TargetFraudCommerceCheckoutServiceImpl getFraudCommerceCheckoutService() {
        return FRAUD_COMMERCE_CHECKOUT_SERVICE;
    }

    /**
     * @return TEST_TICKET_SERVICE
     */
    public static TestTicketBusinessServiceImpl getTestTicketService() {
        return TEST_TICKET_SERVICE;
    }

    /**
     * @return TARGET_TICKET_SERVICE
     */
    public static TargetTicketBusinessServiceImpl getTargetTicketService() {
        return TARGET_TICKET_SERVICE;
    }

    /**
     * @return TARGET_ORDER_ERROR_HANDLER_FACADE
     */
    public static TargetOrderErrorHandlerFacadeImpl getTargetOrderErrorHandlerFacade() {
        return TARGET_ORDER_ERROR_HANDLER_FACADE;
    }

    /**
     * @return TARGET_PRODUCT_IMPORT_FACADE
     */
    public static TargetProductImportIntegrationFacade getTargetProductImportFacade() {
        return TARGET_PRODUCT_IMPORT_FACADE;
    }

    /**
     * 
     * @return TARGET_CATEGORY_IMPORT_FACADE
     */
    public static TargetCategoryImportIntegrationFacade getTargetCategoryImportIntegrationFacade() {
        return TARGET_CATEGORY_IMPORT_FACADE;
    }

    public static TargetSendSmsClientMock getTargetSendSmsClient() {
        return TARGET_SEND_SMS_CLIENT;
    }

    public static ClickAndCollectNotificationFacadeImpl getClickAndCollectnotificationFacade() {
        return CLICK_AND_COLLECTNOTIFICATION_FACADE;
    }

    public static SendClickAndCollectSmsNotificationAction getSendClickAndCollectSmsNotificationaction() {
        return SEND_CLICK_AND_COLLECT_SMS_NOTIFICATIONACTION;
    }

    public static MockTargetBusinessProcessService getTargetBusinessProcessService() {
        return TARGET_BUSINESS_PROCESS_SERVICE;
    }

    public static TargetCustomerSubscriptionFacadeImpl getTargetCustomerSubscriptionFacade() {
        return TARGET_CUSTOMER_SUBSCRIPTION_FACADE;
    }

    public static TargetCustomerSubscriptionClientMock getTargetCustomerSubscriptionClientMock() {
        return TARGET_CUSTOMER_SUBSCRIPTION_CLIENT_MOCK;
    }

    public static SendCustomerSubscriptionAction getSendCustomerSubscriptionAction() {
        return SEND_CUSTOMER_SUBSCRIPTION_ACTION;
    }

    public static GenerateTaxInvoiceAction getGenerateTaxInvoiceAction() {
        return GENERATE_TAX_INVOICE_ACTION;
    }

    public static TargetStoreLocatorFacade getTargetStoreLocatorFacade() {
        return TARGET_STORE_LOCATOR_FACADE;
    }

    public static TargetStoreFinderServiceImpl getTargetStoreFinderService() {
        return TARGET_STORE_FINDER_SERVICE;
    }

    public static TargetInStoreIntegrationFacade getTargetInStoreIntegrationFacade() {
        return TARGET_INSTORE_INTEGRATION_FACADE;
    }

    public static ConsignmentsCreationHelper getConsignmentCreationHelper() {
        return CONSIGNMENT_CREATION_HELPER;
    }

    public static OrderCreationHelper getOrderCreationHelper() {
        return ORDER_CREATION_HELPER;
    }

    public static TargetConsignmentService getTargetConsignmentService() {
        return TARGET_CONSIGNMENT_SERVICE;
    }

    public static ConsignmentIntegrationFacade getConsignmentIntegrationFacade() {
        return CONSIGNMENT_INTEGRATION_FACADE;
    }

    /**
     * @return the targetStoreConsignmentService
     */
    public static TargetStoreConsignmentService getTargetStoreConsignmentService() {
        return TARGET_STORE_CONSIGNMENT_SERVICE;
    }

    /**
     * @return the targetInterStoreTransferService
     */
    public static TargetInterStoreTransferService getTargetInterStoreTransferService() {
        return TARGET_INTER_STORE_TRANSFER_SERVICE;
    }

    public static InterceptorMapping getStoreFulfillmentCapabilitiesInterceptor() {
        return STORE_FULFILLMENT_CAPABILITIES_INTERCEPTOR;
    }

    public static InterceptorMapping getGlobalStoreFulfilmentInterceptorMapping() {
        return GLOBAL_STORE_FULFILMENT_INTERCEPTOR_MAPPING;
    }

    public static TargetGlobalStoreFulfilmentCapabilitiesDao getTargetGlobalStoreFulfilmentCapabilitiesDao() {
        return TARGET_GLOBAL_STORE_FULFILMENT_DAO;
    }

    public static AutoRejectInStoreAckByWarehouse getAutoRejectInStoreAckByWarehouse() {
        return AUTO_REJECT_INSTORE_ACK_BY_WAREHOUSE;
    }

    public static AutoRejectInStoreWavedJob getAutoRejectNonPickedByWarehouse() {
        return AUTO_REJECT_NOPICKED_BY_WAREHOUSE;
    }

    public static PurchaseOptionConfigServiceImpl getPurchaseOptionConfigService() {
        return PURCHASE_OPTION_CONFIG;
    }

    public static TargetStoreLocatorIntegrationFacade getTargetStoreLocatorIntegrationFacade() {
        return TARGET_STORE_LOCATOR_INTEGRATION_FACADE;
    }

    public static TargetManifestService getTargetManifestService() {
        return TARGET_MANIFEST_SERVICE;
    }

    public static TargetManifestDaoImpl getTargetManifestDao() {
        return TARGET_MANIFEST_DAO;
    }

    public static ManifestCreationHelper getManifestCreationHelper() {
        return MANIFEST_CREATION_HELPER;
    }

    public static TestTargetCommerceCartFactory getTargetCommerceCartFactory() {
        return TARGET_COMMERCE_CART_FACTORY;
    }

    public static TargetOpeningDayService getTargetOpeningDayService() {
        return TARGET_OPENING_DAY_SERVICE;
    }

    /**
     * @return the targetGlobalStoreFulfilmentService
     */
    public static TargetGlobalStoreFulfilmentService getTargetGlobalStoreFulfilmentService() {
        return TARGET_GLOBAL_STORE_FULFILMENT_SERVICE;
    }

    public static TargetFindDeliveryCostStrategy getTargetFindDeliveryCostStrategy() {
        return TARGET_FIND_DELIVERY_STRATEGY;
    }

    public static FindOrderRefundedShippingStrategy getFindOrderRefundedShippingStrategy() {
        return FIND_ORDER_REFUNDED_SHIPPING_STRATEGY;
    }

    public static SendSmsToStoreForOpenOrdersJob getSendSmsToStoreForOpenOrdersJob() {
        return SEND_SMS_TO_STORE_FOR_OPEN_ORDERS_JOB;
    }

    /**
     * @return the targetDealsfacade
     */
    public static TargetDealsFacade getTargetDealsfacade() {
        return TARGET_DEALS_FACADE;
    }

    /**
     * @return the targetVoucherFacade
     */
    public static TargetVoucherFacade getTargetVoucherFacade() {
        return TARGET_VOUCHER_FACADE;
    }

    public static TargetPostCodeService getTargetPostCodeService() {
        return TARGET_POSTCODE_SERVICE;
    }

    /**
     * @return the targetProductListerFacade
     */
    public static TargetProductListerFacade getTargetProductListerFacade() {
        return TARGET_PRODUCT_LISTER_FACADE;
    }

    /**
     * @return the commonI18nService
     */
    public static CommonI18NService getCommonI18nService() {
        return COMMON_I18N_SERVICE;
    }

    /**
     * @return the pre populate checkout facade
     */
    public static TargetPrepopulateCheckoutCartFacade getPrepopulateCheckoutFacade() {
        return TARGET_PREPOPULATE_CHECKOUT_FACADE;
    }

    /**
     * @return the checkout response facade
     */
    public static TargetCheckoutResponseFacade getCheckoutResponseFacade() {
        return TARGET_CHECKOUT_RESPONSE_FACADE;
    }

    /**
     * @return the sendCustomerNewsletterSubscriptionAction
     */
    public static SendCustomerSubscriptionAction getSendCustomerNewsletterSubscriptionAction() {
        return SEND_CUSTOMER_NEWSLETTER_SUBSCRIPTION_ACTION;
    }

    /**
     * @return the sendCustomerPersonaldetailsSubscriptionAction
     */
    public static SendCustomerSubscriptionAction getSendCustomerPersonaldetailsSubscriptionAction() {
        return SEND_CUSTOMER_PERSONALDETAILS_SUBSCRIPTION_ACTION;
    }

    /*
     * @return RetransmitManifestsJob
     */
    public static RetransmitManifestsJob getRetransmitManifestsJob() {
        return RETRANSMIT_MANIFEST_JOB;
    }

    public static ProductTypeService getProductTypeService() {
        return PRODUCT_TYPE_SERVICE;
    }

    /**
     * @return the targetConsignmentDao
     */
    public static TargetConsignmentDaoImpl getTargetConsignmentDao() {
        return TARGET_CONSIGNMENT_DAO;
    }

    /**
     * @return the targetOrderFacade
     */
    public static TargetOrderFacade getTargetOrderFacade() {
        return TARGET_ORDER_FACADE;
    }

    /**
     * @return the ipgClientMock
     */
    public static IpgClientMock getIpgClientMock() {
        return IPG_CLIENT_MOCK;
    }

    /**
     * @return the targetLookPageFacade
     */
    public static TargetLookPageFacade getTargetLookPageFacade() {
        return TARGET_LOOK_PAGE_FACADE;
    }

    /**
     * @return the giftCardService
     */
    public static GiftCardService getGiftCardService() {
        return GIFT_CARD_SERVICE;
    }

    /**
     * @return the targetSplitWithRoutingStrategy
     */
    public static TargetSplitWithRoutingStrategy getTargetSplitWithRoutingStrategy() {
        return TARGET_SPLIT_WITH_ROUTING_STRATEGY;
    }

    /**
     * @return the giftcardWarehouseSelector
     */
    public static ExternalWarehouseSelectorStrategy getExternalWarehouseSelector() {
        return EXTERNAL_WAREHOUSE_SELECTOR;
    }

    /**
     * @return the targetPartnerOrderService
     */
    public static TestTargetPartnerOrderServiceImpl getTargetPartnerOrderService() {
        return TARGET_PARTNER_ORDER_SERVICE;
    }

    /**
     * @return the stockUpdateIntegrationFacade
     */
    public static StockUpdateIntegrationFacadeImpl getStockUpdateIntegrationFacade() {
        return STOCK_UPDATE_INT_FACADE;
    }

    /**
     * @return the orderAckIntFacade
     */
    public static OrderAckIntegrationFacade getOrderAckIntFacade() {
        return ORDER_ACK_INT_FACADE;
    }

    /**
     * @return the orderShipIntFacade
     */
    public static ShipConfirmIntegrationFacade getOrderShipIntFacade() {
        return ORDER_SHIP_INT_FACADE;
    }

    /**
     * @return the pickConfIntegrationFacade
     */
    public static PickConfirmIntegrationFacadeImpl getPickConfIntegrationFacade() {
        return PICK_CONF_INTEGRATION_FACADE;
    }

    /**
     * @return the reverseGiftCardAction
     */
    public static ReverseGiftCardAction getReverseGiftCardAction() {
        return REVERSE_GIFT_CARD_ACTION;
    }

    public static TargetWishListFacade getTargetWishListFacade() {
        return WISH_LIST_FACADE;
    }

    public static TargetFeatureSwitchService getTargetFeatureServices() {
        return TARGET_FEATURE_SWITCH_SERVICES;
    }

    /**
     * @return TargetCheckoutResponseFacade
     */
    public static TargetCheckoutResponseFacade getTargetCheckoutResponseFacade() {
        return TARGET_CHECKOUT_RESPONSE_FACADE;
    }

    /**
     * 
     * @return TargetWishListServiceImpl
     */
    public static TargetWishListServiceImpl getTargetWishListService() {

        return TARGET_WISHLIST_SERVICE;
    }

    public static TargetShareWishlistClientMock getShareFavouritesRestClient() {
        return SHARE_WISHLIST_REST_CLIENT;
    }

    /**
     * @return the updatePartnerWithShipmentInfoAction
     */
    public static UpdatePartnerWithShipmentInfoAction getUpdatePartnerWithShipmentInfoAction() {
        return UPDATE_PARTNER_WITH_SHIPMENT_INFO_ACTION;
    }



    public static TargetDealService getTargetDealService() {
        return TARGET_DEAL_SERVICE;
    }

    /**
     * @return the updateInventoryToPartnerAction
     */
    public static UpdateInventoryToPartnerAction getUpdateInventoryToPartnerAction() {
        return UPDATE_INVENTORY_TO_PARTNER_ACTION;
    }

    /**
     * @return the configService
     */
    public static ConfigurationService getConfigService() {
        return CONFIG_SERVICE;
    }

    /**
     * @return the cronjobService
     */
    public static CronJobService getCronjobService() {
        return CRONJOB_SERVICE;
    }

    /**
     * @return the autoupdatepickedupjob
     */
    public static AutoUpdatePartnerPickedupJob getAutoUpdatePickedupjob() {
        return AUTOUPDATEPICKEDUPJOB;
    }

    /**
     * @return the updatePartnerWithPickedupStatusAction
     */
    public static UpdatePartnerWithPickedupStatusAction getUpdatePartnerWithPickedupStatusAction() {
        return UPDATE_PARTNER_WITH_PICKEDUP_STATUS_ACTION;
    }

    /**
     * @return the updatePartnerWithReadyForPickedupStatusAction
     */
    public static UpdatePartnerWithReadyForPickupStatusAction getUpdatePartnerWithReadyForPickedupStatusAction() {
        return UPDATE_PARTNER_WITH_READY_FOR_PICKEDUP_STATUS_ACTION;
    }

    /**
     * @return the paypalServiceMock
     */
    public static PayPalServiceMock getPaypalServiceMock() {
        return PAYPAL_SERVICE_MOCK;
    }

    public static TargetCsCheckoutService getTargetCsCheckoutService() {
        return TARGET_CS_COCKPIT_SERVICE;
    }

    /**
     * @return the targetCheckoutLoginResponseFacade
     */
    public static TargetCheckoutLoginResponseFacade getTargetCheckoutLoginResponseFacade() {
        return TARGET_CHECKOUT_LOGIN_RESPONSE_FACADE;
    }

    /**
     * @return the targetAuthenticationFacade
     */
    public static TargetAuthenticationFacade getTargetAuthenticationFacade() {
        return TARGET_AUTHENTICATION_FACADE;
    }

    /**
     * @return the targetWmSendToWarehouseLogger
     */
    public static MockWMSendToWarehouseLogger getTargetWmSendToWarehouseLogger() {
        return TARGET_WM_SEND_TO_WAREHOUSE_LOGGER;
    }

    /**
     * @return the checkoutConfigurationFacade
     */
    public static CheckoutConfigurationFacade getCheckoutConfigurationFacade() {
        return CHECKOUT_CONFIGURATION_FACADE;
    }

    /**
     * 
     * @return TargetCarrierSelectionService
     */
    public static TargetCarrierSelectionService getTargetCarrierSelectionService() {
        return TARGET_CARRIER_SELECTION_SERVICE;
    }

    /*
     * @return the targetBcryptPasswordEncoder
     */
    public static TargetBcryptPasswordEncoder getTargetBcryptPasswordEncoder() {
        return TARGET_BCRYPT_PASSWORD_ENCODER;
    }

    /**
     * @return the targetMd5PasswordEncoder
     */
    public static TargetMD5PasswordEncoder getTargetMd5PasswordEncoder() {
        return TARGET_MD5_PASSWORD_ENCODER;
    }

    public static StockVisibilityClientMock getStockVisibilityClientMock() {
        return STOCK_VISIBILITY_CLIENT_MOCK;
    }

    /**
     * @return the targetStoreFinderStockFacade
     */
    public static TargetStoreFinderStockFacade getTargetStoreFinderStockFacade() {
        return TARGET_STORE_FINDER_STOCK_FACADE;
    }

    /**
     * @return the defaultTargetStockLookUpFacade
     */
    public static DefaultTargetStockLookUpFacade getTargetStockLookupFacade() {
        return TARGET_STOCKLOOKUP_FACADE;
    }

    public static TargetShopTheLookFacade getTargetShopTheLookFacade() {
        return TARGET_SHOP_THE_LOOK_FACADE;
    }

    /**
     * @return the targetSocialMediaProductsFacade
     */
    public static TargetSocialMediaProductsFacade getTargetSocialMediaProductsFacade() {
        return TARGET_SOCIAL_MEDIA_PRODUCTS_FACADE;
    }

    /**
     * @return the shopTheLookImportIntegrationFacade
     */
    public static ShopTheLookImportIntegrationFacade getShopTheLookImportIntegrationFacade() {
        return SHOP_THE_LOOK_IMPORT_INTEGRATION_FACADE;
    }

    /**
     * @return classification Service
     */
    public static ClassificationService getClassificationService() {
        return TARGET_CLASSIFICATION_SERVICE;
    }

    /**
     * @return the targetFeatureSwitchDao
     */
    public static TargetFeatureSwitchDao getTargetFeatureSwitchDao() {
        return TARGET_FEATURE_SWITCH_DAO;
    }

    /**
     * @return the targetInventoryAdjustmentClientImpl
     */
    public static TargetInventoryAdjustmentClientImpl getTargetInventoryAdjustmentClient() {
        return TARGET_INVENTORY_ADJUSTMENT_CLIENT;
    }

    /**
     * @return the targetAfterpayClientMock
     */
    public static TargetAfterpayClientMock getTargetAfterpayClientMock() {
        return TARGET_AFTERPAY_CLIENT_MOCK;
    }

    /**
     * @return the targetZippayClientMock
     */
    public static TargetZippayClientMock getTargetZippayClientMock() {
        return TARGET_ZIPPAY_CLIENT_MOCK;
    }

    /**
     * @return the afterpayConfigDaoImpl
     */
    public static AfterpayConfigDaoImpl getAfterpayConfigDaoImpl() {
        return AFTERPAY_CONFIG_DAO_IMPL;
    }

    /**
     * @return the targetAfterpayConfigurationJob
     */
    public static TargetAfterpayConfigurationJob getTargetAfterpayConfigurationJob() {
        return TARGET_AFTERPAY_CONFIGURATION_JOB;
    }

    /**
     * @return the afterpayConfigServiceImpl
     */
    public static AfterpayConfigServiceImpl getAfterpayConfigServiceImpl() {
        return AFTERPAY_CONFIG_SERVICE_IMPL;
    }

    /*
     * @return the targetOriginalCategoryPopulatorServiceImpl
     */
    public static TargetOriginalCategoryPopulatorServiceImpl getTragetOriginalCategoryPopulatorService() {
        return TARGET_ORIGINAL_CATEGORY_PRODUCT_SERVICE;
    }

    /**
     * @return the targetAfterpayHelper
     */
    public static TargetAfterpayHelper getTargetAfterpayHelper() {
        return TARGET_AFTERPAY_HELPER;
    }

    public static MockAusPostShipsterClient getMockAuspostShipsterClient() {
        return MOCK_AUSPOST_DELIVERY_CLUB_CLIENT;
    }

    /**
     * @return the shipsterConfigService
     */
    public static ShipsterConfigService getShipsterConfigService() {
        return SHIPSTER_CONFIG_SERVICE;
    }

    /**
     * @return the fluentClientMock
     */
    public static FluentClientMock getFluentClientMock() {
        return FLUENT_CLIENT_MOCK;
    }

    /**
     * @return the locationFeedJob
     */
    public static LocationFeedJob getLocationFeedJob() {
        return LOCATION_FEED_JOB;
    }

    /**
     * @return the fluentBatchResponseService
     */
    public static FluentBatchResponseService getFluentBatchResponseService() {
        return FLUENT_BATCH_RESPONSE_SERVICE;
    }

    /**
     * @return the fluentUpdateStatusService
     */
    public static FluentUpdateStatusService getFluentUpdateStatusService() {
        return FLUENT_UPDATE_STATUS_SERVICE;
    }

    /**
     * @return the locationFeedStatusCheckJob
     */
    public static LocationFeedStatusCheckJob getLocationFeedStatusCheckJob() {
        return LOCATION_FEED_STATUS_CHECK_JOB;
    }

    /**
     * @return the endecaProductQueryBuilder
     */
    public static EndecaProductQueryBuilderMock getEndecaProductQueryBuilder() {
        return ENDECA_PRODUCT_QUERY_BUILDER;
    }

    /**
     * @return the fluentFulfilmentService
     */
    public static FluentFulfilmentService getFluentFulfilmentService() {
        return FLUENT_FULFILMENT_SERVICE;
    }

    /**
     * @return the targetOrderEntryService
     */
    public static TargetOrderEntryService getTargetOrderEntryService() {
        return TARGET_ORDER_ENTRY_SERVICE;
    }

    /**
     * @return the fluentOrderService
     */
    public static FluentOrderService getFluentOrderService() {
        return FLUENT_ORDER_SERVICE;
    }
}

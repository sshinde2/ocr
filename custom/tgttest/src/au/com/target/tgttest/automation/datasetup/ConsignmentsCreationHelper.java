/**
 * 
 */
package au.com.target.tgttest.automation.datasetup;

import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.AddressService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.enums.ConsignmentRejectReason;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.model.ConsignmentParcelModel;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtfulfilment.service.TargetManifestService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.ProductUtil;
import au.com.target.tgttest.automation.facade.bean.ConsignmentForStore;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import au.com.target.tgttest.automation.util.ProductQuantityParser;
import au.com.target.tgttest.constants.TgttestConstants;
import au.com.target.tgttinker.mock.instore.ProductData;
import au.com.target.tgtwsfacades.instore.dto.consignments.Parcel;


/**
 * Helper class to create the consignment models
 * 
 * @author Vivek
 */
public class ConsignmentsCreationHelper {

    private static final String CONSIGNMENT_SEARCH_QUERY = "SELECT {" + TargetConsignmentModel._TYPECODE + "."
            + TargetConsignmentModel.PK
            + "} FROM {" + TargetConsignmentModel._TYPECODE
            + "} WHERE {"
            + TargetConsignmentModel._TYPECODE + "." + TargetConsignmentModel.CODE + "} LIKE ?consignmentFragment";

    private static final Double TEN = Double.valueOf(10);

    @Resource
    private ModelService modelService;

    @Resource
    private TargetWarehouseService targetWarehouseService;

    @Resource
    private TargetPointOfServiceService pointOfServiceService;

    @Resource
    private DeliveryModeService deliveryModeService;

    @Resource
    private CommonI18NService commonI18NService;

    @Resource
    private UserService userService;

    @Resource
    private TargetManifestService targetManifestService;

    @Resource
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Resource
    private AddressService addressService;

    @Resource
    private UnitService unitService;

    /**
     * Creates TargetConsignmentModels for each consignmentForStore entry and attaches it to an Order
     * 
     * @param consignmentEntries
     * @param fulfilmentStore
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     * @throws DuplicateUidException
     */
    public void createConsignmentsForStore(final List<ConsignmentForStore> consignmentEntries,
            final Integer fulfilmentStore, final String salesChannel)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {

        final TargetPointOfServiceModel fulfilmentPOS = getPOSModel(fulfilmentStore);
        final WarehouseModel warehouse = getWarehouseFromPOS(fulfilmentStore, fulfilmentPOS);
        createConsignmentsForWarehouse(consignmentEntries, warehouse, salesChannel);
    }

    /**
     * Creates TargetConsignmentModels for each consignmentForStore entry and attaches it to an Order
     * 
     * @param consignmentEntries
     * @param fulfilmentStore
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     * @throws DuplicateUidException
     */
    public void createConsignmentsFromFluentForStore(final List<ConsignmentForStore> consignmentEntries,
            final Integer fulfilmentStore, final String salesChannel)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {

        final TargetPointOfServiceModel fulfilmentPOS = getPOSModel(fulfilmentStore);
        final WarehouseModel warehouse = getWarehouseFromPOS(fulfilmentStore, fulfilmentPOS);
        createConsignmentsForWarehouseFromFluent(consignmentEntries, warehouse, salesChannel);
    }

    /**
     * @param fulfilmentStore
     * @param fulfilmentPOS
     * @return WarehouseModel
     */
    private WarehouseModel getWarehouseFromPOS(final Integer fulfilmentStore,
            final TargetPointOfServiceModel fulfilmentPOS) {
        final WarehouseModel warehouse = targetWarehouseService.getWarehouseForPointOfService(fulfilmentPOS);
        if (null == warehouse) {
            throw new RuntimeException("No warehouse set up for store: " + fulfilmentStore);
        }
        return warehouse;
    }

    /**
     * @param fulfilmentStore
     * @return TargetPointOfServiceModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    private TargetPointOfServiceModel getPOSModel(final Integer fulfilmentStore)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetPointOfServiceModel fulfilmentPOS = pointOfServiceService
                .getPOSByStoreNumber(fulfilmentStore);
        if (null == fulfilmentPOS) {
            throw new RuntimeException("No POS found for store number  : " + fulfilmentStore);
        }
        return fulfilmentPOS;
    }

    /**
     * Creates TargetConsignmentModels for each consignmentForStore entry and attaches it to an Order
     * 
     * @param consignmentEntries
     * @param fulfilmentStore
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     * @throws DuplicateUidException
     */
    public void createConsignmentsForStoreFromFluentFulfilments(final List<ConsignmentForStore> consignmentEntries,
            final Integer fulfilmentStore)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {

        final TargetPointOfServiceModel fulfilmentPOS = getPOSModel(fulfilmentStore);
        final WarehouseModel warehouse = getWarehouseFromPOS(fulfilmentStore, fulfilmentPOS);
        createConsignmentsForWarehouseFromFluent(consignmentEntries, warehouse, "WEB");
    }

    /**
     * Creates TargetConsignmentModels against a warehouse code and attaches it to an Order
     * 
     * @param consignmentEntries
     * @param warehouseCode
     * @throws DuplicateUidException
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    public void createConsignmentForWarehouseCode(final List<ConsignmentForStore> consignmentEntries,
            final String warehouseCode)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {
        if (StringUtils.isBlank(warehouseCode)) {
            throw new RuntimeException("Warehouse code cannot be null" + warehouseCode);
        }
        final WarehouseModel warehouse = targetWarehouseService.getWarehouseForCode(warehouseCode);
        createConsignmentsForWarehouse(consignmentEntries, warehouse, "WEB");
    }

    /**
     * Creates TargetConsignmentModels against a warehouse model and attaches it to an Order
     * 
     * @param consignmentEntries
     * @param warehouse
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     * @throws DuplicateUidException
     */
    private void createConsignmentsForWarehouse(final List<ConsignmentForStore> consignmentEntries,
            final WarehouseModel warehouse, final String salesChannel)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {

        OrderModel order = null;

        for (final ConsignmentForStore entry : consignmentEntries) {

            final TargetAddressModel shippingAddress = createShippingAddressForConsignment(entry);
            // Set customer if supplied
            setCustomerForAddress(entry, shippingAddress);
            order = createOrder(entry, salesChannel, false);
            final TargetConsignmentModel consignment = createConsignmentForOrder(warehouse, order, entry,
                    shippingAddress);
            createTargetCarrierModelForConsignment(entry, consignment);
            final Set<ConsignmentModel> consignmentSet = new HashSet<>();
            consignmentSet.add(consignment);
            order.setConsignments(consignmentSet);
            order.setStatus(ServiceLookup.getOrderCreationHelper().getOrderStatus(entry.getConsignmentStatus()));
            modelService.save(order);
        }
        Order.setOrder(order);
    }


    /**
     * Creates TargetConsignmentModels against a warehouse model and attaches it to an Order and does not create target
     * carrier model
     * 
     * @param consignmentEntries
     * @param warehouse
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     * @throws DuplicateUidException
     */
    private void createConsignmentsForWarehouseFromFluent(final List<ConsignmentForStore> consignmentEntries,
            final WarehouseModel warehouse, final String salesChannel)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {

        OrderModel order = null;

        for (final ConsignmentForStore entry : consignmentEntries) {

            final TargetAddressModel shippingAddress = createShippingAddressForConsignment(entry);
            // Set customer if supplied
            setCustomerForAddress(entry, shippingAddress);
            order = createOrder(entry, salesChannel, true);
            final TargetConsignmentModel consignment = createConsignmentForOrder(warehouse, order, entry,
                    shippingAddress);
            final Set<ConsignmentModel> consignmentSet = new HashSet<>();
            consignmentSet.add(consignment);
            order.setConsignments(consignmentSet);
            order.setStatus(ServiceLookup.getOrderCreationHelper().getOrderStatus(entry.getConsignmentStatus()));
            modelService.save(order);
        }
        Order.setOrder(order);
    }

    /**
     * @param warehouse
     * @param order
     * @param entry
     * @param shippingAddress
     * @return targetConsignment model
     */
    private TargetConsignmentModel createConsignmentForOrder(final WarehouseModel warehouse, final OrderModel order,
            final ConsignmentForStore entry, final TargetAddressModel shippingAddress) {
        final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();
        final DeliveryModeModel deliveryMode = getDeliveryMethod(entry.getDeliveryMethod());
        final TargetConsignmentModel consignment = createConsignment(
                entry.getConsignmentCode(), entry.getProducts(),
                getConsignmentStatus(entry.getConsignmentStatus()),
                warehouse,
                shippingAddress,
                dateUtil.interpretStringAsDate(entry.getCreatedDate()),
                dateUtil.interpretStringAsDate(entry.getWaveDate()),
                dateUtil.interpretStringAsDate(entry.getPickDate()),
                dateUtil.interpretStringAsDate(entry.getPackDate()),
                dateUtil.interpretStringAsDate(entry.getShipDate()),
                dateUtil.interpretStringAsDate(entry.getCancelDate()),
                entry.getRejectReason(),
                deliveryMode,
                getManifest(entry.getManifestID()),
                entry.getBoxes(),
                order);
        return consignment;
    }

    /**
     * @param entry
     * @return orderModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     * @throws DuplicateUidException
     */
    private OrderModel createOrder(final ConsignmentForStore entry, final String salesChannel, final boolean fluent)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException, DuplicateUidException {
        OrderModel order;
        order = ServiceLookup.getOrderCreationHelper().createOrder(
                entry.getDeliveryMethod(),
                entry.getConsignmentCode(), false,
                entry.getCncStoreNumber(), Double.valueOf(00), salesChannel);

        if (fluent) {
            order.setFluentId(order.getCode());
        }

        return order;
    }

    /**
     * @param entry
     * @param shippingAddress
     */
    private void setCustomerForAddress(final ConsignmentForStore entry, final TargetAddressModel shippingAddress) {
        if (StringUtils.isNotEmpty(entry.getCustomer())) {
            final String customerName = entry.getCustomer();
            if (StringUtils.isNotBlank(customerName)) {
                final String[] parts = customerName.split(" ");
                shippingAddress.setFirstname(parts[0].trim());
                shippingAddress.setLastname(parts[1].trim());
            }
        }
    }

    /**
     * @param entry
     * @return targetAddressModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    private TargetAddressModel createShippingAddressForConsignment(final ConsignmentForStore entry)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        TargetAddressModel shippingAddress = null;

        // If destination field is supplied then use it to construct the address
        if (StringUtils.isNotEmpty(entry.getDestination())) {

            shippingAddress = modelService.create(TargetAddressModel.class);

            final String string = entry.getDestination();
            final String[] parts = string.split(",");

            if (parts.length == 4) {
                // whole address with street
                setDeliveryAddress(shippingAddress, parts[0].trim(), parts[1].trim(),
                        parts[2].trim(), parts[3].trim());
            }
            else if (parts.length == 3) {
                // town, state, postcode
                setDeliveryAddress(shippingAddress, "", parts[0].trim(),
                        parts[1].trim(), parts[2].trim());
            }
            else if (parts.length == 2) {
                // town, state
                setDeliveryAddress(shippingAddress, "", parts[0].trim(),
                        parts[1].trim(), "");
            }
        }

        // Else for Cnc use the cnc store address
        else if (entry.getCncStoreNumber() != null) {
            final TargetPointOfServiceModel cncPOS = pointOfServiceService
                    .getPOSByStoreNumber(entry.getCncStoreNumber());

            shippingAddress = (TargetAddressModel)addressService.cloneAddress(cncPOS.getAddress());
        }

        // otherwise just set a default home address
        else {
            shippingAddress = modelService.create(TargetAddressModel.class);

            setDeliveryAddress(shippingAddress, "1 Aberdeen Street", "Geelong", "VIC", "3220");
        }

        shippingAddress.setPhone1(entry.getPhone());
        return shippingAddress;
    }

    /**
     * @param entry
     * @param consignment
     */
    private void createTargetCarrierModelForConsignment(final ConsignmentForStore entry,
            final TargetConsignmentModel consignment) {
        final TargetCarrierModel carrierModel = ServiceLookup.getTargetCarrierSelectionService()
                .getPreferredCarrier(consignment, entry.isBulky(), false);
        consignment.setTargetCarrier(carrierModel);
        // Set some other stuff on the consignment
        consignment.setTrackingID(entry.getTrackingId());
        modelService.save(consignment);
    }


    /**
     * Create a consignment given hundreds of confusing parameters
     * 
     * @param code
     * @param status
     * @param warehouse
     * @param addressModel
     * @param creationTime
     * @param waveDate
     * @param pickDate
     * @param packDate
     * @param shipDate
     * @param cancelDate
     * @param rejectReason
     * @param deliveryMode
     * @param manifest
     * @param parcels
     * @param order
     * 
     * @return consignment
     */
    public TargetConsignmentModel createConsignment(final String code, final String products,
            final ConsignmentStatus status,
            final WarehouseModel warehouse,
            final AddressModel addressModel, final Date creationTime, final Date waveDate, final Date pickDate,
            final Date packDate, final Date shipDate, final Date cancelDate,
            final String rejectReason, final DeliveryModeModel deliveryMode,
            final TargetManifestModel manifest, final Integer parcels, final OrderModel order) {

        final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
        consignment.setCode(code);
        consignment.setStatus(status);
        consignment.setWarehouse(warehouse);
        consignment.setCreationtime(creationTime);
        consignment.setWavedDate(waveDate);
        consignment.setPickConfirmDate(pickDate);
        consignment.setPackedDate(packDate);
        consignment.setShippingDate(shipDate);
        consignment.setCancelDate(cancelDate);
        consignment.setDeliveryMode(deliveryMode);
        consignment.setManifest(manifest);
        consignment.setPickConfirmDate(pickDate);
        consignment.setParcelCount(parcels);
        consignment.setOrder(order);

        if (StringUtils.isNotEmpty(rejectReason)) {
            consignment.setRejectReason(this.getConsignmentRejectReason(rejectReason));
        }

        // Create the address on the order since it needs an owner
        if (addressModel.getOwner() == null) {
            addressModel.setOwner(order);
            modelService.save(addressModel);
        }

        consignment.setShippingAddress(addressModel);

        if (StringUtils.isNotEmpty(products)) {
            setConsignmentProducts(order, consignment, products);
        }
        else {
            order.setEntries(null);
            modelService.save(order);
            modelService.refresh(order);
            setConsignmentProducts(order, consignment, "1 of 10000011");

        }
        modelService.save(order);
        modelService.save(consignment);
        modelService.refresh(consignment);


        return consignment;
    }

    public void removeAllConsignments() {
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(CONSIGNMENT_SEARCH_QUERY);
        searchQuery.addQueryParameter("consignmentFragment", "%" + TgttestConstants.ORDER_PREFIX + "%");
        final SearchResult<TargetConsignmentModel> searchResult = ServiceLookup.getFlexibleSearchService().search(
                searchQuery);

        if (CollectionUtils.isNotEmpty(searchResult.getResult())) {
            for (final TargetConsignmentModel consignment : searchResult.getResult()) {

                ServiceLookup.getModelService().refresh(consignment);

                // Clear down parcels
                if (consignment.getParcelsDetails() != null) {

                    for (final ConsignmentParcelModel parcel : consignment.getParcelsDetails()) {
                        ServiceLookup.getModelService().remove(parcel);
                    }
                }

                ServiceLookup.getModelService().refresh(consignment);
                ServiceLookup.getModelService().remove(consignment);
            }
        }

    }


    /**
     * Create parcels on the given consignment
     * 
     * @param consignmentCode
     * @param parcelList
     */
    public void createParcels(final String consignmentCode, final List<Parcel> parcelList) {

        final TargetConsignmentModel consignment = ServiceLookup.getTargetStoreConsignmentService()
                .getConsignmentByCode(consignmentCode);

        for (final Parcel parcel : parcelList) {

            final ConsignmentParcelModel model = modelService.create(ConsignmentParcelModel.class);
            model.setActualWeight(parcel.getWeight());
            model.setLength(parcel.getLength());
            model.setWidth(parcel.getWidth());
            model.setHeight(parcel.getHeight());
            model.setConsignment(consignment);
            modelService.save(model);
        }
    }

    private void setDeliveryAddress(final TargetAddressModel shippingAddress, final String street, final String town,
            final String state, final String postcode) {

        shippingAddress.setStreetname(street);
        shippingAddress.setTown(town);
        shippingAddress.setDistrict(state);
        shippingAddress.setPostalcode(postcode);
    }

    private ConsignmentStatus getConsignmentStatus(final String status) {
        if (StringUtils.isBlank(status)) {
            return ConsignmentStatus.CREATED;
        }
        return ConsignmentStatus.valueOf(status);
    }

    private ConsignmentRejectReason getConsignmentRejectReason(final String reason) {
        return ConsignmentRejectReason.valueOf(reason);
    }

    private DeliveryModeModel getDeliveryMethod(final String deliveryMethod) {
        if (deliveryMethod != null) {
            return deliveryModeService.getDeliveryModeForCode(deliveryMethod);
        }

        return deliveryModeService.getDeliveryModeForCode(CheckoutUtil.CLICK_AND_COLLECT_CODE);
    }

    private TargetManifestModel getManifest(final String manifestID) {
        if (StringUtils.isNotEmpty(manifestID)) {
            TargetManifestModel manifest;
            try {
                manifest = targetManifestService.getManifestByCode(manifestID);
                return manifest;
            }
            catch (final NotFoundException e) {
                Assert.assertNotNull(e);
            }
            manifest = modelService.create(TargetManifestModel.class);
            manifest.setCode(manifestID);
            modelService.save(manifest);
            return manifest;
        }
        return null;
    }


    private void setConsignmentProducts(final OrderModel order, final TargetConsignmentModel consignment,
            final String products) {

        final List<ProductData> listProducts = ProductQuantityParser.parseProductList(products);

        final Set<ConsignmentEntryModel> conEntries = new HashSet<>();
        final List<AbstractOrderEntryModel> newOrderEntries = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(order.getEntries())) {
            newOrderEntries.addAll(order.getEntries());
        }

        for (final ProductData product : listProducts) {

            final int qty = Integer.valueOf(product.getQuantity()).intValue();

            final OrderEntryModel oe = createOrderEntry(order, product.getCode(), qty);
            newOrderEntries.add(oe);
            conEntries.add(createConsignmentEntry(consignment, oe, qty));
        }

        order.setEntries(newOrderEntries);
        consignment.setConsignmentEntries(conEntries);
    }

    private OrderEntryModel createOrderEntry(final OrderModel order, final String code, final int qty) {

        final ProductModel product = ProductUtil.getProductModel(code);
        final OrderEntryModel entry = modelService.create(OrderEntryModel.class);
        entry.setBasePrice(TEN);
        entry.setOrder(order);
        entry.setProduct(product);
        entry.setQuantity(Long.valueOf(qty));
        entry.setUnit(product.getUnit());
        modelService.save(entry);
        return entry;
    }

    private ConsignmentEntryModel createConsignmentEntry(final TargetConsignmentModel consignment,
            final OrderEntryModel oe, final int qty) {

        final ConsignmentEntryModel entry = modelService.create(ConsignmentEntryModel.class);
        entry.setOrderEntry(oe);
        entry.setOwner(consignment);
        entry.setConsignment(consignment);
        entry.setQuantity(Long.valueOf(qty));
        if ("SHIPPED".equalsIgnoreCase(consignment.getStatus().getCode())
                || "PACKED".equalsIgnoreCase(consignment.getStatus().getCode())
                || "PICKED".equalsIgnoreCase(consignment.getStatus().getCode())) {
            entry.setShippedQuantity(Long.valueOf(qty));
        }
        modelService.save(entry);

        return entry;
    }

    /**
     * Method to create consignment for order
     * 
     * @param consignments
     * @param order
     */
    public void createConsignments(final List<ConsignmentForStore> consignments, final OrderModel order,
            final AddressModel address) {
        int consignmentShippedCount = 0;
        for (final ConsignmentForStore consignment : consignments) {
            final WarehouseModel warehouse = ServiceLookup.getWarehouseService().getWarehouseForCode(
                    consignment.getWarehouse() + "Warehouse");
            final TargetConsignmentModel consignmentModel = ServiceLookup.getConsignmentCreationHelper()
                    .createConsignment(String.valueOf(consignmentShippedCount) + order.getCode(),
                            consignment.getProducts(), ConsignmentStatus.valueOf(consignment.getConsignmentStatus()),
                            warehouse, address, new Date(), null, null, null, null, null, null, null, null, null,
                            order);
            if (consignment.getConsignmentStatus().equalsIgnoreCase("shipped")) {
                consignmentShippedCount++;
                for (final ConsignmentEntryModel consignmentEntry : consignmentModel.getConsignmentEntries()) {
                    consignmentEntry.setShippedQuantity(consignmentEntry.getQuantity());
                    ServiceLookup.getModelService().save(consignmentEntry);
                }
            }
        }
        if (consignmentShippedCount == consignments.size()) {
            order.setStatus(OrderStatus.COMPLETED);
        }
        else {
            order.setStatus(OrderStatus.INPROGRESS);
        }
    }

}

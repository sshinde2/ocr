/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.fest.assertions.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Utils for business processes
 * 
 */
public final class BusinessProcessCustomerUtil {

    private BusinessProcessCustomerUtil() {
        // util
    }

    public static void waitForProcessToFinishAndCheckSuccess(final String processName, final String emailId)
            throws InterruptedException {

        final ProcessState state = waitForProcessToFinish(processName, emailId);
        assertThat(state).isEqualTo(ProcessState.SUCCEEDED);
    }

    public static void waitForProcessAndCheckStillRunning(final String processName, final String emailId)
            throws InterruptedException {

        final ProcessState state = waitForProcessToFinish(processName, emailId);
        assertThat(state).isEqualTo(ProcessState.RUNNING);
    }

    /**
     * Wait for process to finish and return the state
     * 
     * @param processName
     * @return state of process
     * @throws InterruptedException
     */
    private static ProcessState waitForProcessToFinish(final String processName, final String userEmail)
            throws InterruptedException {

        ProcessState state = null;
        final StoreFrontCustomerProcessModel process = getprocessModelWithProcess(userEmail, processName);
        for (int i = 0; i < 60; i++) {

            Thread.sleep(1000);
            ServiceLookup.getModelService().refresh(process);
            assertThat("Business process not present:" + processName, process, notNullValue());

            state = process.getState();

            if (state.equals(ProcessState.SUCCEEDED) || state.equals(ProcessState.FAILED)
                    || state.equals(ProcessState.ERROR)) {
                break;
            }
        }

        return state;
    }


    /**
     * Gets the StoreFrontCustomerProcessModel for the given customer
     * 
     * @param uid
     * @return StoreFrontCustomerProcessModel
     */
    public static StoreFrontCustomerProcessModel getprocessModelWithProcess(final String uid, final String processName) {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT {p:" + StoreFrontCustomerProcessModel.PK + "} FROM {"
                + StoreFrontCustomerProcessModel._TYPECODE);
        query.append(" AS p JOIN " + TargetCustomerModel._TYPECODE + " AS c ON {c:" + TargetCustomerModel.PK
                + "} = {p:"
                + StoreFrontCustomerProcessModel.CUSTOMER + "}}");
        query.append(" WHERE {c:" + TargetCustomerModel.UID + "} = ?email");
        query.append(" AND {p:" + StoreFrontCustomerProcessModel.PROCESSDEFINITIONNAME + "} = ?processName");
        query.append(" order by {p:modifiedtime} desc");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", uid);
        params.put("processName", processName);
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(StoreFrontCustomerProcessModel.class));
        final SearchResult searchResult = ServiceLookup.getFlexibleSearchService().search(searchQuery);
        return (StoreFrontCustomerProcessModel)searchResult.getResult().get(0);
    }


    /**
     * Gets the StoreFrontCustomerProcessModel for the given customer
     * 
     * @param uid
     * @return StoreFrontCustomerProcessModel
     */
    public static List<StoreFrontCustomerProcessModel> getprocessModelForCustomer(final String uid) {
        final StringBuilder query = new StringBuilder();
        query.append("SELECT {p:" + StoreFrontCustomerProcessModel.PK + "} FROM {"
                + StoreFrontCustomerProcessModel._TYPECODE);
        query.append(" AS p JOIN " + TargetCustomerModel._TYPECODE + " AS c ON {c:" + TargetCustomerModel.PK
                + "} = {p:"
                + StoreFrontCustomerProcessModel.CUSTOMER + "}}");
        query.append(" WHERE {c:" + TargetCustomerModel.UID + "} = ?email");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", uid);
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query.toString());
        searchQuery.addQueryParameters(params);
        searchQuery.setResultClassList(Collections.singletonList(StoreFrontCustomerProcessModel.class));
        final SearchResult searchResult = ServiceLookup.getFlexibleSearchService().search(searchQuery);
        List<StoreFrontCustomerProcessModel> processes = new ArrayList<>();
        processes = searchResult.getResult();
        return processes;
    }


    /**
     * Teardown business processes
     */
    public static void teardownCustomerProcesses() {

        final List<StoreFrontCustomerProcessModel> processess = getprocessModelForCustomer(CustomerUtil
                .getStandardTestCustomerUid());
        if (CollectionUtils.isNotEmpty(processess)) {
            ServiceLookup.getModelService().removeAll(processess);
        }
    }
}

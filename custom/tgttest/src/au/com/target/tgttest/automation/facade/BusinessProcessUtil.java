/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.fest.assertions.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.jalo.BusinessProcess;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.domain.Order;
import junit.framework.Assert;


/**
 * Utils for business processes
 * 
 */
public final class BusinessProcessUtil {

    private static final Logger LOG = Logger.getLogger(BusinessProcessUtil.class.getName());

    private static final String GET_ALL_SMS_BUSINESS_PROCESS_QUERY = "SELECT {"
            + BusinessProcessModel.PK + "} FROM {" + BusinessProcessModel._TYPECODE
            + "} WHERE {" + BusinessProcessModel.CODE + "} LIKE 'sendSmsToStoreForOpenOrdersProcess%' ";

    private static final String REVERSE_GIFT_CARD_PROCESS = "reverseGiftcardProcess";

    private BusinessProcessUtil() {
        // util
    }

    public static void waitForProcessToFinishAndCheckSuccess(final String processName) throws InterruptedException {
        final ProcessState state = waitForProcessToFinish(processName);
        checkState(state, ProcessState.SUCCEEDED);
    }

    public static void waitForProcessToFinishAndCheckError(final String processName) throws InterruptedException {
        final ProcessState state = waitForProcessToFinish(processName);
        checkState(state, ProcessState.ERROR);
    }

    public static void waitForExpectedProcessToFinishAndCheckSuccess(final String processName)
            throws InterruptedException {
        final ProcessState state = waitForProcessToFinish(processName);
        assertThat(state).isNotNull();
        checkState(state, ProcessState.SUCCEEDED);
    }

    public static void waitForProcessToFinishAndCheckSuccess(final String processName, final String orderCode)
            throws InterruptedException {
        final ProcessState state = waitForProcessToFinish(processName, orderCode);
        checkState(state, ProcessState.SUCCEEDED);
    }

    public static void waitForExpectedProcessToFinishAndCheckSuccess(final String processName, final String orderCode)
            throws InterruptedException {
        final ProcessState state = waitForProcessToFinish(processName, orderCode);
        assertThat(state).isNotNull();
        checkState(state, ProcessState.SUCCEEDED);
    }

    public static void waitForProcessToFinishAndCheckSuccess(final OrderProcessModel process)
            throws InterruptedException {
        final ProcessState state = waitForProcessToFinish(process);
        assertThat(state).isNotNull();
        checkState(state, ProcessState.SUCCEEDED);
    }

    public static void waitForProcessAndCheckStillRunning(final String processName) throws InterruptedException {
        final ProcessState state = waitForProcessToFinish(processName);
        assertThat(state).isNotNull();
        checkState(state, ProcessState.RUNNING);
    }

    public static ProcessState waitForProcessToFinish(final String processName) throws InterruptedException {
        return waitForProcessToFinish(processName, null);
    }

    public static ProcessState waitForProcessToFinish(final String processName, final String orderCode)
            throws InterruptedException {
        ProcessState state = null;
        OrderProcessModel process = null;
        String code = orderCode;

        if (null == code) {
            code = Order.getInstance().getOrderModel().getCode();
        }

        for (int i = 0; i < 2; i++) {
            process = getOrderProcess(processName, code);
            if (null != process) {
                break;
            }
            Thread.sleep(1000);
        }

        if (null != process) {
            state = waitForProcessToFinish(process);
        }
        return state;
    }

    /**
     * Teardown business processes
     */
    public static void teardownOrderProcesses() {

        if (Order.getInstance() != null && Order.getInstance().getOrderModel() != null) {
            try {
                final OrderModel orderModel = Order.getInstance().getOrderModel();
                final Collection<OrderProcessModel> processes = orderModel.getOrderProcess();

                ServiceLookup.getModelService().removeAll(processes);
            }
            catch (final Exception e) {
                LOG.warn("Exception in order process teardown", e);
            }
        }
    }

    /**
     * get the latest created business process
     * 
     * @return BusinessProcessModel
     */
    public static BusinessProcessModel getLatestCreatedBusinessProcess() {
        final String query = "select {"
                + BusinessProcessModel.PK
                + "} from {" + BusinessProcessModel._TYPECODE + "} ORDER BY  {" + BusinessProcessModel.CREATIONTIME
                + "} DESC";
        final SearchResult<BusinessProcessModel> searchResult = ServiceLookup.getFlexibleSearchService().search(query);
        final List<BusinessProcessModel> resultList = searchResult.getResult();
        if (CollectionUtils.isNotEmpty(resultList)) {
            return resultList.get(0);
        }
        return null;
    }

    /**
     * set the business process retry time for customer subscription
     * 
     * @param times
     * @param intervalTime
     */
    public static void setBusinessProcssRetryTimeForCustomerSubscription(final int times, final int intervalTime) {
        ServiceLookup.getSendCustomerPersonaldetailsSubscriptionAction().setAllowedRetries(times);
        ServiceLookup.getSendCustomerPersonaldetailsSubscriptionAction().setRetryInterval(intervalTime);

    }

    /**
     * set the business process retry time for customer subscription
     * 
     * @param times
     * @param intervalTime
     */
    public static void setBusinessProcssRetryTimeForCustomerNewsletterSubscription(final int times,
            final int intervalTime) {
        ServiceLookup.getSendCustomerNewsletterSubscriptionAction().setAllowedRetries(times);
        ServiceLookup.getSendCustomerNewsletterSubscriptionAction().setRetryInterval(intervalTime);

    }


    /**
     * Wait for process to finish and return the state
     * 
     * @param processModel
     * @return state of process
     * @throws InterruptedException
     */
    public static ProcessState waitForProcessToFinish(final BusinessProcessModel processModel)
            throws InterruptedException {
        ProcessState state = null;
        if (null != processModel) {
            for (int i = 0; i < 60; i++) {
                Thread.sleep(1000);
                ServiceLookup.getModelService().refresh(processModel);
                assertThat("Business process not present:" + processModel.getCode(), processModel, notNullValue());

                state = processModel.getState();
                if (state.equals(ProcessState.SUCCEEDED) || state.equals(ProcessState.FAILED)
                        || state.equals(ProcessState.ERROR)) {
                    break;
                }
                Thread.sleep(1000);
            }
        }
        return state;
    }


    public static ProcessState waitForBusinessProcessWithNameLikeToFinish(final String processNameLike)
            throws InterruptedException {

        BusinessProcessModel process = null;
        for (int i = 0; i < 2; i++) {
            process = getBusinessProcessByNameLike(processNameLike);
            if (null != process) {
                break;
            }
            Thread.sleep(1000);
        }

        ProcessState state = null;
        if (null != process) {
            state = waitForProcessToFinish(process);
        }
        return state;
    }

    private static BusinessProcessModel getBusinessProcessByNameLike(final String processNameLike) {

        BusinessProcessModel process = null;
        final FlexibleSearchQuery query = new FlexibleSearchQuery("select {"
                + BusinessProcess.PK
                + "} from {" + BusinessProcessModel._TYPECODE + "}"
                + " where {code} like '%" + processNameLike + "%' ");
        final SearchResult result = ServiceLookup.getFlexibleSearchService().search(query);
        if (result.getCount() > 0) {
            process = ((BusinessProcessModel)result.getResult().get(0));
        }

        return process;
    }

    private static List<BusinessProcessModel> getBusinessProcessListByNameLike(final String processNameLike) {

        final FlexibleSearchQuery query = new FlexibleSearchQuery("select {"
                + BusinessProcess.PK
                + "} from {" + BusinessProcessModel._TYPECODE + "}"
                + " where {code} like '%" + processNameLike + "%' ");
        final SearchResult<BusinessProcessModel> result = ServiceLookup.getFlexibleSearchService().search(query);
        return result.getResult();
    }

    /**
     * Tear down business process
     */
    public static void teardownBusinessProcess(final BusinessProcessModel processModel) {
        if (processModel != null) {
            try {

                ServiceLookup.getModelService().remove(processModel);
            }
            catch (final Exception e) {
                LOG.warn("Exception in business processes teardown", e);

            }
        }
    }



    /**
     * @param state
     */
    private static void checkState(final ProcessState state, final ProcessState expectedState) {
        if (null != state) {
            assertThat(state).isEqualTo(expectedState);

            // Refresh the order since it will have changed as a result of the business process
            final Order order = Order.getInstance();
            if (null != order) {
                order.refresh();
            }
        }
    }

    public static OrderProcessModel getOrderProcess(final String processName) throws InterruptedException {

        // Assumes order is set in domain 
        final String code = Order.getInstance().getOrderModel().getCode();

        OrderProcessModel process = null;
        for (int i = 0; i < 1; i++) {
            process = getOrderProcess(processName, code);
            if (null != process) {
                break;
            }
            Thread.sleep(1000);
        }

        return process;
    }


    private static OrderProcessModel getOrderProcess(final String processName, final String orderCode) {
        Assert.assertNotNull(orderCode);
        final OrderModel orderModel = CheckoutUtil.getOrder(orderCode);
        final Collection<OrderProcessModel> processes = orderModel.getOrderProcess();

        OrderProcessModel placeOrderProcess = null;
        for (final OrderProcessModel process : processes) {
            if (process.getProcessDefinitionName().equals(processName)) {

                placeOrderProcess = process;
                break;
            }
        }

        return placeOrderProcess;
    }

    public static void waitForAllOrderProcessesToFinish(final OrderModel order) throws InterruptedException {
        if (order != null) {
            final Iterator<OrderProcessModel> it = order.getOrderProcess().iterator();
            while (it.hasNext()) {
                BusinessProcessUtil.waitForProcessToFinish(it.next());
            }
        }
    }

    public static void removeAllSmsBusinessProcesses() {

        final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_ALL_SMS_BUSINESS_PROCESS_QUERY);
        final SearchResult<BusinessProcessModel> searchResult = ServiceLookup
                .getFlexibleSearchService().search(query);
        ServiceLookup.getModelService().removeAll(searchResult.getResult());
    }

    public static void removeAllReverseGiftcardProcesses() {

        ServiceLookup.getModelService().removeAll(getBusinessProcessListByNameLike(REVERSE_GIFT_CARD_PROCESS));
    }

    public static void waitForOrderBusinessProcesses() throws InterruptedException {
        if (Order.getInstance().getOrderModel() != null) {
            waitForExpectedProcessToFinishAndCheckSuccess("placeOrderProcess");
            waitForProcessToFinishAndCheckSuccess("acceptOrderProcess");
            waitForProcessToFinishAndCheckSuccess("rejectOrderProcess");
            waitForProcessToFinishAndCheckSuccess("reviewOrderProcess");
        }
    }

    public static void waitForOrderBusinessProcessesWithError() throws InterruptedException {
        if (Order.getInstance().getOrderModel() != null) {
            waitForProcessToFinishAndCheckSuccess("placeOrderProcess");
            waitForProcessToFinishAndCheckError("acceptOrderProcess");
        }
    }

    public static void waitForPreOrderBusinessProcesses() throws InterruptedException {
        if (Order.getInstance().getOrderModel() != null) {
            waitForExpectedProcessToFinishAndCheckSuccess("placePreOrderProcess");
            waitForProcessToFinishAndCheckSuccess("acceptPreOrderProcess");
            waitForProcessToFinishAndCheckSuccess("rejectOrderProcess");
            waitForProcessToFinishAndCheckSuccess("reviewOrderProcess");
        }
    }

    public static void waitForFluentOrderBusinessProcesses() throws InterruptedException {
        if (Order.getInstance().getOrderModel() != null) {
            waitForExpectedProcessToFinishAndCheckSuccess("placeFluentOrderProcess");
            waitForProcessToFinishAndCheckSuccess("acceptFluentOrderProcess");
            waitForProcessToFinishAndCheckSuccess("rejectOrderProcess");
            waitForProcessToFinishAndCheckSuccess("reviewOrderProcess");
            BusinessProcessUtil.waitForProcessToFinishAndCheckSuccess("fluentConsignmentShippedProcess");
        }
    }

    /**
     * @throws InterruptedException
     *
     */
    public static void waitForFluentPreOrderBusinessProcesses() throws InterruptedException {
        if (Order.getInstance().getOrderModel() != null) {
            waitForExpectedProcessToFinishAndCheckSuccess("placeFluentPreOrderProcess");
            waitForProcessToFinishAndCheckSuccess("fluentAcceptPreOrderProcess");
        }
    }

    public static void waitForFluentOrderBusinessProcessesWithError() throws InterruptedException {
        if (Order.getInstance().getOrderModel() != null) {
            waitForProcessToFinishAndCheckSuccess("placeFluentOrderProcess");
            waitForProcessToFinishAndCheckError("acceptOrderProcess");
        }
    }

    /**
     * @throws InterruptedException
     */
    public static void waitForRejectBusinessProcesses() throws InterruptedException {
        if (Order.getInstance().getOrderModel() != null) {
            waitForProcessToFinishAndCheckSuccess("rejectOrderProcess");
        }
    }

    /**
     * @throws InterruptedException
     */
    public static void waitForRejectBusinessProcessesWithError() throws InterruptedException {
        if (Order.getInstance().getOrderModel() != null) {
            waitForProcessToFinishAndCheckError("rejectOrderProcess");
        }
    }

    /**
     * @throws InterruptedException
     * 
     */
    public static void waitForFluentPickedOrderBusinessProcesses() throws InterruptedException {
        waitForProcessToFinishAndCheckSuccess("fluentConsignmentPickedProcess");

    }

    /**
     * @throws InterruptedException
     * 
     */
    public static void waitForFluentOrderShippedBusinessProcesses() throws InterruptedException {

        waitForProcessToFinishAndCheckSuccess("fluentConsignmentShippedProcess");
        waitForProcessToFinishAndCheckSuccess("fluentOrderShippedProcess");
        waitForProcessToFinishAndCheckSuccess("updateOrderStatusCompleted");

    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.RefundReason;
import de.hybris.platform.basecommerce.enums.ReturnAction;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.OrderCancelEntry;
import de.hybris.platform.ordercancel.OrderCancelException;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.returns.model.RefundEntryModel;
import de.hybris.platform.returns.model.ReturnEntryModel;
import de.hybris.platform.returns.model.ReturnRequestModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.ordercancel.TargetOrderCancelRequest;
import au.com.target.tgtcore.ordercancel.data.IpgNewRefundInfoDTO;
import au.com.target.tgtcore.ordercancel.enums.RequestOrigin;
import au.com.target.tgtcore.refund.TargetOrderRefundException;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.CartEntry;


/**
 * Cancel and refund facade
 * 
 */
public final class CancelUtil {

    private static final Logger LOG = Logger.getLogger(CancelUtil.class.getName());

    private CancelUtil() {
        // util
    }

    /**
     * @param orderModel
     * @param entries
     * @param cancelReason
     * @param notes
     * @param shippingAmountToRefund
     * @return status
     * @throws OrderCancelException
     */
    public static String partialCancelRequest(final List<CartEntry> entries, final OrderModel orderModel,
            final CancelReason cancelReason, final String notes, final Double shippingAmountToRefund,
            final IpgNewRefundInfoDTO ipgNewRefundInfoDTO)
                    throws OrderCancelException {

        // Construct the OrderCancelEntrys
        final List<OrderCancelEntry> orderCancelEntries = new ArrayList<>();

        for (final CartEntry entry : entries) {

            final OrderEntryModel entryModel = findEntry(orderModel, entry.getProduct());
            final OrderCancelEntry orderCancelEntry = new OrderCancelEntry(entryModel, entry.getQty(),
                    notes, cancelReason);
            orderCancelEntries.add(orderCancelEntry);
        }

        final TargetOrderCancelRequest orderCancelRequest = new TargetOrderCancelRequest(orderModel,
                orderCancelEntries);
        orderCancelRequest.setIpgNewRefundInfoDTO(ipgNewRefundInfoDTO);
        return processCancelRequest(orderCancelRequest, cancelReason, notes, shippingAmountToRefund, null);
    }

    /**
     * @param orderModel
     * @param cancelReason
     * @param notes
     * @param shippingAmountToRefund
     * @param requestOrigin
     * @return status
     * @throws OrderCancelException
     */
    public static String fullCancelRequest(final OrderModel orderModel,
            final CancelReason cancelReason, final String notes, final Double shippingAmountToRefund,
            final IpgNewRefundInfoDTO ipgNewRefundInfoDTO, final RequestOrigin requestOrigin)
                    throws OrderCancelException {

        final TargetOrderCancelRequest orderCancelRequest = new TargetOrderCancelRequest(orderModel);
        orderCancelRequest.setIpgNewRefundInfoDTO(ipgNewRefundInfoDTO);
        return processCancelRequest(orderCancelRequest, cancelReason, notes, shippingAmountToRefund, requestOrigin);
    }

    private static String processCancelRequest(final TargetOrderCancelRequest orderCancelRequest,
            final CancelReason cancelReason, final String notes, final Double shippingAmountToRefund,
            final RequestOrigin requestOrigin)
                    throws OrderCancelException {

        orderCancelRequest.setCancelReason(cancelReason);
        orderCancelRequest.setNotes(notes);
        orderCancelRequest.setShippingAmountToRefund(shippingAmountToRefund);
        orderCancelRequest.setRequestOrigin(requestOrigin);

        final OrderCancelRecordEntryModel orderRequestRecord = ServiceLookup.getTargetOrderCancelService()
                .requestOrderCancel(orderCancelRequest, ServiceLookup.getUserService().getAdminUser());

        return orderRequestRecord.getCancelResult().getCode();
    }


    /**
     * @param entries
     * @param orderModel
     * @param refundReason
     * @param returnAction
     * @param notes
     * @param shippingAmountToRefund
     * @throws OrderCancelException
     */
    public static void createRefundRequest(final List<CartEntry> entries, final OrderModel orderModel,
            RefundReason refundReason, final ReturnAction returnAction, final String notes,
            final Double shippingAmountToRefund, final IpgNewRefundInfoDTO ipgNewRefundInfoDTO)
                    throws OrderCancelException {

        final ReturnRequestModel refundRequest = ServiceLookup.getTargetOrderReturnService().createReturnRequest(
                orderModel);
        refundRequest.setShippingAmountToRefund(shippingAmountToRefund);
        final String rmaString = ServiceLookup.getTargetOrderReturnService().createRMA(refundRequest);
        refundRequest.setRMA(rmaString);

        BigDecimal entriesRefund = BigDecimal.ZERO;
        final List<ReturnEntryModel> orderReturnEntries = new ArrayList<>();
        for (final CartEntry entry : entries) {

            final AbstractOrderEntryModel entryModel = findEntry(orderModel, entry.getProduct());
            RefundReason entryRefundReason = StringUtils.isNotBlank(entry.getReturnReason())
                    ? RefundReason.valueOf(entry.getReturnReason()) : refundReason;
            final RefundEntryModel refundEntry = ServiceLookup.getTargetOrderReturnService().createRefund(
                    refundRequest, entryModel, notes, Long.valueOf(entry.getQty()), returnAction, entryRefundReason);
            orderReturnEntries.add(refundEntry);
            entriesRefund = entriesRefund.add(BigDecimal.valueOf(entry.getQty()
                    * entryModel.getBasePrice().doubleValue()));
        }

        refundRequest.setReturnEntries(orderReturnEntries);

        final BigDecimal totalRefund = entriesRefund.add(BigDecimal.valueOf(shippingAmountToRefund.doubleValue()))
                .setScale(2);
        refundRequest.setRefundedAmount(Double.valueOf(totalRefund.doubleValue()));
        try {
            ServiceLookup.getTargetRefundService().apply(refundRequest, orderModel, null, ipgNewRefundInfoDTO);
        }
        catch (final TargetOrderRefundException e) {
            LOG.info("TargetOrderRefundException", e);
        }


    }

    private static OrderEntryModel findEntry(final OrderModel order, final String productCode) {

        OrderEntryModel entryModel = null;

        for (final AbstractOrderEntryModel orderEntry : order.getEntries()) {
            if (orderEntry.getProduct().getCode().equals(productCode)) {
                entryModel = (OrderEntryModel)orderEntry;
                break;
            }
        }

        assertThat("Could not find entry to cancel/refund: " + productCode, entryModel, notNullValue());

        return entryModel;
    }


}

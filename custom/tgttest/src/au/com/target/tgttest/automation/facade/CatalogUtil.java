/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Catalog-related facade
 * 
 */
public final class CatalogUtil {

    private CatalogUtil() {
        // utility
    }

    public static void setSessionCatalogVersion() {
        // ProductService requires product catalog versions to be set in session scope
        // See https://wiki.hybris.com/display/release4/How+To+Display+a+Product+-+Tutorial
        ServiceLookup.getCatalogVersionService().setSessionCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION);
    }

    public static void setSessionStagedCatalogVersion() {
        // ProductService requires product catalog versions to be set in session scope
        // See https://wiki.hybris.com/display/release4/How+To+Display+a+Product+-+Tutorial
        ServiceLookup.getCatalogVersionService().setSessionCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
    }
}

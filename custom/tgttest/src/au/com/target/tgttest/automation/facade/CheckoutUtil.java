/**
 *
 */
package au.com.target.tgttest.automation.facade;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.IpgGiftCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.cscockpit.exceptions.ValidationException;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.fest.assertions.Assertions;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.http.HttpStatus;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcs.constants.TgtcsConstants;
import au.com.target.tgtfacades.flybuys.data.FlybuysRedeemTierData;
import au.com.target.tgtfacades.order.data.ClickAndCollectDeliveryDetailData;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderRequest;
import au.com.target.tgtfacades.order.data.TargetPlaceOrderResult;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderAfterpayPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderIPGPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderPayPalPaymentInfoData;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfacades.user.data.TargetAddressData;
import au.com.target.tgtfraud.order.impl.TargetFraudCommerceCheckoutServiceImpl;
import au.com.target.tgtfraud.order.impl.TestCommerceCheckoutServiceImpl;
import au.com.target.tgtpayment.commands.result.TargetCardPaymentResult;
import au.com.target.tgtpayment.commands.result.TargetCardResult;
import au.com.target.tgtpayment.commands.result.TargetQueryTransactionDetailsResult;
import au.com.target.tgtpayment.enums.IpgPaymentTemplateType;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.model.IpgPaymentInfoModel;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CapturePaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Item;
import au.com.target.tgtpaymentprovider.afterpay.data.types.Money;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.PriceData;
import au.com.target.tgttest.automation.facade.bean.StockLevelData;
import au.com.target.tgttest.automation.facade.bean.TlogCreditCardPaymentInfo;
import au.com.target.tgttest.automation.facade.domain.Checkout;
import au.com.target.tgttest.automation.facade.domain.Customer;
import au.com.target.tgttest.automation.facade.domain.Order;
import au.com.target.tgttinker.mock.payment.TargetAfterpayClientMock;



/**
 * Checkout facade
 *
 */
public final class CheckoutUtil {

    public static final String CC_CARDNUMBER = "5123450000000346";
    public static final String VALID_TMD = "2771166666666";
    public static final String INVALID_TMD = "666";
    public static final String FLYBUYS_CODE = "6008943218616910";

    // Delivery mode codes
    public static final String HOME_DELIVERY_CODE = "home-delivery";
    public static final String EXPRESS_DELIVERY_CODE = "express-delivery";
    public static final String EBAY_EXPRESS_DELIVERY_CODE = "ebay-express-delivery";
    public static final String EBAY_HOME_DELIVERY_CODE = "eBay-delivery";
    public static final String EBAY_CLICK_AND_COLLECT_CODE = "ebay-click-and-collect";
    public static final String CLICK_AND_COLLECT_CODE = "click-and-collect";
    public static final String DIGITAL_GIFT_CARD_CODE = "digital-gift-card";
    public static final String DEL_MODE_NOT_CHANGED = "not changed";

    public static final String BUY_NOW_CODE = "buynow";

    // Payment methods
    public static final String NEW_CARD_METHOD = "creditcard";
    public static final String PAYPAL_METHOD = "paypal";
    public static final String PINPAD_METHOD = "pinpad";
    public static final String IPG = "ipg";
    public static final String IPG_MIXED = "giftcard";
    public static final String PAYPAL_HERE_METHOD = "paypalhere";
    public static final String AFTERPAY_METHOD = "afterpay";
    public static final String ZIPPAY_METHOD = "zippay";

    // Payment mode codes
    public static final String CREDITCARD_MODE_CODE = "creditcard";
    public static final String PINPAD_MODE_CODE = "pinpad";
    public static final String PAYPAL_MODE_CODE = "paypal";
    public static final String DEFAULT_CARD_TOKEN = "78901234556";
    public static final boolean DEFAULT_BILLING_ADDRESS = true;

    public static final String SAMPLE_ADDRESS = "1 Aberdeen Street, Newtown, VIC, 3003";
    public static final String CC_EXPIRY_DATE = "10//20";

    // Type of checkout
    public static final int GEELONG_STORE_NUMBER = 7001;

    // Type of checkout
    public static final String ANY_PRD_CODE = "10000011";
    public static final int ANY_PRD_QTY = 2;
    public static final double ANY_PRD_PRICE = 15.00;

    public static final String GUEST_CHECKOUT = "guest-checkout";
    public static final String REGISTERED_USER_CHECKOUT = "registered-user-checkout";

    public static final String AVAILABLE_FLYBUYSPOINTS = "12000";

    //afterpay
    public static final String AFTERPAY_TOKEN = "bahek6l74jq357i09ht54vasf34fueeff7ah6ik99lffncdk8qd0";
    public static final String AFTERPAY_CAPTURE_PAYMENT_ID = "23866677";
    public static final String AFTERPAY_CAPTURE_PAYMENT_APPROVED = "APPROVED";
    public static final String AFTERPAY_CAPTURE_PAYMENT_DECLINED = "DECLINED";

    private static final String FLYBUYS_NUMBER = "1111111111";
    private static final Date DOB = new Date();
    private static final String POSTCODE = "3000";
    private static final Logger LOG = Logger.getLogger(CheckoutUtil.class);
    private static final double ANY_PRD_WEIGHT = 1.0;
    private static final String GUEST_FIRSTNAME = "firstname";
    private static final String GUEST_LASTNAME = "lastname";


    // Remember checkoutCartCode for kiosk orders
    private static String checkoutCartCode;

    private CheckoutUtil() {
        // utility
    }

    /**
     * Run the checkout features before place the order
     * 
     * @throws Exception
     */
    private static void prePlaceOrder() throws Exception {
        CartUtil.createCheckoutCart(Checkout.getInstance().getUserCheckoutMode());
        setDeliveryDetails(Checkout.getInstance().getDeliveryMode(), Checkout.getInstance().getDeliveryAddress());
        setPaymentMethodToCart(Checkout.getInstance().getPaymentMethod());
        setTeamMemberDiscount(Checkout.getInstance().getTmdCardType());
        setFlybuysNumber(Checkout.getInstance().getFlybuysNumber());
        setFlybuysPointsRedemption(Checkout.getInstance().getFlyBuysPoints());
        setVoucher(Checkout.getInstance().getVoucher());

        //update the cart if need
        if (CollectionUtils.isNotEmpty(Checkout.getInstance().getBeforPlaceOrderUpdateCartEntries())) {
            CartUtil.updateCartEntries(Checkout.getInstance().getBeforPlaceOrderUpdateCartEntries());
            Checkout.getInstance().setBeforPlaceOrderUpdateCartEntries(null);
        }

        if (CollectionUtils.isNotEmpty(Checkout.getInstance().getBeforPlaceOrderAddCartEntries())) {
            CartUtil.addCartEntries(Checkout.getInstance().getBeforPlaceOrderAddCartEntries());
            Checkout.getInstance().setBeforPlaceOrderAddCartEntries(null);
        }
        //update the price if need
        if (CollectionUtils.isNotEmpty(Checkout.getInstance().getPosPriceUpdateInCheckout())) {
            for (final PriceData priceData : Checkout.getInstance().getPosPriceUpdateInCheckout()) {
                ProductUtil.updateProductPrice(priceData.getProductCode(), priceData.getPrice());
            }
        }

        if (CollectionUtils.isNotEmpty(Checkout.getInstance().getBeforePlaceOrderStockLevel())) {
            for (final StockLevelData stockData : Checkout.getInstance().getBeforePlaceOrderStockLevel()) {
                StockLevelUtil.updateFastlineProductStock(stockData.getProduct(),
                        Integer.parseInt(stockData.getReserved()), Integer.parseInt(stockData.getAvailable()), 0, 0);
            }
            Checkout.getInstance().setBeforePlaceOrderStockLevel(null);
        }

        if (StringUtils.isNotEmpty(Checkout.getInstance().getProductOutOfStockBeforePlaceOrder())) {
            CartUtil.updateProductOutOfStock(Checkout.getInstance().getProductOutOfStockBeforePlaceOrder());
            Checkout.getInstance().setProductOutOfStockBeforePlaceOrder(null);
        }

        // Set mock for broken order if needed
        if (Checkout.getInstance().isSomethingWrongWithOrderCreation()) {

            setFailedOrderCreationMock();
        }

    }

    /**
     * Place an order with expected success
     *
     * @return the order model
     * @throws Exception
     */
    public static OrderModel placeOrder() throws Exception {

        prePlaceOrder();

        final CartModel checkoutCart = getCheckoutCart();
        checkoutCartCode = checkoutCart.getCode();

        //customer name is not set yet due to re-factor in the Commerce cart
        setDeliveryAddressFirstAndLastName(checkoutCart);

        doFullPlaceOrder(checkoutCart);

        // Unset mock for broken order if needed
        if (Checkout.getInstance().isSomethingWrongWithOrderCreation()) {

            unsetFailedOrderCreationMock();
            return null;
        }

        // assuming success
        final TargetPlaceOrderResultEnum placeOrderResult = Checkout.getInstance().getPlaceOrderResult();


        if (placeOrderResult == TargetPlaceOrderResultEnum.SUCCESS) {
            return getOrder(checkoutCart.getCode());
        }
        else {
            return null;
        }
    }

    /**
     * @param checkoutCart
     */
    private static void setDeliveryAddressFirstAndLastName(final CartModel checkoutCart) {
        if (null != checkoutCart.getDeliveryAddress()
                && checkoutCart.getDeliveryAddress() instanceof TargetAddressModel) {
            final TargetAddressModel deliveryAddress = (TargetAddressModel)checkoutCart.getDeliveryAddress();
            deliveryAddress.setFirstname(deliveryAddress.getFirstname() != null ? deliveryAddress.getFirstname()
                    : GUEST_FIRSTNAME);
            deliveryAddress.setLastname(deliveryAddress.getLastname() != null ? deliveryAddress.getLastname()
                    : GUEST_LASTNAME);
        }
    }

    /**
     * set payment method to the checkout cart
     *
     * @param paymentMethod
     */
    public static void setPaymentMethodToCart(final String paymentMethod) {
        if (CheckoutUtil.NEW_CARD_METHOD.equals(paymentMethod)) {
            setPaymentMode(CREDITCARD_MODE_CODE);
            setCreditCardPaymentDetails();
        }
        else if (CheckoutUtil.PAYPAL_METHOD.equals(paymentMethod)) {
            setPaymentMode(PAYPAL_MODE_CODE);
            if (!Checkout.getInstance().isSpc()) {
                setPaypalPaymentDetails();
            }

        }
        else if (CheckoutUtil.PINPAD_METHOD.equals(paymentMethod)) {
            setPaymentMode(PINPAD_MODE_CODE);
            setPinPadPaymentDetails();
            setKioskStoreNumber(Checkout.getInstance().getStoreNumberAsString());
            setKioskSalesApplication();
        }
        else if (CheckoutUtil.IPG.equals(paymentMethod)) {
            setPaymentMode(CheckoutUtil.IPG);
            setIPGPaymentDetails(IpgPaymentTemplateType.CREDITCARDSINGLE);
        }
        else if (CheckoutUtil.IPG_MIXED.equals(paymentMethod)) {
            setPaymentMode(CheckoutUtil.IPG_MIXED);
            setIPGPaymentDetails(IpgPaymentTemplateType.GIFTCARDMULTIPLE);
        }
        else if (CheckoutUtil.AFTERPAY_METHOD.equals(paymentMethod)) {
            setPaymentMode(CheckoutUtil.AFTERPAY_METHOD);
            setAfterpayPaymentDetails();
        }
        else if (CheckoutUtil.ZIPPAY_METHOD.equals(paymentMethod)) {
            setPaymentMode(CheckoutUtil.ZIPPAY_METHOD);
            setZipPaymentDetails();
        }
        else {
            throw new IllegalArgumentException("Unexpected payment method: " + paymentMethod);
        }

    }

    /**
     * Start an order (eg for pinpad or paypalhere) Start a pinpad order and return the order id
     * 
     * @throws Exception
     */
    public static void startPlaceOrder() throws Exception {

        prePlaceOrder();

        final CartModel checkoutCart = getCheckoutCart();
        checkoutCartCode = checkoutCart.getCode();

        doPreparePlaceOrder();

        // assuming prepare success
        assertThat(Checkout.getInstance().getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.PREPARE_SUCCESS);
    }


    /**
     * Finish pinpad or paypalhere order
     *
     */
    public static OrderModel finishOrder() {

        doFinishPlaceOrder();

        // Unset mock for broken order if needed
        if (Checkout.getInstance().isSomethingWrongWithOrderCreation()) {
            unsetFailedOrderCreationMock();
            return null;
        }

        // assuming success
        assertThat(Checkout.getInstance().getPlaceOrderResult()).isEqualTo(TargetPlaceOrderResultEnum.SUCCESS);

        return getOrder(checkoutCartCode);
    }

    /**
     * Get flybuys redeem tiers
     *
     * @return available tiers
     */
    public static List<FlybuysRedeemTierData> getRedeemTiers() {

        return ServiceLookup.getFlybuysDiscountFacade()
                .getFlybuysRedemptionDataForCheckoutCart().getRedeemTiers();
    }


    private static void doFullPlaceOrder(final CartModel checkoutCart) {
        TargetPlaceOrderResult result = null;
        if (Checkout.getInstance().isSpc()) {
            final TargetPlaceOrderRequest request = new TargetPlaceOrderRequest();
            request.setCookieString("cookieString");
            request.setIpAddress("127.0.0.1");
            if (CheckoutUtil.PAYPAL_METHOD.equalsIgnoreCase(Checkout.getInstance().getPaymentMethod())) {
                final TargetPlaceOrderPayPalPaymentInfoData payPalPaymentInfoData = new TargetPlaceOrderPayPalPaymentInfoData();

                payPalPaymentInfoData.setPaypalSessionToken("1234");
                payPalPaymentInfoData.setPaypalPayerId("payerId");

                request.setPaymentInfo(payPalPaymentInfoData);
            }
            else if (CheckoutUtil.AFTERPAY_METHOD.equalsIgnoreCase(Checkout.getInstance().getPaymentMethod())) {
                final TargetPlaceOrderAfterpayPaymentInfoData paymentInfoData = new TargetPlaceOrderAfterpayPaymentInfoData();
                paymentInfoData.setAfterpayOrderToken(AFTERPAY_TOKEN);
                request.setPaymentInfo(paymentInfoData);
            }
            else if (CheckoutUtil.ZIPPAY_METHOD.equalsIgnoreCase(Checkout.getInstance().getPaymentMethod())) {
                request.setPaymentInfo(ZipPaymentUtil.getMockPaymentInfo());
            }
            else {
                final TargetPlaceOrderIPGPaymentInfoData ipgPaymentInfo = new TargetPlaceOrderIPGPaymentInfoData();

                ipgPaymentInfo.setSessionId(checkoutCart.getUniqueKeyForPayment());
                ipgPaymentInfo.setIpgToken(((IpgPaymentInfoModel)checkoutCart.getPaymentInfo()).getToken());

                request.setPaymentInfo(ipgPaymentInfo);
            }
            result = ServiceLookup.getTargetPlaceOrderFacade().placeOrder(request);

        }
        else {
            result = ServiceLookup.getTargetPlaceOrderFacade().placeOrder();
        }
        assertThat(result).isNotNull();

        // Save the place order result
        Checkout.getInstance().setPlaceOrderResult(result.getPlaceOrderResult());
        Checkout.getInstance().setAdjustedData(result.getAdjustedCartEntriesData());
    }

    private static void doPreparePlaceOrder() {

        final TargetPlaceOrderResult result = ServiceLookup.getTargetPlaceOrderFacade().prepareForPlaceOrder();
        assertThat(result).isNotNull();
        Checkout.getInstance().setPlaceOrderResult(result.getPlaceOrderResult());
    }

    private static void doFinishPlaceOrder() {

        final TargetPlaceOrderResult result = ServiceLookup.getTargetPlaceOrderFacade().finishPlaceOrder();
        assertThat(result).isNotNull();
        Checkout.getInstance().setPlaceOrderResult(result.getPlaceOrderResult());
    }

    /**
     * Get order given code
     *
     * @param code
     * @return OrderModel
     */
    public static OrderModel getOrder(final String code) {

        final OrderModel order = ServiceLookup.getTargetOrderService().findOrderModelForOrderId(code);
        assertThat(order).isNotNull();
        return order;
    }

    public static double getInitialDepositForPreOrder() {
        return ServiceLookup.getTargetSharedConfigService()
                .getDouble(
                        TgtCoreConstants.Config.PREODER_DEPOSIT_AMOUNT, 10);
    }

    /**
     * Get the checkout cart
     *
     * @return checkout cart
     */
    public static CartModel getCheckoutCart() {

        return ServiceLookup.getTargetCheckoutFacade().getCart();
    }

    private static boolean setPaymentMode(final String paymentModeCode) {

        return ServiceLookup.getTargetCheckoutFacade().setPaymentMode(paymentModeCode);
    }

    private static boolean setKioskStoreNumber(final String store) {

        return ServiceLookup.getTargetCheckoutFacade().setKioskStoreNumber(store);
    }

    private static void setKioskSalesApplication() {
        ServiceLookup.getTargetSalesApplicationService().setCurrentSalesApplication(SalesApplication.KIOSK);
    }

    private static void setCreditCardPaymentDetails() {

        // Reference CheckoutPaymentController
        final CCPaymentInfoData paymentInfoData = createCCPaymentInfoData();
        final TargetAddressData billingAddress = createBillingAddress();
        paymentInfoData.setBillingAddress(billingAddress);

        final CCPaymentInfoData newPaymentSubscription = ServiceLookup.getTargetCheckoutFacade()
                .createPaymentSubscription(paymentInfoData);
        if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getSubscriptionId())) {
            ServiceLookup.getTargetCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
        }
        else {
            throw new RuntimeException("Could not create payment subscription");
        }

    }

    private static void setPaypalPaymentDetails() {

        ServiceLookup.getTargetCheckoutFacade().createPayPalPaymentInfo("1234", "payerId");
    }


    private static void setPinPadPaymentDetails() {

        final TargetAddressData billingAddress = createBillingAddress();
        ServiceLookup.getTargetCheckoutFacade().createPinPadPaymentInfo(billingAddress);
    }

    private static void setIPGPaymentDetails(final IpgPaymentTemplateType ipgTemplateType) {
        TargetAddressData billingAddress = null;
        billingAddress = (TargetAddressData)convertToAddressData(Checkout.getInstance().getBillingAddress());
        ServiceLookup.getTargetCheckoutFacade().createIpgPaymentInfo(billingAddress, ipgTemplateType);
        final String token = ServiceLookup.getTargetCheckoutFacade().getIpgSessionToken("testurl").getRequestToken();
        ServiceLookup.getTargetCheckoutFacade().updateIpgPaymentInfoWithToken(token);
        ServiceLookup.getIpgClientMock().setAmount(populateOrderAmntinCents().toString());
    }

    private static void setAfterpayPaymentDetails() {
        final TargetAfterpayClientMock clientMock = ServiceLookup.getTargetAfterpayClientMock();
        //set get order mock to return the same items in cart and cart total
        setGetOrderMock(clientMock);
        //set capture payment mock
        setCapturePaymentMock(clientMock);
    }

    private static void setZipPaymentDetails() {
        //set get order mock to return the same items in cart and cart total
        ZipPaymentUtil.createRetrieveCheckoutMock();
        //set capture payment mock
        ZipPaymentUtil.createCaptureRequestMock();
    }

    private static void setGetOrderMock(final TargetAfterpayClientMock clientMock) {
        if (clientMock.getGetOrderResponse() == null) {
            final GetOrderResponse getOrderResponse = new GetOrderResponse();
            final Money total = new Money();
            total.setCurrency("AUD");
            total.setAmount(getCheckoutCart().getTotalPrice().toString());
            getOrderResponse.setTotalAmount(total);
            getOrderResponse.setItems(CheckoutUtil.createItems(getCheckoutCart().getEntries()));
            clientMock.setGetOrderResponse(getOrderResponse);
        }
    }

    private static void setCapturePaymentMock(final TargetAfterpayClientMock clientMock) {
        if (clientMock.getCapturePaymentResponse() == null) {
            final CapturePaymentResponse capturePaymentResponse = new CapturePaymentResponse();
            capturePaymentResponse.setHttpStatus(HttpStatus.CREATED);
            capturePaymentResponse.setToken(AFTERPAY_TOKEN);
            capturePaymentResponse.setId(AFTERPAY_CAPTURE_PAYMENT_ID);
            capturePaymentResponse.setStatus(AFTERPAY_CAPTURE_PAYMENT_APPROVED);
            capturePaymentResponse
                    .setCreated(new DateTime().toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z")));
            clientMock.setCapturePaymentResponse(capturePaymentResponse);
        }
    }

    public static List<Item> createItems(final List<AbstractOrderEntryModel> entries) {
        final List<Item> items = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(entries)) {
            for (final AbstractOrderEntryModel entry : entries) {
                final Item item = new Item();
                item.setSku(entry.getProduct().getCode());
                item.setQuantity(entry.getQuantity().longValue());
                item.setPrice(CheckoutUtil.createMoney(entry.getBasePrice()));
                items.add(item);
            }
        }
        return items;
    }

    public static Money createMoney(final Double price) {
        final Money money = new Money();
        money.setAmount(BigDecimal.valueOf(price.doubleValue()).setScale(2, RoundingMode.HALF_UP).toString());
        money.setCurrency("AUD");
        return money;
    }

    /**
     * set afterpay client mock to reject the capture payment
     */
    public static void rejectAfterpayCapturePayment() {
        final CapturePaymentResponse capturePaymentResponse = new CapturePaymentResponse();
        capturePaymentResponse.setHttpStatus(HttpStatus.CREATED);
        capturePaymentResponse.setToken(CheckoutUtil.AFTERPAY_TOKEN);
        capturePaymentResponse.setId(CheckoutUtil.AFTERPAY_CAPTURE_PAYMENT_ID);
        capturePaymentResponse.setStatus(CheckoutUtil.AFTERPAY_CAPTURE_PAYMENT_DECLINED);
        capturePaymentResponse
                .setCreated(new DateTime().toString(DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z")));
        ServiceLookup.getTargetAfterpayClientMock().setCapturePaymentResponse(capturePaymentResponse);
    }

    /**
     * set checkout delivery details for the given cart.
     *
     * @param deliveryMode
     */
    public static void setDeliveryDetails(final String deliveryMode, final String deliveryAddress) {
        if (deliveryMode.equals(CheckoutUtil.HOME_DELIVERY_CODE)) {
            setHomeDeliveryDetails(getCheckoutCart(), Customer.getInstance().getUid(), deliveryAddress);
        }
        else if (deliveryMode.equals(CheckoutUtil.CLICK_AND_COLLECT_CODE)) {
            setClickAndCollectDetails(getCheckoutCart());
        }
        else if (deliveryMode.equals(CheckoutUtil.EXPRESS_DELIVERY_CODE)) {
            setExpressDeliveryDetails(getCheckoutCart(), Customer.getInstance()
                    .getUid(), deliveryAddress);
        }
        else if (deliveryMode.equals(CheckoutUtil.DIGITAL_GIFT_CARD_CODE)) {
            setDigitalDeliveryDetails();
        }
        else if (deliveryMode.equals(CheckoutUtil.EBAY_CLICK_AND_COLLECT_CODE)) {
            setEbayClickAndCollectDetails(getCheckoutCart());
        }
        else if (deliveryMode.equals(CheckoutUtil.EBAY_HOME_DELIVERY_CODE)) {
            setEbayHomeDeliveryDetails(getCheckoutCart(), Customer.getInstance().getUid(), deliveryAddress);
        }
        else if (!deliveryMode.equals(CheckoutUtil.DEL_MODE_NOT_CHANGED)) {
            throw new RuntimeException("Unknown delivery mode: " + deliveryMode);
        }
    }

    /**
     * Set home delivery for the given cart.
     *
     * @param cartModel
     * @param uid
     * @param deliveryAddress
     */
    public static void setHomeDeliveryDetails(final CartModel cartModel, final String uid,
            final String deliveryAddress) {

        ServiceLookup.getTargetCheckoutFacade().setDeliveryMode(HOME_DELIVERY_CODE);
        setDeliveryAddress(uid, deliveryAddress);
    }

    /**
     * Set ebay home delivery for the given cart.
     *
     * @param cartModel
     * @param uid
     * @param deliveryAddress
     */
    public static void setEbayHomeDeliveryDetails(final CartModel cartModel, final String uid,
            final String deliveryAddress) {

        ServiceLookup.getTargetCheckoutFacade().setDeliveryMode(EBAY_HOME_DELIVERY_CODE);
        setDeliveryAddress(uid, deliveryAddress);
    }

    /**
     * Set express delivery details
     *
     * @param cartModel
     * @param uid
     */
    public static void setExpressDeliveryDetails(final CartModel cartModel, final String uid,
            final String deliveryAddress) {
        ServiceLookup.getTargetCheckoutFacade().setDeliveryMode(EXPRESS_DELIVERY_CODE);
        setDeliveryAddress(uid, deliveryAddress);
    }

    private static void setDeliveryAddress(final String uid, final String deliveryAddress) {
        AddressData addressData = null;

        if (!"na".equals(deliveryAddress)) {
            if (StringUtils.isNotBlank(deliveryAddress)) {
                addressData = convertToAddressData(deliveryAddress);
            }
            if (uid.equals(CustomerUtil.getDefaultGuestUserId())) {

                // For guest user, create a new address for them
                if (addressData == null) {
                    addressData = createGuestDeliveryAddress();
                }
                ServiceLookup.getTargetUserFacade().addAddress(addressData);
            }
            else if (addressData == null) {
                // For registered user, use their default ship address
                addressData = CustomerUtil.getCurrentCustomer().getDefaultShippingAddress();
            }
        }
        ServiceLookup.getTargetCheckoutFacade().setDeliveryAddress(addressData);
    }


    /**
     * expecting the delivery address to be in the form 11,Coleman Street,Koonibba,SA,5690
     * 
     * @param deliveryAddress
     * @return AddressData with all fields populated
     */
    private static AddressData convertToAddressData(final String deliveryAddress) {
        if (StringUtils.isEmpty(deliveryAddress)) {
            return createBillingAddress();
        }
        else if ("na".equals(deliveryAddress)) {
            return null;
        }
        final TargetAddressData targetAddressData = new TargetAddressData();
        final String[] address = deliveryAddress.split(",");
        Assertions.assertThat(address).isNotEmpty();
        Assertions.assertThat(address.length).isGreaterThanOrEqualTo(4);

        if (address.length == 5) {
            targetAddressData.setLine1(address[0].trim());
            targetAddressData.setLine2(address[1].trim());
            targetAddressData.setTown(address[2].trim());
            targetAddressData.setState(address[3].trim());
            targetAddressData.setPostalCode(address[4].trim());
        }
        else {
            targetAddressData.setLine1(address[0].trim());
            targetAddressData.setTown(address[1].trim());
            targetAddressData.setState(address[2].trim());
            targetAddressData.setPostalCode(address[3].trim());
        }

        final CountryData countryData = new CountryData();
        countryData.setIsocode("AU");
        targetAddressData.setCountry(countryData);
        return targetAddressData;
    }

    /**
     * Set click and collect details on the cart
     *
     * @param cartModel
     */
    public static void setClickAndCollectDetails(final CartModel cartModel) {

        final DeliveryModeModel deliveryMode = ServiceLookup.getTargetDeliveryService()
                .getDeliveryModeForCode(CLICK_AND_COLLECT_CODE);

        final ClickAndCollectDeliveryDetailData cncData = new ClickAndCollectDeliveryDetailData();
        cncData.setStoreNumber(Integer.valueOf(Checkout.getInstance().getStoreNumber()));
        cncData.setTitle("mr");
        cncData.setFirstName("cnc");
        cncData.setLastName("user");
        cncData.setTelephoneNumber("04 55555555");
        cncData.setDeliveryModeModel(deliveryMode);

        ServiceLookup.getTargetCheckoutFacade().setClickAndCollectDeliveryDetails(cncData);
    }

    /**
     * Set ebay click and collect details on the cart
     *
     * @param cartModel
     */
    public static void setEbayClickAndCollectDetails(final CartModel cartModel) {

        final DeliveryModeModel deliveryMode = ServiceLookup.getTargetDeliveryService()
                .getDeliveryModeForCode(EBAY_CLICK_AND_COLLECT_CODE);

        final ClickAndCollectDeliveryDetailData cncData = new ClickAndCollectDeliveryDetailData();
        cncData.setStoreNumber(Integer.valueOf(Checkout.getInstance().getStoreNumber()));
        cncData.setTitle("mr");
        cncData.setFirstName("cnc");
        cncData.setLastName("user");
        cncData.setTelephoneNumber("04 55555555");
        cncData.setDeliveryModeModel(deliveryMode);

        ServiceLookup.getTargetCheckoutFacade().setClickAndCollectDeliveryDetails(cncData);
    }

    public static void setDigitalDeliveryDetails() {

        ServiceLookup.getTargetCheckoutFacade().setDeliveryMode(DIGITAL_GIFT_CARD_CODE);
    }


    /**
     * Set TMD on cart
     *
     * @param indicator
     *            empty, 'valid' or 'invalid'
     */
    public static void setTeamMemberDiscount(final String indicator) {

        if (StringUtils.isNotEmpty(indicator)) {
            final String cardNumber = indicator.equals("valid") ? VALID_TMD : INVALID_TMD;
            ServiceLookup.getCheckoutResponseFacade().applyTmd(cardNumber);
        }
    }


    /**
     * Set flybuys on cart
     *
     * @param flybuysNumber
     */
    public static void setFlybuysNumber(final String flybuysNumber) {
        if (StringUtils.isNotEmpty(flybuysNumber)) {
            ServiceLookup.getTargetCheckoutFacade().setFlybuysNumber(flybuysNumber);
        }
    }

    private static void setVoucher(final String voucherCode) throws CalculationException {

        if (StringUtils.isNotEmpty(voucherCode)) {
            VoucherUtil.applyVoucher(voucherCode, getCheckoutCart());
        }
    }


    private static CCPaymentInfoData createCCPaymentInfoData() {

        final String paymentSessionId = "123";
        final String accountHolderName = "Master Card";
        final String cardNumber = "512345xxxxxxx346";
        final CreditCardType cardType = CreditCardType.MASTER;
        final String expirationMonth = "5";
        final String expirationYear = "2017";


        final CCPaymentInfoData paymentInfoData = new CCPaymentInfoData();
        paymentInfoData.setId(paymentSessionId);
        paymentInfoData.setCardType(cardType.getCode());
        paymentInfoData.setAccountHolderName(accountHolderName);
        paymentInfoData.setCardNumber(cardNumber);
        paymentInfoData.setExpiryMonth(expirationMonth);
        paymentInfoData.setExpiryYear(expirationYear);
        paymentInfoData.setSaved(false);
        return paymentInfoData;
    }

    /**
     * Get the expected credit card tlog info, given the payment details
     *
     * @return TlogCreditCardPaymentInfo
     */
    public static TlogCreditCardPaymentInfo getCreditCardTlogInfoExpected() {

        // Note that the x's are converted to '0' in the tlog output
        final TlogCreditCardPaymentInfo paymentInfo = new TlogCreditCardPaymentInfo(CC_CARDNUMBER, null, true, null,
                null);
        return paymentInfo;
    }

    public static List<TlogCreditCardPaymentInfo> getMultiCardTlogInfoExpected() {
        final OrderModel order = getOrder(checkoutCartCode);
        final List<PaymentTransactionModel> paymentTransactions = order.getPaymentTransactions();
        final PaymentTransactionModel paymentTransaction = paymentTransactions.get(0);

        final List<PaymentTransactionEntryModel> paymentTransactionEntries = paymentTransaction.getEntries();

        final List<PaymentTransactionEntryModel> filteredPaymentTransactionEntries = new ArrayList<PaymentTransactionEntryModel>(
                paymentTransactionEntries);
        CollectionUtils.filter(filteredPaymentTransactionEntries, new Predicate() {

            /* (non-Javadoc)
             * @see org.acommons.collections.Predicate#evaluate(java.lang.Object)
             */
            @Override
            public boolean evaluate(final Object obj) {
                final PaymentTransactionEntryModel ptem = (PaymentTransactionEntryModel)obj;

                return PaymentTransactionType.REFUND_FOLLOW_ON.equals(ptem.getType())
                        && TransactionStatus.ACCEPTED.toString().equals(ptem.getTransactionStatus());
            }

        });

        if (paymentTransactionEntries.isEmpty()) {
            return null;
        }

        final List<TlogCreditCardPaymentInfo> expectedPaymentInfo = new ArrayList<>();
        for (final PaymentTransactionEntryModel paymentTransactionEntry : filteredPaymentTransactionEntries) {
            final String receiptNumber = paymentTransactionEntry.getReceiptNo();
            final BigDecimal amount = paymentTransactionEntry.getAmount();
            String cardNumber = null;

            final PaymentInfoModel paymentInfo = paymentTransactionEntry.getIpgPaymentInfo();
            if (paymentInfo instanceof IpgCreditCardPaymentInfoModel) {
                final IpgCreditCardPaymentInfoModel ipgCCPaymentInfo = (IpgCreditCardPaymentInfoModel)paymentInfo;
                cardNumber = ipgCCPaymentInfo.getNumber();
            }
            else {
                final IpgGiftCardPaymentInfoModel ipgGCPaymentInfo = (IpgGiftCardPaymentInfoModel)paymentInfo;
                cardNumber = ipgGCPaymentInfo.getMaskedNumber();
            }

            expectedPaymentInfo
                    .add(new TlogCreditCardPaymentInfo(cardNumber, receiptNumber, true, amount.negate(), null));
        }
        return expectedPaymentInfo;
    }

    public static void createAnyCart() throws Exception {
        ProductUtil.updateProductPrice(ANY_PRD_CODE, ANY_PRD_PRICE);
        ProductUtil.setProductWeight(ANY_PRD_CODE, Double.valueOf(ANY_PRD_WEIGHT));
        CartUtil.addToCart(ANY_PRD_CODE, ANY_PRD_QTY);
        CartUtil.recalculateSessionCart();
    }

    public static void teardownOrder() {

        if (Order.getInstance() != null && Order.getInstance().getOrderModel() != null) {
            try {
                ServiceLookup.getModelService().remove(Order.getInstance().getOrderModel());
            }
            catch (final Exception e) {
                LOG.warn("Exception in order teardown", e);
            }
        }

    }

    /**
     * Gets the receiptnumber for successful transaction
     * 
     * @return receiptNumber
     */
    public static String getReceiptNumberForSuccessfulTrans() {
        final ModelService modelService = ServiceLookup.getModelService();
        String receiptNumber = null;
        final Order order = Order.getInstance();
        for (final PaymentTransactionModel paymentTrans : order.getOrderModel().getPaymentTransactions()) {
            modelService.refresh(paymentTrans);
            final List<PaymentTransactionEntryModel> entries = paymentTrans.getEntries();
            for (final PaymentTransactionEntryModel entry : entries) {
                if (PaymentTransactionType.CAPTURE.equals(entry.getType())
                        && (entry.getTransactionStatus().equals(TransactionStatus.ACCEPTED.toString()))) {
                    receiptNumber = entry.getReceiptNo();
                    return receiptNumber;
                }
            }
        }
        return receiptNumber;
    }

    private static TargetAddressData createGuestDeliveryAddress() {

        final TargetAddressData newAddress = new TargetAddressData();
        newAddress.setTitleCode("Mr");
        newAddress.setFirstName("Guest");
        newAddress.setLastName("User");
        newAddress.setPhone("0455555555");
        newAddress.setBillingAddress(false);
        newAddress.setShippingAddress(true);
        newAddress.setDefaultAddress(true);

        final CountryData countryData = new CountryData();
        countryData.setIsocode("AU");
        newAddress.setCountry(countryData);

        newAddress.setLine1("Guest line 1");
        newAddress.setTown("Guest Town");
        newAddress.setPostalCode("3000");
        newAddress.setState("VIC");
        newAddress.setAddressValidated(true);

        return newAddress;
    }

    private static TargetAddressData createBillingAddress() {

        final TargetAddressData newAddress = new TargetAddressData();
        newAddress.setTitleCode("Mr");
        newAddress.setFirstName("Master");
        newAddress.setLastName("Card");
        newAddress.setPhone("0455555555");
        newAddress.setBillingAddress(true);
        newAddress.setShippingAddress(false);

        final CountryData countryData = new CountryData();
        countryData.setIsocode("AU");
        newAddress.setCountry(countryData);

        newAddress.setLine1("1 Aberdeen Street");
        newAddress.setTown("Newtown");
        newAddress.setPostalCode("3220");
        newAddress.setState("VIC");
        newAddress.setAddressValidated(true);

        return newAddress;
    }

    private static void setFlybuysPointsRedemption(final int points) {

        if (points > 0) {
            final FlybuysResponseType authResponse = ServiceLookup.getFlybuysDiscountFacade()
                    .getFlybuysRedemptionData(FLYBUYS_NUMBER, DOB, POSTCODE)
                    .getResponse();
            assertThat(authResponse).isEqualTo(FlybuysResponseType.SUCCESS);

            // TODO map the points to the redeem code by referencing the options in session
            ServiceLookup.getFlybuysDiscountFacade().applyFlybuysDiscountToCheckoutCart("DUMMYREDEEMCODE1");
        }

    }

    private static void setFailedOrderCreationMock() {

        // Replace commerce checkout service with one that just returns null order
        final TestCommerceCheckoutServiceImpl impl = ServiceLookup.getTestCommerceCheckoutServiceImpl();
        ServiceLookup.getTargetPlaceOrderFacade().setTargetCommerceCheckoutService(impl);

    }

    private static void unsetFailedOrderCreationMock() {

        final TargetFraudCommerceCheckoutServiceImpl impl = ServiceLookup.getFraudCommerceCheckoutService();
        ServiceLookup.getTargetPlaceOrderFacade().setTargetCommerceCheckoutService(impl);
    }

    @SuppressWarnings("boxing")
    private static Integer populateOrderAmntinCents() {
        final BigDecimal orderTotalCents = new BigDecimal(getCheckoutCart().getTotalPrice())
                .multiply(new BigDecimal("100"));
        return Integer.valueOf(orderTotalCents.intValue());
    }

    /**
     * Method to set b2b sales channel attribute in session
     * 
     * @param value
     */
    public static void setB2BValueInSession(final Boolean value) {
        ServiceLookup.getSessionService().setAttribute(TgtcsConstants.IS_B2B_ORDER_ATTR, value);
    }

    /**
     * Method to place order from cscockpit
     * 
     * @throws DuplicateUidException
     * @throws CalculationException
     * @throws ValidationException
     */
    public static void placeOrderForCsCockpit()
            throws DuplicateUidException, CalculationException, ValidationException {
        CartUtil.createCheckoutCart(Checkout.getInstance().getUserCheckoutMode());
        setDeliveryDetails(HOME_DELIVERY_CODE, SAMPLE_ADDRESS);
        setPaymentMethodToCart(IPG);
        setBillingAddressInCart();
        setIpgTransactionResultInSession();
        CartUtil.recalculateSessionCart();

        final OrderModel orderModel = ServiceLookup.getTargetCsCheckoutService()
                .doCheckout(CheckoutUtil.getCheckoutCart());
        Order.setOrder(orderModel);
        clearCsCockpitSessionData();
    }

    /**
     * Clear the session data previously set to place order from cscockpit
     */
    private static void clearCsCockpitSessionData() {
        ServiceLookup.getSessionService().removeAttribute(TgtcsConstants.IPG_TRANSACTION_RESULT_KEY);
        ServiceLookup.getSessionService().removeAttribute(TgtcsConstants.IS_B2B_ORDER_ATTR);
    }

    /**
     * Method to set up ipg transaction result in session
     */
    private static void setIpgTransactionResultInSession() {
        final TargetQueryTransactionDetailsResult result = new TargetQueryTransactionDetailsResult();
        final TargetCardResult cardResult = new TargetCardResult();
        cardResult.setCardType(CreditCardType.AMEX.toString());
        final TargetCardPaymentResult cardPaymentResult = new TargetCardPaymentResult();

        cardPaymentResult.setAmount(new BigDecimal(CheckoutUtil.getCheckoutCart().getTotalPrice().doubleValue()));
        cardResult.setTargetCardPaymentResult(cardPaymentResult);
        cardResult.setCardExpiry(CC_EXPIRY_DATE);
        cardResult.setCardNumber(CC_CARDNUMBER);
        final List<TargetCardResult> cardResults = new ArrayList<>();
        cardResults.add(cardResult);

        result.setCardResults(cardResults);
        result.setSuccess(true);

        ServiceLookup.getSessionService().setAttribute(TgtcsConstants.IPG_TRANSACTION_RESULT_KEY, result);

    }

    /**
     * Method to set billing address in cart
     */
    private static void setBillingAddressInCart() {
        final CartModel cartModel = ServiceLookup.getTargetCheckoutFacade().getCart();
        cartModel.setPaymentAddress(cartModel.getPaymentInfo().getBillingAddress());
        ServiceLookup.getModelService().save(cartModel);
    }

}

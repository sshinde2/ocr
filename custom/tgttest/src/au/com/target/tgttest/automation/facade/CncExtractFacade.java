/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import java.io.ByteArrayInputStream;

import javax.jms.JMSException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgttest.automation.mock.MockJmsMessageDispatcher;


/**
 * @author smishra1
 *
 */
public class CncExtractFacade {

    private static final String CNC_ORDER_EXTRACT_QUEUE = "cnc-orders-extract-queue";

    private CncExtractFacade() {
        // Default constructor
    }

    public static CncExtractDto getCncMessageExtract() throws MessageConversionException, JMSException {
        final String message = MockJmsMessageDispatcher.getMessage(CNC_ORDER_EXTRACT_QUEUE);
        if (null != message) {
            try {
                final JAXBContext cncOrderExtractMessageContext = JAXBContext.newInstance(CncExtractDto.class);
                return (CncExtractDto)cncOrderExtractMessageContext.createUnmarshaller().unmarshal(
                        new ByteArrayInputStream(message.getBytes()));
            }
            catch (final JAXBException e) {
                throw new MessageConversionException("Error while Marshalling the CNC Message Extract :", e);
            }
        }
        return null;
    }

    @XmlRootElement(name = "order")
    public static class CncExtractDto {
        private String storeNum;
        private String id;
        private String type;
        private String title;
        private String firstName;
        private String surname;
        private String email;
        private String mobile;
        private String parcelCount;
        private String paymentStatus;

        public String getStoreNum() {
            return storeNum;
        }

        @XmlAttribute
        public void setStoreNum(final String storeNum) {
            this.storeNum = storeNum;
        }

        public String getId() {
            return id;
        }

        @XmlAttribute
        public void setId(final String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        @XmlAttribute
        public void setType(final String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        @XmlElement
        public void setTitle(final String title) {
            this.title = title;
        }

        public String getFirstName() {
            return firstName;
        }

        @XmlElement
        public void setFirstName(final String firstName) {
            this.firstName = firstName;
        }

        public String getSurname() {
            return surname;
        }

        @XmlElement
        public void setSurname(final String surname) {
            this.surname = surname;
        }

        public String getEmail() {
            return email;
        }

        @XmlElement
        public void setEmail(final String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        @XmlElement
        public void setMobile(final String mobile) {
            this.mobile = mobile;
        }

        public String getParcelCount() {
            return parcelCount;
        }

        @XmlElement
        public void setParcelCount(final String parcelCount) {
            this.parcelCount = parcelCount;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        @XmlElement
        public void setPaymentStatus(final String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.c2l.CurrencyModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgttest.automation.ServiceLookup;


/**
 * currency utils
 * 
 */
public final class CurrencyUtil {

    private CurrencyUtil() {
        // Utility
    }

    /**
     * Get standard currency model, AUD
     * 
     * @return AUD
     */
    public static CurrencyModel getStandardCurrency() {
        final CurrencyModel example = new CurrencyModel();
        example.setIsocode("AUD");
        final List<CurrencyModel> list = ServiceLookup.getFlexibleSearchService().getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(list))
        {
            return list.get(0);
        }
        return null;
    }

}

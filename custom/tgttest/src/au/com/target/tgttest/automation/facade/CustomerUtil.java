/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.exceptions.PasswordMismatchException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtfacades.user.data.TargetCustomerData;
import au.com.target.tgtfacades.user.data.TargetRegisterData;
import au.com.target.tgtfacades.user.impl.TargetUserFacadeImpl;
import au.com.target.tgtpayment.dto.CreditCard;
import au.com.target.tgtpayment.model.IpgCreditCardPaymentInfoModel;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.CreditCardData;
import au.com.target.tgttest.automation.facade.bean.CustomerAccountData;
import au.com.target.tgttest.automation.facade.domain.Customer;


/**
 * Customer utils
 * 
 */
public final class CustomerUtil {

    private static final String DEFAULT_GUEST_USER_ID = "test.guest1@target.com.au";

    // Registered customer
    private static final String STANDARD_CUSTOMER_ID = "test.user1@target.com.au";
    private static final String STANDARD_CUSTOMER_PASSWD = "possword1";

    private static final String UPDATED_CUSTOMER_PASSWD = "test#123456";
    private static final String INVALID_CUST_PASSWD = "testpassword";

    private CustomerUtil() {
        // Utility
    }

    /**
     * Get the standard test customer email id
     * 
     * @return user id
     */
    public static String getStandardTestCustomerUid() {
        return STANDARD_CUSTOMER_ID;
    }



    /**
     * 
     * 
     * @return the defaultGuestUserId
     */
    public static String getDefaultGuestUserId() {
        return DEFAULT_GUEST_USER_ID;
    }


    /**
     * Get standard country
     * 
     * @return Australia
     */
    public static CountryModel getStandardCountry() {
        final CountryModel example = new CountryModel();
        example.setIsocode("AU");
        final List<CountryModel> list = ServiceLookup.getFlexibleSearchService().getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    /**
     * Register the customer represented by Customer.customerData
     * 
     * 
     * @throws DuplicateUidException
     * @throws InterruptedException
     */
    public static void registeruser() throws DuplicateUidException, InterruptedException {

        final CustomerAccountData customerData = Customer.getInstance().getCustomerData();
        final TargetRegisterData registerData = populateTargetCustomerData(customerData);
        ServiceLookup.getTargetCustomerFacade().register(registerData);
        BusinessProcessCustomerUtil.waitForProcessToFinishAndCheckSuccess(
                TgtbusprocConstants.BusinessProcess.CUSTOMER_REGISTRATION_PROCESS, registerData.getLogin());
    }

    /**
     * Log in the user represented by Customer.customerData
     */
    public static void loginCustomer() {
        SessionUtil.userLogin(Customer.getInstance().getCustomerData().getLogin());
    }


    public static void loginCustomer(final String uid) {
        SessionUtil.userLogin(uid);
    }


    /**
     * Return the current logged in customer
     * 
     * @return CustomerAccountData
     */
    public static CustomerAccountData getLoggedInCustomerData() {

        final TargetCustomerData custData = getCurrentCustomer();
        final CustomerAccountData data = new CustomerAccountData();
        data.setFirstName(custData.getFirstName());
        data.setLastName(custData.getLastName());
        data.setLogin(custData.getUid());
        return data;
    }


    /**
     * Get current customer data
     * 
     * @return TargetCustomerData
     */
    public static TargetCustomerData getCurrentCustomer() {

        final TargetCustomerData targetCustomerData = (TargetCustomerData)ServiceLookup.getTargetCustomerFacade()
                .getCurrentCustomer();
        return targetCustomerData;
    }


    /**
     * Checks whether the customer is a part of a group
     * 
     * @return TargetCustomerData
     */
    public static boolean isCurrentCustomerPartOfTheGroup(final String groupId) {

        final TargetCustomerModel targetCustomerModel = (TargetCustomerModel)ServiceLookup.getUserService()
                .getCurrentUser();
        final Set<PrincipalGroupModel> principlalGroupModels = targetCustomerModel.getGroups();
        for (final PrincipalGroupModel principalGroupModel : principlalGroupModels) {
            if (StringUtils.equals(principalGroupModel.getUid(), groupId)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Update the first name of the current logged in user
     * 
     * @throws DuplicateUidException
     */
    public static void updateFirstName(final String updatedName) throws DuplicateUidException {

        final TargetCustomerData custData = getCurrentCustomer();
        custData.setFirstName(updatedName);
        ServiceLookup.getTargetCustomerFacade().updateProfile(custData);

    }

    /**
     * Update the password with the new one for the given customer
     * 
     * @throws InterruptedException
     */
    public static void changePassword() throws InterruptedException {
        final TargetCustomerModel model = CustomerUtil.getTargetCustomer(STANDARD_CUSTOMER_ID);
        Customer.setStandardCustomer();
        ServiceLookup.getUserService().setCurrentUser(model);
        ServiceLookup.getTargetCustomerFacade().changePassword(STANDARD_CUSTOMER_PASSWD,
                UPDATED_CUSTOMER_PASSWD);
        BusinessProcessCustomerUtil.waitForProcessToFinishAndCheckSuccess(
                TgtbusprocConstants.BusinessProcess.CUSTOMER_PASSWORD_CHANGE_PROCESS,
                STANDARD_CUSTOMER_ID);
    }

    /**
     * Update the email address/login for the customer with standard password
     * 
     * @throws DuplicateUidException
     */
    public static void updateEmailAddress(final String updatedEmail)
            throws DuplicateUidException {
        ServiceLookup.getTargetCustomerFacade().changeUid(updatedEmail, STANDARD_CUSTOMER_PASSWD);

    }

    /**
     * Update the email address/login for the customer with Invalid password
     * 
     * @throws DuplicateUidException
     */
    public static void updateEmailAddressWithInvalidPasswd(final String updatedEmail) throws DuplicateUidException,
            PasswordMismatchException {
        ServiceLookup.getTargetCustomerFacade().changeUid(updatedEmail, INVALID_CUST_PASSWD);

    }

    /**
     * Update the default address of the customer
     */
    public static void updateDefaultAddress(final String street, final String town, final String postCode) {

        // TODO could we just use ServiceLookup.getTargetUserFacade().getDefaultAddress(); ?
        final List<AddressData> addresses = getAddressData();
        AddressData defaultAddress = new AddressData();
        for (final AddressData addressData : addresses) {
            if ((null != addressData) && (addressData.isDefaultAddress())) {
                defaultAddress = addressData;
                break;
            }
        }
        defaultAddress.setLine1(street);
        defaultAddress.setTown(town);
        defaultAddress.setPostalCode(postCode);
        ServiceLookup.getTargetUserFacade().editAddress(defaultAddress);
    }

    /**
     * Removes the default address of the customer
     */
    public static void removeCustomerAddress() {
        final AddressData address = ServiceLookup.getTargetUserFacade().getDefaultAddress();
        ServiceLookup.getTargetUserFacade().removeAddress(address);
    }

    /**
     * Removes all addresses of the customer
     */
    public static void removeAllCustomerAddresses() {
        final List<AddressData> addresses = ServiceLookup.getTargetUserFacade().getAddressBook();
        for (final AddressData address : addresses) {
            ServiceLookup.getTargetUserFacade().removeAddress(address);
        }
    }

    /**
     * Add new address to the logged in customer
     */
    public static void addNewAddress() {
        final AddressEntry entry = Customer.getInstance().getAddressEntry();
        final CountryData countryData = new CountryData();
        countryData.setIsocode("AU");
        countryData.setName(entry.getCountry());
        final AddressData addressData = new AddressData();
        addressData.setLine1(entry.getAddressLine1());
        addressData.setTown(entry.getTown());
        addressData.setPostalCode(entry.getPostalCode());
        addressData.setDefaultAddress(entry.isDefaultAddress());
        addressData.setCountry(countryData);
        ServiceLookup.getTargetUserFacade().addAddress(addressData);
    }

    /**
     * gets the default address of th4 logged in customer
     * 
     * @return AddressEntry
     */
    public static AddressEntry getDefaultAddress() {
        final AddressEntry addressEnty = populateAddressEntry(ServiceLookup.getTargetUserFacade().getDefaultAddress());
        return addressEnty;
    }

    /**
     * Gets the targetCsutomerModel for the given uid from database
     * 
     * @param userEmail
     * @return TargetCustomerModel
     */
    public static TargetCustomerModel getTargetCustomer(final String userEmail) {
        final TargetCustomerModel example = new TargetCustomerModel();
        example.setUid(userEmail);
        final List<TargetCustomerModel> list = ServiceLookup.getFlexibleSearchService().getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    /**
     * Get all the addresses for the current logged in user
     * 
     * @return list of AddressData
     */
    public static List<AddressData> getAddressData() {
        final List<AddressData> addresses = ServiceLookup.getTargetUserFacade().getAddressBook();
        return addresses;
    }



    /**
     * Populate the TargetRegisterData which is used by facade layer
     * 
     * @param customerData
     * @return TargetRegisterData
     */

    public static TargetRegisterData populateTargetCustomerData(final CustomerAccountData customerData) {

        final TargetRegisterData registerdata = new TargetRegisterData();
        registerdata.setFirstName(customerData.getFirstName());
        registerdata.setLastName(customerData.getLastName());
        registerdata.setLogin(customerData.getLogin());
        registerdata.setPassword(customerData.getPassword());
        registerdata.setTitleCode(customerData.getTitle());
        registerdata.setMobileNumber(customerData.getMobileNumber());
        return registerdata;
    }

    /**
     * Populates the AddressEntry from the AddressData
     * 
     * @param addressData
     * @return AddressEntry
     */
    public static AddressEntry populateAddressEntry(final AddressData addressData) {
        AddressEntry addressEnty = null;
        if (null != addressData) {
            addressEnty = new AddressEntry();
            addressEnty.setAddressLine1(addressData.getLine1());
            addressEnty.setAddressLine2(addressData.getLine2());
            addressEnty.setBillingAddress(addressData.isBillingAddress());
            addressEnty.setShippingAddress(addressData.isShippingAddress());
            addressEnty.setDefaultAddress(addressData.isDefaultAddress());
            addressEnty.setPostalCode(addressData.getPostalCode());
            addressEnty.setTown(addressData.getTown());
        }
        return addressEnty;
    }

    /**
     * Resets the test user data
     */
    public static void resetUserData() {

        ImpexImporter.importCsv("/tgttest/automation-impex/user/reset-test-users.impex");
    }

    /**
     * create the guest user with user email and name
     * 
     * @return customerModel
     * @throws DuplicateUidException
     */
    public static CustomerModel getDefaultGuestUserModel() throws DuplicateUidException {

        final CustomerModel customerModel = ServiceLookup.getTargetCustomerAccountService()
                .registerGuestForAnonymousCheckout(
                        CustomerUtil.getDefaultGuestUserId(), "Guest");

        return customerModel;
    }

    public static List<CCPaymentInfoData> getSavedCreditCards() {
        return ServiceLookup.getTargetUserFacade().getCCPaymentInfos(true);
    }

    public static List<CreditCard> getSavedIpgCreditCards() {

        final List<CreditCardPaymentInfoModel> savedCreditCards = ServiceLookup.getTargetCustomerAccountService()
            .getCreditCardPaymentInfos((TargetCustomerModel)ServiceLookup.getUserService().getCurrentUser(), true);
        final List<CreditCard> savedCardsList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(savedCreditCards)) {
            for (final CreditCardPaymentInfoModel creditCardPaymentInfoModel : savedCreditCards) {

                if (creditCardPaymentInfoModel instanceof IpgCreditCardPaymentInfoModel) {
                    final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel = (IpgCreditCardPaymentInfoModel)creditCardPaymentInfoModel;

                    final CreditCard creditCard = new CreditCard();
                    creditCard.setToken(ipgCreditCardPaymentInfoModel.getToken());
                    creditCard.setTokenExpiry(ipgCreditCardPaymentInfoModel.getTokenExpiry());
                    creditCard.setCardType(ipgCreditCardPaymentInfoModel.getType().toString());
                    creditCard.setMaskedCard(ipgCreditCardPaymentInfoModel.getNumber());
                    creditCard.setCardExpiry(ipgCreditCardPaymentInfoModel.getValidToMonth() + "/"
                        + ipgCreditCardPaymentInfoModel.getValidToYear());
                    creditCard.setBin(ipgCreditCardPaymentInfoModel.getBin());
                    creditCard.setDefaultCard(ipgCreditCardPaymentInfoModel.getDefaultCard());
                    creditCard.setPromoId(ipgCreditCardPaymentInfoModel.getPromoId());

                    creditCard.setCardOnFile(ipgCreditCardPaymentInfoModel.isCardOnFile());
                    creditCard.setFirstCredentialStorage(ipgCreditCardPaymentInfoModel.isFirstCredentialStorage());
                    savedCardsList.add(creditCard);
                }
            }
        }
        return savedCardsList;
    }
    /**
     * @param savedCreditCards
     */
    @SuppressWarnings("boxing")
    public static void setIpgSavedCreditCards(final List<CreditCardData> savedCreditCards) {
        removeSavedCreditCards();
        for (final CreditCardData ccData : savedCreditCards) {
            createIpgPaymentInfo(ccData);
        }
    }

    public static void removeSavedCreditCards() {
        final TargetUserFacadeImpl targetUserFacade = ServiceLookup.getTargetUserFacade();
        final List<CCPaymentInfoData> ccPaymentInfos = targetUserFacade.getCCPaymentInfos(true);
        for (final CCPaymentInfoData ccPaymentInfo : ccPaymentInfos) {
            targetUserFacade.unlinkCCPaymentInfo(ccPaymentInfo.getId());
        }
    }

    /**
     * @param id
     */
    public static void deleteSavedCard(final String id) {
        final TargetUserFacadeImpl targetUserFacade = ServiceLookup.getTargetUserFacade();
        targetUserFacade.unlinkCCPaymentInfo(id);
    }

    public static void setupRegisteredCustomers() {
        ImpexImporter.importCsv("/tgttest/automation-impex/user/remove-test-customer.impex");
        ImpexImporter.importCsv("/tgttest/automation-impex/user/customer-login-test-users.impex");
    }

    /**
     * @param ccData
     */
    @SuppressWarnings("boxing")
    private static void createIpgPaymentInfo(final CreditCardData ccData) {
        final ModelService modelService = ServiceLookup.getModelService();
        final IpgCreditCardPaymentInfoModel ipgCreditCardPaymentInfoModel = modelService.create(
                IpgCreditCardPaymentInfoModel.class);
        final TargetCustomerModel customerModel = (TargetCustomerModel)ServiceLookup.getUserService().getCurrentUser();

        ipgCreditCardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
        ipgCreditCardPaymentInfoModel.setUser(customerModel);
        ipgCreditCardPaymentInfoModel.setNumber(ccData.getCardNumber());
        ipgCreditCardPaymentInfoModel.setOwner(customerModel);
        ipgCreditCardPaymentInfoModel.setIsPaymentSucceeded(true);

        if (StringUtils.isNotEmpty(ccData.getCardType())) {
            final CreditCardType cardType = CreditCardType.valueOf(ccData.getCardType());
            ipgCreditCardPaymentInfoModel.setType(cardType);
        }

        if (StringUtils.isNotEmpty(ccData.getCardExpiry())) {
            ipgCreditCardPaymentInfoModel.setValidToMonth(PaymentTransactionHelper.extractCardExpiryMonth(ccData
                    .getCardExpiry()));
            ipgCreditCardPaymentInfoModel.setValidToYear(PaymentTransactionHelper.extractCardExpiryYear(ccData
                    .getCardExpiry()));
        }

        // IPG does not accept name on credit card.
        ipgCreditCardPaymentInfoModel.setCcOwner("");
        ipgCreditCardPaymentInfoModel.setToken(ccData.getToken());
        ipgCreditCardPaymentInfoModel.setTokenExpiry(ccData.getTokenExpiry());
        ipgCreditCardPaymentInfoModel.setBin(ccData.getBin());
        ipgCreditCardPaymentInfoModel.setDefaultCard(Boolean.valueOf(ccData.isDefaultCard()));
        ipgCreditCardPaymentInfoModel.setCardOnFile(ccData.getCardOnFile() != null ? ccData.getCardOnFile() : false);
        ipgCreditCardPaymentInfoModel.setFirstCredentialStorage(
                ccData.getFirstCredentialStorage() != null ? ccData.getFirstCredentialStorage() : false);
        ipgCreditCardPaymentInfoModel.setSaved(true);
        modelService.save(ipgCreditCardPaymentInfoModel);
        modelService.refresh(ipgCreditCardPaymentInfoModel);
        if (ipgCreditCardPaymentInfoModel.getDefaultCard()) {
            customerModel.setDefaultPaymentInfo(ipgCreditCardPaymentInfoModel);
            modelService.save(customerModel);
            modelService.refresh(customerModel);
        }

    }

}

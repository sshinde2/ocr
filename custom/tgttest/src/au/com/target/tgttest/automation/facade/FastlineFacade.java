/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.fest.assertions.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.springframework.jms.support.converter.MessageConversionException;

import au.com.target.tgtjms.jms.model.ConsignmentExtract;
import au.com.target.tgtjms.jms.model.OrderExtractList;
import au.com.target.tgttest.automation.mock.MockJmsMessageDispatcher;


/**
 * Utilities for checking what gets sent to Fastline
 * 
 * @author sbryan6
 *
 */
public final class FastlineFacade {

    private FastlineFacade() {
        //util
    }

    /**
     * Verify the last consignment extract that went to Fastline exists and return it
     * 
     * @return list
     */
    public static List<ConsignmentExtract> getLastConsignmentExtracts() {

        final String lastOrderExtractMessage = MockJmsMessageDispatcher
                .getMessage("FastlineWarehouse-orderExtract-queue");
        final OrderExtractList orderExtractList = FastlineFacade.getOrderExtractList(lastOrderExtractMessage);
        assertThat(orderExtractList).as("extract list").isNotNull();
        assertThat(orderExtractList.getOrderExtracts()).as("extract list").isNotEmpty();

        return orderExtractList.getOrderExtracts();
    }

    public static List<List<ConsignmentExtract>> getAllExtractMessages() {
        final List<String> lastOrderExtractMessages = MockJmsMessageDispatcher
                .getAllMessages("FastlineWarehouse-orderExtract-queue");
        final List<List<ConsignmentExtract>> consignmentExtracts = new ArrayList<List<ConsignmentExtract>>();
        for (final String extract : lastOrderExtractMessages) {
            final OrderExtractList orderExtractList = getOrderExtractList(extract);
            assertThat(orderExtractList).as("extract list").isNotNull();
            assertThat(orderExtractList.getOrderExtracts()).as("extract list").isNotEmpty();
            consignmentExtracts.add(orderExtractList.getOrderExtracts());
        }
        return consignmentExtracts;
    }

    /**
     * Return true if no consignments were sent to fastline during the test
     * 
     * @return boolean
     */
    public static boolean noConsignmentsWereSentToFastline() {

        final String lastOrderExtractMessage = MockJmsMessageDispatcher
                .getMessage("FastlineWarehouse-orderExtract-queue");

        return (lastOrderExtractMessage == null);
    }


    private static OrderExtractList getOrderExtractList(final String messagePayload) {
        final InputStream stream = new ByteArrayInputStream(messagePayload.getBytes());
        try {
            final JAXBContext orderExtractJaxbContext = JAXBContext.newInstance(OrderExtractList.class);
            return (OrderExtractList)orderExtractJaxbContext.createUnmarshaller().unmarshal(stream);
        }
        catch (final JAXBException e) {
            throw new MessageConversionException("Unable to unmarshal OrderExtractList", e);
        }
    }

}

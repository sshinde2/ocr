/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.ConsignmentForStore;
import au.com.target.tgttest.automation.facade.bean.PickEntry;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;
import au.com.target.tgtwsfacades.integration.dto.Consignment;
import au.com.target.tgtwsfacades.integration.dto.Consignments;
import au.com.target.tgtwsfacades.integration.dto.OrderConfirm;
import au.com.target.tgtwsfacades.integration.dto.Orders;
import au.com.target.tgtwsfacades.integration.dto.PickConfirmEntries;
import au.com.target.tgtwsfacades.integration.dto.PickConfirmOrder;
import au.com.target.tgtwsfacades.integration.dto.ShipConfirm;
import au.com.target.tgtwsfacades.pickconfirm.impl.PickConfirmIntegrationFacadeImpl;


/**
 * @author rsamuel3
 * 
 */
public final class FulfilmentFacade {

    private FulfilmentFacade() {
        // utility class prevent construction
    }

    /**
     * Process fastline ack
     * 
     * @param orderOrConsignmentCode
     */
    public static void processAck(final String orderOrConsignmentCode) {
        final Orders orders = new Orders();
        orders.addOrder(orderOrConsignmentCode);
        ServiceLookup.getOrderAckIntFacade().updateOrderAcknowledgements(orders);
    }

    /**
     * Processes the pick file
     * 
     * @param orderOrConsignmentCode
     * @param consignmentCode
     * @param carrier
     * @param trackingNumber
     * @param parcelCount
     * @param entries
     * @throws FulfilmentException
     */
    public static void processPickFile(final String orderOrConsignmentCode, final String consignmentCode,
            final String carrier,
            final String trackingNumber, final Integer parcelCount,
            final List<PickEntry> entries) throws FulfilmentException {
        final PickConfirmEntries pickConfirmEntries = new PickConfirmEntries();
        final List<PickConfirmEntry> pickConfEntryList = convertPickEntry(entries);
        pickConfirmEntries.setPickConfirmEntries(pickConfEntryList);
        final PickConfirmOrder dto = new PickConfirmOrder();
        dto.setOrderNumber(orderOrConsignmentCode);
        dto.setConsignmentNumber(consignmentCode);
        dto.setCarrier(carrier);
        dto.setParcelCount(parcelCount);
        dto.setPickConfirmEntries(pickConfirmEntries);
        getPickConfIntegrationFacade().handlePickConfirm(dto);
    }

    /**
     * Processes the ship file
     * 
     * @param orderOrConsignmentCode
     * @param date
     * @throws FulfilmentException
     */
    public static void processShipConfirm(final String orderOrConsignmentCode, final Date date)
            throws FulfilmentException {

        final ShipConfirm confirm = new ShipConfirm();
        final ArrayList<OrderConfirm> orderConfirms = new ArrayList<>();
        final OrderConfirm confirm1 = new OrderConfirm();
        confirm1.setOrderNumber(orderOrConsignmentCode);
        confirm1.setManifestNumber("MANFEST1");
        confirm1.setDate(date);
        orderConfirms.add(confirm1);
        confirm.setOrderConfirms(orderConfirms);

        ServiceLookup.getOrderShipIntFacade().handleShipConfirm(confirm);
    }

    /**
     * Processes the cancel for a consignment.
     * 
     * @param consignmentCode
     * @throws FulfilmentException
     */
    public static void processCancelConsignment(final String consignmentCode)
            throws FulfilmentException {
        final Consignments consignments = new Consignments();
        final Consignment consignment = new Consignment();
        consignment.setConsignmentId(consignmentCode);
        consignments.setConsignments(Arrays.asList(consignment));
        ServiceLookup.getConsignmentIntegrationFacade().cancelConsignments(consignments);
    }

    /**
     * Processes the complete for a consignment.
     * 
     * @param consignmentsReceived
     * @throws FulfilmentException
     */
    public static void processCompleteConsignment(final List<ConsignmentForStore> consignmentsReceived)
            throws FulfilmentException {
        for (final ConsignmentForStore consignmentReceived : consignmentsReceived) {
            final Consignments consignments = new Consignments();
            final Consignment consignment = new Consignment();
            consignment.setConsignmentId(consignmentReceived.getConsignmentCode());
            consignment.setCarrier(consignmentReceived.getCarrier());
            final ComplexDateExpressionParser dateUtil = new ComplexDateExpressionParser();
            consignment.setShipDate(dateUtil.interpretStringAsDate(consignmentReceived.getShipDate()));
            consignment.setParcelCount(consignmentReceived.getParcelCount());
            consignment.setTrackingNumber(consignmentReceived.getTrackingId());
            consignments.setConsignments(Arrays.asList(consignment));
            ServiceLookup.getConsignmentIntegrationFacade().completeConsignments(consignments);
        }
    }


    /**
     * converts PickEntry to PickConfirmEntry
     * 
     * @param entries
     * @return List of PickConfirmEntry
     */
    private static List<PickConfirmEntry> convertPickEntry(final List<PickEntry> entries) {
        final List<PickConfirmEntry> pickEntries = new ArrayList<>();
        for (final PickEntry entry : entries) {
            final PickConfirmEntry pickEntry = new PickConfirmEntry();
            pickEntry.setItemCode(entry.getProduct());
            pickEntry.setQuantityShipped(Integer.valueOf(entry.getQty()));
            pickEntries.add(pickEntry);
        }
        return pickEntries;
    }

    public static TargetFulfilmentService getTargetFulfilmentService() {
        return ServiceLookup.getTargetFulfilmentService();
    }

    public static PickConfirmIntegrationFacadeImpl getPickConfIntegrationFacade() {
        return ServiceLookup.getPickConfIntegrationFacade();
    }

    public static void processCompleteConsignmentWithDefaultData(final String consignmentCode)
            throws FulfilmentException {
        final Consignments consignments = new Consignments();
        final Consignment consignment = new Consignment();
        consignment.setConsignmentId(consignmentCode);
        consignment.setCarrier("AP");
        consignment.setShipDate(new Date());
        consignment.setParcelCount(Integer.valueOf(1));
        consignment.setTrackingNumber("JQ12345");
        consignments.setConsignments(Arrays.asList(consignment));
        ServiceLookup.getConsignmentIntegrationFacade().completeConsignments(consignments);
    }
}

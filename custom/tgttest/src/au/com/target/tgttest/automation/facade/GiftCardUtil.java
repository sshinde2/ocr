/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.product.ProductModel;

import au.com.target.tgtcore.giftcards.dto.GiftRecipientDTO;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.product.exception.ProductNotFoundException;
import au.com.target.tgtfacades.giftcards.exceptions.GiftCardValidationException;
import au.com.target.tgtfacades.giftcards.validate.GiftCardValidationErrorType;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.GiftCardCartEntry;
import au.com.target.tgttinker.client.impl.MockSendToWarehouseRestClient.SendToWarehouseResponseType;


/**
 * Utility for dealing with gift card records
 *
 */
public class GiftCardUtil {

    private GiftCardUtil() {
        // util
    }

    /**
     * Set limitations on the given gift card
     * 
     * @param brandId
     * @param maxOrderQty
     * @param maxOrderValue
     */
    public static void setGiftCardLimitations(final String brandId, final Integer maxOrderQty,
            final Integer maxOrderValue) {

        final GiftCardModel giftCard = getGiftCard(brandId);
        assertThat(giftCard).isNotNull();

        giftCard.setMaxOrderQuantity(maxOrderQty);
        giftCard.setMaxOrderValue(maxOrderValue);

        ServiceLookup.getModelService().save(giftCard);
    }

    /**
     * Get GiftCardModel for given brandId
     * 
     * @param brandId
     * @return GiftCardModel
     */
    public static GiftCardModel getGiftCard(final String brandId) {

        return ServiceLookup.getGiftCardService().getGiftCard(brandId);
    }

    /**
     * Add one quantity of gift card to the cart
     * 
     * @param cartEntry
     * @throws CommerceCartModificationException
     */
    public static void addToCart(final GiftCardCartEntry cartEntry) throws CommerceCartModificationException {
        final GiftRecipientDTO giftRecipientDTO = convertToRecipientDTO(cartEntry);
        ServiceLookup.getTargetCartFacade().addToCart(cartEntry.getProductCode(), 1, giftRecipientDTO);
    }

    /**
     * Add one quantity of digital gift card to the cart.
     *
     * @param giftCardCartEntry
     * @throws CommerceCartModificationException
     * @return cartModificationData
     */
    public static CartModificationData digitalGiftCardAddToCart(final GiftCardCartEntry giftCardCartEntry)
            throws CommerceCartModificationException {

        final GiftRecipientDTO giftRecipientDTO = convertToRecipientDTO(giftCardCartEntry);

        final CartModificationData cartModificationData = ServiceLookup.getTargetCartFacade().addToCart(
                giftCardCartEntry.getProductCode(), 1, giftRecipientDTO);

        return cartModificationData;
    }



    /**
     * checks if the product is a giftcard product
     * 
     * @param product
     * @throws ProductNotFoundException
     */
    public static boolean isProductAGiftCard(final ProductModel product) throws ProductNotFoundException {
        final GiftCardModel giftCard = ServiceLookup.getGiftCardService().getGiftCardForProduct(product.getCode());
        if (giftCard != null) {
            return true;
        }
        return false;
    }

    /**
     * remove recipient Entry from cart
     * 
     * @param productCode
     * @param recipientEntryNumber
     * @throws CommerceCartModificationException
     */
    public static void removeRecipientEntry(final String productCode, final int recipientEntryNumber, final String id)
            throws CommerceCartModificationException {

        ServiceLookup.getTargetCartFacade()
                .removeGiftCardRecipientEntry(productCode, recipientEntryNumber, id);
    }

    /**
     * @param cartEntry
     * @return GiftRecipientDTO
     */
    private static GiftRecipientDTO convertToRecipientDTO(final GiftCardCartEntry cartEntry) {
        final GiftRecipientDTO recipientDTO = new GiftRecipientDTO();
        recipientDTO.setFirstName(cartEntry.getFirstName());
        recipientDTO.setLastName(cartEntry.getLastName());
        recipientDTO.setRecipientEmailAddress(cartEntry.getEmail());
        recipientDTO.setMessageText(cartEntry.getMessageText());
        return recipientDTO;
    }



    /**
     * @param productCode
     * @return GiftCardValidationErrorType
     * @throws ProductNotFoundException
     */
    public static GiftCardValidationErrorType validateGiftCardCart(final String productCode)
            throws ProductNotFoundException {
        try {
            ServiceLookup.getTargetCartFacade().validateGiftCards(productCode, 1L);
        }
        catch (final GiftCardValidationException e) {
            return e.getErrorType();
        }
        return null;
    }

    public static void removeGiftCardIfExist(final String brandId) {
        final GiftCardModel giftCard = ServiceLookup.getGiftCardService().getGiftCard(brandId);
        if (giftCard != null) {
            ServiceLookup.getModelService().remove(giftCard);
        }
    }

    public static void setErrorResponseFromIncommWarehouse(final String errorResponse) {
        ServiceLookup.getSendToWarehouseRestClient().setResponseType(SendToWarehouseResponseType.ERROR);
        if (errorResponse.equals("AlreadyExists")) {
            ServiceLookup.getSendToWarehouseRestClient().setErrorValue("103:Order Already Exists");
        }
        else if (errorResponse.equals("InsufficientInventory")) {
            ServiceLookup.getSendToWarehouseRestClient().setErrorValue("400208: Insufficient Inventory");
        }
        else if (errorResponse.equals("BrandNotFound")) {
            ServiceLookup.getSendToWarehouseRestClient().setErrorValue("404201: Brand Not Found");
        }
        else if (errorResponse.equals("ServiceUnavailable")) {
            ServiceLookup.getSendToWarehouseRestClient().setErrorValue("na: Service Unavailable");
        }
    }

}

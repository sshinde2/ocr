/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Util for dealing with Manifests
 * 
 * @author sbryan6
 *
 */
public class ManifestUtil {

    private ManifestUtil() {
        // utility
    }

    /**
     * Remove all manifests.
     */
    public static void removeAllManifests() {
        ServiceLookup.getManifestCreationHelper().removeExistingManifests();
    }

}

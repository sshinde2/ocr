/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.fest.assertions.Assertions.assertThat;

import org.fest.assertions.Assertions;

import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtshareddto.eftwrapper.dto.EftPaymentDto;
import au.com.target.tgtshareddto.eftwrapper.dto.HybrisResponseDto;
import au.com.target.tgtshareddto.eftwrapper.dto.PaymentDetailsDto;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.TlogPinPadPaymentInfo;


/**
 * Facade for pinpad payment tests
 * 
 */
public final class PinPadUtil {

    private PinPadUtil() {
        // util
    }

    public static void initialise() {

        // Set short retry period 10s for pinpad payment retries, with 3 retries
        ImpexImporter.importCsv("/tgttest/automation-impex/pinpad/pinpad-payment-with-short-retry.impex");
    }

    /**
     * Update hybris payment for the given order
     * 
     * @param orderId
     * @param paymentSuccess
     *            success of the pinpad payment
     */
    public static void updatePinPadPayment(final String orderId, final boolean paymentSuccess) {

        final EftPaymentDto eftpaymentdto = PinPadUtil.buildEftPaymentDto(orderId, paymentSuccess);
        final HybrisResponseDto response = ServiceLookup.getPinpadPaymentFacade().updatePinPadPayment(eftpaymentdto);

        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccessStatus()).isTrue();
        Assertions.assertThat(response.getMessages()).isEmpty();
    }

    private static EftPaymentDto buildEftPaymentDto(final String orderId, final boolean success) {
        final EftPaymentDto dto = new EftPaymentDto();
        dto.setOrderId(orderId);
        final PaymentDetailsDto paymentDetails = new PaymentDetailsDto();
        paymentDetails.setAuthCode("098987345");
        paymentDetails.setJournalRoll("This is the journal Roll");
        paymentDetails.setPan("5123450000000346");
        paymentDetails.setRrn("234");
        paymentDetails.setRespAscii("0");
        paymentDetails.setTransactionSuccess(success);
        paymentDetails.setCardType("5");
        paymentDetails.setCardName("visa");
        paymentDetails.setAccountType("2");
        paymentDetails.setJournal("This is the journal");
        dto.setPaymentDetails(paymentDetails);
        return dto;
    }

    /**
     * Verify the payment info is populated as expected
     * 
     * @param pinpadInfo
     */
    public static void verifyPaymentInfoIsPopulated(final PinPadPaymentInfoModel pinpadInfo) {

        assertThat(pinpadInfo.getAuthCode()).as("pinpad auth code").isEqualTo("098987345");
        assertThat(pinpadInfo.getJournRoll()).as("pinpad journRoll").isEqualTo("This is the journal Roll");
        assertThat(pinpadInfo.getJournal()).as("pinpad journal").isEqualTo("This is the journal");
        assertThat(pinpadInfo.getMaskedCardNumber()).as("pinpad masked card no").isEqualTo("5123450000000346");
        assertThat(pinpadInfo.getRrn()).as("pinpad rrn").isEqualTo("234");
        assertThat(pinpadInfo.getRespAscii()).as("pinpad resp").isEqualTo("0");
    }

    /**
     * Verify the pinpad payment info is empty
     * 
     * @param pinpadInfo
     */
    public static void verifyPaymentInfoIsEmpty(final PinPadPaymentInfoModel pinpadInfo) {

        assertThat(pinpadInfo.getAuthCode()).as("pinpad auth code").isNullOrEmpty();
        assertThat(pinpadInfo.getJournRoll()).as("pinpad journRoll").isNullOrEmpty();
        assertThat(pinpadInfo.getJournal()).as("pinpad journal").isNullOrEmpty();
        assertThat(pinpadInfo.getMaskedCardNumber()).as("pinpad masked card no").isNullOrEmpty();
        assertThat(pinpadInfo.getRrn()).as("pinpad rrn").isNullOrEmpty();
        assertThat(pinpadInfo.getRespAscii()).as("pinpad rrn").isNullOrEmpty();
    }

    /**
     * Get the expected pinpad tlog info, given the payment details
     * 
     * @return TlogPinPadPaymentInfo
     */
    public static TlogPinPadPaymentInfo getPinPadTlogInfoExpected() {

        // Expect the output in tlog to look same as for tns 
        final TlogPinPadPaymentInfo paymentInfo = new TlogPinPadPaymentInfo("5123450000000346", "234", true);
        return paymentInfo;
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import au.com.target.tgtcore.model.TargetPriceRowModel;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductItemDto;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductsDto;
import au.com.target.tgttest.automation.facade.domain.PosProductsDto;


/**
 * @author bhuang3
 * 
 */
public final class PosPriceUpdateUtil {

    private PosPriceUpdateUtil() {
        //
    }

    /**
     * set posProductsDto
     * 
     * @param prouductId
     * @param flag
     */
    public static void setPosProductsDto(final String prouductId, final boolean flag) {
        final IntegrationPosProductsDto dto = new IntegrationPosProductsDto();
        final IntegrationPosProductItemDto itemDto = new IntegrationPosProductItemDto();
        itemDto.setItemCode(prouductId);
        itemDto.setCurrentSalePrice(10);
        itemDto.setPromoEvent(flag);
        final List<IntegrationPosProductItemDto> itemDtoList = new ArrayList<>();
        itemDtoList.add(itemDto);
        dto.setProducts(itemDtoList);
        PosProductsDto.setPosProductsDto(dto);
    }

    public static Boolean getPricePromoEventFromProductId(final String productId) {
        final ProductModel productModel = ProductUtil.getOnlineProductModel(productId);
        final Collection<PriceRowModel> pricesRowList = productModel.getEurope1Prices();
        return ((TargetPriceRowModel)pricesRowList.iterator().next()).getPromoEvent();
    }
}

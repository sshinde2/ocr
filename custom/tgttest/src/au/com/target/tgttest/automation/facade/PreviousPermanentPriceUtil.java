/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.TargetPreviousPermanentPriceModel;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductItemDto;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductItemPriceDto;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductsDto;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.POSPriceEntry;
import au.com.target.tgttest.automation.facade.bean.PrevPermanentPriceEntry;
import au.com.target.tgttest.automation.facade.domain.PosProductsDto;
import au.com.target.tgttest.automation.util.ComplexDateExpressionParser;


/**
 * @author mjanarth
 * 
 */
public final class PreviousPermanentPriceUtil {

    private PreviousPermanentPriceUtil()
    {
        // util
    }

    /**
     * Clears all the PreviousPrice from DB
     * 
     * @param variantCode
     */
    public static void clearPreviousPermanentPrice(final String variantCode) {
        final List<TargetPreviousPermanentPriceModel> previousPermList = getPreviousPermanentPrices(variantCode);
        if (CollectionUtils.isNotEmpty(previousPermList)) {
            ServiceLookup.getModelService().removeAll(previousPermList);
        }
    }

    /**
     * Returns the targetpreviouspermanentPrice with end date as null
     * 
     * @param variantCode
     * @return targetPreviousPriceModel
     */
    public static TargetPreviousPermanentPriceModel getPreviousPermPriceWithNullEndDate(final String variantCode) {

        final TargetPreviousPermanentPriceModel previousPermPrice = ServiceLookup
                .getTgtPreviouspermPriceService()
                .getPreviousPermanentPriceWithNullEndDate(variantCode);
        return previousPermPrice;

    }

    /**
     * Creates the DTO for POSIntegrationFacade
     * 
     * @param variantCode
     * @param posPriceEntries
     */
    public static void createPOSIntegrationData(final String variantCode, final List<POSPriceEntry> posPriceEntries) {
        final IntegrationPosProductItemDto dto = new IntegrationPosProductItemDto();

        final ArrayList<IntegrationPosProductItemPriceDto> futurePrices = new ArrayList<>();
        dto.setItemCode(variantCode);
        if (CollectionUtils.isNotEmpty(posPriceEntries)) {
            dto.setPermanentPrice(posPriceEntries.get(0).getPermPrice().doubleValue());
            dto.setPromoEvent(posPriceEntries.get(0).isPromoEvent());
            dto.setCurrentSalePrice(posPriceEntries.get(0).getSellPrice());
            dto.setRemoveFromSale(false);
        }
        if (CollectionUtils.isNotEmpty(posPriceEntries) && (posPriceEntries.size() > 1)) {
            for (final POSPriceEntry entry : posPriceEntries) {
                final IntegrationPosProductItemPriceDto futurePrice = new IntegrationPosProductItemPriceDto();
                if (entry.getDate().equalsIgnoreCase("null")) {
                    futurePrice.setStart(null);
                }
                else {
                    final Calendar cal = new ComplexDateExpressionParser().interpretStringAsCalendar(entry.getDate());
                    futurePrice.setStart(new Date(cal.getTimeInMillis()));
                }

                futurePrice.setSellPrice(entry.getSellPrice());
                futurePrice.setPermPrice(entry.getPermPrice().doubleValue());
                futurePrices.add(futurePrice);
            }
            dto.setPrices(futurePrices);
        }

        final List<IntegrationPosProductItemDto> dtoList = new ArrayList<>();
        dtoList.add(dto);
        final IntegrationPosProductsDto integrationPosDto = new IntegrationPosProductsDto();
        integrationPosDto.setProducts(dtoList);
        PosProductsDto.setPosProductsDto(integrationPosDto);
    }

    /**
     * Get all the previous Permanent Prices
     * 
     * @param variantCode
     * @return list
     */
    public static List<TargetPreviousPermanentPriceModel> getPreviousPermanentPrices(final String variantCode) {
        final String query = "select {"
                + TargetPreviousPermanentPriceModel.PK
                + "} from {" + TargetPreviousPermanentPriceModel._TYPECODE
                + " } WHERE {TargetPreviousPermanentPrice.VARIANTCODE} = ?productCode";
        final FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(query);
        searchQuery.addQueryParameter("productCode", variantCode);
        final SearchResult<TargetPreviousPermanentPriceModel> searchResult = ServiceLookup.getFlexibleSearchService()
                .search(searchQuery);
        return searchResult.getResult();
    }

    /**
     * creates the targetPrevPermanentPrice in Database
     * 
     * @param priceEntry
     */
    public static TargetPreviousPermanentPriceModel createPreviousPermanentPrice(
            final PrevPermanentPriceEntry priceEntry) {
        final TargetPreviousPermanentPriceModel prevPriceRowModel = ServiceLookup.getModelService().create(
                TargetPreviousPermanentPriceModel.class);
        prevPriceRowModel.setCurrency(getAustralianDollar());
        prevPriceRowModel.setPrice(Double.valueOf(priceEntry.getPrice()));
        if (null != priceEntry.getStartDate()) {
            final Calendar cal = new ComplexDateExpressionParser().interpretStringAsCalendar(priceEntry.getStartDate());
            prevPriceRowModel.setStartDate(new Date(cal.getTimeInMillis()));
        }
        if (priceEntry.getEndDate().equalsIgnoreCase("null") || priceEntry.getEndDate().equalsIgnoreCase("Not Set")) {
            prevPriceRowModel.setEndDate(null);
        }
        else if (null != priceEntry.getEndDate()) {
            final Calendar cal = new ComplexDateExpressionParser().interpretStringAsCalendar(priceEntry.getEndDate());
            prevPriceRowModel.setEndDate(new Date(cal.getTimeInMillis()));
        }
        prevPriceRowModel.setVariantCode(priceEntry.getVariantCode());
        ServiceLookup.getModelService().save(prevPriceRowModel);

        return prevPriceRowModel;
    }

    public static void removeAllPrevPermPriceRecords() {
        final String queryString = "select {" + TargetPreviousPermanentPriceModel.PK
                + "} from {" + TargetPreviousPermanentPriceModel._TYPECODE + "}";
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);

        final SearchResult<TargetPreviousPermanentPriceModel> allPreviousPermanentPrices = ServiceLookup
                .getFlexibleSearchService().<TargetPreviousPermanentPriceModel> search(query);
        ServiceLookup.getModelService().removeAll(allPreviousPermanentPrices.getResult());
    }

    /**
     * 
     * @return currency
     */
    private static CurrencyModel getAustralianDollar() {
        final CurrencyModel example = new CurrencyModel();
        example.setIsocode("AUD");
        final List<CurrencyModel> list = ServiceLookup.getFlexibleSearchService().getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(list))
        {
            return list.get(0);
        }
        return null;
    }

    /**
     * import the wasNow config impex
     */
    public static void initialise() {
        ImpexImporter.importCsv("/tgttest/automation-impex/product/was-now-price-config.impex");
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Sales Application util
 * 
 */
public final class SalesApplicationUtil {

    private static final String MOBILE_APP = "MobileApp";
    private static final String WEB = "Web";
    private static final String KIOSK = "Kiosk";
    private static final String EBAY = "Ebay";
    private static final String WEBMOBILE = "WebMobile";

    private SalesApplicationUtil() {
        //Should not instantiate
    }

    public static void setSalesApplication(final String salesChannelCode) {
        switch (salesChannelCode) {
            case MOBILE_APP:
                ServiceLookup.getTargetSalesApplicationService().setCurrentSalesApplication(SalesApplication.MOBILEAPP);
                break;
            case WEB:
                ServiceLookup.getTargetSalesApplicationService().setCurrentSalesApplication(SalesApplication.WEB);
                break;
            case EBAY:
                ServiceLookup.getTargetSalesApplicationService().setCurrentSalesApplication(SalesApplication.KIOSK);
                break;
            case WEBMOBILE:
                ServiceLookup.getTargetSalesApplicationService().setCurrentSalesApplication(SalesApplication.KIOSK);
                break;
            case KIOSK:
                ServiceLookup.getTargetSalesApplicationService().setCurrentSalesApplication(SalesApplication.KIOSK);
                break;
            default:
                ServiceLookup.getTargetSalesApplicationService().setCurrentSalesApplication(SalesApplication.WEB);
                break;
        }
    }

    public static void resetSalesApplicationToDefault() {
        setSalesApplication(WEB);
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import static org.junit.Assert.assertEquals;

import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.ProductImportData;
import au.com.target.tgttest.automation.facade.bean.ProductImportVariantData;
import au.com.target.tgtwsfacades.integration.dto.IntegrationActivationDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductMediaDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSecondaryImagesDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;
import au.com.target.tgtwsfacades.productimport.impl.TargetProductImportIntegrationFacade;


/**
 * @author knemalik
 * 
 */
public final class StepProductUtil {


    private StepProductUtil() {

    }

    public static void initializeStepImportData() {
        ImpexImporter.importCsv("/tgtwsfacades/test/testProductimportExpressbyflagFeatureOn.impex");
    }

    /**
     * set stepProductsDto
     * 
     * @param productCode
     */
    public static IntegrationActivationDto getIntegrationActivationData(final String productCode,
            final String approvalStatus) {
        final IntegrationActivationDto dto = new IntegrationActivationDto();
        dto.setVariantCode(productCode);
        dto.setApprovalStatus(approvalStatus);
        return dto;

    }

    public static IntegrationProductDto getColourVariantOnlyData(final String code) {

        // this is adapted from TargetProductImportIntegrationFacadeUnitTest
        final IntegrationProductDto dto = new IntegrationProductDto();

        dto.setIsAssortment(Boolean.FALSE);
        dto.setIsStylegroup(Boolean.TRUE);
        dto.setProductCode("P" + code);
        dto.setName("Baby Thing");
        dto.setProductType("normal");
        dto.setPrimaryCategory("W93746");
        dto.setSecondaryCategory(Collections.singletonList("W93746"));
        dto.setBrand("brand");
        dto.setApprovalStatus("active");
        dto.setVariantCode(code);

        dto.setDepartment(Integer.valueOf(999));

        // description
        dto.setDescription("blahblahblah");

        // bulky product / correct product type
        dto.setBulky("N");

        // available for layby
        dto.setAvailableLayby("Y");

        dto.setAvailableLongtermLayby("Y");

        // available for CNC
        dto.setAvailableCnc("Y");

        // available for MHD
        dto.setAvailableHomeDelivery("Y");

        // sizeType
        dto.setIsSizeOnly(Boolean.FALSE);
        dto.setSizeType("BabySize");

        return dto;
    }

    public static IntegrationProductDto getColourVariantOnlyData(final ProductImportData importData) {
        final IntegrationProductDto dto = getColourVariantOnlyData(importData.getProductCode());
        dto.setName(importData.getProductName());
        dto.setVariantCode(importData.getVariantCode());
        dto.setPrimaryCategory(importData.getPrimaryCategory());
        List<String> secondaryCategories = null;
        if (StringUtils.isNotEmpty(importData.getSecondaryCategory())) {
            secondaryCategories = Arrays.asList(importData.getSecondaryCategory());
        }
        dto.setSecondaryCategory(secondaryCategories);
        dto.setBrand(importData.getBrand());
        if (StringUtils.isNotEmpty(importData.getDepartment())) {
            dto.setDepartment(Integer.valueOf(importData.getDepartment()));
        }
        else {
            dto.setDepartment(null);
        }

        if (importData.getExcludePaymentMethods() != null) {
            final List<String> methods = new ArrayList<>(Arrays.asList(importData.getExcludePaymentMethods()
                    .split(",")));
            dto.setExcludePaymentMethods(methods);
        }

        dto.setDescription(importData.getProductDescription());
        dto.setApprovalStatus(importData.getApprovalStatus());
        dto.setShowIfOutOfStock(importData.getShowWhenOutOfStock());
        dto.setBulky(importData.getBulky());
        dto.setProductType(importData.getProductType());
        dto.setAvailableExpressDelivery(importData.getAvailableExpressDelivery());
        dto.setAvailableLayby(importData.getAvailableForLayby());
        dto.setAvailableLongtermLayby(importData.getAvailableLongTermLayby());
        if (importData.getAvailableCnc() != null) {
            dto.setAvailableCnc(importData.getAvailableCnc());
        }
        dto.setIsSizeOnly(Boolean.valueOf(importData.isSizeOnly()));
        if (StringUtils.isNotEmpty(importData.getSizeType())) {
            dto.setSizeType(importData.getSizeType());
        }
        else {
            dto.setSizeType(null);
        }
        if (StringUtils.isNotEmpty(importData.getSize())) {
            dto.setSize(importData.getSize());
        }
        else {
            dto.setSize(null);
        }
        dto.setProductOnlineDate(importData.getOnlineDate());
        dto.setProductOfflineDate(importData.getOfflineDate());
        if (StringUtils.isNotEmpty(importData.getAvailableHomeDelivery())) {
            dto.setAvailableHomeDelivery(importData.getAvailableHomeDelivery());
        }
        else {
            dto.setAvailableHomeDelivery(null);
        }
        if (StringUtils.isNotEmpty(importData.getShowWhenOutOfStock())) {
            dto.setShowIfOutOfStock(
                    "NA".equals(importData.getShowWhenOutOfStock()) ? null : importData.getShowWhenOutOfStock());
        }

        if (StringUtils.isNotBlank(importData.getWarehouse())) {
            final List<String> warehouses = new ArrayList<>();
            warehouses.add(importData.getWarehouse());
            dto.setWarehouses(warehouses);
        }
        dto.setDenominaton(importData.getDenominaton());
        dto.setGiftcardBrandId(importData.getGiftcardBrandId());
        dto.setCareInstructions(importData.getCareInstructions());
        dto.setMaterials(importData.getMaterials());
        dto.setWebExtraInfo(importData.getWebExtraInfo());
        dto.setWebFeatures(importData.getWebFeatures());
        dto.setAvailableEbayExpressDelivery(importData.getAvailableEbayExpressDelivery());
        dto.setAvailableOnEbay(importData.getAvailableOnEbay());
        dto.setEan(importData.getEan());
        dto.setMerchProductStatus(importData.getMerchProductStatus());
        setupMedia(dto, importData);

        dto.setPreOrderEmbargoReleaseDate(importData.getPreOrderEmbargoReleaseDate());
        dto.setPreOrderStartDate(importData.getPreOrderStartDate());
        dto.setPreOrderEndDate(importData.getPreOrderEndDate());
        dto.setPreOrderOnlineQuantity(importData.getPreOrderOnlineQuantity());
        if (StringUtils.isNotEmpty(importData.getDisplayOnly())) {
            dto.setDisplayOnly(importData.getDisplayOnly());
        }
        return dto;
    }

    /**
     * @param importData
     */
    private static void setupMedia(final IntegrationProductDto dto, final ProductImportData importData) {
        final TargetProductImportIntegrationFacade targetProductImportFacade = ServiceLookup
                .getTargetProductImportFacade();
        targetProductImportFacade.setAssociateMedia(StringUtils.isNotEmpty(importData.getPrimaryImage()));
        final IntegrationProductMediaDto media = new IntegrationProductMediaDto();
        media.setPrimaryImage(importData.getPrimaryImage());
        final IntegrationSecondaryImagesDto secImgs = new IntegrationSecondaryImagesDto();
        if (importData.getSecondaryImages() != null) {
            secImgs.setSecondaryImage(Arrays.asList(importData.getSecondaryImages()));
        }
        media.setSecondaryImages(secImgs);
        if ("NA".equals(importData.getPrimaryImage())) {
            dto.setProductMedia(null);
        }
        else {
            dto.setProductMedia(media);
        }
    }

    public static IntegrationProductDto populateVariants(final IntegrationProductDto productDto,
            final ProductImportVariantData variantData) {
        final IntegrationVariantDto variantDto = populateAVariant(variantData.getCode());
        variantDto.setApprovalStatus(variantData.getApprovalStatus());
        variantDto.setSize(variantData.getSize());
        variantDto.setMerchProductStatus(variantData.getMerchProductStatus());
        productDto.addVariant(variantDto);
        return productDto;
    }

    public static IntegrationProductDto getSizeVariantOnlyData(final String code) {

        // this is adapted from TargetProductImportIntegrationFacadeUnitTest
        final IntegrationProductDto dto = new IntegrationProductDto();

        dto.setIsAssortment(Boolean.FALSE);
        dto.setIsStylegroup(Boolean.TRUE);
        dto.setProductCode("P" + code);
        dto.setName("Baby Thing");
        dto.setProductType("normal");
        dto.setPrimaryCategory("W93746");
        dto.setSecondaryCategory(Collections.singletonList("W93746"));
        dto.setBrand("brand");
        dto.setApprovalStatus("active");
        dto.setVariantCode(code);

        dto.setDepartment(Integer.valueOf(355));

        // description
        dto.setDescription("blahblahblah");

        // bulky product / correct product type
        dto.setBulky("N");

        // available for layby
        dto.setAvailableLayby("Y");

        dto.setAvailableLongtermLayby("Y");

        // available for CNC
        dto.setAvailableCnc("Y");

        // available for MHD
        dto.setAvailableHomeDelivery("Y");

        // sizeType
        dto.setIsSizeOnly(Boolean.TRUE);

        dto.setSizeType("BabySize");
        dto.setSize("L");

        return dto;
    }

    public static IntegrationProductDto getColourVariantWithVariantsData(final String code) {

        // this is adapted from TargetProductImportIntegrationFacadeUnitTest
        final IntegrationProductDto dto = new IntegrationProductDto();

        dto.setIsAssortment(Boolean.FALSE);
        dto.setIsStylegroup(Boolean.TRUE);
        dto.setProductCode("P" + code);
        dto.setName("Baby Thing");
        dto.setProductType("normal");
        dto.setPrimaryCategory("W93746");
        dto.setSecondaryCategory(Collections.singletonList("W93746"));
        dto.setBrand("brand");
        dto.setApprovalStatus("active");
        dto.setVariantCode("C" + code);

        dto.setDepartment(Integer.valueOf(355));

        // description
        dto.setDescription("blahblahblah");

        // bulky product / correct product type
        dto.setBulky("N");

        // available for layby
        dto.setAvailableLayby("Y");

        dto.setAvailableLongtermLayby("Y");

        // available for CNC
        dto.setAvailableCnc("Y");

        // available for MHD
        dto.setAvailableHomeDelivery("Y");

        // sizeType
        dto.setIsSizeOnly(Boolean.FALSE);
        dto.setSizeType("BabySize");

        final IntegrationVariantDto sizeVar = populateAVariant(code);

        dto.getVariants().add(sizeVar);

        return dto;
    }

    /**
     * @param code
     * @return IntegrationVariantDto
     */
    private static IntegrationVariantDto populateAVariant(final String code) {
        final IntegrationVariantDto sizeVar = new IntegrationVariantDto();
        sizeVar.setApprovalStatus("active");
        sizeVar.setBaseProduct("C" + code);
        sizeVar.setEan("159159");
        sizeVar.setSize("M");
        sizeVar.setVariantCode(code);
        return sizeVar;
    }

    public static IntegrationProductDto getGiftCardData(final String code) {

        final IntegrationProductDto dto = getColourVariantWithVariantsData(code);

        dto.setName("Gift Card");
        dto.setProductType("digital");

        // available for CNC
        dto.setAvailableCnc("Y");

        // available for MHD
        dto.setAvailableHomeDelivery("Y");

        return dto;
    }

    public static boolean containsDepartment(final TargetProductModel targetProductModel, final String department) {
        TargetMerchDepartmentModel targetMerchDepartmentModel = null;
        for (final CategoryModel categoryModel : targetProductModel.getSupercategories()) {
            if (categoryModel instanceof TargetMerchDepartmentModel) {
                targetMerchDepartmentModel = (TargetMerchDepartmentModel)categoryModel;
                if (department.equals(targetMerchDepartmentModel.getCode())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void verifyProductFeatures(final ProductImportData dto, final TargetProductModel baseProduct) {
        final FeatureList features = ServiceLookup.getClassificationService().getFeatures(baseProduct);
        for (final Feature feature : features.getFeatures()) {
            final String code = feature.getClassAttributeAssignment().getClassificationAttribute().getCode();

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_LICENSE)) {
                final FeatureValue featureValue = feature.getValue();
                final Object value = featureValue != null ? featureValue.getValue() : null;
                assertEquals("Can create new base product - feature / licence", dto.getLicense(),
                        value);
            }

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_GENDER)) {
                final List<FeatureValue> genders = feature.getValues();
                for (final FeatureValue val : genders) {
                    assertEquals("Can create new base product - feature / licence", dto.getGenders()[0],
                            val.getDescription());
                    assertEquals("Can create new base product - feature / licence", dto.getGenders()[0]
                            .toLowerCase(),
                            ((ClassificationAttributeValueModel)val.getValue()).getCode());
                }
            }

            if (code.equalsIgnoreCase(TargetProductImportConstants.FEATURE_AGE_RANGE)) {
                final FeatureValue featureValue = feature.getValue();
                final Object value = featureValue != null ? featureValue.getValue() : null;
                assertEquals("Can create new base product - feature / age", dto.getAgeFrom(),
                        value);
            }
        }
    }

}

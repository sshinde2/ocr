/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.paymentstandard.model.StandardPaymentModeModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.SavedCncStoreDetailsModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtpayment.enums.PaymentProviderTypeEnum;
import au.com.target.tgtpayment.model.PaypalPaymentInfoModel;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.PreviousOrderDetails;
import au.com.target.tgttest.automation.facade.bean.SavedUserPreferences;
import au.com.target.tgttest.automation.facade.bean.SinglePageCheckoutPrePopulatedCartDetails;


/**
 * @author ajit
 *
 */
public class UserSinglePageCheckoutPreferencesUtil {

    private static final String PAYMENT_MODE = "CreditCard";
    private static final String NOT_AVAILABLE = "N/A";
    private static final String ORDER_NUMBER = "auto1010101";
    private static final String EXPRESS_DELIVERY = "express-delivery";

    private UserSinglePageCheckoutPreferencesUtil() {
        //        private constructor
    }


    public static void setRegisteredUserWithCreateNewCart() throws Exception {
        CheckoutUtil.createAnyCart();
        CartUtil.createCheckoutCart(CheckoutUtil.REGISTERED_USER_CHECKOUT);
    }

    public static void setRegisteredUserWithCart() throws DuplicateUidException {
        CartUtil.createCheckoutCart(CheckoutUtil.REGISTERED_USER_CHECKOUT);
    }

    /**
     * Creates the cc payment info.
     *
     * @param userSavedPreference
     *            the user saved preference
     * @param customerModel
     *            the customer model
     * @return CreditCardPaymentInfoModel
     */
    public static CreditCardPaymentInfoModel createCCPaymentInfo(final SavedUserPreferences userSavedPreference,
            final TargetCustomerModel customerModel) {
        final CreditCardPaymentInfoModel paymentInfoModel = getCCPaymentInfoModel(userSavedPreference.getCreditCard(),
                customerModel, true);
        return paymentInfoModel;
    }

    /**
     * Creates the saved cnc store details.
     *
     * @param userSavedPreference
     *            the user saved preference
     * @param customerModel
     *            the customer model
     * @return the saved cnc store details model
     * @throws TargetUnknownIdentifierException
     *             the target unknown identifier exception
     * @throws TargetAmbiguousIdentifierException
     *             the target ambiguous identifier exception
     */
    public static SavedCncStoreDetailsModel createSavedCNCStoreDetails(
            final SavedUserPreferences userSavedPreference,
            final TargetCustomerModel customerModel)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final SavedCncStoreDetailsModel cncStore = ServiceLookup.getModelService().create(
                SavedCncStoreDetailsModel.class);
        cncStore.setFirstName("firstName");
        cncStore.setLastName("lastName");
        cncStore.setPhoneNumber("1234567891");
        cncStore.setTitle("Mr");
        final String[] posDetails = userSavedPreference.getCncDetails().split(",");
        cncStore.setStore(ServiceLookup.getTargetPointOfServiceService().getPOSByStoreNumber(
                Integer.valueOf(posDetails[1])));
        cncStore.setTargetCustomer(customerModel);
        ServiceLookup.getModelService().save(cncStore);
        ServiceLookup.getModelService().refresh(cncStore);
        return cncStore;
    }


    /**
     * Gets the previous order payment details.
     *
     * @param previousOrder
     *            the previous order
     * @param customerModel
     *            the customer model
     * @return the previous order payment details
     */
    public static PaymentInfoModel getPreviousOrderPaymentDetails(final PreviousOrderDetails previousOrder,
            final TargetCustomerModel customerModel) {
        final PaymentInfoModel paymentInfoModel;
        if (PAYMENT_MODE.equalsIgnoreCase(previousOrder.getPaymentType())) {
            paymentInfoModel = getCCPaymentInfoModel("C0000000000001", customerModel, false);
        }
        else {
            paymentInfoModel = getPaypalPaymentInfoModel(customerModel);
        }
        return paymentInfoModel;
    }

    /**
     * @param customerModel
     * @return paymentInfoModel
     */
    private static PaymentInfoModel getPaypalPaymentInfoModel(final TargetCustomerModel customerModel) {
        final PaypalPaymentInfoModel paymentInfoModel = ServiceLookup.getModelService().create(
                PaypalPaymentInfoModel.class);
        paymentInfoModel.setCode("Paypal");
        paymentInfoModel.setPayerId("paypalPayerId");
        paymentInfoModel.setToken("paypalPaymentToken");
        paymentInfoModel.setUser(customerModel);
        return paymentInfoModel;
    }

    /**
     * @param creditCardNum
     * @param customerModel
     * @return CreditCardPaymentInfoModel
     */
    protected static CreditCardPaymentInfoModel getCCPaymentInfoModel(final String creditCardNum,
            final TargetCustomerModel customerModel, final boolean saved) {
        final CreditCardPaymentInfoModel paymentInfoModel = ServiceLookup.getModelService().create(
                CreditCardPaymentInfoModel.class);
        paymentInfoModel.setSaved(saved);
        paymentInfoModel.setSubscriptionValidated(true);
        paymentInfoModel.setIsPaymentSucceeded(Boolean.TRUE);
        paymentInfoModel.setNumber(creditCardNum);
        paymentInfoModel.setValidToMonth("Jan");
        paymentInfoModel.setValidToYear("2018");
        paymentInfoModel.setCode("Code");
        paymentInfoModel.setCcOwner("CcOwner");
        paymentInfoModel.setType(CreditCardType.VISA);
        paymentInfoModel.setUser(customerModel);
        return paymentInfoModel;
    }

    /**
     * @param deliveryAddress
     * @return targetAddressModel
     */
    public static TargetAddressModel getAddressModel(final String deliveryAddress) {
        TargetAddressModel targetAddressModel = ServiceLookup.getModelService().create(TargetAddressModel.class);
        if (!NOT_AVAILABLE.equalsIgnoreCase(deliveryAddress)) {
            final String address[] = deliveryAddress.split(",");
            targetAddressModel.setFirstname("firstName");
            targetAddressModel.setLastname("lastName");
            targetAddressModel.setPhone1("1234567898");
            targetAddressModel.setLine1(address[0]);
            targetAddressModel.setLine2(address[1]);
            targetAddressModel.setTown(address[2]);
            final CountryModel countryModel = ServiceLookup.getCommonI18nService().getCountry("AU");

            for (final RegionModel regionModel : countryModel.getRegions()) {
                if (regionModel.getAbbreviation().equalsIgnoreCase(address[3])) {
                    targetAddressModel.setRegion(regionModel);
                    break;
                }
            }
            targetAddressModel.setPostalcode(address[4]);
            targetAddressModel.setCountry(countryModel);
            targetAddressModel.setOwner(countryModel);
            ServiceLookup.getModelService().save(targetAddressModel);
            ServiceLookup.getModelService().refresh(targetAddressModel);
        }
        else {
            targetAddressModel = null;
        }
        return targetAddressModel;
    }

    /**
     * @param userSavedPreferences
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    public static void setCustomerPreferences(final List<SavedUserPreferences> userSavedPreferences)
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final SavedUserPreferences userSavedPreference = userSavedPreferences.get(0);

        final TargetCustomerModel customerModel = CustomerUtil.getTargetCustomer(CustomerUtil
                .getStandardTestCustomerUid());
        setDeliveryMode(userSavedPreference, customerModel);
        setSavedCNCStoreDetails(userSavedPreference, customerModel);
        setPaymentInfo(userSavedPreference, customerModel);
        final TargetAddressModel targetAddress = getAddressModel(userSavedPreference.getDefaultDeliveryAddress());
        customerModel.setDefaultShipmentAddress(targetAddress);
        ServiceLookup.getModelService().save(customerModel);
    }

    /**
     * @param previousOrders
     * @throws Exception
     */
    public static void setCustomerPreviourOrder(final List<PreviousOrderDetails> previousOrders) throws Exception {
        final PreviousOrderDetails previousOrder = previousOrders.get(0);
        final TargetCustomerModel customerModel = CustomerUtil.getTargetCustomer(CustomerUtil
                .getStandardTestCustomerUid());
        final List<OrderModel> orderList = new ArrayList<>();
        final OrderModel orderModel = ServiceLookup.getOrderCreationHelper().createOrder(
                previousOrder.getPreviousDeliveryMode(), ORDER_NUMBER, true, Integer.valueOf(00), Double.valueOf(00),
                "WEB");

        orderModel.setDeliveryMode(ServiceLookup.getTargetDeliveryService()
                .getDeliveryModeForCode(previousOrder.getPreviousDeliveryMode()));

        final PaymentInfoModel paymentInfoModel = getPreviousOrderPaymentDetails(previousOrder, customerModel);
        orderModel.setPaymentInfo(paymentInfoModel);
        final StandardPaymentModeModel paymentMode = ServiceLookup.getModelService().create(
                StandardPaymentModeModel.class);
        paymentMode.setActive(Boolean.TRUE);
        orderModel.setPaymentMode(ServiceLookup.getPaymentModeService().getPaymentModeForCode(
                PaymentProviderTypeEnum.CREDITCARD.getCode()));

        final TargetAddressModel targetAddress = getAddressModel(previousOrder
                .getDeliveryAddress());
        orderModel.setDeliveryAddress(targetAddress);

        ServiceLookup.getModelService().save(orderModel);
        orderList.add(orderModel);
        customerModel.setOrders(orderList);
        ServiceLookup.getModelService().save(paymentInfoModel);
        ServiceLookup.getModelService().save(customerModel);
    }

    /**
     * @throws CalculationException
     */
    public static void doCheckoutPopulateCart() throws CalculationException {
        ServiceLookup.getPrepopulateCheckoutFacade().isCartPrePopulatedForCheckout();
    }

    /**
     * @param userSavedPreference
     * @param customerModel
     */
    public static void setPaymentInfo(final SavedUserPreferences userSavedPreference,
            final TargetCustomerModel customerModel) {
        List<PaymentInfoModel> paymentInfoModels = Collections.EMPTY_LIST;
        if (!NOT_AVAILABLE.equalsIgnoreCase(userSavedPreference.getCreditCard())) {
            paymentInfoModels = new ArrayList<>();
            final CreditCardPaymentInfoModel paymentInfoModel = createCCPaymentInfo(
                    userSavedPreference, customerModel);
            ServiceLookup.getModelService().save(paymentInfoModel);
            paymentInfoModels.add(paymentInfoModel);
        }
        customerModel.setPaymentInfos(paymentInfoModels);
    }


    /**
     * @param userSavedPreference
     * @param customerModel
     * @throws TargetUnknownIdentifierException
     * @throws TargetAmbiguousIdentifierException
     */
    public static void setSavedCNCStoreDetails(final SavedUserPreferences userSavedPreference,
            final TargetCustomerModel customerModel) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        List<SavedCncStoreDetailsModel> cncStoreDetails = Collections.EMPTY_LIST;
        if (!NOT_AVAILABLE.equalsIgnoreCase(userSavedPreference.getCncDetails())) {
            cncStoreDetails = new ArrayList<>();
            final SavedCncStoreDetailsModel cncStore = createSavedCNCStoreDetails(
                    userSavedPreference, customerModel);
            cncStoreDetails.add(cncStore);
        }
        customerModel.setSavedCncStoreDetails(cncStoreDetails);
    }


    /**
     * @param userSavedPreference
     * @param customerModel
     */
    public static void setDeliveryMode(final SavedUserPreferences userSavedPreference,
            final TargetCustomerModel customerModel) {
        if (!NOT_AVAILABLE.equalsIgnoreCase(userSavedPreference.getDeliveryMode())) {
            customerModel.setPreferredDeliveryMode((TargetZoneDeliveryModeModel)ServiceLookup
                    .getTargetDeliveryService()
                    .getDeliveryModeForCode(userSavedPreference.getDeliveryMode()));
        }
        else {
            customerModel.setPreferredDeliveryMode(null);
        }
    }


    /**
     * @param prePopulateCart
     * @param cart
     */
    public static void verifyCartModel(final List<SinglePageCheckoutPrePopulatedCartDetails> prePopulateCart,
            final CartModel cart) {
        final SinglePageCheckoutPrePopulatedCartDetails expectedCartDetails = prePopulateCart.get(0);
        Assert.assertNotNull(expectedCartDetails);
        Assert.assertNotNull(cart);
        final TargetCustomerModel customer = (TargetCustomerModel)cart.getUser();
        final SavedCncStoreDetailsModel savedCncDetails = CollectionUtils
                .isNotEmpty(customer.getSavedCncStoreDetails()) ? customer
                .getSavedCncStoreDetails().iterator().next() : null;
        if (!NOT_AVAILABLE.equalsIgnoreCase(expectedCartDetails.getDeliveryMode())) {
            Assert.assertEquals(expectedCartDetails.getDeliveryMode(), cart.getDeliveryMode().getCode());
        }
        else {
            Assert.assertNull(cart.getDeliveryMode());
        }
        Assert.assertNull(cart.getPaymentInfo());
        final TargetAddressModel addressModel = (TargetAddressModel)cart.getDeliveryAddress();
        if (!NOT_AVAILABLE.equalsIgnoreCase(expectedCartDetails.getDeliveryDetails())) {
            final String address[] = expectedCartDetails.getDeliveryDetails().split(",");
            Assert.assertEquals(address[0], addressModel.getLine1());
            Assert.assertEquals(address[1], addressModel.getLine2());
            Assert.assertEquals(address[2], addressModel.getTown());
            Assert.assertEquals(address[3], addressModel.getRegion().getAbbreviation());
            Assert.assertEquals(address[4], addressModel.getPostalcode());
            final String storeDetails[] = expectedCartDetails.getCNCDetails().split(",");
            if (savedCncDetails != null) {
                Assert.assertEquals(storeDetails[0], savedCncDetails.getStore().getAddress().getTown());
                Assert.assertEquals(storeDetails[1], savedCncDetails.getStore().getStoreNumber().toString());
            }
        }
        final DeliveryModeModel deliveryMode = customer.getOrders().iterator().next().getDeliveryMode();
        if (EXPRESS_DELIVERY.equalsIgnoreCase(deliveryMode.getCode())) {
            Assert.assertNull(addressModel);
        }
    }
}

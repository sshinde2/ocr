/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.price.DiscountModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.voucher.model.DateRestrictionModel;
import de.hybris.platform.voucher.model.RestrictionModel;
import de.hybris.platform.voucher.model.VoucherModel;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.InstanceofPredicate;

import au.com.target.tgtcore.model.FlybuysDiscountModel;
import au.com.target.tgttest.automation.ImpexImporter;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Util for setting up and applying vouchers
 * 
 */
public final class VoucherUtil {

    private static final int ONE_DAY_IN_MILLISECONDS = 86400000;

    private VoucherUtil() {
        // utility
    }

    public static void initialise() {

        // We need to initialise voucher before each scenario to remove invalidations
        ImpexImporter.importCsv("/tgttest/automation-impex/voucher/vouchers-teardown.impex");
        ImpexImporter.importCsv("/tgttest/automation-impex/voucher/vouchers.impex");
    }

    public static void teardown() {

        ImpexImporter.importCsv("/tgttest/automation-impex/voucher/vouchers-teardown.impex");
    }


    public static void initialiseRestrictions() {
        ImpexImporter.importCsv("/tgttest/automation-impex/voucher/voucher-restrictions-teardown.impex");
        ImpexImporter.importCsv("/tgttest/automation-impex/voucher/voucher-restrictions.impex");
    }

    public static void teardownRestrictions() {
        ImpexImporter.importCsv("/tgttest/automation-impex/voucher/voucher-restrictions-teardown.impex");
    }

    public static void expireVoucher(final String voucher) {
        final Date date = new Date();
        date.setTime(date.getTime() - ONE_DAY_IN_MILLISECONDS);
        final VoucherModel voucherModel = ServiceLookup.getVoucherService().getVoucher(voucher);
        final Set<RestrictionModel> restrictions = voucherModel.getRestrictions();
        for (final RestrictionModel restriction : restrictions) {
            if (restriction instanceof DateRestrictionModel) {
                final DateRestrictionModel dateRestriction = (DateRestrictionModel)restriction;
                dateRestriction.setEndDate(date);
            }
        }
    }

    /**
     * Apply a voucher to the checkout cart
     * 
     * @param voucher
     * @param cartModel
     * @throws CalculationException
     */
    public static void applyVoucher(final String voucher, final CartModel cartModel) throws CalculationException {

        final VoucherModel voucherModel = ServiceLookup.getVoucherService().getVoucher(voucher);
        if (voucherModel != null && ServiceLookup.getVoucherModelService().isApplicable(voucherModel, cartModel)) {

            ServiceLookup.getTargetCheckoutFacade().applyVoucher(voucher);
            CartUtil.recalculateSessionCart();
        }

    }

    /**
     * check flybuys discount in cart
     * 
     * @return true or false
     */
    public static boolean isFlybuysDiscountInCart() {
        final CartModel cartModel = CartUtil.getSessionCartModel();
        final Collection<DiscountModel> appliedVouchers = ServiceLookup.getVoucherService().getAppliedVouchers(
                cartModel);
        CollectionUtils.filter(appliedVouchers, InstanceofPredicate.getInstance(FlybuysDiscountModel.class));
        return CollectionUtils.isNotEmpty(appliedVouchers);
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.user.UserModel;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttest.automation.facade.bean.WishListEntry;
import au.com.target.tgtwishlist.data.TargetWishListEntryData;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.logger.TgtWishListLogger;
import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author pthoma20
 *
 */
public class WishListUtil {

    private WishListUtil() {
        //Do not Instantiate
    }

    /**
     * 
     * @param maxEntry
     */
    public static void setMaxEntries(final Integer maxEntry) {
        ServiceLookup.getTargetWishListService().setMaxListSize(maxEntry);
    }

    /**
     * 
     * @param targetWishListModel
     * @param entries
     * @throws ParseException
     */
    public static void setUpProductsForCustomerWishList(final TargetWishListModel targetWishListModel,
            final List<WishListEntry> entries) throws ParseException {
        for (final WishListEntry wishListEntry : entries) {
            ServiceLookup.getTargetWishListService().addProductToWishList(targetWishListModel,
                    createTargetWishListEntryDataFromWishListEntry(wishListEntry),
                    TgtWishListLogger.ADD_FAV_LIST_ACTION,
                    ServiceLookup.getTargetSalesApplicationService().getCurrentSalesApplication());
        }
    }

    /**
     * 
     * @param wishListEntry
     * @return targetWishListEntryData
     * @throws ParseException
     */
    private static TargetWishListEntryData createTargetWishListEntryDataFromWishListEntry(
            final WishListEntry wishListEntry) throws ParseException {
        final TargetWishListEntryData targetWishListEntryData = new TargetWishListEntryData();
        targetWishListEntryData.setBaseProductCode(wishListEntry.getBaseProductCode());
        targetWishListEntryData.setSelectedVariantCode(wishListEntry.getSelectedVariantCode());
        targetWishListEntryData.setTimeStamp(wishListEntry.getTimestamp());
        return targetWishListEntryData;
    }

    /**
     * 
     * @param emailId
     * @param listName
     * @param type
     * @return targetWishListModel
     */
    public static TargetWishListModel retrieveWishList(final String emailId, final String listName,
            final WishListTypeEnum type) {
        final UserModel userModel = ServiceLookup.getUserService().getUserForUID(emailId);
        return ServiceLookup.getTargetWishListService().retrieveWishList(
                listName, userModel, type);
    }

    /**
     * @param userModel
     */
    public static void removeFavouritesForCustomer(final UserModel userModel) {
        final TargetWishListModel targetWishlistModel = ServiceLookup.getTargetWishListService().retrieveWishList(
                StringUtils.EMPTY, userModel, WishListTypeEnum.FAVOURITE);
        if (targetWishlistModel != null) {
            ServiceLookup.getModelService().remove(targetWishlistModel);
        }
    }


}

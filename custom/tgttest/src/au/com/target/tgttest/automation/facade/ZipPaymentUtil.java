package au.com.target.tgttest.automation.facade;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderPaymentInfoData;
import au.com.target.tgtfacades.order.data.placeorder.TargetPlaceOrderZipPaymentInfoData;
import au.com.target.tgtpaymentprovider.constants.TgtpaymentproviderConstants;
import au.com.target.tgtpaymentprovider.zippay.data.response.CheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateChargeResponse;
import au.com.target.tgtpaymentprovider.zippay.data.types.Item;
import au.com.target.tgtpaymentprovider.zippay.data.types.Order;
import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgttinker.mock.payment.TargetZippayClientMock;


public class ZipPaymentUtil {

    public static final String REQUEST_PARAM_ZIPPAY_STATUS_APPROVED = "approved";

    private static final String STATIC_CHECKOUT_ID = "x3EcH2xETPgGTNT2PeKe-p68w9VtDFySAHBfen2bn";

    /**
     * added to prevent acidential initialization
     */
    private ZipPaymentUtil() {
        throw new IllegalAccessError("ZipPayment Utility class");
    }

    public static CartModel getCheckoutCart() {

        return ServiceLookup.getTargetCheckoutFacade().getCart();
    }

    public static TargetZippayClientMock getMockClient() {
        return ServiceLookup.getTargetZippayClientMock();
    }

    public static String getCheckoutId(final boolean random) {
        if (random) {
            return UUID.randomUUID().toString();
        }
        return STATIC_CHECKOUT_ID;
    }

    public static void createRetrieveCheckoutMock() {
        final CheckoutResponse response = new CheckoutResponse();
        final Order orderData = new au.com.target.tgtpaymentprovider.zippay.data.types.Order();
        orderData.setAmount(getCheckoutCart().getTotalPrice().doubleValue());
        orderData.setCurrency(TgtCoreConstants.AUSTRALIAN_DOLLARS);
        orderData.setItems(createItems(getCheckoutCart().getEntries()));
        response.setOrder(orderData);
        response.setState(REQUEST_PARAM_ZIPPAY_STATUS_APPROVED);
        getMockClient().setRetrieveCheckoutResponse(response);
    }

    public static void createCaptureRequestMock() {

        if (getMockClient().getCaptureRequestResponse() != null) {
            return;
        }

        final CreateChargeResponse response = new CreateChargeResponse();
        response.setId("121243434");
        response.setReceiptNumber("444324234");
        response.setState("captured");
        getMockClient().setCaptureRequestResponse(response);
    }

    public static List<Item> createItems(final List<AbstractOrderEntryModel> entries) {
        final List<Item> items = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(entries)) {
            for (final AbstractOrderEntryModel entry : entries) {
                final Item item = new Item();
                item.setType(TgtpaymentproviderConstants.SKU);
                item.setName(entry.getProduct().getName());
                item.setReference(entry.getProduct().getCode());
                item.setQuantity(entry.getQuantity().longValue());
                item.setAmount(entry.getBasePrice().doubleValue());
                items.add(item);
            }
        }
        return items;
    }

    public static TargetPlaceOrderPaymentInfoData getMockPaymentInfo() {
        final TargetPlaceOrderZipPaymentInfoData paymentInfoData = new TargetPlaceOrderZipPaymentInfoData();
        paymentInfoData.setCheckoutId(getCheckoutId(true));
        paymentInfoData.setResult(REQUEST_PARAM_ZIPPAY_STATUS_APPROVED);
        return paymentInfoData;
    }

    /**
     * set zippay client mock to reject the capture payment
     */
    public static void rejectCapturePayment() {
        final CreateChargeResponse response = new CreateChargeResponse();
        response.setId("121243434");
        response.setReceiptNumber("444324234");
        response.setState("rejected");
        getMockClient().setCaptureRequestResponse(response);
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import org.springframework.util.StringUtils;


/**
 * @author mjanarth
 *
 */
public class AccertifyCardInfo {

    private String paymentType;
    private String cardNumber;
    private String cardHolderName;
    private String cardType;
    private String cardAuthorizedAmount;
    private String cardExpiryDate;
    private String cardTxnID;
    private String cardToken;
    private String cardReceipt;
    private String paypalAmount;
    private String paypalStatus;
    private boolean isPaymentSuccess;
    private String zipOrderID;
    private String zipAmount;
    private String zipStatus;
    private String zipRefundId;


    /**
     * @return the paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType
     *            the paymentType to set
     */
    public void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber
     *            the cardNumber to set
     */
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardHolderName
     */
    public String getCardHolderName() {
        return cardHolderName;
    }

    /**
     * @param cardHolderName
     *            the cardHolderName to set
     */
    public void setCardHolderName(final String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cardAuthorizedAmount
     */
    public String getCardAuthorizedAmount() {
        return cardAuthorizedAmount;
    }

    /**
     * @param cardAuthorizedAmount
     *            the cardAuthorizedAmount to set
     */
    public void setCardAuthorizedAmount(final String cardAuthorizedAmount) {
        this.cardAuthorizedAmount = cardAuthorizedAmount;
    }

    /**
     * @return the cardExpiryDate
     */
    public String getCardExpiryDate() {
        if (StringUtils.isEmpty(cardExpiryDate.trim())) {
            return null;
        }
        else {
            return cardExpiryDate;
        }
    }

    /**
     * @param cardExpiryDate
     *            the cardExpiryDate to set
     */
    public void setCardExpiryDate(final String cardExpiryDate) {
        if (!StringUtils.isEmpty(cardExpiryDate.trim())) {
            this.cardExpiryDate = cardExpiryDate;
        }
    }

    /**
     * @return the cardTxnID
     */
    public String getCardTxnID() {
        return cardTxnID;
    }

    /**
     * @param cardTxnID
     *            the cardTxnID to set
     */
    public void setCardTxnID(final String cardTxnID) {
        this.cardTxnID = cardTxnID;
    }

    /**
     * @return the cardToken
     */
    public String getCardToken() {
        if (StringUtils.isEmpty(cardToken)) {
            return null;
        }
        else {
            return cardToken;
        }
    }

    /**
     * @param cardToken
     *            the cardToken to set
     */
    public void setCardToken(final String cardToken) {
        this.cardToken = cardToken;
    }

    /**
     * @return the cardReceipt
     */
    public String getCardReceipt() {
        return cardReceipt;
    }

    /**
     * @param cardReceipt
     *            the cardReceipt to set
     */
    public void setCardReceipt(final String cardReceipt) {
        this.cardReceipt = cardReceipt;
    }

    /**
     * @return the isPaymentSuccess
     */
    public boolean isPaymentSuccess() {
        return isPaymentSuccess;
    }

    /**
     * @param paymentSuccess
     *            the isPaymentSuccess to set
     */
    public void setPaymentSuccess(final boolean paymentSuccess) {
        this.isPaymentSuccess = paymentSuccess;
    }



    /**
     * @return the paypalAmount
     */
    public String getPaypalAmount() {
        return paypalAmount;
    }

    /**
     * @param paypalAmount
     *            the paypalAmount to set
     */
    public void setPaypalAmount(final String paypalAmount) {
        this.paypalAmount = paypalAmount;
    }

    /**
     * @return the paypalStatus
     */
    public String getPaypalStatus() {
        return paypalStatus;
    }

    /**
     * @param paypalStatus
     *            the paypalStatus to set
     */
    public void setPaypalStatus(final String paypalStatus) {
        this.paypalStatus = paypalStatus;
    }

    /**
     * @return the zipOrderID
     */
    public String getZipOrderID() {
        return zipOrderID;
    }

    /**
     * @param zipOrderID
     *            the zipOrderID to set
     */
    public void setZipOrderID(final String zipOrderID) {
        this.zipOrderID = zipOrderID;
    }

    /**
     * @return the zipAmount
     */
    public String getZipAmount() {
        return zipAmount;
    }

    /**
     * @param zipAmount
     *            the zipAmount to set
     */
    public void setZipAmount(final String zipAmount) {
        this.zipAmount = zipAmount;
    }

    /**
     * @return the zipStatus
     */
    public String getZipStatus() {
        return zipStatus;
    }

    /**
     * @param zipStatus
     *            the zipStatus to set
     */
    public void setZipStatus(final String zipStatus) {
        this.zipStatus = zipStatus;
    }

    /**
     * @return the zipRefundId
     */
    public String getZipRefundId() {
        return zipRefundId;
    }

    /**
     * @param zipRefundId the zipRefundId to set
     */
    public void setZipRefundId(String zipRefundId) {
        this.zipRefundId = zipRefundId;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author cwijesu1
 * 
 */
public class AccertifyTransactionInfo {
    private final String targetStaffDiscountCard;
    private final double totalAmount;
    private final double promotionAmount;

    public AccertifyTransactionInfo(final String targetStaffDiscountCard, final int totalAmount,
            final int promotionAmount) {

        this.targetStaffDiscountCard = targetStaffDiscountCard;
        this.totalAmount = totalAmount;
        this.promotionAmount = promotionAmount;
    }

    /**
     * @return the targetStaffDiscountCard
     */
    public String getTargetStaffDiscountCard() {
        return targetStaffDiscountCard;
    }

    /**
     * @return the totalAmount
     */
    public double getTotalAmount() {
        return totalAmount;
    }

    /**
     * @return the promotionAmount
     */
    public double getPromotionAmount() {
        return promotionAmount;
    }

}

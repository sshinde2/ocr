/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;



/**
 * @author mjanarth
 * 
 */
public class AddressEntry {
    private String firstName;
    private String lastName;
    private String addressLine1;
    private String addressLine2;
    private String town;
    private String postalCode;
    private String phone;
    private boolean shippingAddress;
    private boolean billingAddress;
    private String country;
    private boolean defaultAddress;
    private String shippingMethod;
    private String region;
    private String shippingClass;

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }


    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @param town
     *            the town to set
     */
    public void setTown(final String town) {
        this.town = town;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode
     *            the postalCode to set
     */
    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * @return the shippingAddress
     */
    public boolean isShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress
     *            the shippingAddress to set
     */
    public void setShippingAddress(final boolean shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * @return the billingAddress
     */
    public boolean isBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress
     *            the billingAddress to set
     */
    public void setBillingAddress(final boolean billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the defaultAddress
     */
    public boolean isDefaultAddress() {
        return defaultAddress;
    }

    /**
     * @param defaultAddress
     *            the defaultAddress to set
     */
    public void setDefaultAddress(final boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    /**
     * @return the addressLine1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * @param addressLine1
     *            the addressLine1 to set
     */
    public void setAddressLine1(final String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     * @return the addressLine2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * @param addressLine2
     *            the addressLine2 to set
     */
    public void setAddressLine2(final String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country
     *            the country to set
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    /**
     * @return the shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * @param shippingMethod
     *            the shippingMethod to set
     */
    public void setShippingMethod(final String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region
     *            the region to set
     */
    public void setRegion(final String region) {
        this.region = region;
    }

    /**
     * @return the shippingClass
     */
    public String getShippingClass() {
        return shippingClass;
    }

    /**
     * @param shippingClass
     *            the shippingClass to set
     */
    public void setShippingClass(final String shippingClass) {
        this.shippingClass = shippingClass;
    }
}

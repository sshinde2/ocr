/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bhuang3
 *
 */
public class AfterpayConfigurationData {

    private String type;

    private String description;

    private String max;

    private String min;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the max
     */
    public String getMax() {
        return max;
    }

    /**
     * @param max
     *            the max to set
     */
    public void setMax(final String max) {
        this.max = max;
    }

    /**
     * @return the min
     */
    public String getMin() {
        return min;
    }

    /**
     * @param min
     *            the min to set
     */
    public void setMin(final String min) {
        this.min = min;
    }


}

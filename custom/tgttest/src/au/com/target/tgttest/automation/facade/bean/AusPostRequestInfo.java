/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represents info in the label/manifest requests sent to webmethods for AusPost
 * 
 * @author sbryan6
 *
 */
public class AusPostRequestInfo {

    private String store;
    private String merchantLocationId;
    private String storeAddress;
    private String layout;
    private boolean branding;
    private String trackingId;
    private String deliveryName;
    private String deliveryAddress;


    /**
     * @return the store
     */
    public String getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final String store) {
        this.store = store;
    }

    /**
     * @return the storeAddress
     */
    public String getStoreAddress() {
        return storeAddress;
    }

    /**
     * @param storeAddress
     *            the storeAddress to set
     */
    public void setStoreAddress(final String storeAddress) {
        this.storeAddress = storeAddress;
    }

    /**
     * @return the layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * @param layout
     *            the layout to set
     */
    public void setLayout(final String layout) {
        this.layout = layout;
    }

    /**
     * @return the branding
     */
    public boolean getBranding() {
        return branding;
    }

    /**
     * @param branding
     *            the branding to set
     */
    public void setBranding(final boolean branding) {
        this.branding = branding;
    }

    /**
     * @return the trackingId
     */
    public String getTrackingId() {
        return trackingId;
    }

    /**
     * @param trackingId
     *            the trackingId to set
     */
    public void setTrackingId(final String trackingId) {
        this.trackingId = trackingId;
    }

    /**
     * @return the deliveryName
     */
    public String getDeliveryName() {
        return deliveryName;
    }

    /**
     * @param deliveryName
     *            the deliveryName to set
     */
    public void setDeliveryName(final String deliveryName) {
        this.deliveryName = deliveryName;
    }

    /**
     * @return the deliveryAddress
     */
    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * @param deliveryAddress
     *            the deliveryAddress to set
     */
    public void setDeliveryAddress(final String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the merchantLocationId
     */
    public String getMerchantLocationId() {
        return merchantLocationId;
    }

    /**
     * @param merchantLocationId
     *            the merchantLocationId to set
     */
    public void setMerchantLocationId(final String merchantLocationId) {
        this.merchantLocationId = merchantLocationId;
    }



}

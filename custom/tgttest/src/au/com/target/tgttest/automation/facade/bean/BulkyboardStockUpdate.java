/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represent a bulky board stock update from WebMethods
 * 
 */
public class BulkyboardStockUpdate {

    private final int store;
    private final String product;
    private final int soh;
    private final String message;
    private final boolean success;

    /**
     * @param store
     * @param product
     * @param soh
     * @param message
     */
    public BulkyboardStockUpdate(final int store, final String product, final int soh, final String message,
            final boolean success) {
        super();
        this.store = store;
        this.product = product;
        this.soh = soh;
        this.message = message;
        this.success = success;
    }

    public int getStore() {
        return store;
    }

    public String getProduct() {
        return product;
    }

    public int getSoh() {
        return soh;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }
}

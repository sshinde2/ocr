/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.math.BigDecimal;


/**
 * Represent a cart entry
 * 
 */
public class CartEntry {
    private String product;
    private int qty;
    private Double price;
    private Double weight;
    private String productHeading;
    private String size;
    private String colour;
    private String image;
    private Double totalPrice;
    private Double totalItemPrice;
    private Double discount;
    private boolean dealsApplied;
    private String baseProductCode;
    private String baseProductName;
    private String brand;
    private String topLevelCategory;
    private boolean assorted;
    private BigDecimal preOrderDepositAmount;
    private String returnReason;
    private BigDecimal preOrderOutstandingAmount;

    /**
     * This is used implicitly in givenCartEntries parameter
     * 
     * @param product
     * @param qty
     * @param price
     */
    public CartEntry(final String product, final int qty, final Double price) {
        this.product = product;
        this.qty = qty;
        this.price = price;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final String product) {
        this.product = product;
    }

    /**
     * @return the qty
     */
    public int getQty() {
        return qty;
    }

    /**
     * @param qty
     *            the qty to set
     */
    public void setQty(final int qty) {
        this.qty = qty;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final Double price) {
        this.price = price;
    }

    /**
     * @return the weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * @param weight
     *            the weight to set
     */
    public void setWeight(final Double weight) {
        this.weight = weight;
    }

    /**
     * @return the productHeading
     */
    public String getProductHeading() {
        return productHeading;
    }

    /**
     * @param productHeading
     *            the productHeading to set
     */
    public void setProductHeading(final String productHeading) {
        this.productHeading = productHeading;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * @param colour
     *            the colour to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image
     *            the image to set
     */
    public void setImage(final String image) {
        this.image = image;
    }

    /**
     * @return the totalPrice
     */
    public Double getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     *            the totalPrice to set
     */
    public void setTotalPrice(final Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * @return the totalItemPrice
     */
    public Double getTotalItemPrice() {
        return totalItemPrice;
    }

    /**
     * @param totalItemPrice
     *            the totalItemPrice to set
     */
    public void setTotalItemPrice(final Double totalItemPrice) {
        this.totalItemPrice = totalItemPrice;
    }

    /**
     * @return the discount
     */
    public Double getDiscount() {
        return discount;
    }

    /**
     * @param discount
     *            the discount to set
     */
    public void setDiscount(final Double discount) {
        this.discount = discount;
    }

    /**
     * @return the dealsApplied
     */
    public boolean isDealsApplied() {
        return dealsApplied;
    }

    /**
     * @param dealsApplied
     *            the dealsApplied to set
     */
    public void setDealsApplied(final boolean dealsApplied) {
        this.dealsApplied = dealsApplied;
    }

    /**
     * @return the baseProductCode
     */
    public String getBaseProductCode() {
        return baseProductCode;
    }

    /**
     * @param baseProductCode
     *            the baseProductCode to set
     */
    public void setBaseProductCode(final String baseProductCode) {
        this.baseProductCode = baseProductCode;
    }

    /**
     * @return the baseProductName
     */
    public String getBaseProductName() {
        return baseProductName;
    }

    /**
     * @param baseProductName
     *            the baseProductName to set
     */
    public void setBaseProductName(final String baseProductName) {
        this.baseProductName = baseProductName;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }

    /**
     * @return the topLevelCategory
     */
    public String getTopLevelCategory() {
        return topLevelCategory;
    }

    /**
     * @param topLevelCategory
     *            the topLevelCategory to set
     */
    public void setTopLevelCategory(final String topLevelCategory) {
        this.topLevelCategory = topLevelCategory;
    }

    /**
     * @return the assorted
     */
    public boolean isAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final boolean assorted) {
        this.assorted = assorted;
    }

    /**
     *
     * @return the preOrderDepositAmount
     */
    public BigDecimal getPreOrderDepositAmount() {
        return preOrderDepositAmount;
    }

    /**
     * @param preOrderDepositAmount
     *            the preOrderDepositAmount to set
     */
    public void setPreOrderDepositAmount(final BigDecimal preOrderDepositAmount) {
        this.preOrderDepositAmount = preOrderDepositAmount;
    }

    /**
     * @return the returnReason
     */
    public String getReturnReason() {
        return returnReason;
    }

    /**
     * @param returnReason
     *            the returnReason to set
     */
    public void setReturnReason(final String returnReason) {
        this.returnReason = returnReason;
    }

    /**
     * @return the preOrderOutstandingAmount
     */
    public BigDecimal getPreOrderOutstandingAmount() {
        return preOrderOutstandingAmount;
    }

    /**
     * @param preOrderOutstandingAmount
     *            the preOrderOutstandingAmount to set
     */
    public void setPreOrderOutstandingAmount(final BigDecimal preOrderOutstandingAmount) {
        this.preOrderOutstandingAmount = preOrderOutstandingAmount;
    }

}

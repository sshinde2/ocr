/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author sbryan6
 *
 */
public class ConsignmentEntryToWebmethodsInfo {

    private String productId;
    private String denomination;
    private String brandId;
    private String giftCardStyle;
    private String name;
    private String giftCardProductId;


    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     *            the productId to set
     */
    public void setProductId(final String productId) {
        this.productId = productId;
    }

    /**
     * @return the denomination
     */
    public String getDenomination() {
        return denomination;
    }

    /**
     * @param denomination
     *            the denomination to set
     */
    public void setDenomination(final String denomination) {
        this.denomination = denomination;
    }

    /**
     * @return the brandId
     */
    public String getBrandId() {
        return brandId;
    }

    /**
     * @param brandId
     *            the brandId to set
     */
    public void setBrandId(final String brandId) {
        this.brandId = brandId;
    }

    /**
     * @return the giftCardStyle
     */
    public String getGiftCardStyle() {
        return giftCardStyle;
    }

    /**
     * @param giftCardStyle
     *            the giftCardStyle to set
     */
    public void setGiftCardStyle(final String giftCardStyle) {
        this.giftCardStyle = giftCardStyle;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the giftCardProductId
     */
    public String getGiftCardProductId() {
        return giftCardProductId;
    }

    /**
     * @param giftCardProductId
     *            the giftCardProductId to set
     */
    public void setGiftCardProductId(final String giftCardProductId) {
        this.giftCardProductId = giftCardProductId;
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 *
 */
public class ConsignmentToFastline {
    private String itemCode;
    private Long quantityOrdered;
    private Double itemAmount;
    private Double orderGoodsAmount;
    private Double itemWeight;
    private String orderPurpose;
    private Double deliveryFee;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the quantityOrdered
     */
    public Long getQuantityOrdered() {
        return quantityOrdered;
    }

    /**
     * @param quantityOrdered
     *            the quantityOrdered to set
     */
    public void setQuantityOrdered(final Long quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    /**
     * @return the itemAmount
     */
    public Double getItemAmount() {
        return itemAmount;
    }

    /**
     * @param itemAmount
     *            the itemAmount to set
     */
    public void setItemAmount(final Double itemAmount) {
        this.itemAmount = itemAmount;
    }

    /**
     * @return the orderGoodsAmount
     */
    public Double getOrderGoodsAmount() {
        return orderGoodsAmount;
    }

    /**
     * @param orderGoodsAmount
     *            the orderGoodsAmount to set
     */
    public void setOrderGoodsAmount(final Double orderGoodsAmount) {
        this.orderGoodsAmount = orderGoodsAmount;
    }

    /**
     * @return the itemWeight
     */
    public Double getItemWeight() {
        return itemWeight;
    }

    /**
     * @param itemWeight
     *            the itemWeight to set
     */
    public void setItemWeight(final Double itemWeight) {
        this.itemWeight = itemWeight;
    }

    /**
     * @return the orderPurpose
     */
    public String getOrderPurpose() {
        return orderPurpose;
    }

    /**
     * @param orderPurpose
     *            the orderPurpose to set
     */
    public void setOrderPurpose(final String orderPurpose) {
        this.orderPurpose = orderPurpose;
    }

    /**
     * @return the deliveryFee
     */
    public Double getDeliveryFee() {
        return deliveryFee;
    }

    /**
     * @param deliveryFee
     *            the deliveryFee to set
     */
    public void setDeliveryFee(final Double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

}

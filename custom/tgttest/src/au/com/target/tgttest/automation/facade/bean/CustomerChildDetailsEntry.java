/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;



/**
 * @author mjanarth
 *
 */
public class CustomerChildDetailsEntry {

    private String childName;

    private String childBirthday;

    private String childGender;

    /**
     * @return the childName
     */
    public String getChildName() {
        return childName;
    }

    /**
     * @param childName
     *            the childName to set
     */
    public void setChildName(final String childName) {
        this.childName = childName;
    }



    /**
     * @return the childGender
     */
    public String getChildGender() {
        return childGender;
    }

    /**
     * @param childGender
     *            the childGender to set
     */
    public void setChildGender(final String childGender) {
        this.childGender = childGender;
    }

    /**
     * @return the childBirthday
     */
    public String getChildBirthday() {
        return childBirthday;
    }

    /**
     * @param childBirthday
     *            the childBirthday to set
     */
    public void setChildBirthday(final String childBirthday) {
        this.childBirthday = childBirthday;
    }




}

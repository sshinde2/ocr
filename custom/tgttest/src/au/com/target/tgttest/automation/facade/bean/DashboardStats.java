/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;



/**
 * @author Vivek
 *
 */
public class DashboardStats {

    private final String dashboardStatus;
    private final String comment;
    private final int total;

    /**
     * @param dashboardStatus
     * @param total
     */
    public DashboardStats(final String dashboardStatus, final String comment, final int total) {
        this.dashboardStatus = dashboardStatus;
        this.comment = comment;
        this.total = total;
    }

    /**
     * @return the dashboardStatus
     */
    public String getDashboardStatus() {
        return dashboardStatus;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @return the total
     */
    public int getTotal() {
        return total;
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represent a deal break point to set up
 * 
 */
public class DealBreakPointEntry {

    private final int qty;
    private final double unitPrice;

    /**
     * @param qty
     * @param unitPrice
     */
    public DealBreakPointEntry(final int qty, final double unitPrice) {
        super();
        this.qty = qty;
        this.unitPrice = unitPrice;
    }

    public int getQty() {
        return qty;
    }

    public double getUnitPrice() {
        return unitPrice;
    }


}

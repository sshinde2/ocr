/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author sbryan6
 * 
 */
public class DealParameters {

    private final String name;
    private final String rewardType;
    private final Double rewardValue;
    private final Integer rewardMaxQty;

    /**
     * @param name
     * @param rewardType
     * @param rewardValue
     * @param rewardMaxQty
     */
    public DealParameters(final String name, final String rewardType, final Double rewardValue,
            final Integer rewardMaxQty) {
        super();
        this.name = name;
        this.rewardType = rewardType;
        this.rewardValue = rewardValue;
        this.rewardMaxQty = rewardMaxQty;
    }

    public String getName() {
        return name;
    }

    public String getRewardType() {
        return rewardType;
    }

    public Double getRewardValue() {
        return rewardValue;
    }

    public Integer getRewardMaxQty() {
        return rewardMaxQty;
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.util.ArrayList;
import java.util.List;


/**
 * @author sbryan6
 * 
 */
public class DealReferenceData {

    public static final String DEAL_NAME_BUYGET = "buyget";
    private static List<DealReferenceData> dealData = new ArrayList<>();

    private String dealName;
    private String dealCode;
    private String qualifierCat;
    private String rewardCat;

    static {
        dealData.add(new DealReferenceData("buyget", "9110", "dealQCat110", "dealRCat110"));
        dealData.add(new DealReferenceData("buygetsamelist", "9101", "dealQCat101", "dealQCat101"));
        dealData.add(new DealReferenceData("valuebundle", "9106", "dealQCat106", "dealQCat106"));
    }

    /**
     * Instantiates a new deal reference data.
     * 
     * @param dealName
     *            the deal name
     * @param dealCode
     *            the deal code
     * @param qualifierCat
     *            the qualifier cat
     * @param rewardCat
     *            the reward cat
     */
    public DealReferenceData(final String dealName, final String dealCode, final String qualifierCat,
            final String rewardCat) {
        super();
        this.dealName = dealName;
        this.dealCode = dealCode;
        this.qualifierCat = qualifierCat;
        this.rewardCat = rewardCat;
    }

    public static DealReferenceData getDealData(final String name) {

        for (final DealReferenceData deal : dealData) {
            if (name.equalsIgnoreCase(deal.getDealName())) {
                return deal;
            }
        }
        return null;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(final String dealName) {
        this.dealName = dealName;
    }

    public String getDealCode() {
        return dealCode;
    }

    public void setDealCode(final String dealCode) {
        this.dealCode = dealCode;
    }

    public String getQualifierCat() {
        return qualifierCat;
    }

    public void setQualifierCat(final String qualifierCat) {
        this.qualifierCat = qualifierCat;
    }

    public String getRewardCat() {
        return rewardCat;
    }

    public void setRewardCat(final String rewardCat) {
        this.rewardCat = rewardCat;
    }


}

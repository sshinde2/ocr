/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represent delivery mode availability
 * 
 */
public class DeliveryModeAvailability {

    private final String name;
    private final boolean available;
    private String shortDescription;
    private String longDescription;
    private String code;

    /**
     * @param name
     * @param available
     */
    public DeliveryModeAvailability(final String name, final boolean available) {
        super();
        this.name = name;
        this.available = available;
    }

    public String getName() {
        return name;
    }

    public boolean isAvailable() {
        return available;
    }

    /**
     * @return the shortDescription
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * @param shortDescription
     *            the shortDescription to set
     */
    public void setShortDescription(final String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the longDescription
     */
    public String getLongDescription() {
        return longDescription;
    }

    /**
     * @param longDescription
     *            the longDescription to set
     */
    public void setLongDescription(final String longDescription) {
        this.longDescription = longDescription;
    }

}

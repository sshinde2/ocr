/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author siddharam
 *
 */
public class DeliveryModeRestrictionData {

    private String postCode;
    private String catchmentArea;
    private String productType;
    private String qty;
    private String deliveryFee;
    private String lowestDeliveryFee;

    /**
     * @return the lowestDeliveryFee
     */
    public String getLowestDeliveryFee() {
        return lowestDeliveryFee;
    }

    /**
     * @param lowestDeliveryFee
     *            the lowestDeliveryFee to set
     */
    public void setLowestDeliveryFee(final String lowestDeliveryFee) {
        this.lowestDeliveryFee = lowestDeliveryFee;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode
     *            the postCode to set
     */
    public void setPostCode(final String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the catchmentArea
     */
    public String getCatchmentArea() {
        return catchmentArea;
    }

    /**
     * @param catchmentArea
     *            the catchmentArea to set
     */
    public void setCatchmentArea(final String catchmentArea) {
        this.catchmentArea = catchmentArea;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType
     *            the productType to set
     */
    public void setProductType(final String productType) {
        this.productType = productType;
    }

    /**
     * @return the qty
     */
    public String getQty() {
        return qty;
    }

    /**
     * @param qty
     *            the qty to set
     */
    public void setQty(final String qty) {
        this.qty = qty;
    }

    /**
     * @return the deliveryFee
     */
    public String getDeliveryFee() {
        return deliveryFee;
    }

    /**
     * @param deliveryFee
     *            the deliveryFee to set
     */
    public void setDeliveryFee(final String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }
}

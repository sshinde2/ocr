/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author smishra1
 *
 */
public class DropShipConsignmentEntryToWMInfo {
    private String productId;
    private String ean;
    private String name;

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     *            the productId to set
     */
    public void setProductId(final String productId) {
        this.productId = productId;
    }

    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }

    /**
     * @param ean
     *            the ean to set
     */
    public void setEan(final String ean) {
        this.ean = ean;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author hsing179
 *
 */
public class FailZipDetails {
    private String failedRequestId;
    private String failedAmount;
    private String failedStatus;
    private int totalFailedZIPAttempts;

    /**
     * @return the failedRequestId
     */
    public String getFailedRequestId() {
        return failedRequestId;
    }

    /**
     * @param failedRequestId
     *            the failedRequestId to set
     */
    public void setFailedRequestId(final String failedRequestId) {
        this.failedRequestId = failedRequestId;
    }

    /**
     * @return the failedAmount
     */
    public String getFailedAmount() {
        return failedAmount;
    }

    /**
     * @param failedAmount
     *            the failedAmount to set
     */
    public void setFailedAmount(final String failedAmount) {
        this.failedAmount = failedAmount;
    }

    /**
     * @return the failedStatus
     */
    public String getFailedStatus() {
        return failedStatus;
    }

    /**
     * @param failedStatus
     *            the failedStatus to set
     */
    public void setFailedStatus(final String failedStatus) {
        this.failedStatus = failedStatus;
    }

    /**
     * @return the totalFailedZIPAttempts
     */
    public int getTotalFailedZIPAttempts() {
        return totalFailedZIPAttempts;
    }

    /**
     * @param totalFailedZIPAttempts
     *            the totalFailedZIPAttempts to set
     */
    public void setTotalFailedZIPAttempts(final int totalFailedZIPAttempts) {
        this.totalFailedZIPAttempts = totalFailedZIPAttempts;
    }

}

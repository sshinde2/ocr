/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bhuang3
 *
 */
public class FluentUpdateStatusData {

    private boolean lastRunSuccessful;

    private String fluentId;

    private String code;

    private String errorCode;

    /**
     * @return the lastRunSuccessful
     */
    public boolean isLastRunSuccessful() {
        return lastRunSuccessful;
    }

    /**
     * @param lastRunSuccessful
     *            the lastRunSuccessful to set
     */
    public void setLastRunSuccessful(final boolean lastRunSuccessful) {
        this.lastRunSuccessful = lastRunSuccessful;
    }

    /**
     * @return the fluentId
     */
    public String getFluentId() {
        return fluentId;
    }

    /**
     * @param fluentId
     *            the fluentId to set
     */
    public void setFluentId(final String fluentId) {
        this.fluentId = fluentId;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * @param errorCode
     *            the errorCode to set
     */
    public void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }


}

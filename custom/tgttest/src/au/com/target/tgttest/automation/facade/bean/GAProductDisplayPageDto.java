/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author smishra1
 *
 */
public class GAProductDisplayPageDto {
    private String baseCode;
    private String baseName;
    private String departmentCategory;
    private String brand;
    private String price;
    private String code;

    /**
     * @return the baseCode
     */
    public String getBaseCode() {
        return baseCode;
    }

    /**
     * @param baseCode
     *            the baseCode to set
     */
    public void setBaseCode(final String baseCode) {
        this.baseCode = baseCode;
    }

    /**
     * @return the baseName
     */
    public String getBaseName() {
        return baseName;
    }

    /**
     * @param baseName
     *            the baseName to set
     */
    public void setBaseName(final String baseName) {
        this.baseName = baseName;
    }

    /**
     * @return the departmentCategory
     */
    public String getDepartmentCategory() {
        return departmentCategory;
    }

    /**
     * @param departmentCategory
     *            the departmentCategory to set
     */
    public void setDepartmentCategory(final String departmentCategory) {
        this.departmentCategory = departmentCategory;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final String price) {
        this.price = price;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author htan3
 *
 */
public class GeolocationData {

    private String locationText;

    private double latitude;

    private double longitude;

    private String locality;

    private String postcode;

    private String state;

    /**
     * @return the locationText
     */
    public String getLocationText() {
        return locationText;
    }

    /**
     * @param locationText
     *            the locationText to set
     */
    public void setLocationText(final String locationText) {
        this.locationText = locationText;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude
     *            the latitude to set
     */
    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the locality
     */
    public String getLocality() {
        return locality;
    }

    /**
     * @param locality
     *            the locality to set
     */
    public void setLocality(final String locality) {
        this.locality = locality;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }


}

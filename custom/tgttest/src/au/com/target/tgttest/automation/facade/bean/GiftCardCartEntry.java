/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rsamuel3
 *
 */
public class GiftCardCartEntry {
    private String productCode;
    private String firstName;
    private String lastName;
    private String email;
    private String messageText;
    private String dealName;
    private Character dealRole;
    private int minQty;
    private Double price;


    /**
     * @return the dealName
     */
    public String getDealName() {
        return dealName;
    }

    /**
     * @return the dealRole
     */
    public Character getDealRole() {
        return dealRole;
    }

    /**
     * @param dealRole
     *            the dealRole to set
     */
    public void setDealRole(final Character dealRole) {
        this.dealRole = dealRole;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final Double price) {
        this.price = price;
    }

    /**
     * @param dealName
     *            the dealName to set
     */
    public void setDealName(final String dealName) {
        this.dealName = dealName;
    }



    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the messageText
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * @param messageText
     *            the messageText to set
     */
    public void setMessageText(final String messageText) {
        this.messageText = messageText;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the minQty
     */
    public int getMinQty() {
        return minQty;
    }

    /**
     * @param minQty
     *            the minQty to set
     */
    public void setMinQty(final int minQty) {
        this.minQty = minQty;
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author sbryan6
 *
 */
public class GiftCardProductDetails {

    private String productCode;
    private boolean giftCardFlag;
    private String deliveryModes;
    private String cardValue;
    private String cardDesign;
    private Integer quantity;
    private Double discountPrice;
    private Double basePrice;
    private Double totalPrice;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the giftCardFlag
     */
    public boolean isGiftCardFlag() {
        return giftCardFlag;
    }

    /**
     * @param giftCardFlag
     *            the giftCardFlag to set
     */
    public void setGiftCardFlag(final boolean giftCardFlag) {
        this.giftCardFlag = giftCardFlag;
    }

    /**
     * @return the deliveryModes
     */
    public String getDeliveryModes() {
        return deliveryModes;
    }

    /**
     * @param deliveryModes
     *            the deliveryModes to set
     */
    public void setDeliveryModes(final String deliveryModes) {
        this.deliveryModes = deliveryModes;
    }

    /**
     * @return the cardValue
     */
    public String getCardValue() {
        return cardValue;
    }

    /**
     * @param cardValue
     *            the cardValue to set
     */
    public void setCardValue(final String cardValue) {
        this.cardValue = cardValue;
    }

    /**
     * @return the cardDesign
     */
    public String getCardDesign() {
        return cardDesign;
    }

    /**
     * @param cardDesign
     *            the cardDesign to set
     */
    public void setCardDesign(final String cardDesign) {
        this.cardDesign = cardDesign;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the discountPrice
     */
    public Double getDiscountPrice() {
        return discountPrice;
    }

    /**
     * @param discountPrice
     *            the discountPrice to set
     */
    public void setDiscountPrice(final Double discountPrice) {
        this.discountPrice = discountPrice;
    }

    /**
     * @return the basePrice
     */
    public Double getBasePrice() {
        return basePrice;
    }

    /**
     * @param basePrice
     *            the basePrice to set
     */
    public void setBasePrice(final Double basePrice) {
        this.basePrice = basePrice;
    }

    /**
     * @return the totalPrice
     */
    public Double getTotalPrice() {
        return totalPrice;
    }

    /**
     * @param totalPrice
     *            the totalPrice to set
     */
    public void setTotalPrice(final Double totalPrice) {
        this.totalPrice = totalPrice;
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author smishra1
 *
 */
public class HardCopyData {
    private String consignmentId;
    private String chargeZone;
    private String chargeZoneDescription;
    private String chargeCode;
    private String deliveryName;
    private String deliveryPostcode;
    private String deliverySuburb;
    private String articles;
    private String weight;

    /**
     * @return the consignmentId
     */
    public String getConsignmentId() {
        return consignmentId;
    }

    /**
     * @param consignmentId
     *            the consignmentId to set
     */
    public void setConsignmentId(final String consignmentId) {
        this.consignmentId = consignmentId;
    }

    /**
     * @return the chargeZone
     */
    public String getChargeZone() {
        return chargeZone;
    }

    /**
     * @param chargeZone
     *            the chargeZone to set
     */
    public void setChargeZone(final String chargeZone) {
        this.chargeZone = chargeZone;
    }

    /**
     * @return the chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * @param chargeCode
     *            the chargeCode to set
     */
    public void setChargeCode(final String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * @return the deliveryName
     */
    public String getDeliveryName() {
        return deliveryName;
    }

    /**
     * @param deliveryName
     *            the deliveryName to set
     */
    public void setDeliveryName(final String deliveryName) {
        this.deliveryName = deliveryName;
    }

    /**
     * @return the deliveryPostcode
     */
    public String getDeliveryPostcode() {
        return deliveryPostcode;
    }

    /**
     * @param deliveryPostcode
     *            the deliveryPostcode to set
     */
    public void setDeliveryPostcode(final String deliveryPostcode) {
        this.deliveryPostcode = deliveryPostcode;
    }

    /**
     * @return the deliverySuburb
     */
    public String getDeliverySuburb() {
        return deliverySuburb;
    }

    /**
     * @param deliverySuburb
     *            the deliverySuburb to set
     */
    public void setDeliverySuburb(final String deliverySuburb) {
        this.deliverySuburb = deliverySuburb;
    }

    /**
     * @return the articles
     */
    public String getArticles() {
        return articles;
    }

    /**
     * @param articles
     *            the articles to set
     */
    public void setArticles(final String articles) {
        this.articles = articles;
    }

    /**
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight
     *            the weight to set
     */
    public void setWeight(final String weight) {
        this.weight = weight;
    }

    /**
     * @return the chargeZoneDescription
     */
    public String getChargeZoneDescription() {
        return chargeZoneDescription;
    }

    /**
     * @param chargeZoneDescription
     *            the chargeZoneDescription to set
     */
    public void setChargeZoneDescription(final String chargeZoneDescription) {
        this.chargeZoneDescription = chargeZoneDescription;
    }


}

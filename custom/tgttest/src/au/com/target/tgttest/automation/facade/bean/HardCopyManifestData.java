/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author smishra1
 *
 */
public class HardCopyManifestData {
    private String mlid;
    private String storeAddress;
    private String manifestDate;
    private String manifestId;
    private String totalArticles;
    private String totalWeight;

    /**
     * @return the mlid
     */
    public String getMlid() {
        return mlid;
    }

    /**
     * @param mlid
     *            the mlid to set
     */
    public void setMlid(final String mlid) {
        this.mlid = mlid;
    }

    /**
     * @return the storeAddress
     */
    public String getStoreAddress() {
        return storeAddress;
    }

    /**
     * @param storeAddress
     *            the storeAddress to set
     */
    public void setStoreAddress(final String storeAddress) {
        this.storeAddress = storeAddress;
    }

    /**
     * @return the manifestDate
     */
    public String getManifestDate() {
        return manifestDate;
    }

    /**
     * @param manifestDate
     *            the manifestDate to set
     */
    public void setManifestDate(final String manifestDate) {
        this.manifestDate = manifestDate;
    }

    /**
     * @return the manifestId
     */
    public String getManifestId() {
        return manifestId;
    }

    /**
     * @param manifestId
     *            the manifestId to set
     */
    public void setManifestId(final String manifestId) {
        this.manifestId = manifestId;
    }

    /**
     * @return the totalArticles
     */
    public String getTotalArticles() {
        return totalArticles;
    }

    /**
     * @param totalArticles
     *            the totalArticles to set
     */
    public void setTotalArticles(final String totalArticles) {
        this.totalArticles = totalArticles;
    }

    /**
     * @return the totalWeight
     */
    public String getTotalWeight() {
        return totalWeight;
    }

    /**
     * @param totalWeight
     *            the totalWeight to set
     */
    public void setTotalWeight(final String totalWeight) {
        this.totalWeight = totalWeight;
    }
}

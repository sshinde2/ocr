/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author ajit
 *
 */
public class ImportGiftCard {

    private final String brandID;
    private final String productID;
    private final String denomination;
    private final String giftCardStyle;

    /**
     * @param brandID
     * @param productID
     * @param denomination
     * @param giftCardStyle
     */
    public ImportGiftCard(final String brandID, final String productID, final String denomination,
            final String giftCardStyle) {
        super();
        this.brandID = brandID;
        this.productID = productID;
        this.denomination = denomination;
        this.giftCardStyle = giftCardStyle;
    }

    /**
     * @return the brandID
     */
    public String getBrandID() {
        return brandID;
    }

    /**
     * @return the productID
     */
    public String getProductID() {
        return productID;
    }

    /**
     * @return the denomination
     */
    public String getDenomination() {
        return denomination;
    }

    /**
     * @return the giftCardStyle
     */
    public String getGiftCardStyle() {
        return giftCardStyle;
    }



}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author Rahul
 *
 */
public class ManifestForStore {

    private String manifestID;
    private String manifestDate;
    private String manifestTime;
    private boolean transmitted = false;
    private String webmethodsResponse;

    /**
     * @return the manifestID
     */
    public String getManifestID() {
        return manifestID;
    }

    /**
     * @param manifestID
     *            the manifestID to set
     */
    public void setManifestID(final String manifestID) {
        this.manifestID = manifestID;
    }

    /**
     * @return the manifestDate
     */
    public String getManifestDate() {
        return manifestDate;
    }

    /**
     * @param manifestDate
     *            the manifestDate to set
     */
    public void setManifestDate(final String manifestDate) {
        this.manifestDate = manifestDate;
    }

    /**
     * @return the manifestTime
     */
    public String getManifestTime() {
        return manifestTime;
    }

    /**
     * @param manifestTime
     *            the manifestTime to set
     */
    public void setManifestTime(final String manifestTime) {
        this.manifestTime = manifestTime;
    }

    /**
     * @return the transmitted
     */
    public boolean isTransmitted() {
        return transmitted;
    }

    /**
     * @param transmitted
     *            the transmitted to set
     */
    public void setTransmitted(final boolean transmitted) {
        this.transmitted = transmitted;
    }

    /**
     * @return the webmethodsResponse
     */
    public String getWebmethodsResponse() {
        return webmethodsResponse;
    }

    /**
     * @param webmethodsResponse
     *            the webmethodsResponse to set
     */
    public void setWebmethodsResponse(final String webmethodsResponse) {
        this.webmethodsResponse = webmethodsResponse;
    }
}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;


/**
 * @author pthoma20
 * 
 */
public class MobileDealParameters {

    public static final String FUTURE = "future";

    public static final String PAST = "past";


    private String dealId;

    private Boolean availableForMobile;

    private Boolean enabled;

    private String longTitle;

    private String startDate;

    private String offerHeading;

    private Boolean featured;

    /**
     * @return the dealId
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * @param dealId
     *            the dealId to set
     */
    public void setDealId(final String dealId) {
        this.dealId = dealId;
    }

    /**
     * @return the availableForMobile
     */
    public Boolean getAvailableForMobile() {
        return availableForMobile;
    }

    /**
     * @param availableForMobile
     *            the availableForMobile to set
     */
    public void setAvailableForMobile(final Boolean availableForMobile) {
        this.availableForMobile = availableForMobile;
    }

    /**
     * @return the enabled
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled
     *            the enabled to set
     */
    public void setEnabled(final Boolean enabled) {
        this.enabled = enabled;
    }


    /**
     * @return the longTitle
     */
    public String getLongTitle() {
        return longTitle;
    }

    /**
     * @param longTitle
     *            the longTitle to set
     */
    public void setLongTitle(final String longTitle) {
        this.longTitle = longTitle;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(final String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the offerHeading
     */
    public String getOfferHeading() {
        return offerHeading;
    }

    /**
     * @return the offerHeading
     */
    public List<String> getOfferHeadingsAsList() {
        if (StringUtils.isNotBlank(offerHeading)) {
            return Arrays.asList(offerHeading.split("\\s*,\\s*"));
        }
        return null;
    }

    /**
     * @return the featured
     */
    public Boolean getFeatured() {
        return featured;
    }

    /**
     * @param featured
     *            the featured to set
     */
    public void setFeatured(final Boolean featured) {
        this.featured = featured;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import org.apache.commons.lang.StringUtils;



/**
 * @author pthoma20
 * 
 */
public class MobileOfferHeadingsParameter {

    private String code;

    private String colour;

    private String name;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        return StringUtils.remove(colour, "'");
    }

    /**
     * @param colour
     *            the colour to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;


/**
 * @author pthoma20
 * 
 */
public class MobileVoucherParameters {

    public static final String FUTURE = "future";

    public static final String PAST = "past";


    private String voucherId;

    private Boolean availableForMobile;

    private Boolean enabledOnline;

    private Boolean enabledInStore;

    private String pageTitle;

    private Long barcode;


    private String longTitle;

    private String startDate;

    private String voucherCode;

    private String offerHeading;



    /**
     * @return the offerHeading
     */
    public String getOfferHeading() {
        return offerHeading;
    }

    /**
     * @return the offerHeading
     */
    public List<String> getOfferHeadingsAsList() {
        if (StringUtils.isNotBlank(offerHeading)) {
            return Arrays.asList(offerHeading.split("\\s*,\\s*"));
        }
        return null;
    }

    /**
     * @return the availableForMobile
     */
    public Boolean getAvailableForMobile() {
        return availableForMobile;
    }

    /**
     * @return the enabledInStore
     */
    public Boolean getEnabledInStore() {
        return enabledInStore;
    }


    /**
     * @return the voucherId
     */
    public String getVoucherId() {
        return voucherId;
    }

    /**
     * @return the enabledOnline
     */
    public Boolean getEnabledOnline() {
        return enabledOnline;
    }

    /**
     * @return the pageTitle
     */
    public String getPageTitle() {
        return pageTitle;
    }

    /**
     * @return the longTitle
     */
    public String getLongTitle() {
        return longTitle;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @return the voucherCode
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * @return the barcode
     */
    public Long getBarcode() {
        return barcode;
    }

    /**
     * @param barcode
     *            the barcode to set
     */
    public void setBarcode(final Long barcode) {
        this.barcode = barcode;
    }

    /**
     * @param voucherId
     *            the voucherId to set
     */
    public void setVoucherId(final String voucherId) {
        this.voucherId = voucherId;
    }

    /**
     * @param availableForMobile
     *            the availableForMobile to set
     */
    public void setAvailableForMobile(final Boolean availableForMobile) {
        this.availableForMobile = availableForMobile;
    }

    /**
     * @param enabledOnline
     *            the enabledOnline to set
     */
    public void setEnabledOnline(final Boolean enabledOnline) {
        this.enabledOnline = enabledOnline;
    }

    /**
     * @param enabledInStore
     *            the enabledInStore to set
     */
    public void setEnabledInStore(final Boolean enabledInStore) {
        this.enabledInStore = enabledInStore;
    }

    /**
     * @param pageTitle
     *            the pageTitle to set
     */
    public void setPageTitle(final String pageTitle) {
        this.pageTitle = pageTitle;
    }


    /**
     * @param longTitle
     *            the longTitle to set
     */
    public void setLongTitle(final String longTitle) {
        this.longTitle = longTitle;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(final String startDate) {
        this.startDate = startDate;
    }

    /**
     * @param voucherCode
     *            the voucherCode to set
     */
    public void setVoucherCode(final String voucherCode) {
        this.voucherCode = voucherCode;
    }

    /**
     * @param offerHeading
     *            the offerHeading to set
     */
    public void setOfferHeading(final String offerHeading) {
        this.offerHeading = offerHeading;
    }
}

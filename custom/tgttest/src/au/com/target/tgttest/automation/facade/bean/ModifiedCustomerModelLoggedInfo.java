/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bpottass
 *
 */
public class ModifiedCustomerModelLoggedInfo {

    private String changedBy;
    private String changedAttribute;
    private String oldValue;
    private String newValue;

    /**
     * @return the changedBy
     */
    public String getChangedBy() {
        return changedBy;
    }

    /**
     * @param changedBy
     *            the changedBy to set
     */
    public void setChangedBy(final String changedBy) {
        this.changedBy = changedBy;
    }

    /**
     * @return the changedAttribute
     */
    public String getChangedAttribute() {
        return changedAttribute;
    }

    /**
     * @param changedAttribute
     *            the changedAttribute to set
     */
    public void setChangedAttribute(final String changedAttribute) {
        this.changedAttribute = changedAttribute;
    }

    /**
     * @return the oldValue
     */
    public String getOldValue() {
        return oldValue;
    }

    /**
     * @param oldValue
     *            the oldValue to set
     */
    public void setOldValue(final String oldValue) {
        this.oldValue = oldValue;
    }

    /**
     * @return the newValue
     */
    public String getNewValue() {
        return newValue;
    }

    /**
     * @param newValue
     *            the newValue to set
     */
    public void setNewValue(final String newValue) {
        this.newValue = newValue;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


/**
 * @author rmcalave
 *
 */
public class OrderEntry {
    private final String product;
    private final int qty;

    public OrderEntry(final String product, final int quantity) {
        this.product = product;
        this.qty = quantity;
    }

    /**
     * @return the productCode
     */
    public String getProduct() {
        return product;
    }

    /**
     * @return the quantity
     */
    public int getQty() {
        return qty;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof OrderEntry)) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        final OrderEntry rhs = (OrderEntry)obj;
        return new EqualsBuilder().append(product, rhs.getProduct()).append(qty, rhs.getQty()).isEquals();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(product).append(qty).toHashCode();
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represent expected order fields
 * 
 */
public class OrderFields {

    private final String total;
    private final String subtotal;
    private final String deliveryCost;
    private final String totalTax;
    private final String totalDiscounts;
    private final Integer pointsConsumed;
    private final Boolean containsDigitalEntriesOnly;

    /**
     * @param total
     * @param subtotal
     * @param deliveryCost
     * @param totalTax
     * @param totalDiscounts
     */
    public OrderFields(final String total, final String subtotal, final String deliveryCost, final String totalTax,
            final String totalDiscounts, final Integer pointsConsumed, final Boolean containsDigitalEntriesOnly) {
        super();
        this.total = total;
        this.subtotal = subtotal;
        this.deliveryCost = deliveryCost;
        this.totalTax = totalTax;
        this.totalDiscounts = totalDiscounts;
        this.pointsConsumed = pointsConsumed;
        this.containsDigitalEntriesOnly = containsDigitalEntriesOnly;
    }

    public String getTotal() {
        return total;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public String getDeliveryCost() {
        return deliveryCost;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public String getTotalDiscounts() {
        return totalDiscounts;
    }

    /**
     * @return the pointsConsumed
     */
    public Integer getPointsConsumed() {
        return pointsConsumed;
    }

    /**
     * @return the containsDigitalEntriesOnly
     */
    public Boolean getContainsDigitalEntriesOnly() {
        return containsDigitalEntriesOnly;
    }


}

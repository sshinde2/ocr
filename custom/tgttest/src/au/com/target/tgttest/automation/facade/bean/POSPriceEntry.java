/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;



/**
 * @author mjanarth
 * 
 */
public class POSPriceEntry {

    private String date;
    private boolean promoEvent;
    private Double permPrice;
    private double sellPrice;

    public POSPriceEntry(final String date, final boolean promoEvent, final double permPrice, final double sellPrice) {
        this.date = date;
        this.promoEvent = promoEvent;
        this.permPrice = Double.valueOf(permPrice);
        this.sellPrice = sellPrice;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(final String date) {
        this.date = date;
    }

    /**
     * @return the promoEvent
     */
    public boolean isPromoEvent() {
        return promoEvent;
    }

    /**
     * @param promoEvent
     *            the promoEvent to set
     */
    public void setPromoEvent(final boolean promoEvent) {
        this.promoEvent = promoEvent;
    }

    /**
     * @return the permPrice
     */
    public Double getPermPrice() {
        return permPrice;
    }

    /**
     * @param permPrice
     *            the permPrice to set
     */
    public void setPermPrice(final Double permPrice) {
        this.permPrice = permPrice;
    }

    /**
     * @return the sellPrice
     */
    public double getSellPrice() {
        return sellPrice;
    }

    /**
     * @param sellPrice
     *            the sellPrice to set
     */
    public void setSellPrice(final double sellPrice) {
        this.sellPrice = sellPrice;
    }


}

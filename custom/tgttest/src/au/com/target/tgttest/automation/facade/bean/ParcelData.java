/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author smishra1
 *
 */
public class ParcelData {
    private String consignmentCode;
    private Double height;
    private Double width;
    private Double length;
    private Double weight;

    /**
     * @return the consignmentCode
     */
    public String getConsignmentCode() {
        return consignmentCode;
    }

    /**
     * @param consignmentCode
     *            the consignmentCode to set
     */
    public void setConsignmentCode(final String consignmentCode) {
        this.consignmentCode = consignmentCode;
    }

    /**
     * @return the height
     */
    public Double getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(final Double height) {
        this.height = height;
    }

    /**
     * @return the width
     */
    public Double getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(final Double width) {
        this.width = width;
    }

    /**
     * @return the length
     */
    public Double getLength() {
        return length;
    }

    /**
     * @param length
     *            the length to set
     */
    public void setLength(final Double length) {
        this.length = length;
    }

    /**
     * @return the weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * @param weight
     *            the weight to set
     */
    public void setWeight(final Double weight) {
        this.weight = weight;
    }


}

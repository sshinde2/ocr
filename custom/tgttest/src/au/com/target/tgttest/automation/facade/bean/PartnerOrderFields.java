package au.com.target.tgttest.automation.facade.bean;

/**
 * @author Nandini
 *
 */
public class PartnerOrderFields extends OrderFields {

    private final String salesChannel;
    private final String paymentType;
    private final String currency;
    private final String country;
    private final String createdDate;
    private final String fulfillmentType;
    private final String storeNumber;
    private final String deliveryAddress;

    /**
     * @param total
     * @param subtotal
     * @param deliveryCost
     * @param totalTax
     * @param totalDiscounts
     * @param salesChannel
     * @param paymentType
     * @param currency
     * @param country
     * @param createdDate
     * @param fulfillmentType
     * @param storeNumber
     * @param deliveryAddress
     */
    public PartnerOrderFields(final String total, final String subtotal, final String deliveryCost,
            final String totalTax,
            final String totalDiscounts, final String salesChannel, final String paymentType, final String currency,
            final String country,
            final String createdDate, final String fulfillmentType, final String storeNumber,
            final String deliveryAddress) {
        super(total, subtotal, deliveryCost, totalTax, totalDiscounts, null, null);
        this.salesChannel = salesChannel;
        this.paymentType = paymentType;
        this.currency = currency;
        this.country = country;
        this.createdDate = createdDate;
        this.fulfillmentType = fulfillmentType;
        this.storeNumber = storeNumber;
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the salesChannel
     */
    public String getSalesChannel() {
        return salesChannel;
    }

    /**
     * @return the paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @return the createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * @return the fulfillmentType
     */
    public String getFulfillmentType() {
        return fulfillmentType;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @return the deliveryAddress
     */
    public String getDeliveryAddress() {
        return deliveryAddress;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author sbryan6
 * 
 */
public class PosRequest {

    private final int store;
    private final String state;
    private final String ean;

    /**
     * @param store
     * @param state
     * @param ean
     */
    public PosRequest(final int store, final String state, final String ean) {
        super();
        this.store = store;
        this.state = state;
        this.ean = ean;
    }

    public int getStore() {
        return store;
    }

    public String getState() {
        return state;
    }

    public String getEan() {
        return ean;
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rsamuel3
 *
 */
public class PostcodeData {
    private String postcode;

    private String chargeZone;

    private String state;

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the chargeZone
     */
    public String getChargeZone() {
        return chargeZone;
    }

    /**
     * @param chargeZone
     *            the chargeZone to set
     */
    public void setChargeZone(final String chargeZone) {
        this.chargeZone = chargeZone;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }
}

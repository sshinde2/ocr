/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author mjanarth
 * 
 */
public class PrevPermanentPriceEntry {
    private String variantCode;
    private String startDate;
    private String endDate;
    private double price;


    public PrevPermanentPriceEntry(final String variantCode, final String startDate, final String endDate,
            final double price) {
        this.variantCode = variantCode;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;

    }

    /**
     * @return the variantCode
     */
    public String getVariantCode() {
        return variantCode;
    }

    /**
     * @param variantCode
     *            the variantCode to set
     */
    public void setVariantCode(final String variantCode) {
        this.variantCode = variantCode;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(final String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(final String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final double price) {
        this.price = price;
    }

}

package au.com.target.tgttest.automation.facade.bean;

/**
 * @author mjanarth
 * 
 */
public class PriceData {

    private String productCode;
    private double price;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final double price) {
        this.price = price;
    }

}
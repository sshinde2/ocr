/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author ragarwa3
 *
 */
public class ProductDimensionsData {

    private String productId;
    private Double weight;

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     *            the productId to set
     */
    public void setProductId(final String productId) {
        this.productId = productId;
    }

    /**
     * @return the weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * @param weight
     *            the weight to set
     */
    public void setWeight(final Double weight) {
        this.weight = weight;
    }

}

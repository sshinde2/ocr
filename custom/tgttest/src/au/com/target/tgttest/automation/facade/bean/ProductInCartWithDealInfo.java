/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author sbryan6
 * 
 */
public class ProductInCartWithDealInfo extends CartEntry {

    private String dealName;

    private Character dealRole;

    private boolean deal;

    /**
     * @param product
     * @param qty
     * @param price
     * @param dealName
     * @param dealRole
     */
    public ProductInCartWithDealInfo(final String product, final int qty, final double price, final String dealName,
            final Character dealRole) {
        super(product, qty, Double.valueOf(price));
        this.dealName = dealName;
        this.dealRole = dealRole;
    }

    public ProductInCartWithDealInfo(final String product, final int qty, final double price, final boolean deal) {
        super(product, qty, Double.valueOf(price));
        this.deal = deal;
    }

    public String getDealName() {
        return dealName;
    }

    public Character getDealRole() {
        return dealRole;
    }

    /**
     * @return the isDeal
     */
    public boolean isDeal() {
        return deal;
    }

    /**
     * @param dealName
     *            the dealName to set
     */
    public void setDealName(final String dealName) {
        this.dealName = dealName;
    }

    /**
     * @param dealRole
     *            the dealRole to set
     */
    public void setDealRole(final Character dealRole) {
        this.dealRole = dealRole;
    }

    /**
     * @param deal
     *            the isDeal to set
     */
    public void setDeal(final boolean deal) {
        this.deal = deal;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author rmcalave
 *
 */
public class ProductPartialData {
    private String productCode;

    private String dealDescription;

    private String couldFireMessage;

    private boolean clearance;

    private String stockLevel;

    private String stockStatus;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the dealDescription
     */
    public String getDealDescription() {
        return dealDescription;
    }

    /**
     * @param dealDescription
     *            the dealDescription to set
     */
    public void setDealDescription(final String dealDescription) {
        this.dealDescription = dealDescription;
    }

    /**
     * @return the couldFireMessage
     */
    public String getCouldFireMessage() {
        return couldFireMessage;
    }

    /**
     * @param couldFireMessage
     *            the couldFireMessage to set
     */
    public void setCouldFireMessage(final String couldFireMessage) {
        this.couldFireMessage = couldFireMessage;
    }

    /**
     * @return the clearance
     */
    public boolean isClearance() {
        return clearance;
    }

    /**
     * @param clearance
     *            the clearance to set
     */
    public void setClearance(final boolean clearance) {
        this.clearance = clearance;
    }

    /**
     * @return the stockLevel
     */
    public String getStockLevel() {
        return stockLevel;
    }

    /**
     * @param stockLevel
     *            the stockLevel to set
     */
    public void setStockLevel(final String stockLevel) {
        this.stockLevel = stockLevel;
    }

    /**
     * @return the stockStatus
     */
    public String getStockStatus() {
        return stockStatus;
    }

    /**
     * @param stockStatus
     *            the stockStatus to set
     */
    public void setStockStatus(final String stockStatus) {
        this.stockStatus = stockStatus;
    }

}

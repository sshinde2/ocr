/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bhuang3
 * 
 */
public class ProductPriceData {

    private Double wasPrice;
    private Double nowPrice;
    private Double fromPrice;
    private Double toPrice;
    private Double fromWasPrice;
    private Double toWasPrice;

    /**
     * @return the wasPrice
     */
    public Double getWasPrice() {
        return wasPrice;
    }

    /**
     * @param wasPrice
     *            the wasPrice to set
     */
    public void setWasPrice(final Double wasPrice) {
        this.wasPrice = wasPrice;
    }

    /**
     * @return the nowPrice
     */
    public Double getNowPrice() {
        return nowPrice;
    }

    /**
     * @param nowPrice
     *            the nowPrice to set
     */
    public void setNowPrice(final Double nowPrice) {
        this.nowPrice = nowPrice;
    }

    /**
     * @return the fromPrice
     */
    public Double getFromPrice() {
        return fromPrice;
    }

    /**
     * @param fromPrice
     *            the fromPrice to set
     */
    public void setFromPrice(final Double fromPrice) {
        this.fromPrice = fromPrice;
    }

    /**
     * @return the toPrice
     */
    public Double getToPrice() {
        return toPrice;
    }

    /**
     * @param toPrice
     *            the toPrice to set
     */
    public void setToPrice(final Double toPrice) {
        this.toPrice = toPrice;
    }

    /**
     * @return the fromWasPrice
     */
    public Double getFromWasPrice() {
        return fromWasPrice;
    }

    /**
     * @param fromWasPrice
     *            the fromWasPrice to set
     */
    public void setFromWasPrice(final Double fromWasPrice) {
        this.fromWasPrice = fromWasPrice;
    }

    /**
     * @return the toWasPrice
     */
    public Double getToWasPrice() {
        return toWasPrice;
    }

    /**
     * @param toWasPrice
     *            the toWasPrice to set
     */
    public void setToWasPrice(final Double toWasPrice) {
        this.toWasPrice = toWasPrice;
    }





}

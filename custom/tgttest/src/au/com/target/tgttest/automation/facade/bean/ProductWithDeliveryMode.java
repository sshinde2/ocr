/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bhuang3
 * 
 */
public class ProductWithDeliveryMode {

    private final String product;
    private final String deliveryMode;

    public ProductWithDeliveryMode(final String product, final String deliveryMode) {
        this.product = product;
        this.deliveryMode = deliveryMode;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }


    public String getProduct() {
        return product;
    }


}

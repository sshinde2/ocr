/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author Pradeep
 *
 */
public class ProductsStockDetails {

    private String sellableVariants;

    private String stockAvailability;

    /**
     * @return the sellableVariants
     */
    public String getSellableVariants() {
        return sellableVariants;
    }

    /**
     * @return the stockAvailability
     */
    public String getStockAvailability() {
        return stockAvailability;
    }

    /**
     * @param sellableVariants
     *            the sellableVariants to set
     */
    public void setSellableVariants(final String sellableVariants) {
        this.sellableVariants = sellableVariants;
    }

    /**
     * @param stockAvailability
     *            the stockAvailability to set
     */
    public void setStockAvailability(final String stockAvailability) {
        this.stockAvailability = stockAvailability;
    }




}

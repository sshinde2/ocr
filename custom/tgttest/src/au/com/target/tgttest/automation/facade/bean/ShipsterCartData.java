/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author bhuang3
 *
 */
public class ShipsterCartData {

    private String originalDeliveryCost;

    private Boolean ausPostDeliveryClubFreeDelivery;

    private Boolean ausPostClubMember;

    /**
     * @return the originalDeliveryCost
     */
    public String getOriginalDeliveryCost() {
        return originalDeliveryCost;
    }

    /**
     * @param originalDeliveryCost
     *            the originalDeliveryCost to set
     */
    public void setOriginalDeliveryCost(final String originalDeliveryCost) {
        this.originalDeliveryCost = originalDeliveryCost;
    }

    /**
     * @return the ausPostDeliveryClubFreeDelivery
     */
    public Boolean getAusPostDeliveryClubFreeDelivery() {
        return ausPostDeliveryClubFreeDelivery;
    }

    /**
     * @param ausPostDeliveryClubFreeDelivery
     *            the ausPostDeliveryClubFreeDelivery to set
     */
    public void setAusPostDeliveryClubFreeDelivery(final Boolean ausPostDeliveryClubFreeDelivery) {
        this.ausPostDeliveryClubFreeDelivery = ausPostDeliveryClubFreeDelivery;
    }

    /**
     * @return the ausPostClubMember
     */
    public Boolean getAusPostClubMember() {
        return ausPostClubMember;
    }

    /**
     * @param ausPostClubMember
     *            the ausPostClubMember to set
     */
    public void setAusPostClubMember(final Boolean ausPostClubMember) {
        this.ausPostClubMember = ausPostClubMember;
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author pthoma20
 *
 */
public class ShopTheLookCollectionsData {

    private String colletionId;

    private String collectionName;

    private int countOfLooks;

    /**
     * @return the colletionId
     */
    public String getColletionId() {
        return colletionId;
    }

    /**
     * @param colletionId
     *            the colletionId to set
     */
    public void setColletionId(final String colletionId) {
        this.colletionId = colletionId;
    }

    /**
     * @return the collectionName
     */
    public String getCollectionName() {
        return collectionName;
    }

    /**
     * @param collectionName
     *            the collectionName to set
     */
    public void setCollectionName(final String collectionName) {
        this.collectionName = collectionName;
    }

    /**
     * @return the countOfLooks
     */
    public int getCountOfLooks() {
        return countOfLooks;
    }

    /**
     * @param countOfLooks
     *            the countOfLooks to set
     */
    public void setCountOfLooks(final int countOfLooks) {
        this.countOfLooks = countOfLooks;
    }


}
/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author pthoma20
 *
 */
public class ShopTheLookData {

    private String shopTheLookId;

    private String title;

    private int countOfLooks;

    /**
     * @return the shopTheLookId
     */
    public String getShopTheLookId() {
        return shopTheLookId;
    }

    /**
     * @param shopTheLookId
     *            the shopTheLookId to set
     */
    public void setShopTheLookId(final String shopTheLookId) {
        this.shopTheLookId = shopTheLookId;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * @return the countOfLooks
     */
    public int getCountOfLooks() {
        return countOfLooks;
    }

    /**
     * @param countOfLooks
     *            the countOfLooks to set
     */
    public void setCountOfLooks(final int countOfLooks) {
        this.countOfLooks = countOfLooks;
    }



}
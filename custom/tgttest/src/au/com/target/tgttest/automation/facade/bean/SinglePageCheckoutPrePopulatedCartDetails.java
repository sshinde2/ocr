/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author ajit
 *
 */
public class SinglePageCheckoutPrePopulatedCartDetails {

    private final String creditCard;
    private final String deliveryMode;
    private final String deliveryDetails;
    private final String cNCDetails;

    /**
     * @param creditCard
     * @param deliveryMode
     * @param deliveryDetails
     * @param cNCDetails
     */
    public SinglePageCheckoutPrePopulatedCartDetails(final String creditCard, final String deliveryMode,
            final String deliveryDetails,
            final String cNCDetails) {
        super();
        this.creditCard = creditCard;
        this.deliveryMode = deliveryMode;
        this.deliveryDetails = deliveryDetails;
        this.cNCDetails = cNCDetails;
    }

    /**
     * @return the creditCard
     */
    public String getCreditCard() {
        return creditCard;
    }

    /**
     * @return the deliveryMode
     */
    public String getDeliveryMode() {
        return deliveryMode;
    }

    /**
     * @return the deliveryDetails
     */
    public String getDeliveryDetails() {
        return deliveryDetails;
    }

    /**
     * @return the cNCDetails
     */
    public String getCNCDetails() {
        return cNCDetails;
    }

}

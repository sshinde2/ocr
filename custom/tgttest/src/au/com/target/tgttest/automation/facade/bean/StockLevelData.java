/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Transfer object for setting FastLine Stock
 * 
 * @author jjayawa1
 *
 */
public class StockLevelData {
    private String product;
    private String reserved;
    private String available;
    private String warehouseCode;
    private String responseStatus;
    private String maxPreOrder;
    private String preOrder;

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final String product) {
        this.product = product;
    }

    /**
     * @return the reserved
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * @param reserved
     *            the reserved to set
     */
    public void setReserved(final String reserved) {
        this.reserved = reserved;
    }

    /**
     * @return the available
     */
    public String getAvailable() {
        return available;
    }

    /**
     * @param available
     *            the available to set
     */
    public void setAvailable(final String available) {
        this.available = available;
    }

    /**
     * @return the warehouseCode
     */
    public String getWarehouseCode() {
        return warehouseCode;
    }

    /**
     * @param warehouseCode
     *            the warehouseCode to set
     */
    public void setWarehouseCode(final String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    /**
     * @return the responseStatus
     */
    public String getResponseStatus() {
        return responseStatus;
    }

    /**
     * @param responseStatus
     *            the responseStatus to set
     */
    public void setResponseStatus(final String responseStatus) {
        this.responseStatus = responseStatus;
    }

    /**
     * @return the preOrder
     */
    public String getPreOrder() {
        return preOrder;
    }

    /**
     * @param preOrder
     *            the preOrder to set
     */
    public void setPreOrder(final String preOrder) {
        this.preOrder = preOrder;
    }

    /**
     * @return the maxPreOrder
     */
    public String getMaxPreOrder() {
        return maxPreOrder;
    }

    /**
     * @param maxPreOrder
     *            the maxPreOrder to set
     */
    public void setMaxPreOrder(final String maxPreOrder) {
        this.maxPreOrder = maxPreOrder;
    }
}

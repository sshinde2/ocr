/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author sbryan6
 *
 */
public class StoreFulfilmentCapabilitiesInfo {

    private String store;
    private String storeNumber;
    private String instoreEnabled;
    private String inStock;
    private String state;
    private String allowDeliveryToSameStore;
    private String allowDeliveryToAnotherStore;
    private String deliveryModesAllowed;

    private String merchantLocationId;

    private String chargeCode;
    private Integer chargeAccountNumber;
    private String username;
    private Integer productCode;
    private Integer serviceCode;
    private boolean clearProductExclusions;
    private boolean clearCategoryExclusions;

    /**
     * @return the store
     */
    public String getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final String store) {
        this.store = store;
    }

    /**
     * @return the instoreEnabled
     */
    public String getInstoreEnabled() {
        return instoreEnabled;
    }

    /**
     * @param instoreEnabled
     *            the instoreEnabled to set
     */
    public void setInstoreEnabled(final String instoreEnabled) {
        this.instoreEnabled = instoreEnabled;
    }

    /**
     * @return the inStock
     */
    public String getInStock() {
        return inStock;
    }

    /**
     * @param inStock
     *            the inStock to set
     */
    public void setInStock(final String inStock) {
        this.inStock = inStock;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the allowDeliveryToSameStore
     */
    public String getAllowDeliveryToSameStore() {
        return allowDeliveryToSameStore;
    }

    /**
     * @param allowDeliveryToSameStore
     *            the allowDeliveryToSameStore to set
     */
    public void setAllowDeliveryToSameStore(final String allowDeliveryToSameStore) {
        this.allowDeliveryToSameStore = allowDeliveryToSameStore;
    }

    /**
     * @return the allowDeliveryToAnotherStore
     */
    public String getAllowDeliveryToAnotherStore() {
        return allowDeliveryToAnotherStore;
    }

    /**
     * @param allowDeliveryToAnotherStore
     *            the allowDeliveryToAnotherStore to set
     */
    public void setAllowDeliveryToAnotherStore(final String allowDeliveryToAnotherStore) {
        this.allowDeliveryToAnotherStore = allowDeliveryToAnotherStore;
    }

    /**
     * @return the deliveryModesAllowed
     */
    public String getDeliveryModesAllowed() {
        return deliveryModesAllowed;
    }

    /**
     * @param deliveryModesAllowed
     *            the deliveryModesAllowed to set
     */
    public void setDeliveryModesAllowed(final String deliveryModesAllowed) {
        this.deliveryModesAllowed = deliveryModesAllowed;
    }

    /**
     * @return the merchantLocationId
     */
    public String getMerchantLocationId() {
        return merchantLocationId;
    }

    /**
     * @param merchantLocationId
     *            the merchantLocationId to set
     */
    public void setMerchantLocationId(final String merchantLocationId) {
        this.merchantLocationId = merchantLocationId;
    }

    /**
     * @return the chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * @return the chargeAccountNumber
     */
    public Integer getChargeAccountNumber() {
        return chargeAccountNumber;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @return the productCode
     */
    public Integer getProductCode() {
        return productCode;
    }

    /**
     * @return the serviceCode
     */
    public Integer getServiceCode() {
        return serviceCode;
    }

    /**
     * @param chargeCode
     *            the chargeCode to set
     */
    public void setChargeCode(final String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * @param chargeAccountNumber
     *            the chargeAccountNumber to set
     */
    public void setChargeAccountNumber(final Integer chargeAccountNumber) {
        this.chargeAccountNumber = chargeAccountNumber;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final Integer productCode) {
        this.productCode = productCode;
    }

    /**
     * @param serviceCode
     *            the serviceCode to set
     */
    public void setServiceCode(final Integer serviceCode) {
        this.serviceCode = serviceCode;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the clearProductExclusions
     */
    public boolean isClearProductExclusions() {
        return clearProductExclusions;
    }

    /**
     * @param clearProductExclusions
     *            the clearProductExclusions to set
     */
    public void setClearProductExclusions(final boolean clearProductExclusions) {
        this.clearProductExclusions = clearProductExclusions;
    }

    /**
     * @return the clearCategoryExclusions
     */
    public boolean isClearCategoryExclusions() {
        return clearCategoryExclusions;
    }

    /**
     * @param clearCategoryExclusions
     *            the clearCategoryExclusions to set
     */
    public void setClearCategoryExclusions(final boolean clearCategoryExclusions) {
        this.clearCategoryExclusions = clearCategoryExclusions;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * @author pthoma20
 *
 */
public class StoresData {

    private String state;

    private Integer storeNumber;

    private String storeName;

    private String targetOpeningHours;

    private String formattedAddress;

    private String phone;

    private double latitude;

    private double longitude;

    private String timeZone;

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the storeNumber
     */
    public Integer getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName
     *            the storeName to set
     */
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the targetOpeningHours
     */
    public String getTargetOpeningHours() {
        return targetOpeningHours;
    }

    /**
     * @param targetOpeningHours
     *            the targetOpeningHours to set
     */
    public void setTargetOpeningHours(final String targetOpeningHours) {
        this.targetOpeningHours = targetOpeningHours;
    }

    /**
     * @return the formattedAddress
     */
    public String getFormattedAddress() {
        return formattedAddress;
    }

    /**
     * @param formattedAddress
     *            the formattedAddress to set
     */
    public void setFormattedAddress(final String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude
     *            the latitude to set
     */
    public void setLatitude(final double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude
     *            the longitude to set
     */
    public void setLongitude(final double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the timeZone
     */
    public String getTimeZone() {
        return timeZone;
    }

    /**
     * @param timeZone
     *            the timeZone to set
     */
    public void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone
     *            the phone to set
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }
}

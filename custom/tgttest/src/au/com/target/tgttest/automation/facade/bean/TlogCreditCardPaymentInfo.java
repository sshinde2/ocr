/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.math.BigDecimal;


/**
 * Represents credit card payment info in tlog
 * 
 */
public class TlogCreditCardPaymentInfo {

    private final String cardNumber;
    private final String rrn;
    private final boolean approved;
    private final BigDecimal amount;
    private final String type;


    /**
     * @param card
     * @param rrn
     * @param approved
     */
    public TlogCreditCardPaymentInfo(final String card, final String rrn, final boolean approved,
            final BigDecimal amount, final String type) {
        super();
        this.cardNumber = card;
        this.rrn = rrn;
        this.approved = approved;
        this.amount = amount;
        this.type = type;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getRrn() {
        return rrn;
    }

    public boolean isApproved() {
        return approved;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

/**
 * Represent expected TransDiscount element in tlog
 * 
 */
public class TlogTransDiscount {

    private final String type;
    private final String code;
    private final int pct;
    private final int amount;
    private final String style;
    private final int points;
    private final String redeemCode;
    private final String confirmationCode;

    public TlogTransDiscount(final String type, final String code, final int pct, final int amount, final String style,
            final int points, final String redeemCode, final String confirmationCode) {

        this.type = type;
        this.code = code;
        this.pct = pct;
        this.amount = amount;
        this.style = style;
        this.points = points;
        this.redeemCode = redeemCode;
        this.confirmationCode = confirmationCode;
    }

    public String getType() {
        return type;
    }

    public String getCode() {
        return code;
    }

    public int getPct() {
        return pct;
    }

    public int getAmount() {
        return amount;
    }

    public String getStyle() {
        return style;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @return the redeemCode
     */
    public String getRedeemCode() {
        return redeemCode;
    }

    /**
     * @return the confirmationCode
     */
    public String getConfirmationCode() {
        return confirmationCode;
    }




}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;


/**
 * @author rsamuel3
 *
 */
public class WishListEntry {
    private String selectedVariantCode;
    private String baseProductCode;
    private String timestamp;
    private String price;
    private String priceRange;
    private String imageUrl;
    private String colour;
    private String size;


    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * @param colour
     *            the colour to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the selectedVariantCode
     */
    public String getSelectedVariantCode() {
        return selectedVariantCode;
    }

    /**
     * @param selectedVariantCode
     *            the selectedVariantCode to set
     */
    public void setSelectedVariantCode(final String selectedVariantCode) {
        this.selectedVariantCode = selectedVariantCode;
    }

    /**
     * @return the baseProductCode
     */
    public String getBaseProductCode() {
        return baseProductCode;
    }

    /**
     * @param baseProductCode
     *            the baseProductCode to set
     */
    public void setBaseProductCode(final String baseProductCode) {
        this.baseProductCode = baseProductCode;
    }

    /**
     * @return the timestamp
     * @throws ParseException
     */
    public String getTimestamp() throws ParseException {
        return getTimeInMillis();
    }

    /**
     * @param timestamp
     *            the timestamp to set
     */
    public void setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return String representing time in millis
     * @throws ParseException
     */
    private String getTimeInMillis() throws ParseException {
        final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        return String.valueOf(format.parse(this.timestamp).getTime());
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final String price) {
        this.price = price;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl
     *            the imageUrl to set
     */
    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the priceRange
     */
    public String getPriceRange() {
        return priceRange;
    }

    /**
     * @param priceRange
     *            the priceRange to set
     */
    public void setPriceRange(final String priceRange) {
        this.priceRange = priceRange;
    }
}

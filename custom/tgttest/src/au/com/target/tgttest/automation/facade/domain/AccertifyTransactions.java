/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.springframework.util.StringUtils;

import au.com.target.tgtfraud.provider.data.BillingAddress;
import au.com.target.tgtfraud.provider.data.CreditCardDetail;
import au.com.target.tgtfraud.provider.data.FailedGiftCardDetail;
import au.com.target.tgtfraud.provider.data.FailedPaymentInfo;
import au.com.target.tgtfraud.provider.data.Order;
import au.com.target.tgtfraud.provider.data.OrderItem;
import au.com.target.tgtfraud.provider.data.PaymentInfo;
import au.com.target.tgtfraud.provider.data.Transaction;
import au.com.target.tgtfraud.provider.data.ZipPaymentDetail;
import au.com.target.tgttest.automation.facade.bean.AccertifyCardInfo;
import au.com.target.tgttest.automation.facade.bean.AccertifyOrderItem;
import au.com.target.tgttest.automation.facade.bean.AccertifyTransactionInfo;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.CreditCardData;
import au.com.target.tgttest.automation.facade.bean.FailZipDetails;
import au.com.target.tgttest.automation.mock.MockAccertifyRestOperations;


/**
 * @author cwijesu1
 * 
 */
public class AccertifyTransactions {

    private final Transaction transaction;

    /**
     * @param transaction
     */
    public AccertifyTransactions(final Transaction transaction) {
        this.transaction = transaction;
    }

    public static AccertifyTransactions getLastTransaction()
    {
        final Transaction trans = MockAccertifyRestOperations.getTransaction();
        return new AccertifyTransactions(trans);

    }

    public String getTargetStaffDiscountCard()
    {
        return getOrder().getMemberInformation().getTargetStaffDiscountCard();
    }

    public double getTotalAmount()
    {
        return getOrder().getTotalAmount().doubleValue();
    }

    public double getPromotionAmount()
    {
        return getOrder().getPromotionAmount().doubleValue();
    }

    private Order getOrder()
    {
        assertThat(transaction.getOrders().size()).as("Number of orders in the accertify transaction").isEqualTo(1);
        return transaction.getOrders().get(0);
    }

    public void verifyEntries(final List<AccertifyTransactionInfo> expectedEntries)
    {
        //#TODO needs to find a way to assign a null to a double value. if the bigdesimal is null force the promitive double to be zero
        double promoamt;
        String tmd;
        for (final AccertifyTransactionInfo accertifyEntry : expectedEntries)
        {
            tmd = getOrder().getMemberInformation().getTargetStaffDiscountCard();
            if (getOrder().getPromotionAmount() == null) {
                promoamt = 0.0;
            }
            else
            {
                promoamt = getOrder().getPromotionAmount().doubleValue();
            }


            if (tmd == null) {
                tmd = "";
            }
            assertThat(tmd).as("TMD Card Number").isEqualTo(
                    accertifyEntry.getTargetStaffDiscountCard());
            assertThat(getOrder().getTotalAmount().doubleValue()).as("Total Amount").isEqualTo(
                    accertifyEntry.getTotalAmount());
            assertThat(promoamt).as("Promotional amount").isEqualTo(
                    accertifyEntry.getPromotionAmount());
        }
    }

    public void verifyPaymentInformation(final List<AccertifyCardInfo> cardInfos) {
        for (final AccertifyCardInfo cardInfo : cardInfos) {
            verifyPaymentInformation(cardInfo);
        }
    }

    /**
     * @param cardInfo
     */
    private void verifyPaymentInformation(final AccertifyCardInfo cardInfo) {
        final List<PaymentInfo> paymentInfos = transaction.getOrders().get(0).getPaymentInformation();
        for (final PaymentInfo paymentInfo : paymentInfos) {
            if ("Zippay".equals(cardInfo.getPaymentType())) {
                assertThat(paymentInfo.getPaymentType().getType()).as("Payment Type").isEqualTo(
                        cardInfo.getPaymentType());
                assertThat(paymentInfo.getZipAmount().doubleValue()).as("Zip Amount").isEqualTo(
                        Double.valueOf(cardInfo.getZipAmount()));
                assertThat(paymentInfo.getZipStatus()).as("Zip Status").isEqualTo(
                        cardInfo.getZipStatus());
                assertThat(paymentInfo.getZipRefundId()).as("Zip Refund Id").isEqualTo(
                        cardInfo.getZipRefundId());
            }
            else if ("PayPal".equals(cardInfo.getPaymentType())
                    || paymentInfo.getCardNumber().equals(cardInfo.getCardNumber())) {
                assertThat(paymentInfo.getPaymentType().getType()).as("Payment Type").isEqualTo(
                        cardInfo.getPaymentType());
                if ("PayPal".equals(cardInfo.getPaymentType())) {
                    assertThat(paymentInfo.getPayPalAmount().doubleValue()).as("Paypal Amount").isEqualTo(
                            Double.valueOf(cardInfo.getPaypalAmount()));
                    assertThat(paymentInfo.getPayPalStatus()).as("Paypal Status").isEqualTo(
                            cardInfo.getPaypalStatus());
                }
                else {
                    assertThat(paymentInfo.getCardType()).as("Card Type").isEqualTo(
                            cardInfo.getCardType());
                    assertThat(paymentInfo.getCardAuthorizedAmount().doubleValue()).as("Card Amount").isEqualTo(
                            Double.valueOf((cardInfo.getCardAuthorizedAmount())));
                    assertThat(paymentInfo.getCardExpireDate()).as("Card ExpiryDate").isEqualTo(
                            cardInfo.getCardExpiryDate());
                    assertThat(paymentInfo.getCardRRN()).as("Receipt Number").isEqualTo(
                            cardInfo.getCardReceipt());
                    assertThat(paymentInfo.getCardTokenID()).as("Card Token").isEqualTo(cardInfo.getCardToken());
                }
            }
        }


    }

    public void verifyAddressInformation(final AddressEntry addressEntry) {
        final BillingAddress billingAddress = transaction.getOrders().get(0).getBillingAddress();
        assertThat(billingAddress.getBillingFirstName()).as("FirstName").isEqualTo(
                addressEntry.getFirstName());
        assertThat(billingAddress.getBillingLastName()).as("Last Name").isEqualTo(
                addressEntry.getLastName());
        assertThat(billingAddress.getBillingAddressLine1()).as("Billing Address Line1").isEqualTo(
                addressEntry.getAddressLine1());
        if (StringUtils.isEmpty(addressEntry.getAddressLine2())) {
            assertThat(billingAddress.getBillingAddressLine2()).isNull();
        }
        else {
            assertThat(billingAddress.getBillingAddressLine2()).as("Billing Address Line2").isEqualTo(
                    addressEntry.getAddressLine2());
        }
        assertThat(billingAddress.getBillingCity()).as("City").isEqualTo(
                addressEntry.getTown());
        assertThat(billingAddress.getBillingPostalCode()).as("Postal Code").isEqualTo(
                addressEntry.getPostalCode());
        assertThat(billingAddress.getBillingRegion()).as("Region").isEqualTo(
                addressEntry.getRegion());
        assertThat(billingAddress.getBillingCountry()).as("Country").isEqualTo(
                addressEntry.getCountry());
    }

    public void verifyCCPaymentFailedAttempts(final int attempts) {
        final FailedPaymentInfo faildInfo = transaction.getOrders().get(0).getFailedPaymentInformation();
        assertThat(faildInfo.getTotalFailedCCAttempts()).isEqualTo(attempts);
    }

    public void verifyGCPaymentFailedAttempts(final int attempts) {
        final FailedPaymentInfo faildInfo = transaction.getOrders().get(0).getFailedPaymentInformation();
        assertThat(faildInfo.getTotalFailedGCAttempts()).isEqualTo(attempts);
    }

    public void verifyFailedCCDetails(final List<CreditCardData> cardInfos) {
        final FailedPaymentInfo faildInfo = transaction.getOrders().get(0).getFailedPaymentInformation();
        assertThat(faildInfo.getFailCCdetails()).isNotNull();
        assertThat(faildInfo.getFailCCdetails().size()).isEqualTo(1);
        final CreditCardDetail cardDetail = faildInfo.getFailCCdetails().get(0);
        assertThat(cardDetail.getFailedcardNumber()).isEqualTo(cardInfos.get(0).getCardNumber());
        assertThat(cardDetail.getFailedcardType()).isEqualTo(cardInfos.get(0).getCardType());
        assertThat(cardDetail.getFailedcardExpireDate()).isEqualTo(cardInfos.get(0).getCardExpiry());
    }

    public void verifyFailedGCDetails(final List<CreditCardData> cardInfos) {
        final FailedPaymentInfo faildInfo = transaction.getOrders().get(0).getFailedPaymentInformation();
        assertThat(faildInfo.getFailGCdetails()).isNotNull();
        assertThat(faildInfo.getFailGCdetails().size()).isEqualTo(cardInfos.size());
        int count = 0;
        for (final FailedGiftCardDetail cardDetail : faildInfo.getFailGCdetails()) {
            final CreditCardData creditCardData = cardInfos.get(count++);
            assertThat(cardDetail.getFailedReceiptNumber()).isEqualTo(creditCardData.getCardNumber());
            assertThat(cardDetail.getFailedcardType()).isEqualTo(creditCardData.getCardType());
            assertThat(cardDetail.getFailedAmount()).isEqualTo(creditCardData.getAmount());
        }
    }

    public void verifyOrderType(final String orderType) {
        assertThat(transaction.getOrders().get(0).getOrderType().getType()).isEqualTo(orderType);
    }

    public void verifyTotalAmount(final String amount) {
        assertThat(getOrder().getTotalAmount().doubleValue()).as("Total Amount").isEqualTo(Double.parseDouble(amount));
    }

    public void verifyFailedCCBillingInfo(final AddressEntry addressEntry) {
        final FailedPaymentInfo paymentInfo = transaction.getOrders().get(0).getFailedPaymentInformation();
        assertThat(paymentInfo.getFailCCdetails()).isNotNull();
        final CreditCardDetail cardDetail = paymentInfo.getFailCCdetails().get(0);
        assertThat(cardDetail.getFailedbillingFirstName()).as("FirstName").isEqualTo(
                addressEntry.getFirstName());
        assertThat(cardDetail.getFailedbillingLastName()).as("Last Name").isEqualTo(
                addressEntry.getLastName());
        assertThat(cardDetail.getFailedbillingAddressLine1()).as("Billing Address Line1").isEqualTo(
                addressEntry.getAddressLine1());
        if (StringUtils.isEmpty(addressEntry.getAddressLine2())) {
            assertThat(cardDetail.getFailedbillingAddressLine2()).isNull();
        }
        else {
            assertThat(cardDetail.getFailedbillingAddressLine2()).as("Billing Address Line2").isEqualTo(
                    addressEntry.getAddressLine2());
        }
        assertThat(cardDetail.getFailedbillingCity()).as("City").isEqualTo(
                addressEntry.getTown());
        assertThat(cardDetail.getFailedbillingPostalCode()).as("Postal Code").isEqualTo(
                addressEntry.getPostalCode());
        assertThat(cardDetail.getFailedbillingRegion()).as("Region").isEqualTo(
                addressEntry.getRegion());
        assertThat(cardDetail.getFailedbillingCountry()).as("Country").isEqualTo(
                addressEntry.getCountry());
    }

    public void isBillingAddressIncluded(final boolean included) {
        final BillingAddress billingAddress = transaction.getOrders().get(0).getBillingAddress();
        if (included) {
            assertThat(billingAddress).isNotNull();
        }
        else {
            assertThat(billingAddress).isNull();
        }
    }

    public void isBillingInfoIncludedInFailedCCDetails(final boolean included) {
        final CreditCardDetail failedCC = transaction.getOrders().get(0).getFailedPaymentInformation()
                .getFailCCdetails().get(0);
        if (included) {
            assertThat(failedCC.getFailedbillingAddressLine1()).isNotNull();
            assertThat(failedCC.getFailedbillingCity()).isNotNull();
        }
        else {
            assertThat(failedCC.getFailedbillingAddressLine1()).isNull();
            assertThat(failedCC.getFailedbillingCity()).isNull();
        }
    }

    public void verifyOrderItem(final List<AccertifyOrderItem> orderItems) {
        final List<OrderItem> actualOrderItems = getOrder().getOrderDetails().getOrderItem();
        assertThat(actualOrderItems).isNotEmpty();
        assertThat(actualOrderItems.size()).isEqualTo(orderItems.size());
        int count = 0;
        for (final AccertifyOrderItem expectedOrderItem : orderItems) {
            final OrderItem actualOrderItem = actualOrderItems.get(count++);
            assertThat(actualOrderItem.getItemNumber()).isEqualTo(expectedOrderItem.getItemNumber());
            assertThat(actualOrderItem.getQuantity()).isEqualTo(expectedOrderItem.getQuantity());
            assertThat(actualOrderItem.getCategory()).isEqualTo(expectedOrderItem.getCategory());
            verifyRecipientEmailAddress(expectedOrderItem, actualOrderItem);
        }
    }

    private void verifyRecipientEmailAddress(final AccertifyOrderItem expected, final OrderItem actual) {
        final String[] emailAddresses = expected.getRecipientEmailAddresses().split(",");
        assertThat(emailAddresses.length).isEqualTo(actual.getRecipientemailAddress().size());
        for (int i = 0; i < emailAddresses.length; i++) {
            assertThat(emailAddresses[i]).isEqualTo(actual.getRecipientemailAddress().get(i));
        }
    }

    public void verifyFailedPaymentInformation(final List<FailZipDetails> items) {
        final FailZipDetails item = items.get(0);
        final FailedPaymentInfo failedPaymentInfo = transaction.getOrders().get(0).getFailedPaymentInformation();
        final List<ZipPaymentDetail> failzipDetail = failedPaymentInfo.getFailZIPDetails();
        assertThat(failedPaymentInfo.getTotalFailedZIPAttempts()).isEqualTo(item.getTotalFailedZIPAttempts());
        if (!failzipDetail.isEmpty()) {
            final ZipPaymentDetail detail = failzipDetail.get(0);
            assertThat(detail.getFailedAmount()).isEqualTo(item.getFailedAmount());
            assertThat(detail.getFailedStatus()).isEqualTo(item.getFailedStatus());
            assertThat(detail.getFailedRequestId()).isNotEmpty();
        }

    }
}

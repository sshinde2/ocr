/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;

import de.hybris.platform.processengine.model.BusinessProcessModel;


/**
 * @author bhuang3
 * 
 */
public final class BusinessProcess {

    private static BusinessProcessModel businessProcess;

    private BusinessProcess() {
        //
    }



    public static BusinessProcessModel getInstance() {
        return businessProcess;
    }

    public static void setBusinessProcessModel(final BusinessProcessModel process) {
        businessProcess = process;
    }
}

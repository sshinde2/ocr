/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;

import java.util.List;

import au.com.target.tgtfacades.delivery.data.TargetZoneDeliveryModeData;
import au.com.target.tgtfacades.order.data.AdjustedCartEntriesData;
import au.com.target.tgtfacades.order.enums.TargetPlaceOrderResultEnum;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.bean.CartEntry;
import au.com.target.tgttest.automation.facade.bean.PriceData;
import au.com.target.tgttest.automation.facade.bean.StockLevelData;


/**
 * @author bhuang3
 * 
 */
public final class Checkout {

    public static final int GEELONG_STORE_NUMBER = 7001;

    private static Checkout checkout;

    private String userCheckoutMode;
    private String deliveryMode;
    private String paymentMethod;
    private String tmdCardType;
    private String flybuysNumber;
    private int flyBuysPoints;
    private String voucher;
    private String deliveryAddress;
    private String billingAddress;

    private List<PriceData> posPriceUpdateInCheckout;
    private boolean somethingWrongWithOrderCreation = false;
    private int storeNumber;
    private List<CartEntry> beforPlaceOrderUpdateCartEntries;
    private List<CartEntry> beforPlaceOrderAddCartEntries;
    private List<StockLevelData> beforePlaceOrderStockLevel;

    private TargetPlaceOrderResultEnum placeOrderResult;
    private AdjustedCartEntriesData adjustedData;
    private String productOutOfStockBeforePlaceOrder;
    private List<TargetZoneDeliveryModeData> applicableDeliveryModeData;
    private Response lastResponse;
    private boolean deliveryFeeChanged = false;
    private boolean spc = false;

    /**
     * set default values
     */
    private Checkout() {
        userCheckoutMode = CheckoutUtil.GUEST_CHECKOUT;
        deliveryMode = CheckoutUtil.HOME_DELIVERY_CODE;
        paymentMethod = CheckoutUtil.NEW_CARD_METHOD;
        tmdCardType = "";
        flybuysNumber = CheckoutUtil.FLYBUYS_CODE;
        flyBuysPoints = 0;
        storeNumber = GEELONG_STORE_NUMBER;
        voucher = "";
        applicableDeliveryModeData = null;
    }

    /**
     * static method to get the singleton
     * 
     * @return Checkout
     */
    public static Checkout getInstance() {
        return checkout;
    }


    /**
     * Reset defaults for checkout for every scenario, need to call in initialization
     */
    public static void initialise() {
        checkout = new Checkout();
    }

    /**
     * Return true if this is a kisok type payment method
     * 
     * @return true if kiosk method
     */
    public boolean isKioskMethod() {
        return (paymentMethod != null && (paymentMethod.equals(CheckoutUtil.PAYPAL_HERE_METHOD) || paymentMethod
                .equals(CheckoutUtil.PINPAD_METHOD)));
    }

    public void setDeliveryMode(final String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public void setPaymentMethod(final String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }


    public String getUserCheckoutMode() {
        return userCheckoutMode;
    }

    public void setUserCheckoutMode(final String userCheckoutMode) {
        this.userCheckoutMode = userCheckoutMode;
    }

    public List<PriceData> getPosPriceUpdateInCheckout() {
        return posPriceUpdateInCheckout;
    }

    public void setPosPriceUpdateInCheckout(final List<PriceData> posPriceUpdateInCheckout) {
        this.posPriceUpdateInCheckout = posPriceUpdateInCheckout;
    }

    public TargetPlaceOrderResultEnum getPlaceOrderResult() {
        return placeOrderResult;
    }

    public void setPlaceOrderResult(final TargetPlaceOrderResultEnum placeOrderResult) {
        this.placeOrderResult = placeOrderResult;
    }

    public String getTmdCardType() {
        return tmdCardType;
    }

    public void setTmdCardType(final String tmdCardType) {
        this.tmdCardType = tmdCardType;
    }

    public String getFlybuysNumber() {
        return flybuysNumber;
    }

    public void setFlybuysNumber(final String flybuysNumber) {
        this.flybuysNumber = flybuysNumber;
    }

    /**
     * @return the flyBuysPoints
     */
    public int getFlyBuysPoints() {
        return flyBuysPoints;
    }

    /**
     * @param flyBuysPoints
     *            the flyBuysPoints to set
     */
    public void setFlyBuysPoints(final int flyBuysPoints) {
        this.flyBuysPoints = flyBuysPoints;
    }

    public boolean isSomethingWrongWithOrderCreation() {
        return somethingWrongWithOrderCreation;
    }

    public void setSomethingWrongWithOrderCreation(final boolean somethingWrongWithOrderCreation) {
        this.somethingWrongWithOrderCreation = somethingWrongWithOrderCreation;
    }

    public int getStoreNumber() {
        return storeNumber;
    }

    public String getStoreNumberAsString() {
        return String.valueOf(storeNumber);
    }

    public void setStoreNumber(final int storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(final String voucher) {
        this.voucher = voucher;
    }

    /**
     * @return the deliveryAddress
     */
    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * In this method..it is assumed that the delivery address is in the format "11,Coleman Street,Koonibba,SA,5690"
     * 
     * @param deliveryAddress
     *            the deliveryAddress to set
     */
    public void setDeliveryAddress(final String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the billingAddress
     */
    public String getBillingAddress() {
        return billingAddress;
    }

    /**
     * @param billingAddress
     *            the billingAddress to set
     */
    public void setBillingAddress(final String billingAddress) {
        this.billingAddress = billingAddress;
    }

    /**
     * @return the productOutOfStockBeforePlaceOrder
     */
    public String getProductOutOfStockBeforePlaceOrder() {
        return productOutOfStockBeforePlaceOrder;
    }

    /**
     * @param productOutOfStockBeforePlaceOrder
     *            the productOutOfStockBeforePlaceOrder to set
     */
    public void setProductOutOfStockBeforePlaceOrder(final String productOutOfStockBeforePlaceOrder) {
        this.productOutOfStockBeforePlaceOrder = productOutOfStockBeforePlaceOrder;
    }

    /**
     * @return the applicableDeliveryModeData
     */
    public List<TargetZoneDeliveryModeData> getApplicableDeliveryModeData() {
        return applicableDeliveryModeData;
    }

    /**
     * @param applicableDeliveryModeData
     *            the applicableDeliveryModeData to set
     */
    public void setApplicableDeliveryModeData(final List<TargetZoneDeliveryModeData> applicableDeliveryModeData) {
        this.applicableDeliveryModeData = applicableDeliveryModeData;
    }

    public Response getLastResponse() {
        return lastResponse;
    }

    public void setLastResponse(final Response lastResponse) {
        this.lastResponse = lastResponse;
    }

    /**
     * @return the deliveryFeeChanged
     */
    public boolean isDeliveryFeeChanged() {
        return deliveryFeeChanged;
    }

    /**
     * @param deliveryFeeChanged
     *            the deliveryFeeChanged to set
     */
    public void setDeliveryFeeChanged(final boolean deliveryFeeChanged) {
        this.deliveryFeeChanged = deliveryFeeChanged;
    }

    /**
     * @return the adjustedData
     */
    public AdjustedCartEntriesData getAdjustedData() {
        return adjustedData;
    }

    /**
     * @param adjustedData
     *            the adjustedData to set
     */
    public void setAdjustedData(final AdjustedCartEntriesData adjustedData) {
        this.adjustedData = adjustedData;
    }

    public boolean isSpc() {
        return spc;
    }

    /**
     * @param spc
     *            the spc to set
     */
    public void setSpc(final boolean spc) {
        this.spc = spc;
    }

    /**
     * @return the beforPlaceOrderUpdateCartEntries
     */
    public List<CartEntry> getBeforPlaceOrderUpdateCartEntries() {
        return beforPlaceOrderUpdateCartEntries;
    }

    /**
     * @param beforPlaceOrderUpdateCartEntries
     *            the beforPlaceOrderUpdateCartEntries to set
     */
    public void setBeforPlaceOrderUpdateCartEntries(final List<CartEntry> beforPlaceOrderUpdateCartEntries) {
        this.beforPlaceOrderUpdateCartEntries = beforPlaceOrderUpdateCartEntries;
    }

    /**
     * @return the beforPlaceOrderAddCartEntries
     */
    public List<CartEntry> getBeforPlaceOrderAddCartEntries() {
        return beforPlaceOrderAddCartEntries;
    }

    /**
     * @param beforPlaceOrderAddCartEntries
     *            the beforPlaceOrderAddCartEntries to set
     */
    public void setBeforPlaceOrderAddCartEntries(final List<CartEntry> beforPlaceOrderAddCartEntries) {
        this.beforPlaceOrderAddCartEntries = beforPlaceOrderAddCartEntries;
    }

    /**
     * @return the beforePlaceOrderStockLevel
     */
    public List<StockLevelData> getBeforePlaceOrderStockLevel() {
        return beforePlaceOrderStockLevel;
    }

    /**
     * @param beforePlaceOrderStockLevel
     *            the beforePlaceOrderStockLevel to set
     */
    public void setBeforePlaceOrderStockLevel(final List<StockLevelData> beforePlaceOrderStockLevel) {
        this.beforePlaceOrderStockLevel = beforePlaceOrderStockLevel;
    }

}

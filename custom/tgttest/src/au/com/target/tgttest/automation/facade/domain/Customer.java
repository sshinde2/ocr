/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgttest.automation.facade.CustomerUtil;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.CustomerAccountData;


/**
 * Represent the customer for the scenario
 * 
 */
public final class Customer {

    // Details representing a new customer, for registration
    private static final String CUST_FIRST_NAME = "FirstName";
    private static final String CUST_LAST_NAME = "LastName";
    private static final String CUST_LOGIN = "testuser@target.com.au";
    private static final String CUST_PASSWD = "password#123456";

    // Details of the registered customer
    private static final String REGISTERED_CUST_FIRST_NAME = "test";
    private static final String REGISTERED_CUST_LAST_NAME = "user";

    // Singleton Customer
    private static Customer theCustomer;

    private final String uid;

    private AddressEntry addressEntry;
    private CustomerAccountData customerData;

    private Customer(final String uid) {
        this.uid = uid;
    }

    public static Customer getInstance() {
        return theCustomer;
    }

    public static void setCustomer(final String uid) {
        theCustomer = new Customer(uid);
    }

    public static void setStandardCustomer() {
        setCustomer(CustomerUtil.getStandardTestCustomerUid());
    }

    public static void setGuestUser() {
        setCustomer(CustomerUtil.getDefaultGuestUserId());
    }

    public String getUid() {
        return uid;
    }

    public void initialize() {
        setNewCustomerData();
    }

    /**
     * @return the customerData
     */
    public CustomerAccountData getCustomerData() {
        return customerData;
    }

    /**
     * sets the new customer data
     */
    public void setNewCustomerData() {
        customerData = new CustomerAccountData();
        customerData.setFirstName(CUST_FIRST_NAME);
        customerData.setLastName(CUST_LAST_NAME);
        customerData.setLogin(CUST_LOGIN);
        customerData.setPassword(CUST_PASSWD);
        customerData.setTitle("Mr");
    }

    /**
     * sets the registered customer
     */
    public void setRegisteredCustomerData() {
        customerData = new CustomerAccountData();
        customerData.setFirstName(REGISTERED_CUST_FIRST_NAME);
        customerData.setLastName(REGISTERED_CUST_LAST_NAME);
        customerData.setLogin(CustomerUtil.getStandardTestCustomerUid());
        customerData.setPassword(CUST_PASSWD);
        customerData.setTitle("Mr");
    }

    /**
     * sets the registered customer
     */
    public void setNewCustomerAddress(final String addressLine1, final String town, final String postalCode,
            final boolean defaultAddress) {
        addressEntry = new AddressEntry();
        addressEntry.setAddressLine1(addressLine1);
        addressEntry.setTown(town);
        addressEntry.setPostalCode(postalCode);
        addressEntry.setCountry(TgtCoreConstants.COUNTRY_ISO_CODE_AUSTRALIA);
        addressEntry.setDefaultAddress(defaultAddress);
    }


    /**
     * @return the addressEntry
     */
    public AddressEntry getAddressEntry() {
        return addressEntry;
    }



}

/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;

import java.util.List;

import junit.framework.Assert;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtsale.ist.dto.ItemDto;
import au.com.target.tgtsale.ist.dto.StockAdjustmentDto;
import au.com.target.tgttest.automation.facade.bean.InstoreTransferProductEntry;
import au.com.target.tgttest.automation.mock.MockISTMessageConverter;


/**
 * @author Vivek
 *
 */
public class InterStoreTransferUtil {

    private final StockAdjustmentDto adjustmentDto;

    public InterStoreTransferUtil(final StockAdjustmentDto adjustmentDto) {
        this.adjustmentDto = adjustmentDto;
    }

    /**
     * @return InterStoreTransfer
     */
    public static InterStoreTransferUtil getLastInterStoreTransfer() {
        final StockAdjustmentDto adjustment = MockISTMessageConverter.getAdjustmentDto();

        return new InterStoreTransferUtil(adjustment);
    }

    /**
     * Verifies that inter store transfer happened for correct Store
     * 
     * @param fromStore
     */
    public void verifyFromStore(final Integer fromStore) {
        Assert.assertEquals(adjustmentDto.getFromStore(), fromStore);
    }

    /**
     * Verifies that inter store tranfer happened for all the Products
     * 
     * @param instoreTransferEntries
     */
    public void verifyInstoreTransferEntries(final List<InstoreTransferProductEntry> instoreTransferEntries) {
        final List<ItemDto> itemDtos = adjustmentDto.getItems();
        Assert.assertEquals(instoreTransferEntries.size(), itemDtos.size());
        int count = 0;
        for (final ItemDto dto : itemDtos) {
            for (final InstoreTransferProductEntry entry : instoreTransferEntries) {
                if (StringUtils.equals(dto.getCode(), entry.getProduct())) {
                    Assert.assertEquals(dto.getQty().longValue(), entry.getQty());
                    count++;
                }
            }
        }
        Assert.assertEquals(count, itemDtos.size());
    }

    /**
     * Verifies that Inter Store tranfer didn't happen
     */
    public void verifyInstoreTransferNotTriggered() {
        Assert.assertNull(adjustmentDto);
    }
}

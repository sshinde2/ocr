/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;

import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Represent the order in the scenario
 * 
 */
public final class Order {

    private static Order theOrder;

    private final OrderModel orderModel;

    private Order(final OrderModel orderModel) {
        this.orderModel = orderModel;
    }

    public static void reset() {
        theOrder = null;
    }

    public static Order getInstance() {
        return theOrder;
    }

    public static void setOrder(final OrderModel orderModel) {
        theOrder = new Order(orderModel);
    }

    public OrderModel getOrderModel() {
        if (orderModel != null) {
            refresh();
        }
        return orderModel;
    }

    public void refresh() {
        ServiceLookup.getModelService().refresh(orderModel);
    }

    public String getOrderNumber() {
        return orderModel.getCode();
    }

    public double getTotal() {
        return orderModel.getTotalPrice().doubleValue();
    }

    public double getDeliveryCost() {
        return orderModel.getDeliveryCost().doubleValue();
    }

    public double getSubtotal() {
        return orderModel.getSubtotal().doubleValue();
    }

    public double getTotalTax() {
        return orderModel.getTotalTax().doubleValue();
    }

    public double getTotalDiscounts() {
        return orderModel.getTotalDiscounts().doubleValue();
    }

    public String getOrderStatus() {
        return orderModel.getStatus().getCode();

    }


}

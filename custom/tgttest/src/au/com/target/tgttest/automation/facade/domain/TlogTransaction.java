/**
 * 
 */
package au.com.target.tgttest.automation.facade.domain;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Delta;

import au.com.target.tgtsale.tlog.data.AddressInfo;
import au.com.target.tgtsale.tlog.data.AddressTypeEnum;
import au.com.target.tgtsale.tlog.data.CustomerInfo;
import au.com.target.tgtsale.tlog.data.DiscountInfo;
import au.com.target.tgtsale.tlog.data.Flybuys;
import au.com.target.tgtsale.tlog.data.ItemInfo;
import au.com.target.tgtsale.tlog.data.LayByFee;
import au.com.target.tgtsale.tlog.data.TenderInfo;
import au.com.target.tgtsale.tlog.data.TenderTypeEnum;
import au.com.target.tgtsale.tlog.data.Transaction;
import au.com.target.tgtsale.tlog.data.TransactionDeal;
import au.com.target.tgttest.automation.facade.CheckoutUtil;
import au.com.target.tgttest.automation.facade.bean.AddressEntry;
import au.com.target.tgttest.automation.facade.bean.CustomerAccountData;
import au.com.target.tgttest.automation.facade.bean.TlogCreditCardPaymentInfo;
import au.com.target.tgttest.automation.facade.bean.TlogEntry;
import au.com.target.tgttest.automation.facade.bean.TlogPinPadPaymentInfo;
import au.com.target.tgttest.automation.facade.bean.TlogTransDeal;
import au.com.target.tgttest.automation.facade.bean.TlogTransDiscount;
import au.com.target.tgttest.automation.mock.MockTLOGMessageConverter;


/**
 * Represents an actual TLOG transaction result.<br/>
 * Initialised with a tlog.data.Transaction - but could decide to use XML.
 * 
 */
public class TlogTransaction {

    private final Transaction transaction;

    public TlogTransaction(final Transaction transaction) {

        this.transaction = transaction;
    }

    /**
     * Get the last instance of TLOG Transaction that was made.
     * 
     * @return TlogTransaction
     */
    public static TlogTransaction getLastTransaction() {

        // Get the last transaction from the TLOG Mock converter
        final Transaction trans = MockTLOGMessageConverter.getTransaction();

        return new TlogTransaction(trans);
    }

    /**
     * Verify TLOG entries against expected
     * 
     * @param expectedEntries
     */
    public void verifyEntries(final List<TlogEntry> expectedEntries) {

        assertThat(transaction.getItems().size()).isEqualTo(expectedEntries.size());

        final List<Integer> used = new ArrayList<>();
        for (final TlogEntry entry : expectedEntries) {
            used.add(checkEntryIsInTransaction(entry, used));
        }

    }

    /**
     * Verify TLOG entries against expected
     * 
     * @param reasonCode
     */
    public void verifyTlogReason(final String reasonCode) {


        for (final ItemInfo actualEntry : transaction.getItems()) {
            assertThat(actualEntry.getReason()).isNotNull();
            if (actualEntry.getReason() != null) {
                assertThat(actualEntry.getReason().getCode()).isEqualTo(reasonCode);
            }
        }

    }

    public String getUsername() {
        return transaction.getOrder().getUserName();
    }

    public String getType() {
        return transaction.getType().getType();
    }

    public String getLaybyType() {
        return transaction.getLaybyType();
    }

    public String getLaybyVersion() {
        return transaction.getLaybyVersion();
    }

    public LayByFee getLaybyFee() {
        return transaction.getLaybyFee();
    }

    public CustomerInfo getCustomerInfo() {
        return transaction.getCustomerInfo();
    }

    public int getShipping() {
        return getIntValueOfPrice(transaction.getShippingInfo().getAmount());
    }

    public String getCurrency() {
        return transaction.getCurrency();
    }

    public String getSalesChannel() {
        return transaction.getSalesChannel();
    }

    public int getTotalTender() {
        final List<TenderInfo> tenders = transaction.getTender();
        assertThat(tenders).isNotEmpty();

        BigDecimal totalTender = BigDecimal.ZERO;

        for (final TenderInfo tender : tenders) {
            if (!"Y".equals(tender.getApproved())) {
                continue;
            }

            totalTender = totalTender.add(tender.getAmount());
        }

        return getIntValueOfPrice(totalTender);
    }

    private Integer checkEntryIsInTransaction(final TlogEntry expectedEntry, final List<Integer> used) {

        Integer foundMatch = null;

        int count = 0;
        for (final ItemInfo actualEntry : transaction.getItems()) {
            count++;

            if (!used.contains(Integer.valueOf(count)) && actualEntry.getId().equals(expectedEntry.getCode())) {

                // For a particular product, there may be entries with and without discount and deal,
                // so match based on that as well as code
                if (StringUtils.isNotEmpty(expectedEntry.getItemDiscountType())
                        && actualEntry.getItemDiscount() == null) {
                    continue;
                }
                if (StringUtils.isEmpty(expectedEntry.getItemDiscountType())
                        && actualEntry.getItemDiscount() != null) {
                    continue;
                }

                if (StringUtils.isNotEmpty(expectedEntry.getItemDealType())
                        && actualEntry.getItemDeal() == null) {
                    continue;
                }
                if (StringUtils.isEmpty(expectedEntry.getItemDealType())
                        && actualEntry.getItemDeal() != null) {
                    continue;
                }

                foundMatch = Integer.valueOf(count);

                assertThat(actualEntry.getQuantity()).isEqualTo(Long.valueOf(expectedEntry.getQty()));
                assertThat(getIntValueOfPrice(actualEntry.getPrice())).isEqualTo(expectedEntry.getPrice());
                assertThat(getIntValueOfPrice(actualEntry.getFilePrice())).isEqualTo(expectedEntry.getFilePrice());

                // Optional itemDiscount
                if (StringUtils.isNotEmpty(expectedEntry.getItemDiscountType())) {
                    assertThat(actualEntry.getItemDiscount()).as("Actual Item discount").isNotNull();

                    assertThat(actualEntry.getItemDiscount().getType().getType()).isEqualTo(
                            expectedEntry.getItemDiscountType());
                    assertThat(actualEntry.getItemDiscount().getPercentage()).isEqualTo(
                            expectedEntry.getItemDiscountPct().doubleValue(), Delta.delta(0.01));
                    assertThat(getIntValueOfPrice(actualEntry.getItemDiscount().getAmount())).isEqualTo(
                            expectedEntry.getItemDiscountAmount());

                }

                // Optional itemDeal
                if (StringUtils.isNotEmpty(expectedEntry.getItemDealType())) {

                    assertThat(actualEntry.getItemDeal().getType()).isEqualTo(expectedEntry.getItemDealType());
                    assertThat(actualEntry.getItemDeal().getId()).isEqualTo(expectedEntry.getItemDealId());
                    assertThat(actualEntry.getItemDeal().getInstance()).isEqualTo(
                            expectedEntry.getItemDealInstance().toString());
                    assertThat(actualEntry.getItemDeal().getMarkdown()).isEqualTo(
                            expectedEntry.getItemDealMarkdown().toString());

                }

            }
        }

        assertThat(foundMatch).isNotNull();

        return foundMatch;
    }

    private int getIntValueOfPrice(final BigDecimal price) {
        return price.multiply(BigDecimal.valueOf(100)).intValue();
    }

    /**
     * Verify the TransDiscounts element
     * 
     * @param expected
     */
    public void verifyTransDiscounts(final List<TlogTransDiscount> expected) {

        final List<DiscountInfo> transDiscounts = transaction.getDiscounts();

        assertThat(transDiscounts.size()).isEqualTo(expected.size());

        for (final TlogTransDiscount expectedTransDiscount : expected) {

            boolean found = false;
            String actuals = "";
            for (final DiscountInfo discount : transDiscounts) {

                actuals += " type=" + discount.getType().getType()
                        + " amount=" + getIntValueOfPrice(discount.getAmount())
                        + " code=" + discount.getCode();

                if (discount.getType().getType().equals("staff")) {

                    actuals += " pct=" + discount.getPercentage().intValue();

                    // Replace 'tmd' for the valid tmd code
                    final String code = expectedTransDiscount.getCode().equals("tmd") ? CheckoutUtil.VALID_TMD
                            : expectedTransDiscount.getCode();

                    if (discount.getType().getType().equals(expectedTransDiscount.getType())
                            && getIntValueOfPrice(discount.getAmount()) == expectedTransDiscount.getAmount()
                            && discount.getCode().equals(code)
                            && discount.getPercentage().intValue() == expectedTransDiscount.getPct()) {
                        found = true;
                        break;
                    }
                }

                if (discount.getType().getType().equals("promo")) {

                    actuals += " style=" + expectedTransDiscount.getStyle();

                    if (discount.getType().getType().equals(expectedTransDiscount.getType())
                            && getIntValueOfPrice(discount.getAmount()) == expectedTransDiscount.getAmount()
                            && discount.getCode().equals(expectedTransDiscount.getCode())
                            && discount.getStyle().getStyle().equals(expectedTransDiscount.getStyle())) {
                        found = true;
                        break;
                    }
                }

                if (discount.getType().getType().equalsIgnoreCase("flybuys")) {

                    if (discount.getType().getType().equals(expectedTransDiscount.getType())
                            && getIntValueOfPrice(discount.getAmount()) == expectedTransDiscount.getAmount()
                            && discount.getCode().equals(CheckoutUtil.FLYBUYS_CODE)) {
                        found = true;
                        break;
                    }
                }

            }

            final String message = "Cannot match expected TransDiscount: expected:"
                    + " type=" + expectedTransDiscount.getType()
                    + " amount=" + expectedTransDiscount.getAmount()
                    + " code=" + expectedTransDiscount.getCode()
                    + " pct=" + expectedTransDiscount.getPct()
                    + " style=" + expectedTransDiscount.getStyle()
                    + " got:" + actuals;
            assertThat(found).overridingErrorMessage(message).isTrue();
        }

    }

    /**
     * Verify the transdeal elements
     * 
     * @param expected
     */
    public void verifyTransDeals(final List<TlogTransDeal> expected) {

        final List<TransactionDeal> transDeals = transaction.getTransactionDeal();

        assertThat(transDeals.size()).isEqualTo(expected.size());

        for (final TlogTransDeal expectedTransDeal : expected) {

            boolean found = false;
            String actuals = "";
            for (final TransactionDeal deal : transDeals) {

                actuals += " type=" + deal.getType()
                        + " id=" + deal.getId()
                        + " instance=" + deal.getInstance()
                        + " markdown=" + deal.getMarkdown();

                if (deal.getType().equals(expectedTransDeal.getType())
                        && deal.getId().equals(expectedTransDeal.getId())
                        && deal.getInstance().equals(Integer.toString(expectedTransDeal.getInstance()))
                        && deal.getMarkdown().equals(Integer.toString(expectedTransDeal.getMarkdown()))) {
                    found = true;
                    break;
                }

            }

            final String message = "Cannot match expected TransDeal: expected:"
                    + " type=" + expectedTransDeal.getType()
                    + " id=" + expectedTransDeal.getId()
                    + " instance=" + expectedTransDeal.getInstance()
                    + " markdown=" + expectedTransDeal.getMarkdown()
                    + " got:" + actuals;
            assertThat(found).overridingErrorMessage(message).isTrue();
        }

    }

    /**
     * Verify credit card payment info in tlog tender
     * 
     * @param cardInfo
     */
    public void verifyCreditCardPaymentInfo(final TlogCreditCardPaymentInfo cardInfo) {

        final TenderInfo tender = transaction.getTender().get(0);

        assertThat(tender.getCardNumber()).as("tlog cardnumber").isEqualTo(cardInfo.getCardNumber());
        assertThat(tender.getCreditCardRRN()).as("tlog rrn").isNotEmpty();
        assertThat(tender.getApproved()).as("tlog approved").isEqualTo(cardInfo.isApproved() ? "Y" : "N");
    }

    /**
     * Verify pinpad payment info in tlog tender
     * 
     * @param cardInfo
     */
    public void verifyPinPadPaymentInfo(final TlogPinPadPaymentInfo cardInfo) {

        final TenderInfo tender = transaction.getTender().get(0);

        assertThat(tender.getCardNumber()).as("tlog cardnumber").isEqualTo(cardInfo.getCardNumber());
        assertThat(tender.getCreditCardRRN()).as("tlog rrn").isEqualTo(cardInfo.getRrn());
        assertThat(tender.getApproved()).as("tlog approved").isEqualTo(cardInfo.isApproved() ? "Y" : "N");
    }

    public void verifyMultiCardPaymentInfo(final List<TlogCreditCardPaymentInfo> cardInfoList) {
        final List<TenderInfo> tlogTenders = transaction.getTender();

        assertThat(tlogTenders).hasSize(cardInfoList.size());

        final Map<String, TlogCreditCardPaymentInfo> cardInfoMap = new HashMap<String, TlogCreditCardPaymentInfo>();
        for (final TlogCreditCardPaymentInfo cardInfo : cardInfoList) {
            cardInfoMap.put(cardInfo.getCardNumber(), cardInfo);
        }

        final List<TlogCreditCardPaymentInfo> usedCardInfo = new ArrayList<>();
        for (final TenderInfo tlogTender : tlogTenders) {
            TlogCreditCardPaymentInfo cardInfo = null;
            if (tlogTender.getCardNumber() == null) {
                cardInfo = cardInfoMap.get("null");
            }
            else {
                cardInfo = cardInfoMap.get(tlogTender.getCardNumber());
            }
            assertThat(usedCardInfo).excludes(cardInfo);
            usedCardInfo.add(cardInfo);

            verifyCardNumber(cardInfo, tlogTender);

            if (cardInfo.getRrn() != null) {
                assertThat(tlogTender.getCreditCardRRN()).isEqualTo(cardInfo.getRrn());
            }

            assertThat(tlogTender.getApproved()).isEqualTo(cardInfo.isApproved() ? "Y" : "N");
            assertThat(getIntValueOfPrice(tlogTender.getAmount()))
                    .isEqualTo(getIntValueOfPrice(cardInfo.getAmount()));
        }

        assertThat(usedCardInfo).containsOnly(cardInfoList.toArray());
    }

    public void verifyAfterpaySaleInfo() {
        final List<TenderInfo> tlogTenders = transaction.getTender();
        assertThat(tlogTenders).hasSize(1);
        assertThat(tlogTenders.get(0).getAfterPayOrderId()).isNotEmpty();
        assertThat(tlogTenders.get(0).getAfterPayRefundId()).isEqualTo("");
    }

    public void verifyPaymentPartnerSaleInfo(final String paymentPartner) {
        final List<TenderInfo> tlogTenders = transaction.getTender();
        assertThat(tlogTenders).hasSize(1);
        if (StringUtils.equalsIgnoreCase(paymentPartner, "zip")) {
            assertThat(tlogTenders.get(0).getOrderId()).isEqualTo("444324234");
            assertThat(tlogTenders.get(0).getRefundId()).isEqualTo("");
        }
    }

    public void verifyRefundInfo(final String refundId) {
        final List<TenderInfo> tlogTenders = transaction.getTender();
        assertThat(tlogTenders).hasSize(1);
        assertThat(tlogTenders.get(0).getType()).isNotNull();
        assertThat(tlogTenders.get(0).getOrderId()).isNotEmpty();
        assertThat(tlogTenders.get(0).getRefundId()).isEqualTo(refundId);
    }

    public void verifyAfterpayRefundInfo() {
        final List<TenderInfo> tlogTenders = transaction.getTender();
        assertThat(tlogTenders).hasSize(1);
        assertThat(tlogTenders.get(0).getAfterPayOrderId()).isNotEmpty();
        assertThat(tlogTenders.get(0).getAfterPayRefundId()).isNotEmpty();
    }

    public void verifySingleCardPaymentInfo(final List<TlogCreditCardPaymentInfo> cardInfoList) {
        assertThat(cardInfoList).hasSize(1);

        final List<TenderInfo> tlogTenders = transaction.getTender();
        assertThat(tlogTenders).hasSize(1);

        final TlogCreditCardPaymentInfo cardInfo = cardInfoList.get(0);
        final TenderInfo tlogTender = tlogTenders.get(0);

        verifyCardNumber(cardInfo, tlogTender);

        if (cardInfo.getRrn() != null) {
            assertThat(tlogTender.getCreditCardRRN()).isEqualTo(cardInfo.getRrn());
        }

        assertThat(tlogTender.getApproved()).isEqualTo(cardInfo.isApproved() ? "Y" : "N");
        assertThat(getIntValueOfPrice(tlogTender.getAmount()))
                .isEqualTo(getIntValueOfPrice(cardInfo.getAmount()));
        assertThat(tlogTender.getType().getType()).isEqualTo(cardInfo.getType());
    }

    public void verifyFlybuys(final String flybuysNumber) {
        final Flybuys flybuys = transaction.getFlybuys();
        assertThat(flybuys.getNumber()).as("FlyBuys Number").isEqualTo(flybuysNumber);
    }

    /**
     * very the first tender type
     * 
     * @param tenderType
     */
    public void verifyTenderType(final TenderTypeEnum tenderType) {
        final TenderInfo tenderInfo = (TenderInfo)CollectionUtils.get(transaction.getTender(), 0);
        assertThat(tenderInfo.getType()).as("tender type").isEqualTo(tenderType);
    }

    /**
     * Verify the layby type
     * 
     * @param laybyType
     */
    public void verifyLaybyType(final String laybyType) {
        assertThat(transaction.getLaybyType()).as("Layby Type").isEqualTo(laybyType);
    }

    public void verifyLaybyVersion(final String laybyVersion) {
        assertThat(transaction.getLaybyVersion()).as("Layby version").isEqualTo(laybyVersion);
    }

    public void verifyPaymentInfo() {
        assertThat(transaction.getPayment()).isNotNull();
        assertThat(transaction.getPayment().getAmount().doubleValue()).isEqualTo(
                CheckoutUtil.getInitialDepositForPreOrder());
    }

    public void verifyCustomerAddress(final List<AddressEntry> expectedEntries) {
        final List<AddressInfo> addresses = getCustomerInfo().getAddress();
        assertThat(addresses.size()).isEqualTo(expectedEntries.size());
        for (int i = 0; i < expectedEntries.size(); i++) {
            assertThat(addresses.get(i).getAddressLine1()).as("Address Line1").isEqualTo(
                    expectedEntries.get(i).getAddressLine1());
            assertThat(addresses.get(i).getAddressLine2()).as("Address Line2").isEqualTo(
                    expectedEntries.get(i).getAddressLine2());
            assertThat(addresses.get(i).getCity()).as("Post Code").isEqualTo(
                    expectedEntries.get(i).getRegion());
            assertThat(addresses.get(i).getPostCode()).as("Post Code").isEqualTo(
                    expectedEntries.get(i).getPostalCode());
            assertThat(addresses.get(i).getCountry()).as("Country").isEqualTo(
                    expectedEntries.get(i).getCountry());
            if (expectedEntries.get(i).isBillingAddress()) {
                assertThat(addresses.get(i).getType()).as("Address Type Billing").isEqualTo(AddressTypeEnum.BILLING);
            }
            if (expectedEntries.get(i).isShippingAddress()) {
                assertThat(addresses.get(i).getType()).as("Address Type Delivery").isEqualTo(AddressTypeEnum.DELIVERY);
            }
        }
    }

    public void verifyCustomerInfo(final CustomerAccountData expectedCustomerData) {
        final CustomerInfo actualCustomer = getCustomerInfo();
        assertThat(actualCustomer.getFirstName()).as("First Name").isEqualTo(expectedCustomerData.getFirstName());
        assertThat(actualCustomer.getLastName()).as("Last Name").isEqualTo(expectedCustomerData.getLastName());
        assertThat(actualCustomer.getEmail()).as("Email Address").isEqualTo(expectedCustomerData.getEmailAddress());
    }

    /**
     * verify the tlog due date is equal to the date in the order
     */
    public void verifyTlogDueDate() {
        final OrderModel order = Order.getInstance().getOrderModel();
        final Date orderSalesDate = order.getNormalSaleStartDateTime();
        assertThat(orderSalesDate).as("Order Normal Sales Date").isNotNull();
        assertThat(transaction.getDueDate()).as("Due Date").isEqualTo(orderSalesDate);
    }

    private void verifyCardNumber(final TlogCreditCardPaymentInfo expected, final TenderInfo actual) {
        if ("null".equalsIgnoreCase(expected.getCardNumber())) {
            assertThat(actual.getCardNumber()).isNull();
        }
        else {
            assertThat(actual.getCardNumber()).isEqualTo(expected.getCardNumber());
        }
    }

}

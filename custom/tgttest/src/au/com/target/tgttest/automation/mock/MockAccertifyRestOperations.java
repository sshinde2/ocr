/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import java.net.URI;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import au.com.target.tgtfraud.provider.data.RecommendationTypeEnum;
import au.com.target.tgtfraud.provider.data.Result;
import au.com.target.tgtfraud.provider.data.Transaction;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Sets up a mock RestOperations to capture accertify transaction instead of actually calling the REST API
 * 
 */
public final class MockAccertifyRestOperations {

    private static final Logger LOG = Logger.getLogger(MockAccertifyRestOperations.class.getName());

    private static Transaction transaction;

    private static String recommendation;

    private MockAccertifyRestOperations() {
        // util
    }

    // CHECKSTYLE:OFF
    /**
     * Set the mock RestOperations
     * 
     */
    public static void setMock() {

        final RestOperations mockRestOperations = new RestOperations() {

            private Result postForObjectInternal(final Object request) {
                transaction = (Transaction)request;

                LOG.info("Returning mock ACCEPT response from accertify mockRestOperations");
                final Result result = new Result();
                result.setRecommendation(RecommendationTypeEnum.valueOf(recommendation));
                return result;
            }

            @Override
            public void delete(final URI uri) throws RestClientException {
                // YTODO Auto-generated method stub

            }

            @Override
            public void delete(final String s, final Object... aobj) throws RestClientException {
                // YTODO Auto-generated method stub

            }

            @Override
            public void delete(final String s, final Map<String, ?> map) throws RestClientException {
                // YTODO Auto-generated method stub

            }

            @Override
            public <T> ResponseEntity<T> exchange(final URI uri, final HttpMethod httpmethod,
                    final HttpEntity<?> httpentity,
                    final Class<T> class1) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> exchange(final String s, final HttpMethod httpmethod,
                    final HttpEntity<?> httpentity,
                    final Class<T> class1, final Object... aobj) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> exchange(final String s, final HttpMethod httpmethod,
                    final HttpEntity<?> httpentity,
                    final Class<T> class1, final Map<String, ?> map) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> T execute(final URI uri, final HttpMethod httpmethod, final RequestCallback requestcallback,
                    final ResponseExtractor<T> responseextractor) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> T execute(final String s, final HttpMethod httpmethod, final RequestCallback requestcallback,
                    final ResponseExtractor<T> responseextractor, final Object... aobj) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> T execute(final String s, final HttpMethod httpmethod, final RequestCallback requestcallback,
                    final ResponseExtractor<T> responseextractor, final Map<String, ?> map) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> getForEntity(final URI uri, final Class<T> class1) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> getForEntity(final String s, final Class<T> class1, final Object... aobj)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> getForEntity(final String s, final Class<T> class1, final Map<String, ?> map)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> T getForObject(final URI uri, final Class<T> class1) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> T getForObject(final String s, final Class<T> class1, final Object... aobj)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> T getForObject(final String s, final Class<T> class1, final Map<String, ?> map)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public HttpHeaders headForHeaders(final URI uri) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public HttpHeaders headForHeaders(final String s, final Object... aobj) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public HttpHeaders headForHeaders(final String s, final Map<String, ?> map) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public Set<HttpMethod> optionsForAllow(final URI uri) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public Set<HttpMethod> optionsForAllow(final String s, final Object... aobj) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public Set<HttpMethod> optionsForAllow(final String s, final Map<String, ?> map)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> postForEntity(final URI uri, final Object obj, final Class<T> class1)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> postForEntity(final String s, final Object obj, final Class<T> class1,
                    final Object... aobj)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> postForEntity(final String s, final Object obj, final Class<T> class1,
                    final Map<String, ?> map)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public URI postForLocation(final URI uri, final Object obj) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public URI postForLocation(final String s, final Object obj, final Object... aobj)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public URI postForLocation(final String s, final Object obj, final Map<String, ?> map)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> T postForObject(final URI uri, final Object obj, final Class<T> class1)
                    throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> T postForObject(final String s, final Object obj, final Class<T> class1, final Object... aobj)
                    throws RestClientException {
                return (T)postForObjectInternal(obj);
            }

            @Override
            public <T> T postForObject(final String s, final Object obj, final Class<T> class1,
                    final Map<String, ?> map)
                    throws RestClientException {
                return (T)postForObjectInternal(obj);
            }

            @Override
            public void put(final URI uri, final Object obj) throws RestClientException {
                // YTODO Auto-generated method stub

            }

            @Override
            public void put(final String s, final Object obj, final Object... aobj) throws RestClientException {
                // YTODO Auto-generated method stub

            }

            @Override
            public void put(final String s, final Object obj, final Map<String, ?> map) throws RestClientException {
                // YTODO Auto-generated method stub

            }

            @Override
            public <T> ResponseEntity<T> exchange(final URI arg0, final HttpMethod arg1, final HttpEntity<?> arg2,
                    final ParameterizedTypeReference<T> arg3) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> exchange(final String arg0, final HttpMethod arg1, final HttpEntity<?> arg2,
                    final ParameterizedTypeReference<T> arg3, final Object... arg4) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

            @Override
            public <T> ResponseEntity<T> exchange(final String arg0, final HttpMethod arg1, final HttpEntity<?> arg2,
                    final ParameterizedTypeReference<T> arg3, final Map<String, ?> arg4) throws RestClientException {
                // YTODO Auto-generated method stub
                return null;
            }

        };

        ServiceLookup.getTargetAccertify().setRestOperations(mockRestOperations);
        ServiceLookup.getTargetMockAccertify().setRestOperations(mockRestOperations);


    }

    // CHECKSTYLE:ON

    /**
     * Get the last Accertify transaction
     * 
     * @return transaction
     */
    public static Transaction getTransaction() {
        return transaction;
    }

    /**
     * @return the recommendation
     */
    public static String getRecommendation() {
        return recommendation;
    }

    /**
     * @param recommendation
     *            the recommendation to set
     */
    public static void setRecommendation(final String recommendation) {
        MockAccertifyRestOperations.recommendation = recommendation;
    }

    /**
     * Sets the default recommendation.
     */
    public static void setDefaultRecommendation() {
        MockAccertifyRestOperations.recommendation = "ACCEPT";
    }

}

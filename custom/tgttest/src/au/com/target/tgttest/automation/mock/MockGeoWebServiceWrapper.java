/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import de.hybris.platform.storelocator.GPS;
import de.hybris.platform.storelocator.GeoWebServiceWrapper;
import de.hybris.platform.storelocator.data.AddressData;
import de.hybris.platform.storelocator.exception.GeoServiceWrapperException;
import de.hybris.platform.storelocator.location.Location;
import de.hybris.platform.storelocator.route.DistanceAndRoute;

import java.util.HashMap;

import au.com.target.tgttest.automation.ServiceLookup;


/**
 * @author bhuang3
 *
 */
public class MockGeoWebServiceWrapper {

    private static HashMap<String, GPS> gpsMap = new HashMap<String, GPS>();

    private MockGeoWebServiceWrapper() {
        //
    }



    public static void setMock() {

        ServiceLookup.getTargetStoreFinderService().setGeoWebServiceWrapper(new GeoWebServiceWrapper() {

            @Override
            public DistanceAndRoute getDistanceAndRoute(final GPS paramGPS, final Location paramLocation)
                    throws GeoServiceWrapperException {
                return null;
            }

            @Override
            public DistanceAndRoute getDistanceAndRoute(final Location paramLocation1, final Location paramLocation2)
                    throws GeoServiceWrapperException {
                return null;
            }

            @Override
            public GPS geocodeAddress(final AddressData paramAddressData) throws GeoServiceWrapperException {
                if (gpsMap.containsKey(paramAddressData.getCity())) {
                    return gpsMap.get(paramAddressData.getCity());
                }
                else {
                    throw new GeoServiceWrapperException();
                }
            }

            @Override
            public GPS geocodeAddress(final Location paramLocation) throws GeoServiceWrapperException {
                return null;
            }

            @Override
            public String formatAddress(final Location paramLocation) throws GeoServiceWrapperException {
                return null;
            }
        });
    }

    public static void setGps(final String locationText, final GPS gps) {
        MockGeoWebServiceWrapper.gpsMap.put(locationText, gps);
    }

    public static void resetGpsMap() {
        MockGeoWebServiceWrapper.gpsMap.clear();
    }
}

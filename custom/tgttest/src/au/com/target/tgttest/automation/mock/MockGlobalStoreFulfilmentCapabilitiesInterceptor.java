/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import au.com.target.tgtfulfilment.interceptor.GlobalStoreFulfilmentCapabilitiesValidateInterceptor;


/**
 * Mock GlobalStoreFulfilmentCapabilitiesInterceptor
 * 
 * @author Pratik
 *
 */
public class MockGlobalStoreFulfilmentCapabilitiesInterceptor extends
        GlobalStoreFulfilmentCapabilitiesValidateInterceptor {

    private static MockGlobalStoreFulfilmentCapabilitiesInterceptor interceptor = new MockGlobalStoreFulfilmentCapabilitiesInterceptor();

    public static MockGlobalStoreFulfilmentCapabilitiesInterceptor getInstance() {
        return interceptor;
    }

    @Override
    public void onValidate(final Object model, final InterceptorContext ctx) throws InterceptorException {
        // Do nothing, no validation done during automation test.
    }
}
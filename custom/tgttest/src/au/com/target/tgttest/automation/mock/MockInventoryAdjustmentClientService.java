/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgtwebmethods.inventory.dto.request.InventoryAdjustmentRequestDto;
import au.com.target.tgtwebmethods.inventory.dto.response.InventoryAdjustmentResponseDto;
import au.com.target.tgtwebmethods.stock.client.impl.TargetInventoryAdjustmentClientImpl;


/**
 * Set up a mock MockInventoryAdjustmentClientService which captures the transaction.<br/>
 * NOT thread safe.
 * 
 */
public final class MockInventoryAdjustmentClientService {

    private static InventoryAdjustmentRequestDto lastRequestBody;

    private MockInventoryAdjustmentClientService() {
        //Default Constructor
    }

    /**
     * Set the mock client
     * 
     */
    public static void setMock() {

        final TargetInventoryAdjustmentClientImpl targetInventoryAdjustmentClient = ServiceLookup
                .getTargetInventoryAdjustmentClient();


        targetInventoryAdjustmentClient.setRestTemplate(new RestTemplate() {

            @Override
            public <T> ResponseEntity<T> exchange(final String url, final HttpMethod method,
                    final HttpEntity<?> requestEntity, final Class<T> responseType, final Object... uriVariables)
                    throws RestClientException {

                lastRequestBody = (InventoryAdjustmentRequestDto)requestEntity.getBody();
                final InventoryAdjustmentResponseDto response = new InventoryAdjustmentResponseDto();
                response.setResponseCode("0");
                response.setResponseMessage("success");
                return new ResponseEntity(
                        response,
                        HttpStatus.OK);
            }
        });
    }

    public static InventoryAdjustmentRequestDto getLastRequest() {
        return lastRequestBody;
    }

}

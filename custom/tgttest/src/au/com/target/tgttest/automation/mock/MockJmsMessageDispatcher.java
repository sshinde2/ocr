/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jms.JmsException;

import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.gateway.impl.TargetJmsMessageDispatcher;
import au.com.target.tgtjms.jms.message.AbstractMessage;
import au.com.target.tgtjms.jms.service.impl.JmsSendToWarehouseProtocol;
import au.com.target.tgtjms.jms.service.impl.TargetPOSExtractsServiceImpl;
import au.com.target.tgtjms.jms.service.impl.TargetTransactionLogServiceImpl;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Mock JmsMessageDespatcher captures the message.<br/>
 * NOT thread safe.
 * 
 */
public final class MockJmsMessageDispatcher {

    private static Map<String, List<String>> queueMap = new HashMap<String, List<String>>();

    private MockJmsMessageDispatcher() {
        // utility
    }

    /**
     * Set the mock dispatcher to log and capture the message
     * 
     */
    public static void setMock() {

        final JmsMessageDispatcher mockDispatcher = new TargetJmsMessageDispatcher() {

            @Override
            public void send(final String queue, final String message) throws JmsException {

                List<String> msgList = queueMap.get(queue);
                if (msgList == null) {
                    msgList = new ArrayList<>();
                    queueMap.put(queue, msgList);
                }
                msgList.add(message);
            }

            @Override
            public void send(final String queue, final AbstractMessage message) {
                send(queue, message.convertToMessagePayload());
            }

            @Override
            public void sendMessageWithHeaders(final String queue, final AbstractMessage message,
                    final Map<String, String> headers) {
                send(queue, message.convertToMessagePayload());
            }

        };

        setMockForTransactionLogService(mockDispatcher);
        setMockForEsbFulfilmentProcessService(mockDispatcher);
        setMockForCncExtract(mockDispatcher);
    }


    /**
     * @param mockDispatcher
     */
    private static void setMockForTransactionLogService(final JmsMessageDispatcher mockDispatcher) {
        final TargetTransactionLogServiceImpl tlogService = (TargetTransactionLogServiceImpl)ServiceLookup
                .getTargetTransactionLogService();
        tlogService.setMessageDispatcher(mockDispatcher);
    }

    /**
     * @param mockDispatcher
     */
    private static void setMockForEsbFulfilmentProcessService(final JmsMessageDispatcher mockDispatcher) {
        final JmsSendToWarehouseProtocol fulfilmentProcessService = (JmsSendToWarehouseProtocol)ServiceLookup
                .getEsbFulfilmentProcessService();
        fulfilmentProcessService.setMessageDispatcher(mockDispatcher);
    }

    /**
     * setting MessageDispatcher to post the message in the mock queue.
     * 
     * @param mockDispatcher
     */
    private static void setMockForCncExtract(final JmsMessageDispatcher mockDispatcher) {
        final TargetPOSExtractsServiceImpl posExtractService = (TargetPOSExtractsServiceImpl)ServiceLookup
                .getTargetPosExtractService();
        posExtractService.setMessageDispatcher(mockDispatcher);
    }

    /**
     * Get the last jms message from the given queue.
     * 
     * @param queue
     * @return the last jms message from queue
     */
    public static String getMessage(final String queue) {

        if (queueMap.get(queue) == null) {
            return null;
        }

        return queueMap.get(queue).remove(0);
    }

    public static List<String> getAllMessages(final String queue) {
        if (queueMap.get(queue) == null) {
            return null;
        }
        return queueMap.get(queue);
    }

    public static void clean() {
        queueMap = new HashMap<String, List<String>>();
    }

}

/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import javax.xml.bind.JAXBException;

import au.com.target.tgtjms.jms.converter.TLOGMessageConverter;
import au.com.target.tgtjms.jms.service.impl.TargetTransactionLogServiceImpl;
import au.com.target.tgtsale.tlog.data.TLog;
import au.com.target.tgtsale.tlog.data.Transaction;
import au.com.target.tgttest.automation.ServiceLookup;


/**
 * Set up a mock TLOGMessageConverter which captures the transaction.<br/>
 * NOT thread safe.
 * 
 */
public final class MockTLOGMessageConverter {

    private static Transaction transaction;

    private MockTLOGMessageConverter() {
        // util
    }

    /**
     * Set the mock converter
     * 
     * @throws JAXBException
     * 
     */
    public static void setMock() throws JAXBException {

        // Mock TLOGMessageConverter captures the transaction
        final TLOGMessageConverter converter = new TLOGMessageConverter() {

            @Override
            public String getMessagePayload(final TLog tlog) {
                transaction = tlog.getTransactions().get(0);

                return "dummy";
            }
        };

        final TargetTransactionLogServiceImpl tlogService = (TargetTransactionLogServiceImpl)ServiceLookup
                .getTargetTransactionLogService();
        tlogService.setTlogMessageConverter(converter);
    }

    /**
     * Retrieve the last transaction converted.
     * 
     * @return transaction
     */
    public static Transaction getTransaction() {
        return transaction;
    }

    /**
     * Clear captured transaction
     */
    public static void clear() {
        transaction = null;
    }

}

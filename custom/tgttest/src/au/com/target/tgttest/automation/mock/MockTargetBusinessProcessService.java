/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.impl.TargetBusinessProcessServiceImpl;


/**
 * This mock when enabled will disable the placeOrderProcess business process
 * 
 * @author sbryan6
 *
 */
public class MockTargetBusinessProcessService extends TargetBusinessProcessServiceImpl {

    private static final Logger LOG = Logger.getLogger(MockTargetBusinessProcessService.class.getName());

    private boolean active = false;


    @Override
    public OrderProcessModel startPlaceOrderProcess(final OrderModel orderModel, final PaymentTransactionEntryModel ptem) {

        if (active) {

            LOG.info("Mock startPlaceOrderProcess for order: " + orderModel.getCode());
            return null;
        }

        return super.startPlaceOrderProcess(orderModel, ptem);
    }

    /**
     * Set this mock active or not
     * 
     * @param active
     */
    public void setActive(final boolean active) {
        this.active = active;
    }

}

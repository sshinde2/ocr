/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgtwebmethods.ca.data.request.UpdateInventoryItemQuantityRequest;
import au.com.target.tgtwebmethods.ca.impl.TargetCaInventoryServiceImpl;


/**
 * @author pradeep a s
 *
 */
public class MockTargetCaInventoryService {

    private static UpdateInventoryItemQuantityRequest updateInventoryItemQuantityReq;

    private MockTargetCaInventoryService() {
        // util
    }

    /**
     * Sets the mock TargetCaShippingService.
     */
    public static void setMock() {

        final TargetCaInventoryServiceImpl mockCaInventoryService = new TargetCaInventoryServiceImpl() {

            @Override
            protected UpdateInventoryItemQuantityRequest createInventoryUpdateRequest(final String productCode,
                    final int quantity) {
                updateInventoryItemQuantityReq =
                        super.createInventoryUpdateRequest(productCode, quantity);
                return updateInventoryItemQuantityReq;
            }

            @Override
            protected boolean processInventoryUpdateRequest(
                    final UpdateInventoryItemQuantityRequest updateInventoryItemQuantityRequest,
                    final String productCode) {
                return true;
            }

        };

        ServiceLookup.getUpdateInventoryToPartnerAction().setTargetCaInventoryService(mockCaInventoryService);
    }

    /**
     * @return the updateInventoryItemQuantityRequest
     */
    public static UpdateInventoryItemQuantityRequest getUpdateInventoryItemQuantityRequest() {
        return updateInventoryItemQuantityReq;
    }

    /**
     * Clear captured shipment request.
     */
    public static void clear() {
        updateInventoryItemQuantityReq = null;
    }


}

/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import au.com.target.tgttest.automation.ServiceLookup;
import au.com.target.tgtwebmethods.ca.data.request.SubmitOrderShipmentRequest;
import au.com.target.tgtwebmethods.ca.impl.TargetCaShippingServiceImpl;


/**
 * @author ragarwa3
 *
 */
public class MockTargetCaShippingService {

    private static SubmitOrderShipmentRequest shipmentRequest;

    private MockTargetCaShippingService() {
        // util
    }

    /**
     * Sets the mock TargetCaShippingService.
     */
    public static void setMock() {

        final TargetCaShippingServiceImpl mockCaShippingService = new TargetCaShippingServiceImpl() {

            @Override
            protected SubmitOrderShipmentRequest createDeliveryOrderShipmentRequest(final OrderModel orderModel) {
                shipmentRequest = super.createDeliveryOrderShipmentRequest(orderModel);
                return shipmentRequest;
            }

            @Override
            protected SubmitOrderShipmentRequest createDeliveryConsignmentShipmentRequest(
                    final ConsignmentModel consignmentModel) {
                shipmentRequest = super.createDeliveryConsignmentShipmentRequest(consignmentModel);
                return shipmentRequest;
            }

            @Override
            protected SubmitOrderShipmentRequest createCncOrderShipmentRequest(
                    final OrderModel orderModel, final String orderStatus) {
                shipmentRequest = super.createCncOrderShipmentRequest(orderModel, orderStatus);
                return shipmentRequest;
            }

            @Override
            protected boolean processOrderShipmentListRequest(
                    final SubmitOrderShipmentRequest submitOrderShipmentListRequest) {
                // return true and do nothing
                return true;
            }

        };

        ServiceLookup.getUpdatePartnerWithShipmentInfoAction().setTargetCaShippingService(mockCaShippingService);
        ServiceLookup.getUpdatePartnerWithReadyForPickedupStatusAction().setTargetCaShippingService(
                mockCaShippingService);
        ServiceLookup.getUpdatePartnerWithPickedupStatusAction().setTargetCaShippingService(mockCaShippingService);

    }

    /**
     * Retrieve the last shipment request.
     * 
     * @return the shipmentRequest
     */
    public static SubmitOrderShipmentRequest getShipmentRequest() {
        return shipmentRequest;
    }

    /**
     * Clear captured shipment request.
     */
    public static void clear() {
        shipmentRequest = null;
    }

}

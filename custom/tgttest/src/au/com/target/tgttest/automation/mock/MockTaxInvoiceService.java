/**
 * 
 */
package au.com.target.tgttest.automation.mock;

import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.core.model.order.OrderModel;

import au.com.target.tgtcore.taxinvoice.TaxInvoiceException;
import au.com.target.tgtcore.taxinvoice.TaxInvoiceService;
import au.com.target.tgtcore.taxinvoice.impl.TaxInvoiceServiceImpl;


/**
 * MockTaxInvoiceService to skip creating a tax invoice.
 * 
 * @author jjayawa1
 *
 */
public class MockTaxInvoiceService extends TaxInvoiceServiceImpl {


    private static TaxInvoiceService taxInvoiceService = new MockTaxInvoiceService();

    public static TaxInvoiceService getInstance() {
        return taxInvoiceService;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.taxinvoice.impl.TaxInvoiceServiceImpl#generateTaxInvoiceForOrder(de.hybris.platform.core.model.order.OrderModel)
     */
    @Override
    public DocumentModel generateTaxInvoiceForOrder(final OrderModel orderModel) throws TaxInvoiceException {
        return null;
    }

}

/**
 * 
 */
package au.com.target.tgttest.csvgen;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Required;


/**
 * Utility to generate csv files from flexible search queries
 * 
 */
public class CsvGenerator {

    public static final String SEPERATOR = ";";
    public static final String EOL = "\n";

    private FlexibleSearchService flexibleSearchService;
    private ConfigurationService configurationService;


    /**
     * Generate a CSV file from the given query and parameters.<br/>
     * 
     * NOTE this tries to cast all columns to Strings - for example floats will end up like '10.00E0'.<br/>
     * The file will be generated in the configured folder (property tgttest.csvgen.directory)
     * 
     * @param header
     *            header row with column-headers separated by SEPERATOR
     * @param queryString
     *            the flexible search query
     * @param maxRows
     *            the max number of data rows to generate in the CSV
     * @param filename
     *            the output filename
     * @return CSV content
     */
    public String generateCsvFile(final String header, final String queryString, final int maxRows,
            final String filename) {

        final StringBuilder csv = new StringBuilder();
        csv.append(header).append(EOL);

        final String[] headerItems = header.split(SEPERATOR);

        final List<String> lines = getResults(queryString, maxRows, headerItems.length);

        for (final String line : lines) {
            csv.append(line).append(EOL);
        }

        final File file = getFile(filename);

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file, "UTF-8");
            writer.write(csv.toString());
        }
        catch (final Exception e) {
            throw new IllegalStateException("Exception writing to file: " + filename, e);
        }
        finally {
            IOUtils.closeQuietly(writer);
        }

        return csv.toString();
    }


    private List<String> getResults(final String queryString, final int maxRows, final int numCols) {

        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        final List<Class> types = new ArrayList<>();

        for (int i = 0; i < numCols; i++) {
            types.add(String.class);
        }

        query.setResultClassList(types);

        final SearchResult<List<Object>> searchResult = flexibleSearchService.search(query);
        final List<List<Object>> results = searchResult.getResult();
        final List<String> listLines = new LinkedList<>();

        int count = 0;
        for (final List<Object> row : results) {

            count++;
            if (count > maxRows) {
                break;
            }

            final StringBuilder line = new StringBuilder();
            boolean first = true;

            for (final Object value : row) {

                if (!first) {
                    line.append(SEPERATOR);
                }
                first = false;

                if (value != null) {
                    line.append(value);
                }
            }
            listLines.add(line.toString());
        }

        return listLines;
    }


    private File getFile(final String name) {
        final String outputFolder = configurationService.getConfiguration().getString("tgttest.csvgen.directory");

        return new File(outputFolder + name);
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }


}

/**
 * 
 */
package au.com.target.tgttest.instore;

import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfulfilment.dao.TargetManifestDao;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgttest.automation.facade.bean.ManifestForStore;


/**
 * @author Rahul
 *
 */
public class ManifestCreationHelper {

    @Resource
    private ModelService modelService;

    @Resource
    private TargetManifestDao targetManifestDao;

    /**
     * Removes all the existing manifests.
     */
    public void removeExistingManifests() {
        final List<TargetManifestModel> existingManifests = targetManifestDao.find();
        modelService.removeAll(existingManifests);
    }

    /**
     * Creates the manifests for store.
     *
     * @param manifests
     *            the manifests
     */
    public void createManifestsForStore(final List<ManifestForStore> manifests) {
        if (CollectionUtils.isNotEmpty(manifests)) {
            TargetManifestModel manifestModel;
            for (final ManifestForStore manifest : manifests) {
                try {
                    manifestModel = targetManifestDao.getManifestByCode(manifest.getManifestID());
                }
                catch (final TargetUnknownIdentifierException | TargetAmbiguousIdentifierException e) {
                    manifestModel = modelService.create(TargetManifestModel.class);
                }
                manifestModel.setSent(manifest.isTransmitted());
                manifestModel.setCode(manifest.getManifestID());
                manifestModel.setDate(getDateObject(manifest.getManifestDate(), manifest.getManifestTime()));
                modelService.save(manifestModel);
            }
        }
    }

    /**
     * Gets the date ojbect.
     *
     * @param manifestDate
     *            the manifest date
     * @param manifestTime
     *            the manifest time
     * @return the date ojbect
     */
    public Date getDateObject(final String manifestDate, final String manifestTime) {
        final Calendar calendar = Calendar.getInstance();
        int daysToAdd = 0;

        if ("today".equalsIgnoreCase(manifestDate)) {
            daysToAdd = 0;
        }
        else if ("yesterday".equalsIgnoreCase(manifestDate)) {
            daysToAdd = -1;
        }
        else {
            final String[] splittedDate = manifestDate.split("days", 2);
            if ("before".equalsIgnoreCase(splittedDate[1].trim())) {
                daysToAdd = -(Integer.valueOf(splittedDate[0].trim()).intValue());
            }
        }

        if (StringUtils.isNotBlank(manifestTime)) {
            final String[] splittedTime = manifestTime.split(":", 2);
            calendar.set(Calendar.HOUR, Integer.valueOf(splittedTime[0].trim()).intValue());
            calendar.set(Calendar.MINUTE, Integer.valueOf(splittedTime[1].trim()).intValue());
        }

        calendar.set(Calendar.SECOND, 0);
        return DateUtils.addDays(calendar.getTime(), daysToAdd);
    }

}

/**
 * 
 */
package au.com.target.tgttest.order.impl;

import au.com.target.tgtebay.order.impl.TargetPartnerOrderServiceImpl;
import au.com.target.tgttest.constants.TgttestConstants;


/**
 * @author ragarwa3
 *
 */
public class TestTargetPartnerOrderServiceImpl extends TargetPartnerOrderServiceImpl {

    private boolean active;

    @Override
    protected String generateCartOrderNumber() {
        String cartOrderNumber = super.generateCartOrderNumber();

        if (active) {
            cartOrderNumber = TgttestConstants.ORDER_PREFIX + cartOrderNumber;
        }

        return cartOrderNumber;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(final boolean active) {
        this.active = active;
    }

}

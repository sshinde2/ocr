/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


@IntegrationTest
@Ignore
//test is ignored for now until we find a way to run end to end bus proc on junit tenant
public class RetryActionIntTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    @Resource
    private BusinessProcessService businessProcessService;

    @SuppressWarnings("boxing")
    @Test
    public void testRetryLaterException() throws RetryLaterException, Exception {
        final BusinessProcessModel businessProcess = businessProcessService.createProcess("testProcess", "testProcess");
        businessProcessService.startProcess(businessProcess);

        while (ProcessState.RUNNING.equals(businessProcess.getState())) {
            Thread.sleep(1000);
        }

        Assert.assertEquals(2, businessProcess.getTaskLogs().size());
    }
}

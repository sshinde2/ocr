/**
 * 
 */
package au.com.target.tgtbusproc.actions;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtbusproc.util.ProcessParameterTestUtil;


@IntegrationTest
public class SendEmailActionIntTest extends ServicelayerTransactionalTest {

    @Resource
    private SendEmailAction sendOrderConfEmailTestAction;

    @Resource
    private ModelService modelService;

    @Resource
    private BusinessProcessService businessProcessService;

    @Test
    public void testSendEmailActionSuccess() throws RetryLaterException, Exception {

        final OrderModel order = ProcessParameterTestUtil.createOrderForCode("testOrder", modelService);

        final OrderProcessModel opm = ProcessParameterTestUtil.createProcess(order, "process1", modelService,
                businessProcessService);

        final Transition trans = sendOrderConfEmailTestAction.executeAction(opm);
        Assert.assertEquals(Transition.OK, trans);
    }



}

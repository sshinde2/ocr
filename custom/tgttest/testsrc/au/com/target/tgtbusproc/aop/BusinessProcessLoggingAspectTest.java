/**
 * 
 */
package au.com.target.tgtbusproc.aop;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import au.com.target.tgtbusproc.actions.DummyProcessAction;


@IntegrationTest
public class BusinessProcessLoggingAspectTest extends ServicelayerTransactionalTest {

    @Resource
    private DummyProcessAction dummyProcessAction;

    private OrderProcessModel orderProcessModel;

    @Before
    public void setup() {
        orderProcessModel = new OrderProcessModel();
        orderProcessModel.setCode("process-001");

        final OrderModel order = new OrderModel();
        order.setCode("order-001");

        orderProcessModel.setOrder(order);
    }

    @Test
    @Ignore
    // this is for testing exception senario, please turn this on only in local dev
    // also uncomment the code in DummyProcessAction
    public void testAfterThrowing() throws RetryLaterException, Exception {
        dummyProcessAction.execute(orderProcessModel);
    }

    @Test
    public void testBeforeAction() throws RetryLaterException, Exception {
        dummyProcessAction.execute(orderProcessModel);
    }
}

/**
 * 
 */
package au.com.target.tgtbusproc.util;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.CancelReason;
import de.hybris.platform.basecommerce.enums.OrderModificationEntryStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordEntryModel;
import de.hybris.platform.ordercancel.model.OrderCancelRecordModel;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.ordermodify.model.OrderModificationRecordModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.Date;

import javax.annotation.Resource;

import org.junit.Test;


@IntegrationTest
public class OrderProcessParameterHelperIntTest extends ServicelayerTransactionalTest {

    @Resource
    private OrderProcessParameterHelper orderProcessParameterHelper;

    @Resource
    private ModelService modelService;

    @Resource
    private OrderHistoryService orderHistoryService;

    @Resource
    private CatalogVersionService catalogVersionService;

    @Resource
    private BusinessProcessService businessProcessService;

    @Test
    public void testOrderProcessParameterHelperForRefundAmount() {

        final OrderModel order = ProcessParameterTestUtil.createOrderForCode("testOrder", modelService);
        final OrderProcessModel process = ProcessParameterTestUtil.createProcess(order, "TestProcess", modelService,
                businessProcessService);

        final BigDecimal refundAmount = BigDecimal.valueOf(7);
        orderProcessParameterHelper.setRefundAmount(process, refundAmount);

        assertThat(orderProcessParameterHelper.getRefundAmount(process)).isNotNull();
        assertThat(refundAmount).isEqualTo(orderProcessParameterHelper.getRefundAmount(process));
    }

    @Test
    public void testOrderProcessParameterHelperForOrderCancelRequest() {

        final OrderModel order = ProcessParameterTestUtil.createOrderForCode("testOrder", modelService);
        final OrderProcessModel process = ProcessParameterTestUtil.createProcess(order, "TestProcess", modelService,
                businessProcessService);

        ProcessParameterTestUtil.createHistorySnapshot(order, "cancelled", orderHistoryService, modelService);

        final OrderModificationRecordModel modRecord = new OrderCancelRecordModel();
        modRecord.setOrder(order);
        modRecord.setIdentifier("123");
        modelService.save(modRecord);

        final OrderCancelRecordEntryModel orderCancelRequest = new OrderCancelRecordEntryModel();
        orderCancelRequest.setCode("cancelRequest");
        orderCancelRequest.setCancelReason(CancelReason.CUSTOMERREQUEST);
        orderCancelRequest.setCreationtime(new Date());
        orderCancelRequest.setNotes("Dont want it any more");
        orderCancelRequest.setOriginalVersion(order.getHistoryEntries().get(0));
        orderCancelRequest.setModificationRecord(modRecord);
        orderCancelRequest.setTimestamp(new Date());
        orderCancelRequest.setStatus(OrderModificationEntryStatus.SUCCESSFULL);
        modelService.save(orderCancelRequest);

        orderProcessParameterHelper.setOrderCancelRequest(process, orderCancelRequest);

        final OrderCancelRecordEntryModel retrieved = orderProcessParameterHelper.getOrderCancelRequest(process);
        assertThat(CancelReason.CUSTOMERREQUEST).isEqualTo(retrieved.getCancelReason());
        assertThat("Dont want it any more").isEqualTo(retrieved.getNotes());
    }

    @Test
    public void testOrderProcessParameterHelperSetTwice() {

        final OrderModel order = ProcessParameterTestUtil.createOrderForCode("testOrder", modelService);
        final OrderProcessModel process = ProcessParameterTestUtil.createProcess(order, "TestProcess", modelService,
                businessProcessService);

        final BigDecimal refundAmount = BigDecimal.valueOf(7);
        orderProcessParameterHelper.setRefundAmount(process, refundAmount);
        assertThat(refundAmount).isEqualTo(orderProcessParameterHelper.getRefundAmount(process));

        final BigDecimal refundAmount2 = BigDecimal.valueOf(8);
        orderProcessParameterHelper.setRefundAmount(process, refundAmount2);
        assertThat(refundAmount2).isEqualTo(orderProcessParameterHelper.getRefundAmount(process));

    }

    @Test
    public void testPartnerUpdateRequired() {
        final OrderModel order = ProcessParameterTestUtil.createOrderForCode("testOrder", modelService);
        final OrderProcessModel process = ProcessParameterTestUtil.createProcess(order, "TestProcess", modelService,
                businessProcessService);

        orderProcessParameterHelper.setPartnerUpdateRequired(process, true);

        assertThat(Boolean.valueOf(orderProcessParameterHelper.isPartnerUpdateRequired(process))).isNotNull();
        assertThat(orderProcessParameterHelper.isPartnerUpdateRequired(process)).isTrue();
    }
}

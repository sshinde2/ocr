/**
 * 
 */
package au.com.target.tgtcore.actions;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.PaymentModeService;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtbusproc.util.ProcessParameterTestUtil;
import au.com.target.tgtcore.actions.FlybuysConsumeAction.Transition;
import au.com.target.tgtcore.flybuys.service.FlybuysDiscountService;

import com.google.common.base.Charsets;


/**
 * The Class FlybuysConsumeActionIntTest.
 */
@IntegrationTest
public class FlybuysConsumeActionIntTest extends ServicelayerTransactionalTest {

    @Resource
    private FlybuysConsumeAction flybuysConsumeAction;

    @Resource
    private ModelService modelService;

    @Resource
    private PaymentModeService paymentModeService;

    @Resource
    private BusinessProcessService businessProcessService;

    @Resource
    private FlybuysDiscountService flybuysDiscountService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        flybuysConsumeAction.setFlybuysDiscountService(flybuysDiscountService);
    }

    @Test
    public void testFlybuysConsumeAction() throws RetryLaterException, Exception {

        // Just check that the action runs without errors

        final PaymentModeModel paymentModeModel = paymentModeService.getPaymentModeForCode("creditcard");
        final OrderModel order = ProcessParameterTestUtil.createOrderForCode("testOrder", modelService);
        ProcessParameterTestUtil.setPaymentMethodToOrder(order, paymentModeModel, modelService);

        final OrderProcessModel opm = ProcessParameterTestUtil.createProcess(order, "process1", modelService,
                businessProcessService);
        final String status = flybuysConsumeAction.execute(opm);

        Assert.assertEquals(Transition.ACCEPT.name(), status);
    }

}

package au.com.target.tgtcore.category.daos.impl;

import static junit.framework.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.constants.CatalogConstants;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserConstants;
import de.hybris.platform.servicelayer.user.daos.UserDao;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import au.com.target.tgtcore.category.daos.TargetCategoryDao;
import au.com.target.tgtcore.model.TargetProductCategoryModel;


/**
 * Test suite for {@link TargetCategoryDaoImpl}.
 */
@IntegrationTest
public class TargetCategoryDaoImplTest extends ServicelayerTransactionalTest {

    @Resource(name = "modelService")
    private ModelService modelService;

    @Resource(name = "targetCategoryDao")
    private TargetCategoryDao categoryDao;

    @Resource(name = "userDao")
    private UserDao userDao;

    private Collection<CatalogVersionModel> catalogVersions;
    private List<PrincipalModel> allowedPrincipals;


    /**
     * Initializes test cases before run.
     * 
     */
    @Before
    public void setUp() {
        catalogVersions = ImmutableList.of(
                createCatalogVersion("myCatalog", "Staged"));
        allowedPrincipals = ImmutableList.of(
                (PrincipalModel)userDao.findUserByUID(UserConstants.ANONYMOUS_CUSTOMER_UID));
        JaloSession.getCurrentSession().getSessionContext()
                .setAttribute(CatalogConstants.SESSION_CATALOG_VERSIONS, ImmutableList.copyOf(
                        Iterables.transform(catalogVersions, new Function<CatalogVersionModel, PK>() {
                            @Override
                            public PK apply(final CatalogVersionModel input) {
                                return input.getPk();
                            }
                        })));
    }

    /**
     * Verifies that {@link TargetCategoryDao#findLeafCategoriesWithProducts()} returns categories that belongs to
     * current session catalog version and don't have children.
     */
    @Test
    public void testFindLeafCategoriesWithProducts() {
        final CategoryModel childCategory = createCategory("child", CategoryModel.class);
        createCategory("parent", CategoryModel.class, childCategory);

        assertEquals("Leaf Categories with products", 0, categoryDao.findLeafCategoriesWithProducts().size());
    }

    /**
     * Verifies that {@link TargetCategoryDao#findAllCategories()} returns categories that belongs to current session
     * catalog version.
     */
    @Test
    public void testFindAllCategories() {
        final CategoryModel childCategory = createCategory("child", TargetProductCategoryModel.class);
        createCategory("parent", TargetProductCategoryModel.class, childCategory);

        // 2 categories should now exist, child and parent. TMDStd is not a TargetProductCategoryModel and shouldn't be included.
        assertEquals(2, categoryDao.findAllCategories().size());
    }


    /**
     * Creates a category with given {@code code} and {@code children} properties. It also implicitly sets already
     * initialized catalog version and collection of allowed principals.
     * 
     * @param code
     *            the category code
     * @param children
     *            the category children array
     * @return the newly created category model
     */
    private CategoryModel createCategory(final String code, final Class<? extends CategoryModel> categoryClass,
            final CategoryModel... children) {
        final CategoryModel category = modelService.create(categoryClass);
        category.setCatalogVersion(catalogVersions.iterator().next());
        category.setAllowedPrincipals(allowedPrincipals);
        if (children != null && children.length > 0) {
            category.setCategories(ImmutableList.copyOf(children));
        }
        category.setCode(code);
        modelService.save(category);
        return category;
    }

    /**
     * Creates a new catalog version with given {@code name} and {@code version}.
     * 
     * @param name
     *            the name of the catalog
     * @param version
     *            the version of the catalog
     * @return new instance of catalog version
     */
    private CatalogVersionModel createCatalogVersion(final String name, final String version) {
        final CatalogModel catalog = modelService.create(CatalogModel.class);
        catalog.setId(name);
        modelService.save(catalog);
        final CatalogVersionModel catalogVersion = modelService.create(CatalogVersionModel.class);
        catalogVersion.setCatalog(catalog);
        catalogVersion.setVersion(version);
        modelService.save(catalogVersion);
        return catalogVersion;
    }
}

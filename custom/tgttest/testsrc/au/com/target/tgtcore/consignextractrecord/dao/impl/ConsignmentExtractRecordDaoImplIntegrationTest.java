/**
 * 
 */
package au.com.target.tgtcore.consignextractrecord.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.consignextractrecord.dao.ConsignmentExtractRecordDao;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfulfilment.model.ConsignmentExtractRecordModel;


/**
 * @author pratik
 *
 */
@IntegrationTest
public class ConsignmentExtractRecordDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    private static final String HOME_DELIVERY = "home-delivery";
    private static final String SAMPLE_WAREHOUSE = "SampleWarehouse";

    @Resource
    private ConsignmentExtractRecordDao consignmentExtractRecordDao;

    @Resource
    private ModelService modelService;

    @Resource
    private TargetWarehouseService targetWarehouseService;

    @Resource
    private DeliveryModeService deliveryModeService;

    @Test
    public void testGetConsExtractRecodeModelByConsignmentWhenConsExist() throws ImpExException {
        final TargetConsignmentModel consignment = createSampleConsignment("a007");
        final ConsignmentExtractRecordModel consExtRecModel = createConsignmentExtractRecord(consignment);

        final ConsignmentExtractRecordModel actualConsExtRecModel = consignmentExtractRecordDao
                .getConsExtractRecordByConsignment(consignment);
        Assert.assertNotNull(actualConsExtRecModel);
        Assert.assertEquals(consExtRecModel, actualConsExtRecModel);
    }

    @Test
    public void testGetConsExtractRecodeModelByConsignmentWhenConsDoesntExist() throws ImpExException {
        final TargetConsignmentModel consignment = createSampleConsignment("b007");

        final ConsignmentExtractRecordModel actualConsExtRecModel = consignmentExtractRecordDao
                .getConsExtractRecordByConsignment(consignment);
        Assert.assertNull(actualConsExtRecModel);
    }

    @Test
    public void testSaveConsignmentExtractRecord() throws ImpExException {
        final TargetConsignmentModel consignment = createSampleConsignment("a007");
        consignmentExtractRecordDao.saveConsignmentExtractRecord(consignment, true);

        final ConsignmentExtractRecordModel consExtRecModel = consignmentExtractRecordDao
                .getConsExtractRecordByConsignment(consignment);
        Assert.assertNotNull(consExtRecModel);
        Assert.assertEquals(consExtRecModel.getConsignment(), consignment);
        Assert.assertEquals(consExtRecModel.getConsignmentAsId(), Boolean.TRUE);
    }

    /**
     * Method to create ConsignmentExtractRecordModel
     * 
     * @param consignment
     * @return ConsignmentExtractRecordModel
     */
    private ConsignmentExtractRecordModel createConsignmentExtractRecord(final TargetConsignmentModel consignment) {
        final ConsignmentExtractRecordModel consExtRecModel = modelService.create(ConsignmentExtractRecordModel.class);
        consExtRecModel.setConsignment(consignment);
        consExtRecModel.setConsignmentAsId(Boolean.FALSE);
        modelService.save(consExtRecModel);
        return consExtRecModel;
    }

    /**
     * Method to create sample TargetConsignmentModel
     * 
     * @param code
     * @return TargetConsignmentModel
     * @throws ImpExException
     */
    private TargetConsignmentModel createSampleConsignment(final String code) throws ImpExException {
        createSampleWarehouse();
        final WarehouseModel warehouse = targetWarehouseService.getWarehouseForCode(SAMPLE_WAREHOUSE);
        final AddressModel address = createAddress(warehouse);

        final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
        consignment.setCode(code);
        consignment.setStatus(ConsignmentStatus.CREATED);
        consignment.setWarehouse(warehouse);
        consignment.setShippingAddress(address);
        consignment.setDeliveryMode(deliveryModeService.getDeliveryModeForCode(HOME_DELIVERY));
        modelService.save(consignment);
        return consignment;
    }

    /**
     * Method to create sample WarehouseModel
     * 
     * @throws ImpExException
     */
    private void createSampleWarehouse() throws ImpExException {
        importCsv("/tgtcore/test/testWarehouse.impex", Charsets.UTF_8.name());
    }

    /**
     * Method to create sample AddressModel
     * 
     * @return AddressModel
     */
    private AddressModel createAddress(final WarehouseModel warehouse) {
        final AddressModel address = modelService.create(AddressModel.class);
        address.setOwner(warehouse);
        modelService.save(address);
        return address;
    }

}

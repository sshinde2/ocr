/**
 * 
 */
package au.com.target.tgtcore.currency.conversion.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.currency.conversion.dao.CurrencyConversionFactorDao;
import au.com.target.tgtcore.model.CurrencyConversionFactorsModel;
import au.com.target.tgtebay.constants.TgtebayConstants;
import au.com.target.tgtutility.util.TargetDateUtil;

import com.google.common.base.Charsets;


/**
 * @author Nandini
 *
 */
@IntegrationTest
public class CurrencyConversionFactorDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    @Resource
    private CurrencyConversionFactorDao currencyConversionFactorDao;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    private CurrencyModel aUDCurrency;
    private CurrencyModel nZDCurrency;
    private CurrencyModel uSDCurrency;

    @Before
    public void setUp() throws ImpExException {

        importCsv("/tgtcore/test/testCurrencyConversionFactors.impex", Charsets.UTF_8.name());
        final CurrencyModel example = new CurrencyModel();
        example.setIsocode(TgtebayConstants.CURRENCY_CODE_AUD);
        aUDCurrency = flexibleSearchService.getModelByExample(example);
        example.setIsocode(TgtebayConstants.CURRENCY_CODE_NZD);
        nZDCurrency = flexibleSearchService.getModelByExample(example);
        example.setIsocode("USD");
        uSDCurrency = flexibleSearchService.getModelByExample(example);
    }

    @Test
    public void testFindLatestCurrenyFactorWithNZDToAUD() {
        final CurrencyConversionFactorsModel currencyConversionFactorsModel = currencyConversionFactorDao
                .findLatestCurrencyFactor(TargetDateUtil.getStringAsDate("2016-01-06T11:10:00"), nZDCurrency,
                        aUDCurrency);
        Assert.assertNotNull(currencyConversionFactorsModel.getCurrencyConversionFactor());
        Assert.assertEquals(Double.valueOf(0.934271), currencyConversionFactorsModel.getCurrencyConversionFactor());
    }

    @Test
    public void testFindLatestCurrenyFactorWithUSDToAUD() {
        final CurrencyConversionFactorsModel currencyConversionFactorsModel = currencyConversionFactorDao
                .findLatestCurrencyFactor(TargetDateUtil.getStringAsDate("2016-01-06T11:10:00"), uSDCurrency,
                        aUDCurrency);
        Assert.assertNotNull(currencyConversionFactorsModel.getCurrencyConversionFactor());
        Assert.assertEquals(Double.valueOf(1.40750), currencyConversionFactorsModel.getCurrencyConversionFactor());
    }
}

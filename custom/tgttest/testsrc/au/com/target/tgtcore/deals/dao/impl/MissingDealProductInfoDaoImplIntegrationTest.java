package au.com.target.tgtcore.deals.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.deals.dao.MissingDealProductInfoDao;
import au.com.target.tgtcore.model.MissingDealProductInfoModel;


/**
 * Integration test for {@link MissingDealProductInfoDaoImpl}
 */
@IntegrationTest
public class MissingDealProductInfoDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private MissingDealProductInfoDao missingDealProductInfoDao;

    @Resource
    private ModelService modelService;

    private AbstractDealModel activeDeal;

    private AbstractDealModel expiredDeal;

    private AbstractDealModel removedDeal;

    @Before
    public void setup() {
        activeDeal = DealsTestHelper.createValueBundleDeal("1234", "9000", 120d);

        final DateTime now = new DateTime();
        expiredDeal = DealsTestHelper.createValueBundleDeal("5678", "7000", 220d, now.minusDays(1).toDate());

        removedDeal = DealsTestHelper.createValueBundleDeal("9999", "5000", 20d);
    }

    @Test
    public void testFindMissingProductsForExpiredDealsAllActive() {
        createMissingDealProductInfo(activeDeal.getCode(), "product1", "dqc1");
        createMissingDealProductInfo(activeDeal.getCode(), "product2", "drc2");

        final List<MissingDealProductInfoModel> missingDealProductsForExpiredDeals = missingDealProductInfoDao
                .findMissingProductsForExpiredDeals();

        Assert.assertTrue(missingDealProductsForExpiredDeals.isEmpty());
    }

    @Test
    public void testFindMissingProductsForExpiredDealsAllInActive() {
        final MissingDealProductInfoModel missingDealProductInfo1 = createMissingDealProductInfo(expiredDeal.getCode(),
                "product1", "dqc1");
        final MissingDealProductInfoModel missingDealProductInfo2 = createMissingDealProductInfo(expiredDeal.getCode(),
                "product2", "drc2");

        final List<MissingDealProductInfoModel> missingDealProductsForExpiredDeals = missingDealProductInfoDao
                .findMissingProductsForExpiredDeals();

        Assert.assertFalse(missingDealProductsForExpiredDeals.isEmpty());
        Assert.assertEquals(2, missingDealProductsForExpiredDeals.size());
        Assert.assertTrue(missingDealProductsForExpiredDeals.containsAll(Arrays.asList(missingDealProductInfo1,
                missingDealProductInfo2)));
    }

    @Test
    public void testFindMissingProductsForExpiredDealsForRemovedDeals() {
        final MissingDealProductInfoModel missingDealProductInfo1 = createMissingDealProductInfo(removedDeal.getCode(),
                "product1", "dqc1");
        final MissingDealProductInfoModel missingDealProductInfo2 = createMissingDealProductInfo(removedDeal.getCode(),
                "product2", "drc2");

        // remove the deal
        DealsTestHelper.removeDeal(removedDeal);

        final List<MissingDealProductInfoModel> missingDealProductsForExpiredDeals = missingDealProductInfoDao
                .findMissingProductsForExpiredDeals();

        Assert.assertFalse(missingDealProductsForExpiredDeals.isEmpty());
        Assert.assertEquals(2, missingDealProductsForExpiredDeals.size());
        Assert.assertTrue(missingDealProductsForExpiredDeals.containsAll(Arrays.asList(missingDealProductInfo1,
                missingDealProductInfo2)));
    }

    private MissingDealProductInfoModel createMissingDealProductInfo(final String dealId,
            final String productCode, final String dealCategoryCode) {
        final MissingDealProductInfoModel missingDealProductInfo = modelService
                .create(MissingDealProductInfoModel.class);
        missingDealProductInfo.setDealId(dealId);
        missingDealProductInfo.setDealCategoryCode(dealCategoryCode);
        missingDealProductInfo.setProductCode(productCode);
        modelService.save(missingDealProductInfo);
        return missingDealProductInfo;
    }

}

package au.com.target.tgtcore.deals.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.promotions.model.AbstractDealModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtcore.deals.dao.TargetDealDao;


/**
 * Integration test for {@link TargetDealDaoImpl}
 */
@IntegrationTest
public class TargetDealDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Resource
    private ModelService modelService;

    @Resource
    private TargetDealDao targetDealDao;

    @Test
    public void testGetDealForNullId() {
        expectedException.expect(IllegalArgumentException.class);
        targetDealDao.getDealForId(null);
    }

    @Test
    public void testGetDealForInvalidId() {
        DealsTestHelper.createValueBundleDeal("5467", "7009", 350d);

        expectedException.expect(UnknownIdentifierException.class);

        targetDealDao.getDealForId("0000");
    }

    @Test
    public void testGetDealForValidId() {

        DealsTestHelper.createValueBundleDeal("5467", "7009", 350d);

        final AbstractDealModel deal = targetDealDao.getDealForId("5467");
        Assert.assertEquals("5467", deal.getCode());
        Assert.assertEquals("7009", deal.getEventId());
    }

    @Test
    public void testGetAllActiveDeals() {

        final DateTime now = new DateTime();

        // deal with no end date
        final AbstractDealModel dealWithNoEndDate = DealsTestHelper.createValueBundleDeal("5467", "7009", 150d);

        // deal with end date in the past
        DealsTestHelper.createValueBundleDeal("5468", "7009", 250d, now
                .minusDays(1).toDate());

        // deal with end date in the future
        final AbstractDealModel dealExpiringTomorrow = DealsTestHelper.createValueBundleDeal("5469", "7009", 350d, now
                .plusDays(1).toDate());

        // deal ending today
        final AbstractDealModel dealExpiringToday = DealsTestHelper.createValueBundleDeal("5470", "7009", 450d, now
                .plusHours(1).toDate());

        final List<AbstractDealModel> activeDeals = targetDealDao.getAllActiveDeals();

        Assert.assertNotNull(activeDeals);
        Assert.assertEquals(3, activeDeals.size());
        Assert.assertTrue(activeDeals.containsAll(Arrays.asList(dealWithNoEndDate, dealExpiringTomorrow,
                dealExpiringToday)));
    }

    @Test
    public void testGetAllActiveDealCodes() {

        final DateTime now = new DateTime();

        // deal with no end date
        DealsTestHelper.createValueBundleDeal("5467", "7009", 150d);

        // deal with end date in the past
        DealsTestHelper.createValueBundleDeal("5468", "7009", 250d, now
                .minusDays(1).toDate());

        // deal with end date in the future
        DealsTestHelper.createValueBundleDeal("5469", "7009", 350d, now
                .plusDays(1).toDate());

        // deal ending today
        DealsTestHelper.createValueBundleDeal("5470", "7009", 450d, now
                .plusHours(1).toDate());

        final List<String> activeDeals = targetDealDao.getAllActiveDealCodes();

        Assert.assertNotNull(activeDeals);
        Assert.assertEquals(3, activeDeals.size());
        Assert.assertTrue(activeDeals.containsAll(Arrays.asList("5467", "5469", "5470")));
    }





}

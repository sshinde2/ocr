/**
 * 
 */
package au.com.target.tgtcore.delivery;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.ZoneDeliveryModeService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.daos.CountryDao;
import de.hybris.platform.servicelayer.i18n.daos.CurrencyDao;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.product.TargetProductService;

import com.google.common.base.Charsets;


/**
 * The Class TargetZoneDeliveryModeDaoImplTest.
 * 
 * @author rsamuel3
 */
@IntegrationTest
public class TargetZoneDeliveryModeDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    @Resource
    private TargetZoneDeliveryModeDao targetZoneDeliveryModeDao;

    @Resource(name = "targetProductService")
    private TargetProductService targetProductService;

    @Resource
    private TargetDeliveryService targetDeliveryService;

    @Resource
    private ZoneDeliveryModeService zoneDeliveryModeService;

    @Resource
    private CurrencyDao currencyDao;

    @Resource
    private CountryDao countryDao;

    private TargetZoneDeliveryModeModel clickAndCollect = null;
    private TargetZoneDeliveryModeModel homeDelivery = null;
    private TargetZoneDeliveryModeModel expressDelivery = null;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testMultipleProducts.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testDeliveryModeValues.impex", Charsets.UTF_8.name());

        homeDelivery = (TargetZoneDeliveryModeModel)targetDeliveryService.getDeliveryModeForCode("home-delivery");
        setDisplayOrderOnDeliveryMode(homeDelivery, 1, true);

        clickAndCollect = (TargetZoneDeliveryModeModel)targetDeliveryService
                .getDeliveryModeForCode("click-and-collect");
        setDisplayOrderOnDeliveryMode(clickAndCollect, 2, true);

        expressDelivery = (TargetZoneDeliveryModeModel)targetDeliveryService.getDeliveryModeForCode("express-delivery");
        setDisplayOrderOnDeliveryMode(expressDelivery, 3, false);
    }

    @Test
    public void testFilteringInactiveDeliveryModes() {
        final Collection<TargetZoneDeliveryModeModel> availableDeliveryModes = targetZoneDeliveryModeDao
                .findAllCurrentlyActiveDeliveryModes(SalesApplication.WEB);
        assertDeliveryModes(availableDeliveryModes);
    }

    @Test
    public void testFilteringDeliveryModesForSalesChannels() {
        final Collection<TargetZoneDeliveryModeModel> availableDeliveryModesForWeb = targetZoneDeliveryModeDao
                .findAllCurrentlyActiveDeliveryModes(SalesApplication.WEB);
        final Collection<TargetZoneDeliveryModeModel> availableDeliveryModesForKiosk = targetZoneDeliveryModeDao
                .findAllCurrentlyActiveDeliveryModes(SalesApplication.KIOSK);
        assertDeliveryModes(availableDeliveryModesForWeb);
        assertDeliveryModes(availableDeliveryModesForKiosk);
    }

    @Test
    public void testGetDefaultDeliveryMode() {
        final TargetZoneDeliveryModeModel defaultDeliveryMode = targetZoneDeliveryModeDao.getDefaultDeliveryMode();
        Assert.assertNotNull(defaultDeliveryMode);
        Assert.assertEquals("home-delivery", defaultDeliveryMode.getCode());
    }

    @Test
    public void testActiveDeliveryModesForProducts() throws ImpExException {
        final ProductModel product = targetProductService.getProductForCode("P5000");
        final Set<DeliveryModeModel> deliveryModesForProduct = new HashSet(product.getDeliveryModes());
        deliveryModesForProduct.add(expressDelivery);
        deliveryModesForProduct.add(homeDelivery);
        product.setDeliveryModes(deliveryModesForProduct);
        modelService.save(product);

        final Collection<TargetZoneDeliveryModeModel> deliveryModes = targetZoneDeliveryModeDao
                .findAllCurrentlyActiveDeliveryModesForProduct(product);
        assertDeliveryModes(deliveryModes);
    }

    @Test
    public void testActiveDeliveryModesForNullSalesApplication() throws ImpExException {
        final Collection<TargetZoneDeliveryModeModel> allAvailableDeliveryModes = targetZoneDeliveryModeDao
                .findAllCurrentlyActiveDeliveryModes(null);
        final Collection<TargetZoneDeliveryModeModel> deliveryModesForSelectedChannel = targetZoneDeliveryModeDao
                .findAllCurrentlyActiveDeliveryModes(SalesApplication.WEB);
        Assert.assertEquals(6, allAvailableDeliveryModes.size());
        Assert.assertTrue(allAvailableDeliveryModes.size() >= deliveryModesForSelectedChannel.size());
    }

    /**
     * Sets the display order on delivery mode.
     * 
     * @param deliveryMode
     *            the delivery mode
     * @param order
     *            the order
     * @param active
     *            the active
     */
    private void setDisplayOrderOnDeliveryMode(final TargetZoneDeliveryModeModel deliveryMode, final int order,
            final boolean active) {
        deliveryMode.setDisplayOrder(Integer.valueOf(order));
        deliveryMode.setActive(Boolean.valueOf(active));
        modelService.save(deliveryMode);
    }

    /**
     * Assert delivery modes.
     * 
     * @param availableDeliveryModes
     *            the available delivery modes
     */
    private void assertDeliveryModes(final Collection<TargetZoneDeliveryModeModel> availableDeliveryModes) {
        Assertions.assertThat(availableDeliveryModes).isNotNull().isNotEmpty().hasSize(2);
        final Object[] array = availableDeliveryModes.toArray();

        Assertions.assertThat(((TargetZoneDeliveryModeModel)array[0]).getCode()).isEqualTo("home-delivery");
        Assertions.assertThat(((TargetZoneDeliveryModeModel)array[1]).getCode()).isEqualTo("click-and-collect");

        for (final DeliveryModeModel deliveryMode : availableDeliveryModes) {
            Assertions.assertThat(deliveryMode).isInstanceOf(TargetZoneDeliveryModeModel.class);

            final TargetZoneDeliveryModeModel targetDeliveryMode = (TargetZoneDeliveryModeModel)deliveryMode;
            final String code = deliveryMode.getCode();

            Assertions.assertThat(code).doesNotContain("express-delivery");

            if (targetDeliveryMode.getDisplayOrder().equals(Integer.valueOf(2))) {
                Assertions.assertThat(code).isEqualTo("click-and-collect");
            }
        }
    }

}

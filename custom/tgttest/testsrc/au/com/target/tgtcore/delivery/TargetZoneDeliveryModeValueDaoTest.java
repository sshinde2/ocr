/**
 * 
 */
package au.com.target.tgtcore.delivery;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.deliveryzone.model.ZoneModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.daos.CountryDao;
import de.hybris.platform.servicelayer.i18n.daos.CurrencyDao;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.AbstractTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.RestrictableTargetZoneDeliveryModeValueModel;
import au.com.target.tgtcore.model.TargetZoneDeliveryModeValueModel;

import com.google.common.base.Charsets;


@IntegrationTest
public class TargetZoneDeliveryModeValueDaoTest extends ServicelayerTransactionalTest {
    @Resource
    private ModelService modelService;

    @Resource
    private TargetZoneDeliveryModeValueDao targetZoneDeliveryModeValueDao;

    @Resource
    private CurrencyDao currencyDao;

    @Resource
    private CountryDao countryDao;

    private CurrencyModel aud;
    private CountryModel au;
    private TargetZoneDeliveryModeValueModel deliveryModeValue;
    private TargetZoneDeliveryModeValueModel deliveryModeValue2;
    private TargetZoneDeliveryModeModel ausPost;
    private RestrictableTargetZoneDeliveryModeValueModel restrictDeliveryModeValue;
    private RestrictableTargetZoneDeliveryModeValueModel restrictDeliveryModeValue2;
    private RestrictableTargetZoneDeliveryModeValueModel restrictDeliveryModeValue3;

    private ZoneModel zoneModel;
    private Date fromDate;
    private Date toDate;

    @Before
    public void setup() throws ParseException, ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());

        au = countryDao.findCountriesByCode("AU").get(0);
        aud = currencyDao.findCurrenciesByCode("AUD").get(0);

        zoneModel = modelService.create(ZoneModel.class);
        zoneModel.setCode("test_australia");
        zoneModel.setCountries(Collections.singleton(au));
        modelService.save(zoneModel);

        ausPost = modelService.create(TargetZoneDeliveryModeModel.class);
        ausPost.setNet(Boolean.FALSE);
        ausPost.setCode("test_ausPost");
        ausPost.setName("Home Delivery");
        ausPost.setDescription("Australia Post Home Delivery");
        ausPost.setActive(Boolean.TRUE);
        ausPost.setIsDeliveryToStore(Boolean.FALSE);
        ausPost.setDisplayOrder(Integer.valueOf(1));
        ausPost.setDeliveryPromotionRank(Integer.valueOf(10));
        modelService.save(ausPost);

        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        fromDate = sdf.parse("02/07/2013 00:00:00");
        toDate = sdf.parse("02/07/2100 00:00:00");

        deliveryModeValue = modelService.create(TargetZoneDeliveryModeValueModel.class);
        deliveryModeValue.setCurrency(aud);
        deliveryModeValue.setDeliveryMode(ausPost);
        deliveryModeValue.setZone(zoneModel);
        deliveryModeValue.setMinimum(Double.valueOf(0.0d));
        deliveryModeValue.setValue(Double.valueOf(9.0d));
        deliveryModeValue.setBulky(Double.valueOf(49.0d));
        deliveryModeValue.setExtendedBulky(Double.valueOf(98.0d));
        deliveryModeValue.setValidFrom(fromDate);
        deliveryModeValue.setValidTo(toDate);
        deliveryModeValue.setPriority(Integer.valueOf(10));
        modelService.save(deliveryModeValue);

        deliveryModeValue2 = modelService.create(TargetZoneDeliveryModeValueModel.class);
        deliveryModeValue2.setCurrency(aud);
        deliveryModeValue2.setDeliveryMode(ausPost);
        deliveryModeValue2.setZone(zoneModel);
        deliveryModeValue2.setMinimum(Double.valueOf(500.0d));
        deliveryModeValue2.setValue(Double.valueOf(9.0d));
        deliveryModeValue2.setBulky(Double.valueOf(49.0d));
        deliveryModeValue2.setExtendedBulky(Double.valueOf(98.0d));
        deliveryModeValue2.setValidFrom(fromDate);
        deliveryModeValue2.setValidTo(toDate);
        deliveryModeValue2.setPriority(Integer.valueOf(20));
        modelService.save(deliveryModeValue2);

        restrictDeliveryModeValue = modelService.create(RestrictableTargetZoneDeliveryModeValueModel.class);
        restrictDeliveryModeValue.setCurrency(aud);
        restrictDeliveryModeValue.setDeliveryMode(ausPost);
        restrictDeliveryModeValue.setZone(zoneModel);
        restrictDeliveryModeValue.setMinimum(Double.valueOf(0.0d));
        restrictDeliveryModeValue.setValue(Double.valueOf(9.0d));
        restrictDeliveryModeValue.setPriority(Integer.valueOf(15));
        modelService.save(restrictDeliveryModeValue);

        restrictDeliveryModeValue2 = modelService.create(RestrictableTargetZoneDeliveryModeValueModel.class);
        restrictDeliveryModeValue2.setCurrency(aud);
        restrictDeliveryModeValue2.setDeliveryMode(ausPost);
        restrictDeliveryModeValue2.setZone(zoneModel);
        restrictDeliveryModeValue2.setMinimum(Double.valueOf(200.0d));
        restrictDeliveryModeValue2.setValue(Double.valueOf(9.0d));
        restrictDeliveryModeValue2.setPriority(Integer.valueOf(2000));
        modelService.save(restrictDeliveryModeValue2);

        restrictDeliveryModeValue3 = modelService.create(RestrictableTargetZoneDeliveryModeValueModel.class);
        restrictDeliveryModeValue3.setCurrency(aud);
        restrictDeliveryModeValue3.setDeliveryMode(ausPost);
        restrictDeliveryModeValue3.setZone(zoneModel);
        restrictDeliveryModeValue3.setMinimum(Double.valueOf(100.0d));
        restrictDeliveryModeValue3.setValue(Double.valueOf(9.0d));
        restrictDeliveryModeValue3.setPriority(Integer.valueOf(2000));
        modelService.save(restrictDeliveryModeValue3);

    }

    @Test
    public void testFindDeliveryValuesMatchingTargetZoneDeliveryModeValue() {
        final List<AbstractTargetZoneDeliveryModeValueModel> results = targetZoneDeliveryModeValueDao
                .findDeliveryValues(
                        zoneModel, aud, Double.valueOf(100.0), ausPost
                );

        Assert.assertEquals(1, results.size());
    }

    @Test
    public void testFindDeliveryValuesNoMatchingTargetZoneDeliveryModeValue() {
        final List<AbstractTargetZoneDeliveryModeValueModel> results = targetZoneDeliveryModeValueDao
                .findDeliveryValues(
                        zoneModel, aud, Double.valueOf(10.0), ausPost
                );

        Assert.assertEquals(0, results.size());
    }

    @Test
    public void testFindDeliveryValuesSorted() {
        final List<AbstractTargetZoneDeliveryModeValueModel> results = targetZoneDeliveryModeValueDao
                .findDeliveryValuesSortedByPriority(ausPost);

        Assert.assertEquals(5, results.size());
        Assert.assertEquals(restrictDeliveryModeValue2, results.get(0));
        Assert.assertEquals(restrictDeliveryModeValue3, results.get(1));
        Assert.assertEquals(deliveryModeValue2, results.get(2));
        Assert.assertEquals(restrictDeliveryModeValue, results.get(3));
        Assert.assertEquals(deliveryModeValue, results.get(4));
    }

    @Test
    public void testFindDeliveryValuesSortedForProduct() {
        final List<AbstractTargetZoneDeliveryModeValueModel> results = targetZoneDeliveryModeValueDao
                .findDeliveryValuesSortedByPriorityForProduct(ausPost);

        Assert.assertEquals(5, results.size());
        Assert.assertEquals(restrictDeliveryModeValue, results.get(0));
        Assert.assertEquals(deliveryModeValue, results.get(1));
        Assert.assertEquals(restrictDeliveryModeValue2, results.get(2));
        Assert.assertEquals(restrictDeliveryModeValue3, results.get(3));
        Assert.assertEquals(deliveryModeValue2, results.get(4));
    }
}

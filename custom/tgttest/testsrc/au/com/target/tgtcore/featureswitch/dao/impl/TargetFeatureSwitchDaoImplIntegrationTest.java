/**
 * 
 */
package au.com.target.tgtcore.featureswitch.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.featureswitch.dao.TargetFeatureSwitchDao;
import au.com.target.tgtcore.model.TargetFeatureSwitchModel;


/**
 * Integration test for TargetFeatureSwitchDao
 * 
 * @author pratik
 *
 */
@IntegrationTest
public class TargetFeatureSwitchDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    private static final String FEATURE_TNS = "TNS";
    private static final String FEATURE_SINGLE_PAGE_CHECKOUT = "SinglePageCheckout";

    @Resource
    private TargetFeatureSwitchDao targetFeatureSwitchDao;

    @Resource
    private ModelService modelService;

    private List<TargetFeatureSwitchModel> switches;

    @Before
    public void setUp() {
        switches = new ArrayList<>();
        final TargetFeatureSwitchModel featureTns = modelService.create(TargetFeatureSwitchModel.class);
        featureTns.setName(FEATURE_TNS);
        featureTns.setAttrName(FEATURE_TNS);
        featureTns.setEnabled(Boolean.TRUE);
        featureTns.setUiEnabled(Boolean.TRUE);
        featureTns.setComment("Feature TNS");
        modelService.save(featureTns);
        switches.add(featureTns);
        final TargetFeatureSwitchModel featureSinglePageCheckout = modelService.create(TargetFeatureSwitchModel.class);
        featureSinglePageCheckout.setName(FEATURE_SINGLE_PAGE_CHECKOUT);
        featureSinglePageCheckout.setAttrName(FEATURE_SINGLE_PAGE_CHECKOUT);
        featureSinglePageCheckout.setEnabled(Boolean.FALSE);
        featureSinglePageCheckout.setUiEnabled(Boolean.FALSE);
        featureSinglePageCheckout.setComment("Feature Single page checkout");
        modelService.save(featureSinglePageCheckout);
        switches.add(featureSinglePageCheckout);
    }

    @Test
    public void testGetSwitchForFeatureWhenSwitchDoesntExist() {
        assertThat(targetFeatureSwitchDao.getFeatureByName("someNonExistentFeature")).isNull();
    }

    @Test
    public void testGetSwitchForFeatureWhenSwitchExists() {
        final TargetFeatureSwitchModel featureSwitch = targetFeatureSwitchDao.getFeatureByName(FEATURE_TNS);
        assertThat(featureSwitch).isNotNull();
        assertThat(featureSwitch.getName()).isEqualTo(FEATURE_TNS);
        assertThat(featureSwitch.getEnabled()).isTrue();
        assertThat(featureSwitch.getComment()).isEqualTo("Feature TNS");
    }

    /**
     * This method will verify list of enabled features and does not verify UI enabled features.
     */
    @Test
    public void testGetEnabledFeatures() {
        assertThat(targetFeatureSwitchDao.getEnabledFeatures(false).size())
                .isGreaterThan(0);
    }


    /**
     * This method will verify list of enabled and UI enabled features.
     */
    @Test
    public void testGetUIEnabledFeatures() {
        assertThat(targetFeatureSwitchDao.getEnabledFeatures(true).size())
                .isGreaterThan(0);
    }

    @Test
    public void testGetAllFeatures() {
        final List<TargetFeatureSwitchModel> featureSwitches = targetFeatureSwitchDao.getAllFeatures();
        assertThat(featureSwitches).isNotNull();
        assertThat(featureSwitches).hasSize(2);
        final List<String> expectedFeatures = Arrays.asList(FEATURE_TNS, FEATURE_SINGLE_PAGE_CHECKOUT);
        for (final TargetFeatureSwitchModel feature : featureSwitches) {
            assertThat(expectedFeatures).contains(feature.getAttrName());
        }
    }

}

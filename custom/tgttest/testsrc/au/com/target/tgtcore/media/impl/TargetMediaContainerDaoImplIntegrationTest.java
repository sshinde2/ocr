/**
 * 
 */
package au.com.target.tgtcore.media.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.media.TargetMediaContainerDao;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;


/**
 * @author mgazal
 *
 */
@IntegrationTest
public class TargetMediaContainerDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetMediaContainerDao targetMediaContainerDao;

    @Resource
    private TargetProductImportUtil targetProductImportUtil;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtwsfacades/test/testStepStlImport.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testFindMediaContainersByQualifier() {
        final List<MediaContainerModel> mediaContainers = targetMediaContainerDao
                .findMediaContainersByQualifier("IMG123", targetProductImportUtil.getProductCatalogStagedVersion());
        Assert.assertNotNull(mediaContainers);
        Assert.assertEquals(1, mediaContainers.size());
    }

    @Test
    public void testFindMediaContainersByQualifierMissing() {
        final List<MediaContainerModel> mediaContainers = targetMediaContainerDao
                .findMediaContainersByQualifier("dummy", targetProductImportUtil.getProductCatalogStagedVersion());
        Assert.assertNotNull(mediaContainers);
        Assert.assertEquals(0, mediaContainers.size());
    }

}

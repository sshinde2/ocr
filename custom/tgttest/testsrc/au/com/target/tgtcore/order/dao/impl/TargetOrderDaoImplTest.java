package au.com.target.tgtcore.order.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.order.dao.TargetOrderDao;


/**
 * Integration test for {@link TargetOrderDaoImpl}
 * 
 */
@IntegrationTest
public class TargetOrderDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private CartService cartService;

    @Resource
    private TargetOrderDao targetOrderDao;

    @Resource
    private ModelService modelService;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/basicProductSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtlayby/test/testPurchaseOptions.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testTargetCartDao.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testTargetOrderDao.impex", Charsets.UTF_8.name());
    }


    @Test(expected = IllegalArgumentException.class)
    public void testSearchCartByCodeEmptyCode() {
        targetOrderDao.findCartModelForOrderId("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSearchCartByCodeNullCode() {
        targetOrderDao.findCartModelForOrderId(null);
    }

    @Test
    public void testSearchCartByCodeNotExists() {
        final CartModel cart = targetOrderDao.findCartModelForOrderId("shuzbut");

        assertThat(cart).isNull();
    }

    @Test
    public void testSearchCartByCodeExists() {
        final CartModel cart = targetOrderDao.findCartModelForOrderId("testCart");

        assertThat(cart).isNotNull();
        assertThat(cart.getCode()).isEqualTo("testCart");
    }

    @Test
    public void testFindOrderByOrderNumberNullSalesChannel() throws InvalidCartException, DuplicateUidException {
        final OrderModel order = createOrderForCode("012345");
        order.setEBayOrderNumber(Integer.valueOf(12345));
        order.setSalesApplication(SalesApplication.EBAY);
        modelService.save(order);
        final OrderModel eBayOrder = targetOrderDao.findOrdersForPartnerOrderIdAndSalesChannel("12345", null).get(0);
        assertThat(eBayOrder.getEBayOrderNumber().intValue()).isEqualTo(order.getEBayOrderNumber().intValue());
        assertThat(eBayOrder.getSalesApplication().getCode()).isEqualTo(SalesApplication.EBAY.getCode());
    }

    @Test
    public void testFindOrderByOrderNumberWithSalesChannel() throws InvalidCartException, DuplicateUidException {
        final OrderModel order = createOrderForCode("012345");
        order.setEBayOrderNumber(Integer.valueOf(12345));
        order.setSalesApplication(SalesApplication.TRADEME);
        modelService.save(order);
        final OrderModel eBayOrder = targetOrderDao.findOrdersForPartnerOrderIdAndSalesChannel("12345", "TradeMe")
                .get(0);
        assertThat(eBayOrder.getEBayOrderNumber().intValue()).isEqualTo(order.getEBayOrderNumber().intValue());
        assertThat(eBayOrder.getSalesApplication().getCode()).isEqualTo(order.getSalesApplication().getCode());
    }

    @Test
    public void testFindLatestOrderForAUser() {
        final CurrencyModel currency = createCurrency("TEST", "TEST");
        final UserModel user = createUser("user");
        createOrder("012345", currency, user);
        createOrder("012346", currency, user);

        final OrderModel latestOrderForUser = targetOrderDao.findLatestOrderForUser(user);
        assertThat(latestOrderForUser.getCode()).isEqualTo("012346");
    }

    @Test
    public void testFindLatestOrderForANewUser() {
        final UserModel user = createUser("newuser");
        createOrderForCode("012345");
        assertThat(targetOrderDao.findLatestOrderForUser(user)).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindLatestOrderForNullUser() {
        targetOrderDao.findLatestOrderForUser(null);
    }

    @Test
    public void testFindOrderByFluentId() {
        final OrderModel order = targetOrderDao.findOrderByFluentId("1001");
        assertThat(order).isNotNull();
        assertThat(order.getCode()).isEqualTo("testuserorder01");
    }

    @Test(expected = UnknownIdentifierException.class)
    public void testFindOrderByFluentIdUnkown() {
        targetOrderDao.findOrderByFluentId("unkown");
    }

    @Test
    public void testFindParkedOrdersEligibileForRelease() throws ParseException {
        final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        final Date monday = dateformat.parse("2018-01-01T00:00:00");
        final Date tuesday = dateformat.parse("2018-01-02T00:00:00");
        final Date wednesday = dateformat.parse("2018-01-3T00:00:00");
        final Date thursday = dateformat.parse("2018-01-04T00:00:00");
        final Date friday = dateformat.parse("2018-01-05T00:00:00");
        final Date nextMonday = dateformat.parse("2018-01-08T00:00:00");

        final CurrencyModel currency = createCurrency("testcx", "testcx");
        final UserModel user = createUser("preorder");

        // Valid pre orders that will final be fetched
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.PARKED, "preorder1", monday, Boolean.TRUE, null);
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.PARKED, "preorder2", tuesday, Boolean.TRUE, null);
        final OrderModel preorder = createOrderForNormalSalesDatetime(user, currency, OrderStatus.PARKED, "preorder4",
                wednesday, Boolean.FALSE,
                null);
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.PARKED, "preorder5", wednesday, Boolean.TRUE,
                preorder);

        //valid orders that should not be fetched as they are not pre-orders
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.CREATED, getRandomCode(), monday, Boolean.TRUE,
                null);
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.CREATED, getRandomCode(), tuesday, Boolean.TRUE,
                null);

        //valid pre-orders that will not be fetched as they have a normal sales start time after the query date
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.PARKED, getRandomCode(), thursday, Boolean.TRUE,
                null);
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.PARKED, getRandomCode(), friday, Boolean.TRUE,
                null);
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.PARKED, getRandomCode(), nextMonday,
                Boolean.TRUE, null);

        //invalid pre-order that will not be fetched as it does not have a normal sales start date
        createOrderForNormalSalesDatetime(user, currency, OrderStatus.PARKED, "preorder3", null, Boolean.TRUE, null);


        final List<OrderModel> parkedOrders = targetOrderDao.findOrdersBeforeNormalSalesStartDate(wednesday,
                OrderStatus.PARKED);

        assertThat(parkedOrders).hasSize(2);
        assertThat(parkedOrders).onProperty("code").containsExactly("preorder1", "preorder2");
        assertThat(parkedOrders).onProperty("status").containsExactly(OrderStatus.PARKED, OrderStatus.PARKED);
        assertThat(parkedOrders).onProperty("containsPreOrderItems").containsExactly(Boolean.TRUE, Boolean.TRUE);
    }

    @Test
    public void testFindPendingPreOrdersForProduct() {
        final List<OrderModel> orders = targetOrderDao.findPendingPreOrdersForProduct("CP1001");
        assertThat(orders).isNotEmpty().hasSize(3);
        assertThat(orders).onProperty("code").containsExactly("testpreorder01", "testpreorder03", "testpreorder05");
    }

    /**
     * Method to create order with specified code
     * 
     * @param code
     * @return OrderModel
     */
    private OrderModel createOrderForCode(final String code) {
        final CurrencyModel currency = createCurrency("TEST", "TEST");
        final UserModel user = createUser("user");
        return createOrder(code, currency, user);
    }

    /**
     * Method to create OrderModel
     * 
     * @param code
     * @param currency
     * @param user
     * @return OrderModel
     */
    private OrderModel createOrder(final String code, final CurrencyModel currency, final UserModel user) {
        final OrderModel order = new OrderModel();
        order.setCode(code);
        order.setCurrency(currency);
        order.setUser(user);
        order.setDate(new Date());
        modelService.save(order);
        return order;
    }

    /**
     * Method to create user model with specified uid
     * 
     * @param uId
     * @return UserModel
     */
    private UserModel createUser(final String uId) {
        final UserModel user = modelService.create(UserModel.class);
        user.setUid(uId);
        modelService.save(user);
        return user;
    }

    /**
     * Method to create currency model
     * 
     * @param isoCode
     * @param symbol
     * @return CurrencyModel
     */
    private CurrencyModel createCurrency(final String isoCode, final String symbol) {
        final CurrencyModel currency = modelService.create(CurrencyModel.class);
        currency.setIsocode(isoCode);
        currency.setSymbol(symbol);
        modelService.save(currency);
        return currency;
    }

    /**
     * Method to create pre-order for specified sales date
     * 
     * @param code
     * 
     * @return OrderModel
     */
    private OrderModel createOrderForNormalSalesDatetime(final UserModel user, final CurrencyModel currency,
            final OrderStatus status, final String code, final Date normalSaleStartDateTime,
            final Boolean containsPreOrderItems, final OrderModel originalVersion) {
        final OrderModel order = new OrderModel();
        order.setCode(code);
        order.setUser(user);
        order.setCurrency(currency);
        order.setDate(new Date());
        order.setNormalSaleStartDateTime(normalSaleStartDateTime);
        order.setStatus(status);
        order.setContainsPreOrderItems(containsPreOrderItems);
        order.setOriginalVersion(originalVersion);
        modelService.save(order);
        return order;
    }

    private String getRandomCode() {
        return UUID.randomUUID().toString();
    }
}

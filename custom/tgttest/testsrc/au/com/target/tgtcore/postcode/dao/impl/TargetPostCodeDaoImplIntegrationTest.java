package au.com.target.tgtcore.postcode.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.PostCodeModel;


@IntegrationTest
public class TargetPostCodeDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetPostCodeDaoImpl targetPostCodeDao;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testPostCode.impex", "utf-8");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindPostCodeByCodeWithNullCode() {
        targetPostCodeDao.findPostCodeByCode(null);
    }

    @Test
    public void testFindPostCodeByCodeWithNoPostCode() {
        final PostCodeModel result = targetPostCodeDao.findPostCodeByCode("3196");

        assertThat(result).isNull();
    }

    @Test
    public void testFindPostCodeByCode() {
        final String postCode = "3216";
        final PostCodeModel result = targetPostCodeDao.findPostCodeByCode(postCode);

        assertThat(result).isNotNull();
        assertThat(result.getPostCode()).isEqualTo(postCode);
    }
}

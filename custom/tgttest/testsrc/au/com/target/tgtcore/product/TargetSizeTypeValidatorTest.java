/**
 * 
 */
package au.com.target.tgtcore.product;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.impl.TargetSizeTypeServiceImpl;

import com.google.common.base.Charsets;


/**
 * @author nandini
 * 
 */
@IntegrationTest
public class TargetSizeTypeValidatorTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    @Resource
    private TargetSizeTypeServiceImpl sizeTypeService;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/test-size-config.impex", Charsets.UTF_8.name());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSizeTypeNotFound() {
        sizeTypeService.findSizeTypeForCode(null);
    }

    @Test
    public void testNull() {
        Assert.assertNull(sizeTypeService.findSizeTypeForCode("invalid"));
    }

    @Test
    public void testGetSizeTypeConfigRecordExists()
    {
        final SizeTypeModel sizeTypeTeCode = sizeTypeService.findSizeTypeForCode("samplecode2");

        Assert.assertEquals("samplecode2", sizeTypeTeCode.getCode());
        Assert.assertFalse(sizeTypeTeCode.getIsDefault().booleanValue());
    }

    @Test
    public void testEditDefaultSizeType() {
        SizeTypeModel sizeType = sizeTypeService.getDefaultSizeType();
        final String code = sizeType.getCode();
        sizeType.setName("newName");
        modelService.save(sizeType);
        sizeType = sizeTypeService.findSizeTypeForCode(code);

        Assert.assertTrue(sizeType.getIsDefault().booleanValue());
        Assert.assertEquals("newName", sizeType.getName());
    }
}
/**
 * 
 */
package au.com.target.tgtcore.product.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.giftcards.impl.GiftCardServiceImpl;
import au.com.target.tgtcore.model.GiftCardModel;

import com.google.common.base.Charsets;


/**
 * @author smishra1
 * 
 */
@IntegrationTest
public class TargetGiftCardDaoIntegrationTest extends ServicelayerTransactionalTest {

    @Resource(name = "giftCardService")
    private GiftCardServiceImpl giftCardServiceImpl;

    @Resource(name = "targetGiftCardDao")
    private TargetGiftCardDaoImpl targetGiftCardDaoImpl;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testGiftCards.impex", Charsets.UTF_8.name());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testForNullBrandId() throws Exception {
        targetGiftCardDaoImpl.getGiftCardByBrandId(null);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testForNonValidBrandId() throws Exception {
        final GiftCardModel result = targetGiftCardDaoImpl.getGiftCardByBrandId("invalid");
        Assert.assertNull(result);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testServiceWithBlankBrandId() throws Exception {
        final GiftCardModel result = targetGiftCardDaoImpl.getGiftCardByBrandId("");
        Assert.assertNull(result);
    }

    @Test
    public void testGetGiftCardAndValidateFields() throws Exception {
        final GiftCardModel result = targetGiftCardDaoImpl.getGiftCardByBrandId("venue5finediningaus");
        assertThat(result).isNotNull();
        Assert.assertEquals("venue5finediningaus", result.getBrandId());
        Assert.assertEquals("Venue 5 Fine Dining", result.getBrandName());
        Assert.assertEquals(Integer.valueOf(10), result.getMaxOrderQuantity());
        Assert.assertEquals(Integer.valueOf(100), result.getMaxOrderValue());
    }
}

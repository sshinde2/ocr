/**
 * 
 */
package au.com.target.tgtcore.product.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.product.dao.TargetProductDimensionsDao;

import com.google.common.base.Charsets;


/**
 * @author pthoma20
 * 
 */
@IntegrationTest
public class TargetProductDimensionsDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource(name = "targetProductDimensionsDao")
    private TargetProductDimensionsDao targetProductDimensionsDao;

    @Resource
    private CatalogVersionService catalogVersionService;

    @Resource
    private UserService userService;

    private CatalogVersionModel stagedCatalogVersion;
    private CatalogVersionModel onlineCatalogVersion;

    @Before
    public void setUp() throws ImpExException {
        userService.setCurrentUser(userService.getAdminUser());
        stagedCatalogVersion = catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
        onlineCatalogVersion = catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.ONLINE_VERSION);

        importCsv("/tgtcore/test/testProductDimensions.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testFindProductDimensionsPositive() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final TargetProductDimensionsModel targetProductDimensionsModel = targetProductDimensionsDao
                .getTargetProductDimensionByCode(stagedCatalogVersion, "PKG_P1000");
        Assert.assertEquals("PKG_P1000", targetProductDimensionsModel.getDimensionCode());
        Assert.assertEquals(Double.valueOf(1.0), targetProductDimensionsModel.getWeight());
        Assert.assertEquals(Double.valueOf(2.0), targetProductDimensionsModel.getHeight());
        Assert.assertEquals(Double.valueOf(3.2), targetProductDimensionsModel.getLength());
        Assert.assertEquals(Double.valueOf(2.1), targetProductDimensionsModel.getWidth());

    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testFindProductDimensionsWithNoDimensions() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetProductDimensionsModel targetProductDimensionsModel = targetProductDimensionsDao
                .getTargetProductDimensionByCode(stagedCatalogVersion, "PKG_P1220");
        assertThat(targetProductDimensionsModel).isNull();
    }

    @Test
    public void testFindProductDimensionsWithOnlineDimensions() throws ImpExException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final TargetProductDimensionsModel targetProductDimensionsModel = targetProductDimensionsDao
                .getTargetProductDimensionByCode(onlineCatalogVersion, "PKG_P1000");

        Assert.assertEquals("PKG_P1000", targetProductDimensionsModel.getDimensionCode());
        Assert.assertEquals(Double.valueOf(1.0), targetProductDimensionsModel.getWeight());
        Assert.assertEquals(Double.valueOf(2.0), targetProductDimensionsModel.getHeight());
        Assert.assertEquals(Double.valueOf(3.2), targetProductDimensionsModel.getLength());
        Assert.assertEquals(Double.valueOf(2.1), targetProductDimensionsModel.getWidth());
    }

}

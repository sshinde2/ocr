/**
 * TargetProductMerchDepartmentDaoImplTest 
 */
package au.com.target.tgtcore.product.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.product.dao.TargetProductMerchDepartmentDao;
import au.com.target.tgtcore.product.data.TargetProductCountMerchDepRowData;

import com.google.common.base.Charsets;


/**
 * @author bhuang3
 * 
 */
@IntegrationTest
public class TargetProductMerchDepartmentDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private CatalogVersionService catalogVersionService;

    @Resource
    private UserService userService;

    @Resource
    private TargetProductMerchDepartmentDao targetProductMerchDepartmentDao;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testProductsCountInMerchDepartment.impex", Charsets.UTF_8.name());
        userService.setCurrentUser(userService.getAdminUser());
    }

    @Test
    public void testFindProductCountForEachMerchDep() throws ImpExException {
        final List<TargetProductCountMerchDepRowData> result = targetProductMerchDepartmentDao
                .findProductCountForEachMerchDep();
        Assert.assertTrue(!result.isEmpty());
        for (final TargetProductCountMerchDepRowData data : result) {
            if (data.getTargetMerchDepartmentModel().getCode().equals("2000")) {
                Assert.assertEquals(8, data.getProductCount());
            }
            if (data.getTargetMerchDepartmentModel().getCode().equals("2001")) {
                Assert.assertEquals(6, data.getProductCount());
            }
            if (data.getTargetMerchDepartmentModel().getCode().equals("2003")) {
                Assert.fail();
            }
            if (data.getTargetMerchDepartmentModel().getCode().equals("2002")) {
                Assert.fail();
            }
        }
    }
}

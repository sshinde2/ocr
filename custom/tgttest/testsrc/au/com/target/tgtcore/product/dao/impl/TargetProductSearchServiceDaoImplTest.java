package au.com.target.tgtcore.product.dao.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductDepartmentService;

import com.google.common.base.Charsets;


/**
 * Test suite for {@link TargetProductSearchServiceDaoImpl}.
 */
@IntegrationTest
public class TargetProductSearchServiceDaoImplTest extends ServicelayerTransactionalTest {

    private static final String BASE_PRODUCT_CODE = "P5000";
    private static final String PRODUCT_CODE = "P5000_blue_M";
    private static final String SWATCH_CODE = "blue";

    @Resource
    private TargetProductSearchServiceDaoImpl targetProductSearchServiceDao;

    @Resource
    private CatalogVersionService catalogVersionService;

    @Resource
    private UserService userService;

    @Resource
    private ModelService modelService;

    @Resource
    private TargetProductDepartmentService targetProductDepartmentService;

    /**
     * Initializes test cases before run.
     * 
     * @throws ImpExException
     *             if import of test data fails
     */
    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        // sets the necessary user & catalog version attributes into JaloSession,
        // which are implicitly used by search restrictions service
        userService.setCurrentUser(userService.getAdminUser());
    }

    /**
     * Verifies that product size variant can be found by code.
     * 
     * @throws ImpExException
     */
    @Test
    public void testGetSizeVariantListTest() throws ImpExException {
        importCsv("/tgtcore/test/testProducts.impex", Charsets.UTF_8.name());
        final List<TargetSizeVariantProductModel> targetSizes = targetProductSearchServiceDao
                .getTargetSizeVariantProductModelByCode(PRODUCT_CODE);
        Assert.assertNotNull(targetSizes);
        for (final TargetSizeVariantProductModel targetSize : targetSizes) {
            assertEquals(PRODUCT_CODE, targetSize.getCode());
        }
    }

    /**
     * Verifies that color variant can be found by base product and swatch.
     * 
     * @throws ImpExException
     */
    @Test
    public void testGetByBaseProductAndSwatch() throws ImpExException {
        importCsv("/tgtcore/test/testProducts.impex", Charsets.UTF_8.name());
        final CatalogVersionModel catalogVersion = catalogVersionService
                .getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        final TargetColourVariantProductModel colorVariant = targetProductSearchServiceDao
                .getByBaseProductAndSwatch(BASE_PRODUCT_CODE, SWATCH_CODE, catalogVersion);
        assertEquals(BASE_PRODUCT_CODE + '_' + SWATCH_CODE, colorVariant.getCode());
    }

    /**
     * Verifies whether all the products can be retrieved from hybris
     * 
     * @throws ImpExException
     */
    @Test
    public void testGetAllTargetProductsPositive() throws ImpExException {

        importCsv("/tgtcore/test/testMultipleProducts.impex", Charsets.UTF_8.name());
        final List<String> testProductsList = createTestProducts();
        final CatalogVersionModel catalogVersion = catalogVersionService
                .getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        final List<TargetProductModel> targetProductModelList = targetProductSearchServiceDao
                .getAllTargetProductsWithNoDepartment(catalogVersion, 0, 10);
        assertEquals(7, targetProductModelList.size());
        for (final TargetProductModel tgtProductModel : targetProductModelList) {
            Assert.assertTrue(testProductsList.contains(tgtProductModel.getCode()));
            //Removing to ensure no repeats in the retrieved data
            testProductsList.remove(tgtProductModel.getCode());
        }
    }

    /**
     * Verifies whether the batch retrieval of all products is working correctly.
     * 
     * @throws ImpExException
     */
    @Test
    public void testGetAllTargetProductsByBatch() throws ImpExException {
        importCsv("/tgtcore/test/testMultipleProducts.impex", Charsets.UTF_8.name());
        final List<String> testProductsList = createTestProducts();
        final CatalogVersionModel catalogVersion = catalogVersionService
                .getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);

        final int totalTestProducts = testProductsList.size();
        int totalProductsRetrieved = 0;
        final int batchCount = 2;
        final int batchCursor = 0;
        List<TargetProductModel> targetProductModelList;

        do {

            targetProductModelList = targetProductSearchServiceDao
                    .getAllTargetProductsWithNoDepartment(catalogVersion, batchCursor, batchCount);

            for (final TargetProductModel tgtProductModel : targetProductModelList) {
                Assert.assertTrue(testProductsList.contains(tgtProductModel.getCode()));
                //Removing to ensure no repeats in the retrieved data
                testProductsList.remove(tgtProductModel.getCode());
                final TargetMerchDepartmentModel targetMerchDepartmentModel = targetProductDepartmentService
                        .getDepartmentCategoryModel(
                                tgtProductModel.getCatalogVersion(), Integer.valueOf(999));

                tgtProductModel.setMerchDepartment(targetMerchDepartmentModel);
                modelService.save(tgtProductModel);
                totalProductsRetrieved++;
            }

        }
        while (CollectionUtils.isNotEmpty(targetProductModelList));

        Assert.assertEquals(totalTestProducts, totalProductsRetrieved);
    }

    /**
     * Verifies the case when no products are returned.
     * 
     * @throws ImpExException
     */
    @Test
    public void testGetAllTargetProductsNegative() throws ImpExException {

        final CatalogVersionModel catalogVersion = catalogVersionService
                .getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS, TgtCoreConstants.Catalog.OFFLINE_VERSION);
        final List<TargetProductModel> targetProductModelList = targetProductSearchServiceDao
                .getAllTargetProductsWithNoDepartment(catalogVersion, 0, 10);
        Assert.assertTrue(CollectionUtils.isEmpty(targetProductModelList));
    }

    /**
     * Method which aids in creation of Test Verification Data.
     * 
     * @return testProductsList
     * @throws ImpExException
     */
    private List<String> createTestProducts() throws ImpExException {
        final List<String> testProductsList = new ArrayList<>();
        testProductsList.add("P5000");
        testProductsList.add("P5001");
        testProductsList.add("P5002");
        testProductsList.add("P5003");
        testProductsList.add("P5004");
        testProductsList.add("P5005");
        testProductsList.add("P5006");
        return testProductsList;
    }
}
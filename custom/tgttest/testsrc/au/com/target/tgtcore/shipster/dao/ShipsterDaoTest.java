package au.com.target.tgtcore.shipster.dao;


import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtauspost.model.ShipsterConfigModel;
import au.com.target.tgtcore.delivery.TargetDeliveryService;


@IntegrationTest
public class ShipsterDaoTest extends ServicelayerTransactionalTest {


    @Resource
    private ShipsterDao shipsterDao;

    @Resource
    private ModelService modelService;

    @Resource
    private TargetDeliveryService targetDeliveryService;

    private ShipsterConfigModel shipsterConfig;

    private TargetZoneDeliveryModeModel homeDelivery = null;
    private TargetZoneDeliveryModeModel expressDelivery = null;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testDeliveryModeValues.impex", Charsets.UTF_8.name());
        homeDelivery = (TargetZoneDeliveryModeModel)targetDeliveryService.getDeliveryModeForCode("home-delivery");
        expressDelivery = (TargetZoneDeliveryModeModel)targetDeliveryService.getDeliveryModeForCode("express-delivery");
    }

    @Test
    public void testGetShipsterModelForDeliveryModes() {
        createShipsterModel();
        final List<TargetZoneDeliveryModeModel> deliveryModes = new ArrayList<>();
        deliveryModes.add(homeDelivery);
        final Collection<ShipsterConfigModel> models = shipsterDao
                .getShipsterModelForDeliveryModes(deliveryModes);
        assertThat(models).isNotEmpty();
        assertThat(models).hasSize(1);
        for (final ShipsterConfigModel model : models) {
            assertThat(model.getMaxShippingThresholdValue()).isEqualTo(Double.valueOf(25));
            assertThat(model.getMinOrderTotalValue()).isEqualTo(Double.valueOf(20));
            assertThat(model.getActive()).isTrue();
        }
    }

    @Test
    public void testEmptyShipsterModel() {
        final List<TargetZoneDeliveryModeModel> deliveryModes = new ArrayList<>();
        deliveryModes.add(expressDelivery);
        final Collection<ShipsterConfigModel> models = shipsterDao
                .getShipsterModelForDeliveryModes(deliveryModes);
        assertThat(models).isEmpty();
    }


    private void createShipsterModel() {
        shipsterConfig = modelService.create(ShipsterConfigModel.class);
        shipsterConfig.setDeliveryMode(homeDelivery);
        shipsterConfig.setActive(Boolean.TRUE);
        shipsterConfig.setMinOrderTotalValue(Double.valueOf(20.00));
        shipsterConfig.setMaxShippingThresholdValue(Double.valueOf(25.00));
        modelService.save(shipsterConfig);
    }


}

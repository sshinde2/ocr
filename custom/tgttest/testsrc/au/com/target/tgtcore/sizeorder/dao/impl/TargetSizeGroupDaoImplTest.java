/**
 * 
 */
package au.com.target.tgtcore.sizeorder.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.sizeorder.dao.TargetSizeGroupDao;


/**
 * @author mjanarth
 *
 */
@IntegrationTest
public class TargetSizeGroupDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetSizeGroupDao targetSizeGroupDao;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testSizeGroup.impex", "utf-8");
    }

    @Test
    public void testSizeGroupForCode() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetSizeGroupModel result = targetSizeGroupDao.findSizeGroupbyCode("babies");
        assertThat(result).isNotNull();
        assertThat(result.getCode()).as("code").isEqualTo("babies");
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testSizeGroupForCodeUnknown() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        targetSizeGroupDao.findSizeGroupbyCode("abcd");
    }

    @Test
    public void testMaxPosition() {
        final Integer result = targetSizeGroupDao.getMaxSizeGroupPosition();
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(12);
    }
}

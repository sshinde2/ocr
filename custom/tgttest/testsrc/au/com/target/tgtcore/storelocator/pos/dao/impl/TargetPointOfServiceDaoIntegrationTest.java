/**
 * 
 */
package au.com.target.tgtcore.storelocator.pos.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.fest.util.Collections;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;

import com.google.common.base.Charsets;


@IntegrationTest
public class TargetPointOfServiceDaoIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetPointOfServiceDaoImpl targetPointOfServiceDao;

    @Resource
    private BaseStoreService baseStoreService;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Resource
    private ModelService modelService;

    private static final Integer VIRTUAL_STORE_NUMBER = Integer.valueOf(504);

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testTargetPointOfService.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/test/testNTLFulfilmentData.impex", Charsets.UTF_8.name());
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetPOSByStoreNumberInvalid() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        targetPointOfServiceDao.getPOSByStoreNumber(Integer.valueOf(0));
    }

    @Test
    public void testGetPOSByStoreNumber() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final TargetPointOfServiceModel result = targetPointOfServiceDao.getPOSByStoreNumber(Integer.valueOf(234));
        assertThat(result).isNotNull();
        assertThat(result.getStoreNumber()).isEqualTo(Integer.valueOf(234));
        assertThat(result.isVirtualPOS()).isFalse();
    }

    @Test
    public void testGetAllOpenPOS() {
        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao.getAllOpenPOS();
        assertThat(targetPointOfServices).isNotEmpty();
        assertThat(targetPointOfServices.size()).isEqualTo(6);
        assertThat(targetPointOfServices).onProperty("storeNumber").containsOnly(Integer.valueOf(7126),
                Integer.valueOf(7049), Integer.valueOf(7032), Integer.valueOf(7043), Integer.valueOf(234),
                Integer.valueOf(456)).excludes(VIRTUAL_STORE_NUMBER);
    }

    @Test
    public void testGetAllOpenCncPOS() {
        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao.getAllOpenCncPOS();
        assertThat(targetPointOfServices).isNotEmpty();
        assertThat(targetPointOfServices.size()).isEqualTo(1);
        assertThat(targetPointOfServices).onProperty("storeNumber").containsOnly(Integer.valueOf(234))
                .excludes(VIRTUAL_STORE_NUMBER);
    }

    @Test
    public void testGetAllPOSByTypes() {
        final BaseStoreModel baseStoreModel = baseStoreService.getBaseStoreForUid("target");
        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao
                .getAllPOSByTypes(baseStoreModel);
        assertThat(targetPointOfServices).isNotEmpty();
        //POS should be open and of Target or TargetCountry type
        assertThat(targetPointOfServices.size()).isEqualTo(2);
        assertThat(targetPointOfServices).onProperty("storeNumber").containsOnly(Integer.valueOf(234),
                Integer.valueOf(456)).excludes(VIRTUAL_STORE_NUMBER);
    }

    @Test
    public void testGetAllFulfilmentPPDEnabledPOS() {
        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao
                .getAllFulfilmentPPDEnabledPOS();
        assertThat(targetPointOfServices).isNotEmpty();
        assertThat(targetPointOfServices.size()).isEqualTo(4);
    }

    @Test
    public void testGetAllFulfilmentEnabledPOS() {
        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao
                .getAllFulfilmentEnabledPOS();
        assertThat(targetPointOfServices).isNotEmpty();
        assertThat(targetPointOfServices.size()).isEqualTo(4);
        assertThat(targetPointOfServices).onProperty("storeNumber").containsOnly(Integer.valueOf(7126),
                Integer.valueOf(7049), Integer.valueOf(7032), Integer.valueOf(7043)).excludes(VIRTUAL_STORE_NUMBER);

    }

    @Test
    public void testGetAllFulfilmentEnabledPOSInState() {
        setUpAddressInPOS(targetPointOfServiceDao
                .getAllFulfilmentEnabledPOS(), Collections.list("VIC", "SA", "QLD", "WA"));
        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao
                .getAllFulfilmentEnabledPOSInState("VIC");
        assertThat(targetPointOfServices).isNotEmpty();
        assertThat(targetPointOfServices.size()).isEqualTo(1);
    }

    @Test
    public void testGetAllFulfilmentEnabledPOSInStateNoneReturned() {
        setUpAddressInPOS(targetPointOfServiceDao
                .getAllFulfilmentEnabledPOS(), Collections.list("VIC", "SA", "QLD", "WA"));
        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao
                .getAllFulfilmentEnabledPOSInState("NSW");
        assertThat(targetPointOfServices).isEmpty();
    }

    @Test
    public void testGetAllFulfilmentEnabledPOSInStateEmptyState() {
        setUpAddressInPOS(targetPointOfServiceDao
                .getAllFulfilmentEnabledPOS(), Collections.list("VIC", "SA", "QLD", "WA"));
        final List<TargetPointOfServiceModel> targetPointOfServices = targetPointOfServiceDao
                .getAllFulfilmentEnabledPOSInState("");
        assertThat(targetPointOfServices).isEmpty();
    }


    @Test
    public void testGetRegionByAbbreviation() {
        final RegionModel result = targetPointOfServiceDao.getRegionByAbbreviation("NSW");
        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo("New South Wales");
        assertThat(result.getIsocode()).isEqualTo("AU-NSW");
    }

    @Test
    public void testGetRegionForEmptyAbbreviation() {
        final RegionModel result = targetPointOfServiceDao.getRegionByAbbreviation(StringUtils.EMPTY);
        assertThat(result).isNull();
    }

    @Test
    public void testGetRegionForNullAbbreviation() {
        final RegionModel result = targetPointOfServiceDao.getRegionByAbbreviation("null");
        assertThat(result).isNull();
    }

    @Test
    public void testGetRegionForAbbreviationNotFound() {
        final RegionModel result = targetPointOfServiceDao.getRegionByAbbreviation("TMP");
        assertThat(result).isNull();
    }

    @Test
    public void testGetPosForFluent() {
        final List<TargetPointOfServiceModel> posForFluent = targetPointOfServiceDao.getAllPosForFluent();
        assertThat(posForFluent).hasSize(6);
        assertThat(posForFluent).onProperty("storeNumber").containsOnly(Integer.valueOf(7126), Integer.valueOf(7049),
                Integer.valueOf(7032), Integer.valueOf(7043), Integer.valueOf(234), Integer.valueOf(456))
                .excludes(VIRTUAL_STORE_NUMBER);
    }

    /**
     * @param allFulfilmentEnabledPOS
     */
    private void setUpAddressInPOS(final List<TargetPointOfServiceModel> allFulfilmentEnabledPOS,
            final List<String> states) {
        int state = 0;
        for (final TargetPointOfServiceModel pos : allFulfilmentEnabledPOS) {
            final AddressModel address1 = modelService.create(AddressModel.class);
            address1.setDistrict(states.get(state++));
            address1.setOwner(pos);
            pos.setAddress(address1);
            modelService.save(pos);
        }
    }
}

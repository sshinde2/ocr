/**
 * 
 */
package au.com.target.tgtcore.tax.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.order.price.TaxModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.europe1.enums.ProductTaxGroup;
import de.hybris.platform.europe1.enums.UserTaxGroup;
import de.hybris.platform.europe1.model.TaxRowModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.order.TaxService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.constants.TgtCoreConstants;

import com.google.common.base.Charsets;


@IntegrationTest
public class TargetTaxGroupServiceImplTest extends ServicelayerTransactionalTest {

    @Resource
    private CatalogVersionService catalogVersionService;

    @Resource
    private ModelService modelService;

    @Resource
    private EnumerationService enumerationService;

    @Resource
    private TaxService taxService;

    @Resource
    private TargetTaxGroupServiceImpl targetTaxGroupService;

    private CatalogVersionModel offlineCatalog;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());

        offlineCatalog = catalogVersionService.getCatalogVersion(
                TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
    }

    @Test
    public void testFindProductTaxGroupExists() {
        final ProductTaxGroup ptg = targetTaxGroupService.findProductTaxGroup("aus-gst-1000");
        Assert.assertNotNull(ptg);
        Assert.assertEquals("aus-gst-1000", ptg.getCode());

    }

    @Test
    public void testFindProductTaxGroupNotExists() {
        final ProductTaxGroup ptg = targetTaxGroupService.findProductTaxGroup("aus-gst-1005");
        Assert.assertNull(ptg);
    }

    @Test
    public void testCreateProductTaxGroup() {

        final ProductTaxGroup ptg = targetTaxGroupService.createProductTaxGroup("aus-gst-1005");
        Assert.assertNotNull(ptg);
        Assert.assertEquals("aus-gst-1005", enumerationService.getEnumerationName(ptg));

        final ProductTaxGroup ptg2 = targetTaxGroupService.findProductTaxGroup("aus-gst-1005");
        Assert.assertNotNull(ptg2);
        Assert.assertEquals("aus-gst-1005", enumerationService.getEnumerationName(ptg2));
    }

    @Test
    public void testFindOrCreateTargetTaxExists() {

        final TaxModel tax = targetTaxGroupService.findOrCreateTargetTax(1000);
        Assert.assertNotNull(tax);
        Assert.assertEquals("aus-gst-1000", tax.getCode());
        Assert.assertEquals(new Double(10), tax.getValue());
    }

    @Test
    public void testFindOrCreateTargetTaxNew() {

        final TaxModel tax = targetTaxGroupService.findOrCreateTargetTax(1005);
        Assert.assertNotNull(tax);
        Assert.assertEquals("aus-gst-1005", tax.getCode());
        Assert.assertEquals("aus-gst-1005", tax.getName());
        Assert.assertEquals(new Double(10.05), tax.getValue());
    }

    @Test
    public void testExistsTaxRowExists() {

        // There should be one for rate 1000
        final TaxModel tax = targetTaxGroupService.findOrCreateTargetTax(1000);
        Assert.assertNotNull(tax);
        final ProductTaxGroup ptg = targetTaxGroupService.findProductTaxGroup("aus-gst-1000");
        Assert.assertNotNull(ptg);

        Assert.assertTrue(targetTaxGroupService.existsTaxRow(tax, ptg, offlineCatalog));
    }

    @Test
    public void testExistsTaxRowNew() {

        // There should not one for rate 1010
        final TaxModel tax = targetTaxGroupService.findOrCreateTargetTax(1010);
        final ProductTaxGroup ptg = targetTaxGroupService.createProductTaxGroup("aus-gst-1010");

        Assert.assertFalse(targetTaxGroupService.existsTaxRow(tax, ptg, offlineCatalog));
    }

    @Test
    public void testCreateTaxRowExists() {

        final TaxModel tax = targetTaxGroupService.findOrCreateTargetTax(1000);
        final ProductTaxGroup ptg = targetTaxGroupService.findProductTaxGroup("aus-gst-1000");

        Assert.assertNull(targetTaxGroupService.createTaxRow(tax, ptg, offlineCatalog));
    }

    @Test
    public void testCreateTaxRowNew() {

        // There should not one for rate 1010
        final TaxModel tax = targetTaxGroupService.findOrCreateTargetTax(1010);
        final ProductTaxGroup ptg = targetTaxGroupService.createProductTaxGroup("aus-gst-1010");

        final TaxRowModel row = targetTaxGroupService.createTaxRow(tax, ptg, offlineCatalog);
        Assert.assertNotNull(row);
    }

    @Test
    public void testGetUserTaxGroup() {

        final UserTaxGroup utg = targetTaxGroupService.getUserTaxGroup();
        Assert.assertNotNull(utg);
        Assert.assertEquals("aus-taxes", utg.getCode());
    }


}

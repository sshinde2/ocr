/**
 * 
 */
package au.com.target.tgtcore.taxinvoice.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commons.model.DocumentModel;
import de.hybris.platform.commons.model.FormatModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.taxinvoice.TaxInvoiceException;
import au.com.target.tgtcore.taxinvoice.dao.TaxInvoiceDao;
import au.com.target.tgtcore.taxinvoice.impl.TaxInvoiceServiceImpl;


/**
 * {@link TaxInvoiceDaoImpl}
 */
@IntegrationTest
public class TaxInvoiceDaoImplTest extends ServicelayerTransactionalTest {

    private static final String TAXINVOICE_IMPEX = "/tgtwebcore/import/common/taxInvoice.impex";
    private static final String TAXINVOICE_TEMPLATE_CONFIG_KEY = "tgtcore.taxinvoice.template.code";
    private static final String INVOICE_TEMPLATE_SEARCH_QUERY = "select {" + FormatModel.PK + "} from {"
            + FormatModel._TYPECODE + "} where {" + FormatModel.CODE + "} =?templateCode";
    private static final String ORDER_1 = "00110011";
    private static final String ORDER_2 = "00110012";
    private static final String NOT_EXIST_ORDER_CODE = "00110013";
    private static final String CURRENCY = "TEST";
    private static final String TEST1_EXAMPLE_COM = "test1@example.com";

    @Resource
    private TaxInvoiceDao taxInvoiceDao;
    @Resource
    private ModelService modelService;
    @Resource
    private CommonI18NService commonI18NService;
    @Resource
    private TaxInvoiceServiceImpl taxInvoiceService;
    @Resource
    private FlexibleSearchService flexibleSearchService;
    @Resource
    private ConfigurationService configurationService;


    @Before
    public void setUp() throws JaloBusinessException, TaxInvoiceException {

        importCsv(TAXINVOICE_IMPEX, "UTF-8");
        final String taxInvoiceTemplate = configurationService.getConfiguration().getString(
                TAXINVOICE_TEMPLATE_CONFIG_KEY);
        final Map<String, Object> params = new HashedMap();
        params.put("templateCode", taxInvoiceTemplate);
        final SearchResult list = flexibleSearchService.search(INVOICE_TEMPLATE_SEARCH_QUERY, params);
        Assert.assertTrue("taxInvoiceTemplate must exist", list.getTotalCount() > 0);


        final CurrencyModel currency = modelService.create(CurrencyModel.class);
        currency.setIsocode(CURRENCY);
        currency.setSymbol(CURRENCY);
        currency.setActive(Boolean.TRUE);
        modelService.save(currency);


        final UserModel user = modelService.create(UserModel.class);
        user.setUid(TEST1_EXAMPLE_COM);
        modelService.save(user);

        final AddressModel deliveryAddress = modelService.create(AddressModel.class);
        deliveryAddress.setOwner(user);
        modelService.save(deliveryAddress);

        final OrderModel order = modelService.create(OrderModel.class);
        order.setCode(ORDER_1);
        order.setDate(new Date());
        order.setCurrency(commonI18NService.getCurrency(CURRENCY));
        order.setUser(user);
        order.setDeliveryAddress(deliveryAddress);
        modelService.save(order);

        final OrderModel order2 = modelService.create(OrderModel.class);
        order2.setCode(ORDER_2);
        order2.setDate(new Date());
        order2.setCurrency(commonI18NService.getCurrency(CURRENCY));
        order2.setUser(user);
        order2.setDeliveryAddress(deliveryAddress);
        modelService.save(order2);

        taxInvoiceService.generateTaxInvoiceForOrder(order);
        taxInvoiceService.generateTaxInvoiceForOrder(order2);

    }

    @Test
    public void testFindInvoiceDocumentForOrder() {
        final DocumentModel document = taxInvoiceDao.findInvoiceDocumentForOrder(ORDER_1);
        Assert.assertNotNull(document);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testFindNotExistInvoiceDocumentForOrder() {
        taxInvoiceDao.findInvoiceDocumentForOrder(NOT_EXIST_ORDER_CODE);
        Assert.fail("findInvoiceDocumentForOrder has to throw ModelNotFoundException");
    }

}

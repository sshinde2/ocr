/**
 * 
 */
package au.com.target.tgtcore.ticket;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.ticket.enums.CsTicketState;
import de.hybris.platform.ticket.model.CsTicketModel;
import de.hybris.platform.ticket.service.TicketService;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.ticket.service.TargetTicketBusinessService;

import com.google.common.base.Charsets;


/**
 * 
 */
@IntegrationTest
public class TargetTicketBusinessServiceIntegrationTest extends ServicelayerTransactionalTest {
    @Resource
    private TargetTicketBusinessService targetTicketBusinessService;
    @Resource
    private TicketService ticketService;
    @Resource
    private ModelService modelService;
    @Resource
    private CommonI18NService commonI18NService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/import/cockpits/cscockpit/cscockpit-users.impex", Charsets.UTF_8.name());
        importCsv("/tgtcore/import/cockpits/cscockpit/cscockpit-access-rights.impex", Charsets.UTF_8.name());
    }

    private OrderModel minimumOrder(final SalesApplication salesApplication) {
        final UserModel user = modelService.create(UserModel.class);
        user.setUid("fred");
        modelService.save(user);
        final OrderModel order = modelService.create(OrderModel.class);
        order.setDate(new Date());
        order.setCurrency(commonI18NService.getCurrency("AUD"));
        order.setUser(user);
        order.setSalesApplication(salesApplication);
        modelService.save(order);
        return order;
    }

    @Test
    public void testCsAgentGroupForTradeMeSalesApplication() throws Exception {
        final String id = targetTicketBusinessService.createCsAgentTicket("test headline", "test subject",
                "test text", minimumOrder(SalesApplication.TRADEME));
        Assert.assertNotNull(id);
        final CsTicketModel ticket = ticketService.getTicketForTicketId(id);
        Assert.assertNotNull(ticket);
        Assert.assertEquals("csagent", ticket.getAssignedAgent().getUid());
        Assert.assertEquals(CsTicketState.OPEN, ticket.getState());
        Assert.assertNotNull(ticket.getOrder());
        Assert.assertNotNull(ticket.getAssignedGroup());
        Assert.assertEquals(TgtCoreConstants.CsAgentGroup.TRADE_ME_CS_AGENT_GROUP, ticket.getAssignedGroup().getUid());
    }

    @Test
    public void testCsAgentGroupForEbaySalesApplication() throws Exception {
        final String id = targetTicketBusinessService.createCsAgentTicket("test headline", "test subject",
                "test text", minimumOrder(SalesApplication.EBAY));
        Assert.assertNotNull(id);
        final CsTicketModel ticket = ticketService.getTicketForTicketId(id);
        Assert.assertNotNull(ticket);
        Assert.assertEquals("csagent", ticket.getAssignedAgent().getUid());
        Assert.assertEquals(CsTicketState.OPEN, ticket.getState());
        Assert.assertNotNull(ticket.getOrder());
        Assert.assertNotNull(ticket.getAssignedGroup());
        Assert.assertEquals(TgtCoreConstants.CsAgentGroup.EBAY_CS_AGENT_GROUP, ticket.getAssignedGroup().getUid());
    }

    @Test
    public void testCreateCsAgentTicket() throws Exception {
        final String id = targetTicketBusinessService.createCsAgentTicket("test headline", "test subject",
                "test text", minimumOrder(SalesApplication.WEB));

        Assert.assertNotNull(id);
        final CsTicketModel ticket = ticketService.getTicketForTicketId(id);
        Assert.assertNotNull(ticket);
        Assert.assertEquals("csagent", ticket.getAssignedAgent().getUid());
        Assert.assertEquals(CsTicketState.OPEN, ticket.getState());
        Assert.assertNotNull(ticket.getOrder());
    }

    @Test
    public void testCreateCsAgentTicketNullOrder() throws Exception {
        final String id = targetTicketBusinessService.createCsAgentTicket("test headline", "test subject", "test text",
                null);

        Assert.assertNotNull(id);
        final CsTicketModel ticket = ticketService.getTicketForTicketId(id);
        Assert.assertNotNull(ticket);
        Assert.assertEquals("csagent", ticket.getAssignedAgent().getUid());
        Assert.assertEquals(CsTicketState.OPEN, ticket.getState());
        Assert.assertNull(ticket.getOrder());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCsAgentTicketNullHeadline() throws Exception {
        targetTicketBusinessService.createCsAgentTicket(null, "test subject", "test text",
                minimumOrder(SalesApplication.WEB));
    }

    @Test
    public void testCreateCsAgentTicketNullSubject() throws Exception {
        final String id = targetTicketBusinessService.createCsAgentTicket("test headline", null, "test text",
                minimumOrder(SalesApplication.WEB));

        Assert.assertNotNull(id);
        final CsTicketModel ticket = ticketService.getTicketForTicketId(id);
        Assert.assertNotNull(ticket);
        Assert.assertEquals("csagent", ticket.getAssignedAgent().getUid());
        Assert.assertEquals(CsTicketState.OPEN, ticket.getState());
        Assert.assertNotNull(ticket.getOrder());
    }

    @Test
    public void testCreateCsAgentTicketWithCsAgentGroup() throws Exception {
        final String id = targetTicketBusinessService.createCsAgentTicket("test headline", null, "test text",
                minimumOrder(SalesApplication.WEB), TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP);
        Assert.assertNotNull(id);
        final CsTicketModel ticket = ticketService.getTicketForTicketId(id);
        Assert.assertNotNull(ticket);
        Assert.assertEquals(TgtCoreConstants.CsAgentGroup.REFUND_FAILED_CS_AGENT_GROUP, ticket.getAssignedGroup()
                .getUid());
        Assert.assertEquals(CsTicketState.OPEN, ticket.getState());
        Assert.assertNotNull(ticket.getOrder());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateCsAgentTicketNullText() throws Exception {
        targetTicketBusinessService.createCsAgentTicket("test headline", "test subject", null,
                minimumOrder(SalesApplication.WEB));
    }
}

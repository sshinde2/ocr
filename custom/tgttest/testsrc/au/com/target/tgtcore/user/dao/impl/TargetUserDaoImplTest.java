/**
 * 
 */
package au.com.target.tgtcore.user.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.user.dao.TargetUserDao;

import com.google.common.base.Charsets;


/**
 * @author mjanarth
 *
 */
@IntegrationTest
public class TargetUserDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetUserDao targetUserDao;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testTargetCustomer.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testFindCustomerBySubscriptionId() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetCustomerModel customer = targetUserDao.findUserBySubscriptionId("1501");
        Assert.assertNotNull(customer);
        Assert.assertEquals(customer.getSubscriberKey(), "1501");
        Assert.assertEquals(customer.getFirstname(), "test");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindCustomerNull() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final TargetCustomerModel customer = targetUserDao.findUserBySubscriptionId(null);
        Assert.assertNull(customer);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testFindCustomerUnknownuserID() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        targetUserDao.findUserBySubscriptionId("1504");
    }
}

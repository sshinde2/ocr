/**
 * 
 */
package au.com.target.tgtcore.util;

import de.hybris.bootstrap.annotations.IntegrationTest;

import junit.framework.Assert;

import org.junit.Test;


@IntegrationTest
public class OrderEncodingToolsIntegrationTest {

    @Test
    public void testEncodeAndDecodeBarcode() {
        final String encoded = OrderEncodingTools.encodeOrderId("12345678");
        final String decoded = OrderEncodingTools.decodeOrderId(encoded);
        Assert.assertFalse(encoded.equals(decoded));
        Assert.assertEquals("12345678", decoded);

    }
}

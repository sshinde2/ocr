package au.com.target.tgtcore.valuepack.dao.impl;

import static junit.framework.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.valuepack.dao.TargetValuePackTypeDao;
import com.google.common.collect.ImmutableList;

/**
 * Test suite for {@link TargetValuePackTypeDaoImpl}.
 */
@IntegrationTest
public class TargetValuePackTypeDaoImplTest extends ServicelayerTransactionalTest {

    private static final String SKU = "12345";

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @Resource
    private TargetValuePackTypeDao valuePackTypeDao;

    @Resource
    private ModelService modelService;

    private TargetValuePackTypeModel valuePackType;


    /**
     * Initializes test cases before run.
     */
    @Before
    public void setUp() {
        valuePackType = modelService.create(TargetValuePackTypeModel.class);
        valuePackType.setLeadSKU(SKU);
        valuePackType.setValuePackListQualifier(ImmutableList.<TargetValuePackModel>of());
        modelService.save(valuePackType);
    }

    /**
     * Verifies that value pack type can be retrieved by lead SKU.
     */
    @Test
    public void testRetrievalByLeadSku() {
        assertEquals(valuePackType, valuePackTypeDao.getByLeadSku(SKU));
    }

    /**
     * Verifies that exception will be thrown in case of non-existing leading SKU will
     * be provided as an input to {@link TargetValuePackTypeDao#getByLeadSku(String)}.
     */
    @Test
    public void testRecordNotFound() {
        expectedException.expect(ModelNotFoundException.class);
        valuePackTypeDao.getByLeadSku("foo");
    }
}

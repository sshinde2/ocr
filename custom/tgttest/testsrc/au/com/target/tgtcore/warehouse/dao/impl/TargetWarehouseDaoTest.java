/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcore.warehouse.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.warehouse.dao.TargetWarehouseDao;
import au.com.target.tgtfulfilment.enums.IntegrationMethod;


/**
 *
 */
@IntegrationTest
public class TargetWarehouseDaoTest extends ServicelayerTransactionalTest {

    private static final String WAREHOUSE1CODE = "w1";

    private static final String WAREHOUSE2CODE = "w2";

    private static final String WAREHOUSE3CODE = "w3";

    private static final String BBWAREHOUSE1CODE = "bbw1";

    private static final String EXTWAREHOUSE1CODE = "ew1";

    private static final String VENDOR1CODE = "v1";

    private static final Integer POINTOFSERVICE1NUMBER = Integer.valueOf(5001);

    private static final String POINTOFSERVICE1NAME = "POS1";

    @Resource
    private TargetWarehouseDao targetWarehouseDao;

    @Resource
    private ModelService modelService;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    private PointOfServiceModel pointOfService1;

    private WarehouseModel warehouse1;

    private WarehouseModel warehouse2;

    private WarehouseModel warehouse3;

    private WarehouseModel warehouse4;

    private WarehouseModel extWarehouse1;

    private WarehouseModel bbWarehouse1;

    private VendorModel vendor1;

    /**
     *
     */
    @Before
    public void setUp() throws Exception {
        vendor1 = createVendor(VENDOR1CODE);
        pointOfService1 = createPointOfService(POINTOFSERVICE1NUMBER, POINTOFSERVICE1NAME);
        warehouse1 = createWarehouse(WAREHOUSE1CODE, vendor1, Boolean.TRUE, true, false, null, true);
        warehouse2 = createWarehouse(WAREHOUSE2CODE, vendor1, Boolean.FALSE, false, false, null, false);
        warehouse3 = createWarehouse(WAREHOUSE3CODE, vendor1, Boolean.FALSE, false, false, null, true);
        warehouse4 = createWarehouse(WAREHOUSE3CODE, vendor1, Boolean.FALSE, true, false, null, false);
        bbWarehouse1 = createWarehouse(BBWAREHOUSE1CODE, vendor1, Boolean.FALSE, false, false, pointOfService1, false);
        extWarehouse1 = createWarehouse(EXTWAREHOUSE1CODE, vendor1, Boolean.FALSE, true, true, null, false);
    }

    private WarehouseModel createWarehouse(final String code, final VendorModel vendor, final Boolean def,
            final boolean surfaceOnlineStock, final boolean externalWarehouse,
            final PointOfServiceModel pointOfService, final boolean preOrderWarehouse) {
        final WarehouseModel res = modelService.create(WarehouseModel.class);
        res.setCode(code);
        res.setVendor(vendor);
        res.setDefault(def);
        res.setIntegrationMethod(IntegrationMethod.ESB);
        res.setSurfaceStockOnline(surfaceOnlineStock);
        res.setExternalWarehouse(externalWarehouse);
        res.setPreOrderWarehouse(preOrderWarehouse);
        res.setFluentLocationRef(code.toUpperCase());

        if (pointOfService != null) {
            final List<PointOfServiceModel> pointsOfService = new ArrayList<>();
            pointsOfService.add(pointOfService);
            res.setPointsOfService(pointsOfService);
        }

        modelService.save(res);
        return res;
    }

    private VendorModel createVendor(final String code) {
        final VendorModel res = modelService.create(VendorModel.class);
        res.setCode(code);
        modelService.save(res);
        return res;
    }

    private PointOfServiceModel createPointOfService(final Integer storeNumber, final String name) {
        final TargetPointOfServiceModel pos = modelService.create(TargetPointOfServiceModel.class);

        pos.setType(PointOfServiceTypeEnum.TARGET);
        pos.setName(name);
        pos.setStoreNumber(storeNumber);

        modelService.save(pos);
        return pos;
    }

    @Test
    public void testGetWarehouseForCode() {
        assertThat(targetWarehouseDao.getWarehouseForCode(WAREHOUSE1CODE)).containsOnly(warehouse1);
        assertThat(targetWarehouseDao.getWarehouseForCode(WAREHOUSE2CODE)).containsOnly(warehouse2);
        assertThat(targetWarehouseDao.getWarehouseForCode(WAREHOUSE3CODE)).containsOnly(warehouse3,
                warehouse4);
    }

    @Test
    public void testGetWarehouseForPointOfService() {
        final List<WarehouseModel> warehouses = targetWarehouseDao.getWarehouseForPointOfService(POINTOFSERVICE1NUMBER);

        assertThat(warehouses).isNotEmpty().containsOnly(bbWarehouse1);
    }

    @Test
    public void testGetPreOrderWarehouses() {
        final List<WarehouseModel> warehouses = targetWarehouseDao.getPreOrderWarehouses();

        assertThat(warehouses).isNotEmpty().hasSize(2);
        assertThat(warehouses).contains(warehouse1);
        assertThat(warehouses).contains(warehouse3);
    }

    @Test
    public void testGetSurfaceStockOnlineWarehouses() {
        final List<WarehouseModel> warehouses = targetWarehouseDao.getSurfaceStockOnlineWarehouses();

        assertThat(warehouses).isNotEmpty().hasSize(3);
        assertThat(warehouses).contains(warehouse1);
        assertThat(warehouses).contains(warehouse4);
    }

    @Test
    public void testGetExternalWarehouses() {
        final List<WarehouseModel> warehouses = targetWarehouseDao.getExternalWarehouses(true);

        assertThat(warehouses).isNotEmpty().hasSize(1);
        assertThat(warehouses).contains(extWarehouse1);
    }

    @Test
    public void testGetWarehouseForFluentLocationRef() {
        assertThat(targetWarehouseDao.getWarehouseForFluentLocationRef(WAREHOUSE1CODE.toUpperCase()))
                .containsOnly(warehouse1);
    }
}

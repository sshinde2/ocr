/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtcs;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.cscockpit.model.data.DataObject;
import de.hybris.platform.cscockpit.services.search.CsSearchResult;
import de.hybris.platform.cscockpit.services.search.SearchException;
import de.hybris.platform.cscockpit.services.search.generic.DefaultCsFlexibleSearchService;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultCustomerSearchQueryBuilder;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.cscockpit.services.search.impl.DefaultPageable;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcs.widgets.controllers.impl.TargetCustomerSearchQueryBuilder;


/**
 * Integration Test for {@link TargetCustomerSearchQueryBuilder}
 */
@IntegrationTest
public class TargetCustomerSearchQueryBuilderTest extends ServicelayerTransactionalTest {
    private static final String VALID_FLYBUYS_NUMBER = "6008943218616910";
    private static final String UID1 = "uid1.email@yahoo.com";
    private static final String UID2 = "example.test@gmail.com";
    private static final String UID3 = "uid3.email@yahoo.com";
    private static final String UID4 = "uid4.email@yahoo.com";
    private static final String UID5 = "uid5.email@yahoo.com";
    private static final String UID6 = "uid6.email@yahoo.com";
    private static final String UID7 = "uid7.email@yahoo.com";
    private static final String UID8 = "uid8.email@yahoo.com";

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Resource
    private SearchRestrictionService searchRestrictionService;

    @Resource
    private ModelService modelService;

    @Resource
    private SessionService sessionService;

    private final DefaultCsTextSearchCommand command = new DefaultCsTextSearchCommand();

    private final DefaultCsFlexibleSearchService<DefaultCsTextSearchCommand, ItemModel> searchService = new DefaultCsFlexibleSearchService<DefaultCsTextSearchCommand, ItemModel>();//NOPMD

    private final DefaultPageable pageable = new DefaultPageable();

    private TargetCustomerModel customer = new TargetCustomerModel();

    @Before
    public void setUp() throws Exception {

        searchService.setFlexibleSearchService(flexibleSearchService);
        searchService.setSearchRestrictionService(searchRestrictionService);
        searchService.setSessionService(sessionService);
        searchService.setFlexibleSearchQueryBuilder(new TargetCustomerSearchQueryBuilder()); // target cscockpit query builder

        pageable.setPageNumber(0); //first page
        pageable.setPageSize(15);
    }

    @Test
    public void testSearchByFirstNameLastNameFields() throws SearchException {
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("ExampleTest");
        user.setLastname("One");
        user.setName("ExampleTest One");
        user.setUid(UID1);
        user.setFlyBuysCode(VALID_FLYBUYS_NUMBER);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "ExampleTest One");

        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(UID1, customer.getUid());
    }

    @Test
    public void testSearchByPartFirstNameLastNameFields() throws SearchException {
        final String uid = "testapart@apart.com";
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("ExampleTest");
        user.setLastname("OneTest");
        user.setName("ExampleTest OneTest");
        user.setUid(uid);
        user.setFlyBuysCode(VALID_FLYBUYS_NUMBER);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "ExampleTes OneTes");

        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(uid, customer.getUid());
    }

    @Test
    public void testSearchByFirstNameField() throws SearchException {
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("tenthFirstName");
        user.setLastname("tenthLastName");
        user.setName("tenthFirstName tenthLastName");
        user.setUid(UID5);
        user.setFlyBuysCode(VALID_FLYBUYS_NUMBER);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "tenthFirstName");
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(UID5, customer.getUid());
    }

    @Test
    public void testSearchByPartFirstNameField() throws SearchException {
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("eleventhFirstName");
        user.setLastname("eleventhLastName");
        user.setName("eleventhFirstName eleventhLastName");
        user.setUid(UID6);
        user.setFlyBuysCode(VALID_FLYBUYS_NUMBER);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "eleventhFirs");
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(UID6, customer.getUid());
    }

    @Test
    public void testSearchByLastNameField() throws SearchException {
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("twelfthFirstName");
        user.setLastname("twelfthLastName");
        user.setName("twelfthFirstName twelfthLastName");
        user.setUid(UID7);
        user.setFlyBuysCode(VALID_FLYBUYS_NUMBER);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "twelfthLastName");
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(UID7, customer.getUid());
    }

    @Test
    public void testSearchByPartLastNameField() throws SearchException {
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("thirteenthFirstName");
        user.setLastname("thirteenthLastName");
        user.setName("thirteenthFirstName thirteenthLastName");
        user.setUid(UID8);
        user.setFlyBuysCode(VALID_FLYBUYS_NUMBER);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "thirteenthLas");
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(UID8, customer.getUid());
    }


    @Test
    public void testSearchByEmail() throws SearchException {
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("twofirstname");
        user.setLastname("twolastname");
        user.setName("twofirstname twolastname");
        user.setUid(UID2);
        user.setFlyBuysCode("");
        modelService.save(user);

        //search on uid(email)
        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, UID2);
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();


        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(UID2, customer.getUid());
    }

    @Test
    public void testSearchByFlybuysCode() throws SearchException {
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("Forth");
        user.setLastname("User");
        user.setName("Forth User");
        user.setUid(UID3);
        user.setFlyBuysCode(VALID_FLYBUYS_NUMBER);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, VALID_FLYBUYS_NUMBER);
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(UID3, customer.getUid());
    }

    @Test
    public void testSearchNoResult() throws SearchException {
        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "testnoresult");
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();

        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void testSearchSpecialCharacters() throws SearchException {
        final TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("Sixth");
        user.setLastname("User");
        user.setName("Sixth User");
        user.setUid(UID4);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "%Sixth User");
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();
        Assert.assertNotNull(result);
        final ItemModel item = result.get(0).getItem();
        Assert.assertNotNull(item);
        Assert.assertTrue(item instanceof TargetCustomerModel);
        customer = (TargetCustomerModel)item;
        Assert.assertEquals(UID4, customer.getUid());
    }

    @Test
    public void testSearchByFirstNameLastNameReturnMultiRecords() throws SearchException {
        final String uid5 = "targetuidexample5@gmail.com";
        final String uid6 = "targetuidexample6@gmail.com";
        final String uid7 = "targetuidexample7@gmail.com";
        TargetCustomerModel user = modelService.create(TargetCustomerModel.class);

        user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("TargetExample");
        user.setLastname("TestFifth");
        user.setName("TargetExample TestFifth");
        user.setUid(uid5);
        modelService.save(user);

        user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("TargetExample");
        user.setLastname("TestSixth");
        user.setName("TargetExample TestSixth");
        user.setUid(uid6);
        modelService.save(user);

        user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("TargetExample");
        user.setLastname("TestSeventh");
        user.setName("TargetExample TestSeventh");
        user.setUid(uid7);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "TargetExample Test"); //must add space for first name, last name searching
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();
        Assert.assertNotNull(result);

        int count = 0;
        for (int i = 0; i < result.size(); i++) {
            final TargetCustomerModel c = (TargetCustomerModel)result.get(i).getItem();
            if (uid5.equals(c.getUid()) || uid6.equals(c.getUid()) || uid7.equals(c.getUid())) {
                count++;
            }
        }
        Assert.assertEquals(3, count);
    }

    @Test
    public void testSearchByEmailReturnMultiRecords() throws SearchException {
        final String uid5 = "targetuidexample5@gmail.com";
        final String uid6 = "targetuidexample6@gmail.com";
        final String uid7 = "targetuidexample7@gmail.com";

        TargetCustomerModel user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("FifthFistname");
        user.setLastname("Fifth");
        user.setName("FifthFistname Fifth");
        user.setUid(uid5);
        modelService.save(user);

        user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("SixthFistname");
        user.setLastname("Sixth");
        user.setName("SixthFistname Sixth");
        user.setUid(uid6);
        modelService.save(user);

        user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("SeventhFistname");
        user.setLastname("Seventh");
        user.setName("SeventhFistname Seventh");
        user.setUid(uid7);
        modelService.save(user);

        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "targetuidexample");
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        final List<DataObject<ItemModel>> result = searchResult.getResult();
        Assert.assertNotNull(result);

        int count = 0;
        for (int i = 0; i < result.size(); i++) {
            final TargetCustomerModel c = (TargetCustomerModel)result.get(i).getItem();
            if (uid5.equals(c.getUid()) || uid6.equals(c.getUid()) || uid7.equals(c.getUid())) {
                count++;
            }
        }
        Assert.assertEquals(3, count);
    }

    @Test
    public void testSearchAll() throws SearchException {
        final String uid8 = "TargetUIDExample8@gmail.com";
        final String uid9 = "TargetUIDExample9@gmail.com";
        TargetCustomerModel user = modelService.create(TargetCustomerModel.class);

        user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("Alex");
        user.setLastname("Eighth");
        user.setName("Alex Eighth");
        user.setUid(uid8);
        modelService.save(user);

        user = modelService.create(TargetCustomerModel.class);
        user.setFirstname("John");
        user.setLastname("Nineth");
        user.setName("John Nineth");
        user.setUid(uid9);
        modelService.save(user);
        //multiple spaces
        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "   ");
        CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        List<DataObject<ItemModel>> result = searchResult.getResult();
        Assert.assertNotNull(result);
        Assert.assertTrue("Search all fail", result.size() >= 2);

        //empty
        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, "");
        searchResult = searchService.search(command, pageable);
        result = searchResult.getResult();
        Assert.assertNotNull(result);
        Assert.assertTrue("Search all fail", result.size() >= 2);

        //null
        command.setText(DefaultCustomerSearchQueryBuilder.TextField.Name, null);
        searchResult = searchService.search(command, pageable);
        result = searchResult.getResult();
        Assert.assertNotNull(result);
        Assert.assertTrue("Search all fail", result.size() >= 2);

    }

}
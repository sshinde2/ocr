/**
 * 
 */
package au.com.target.tgtcs.search.services.query;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.cscockpit.model.data.DataObject;
import de.hybris.platform.cscockpit.services.search.CsSearchResult;
import de.hybris.platform.cscockpit.services.search.SearchException;
import de.hybris.platform.cscockpit.services.search.generic.DefaultCsFlexibleSearchService;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketPoolSearchQueryBuilder.SearchAgent;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketPoolSearchQueryBuilder.SearchGroup;
import de.hybris.platform.cscockpit.services.search.generic.query.DefaultTicketPoolSearchQueryBuilder.SearchStatus;
import de.hybris.platform.cscockpit.services.search.impl.DefaultCsTextSearchCommand;
import de.hybris.platform.cscockpit.services.search.impl.DefaultPageable;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.ticket.enums.CsTicketCategory;
import de.hybris.platform.ticket.enums.CsTicketPriority;
import de.hybris.platform.ticket.enums.CsTicketState;
import de.hybris.platform.ticket.model.CsAgentGroupModel;
import de.hybris.platform.ticket.model.CsTicketModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcs.search.services.query.TargetTicketPoolSearchQueryBuilder.DynamicSearchGroup;



/**
 * @author mjanarth
 *
 */
@IntegrationTest
public class TargetTicketPoolSearchQueryBuilderTest extends ServicelayerTransactionalTest {

    private static final String HEADLINE = "headline";

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Resource
    private SearchRestrictionService searchRestrictionService;

    @Resource
    private SessionService sessionService;

    @Resource
    private ModelService modelService;

    @Resource
    private UserService userService;

    private CsAgentGroupModel fraudAgentGrp;

    private final String frdAgentGrpName = "fraudGroup";

    private CsAgentGroupModel refundFailureAgentGrp;

    private final String refundFailedGrpName = "refundfailedusergrp";

    private final String ebaygrpName = "ebaygroup";

    private CsAgentGroupModel ebaycsAgentGrp;

    private EmployeeModel csagent;

    private EmployeeModel csadmin;

    private CsTicketModel searchResultTicket;

    private final DefaultCsTextSearchCommand command = new DefaultCsTextSearchCommand();

    private final DefaultCsFlexibleSearchService<DefaultCsTextSearchCommand, ItemModel> searchService = new DefaultCsFlexibleSearchService<DefaultCsTextSearchCommand, ItemModel>();//NOPMD

    private final DefaultPageable pageable = new DefaultPageable();

    private final List<String> ticketList = new ArrayList<>();

    private final TargetTicketPoolSearchQueryBuilder queryBuilder = new TargetTicketPoolSearchQueryBuilder();



    @Before
    public void setUp() throws Exception {
        fraudAgentGrp = modelService.create(CsAgentGroupModel.class);
        fraudAgentGrp.setUid(frdAgentGrpName);
        modelService.save(fraudAgentGrp);
        refundFailureAgentGrp = modelService.create(CsAgentGroupModel.class);
        refundFailureAgentGrp.setUid(refundFailedGrpName);
        modelService.save(refundFailureAgentGrp);
        ebaycsAgentGrp = modelService.create(CsAgentGroupModel.class);
        ebaycsAgentGrp.setUid(ebaygrpName);
        modelService.save(ebaycsAgentGrp);
        csagent = modelService.create(EmployeeModel.class);
        csagent.setUid("csagentest");
        modelService.save(csagent);

        csadmin = modelService.create(EmployeeModel.class);
        csadmin.setUid("csadmintest");
        modelService.save(csadmin);

        searchService.setFlexibleSearchService(flexibleSearchService);
        searchService.setSearchRestrictionService(searchRestrictionService);
        searchService.setSessionService(sessionService);
        queryBuilder.setUserService(userService);
        searchService.setFlexibleSearchQueryBuilder(queryBuilder); // target cscockpit query builder
        pageable.setPageNumber(0); //first page
        pageable.setPageSize(15);
    }

    private void searchAndCheckResult(final List<String> ticketIds) throws SearchException {
        final CsSearchResult<DefaultCsTextSearchCommand, ItemModel> searchResult = searchService.search(command,
                pageable);
        ItemModel item = null;
        final List<DataObject<ItemModel>> result = searchResult.getResult();
        Assert.assertEquals(ticketIds.size(), result.size());
        for (final DataObject<ItemModel> dataobject : result) {
            item = dataobject.getItem();
            Assert.assertTrue(item instanceof CsTicketModel);
            searchResultTicket = (CsTicketModel)item;
            Assert.assertTrue(ticketIds.contains(searchResultTicket.getTicketID()));
        }
    }

    private CsTicketModel createTicket(final CsAgentGroupModel agentGroup, final EmployeeModel agent,
            final CsTicketState state, final String ticketId) {
        final CsTicketModel csTicket = modelService.create(CsTicketModel.class);
        csTicket.setAssignedAgent(agent);
        csTicket.setAssignedGroup(agentGroup);
        csTicket.setState(state);
        csTicket.setTicketID(ticketId);
        fillCommonTicketFields(csTicket);
        return csTicket;
    }

    private void fillCommonTicketFields(final CsTicketModel csTicket) {
        csTicket.setCategory(CsTicketCategory.COMPLAINT);
        csTicket.setPriority(CsTicketPriority.LOW);
        csTicket.setHeadline(HEADLINE);
    }

    @Test
    public void testSearchTicketAgentCorUnAgentGrpAllgrp() throws SearchException {
        ticketList.add("10005");
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(ebaycsAgentGrp, null, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.CurrentUserOrUnassigned);
        command.addFlag(SearchGroup.AllGroups);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);

    }

    @Test
    public void testSearchTicketAgentCorUnAgentCustomgrp() throws SearchException {
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(ebaycsAgentGrp, null, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.CurrentUserOrUnassigned);
        command.setText(DynamicSearchGroup.SearchText, frdAgentGrpName);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);

    }

    @Test
    public void testSearchTicketAgentCorUnAgentAnyGrp() throws SearchException {
        ticketList.add("10005");
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(null, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(ebaycsAgentGrp, null, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.CurrentUserOrUnassigned);
        command.addFlag(SearchGroup.Any);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentAllUSerGrpAll() throws SearchException {
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, csadmin, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(ebaycsAgentGrp, null, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.AllUsers);
        command.addFlag(SearchGroup.AllGroups);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentAllUSerGrpAll2() throws SearchException {
        ticketList.add("10005");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, csadmin, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(ebaycsAgentGrp, csagent, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.AllUsers);
        command.addFlag(SearchGroup.AllGroups);
        command.addFlag(SearchStatus.Closed);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentAllUSerGrpAll3() throws SearchException {
        ticketList.add("10005");
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, csadmin, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(ebaycsAgentGrp, csagent, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        final CsTicketModel ticket = createTicket(fraudAgentGrp, null, CsTicketState.CLOSED, "10007");
        modelService.save(ticket);
        command.addFlag(SearchAgent.AllUsers);
        command.addFlag(SearchGroup.AllGroups);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentAllUSerGrpCustom() throws SearchException {
        ticketList.add("10005");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, csadmin, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(ebaycsAgentGrp, csagent, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.AllUsers);
        command.setText(DynamicSearchGroup.SearchText, ebaygrpName);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }


    @Test
    public void testSearchTicketAgentAnyUserAllGrp() throws SearchException {
        ticketList.add("10005");
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(ebaycsAgentGrp, csagent, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.Any);
        command.addFlag(SearchGroup.AllGroups);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentAnyUserUnassignedGrp() throws SearchException {
        ticketList.add("10005");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(null, csagent, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.Any);
        command.addFlag(SearchGroup.Unassigned);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentAnyUseAnyGrp() throws SearchException {
        ticketList.add("10005");
        ticketList.add("10001");
        ticketList.add("10007");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(null, csagent, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        final CsTicketModel refundTicket = createTicket(refundFailureAgentGrp, csadmin, CsTicketState.CLOSED, "10007");
        modelService.save(refundTicket);
        command.addFlag(SearchAgent.Any);
        command.addFlag(SearchGroup.Any);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentAnyUserCustomGrp() throws SearchException {
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(null, csagent, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.Any);
        command.setText(DynamicSearchGroup.SearchText, frdAgentGrpName);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentUnassignedUserAllGrp() throws SearchException {
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(null, csagent, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.Unassigned);
        command.addFlag(SearchGroup.AllGroups);
        command.addFlag(SearchStatus.Open);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentUnassignedUserUnaassignedGrp() throws SearchException {
        ticketList.add("10005");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(null, null, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.Unassigned);
        command.addFlag(SearchGroup.Unassigned);
        command.addFlag(SearchStatus.Closed);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentUnassignedUserAnyGrp() throws SearchException {
        ticketList.add("10001");
        ticketList.add("10005");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(null, null, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.Unassigned);
        command.addFlag(SearchGroup.Any);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }

    @Test
    public void testSearchTicketAgentUnassignedUserCustomGrp() throws SearchException {
        ticketList.add("10001");
        final CsTicketModel csTicket = createTicket(fraudAgentGrp, null, CsTicketState.OPEN, "10001");
        modelService.save(csTicket);
        final CsTicketModel ebayTicket = createTicket(null, null, CsTicketState.CLOSED, "10005");
        modelService.save(ebayTicket);
        command.addFlag(SearchAgent.Unassigned);
        command.setText(DynamicSearchGroup.SearchText, frdAgentGrpName);
        command.addFlag(SearchStatus.Any);
        searchAndCheckResult(ticketList);
    }
}

/**
 * 
 */
package au.com.target.tgtfacades.product.converters;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTest;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtfacades.product.data.TargetPOSProductData;
import au.com.target.tgtfacades.product.data.TargetPOSProductData.ErrorType;
import au.com.target.tgtsale.product.data.POSProduct;


/**
 * Test for TargetPOSProductConverter
 * 
 */
@IntegrationTest
public class TargetPOSProductConverterTest extends ServicelayerTest {

    private static final String DESCRIPTION = "description";
    private static final String ITEM_CODE = "1111";
    private static final Integer PRICE = Integer.valueOf(1076);
    private static final Integer WAS_PRICE = Integer.valueOf(1354);

    @Resource
    private TargetPOSProductConverter targetPOSProductConverter;

    @Test
    public void testConvert() {

        final POSProduct product = new POSProduct();

        product.setDescription(DESCRIPTION);
        product.setItemCode(ITEM_CODE);
        product.setPriceInCents(PRICE);
        product.setWasPriceInCents(WAS_PRICE);

        final TargetPOSProductData data = targetPOSProductConverter.convert(product);

        Assert.assertEquals(DESCRIPTION, data.getDescription());
        Assert.assertEquals(ITEM_CODE, data.getItemCode());
        Assert.assertNotNull(data.getPrice());
        Assert.assertNotNull(data.getWasPrice());
        Assert.assertEquals(PRICE.intValue(), data.getPrice().getValue().multiply(BigDecimal.valueOf(100)).intValue());
        Assert.assertEquals(WAS_PRICE.intValue(), data.getWasPrice().getValue().multiply(BigDecimal.valueOf(100))
                .intValue());
        Assert.assertEquals(ErrorType.NONE, data.getErrorType());
    }

    @Test
    public void testConvertNullPrices() {

        final POSProduct product = new POSProduct();

        product.setDescription(DESCRIPTION);
        product.setItemCode(ITEM_CODE);
        product.setPriceInCents(null);
        product.setWasPriceInCents(null);

        final TargetPOSProductData data = targetPOSProductConverter.convert(product);

        Assert.assertEquals(DESCRIPTION, data.getDescription());
        Assert.assertEquals(ITEM_CODE, data.getItemCode());
        Assert.assertNull(data.getPrice());
        Assert.assertNull(data.getWasPrice());
        Assert.assertEquals(ErrorType.NONE, data.getErrorType());
    }

    @Test
    public void testConvertNullWasPrice() {

        final POSProduct product = new POSProduct();

        product.setDescription(DESCRIPTION);
        product.setItemCode(ITEM_CODE);
        product.setPriceInCents(PRICE);
        product.setWasPriceInCents(null);

        final TargetPOSProductData data = targetPOSProductConverter.convert(product);

        Assert.assertEquals(DESCRIPTION, data.getDescription());
        Assert.assertEquals(ITEM_CODE, data.getItemCode());
        Assert.assertNotNull(data.getPrice());
        Assert.assertEquals(PRICE.intValue(), data.getPrice().getValue().multiply(BigDecimal.valueOf(100)).intValue());
        Assert.assertNull(data.getWasPrice());
        Assert.assertEquals(ErrorType.NONE, data.getErrorType());
    }

    @Test
    public void testConvertPosError() {

        final POSProduct product = new POSProduct();

        product.setError("Itemcode not found");

        final TargetPOSProductData data = targetPOSProductConverter.convert(product);

        Assert.assertNull(data.getDescription());
        Assert.assertNull(data.getItemCode());
        Assert.assertNull(data.getPrice());
        Assert.assertNull(data.getWasPrice());
        Assert.assertEquals("Itemcode not found", data.getFailureReason());
        Assert.assertEquals(ErrorType.POS, data.getErrorType());
    }

    @Test
    public void testConvertSystemError() {

        final POSProduct product = new POSProduct();

        product.setError("Store not found");
        product.setSystemError(true);

        final TargetPOSProductData data = targetPOSProductConverter.convert(product);

        Assert.assertNull(data.getDescription());
        Assert.assertNull(data.getItemCode());
        Assert.assertNull(data.getPrice());
        Assert.assertNull(data.getWasPrice());
        Assert.assertEquals("Store not found", data.getFailureReason());
        Assert.assertEquals(ErrorType.SYSTEM_ERROR, data.getErrorType());
    }



}

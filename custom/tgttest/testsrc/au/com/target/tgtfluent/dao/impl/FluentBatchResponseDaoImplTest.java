/**
 * 
 */
package au.com.target.tgtfluent.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.dao.FluentBatchResponseDao;
import au.com.target.tgtfluent.enums.FluentBatchStatus;
import au.com.target.tgtfluent.model.AbstractFluentBatchResponseModel;
import au.com.target.tgtfluent.model.LocationFluentBatchResponseModel;


/**
 * @author mgazal
 *
 */
@IntegrationTest
public class FluentBatchResponseDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private FluentBatchResponseDao fluentBatchResponseDao;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtfluent/test/testBatchResponse.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testFind() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final AbstractFluentBatchResponseModel updateStatusModel = fluentBatchResponseDao
                .find(LocationFluentBatchResponseModel._TYPECODE, "1", "1");
        assertThat(updateStatusModel).isInstanceOf(LocationFluentBatchResponseModel.class);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testFindFail() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        fluentBatchResponseDao.find(LocationFluentBatchResponseModel._TYPECODE, "3", "3");
    }

    @Test
    public void testFindByStatuses() {
        final List<AbstractFluentBatchResponseModel> batchResponses = fluentBatchResponseDao.find(
                LocationFluentBatchResponseModel._TYPECODE,
                Arrays.asList(FluentBatchStatus.PENDING, FluentBatchStatus.RUNNING));
        assertThat(batchResponses).isNotEmpty().hasSize(2);
    }

}

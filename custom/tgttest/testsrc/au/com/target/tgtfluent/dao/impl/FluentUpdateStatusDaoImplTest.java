/**
 * 
 */
package au.com.target.tgtfluent.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtfluent.dao.FluentUpdateStatusDao;
import au.com.target.tgtfluent.model.AbstractFluentUpdateStatusModel;
import au.com.target.tgtfluent.model.LocationFluentUpdateStatusModel;


/**
 * @author mgazal
 *
 */
@IntegrationTest
public class FluentUpdateStatusDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private FluentUpdateStatusDao fluentUpdateStatusDao;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtfluent/test/testUpdateStatus.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testFind() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final AbstractFluentUpdateStatusModel updateStatusModel = fluentUpdateStatusDao
                .find(LocationFluentUpdateStatusModel._TYPECODE, "7126");
        assertThat(updateStatusModel).isInstanceOf(LocationFluentUpdateStatusModel.class);
        assertThat(updateStatusModel.getFluentId()).isEqualTo("65789");
    }
}

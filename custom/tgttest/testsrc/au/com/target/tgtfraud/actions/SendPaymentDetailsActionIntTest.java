/**
 * 
 */
package au.com.target.tgtfraud.actions;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.task.RetryLaterException;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtbusproc.util.ProcessParameterTestUtil;
import au.com.target.tgtfraud.impl.TestFraudCheckService;


@IntegrationTest
public class SendPaymentDetailsActionIntTest extends ServicelayerTransactionalTest {

    @Resource
    private SendPaymentDetailsForFraudCheckAction sendPaymentDetailsForFraudCheckAction;

    @Resource
    private ModelService modelService;

    @Resource
    private BusinessProcessService businessProcessService;

    @Before
    public void setup() {
        // Inject TestRetryFraudCheckService into the action
        sendPaymentDetailsForFraudCheckAction.setTargetFraudService(new TestFraudCheckService());
    }


    @Test
    public void testFraudCheckAction() throws RetryLaterException, Exception {

        // Just check that the action runs without errors

        final OrderModel order = ProcessParameterTestUtil.createOrderForCode("testOrder", modelService);

        final OrderProcessModel opm = ProcessParameterTestUtil.createProcess(order, "process1", modelService,
                businessProcessService);

        final String transition = sendPaymentDetailsForFraudCheckAction.execute(opm);

        Assert.assertEquals("OK", transition);
    }

}

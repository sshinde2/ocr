/**
 * 
 */
package au.com.target.tgtfulfilment.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.deliveryzone.model.TargetZoneDeliveryModeModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtfulfilment.model.TargetCarrierModel;


/**
 * @author dcwillia
 * 
 */
@IntegrationTest
public class TargetCarrierDaoImplTest extends ServicelayerTransactionalTest {
    @Resource
    private TargetDeliveryService targetDeliveryService;

    @Resource
    private TargetCarrierDaoImpl targetCarrierDao;

    private TargetZoneDeliveryModeModel cncDelivery;
    private TargetZoneDeliveryModeModel homeDelivery;
    private TargetZoneDeliveryModeModel digitalDelivery;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/test/test-delivery-modes.impex", Charsets.UTF_8.name());
        importCsv("/test/test-targetCarriers.impex", Charsets.UTF_8.name());

        cncDelivery = (TargetZoneDeliveryModeModel)targetDeliveryService.getDeliveryModeForCode("click-and-collect");
        Assert.assertNotNull(cncDelivery);

        homeDelivery = (TargetZoneDeliveryModeModel)targetDeliveryService.getDeliveryModeForCode("home-delivery");
        Assert.assertNotNull(homeDelivery);

        digitalDelivery = (TargetZoneDeliveryModeModel)targetDeliveryService
                .getDeliveryModeForCode("digital-gift-card");
        Assert.assertNotNull(digitalDelivery);
    }

    @Test
    public void testGetApplicableCarriers() throws ImpExException {
        importCsv("/tgtinitialdata/import/carriers/targetCarriers.impex", Charsets.UTF_8.name());

        assertThat(targetCarrierDao.getApplicableCarriers(cncDelivery, false, true).get(0).getCode()).isEqualTo(
                "AustraliaPostInstoreCNC");
        assertThat(targetCarrierDao.getApplicableCarriers(cncDelivery, true, false).get(0).getCode())
                .isEqualTo("TollCnC");
        assertThat(targetCarrierDao.getApplicableCarriers(cncDelivery, false, false).get(0).getCode()).isEqualTo(
                "TollCnC");
        assertThat(targetCarrierDao.getApplicableCarriers(homeDelivery, false, true).get(0).getCode()).isEqualTo(
                "AustraliaPostInstoreHD");

        List<TargetCarrierModel> applicableCarriers = targetCarrierDao.getApplicableCarriers(homeDelivery, false,
                false);
        assertThat(applicableCarriers).isNotEmpty().hasSize(3);
        assertThat(applicableCarriers.get(0).getCode()).isEqualTo("StarTrackHD");

        applicableCarriers = targetCarrierDao.getApplicableCarriers(homeDelivery, true, false);
        assertThat(applicableCarriers).isNotEmpty().hasSize(1);
        assertThat(applicableCarriers.get(0).getCode()).isEqualTo("Toll");
    }

    @Test(expected = ModelNotFoundException.class)
    public void testGetApplicableCarriersWhenNoMatchingDeliveryModesHD() {
        Assert.assertTrue(targetCarrierDao.getApplicableCarriers(homeDelivery, true, true).isEmpty());
    }

    @Test(expected = ModelNotFoundException.class)
    public void testGetApplicableCarriersWhenNoMatchingDeliveryModesCNC() {
        Assert.assertTrue(targetCarrierDao.getApplicableCarriers(cncDelivery, true, true).isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetApplicableCarriersNull() {
        Assert.assertNull(targetCarrierDao.getApplicableCarriers(null, false, false));
    }

    @Test
    public void testGetNullCarrierCnc() {
        final String expected = "NullCnC";
        final TargetCarrierModel tcm = targetCarrierDao.getNullCarrier(cncDelivery);
        assertThat(tcm.getCode()).isEqualTo(expected);
    }

    @Test
    public void testGetNullCarrierDigitalGift() {
        final String expected = "NullDigGift";
        final TargetCarrierModel tcm = targetCarrierDao.getNullCarrier(digitalDelivery);
        assertThat(tcm.getCode()).isEqualTo(expected);
    }

}

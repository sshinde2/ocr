/**
 * 
 */
package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.dao.TargetFulfilmentPointOfServiceDao;

import com.google.common.base.Charsets;


/**
 * @author Nandini
 *
 */
@IntegrationTest
public class TargetFulfilmentPOSDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetFulfilmentPointOfServiceDao targetFulfilmentPointOfServiceDao;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testNTLFulfilmentData.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testGetNTLByRegion() {
        final RegionModel region = new RegionModel();
        region.setIsocode("AU-NSW");
        final TargetPointOfServiceModel result = targetFulfilmentPointOfServiceDao.getNTLByRegion(region);

        Assert.assertNotNull(result);
        Assert.assertEquals("NSWONLINEBNB", result.getName());
    }

    @Test
    public void testGetNTLByRegionForDisbledNTLCapability() {
        final RegionModel region = new RegionModel();
        region.setIsocode("AU-WA");
        final TargetPointOfServiceModel result = targetFulfilmentPointOfServiceDao.getNTLByRegion(region);

        Assert.assertNull(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNTLByRegionForNullRegion() {
        targetFulfilmentPointOfServiceDao.getNTLByRegion(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNTLByRegionForNullIsoCode() {
        final RegionModel region = new RegionModel();
        region.setIsocode(null);
        targetFulfilmentPointOfServiceDao.getNTLByRegion(region);
    }

    @Test
    public void testGetNTLByRegionForInvalidRegion() {
        final RegionModel region = new RegionModel();
        region.setIsocode("AU-VICO");
        final TargetPointOfServiceModel result = targetFulfilmentPointOfServiceDao.getNTLByRegion(region);

        Assert.assertNull(result);
    }

    @Test
    public void testGetNTLByRegionForNotAssociatedRegion() {
        final RegionModel region = new RegionModel();
        region.setIsocode("AU-VIC");
        final TargetPointOfServiceModel result = targetFulfilmentPointOfServiceDao.getNTLByRegion(region);

        Assert.assertNull(result);
    }

}

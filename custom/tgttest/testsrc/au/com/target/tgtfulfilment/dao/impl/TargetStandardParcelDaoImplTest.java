/**
 * 
 */
package au.com.target.tgtfulfilment.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.List;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Test;

import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;

import com.google.common.base.Charsets;


/**
 * @author smishra1
 *
 */
@IntegrationTest
public class TargetStandardParcelDaoImplTest extends ServicelayerTransactionalTest {
    @Resource
    private TargetStandardParcelDaoImpl targetStandardParcelDao;

    /**
     * Test to get the parcel details
     * 
     * @throws ImpExException
     *             Impex exception
     */
    @Test
    public void testGetAllParcelDetails() throws ImpExException {
        importCsv("/test/target-standard-parcel-details-test.impex", Charsets.UTF_8.name());
        final List<TargetStandardParcelModel> standardParcelDetailsList = targetStandardParcelDao
                .getEnabledTargetStandardParcels();
        Assert.assertEquals(standardParcelDetailsList.size(), 13);
    }

    /**
     * Test to check when no data is returned.
     * 
     */
    @Test
    public void testNoDataFoundForParcels() {
        final List<TargetStandardParcelModel> standardParcelDetailsList = targetStandardParcelDao
                .getEnabledTargetStandardParcels();
        Assert.assertNull(standardParcelDetailsList);
    }

    /**
     * Test the values to ensure correct data being returned and is in order.
     *
     * @throws ImpExException
     *             impex exception
     */
    @Test
    public void testDataAndOrderOfValues() throws ImpExException {
        int count = 1;
        importCsv("/test/target-standard-parcel-details-test.impex", Charsets.UTF_8.name());
        final List<TargetStandardParcelModel> standardParcelDetailsList = targetStandardParcelDao
                .getEnabledTargetStandardParcels();
        Assert.assertNotNull(standardParcelDetailsList);
        for (final TargetStandardParcelModel parcelModel : standardParcelDetailsList) {
            Assert.assertEquals(parcelModel.getOrder().intValue(), count++);
        }
    }

}
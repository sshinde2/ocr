/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtfulfilment.stock.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.util.Date;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetAddressModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtfulfilment.enums.IntegrationMethod;
import au.com.target.tgtfulfilment.stock.dao.TargetFulfilmentStockLevelDao;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


@IntegrationTest
public class TargetFulfilmentStockLevelDaoImplTest extends ServicelayerTransactionalTest {
    private static final String WAREHOUSE1CODE = "w1";
    private static final String VENDOR1CODE = "v1";

    @Resource
    private TargetFulfilmentStockLevelDao targetFulfilmentStockLevelDao;

    @Resource
    private ModelService modelService;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    private WarehouseModel warehouse1;
    private VendorModel vendor1;
    private CatalogModel catalog;
    private CatalogVersionModel catalogVersion;
    private PurchaseOptionConfigModel poc1;
    private CurrencyModel currency;
    private UserModel user;
    private UnitModel unit;
    private TargetSizeVariantProductModel variant;
    private ColourModel colourModel;
    private VariantTypeModel colourVariantType;
    private VariantTypeModel sizeVariantType;

    @Before
    public void setUp() throws Exception {
        vendor1 = createVendor(VENDOR1CODE);
        warehouse1 = createWarehouse(WAREHOUSE1CODE, vendor1, Boolean.TRUE);

        catalog = modelService.create(CatalogModel.class);
        catalog.setId("id1");
        modelService.save(catalog);
        catalogVersion = modelService.create(CatalogVersionModel.class);
        catalogVersion.setVersion("v1");
        catalogVersion.setCatalog(catalog);
        modelService.save(catalogVersion);

        poc1 = createPurchaseOptionConfig("poc1", true, warehouse1);

        currency = modelService.create(CurrencyModel.class);
        currency.setIsocode("test");
        currency.setSymbol("A$");
        modelService.save(currency);

        user = modelService.create(UserModel.class);
        user.setUid("mr.rondel");
        modelService.save(user);

        unit = modelService.create(UnitModel.class);
        unit.setCode("test");
        unit.setUnitType("pieces");
        modelService.save(unit);

        colourModel = modelService.create(ColourModel.class);
        colourModel.setCode("blue");
        colourModel.setName("Blue");
        modelService.save(colourModel);

        final VariantTypeModel example = new VariantTypeModel();
        example.setCode("TargetColourVariantProduct");
        colourVariantType = flexibleSearchService.getModelByExample(example);

        example.setCode("TargetSizeVariantProduct");
        sizeVariantType = flexibleSearchService.getModelByExample(example);

        variant = createVariant("p1001");
    }

    private WarehouseModel createWarehouse(final String code, final VendorModel vendor, final Boolean def) {
        final WarehouseModel res = modelService.create(WarehouseModel.class);
        res.setCode(code);
        res.setVendor(vendor);
        res.setDefault(def);
        res.setIntegrationMethod(IntegrationMethod.ESB);
        modelService.save(res);
        return res;
    }

    private VendorModel createVendor(final String code) {
        final VendorModel res = modelService.create(VendorModel.class);
        res.setCode(code);
        modelService.save(res);
        return res;
    }

    @Test
    public void testCalculateReservedStock() {
        createOrderForCode("order1", Long.valueOf(1), OrderStatus.CREATED, false, null); // shud be counted
        createOrderForCode("order2", Long.valueOf(2), OrderStatus.PENDING, false, null); // shud be counted
        createOrderForCode("order3", Long.valueOf(6), OrderStatus.CANCELLED, false, null);// shud not be counted
        createOrderForCode("order4", Long.valueOf(7), OrderStatus.INPROGRESS, true, ConsignmentStatus.CREATED);// shud be counted
        createOrderForCode("order5", Long.valueOf(3), OrderStatus.INPROGRESS, true,
                ConsignmentStatus.SENT_TO_WAREHOUSE);// shud be counted
        createOrderForCode("order6", Long.valueOf(9), OrderStatus.INPROGRESS, true,
                ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);// shud be counted
        createOrderForCode("order7", Long.valueOf(10), OrderStatus.INPROGRESS, true, ConsignmentStatus.PICKED);// shud not be counted
        createOrderForCode("order8", Long.valueOf(11), OrderStatus.INVOICED, true, ConsignmentStatus.PICKED);// shud not be counted
        createOrderForCode("order9", Long.valueOf(12), OrderStatus.COMPLETED, true, ConsignmentStatus.SHIPPED);// shud not be counted
        createOrderForCode("order10", Long.valueOf(13), OrderStatus.CANCELLED, true, ConsignmentStatus.CANCELLED);// shud not be counted
        createOrderForCode("order11", Long.valueOf(13), OrderStatus.INPROGRESS, true, ConsignmentStatus.WAVED);//should be counted

        final long result = targetFulfilmentStockLevelDao.calculateReservedStockAdjustment(variant, warehouse1);
        Assert.assertEquals(35, result);
    }

    private void createOrderForCode(final String code, final Long qty, final OrderStatus orderStatus,
            final boolean createConsignment,
            final ConsignmentStatus consignmentStatus) {

        final OrderModel order = new OrderModel();
        order.setCode(code);
        order.setCurrency(currency);
        order.setUser(user);
        order.setDate(new Date());
        order.setStatus(orderStatus);
        order.setPurchaseOptionConfig(poc1);
        modelService.save(order);

        final OrderEntryModel entry = modelService.create(OrderEntryModel.class);
        entry.setProduct(variant);
        entry.setOrder(order);
        entry.setQuantity(qty);
        entry.setUnit(unit);
        modelService.save(entry);

        final TargetAddressModel address = modelService.create(TargetAddressModel.class);
        address.setOwner(user);
        modelService.save(address);

        if (createConsignment) {
            final TargetConsignmentModel consignment = modelService.create(TargetConsignmentModel.class);
            consignment.setCode(code);
            consignment.setWarehouse(warehouse1);
            consignment.setShippingAddress(address);
            consignment.setOrder(order);
            consignment.setStatus(consignmentStatus);
            modelService.save(consignment);
        }
    }

    private PurchaseOptionConfigModel createPurchaseOptionConfig(final String code, final boolean active,
            final WarehouseModel warehouse) {

        final PurchaseOptionModel po = modelService.create(PurchaseOptionModel.class);
        po.setCode(code);
        po.setName(code);
        modelService.save(po);


        final PurchaseOptionConfigModel res = modelService.create(PurchaseOptionConfigModel.class);
        res.setCode(code);
        res.setPurchaseOption(po);
        if (!active) {
            res.setValidFrom(DateUtils.addDays(new Date(), -2));
            res.setValidTo(DateUtils.addDays(new Date(), -1));
        }
        res.setWarehouse(warehouse);
        modelService.save(res);
        return res;
    }

    private TargetSizeVariantProductModel createVariant(final String code) {
        final ProductTypeModel productType = modelService.create(ProductTypeModel.class);
        productType.setName(code);
        productType.setCode(code);
        productType.setBulky(Boolean.FALSE);
        final BrandModel brand = modelService.create(BrandModel.class);
        brand.setName(code);
        brand.setCode(code);

        final TargetProductModel baseProduct = modelService.create(TargetProductModel.class);
        baseProduct.setVariantType(colourVariantType);
        baseProduct.setCode(code);
        baseProduct.setCatalogVersion(catalogVersion);
        baseProduct.setProductType(productType);
        baseProduct.setBrand(brand);
        modelService.save(baseProduct);

        final TargetColourVariantProductModel colourVariant = modelService
                .create(TargetColourVariantProductModel.class);
        colourVariant.setCode(code + "c");
        colourVariant.setVariantType(sizeVariantType);
        colourVariant.setCatalogVersion(catalogVersion);
        colourVariant.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        colourVariant.setBaseProduct(baseProduct);
        colourVariant.setColour(colourModel);
        colourVariant.setSwatch(colourModel);
        modelService.save(colourVariant);

        final SizeTypeModel typeModel = modelService.create(SizeTypeModel.class);
        typeModel.setCode("Small");
        typeModel.setName("Small");
        modelService.save(typeModel);

        final TargetSizeVariantProductModel sizeVariant = modelService.create(TargetSizeVariantProductModel.class);
        sizeVariant.setCode(code + "s");
        sizeVariant.setCatalogVersion(catalogVersion);
        sizeVariant.setApprovalStatus(ArticleApprovalStatus.APPROVED);
        sizeVariant.setSize("S");
        sizeVariant.setBaseProduct(colourVariant);
        sizeVariant.setSizeType("Small");
        sizeVariant.setSizeClassification(typeModel);
        modelService.save(sizeVariant);
        return sizeVariant;
    }
}

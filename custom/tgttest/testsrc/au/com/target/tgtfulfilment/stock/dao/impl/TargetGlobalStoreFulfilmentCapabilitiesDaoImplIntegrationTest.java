package au.com.target.tgtfulfilment.stock.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Test;

import au.com.target.tgtfulfilment.dao.impl.TargetGlobalStoreFulfilmentCapabilitiesDaoImpl;
import au.com.target.tgtfulfilment.model.GlobalStoreFulfilmentCapabilitiesModel;


/**
 * Integration test for {TargetGlobalStoreFulfilmentCapabilitiesDaoImpl}.
 * 
 * @author ajit
 */
@IntegrationTest
public class TargetGlobalStoreFulfilmentCapabilitiesDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetGlobalStoreFulfilmentCapabilitiesDaoImpl targetGlobalStoreFulfilmentCapabilitiesDao;

    @Resource
    private ModelService modelService;

    @Test
    public void testGetGlobalStoreFulfilmentCapabilitiesNull() {
        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = targetGlobalStoreFulfilmentCapabilitiesDao
                .getGlobalStoreFulfilmentCapabilities();
        Assert.assertEquals(null, capabilitiesModel);
    }

    /**
     * Blackout end date should always be greater then blackout start date and end date should to be a future date.
     */
    @Test
    public void testGetGlobalStoreFulfilmentCapabilitiesWithFutureDate() throws ParseException {

        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = modelService
                .create(GlobalStoreFulfilmentCapabilitiesModel.class);
        capabilitiesModel.setCode("Test001");
        capabilitiesModel.setMaxOrderPeriodStartTime("00:00");
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        capabilitiesModel.setBlackoutStart(c.getTime());
        c.add(Calendar.DATE, 2);
        capabilitiesModel.setBlackoutEnd(c.getTime());
        capabilitiesModel.setMaxTimeToPick(Integer.valueOf(0));
        capabilitiesModel.setMaxItemsPerConsignment(Integer.valueOf(0));
        modelService.save(capabilitiesModel);

        assertGlobalBlackoutTimes(capabilitiesModel);
    }

    /**
     * Test get global store fulfilment capabilities using model with current date as start date(with future time) and
     * end day is future date.
     *
     * @throws ParseException
     *             the parse exception
     */
    @Test
    public void testGetGlobalStoreFulfilmentCapabilitiesWithCurrentDate() throws ParseException {

        final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel = modelService
                .create(GlobalStoreFulfilmentCapabilitiesModel.class);
        capabilitiesModel.setCode("Test002");
        capabilitiesModel.setMaxOrderPeriodStartTime("00:00");
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.HOUR, 1);
        capabilitiesModel.setBlackoutStart(c.getTime());
        c.add(Calendar.DATE, 2);
        capabilitiesModel.setBlackoutEnd(c.getTime());
        capabilitiesModel.setMaxItemsPerConsignment(Integer.valueOf(0));
        capabilitiesModel.setMaxTimeToPick(Integer.valueOf(0));
        modelService.save(capabilitiesModel);

        assertGlobalBlackoutTimes(capabilitiesModel);

    }

    private void assertGlobalBlackoutTimes(final GlobalStoreFulfilmentCapabilitiesModel capabilitiesModel) {

        final GlobalStoreFulfilmentCapabilitiesModel model = targetGlobalStoreFulfilmentCapabilitiesDao
                .getGlobalStoreFulfilmentCapabilities();

        Assert.assertNotNull(model);
        Assert.assertEquals(capabilitiesModel.getCode(), model.getCode());
        Assert.assertEquals(capabilitiesModel.getBlackoutEnd(), model.getBlackoutEnd());
        Assert.assertEquals(capabilitiesModel.getBlackoutStart(), model.getBlackoutStart());
        Assert.assertTrue(model.getBlackoutStart().before(model.getBlackoutEnd()));
        Assert.assertTrue(model.getBlackoutEnd().after(new Date()));

    }
}
/**
 * 
 */
package au.com.target.tgtjms.jms.email;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;
import javax.jms.Message;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtjms.email.TargetJmsEmailService;
import au.com.target.tgtjms.jms.gateway.JmsMessageDispatcher;
import au.com.target.tgtjms.jms.message.AbstractMessage;
import au.com.target.tgtmail.converter.EmailMessageConverter;
import au.com.target.tgtmail.model.Email;
import junit.framework.Assert;


/**
 * @author Olivier Lamy
 */
@IntegrationTest
public class TargetJmsEmailServiceTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetJmsEmailService targetJmsEmailService;

    private JmsMessageDispatcherInterceptor jmsMessageSenderGatewayInterceptor;

    private EmailMessageConverter emailMessageConverter;

    @Before
    public void setup() throws Exception {
        jmsMessageSenderGatewayInterceptor = new JmsMessageDispatcherInterceptor();
        targetJmsEmailService.setJmsMessageDispatcher(jmsMessageSenderGatewayInterceptor);
        emailMessageConverter = new EmailMessageConverter();
    }

    @Test
    public void testSendSimpleEmail() throws Exception {

        final EmailMessageModel emailMessageModel = new EmailMessageModel();
        final EmailAddressModel zorro = new EmailAddressModel();
        zorro.setDisplayName("zorro");
        zorro.setEmailAddress("zorro@foo.fr");
        emailMessageModel.setFromAddress(zorro);

        final EmailAddressModel toto = new EmailAddressModel();
        toto.setDisplayName("toto");
        toto.setEmailAddress("toto@foo.fr");
        emailMessageModel.setToAddresses(Arrays.asList(toto));

        emailMessageModel.setBody("Raise a glass");

        targetJmsEmailService.sendEmail(emailMessageModel);

        Assert.assertEquals(1, jmsMessageSenderGatewayInterceptor.jmsMessagesSended.size());

        final String xmlMessage = jmsMessageSenderGatewayInterceptor.jmsMessagesSended.get(0).message;

        final Email email = emailMessageConverter.convert(xmlMessage);

        Assert.assertEquals("zorro", email.getFromAddress().getDisplayName());

        Assert.assertEquals("zorro@foo.fr", email.getFromAddress().getAddress());

        Assert.assertEquals(1, email.getToAddresses().getEmailAddresses().size());

        Assert.assertEquals("toto", email.getToAddresses().getEmailAddresses().get(0).getDisplayName());

        Assert.assertEquals("toto@foo.fr", email.getToAddresses().getEmailAddresses().get(0).getAddress());

        Assert.assertEquals("Raise a glass", email.getBody());

    }


    private static class JmsMessageDispatcherInterceptor implements JmsMessageDispatcher {

        private final List<JMSMessageSended> jmsMessagesSended = new ArrayList<>();

        @Override
        public void send(final String queue, final String message) {
            this.jmsMessagesSended.add(new JMSMessageSended(message));
        }

        @Override
        public void send(final String queue, final AbstractMessage message) {
            // not available for testing currently
        }

        @Override
        public Message sendAndReceive(final String requestQueue, final String message, final String responseQueue) {
            // not available for testing currently
            return null;
        }

        private static class JMSMessageSended {
            private final String message;

            public JMSMessageSended(final String message) {
                this.message = message;
            }
        }

        /* (non-Javadoc)
         * @see au.com.target.tgtjms.jms.gateway.JmsMessageSenderGateway#sendMessageWithHeaders(java.lang.String, au.com.target.tgtjms.jms.message.AbstractMessage, java.util.Map)
         */
        @Override
        public void sendMessageWithHeaders(final String queue, final AbstractMessage message,
                final Map<String, String> headers) {
            // doing nothing

        }

    }



}

package au.com.target.tgtlayby.cart;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;


/**
 * Integration Test of {@link TargetLaybyCartService}
 */
@IntegrationTest
public class TargetCartServiceImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetLaybyCartService targetLaybyCartService;

    @Resource
    private UserService userService;

    @Resource
    private UnitService unitService;

    @Resource
    private ProductService productService;

    @Resource
    private FlexibleSearchService flexibleSearchService;

    @Before
    public void setUp() throws Exception {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtlayby/test/basicProductSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtlayby/test/testPurchaseOptions.impex", Charsets.UTF_8.name());

        // The Following Impex will create a test cart for 'anonymous' user with the following entries
        // Product: CP1002 Qty:2 PO: BuyNow
        importCsv("/tgtlayby/test/testTargetCart.impex", Charsets.UTF_8.name());

        // Setup data for testing getMasterCartForUser
        // Creates a cart for user 'cartuser'
        importCsv("/tgtlayby/test/testTargetCart2.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testAddNewEntryWithSamePurchaseOption() {
        final UserModel user = userService.getUserForUID("anonymous");
        final CartModel cart = user.getCarts().iterator().next();
        final ProductModel product = productService.getProductForCode("CP1002");
        final UnitModel unit = unitService.getUnitForCode("pieces");
        final CartEntryModel result = targetLaybyCartService.addNewEntry(cart, product, 1, unit);

        Assert.assertNotNull(result);
        Assert.assertEquals(cart, result.getOrder());
        Assert.assertEquals(product, result.getProduct());
        Assert.assertEquals(2, result.getQuantity().longValue());
    }

    @Test
    public void testGetMostRecentCartForCustomerWithNoCart() {
        final CustomerModel customer = (CustomerModel)userService.getUserForUID("nocartuser");
        final CartModel customerCart = targetLaybyCartService.getMostRecentCartForCustomer(customer);
        Assert.assertNull(customerCart);
    }

    @Test
    public void testGetMostRecentCartForCustomer() {
        final CustomerModel customer = (CustomerModel)userService.getUserForUID("cartuser");
        final CartModel customerCart = targetLaybyCartService.getMostRecentCartForCustomer(customer);
        Assert.assertNotNull(customerCart);
        Assert.assertEquals("recentCart", customerCart.getCode());
    }
}

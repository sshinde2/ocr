package au.com.target.tgtlayby.cart.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.cart.dao.TargetCartDao;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;

import com.google.common.base.Charsets;


/**
 * Integration test for {@link TargetCartDaoImpl}
 * 
 */
@IntegrationTest
public class TargetCartDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetCartDao targetCartDao;

    @Resource
    private ModelService modelService;

    @Resource
    private UserService userService;

    @Resource
    private PurchaseOptionService purchaseOptionService;

    @Resource
    private ProductService productService;

    private CartModel cart;

    @Before
    public void setUp() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtlayby/test/basicProductSetup.impex", Charsets.UTF_8.name());
        importCsv("/tgtlayby/test/testPurchaseOptions.impex", Charsets.UTF_8.name());

        // The Following Impex will create a test cart for 'anonymous' user with the following entries
        // Product: CP1002 Qty:2 PO: BuyNow
        importCsv("/tgtlayby/test/testTargetCart.impex", Charsets.UTF_8.name());

        final UserModel user = userService.getUserForUID("anonymous");
        cart = user.getCarts().iterator().next();

        // Setup data for testing getMasterCartForUser
        // Creates a cart for user 'cartuser'
        importCsv("/tgtlayby/test/testTargetCart2.impex", Charsets.UTF_8.name());

    }

    @Test
    public void testFindEntriesForProduct() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final ProductModel product = productService.getProductForCode("CP1002");
        final List<CartEntryModel> entries = targetCartDao.findEntriesForProduct(cart, product);
        Assert.assertNotNull(entries);
        Assert.assertEquals(1, entries.size());
    }

    @Test
    public void testGetMostRecentCartForUserWithNoCart()
    {
        final CustomerModel customer = (CustomerModel)userService.getUserForUID("nocartuser");
        final CartModel customerCart = targetCartDao.getMostRecentCartForUser(customer);
        Assert.assertNull(customerCart);
    }

    @Test
    public void testGetMostRecentCartForUser()
    {
        final CustomerModel customer = (CustomerModel)userService.getUserForUID("cartuser");
        final CartModel customerCart = targetCartDao.getMostRecentCartForUser(customer);
        Assert.assertNotNull(customerCart);
        Assert.assertEquals("recentCart", customerCart.getCode());
        Assert.assertEquals(customer, customerCart.getUser());
    }
}

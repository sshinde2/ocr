/**
 * 
 */
package au.com.target.tgtlayby.order;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Test;

import au.com.target.tgtcore.resumer.impl.PlaceOrderProcessResumer;
import au.com.target.tgtcore.resumer.impl.ReleasePreOrderProcessResumer;
import au.com.target.tgtpayment.enums.PaymentCaptureType;
import au.com.target.tgtpayment.resumer.PaymentProcessResumer;
import au.com.target.tgtpayment.resumer.PaymentProcessResumerFactory;


@IntegrationTest
public class OrderProcessResumerFactoryIntTest extends ServicelayerTransactionalTest {

    @Resource
    private PaymentProcessResumerFactory paymentProcessResumerFactory;

    @Test
    public void testPlaceOrderResumer() {
        final PaymentProcessResumer resumer = paymentProcessResumerFactory
                .getPaymentProcessResumer(PaymentCaptureType.PLACEORDER);
        assertThat(resumer).isNotNull();
        assertThat(resumer instanceof PlaceOrderProcessResumer).isTrue();
    }

    @Test
    public void testReleasePreOrderResumer() {
        final PaymentProcessResumer resumer = paymentProcessResumerFactory
                .getPaymentProcessResumer(PaymentCaptureType.RELEASEPREORDER);
        assertThat(resumer).isNotNull();
        assertThat(resumer instanceof ReleasePreOrderProcessResumer).isTrue();
    }
}

/**
 * 
 */
package au.com.target.tgtlayby.purchase;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;

import com.google.common.base.Charsets;


@IntegrationTest
public class PurchaseOptionConfigServiceTest extends ServicelayerTransactionalTest {

    @Resource
    private PurchaseOptionConfigService purchaseOptionConfigService;

    @Resource
    private ModelService modelService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtlayby/test/testPurchaseOptions.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testGetPurchaseOptionConfigByCode() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final PurchaseOptionConfigModel result = purchaseOptionConfigService
                .getPurchaseOptionConfigByCode("buynow-config1");
        Assert.assertEquals("buynow-config1", result.getCode());
    }

    @Test
    public void testGetActivePurchaseOptionConfigByCodeBuyNow() throws TargetUnknownIdentifierException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("buynow");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final PurchaseOptionConfigModel result = purchaseOptionConfigService
                .getActivePurchaseOptionConfigByPurchaseOption(option);
        Assert.assertEquals("buynow-config1", result.getCode());
    }

    @Test
    public void testGetActivePurchaseOptionConfigByCodeLayby() throws TargetUnknownIdentifierException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("layby");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final PurchaseOptionConfigModel result = purchaseOptionConfigService
                .getActivePurchaseOptionConfigByPurchaseOption(option);
        Assert.assertEquals("layby-config1", result.getCode());
    }


    @Test
    public void testGetActivePurchaseOptionConfigByCodeLongTermLayby() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("longtermlayby");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final PurchaseOptionConfigModel result = purchaseOptionConfigService
                .getActivePurchaseOptionConfigByPurchaseOption(option);
        Assert.assertEquals("longtermlayby-config1", result.getCode());
    }

    @Test
    public void getPurchaseOptionConfigForOrderPlacedDateBuyNow() throws TargetUnknownIdentifierException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("buynow");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final PurchaseOptionConfigModel result = purchaseOptionConfigService
                .getPurchaseOptionConfigForOrderPlacedDate(option, new Date());
        Assert.assertEquals("buynow-config1", result.getCode());
    }

    @Test
    public void getPurchaseOptionConfigForOrderPlacedDateLayBy() throws TargetUnknownIdentifierException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("layby");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final PurchaseOptionConfigModel result = purchaseOptionConfigService
                .getPurchaseOptionConfigForOrderPlacedDate(option, new Date());
        Assert.assertEquals("layby-config1", result.getCode());
    }

    @Test
    public void getPurchaseOptionConfigForOrderPlacedDateLongTermLayby() throws TargetUnknownIdentifierException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("longtermlayby");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final PurchaseOptionConfigModel result = purchaseOptionConfigService
                .getPurchaseOptionConfigForOrderPlacedDate(option, new Date());
        Assert.assertEquals("longtermlayby-config1", result.getCode());
    }

    @Test
    public void testGetAllPurchaseOptionConfigsLongTermLayby() throws ParseException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("longtermlayby");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final List<PurchaseOptionConfigModel> result = purchaseOptionConfigService
                .getAllPurchaseOptionConfigs(option);
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void testGetAllPurchaseOptionConfigsBuynow() throws ParseException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("buynow");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final List<PurchaseOptionConfigModel> result = purchaseOptionConfigService
                .getAllPurchaseOptionConfigs(option);
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void testGetAllPurchaseOptionConfigsLayby() throws ParseException {
        final PurchaseOptionModel example = new PurchaseOptionModel();
        example.setCode("layby");
        @SuppressWarnings("deprecation")
        final PurchaseOptionModel option = modelService.getByExample(example);
        final List<PurchaseOptionConfigModel> result = purchaseOptionConfigService
                .getAllPurchaseOptionConfigs(option);
        Assert.assertEquals(2, result.size());
    }
}

/**
 * 
 */
package au.com.target.tgtlayby.purchase.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.dao.PurchaseOptionConfigDao;

import com.google.common.base.Charsets;


@IntegrationTest
public class PurchaseOptionConfigDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private PurchaseOptionConfigDao purchaseOptionConfigDao;

    @Resource
    private ModelService modelService;

    @Resource
    private CommonI18NService commonI18NService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtcore/test/testSystemSetup.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testGetPurchaseOptionConfigForOrderPlacedDate() throws ParseException, TargetUnknownIdentifierException {
        final OrderModel order = createOrder();
        final PurchaseOptionConfigModel poc = createPurchaseOptionConfig();
        final PurchaseOptionModel po = poc.getPurchaseOption();
        final PurchaseOptionConfigModel result = purchaseOptionConfigDao.getPurchaseOptionConfigForOrderPlacedDate(po,
                order.getDate());
        Assert.assertEquals("test1", result.getCode());
    }

    private OrderModel createOrder() throws ParseException {
        final UserModel user = modelService.create(UserModel.class);
        user.setUid("fred");
        modelService.save(user);
        final OrderModel order = modelService.create(OrderModel.class);
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date date = sdf.parse("17/06/2013 11:59:00");
        order.setDate(date);
        order.setCurrency(commonI18NService.getCurrency("AUD"));
        order.setUser(user);
        modelService.save(order);
        return order;
    }

    private PurchaseOptionConfigModel createPurchaseOptionConfig() throws ParseException {

        final PurchaseOptionModel po = modelService.create(PurchaseOptionModel.class);
        po.setCode("test");
        modelService.save(po);
        final PurchaseOptionConfigModel poc = modelService.create(PurchaseOptionConfigModel.class);
        poc.setCode("test1");
        poc.setPurchaseOption(po);
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date fromDate = sdf.parse("14/06/2013 11:59:00");
        poc.setValidFrom(fromDate);
        final Date toDate = sdf.parse("19/06/2013 11:59:00");
        poc.setValidTo(toDate);
        modelService.save(poc);
        final PurchaseOptionConfigModel poc2 = modelService.create(PurchaseOptionConfigModel.class);
        poc2.setCode("test2");
        poc2.setPurchaseOption(po);
        final SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date fromDate2 = sdf2.parse("10/06/2013 11:59:00");
        poc2.setValidFrom(fromDate2);
        final Date toDate2 = sdf.parse("12/06/2013 11:59:00");
        poc2.setValidTo(toDate2);
        modelService.save(poc2);
        return poc;
    }
}

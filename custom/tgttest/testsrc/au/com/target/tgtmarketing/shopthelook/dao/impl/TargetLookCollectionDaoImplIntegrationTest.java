/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.AbstractConcurrentJaloSessionCallable;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookCollectionModel;
import au.com.target.tgtmarketing.shopthelook.dao.LookCollectionDao;


/**
 * 
 * Integration test for {@link LookCollectionDao}
 * 
 * @author mgazal
 *
 */
@IntegrationTest
public class TargetLookCollectionDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private LookCollectionDao lookCollectionDao;

    @Resource
    private ModelService modelService;

    @Test
    public void getExistingLookCollection()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final String code = "LC01";
        final TargetLookCollectionModel lookCollection = modelService.create(TargetLookCollectionModel.class);
        lookCollection.setId(code);
        lookCollection.setName("Look Collection 1");
        modelService.save(lookCollection);

        try {
            final TargetLookCollectionModel lookCollectionModel = lookCollectionDao.getLookCollectionForCode(code);
            Assert.assertNotNull(lookCollectionModel);
        }
        finally {
            modelService.remove(lookCollection);
        }
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testUnknownLookCollection()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookCollectionDao.getLookCollectionForCode("LC01");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLookCollectionMissingCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookCollectionDao.getLookCollectionForCode(null);
    }

    @Test
    public void testConcurrentCreateDoesNotProduceDuplicateLookCollections()
            throws InterruptedException, ExecutionException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final int noOfThreads = 2;
        // barrier to ensure both threads start at exactly the same time
        final CyclicBarrier barrier = new CyclicBarrier(noOfThreads);
        final String id = "LC01";
        final ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);
        final AbstractConcurrentJaloSessionCallable<TargetLookCollectionModel> createLookCollection = new AbstractConcurrentJaloSessionCallable() {

            @Override
            public TargetLookCollectionModel execute() throws InterruptedException, BrokenBarrierException,
                    TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
                final TargetLookCollectionModel lookCollection = modelService.create(TargetLookCollectionModel.class);
                lookCollection.setId(id);
                lookCollection.setName("Look Collection 1");

                barrier.await();
                try {
                    modelService.save(lookCollection);
                }
                catch (final ModelSavingException e) {
                    //ignore this
                }
                return lookCollectionDao.getLookCollectionForCode(id);
            }
        };

        final List<Future<TargetLookCollectionModel>> futures = new ArrayList<>();
        for (int i = 0; i < noOfThreads; i++) {
            futures.add(executor.submit(createLookCollection));
        }

        for (int i = 1; i < noOfThreads; i++) {
            Assert.assertEquals(futures.get(i).get(), futures.get(i - 1).get());
        }

        // if a concurrency issue does occur, a subsequent invocation would return null coz of ambiguous results
        final TargetLookCollectionModel lookCollection = lookCollectionDao.getLookCollectionForCode(id);
        Assert.assertNotNull(lookCollection);
        Assert.assertEquals(lookCollection, futures.get(0).get());
    }

}

/**
 * 
 */
package au.com.target.tgtmarketing.shopthelook.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Resource;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;

import au.com.target.AbstractConcurrentJaloSessionCallable;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.shopthelook.dao.LookProductDao;


/**
 * 
 * Integration test for {@link LookProductDao}
 * 
 * @author mgazal
 *
 */
@IntegrationTest
public class TargetLookProductDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    private static TargetLookModel look;

    @Resource
    private static ModelService modelService;

    @Resource
    private LookProductDao lookProductDao;

    @Override
    public void prepareApplicationContextAndSession() throws Exception {
        super.prepareApplicationContextAndSession();
        look = modelService.create(TargetLookModel.class);
        look.setId("L01");
        look.setName("Look 1");
        modelService.save(look);
    }

    @Test
    public void getExistingLookProduct()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final String code = "LP01";
        final TargetLookProductModel lookProduct = modelService.create(TargetLookProductModel.class);
        lookProduct.setProductCode(code);
        lookProduct.setLook(look);
        modelService.save(lookProduct);

        try {
            final TargetLookProductModel lookModel = lookProductDao.getLookProductForCodeAndLook(code, look);
            Assert.assertNotNull(lookModel);
        }
        finally {
            modelService.remove(lookProduct);
        }
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testUnknownLookProduct()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookProductDao.getLookProductForCodeAndLook("LP01", look);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLookProductMissingCode()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookProductDao.getLookProductForCodeAndLook(null, look);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLookProductMissingLook()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        lookProductDao.getLookProductForCodeAndLook("LP01", null);
    }

    @Test
    public void testConcurrentCreateDoesNotProduceDuplicateLookProducts()
            throws InterruptedException, ExecutionException, TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final int noOfThreads = 2;
        // barrier to ensure both threads start at exactly the same time
        final CyclicBarrier barrier = new CyclicBarrier(noOfThreads);
        final String id = "LP01";
        final ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);
        final AbstractConcurrentJaloSessionCallable<TargetLookProductModel> createLook = new AbstractConcurrentJaloSessionCallable() {

            @Override
            public TargetLookProductModel execute() throws InterruptedException, BrokenBarrierException,
                    TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
                final TargetLookProductModel lookProduct = modelService.create(TargetLookProductModel.class);
                lookProduct.setProductCode(id);
                lookProduct.setLook(look);

                barrier.await();
                try {
                    modelService.save(lookProduct);
                }
                catch (final ModelSavingException e) {
                    //ignore this
                }
                return lookProductDao.getLookProductForCodeAndLook(id, look);
            }
        };

        final List<Future<TargetLookProductModel>> futures = new ArrayList<>();
        for (int i = 0; i < noOfThreads; i++) {
            futures.add(executor.submit(createLook));
        }

        for (int i = 1; i < noOfThreads; i++) {
            Assert.assertEquals(futures.get(i).get(), futures.get(i - 1).get());
        }

        // if a concurrency issue does occur, a subsequent invocation would return null coz of ambiguous results
        final TargetLookProductModel lookProduct = lookProductDao.getLookProductForCodeAndLook(id, look);
        Assert.assertNotNull(lookProduct);
        Assert.assertEquals(lookProduct, futures.get(0).get());
    }

    @AfterClass
    public static void cleanup() {
        if (look != null) {
            modelService.remove(look);
        }
    }

}

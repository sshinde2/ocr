package au.com.target.tgtmarketing.social.dao.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.fest.assertions.Condition;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtmarketing.model.SocialMediaProductsModel;


@IntegrationTest
public class TargetSocialMediaProductsDaoImplTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetSocialMediaProductsDaoImpl targetSocialMediaProductsDao;

    private final Condition<List<?>> inDescendingOrder = new Condition<List<?>>() {

        /* (non-Javadoc)
         * @see org.fest.assertions.Condition#matches(java.lang.Object)
         */
        @Override
        public boolean matches(final List<?> paramT) {
            if (!isEveryElement(paramT, Date.class)) {
                throw new IllegalArgumentException();
            }

            final List<Date> newList = new ArrayList<Date>((List<Date>)paramT);

            Collections.sort(newList, Collections.reverseOrder()); // Default sort for dates is ascending, so reversing it is descending.
            return paramT.equals(newList);
        }

    };

    @Before
    public void setUp() throws Exception {
        importCsv("/tgtmarketing/test/testSocialMediaProducts.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testFindSocialMediaProductsByCreationTimeDescending() {
        final List<SocialMediaProductsModel> results = targetSocialMediaProductsDao
                .findSocialMediaProductsByCreationTimeDescending(5);

        assertThat(results).hasSize(5);
        assertThat(results).onProperty("url").containsExactly(
                "https://instagram.com/p/9Cfb-2GK9Q/", "https://instagram.com/p/9Ao3GLGKz9/",
                "https://instagram.com/p/8-YpK3mK0q/", "https://instagram.com/p/9Dm36yGK1O/",
                "https://instagram.com/p/9A9kRcGK6s/");

        assertThat(results).onProperty("creationtime").is(inDescendingOrder);
    }

    @Test
    public void testFindSocialMediaProductsByCreationTimeDescendingWithCountLessThanActualResults() {
        final List<SocialMediaProductsModel> results = targetSocialMediaProductsDao
                .findSocialMediaProductsByCreationTimeDescending(3);

        assertThat(results).hasSize(3);
        assertThat(results).onProperty("url").containsExactly(
                "https://instagram.com/p/9Cfb-2GK9Q/", "https://instagram.com/p/9Ao3GLGKz9/",
                "https://instagram.com/p/8-YpK3mK0q/");
        assertThat(results).onProperty("creationtime").is(inDescendingOrder);
    }

    @Test
    public void testFindSocialMediaProductsByCreationTimeDescendingWithCountGreaterThanActualResults() {
        final List<SocialMediaProductsModel> results = targetSocialMediaProductsDao
                .findSocialMediaProductsByCreationTimeDescending(20);

        assertThat(results).hasSize(5);
        assertThat(results).onProperty("url").containsExactly(
                "https://instagram.com/p/9Cfb-2GK9Q/", "https://instagram.com/p/9Ao3GLGKz9/",
                "https://instagram.com/p/8-YpK3mK0q/", "https://instagram.com/p/9Dm36yGK1O/",
                "https://instagram.com/p/9A9kRcGK6s/");
        assertThat(results).onProperty("creationtime").is(inDescendingOrder);
    }

    @Test
    public void testFindSocialMediaProductsByCreationTimeWithNoResults() throws ImpExException {
        importCsv("/tgtmarketing/test/testSocialMediaProducts-delete-all.impex", Charsets.UTF_8.name());

        final List<SocialMediaProductsModel> results = targetSocialMediaProductsDao
                .findSocialMediaProductsByCreationTimeDescending(20);

        assertThat(results).isEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindSocialMediaProductsByCreationTimeDescendingCountZero() {
        targetSocialMediaProductsDao.findSocialMediaProductsByCreationTimeDescending(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindSocialMediaProductsByCreationTimeDescendingCountNegative() {
        targetSocialMediaProductsDao.findSocialMediaProductsByCreationTimeDescending(-1);
    }

    private boolean isEveryElement(final List<?> theList, final Class theType) {
        for (final Object item : theList) {
            if (!theType.isInstance(item)) {
                return false;
            }
        }

        return true;
    }
}

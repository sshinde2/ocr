/**
 * 
 */
package au.com.target.tgtpayment.dao;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.junit.Test;

import au.com.target.tgtpayment.model.AfterpayConfigModel;


/**
 * @author mgazal
 *
 */
@IntegrationTest
public class AfterpayConfigDaoTest extends ServicelayerTransactionalTest {

    @Resource
    private AfterpayConfigDao afterpayConfigDao;

    @Resource
    private ModelService modelService;

    private AfterpayConfigModel afterpayConfigModel;

    @Test
    public void testFetchAfterPayConfig() {
        createAfterPayConfig();
        final AfterpayConfigModel afterpayConfig = afterpayConfigDao.getAfterpayConfig();
        assertThat(afterpayConfig).isNotNull();
        assertThat(afterpayConfig.getPaymentType()).isEqualTo("PAY_BY_INSTALLMENT");
        assertThat(afterpayConfig.getDescription()).isEqualTo("Pay over time");
        assertThat(afterpayConfig.getMinimumAmount()).isEqualByComparingTo(BigDecimal.valueOf(50));
        assertThat(afterpayConfig.getMaximumAmount()).isEqualByComparingTo(BigDecimal.valueOf(1000));
        deleteAfterPayConfig();
    }

    @Test
    public void testFetchAfterPayConfigEmpty() {
        final AfterpayConfigModel afterpayConfig = afterpayConfigDao.getAfterpayConfig();
        assertThat(afterpayConfig).isNull();
    }

    private void createAfterPayConfig() {
        afterpayConfigModel = modelService.create(AfterpayConfigModel.class);
        afterpayConfigModel.setPaymentType("PAY_BY_INSTALLMENT");
        afterpayConfigModel.setDescription("Pay over time");
        afterpayConfigModel.setMinimumAmount(BigDecimal.valueOf(50));
        afterpayConfigModel.setMaximumAmount(BigDecimal.valueOf(1000));
        modelService.save(afterpayConfigModel);
    }

    private void deleteAfterPayConfig() {
        if (afterpayConfigModel != null) {
            modelService.remove(afterpayConfigModel);
        }
    }
}

/**
 * 
 */
package au.com.target.tgtpaymentprovider.paypal.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTest;

import javax.annotation.Resource;

import org.fest.assertions.Assertions;
import org.junit.Ignore;
import org.junit.Test;

import urn.ebay.api.PayPalAPI.GetTransactionDetailsReq;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsRequestType;
import urn.ebay.api.PayPalAPI.GetTransactionDetailsResponseType;
import urn.ebay.api.PayPalAPI.SetExpressCheckoutResponseType;
import urn.ebay.apis.eBLBaseComponents.PaymentStatusCodeType;
import au.com.target.tgtpaymentprovider.paypal.PayPalService;
import au.com.target.tgtpaymentprovider.paypal.exception.InvalidRequestException;
import au.com.target.tgtpaymentprovider.paypal.exception.PayPalServiceException;


/**
 * @author bjames4
 * 
 */
@IntegrationTest
@Ignore
public class PayPalServiceIntegrationTest extends ServicelayerTest
{
    @Resource(name = "payPalService")
    private PayPalService payPalServiceImpl;

    /**
     * @throws PayPalServiceException
     *             Paypal exception
     */
    @Test
    public void testCallSetExpressCheckout() throws PayPalServiceException
    {
        final SetExpressCheckoutResponseType resp = payPalServiceImpl.setExpressCheckout(PaypalTransactionHelper
                .createSetExpCheckoutReq("10"));
        assertNotNull("Expecting non null response", resp);
        assertNotNull("Expecting non null token", resp.getToken());
        assertNotNull("Expecting non null ack", resp.getAck());
        Assertions.assertThat(resp.getErrors()).isEmpty();
    }

    /**
     * @throws PayPalServiceException
     *             Paypal exception
     */
    @Test
    public void testCallSetExpressCheckoutCentsAmount() throws PayPalServiceException
    {
        final SetExpressCheckoutResponseType resp = payPalServiceImpl.setExpressCheckout(PaypalTransactionHelper
                .createSetExpCheckoutReq("10.99"));
        assertNotNull("Expecting non null response", resp);
        assertNotNull("Expecting non null token", resp.getToken());
        assertNotNull("Expecting non null ack", resp.getAck());
        Assertions.assertThat(resp.getErrors()).isEmpty();
    }

    /**
     * @throws PayPalServiceException
     *             Paypal exception
     */
    @Test(expected = InvalidRequestException.class)
    public void testCallGetExpressCheckoutInvalidSession() throws PayPalServiceException
    {
        payPalServiceImpl.getCheckoutDetails(PaypalTransactionHelper
                .createGetExpCheckoutDetailsReq("EC-27118060MH4729531"));
    }

    /**
     * @throws PayPalServiceException
     *             Paypal exception
     */
    @Test(expected = PayPalServiceException.class)
    public void testCallDoExpressCheckoutInvalidSession() throws PayPalServiceException
    {
        payPalServiceImpl.doExpressCheckout(PaypalTransactionHelper.createDoExpCheckoutPaymentReq(
                "EC-27118060MH4729531", "YRDQ6WD68RSQQ", "10"));
    }

    /**
     * @throws PayPalServiceException
     *             -
     */
    @Test
    public void testRetrieveTransaction() throws PayPalServiceException
    {
        final GetTransactionDetailsRequestType requestType = new GetTransactionDetailsRequestType();
        requestType.setTransactionID("8CS35543VK1202200");
        requestType.setVersion("86");

        final GetTransactionDetailsReq request = new GetTransactionDetailsReq();
        request.setGetTransactionDetailsRequest(requestType);

        final GetTransactionDetailsResponseType response = payPalServiceImpl.retrieve(request);
        assertNotNull("Expecting non null response", response);
        assertNotNull("Expecting non null ack", response.getAck());
        assertEquals("Expecting completed", PaymentStatusCodeType.COMPLETED, response
                .getPaymentTransactionDetails().getPaymentInfo().getPaymentStatus());
        Assertions.assertThat(response.getErrors()).isEmpty();

    }
}

package au.com.target.tgtsale.tmd;


import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtsale.model.TargetTMDHotCardModel;



@IntegrationTest
public class TargetTMDHotCardDaoTest extends ServicelayerTransactionalTest {
    @Resource
    private TargetTMDHotCardDao targetTMDHotCardDao;

    @Resource
    private ModelService modelService;


    @Test
    public void testGetHotCardByCardNumberExists() throws Exception {
        final TargetTMDHotCardModel model = modelService.create(TargetTMDHotCardModel.class);
        model.setCardNum(Long.valueOf(1234567890123L));
        modelService.save(model);

        final TargetTMDHotCardModel hotCardModel = targetTMDHotCardDao
                .getTargetHotCardModelByCardNumber(1234567890123L);
        Assert.assertEquals(Long.valueOf(1234567890123L), hotCardModel.getCardNum());
    }


    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetHotCardByCardNumberNotExists() throws Exception {
        final TargetTMDHotCardModel model = modelService.create(TargetTMDHotCardModel.class);
        model.setCardNum(Long.valueOf(1234567890123L));
        modelService.save(model);

        targetTMDHotCardDao.getTargetHotCardModelByCardNumber(4567890123456L);
    }

}
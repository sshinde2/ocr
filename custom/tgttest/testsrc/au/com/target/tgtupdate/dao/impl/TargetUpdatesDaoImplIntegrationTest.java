/**
 * 
 */
package au.com.target.tgtupdate.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtupdate.dao.TargetUpdatesDao;
import au.com.target.tgtupdate.model.TargetUpdatesModel;


/**
 * @author pratik
 *
 */
@IntegrationTest
public class TargetUpdatesDaoImplIntegrationTest extends ServicelayerTransactionalTest {

    @Resource
    private ModelService modelService;

    @Resource
    private TargetUpdatesDao targetUpdatesDao;

    @Before
    public void setUp() throws Exception {
        final TargetUpdatesModel updatesModel = modelService.create(TargetUpdatesModel.class);
        updatesModel.setUpdateName("EXISTINGUPDATE");
        updatesModel.setDeploymentDate(new Date());
        modelService.save(updatesModel);
    }

    @Test
    public void testGetTargetUpdatesModelByIdWhenIdExists() {
        Assert.assertNotNull(targetUpdatesDao.getTargetUpdatesModelById("EXISTINGUPDATE"));
    }

    @Test
    public void testTargetUpdatesModelByIdWhenIdWhenIdDoesntExist() {
        Assert.assertNull(targetUpdatesDao.getTargetUpdatesModelById("NEWUPDATE"));
    }

}

/**
 * 
 */
package au.com.target.tgtwebcore.size.chart;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;

import com.google.common.base.Charsets;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@IntegrationTest
public class SizeChartServiceImplTest extends ServicelayerTransactionalTest {

    private static final String EXISTING_SIZE_LOW = "babysize";

    private static final String EXISTING_SIZE_CAMEL = "BabySize";

    private static final String NON_EXISTING_SIZE_CAMEL = "BigBabySize";

    @Resource
    private SizeChartService sizeChartService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtwebcore/import/common/size-chart.impex", Charsets.UTF_8.name());
    }

    @Test
    public void testFindSizeChart() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        Assert.assertNotNull(sizeChartService.getSizeChartBySizeName(EXISTING_SIZE_CAMEL));
        Assert.assertNotNull(sizeChartService.getSizeChartBySizeName(EXISTING_SIZE_LOW));
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testCantFindSizeChart() throws TargetAmbiguousIdentifierException, TargetUnknownIdentifierException {
        sizeChartService.getSizeChartBySizeName(NON_EXISTING_SIZE_CAMEL);
    }
}

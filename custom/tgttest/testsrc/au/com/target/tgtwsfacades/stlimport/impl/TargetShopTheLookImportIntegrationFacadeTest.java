/**
 * 
 */
package au.com.target.tgtwsfacades.stlimport.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.hamcrest.text.StringContains;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Charsets;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtmarketing.model.TargetLookModel;
import au.com.target.tgtmarketing.model.TargetLookProductModel;
import au.com.target.tgtmarketing.model.TargetShopTheLookModel;
import au.com.target.tgtmarketing.shopthelook.dao.LookCollectionDao;
import au.com.target.tgtmarketing.shopthelook.dao.LookProductDao;
import au.com.target.tgtmarketing.shopthelook.dao.ShopTheLookDao;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookCollectionDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationLookProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationShopTheLookDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSupercategoryDto;


/**
 * @author mgazal
 *
 */
@IntegrationTest
public class TargetShopTheLookImportIntegrationFacadeTest extends ServicelayerTransactionalTest {

    @Resource
    private TargetShopTheLookImportIntegrationFacade shopTheLookImportIntegrationFacade;

    @Resource
    private ShopTheLookDao shopTheLookDao;

    @Resource
    private LookCollectionDao lookCollectionDao;

    @Resource
    private LookProductDao lookProductDao;

    @Resource
    private UserService userService;

    @Before
    public void setup() throws ImpExException {
        importCsv("/tgtwsfacades/test/testStepStlImport.impex", Charsets.UTF_8.name());
        shopTheLookImportIntegrationFacade.setAssociateMedia(true);
        userService.setCurrentUser(userService.getUserForUID("esbstlupdate"));
    }

    @Test
    public void testImportWithMissingCategory()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(1, "C01", 0, 0, 0);

        final IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertThat(response.getMessages().get(0), new StringContains("Category with code 'C01' not found"));
    }

    @Test
    public void testImportWithMissingLookCollections()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(1, "CAT111", 0, 0, 0);

        final IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        final TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(0, shopTheLook.getCollections().size());
    }

    @Test
    public void testUpdateCollections() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(2, "CAT111", 1, 0, 0);

        IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());

        dto.setCollections(Collections.<IntegrationLookCollectionDto> emptyList());
        response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals("Existing looks shouldn't be removed", 1, shopTheLook.getCollections().size());
    }

    @Test
    public void testOverwriteLooks() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(3, "CAT111", 1, 1, 0);

        IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());
        Assert.assertEquals(1, shopTheLook.getCollections().get(0).getLooks().size());

        dto.getCollections().get(0).setLooks(Collections.<IntegrationLookDto> emptyList());
        response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals("Existing looks shouldn't be removed", 1, shopTheLook.getCollections().size());
        Assert.assertEquals("Existing looks shouldn't be removed", 1,
                shopTheLook.getCollections().get(0).getLooks().size());
    }

    @Test
    public void testSkipLookCollectionsWithError()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(4, "CAT111", 1, 0, 0);
        // add empty look collection
        dto.getCollections().add(new IntegrationLookCollectionDto());

        final IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        final TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals("Expect 1 look, the other will not be saved", 1, shopTheLook.getCollections().size());
    }

    @Test
    public void testSkipLooksWithError() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(5, "CAT111", 1, 1, 0);
        // add empty look
        dto.getCollections().get(0).getLooks().add(new IntegrationLookDto());

        final IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        final TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());
        Assert.assertEquals("Expect 1 look, the other will not be saved", 1,
                shopTheLook.getCollections().get(0).getLooks().size());
    }

    @Test
    public void testSkipProductsWithError()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(6, "CAT111", 1, 1, 1);
        // add empty product
        dto.getCollections().get(0).getLooks().get(0).getProducts().add(new IntegrationLookProductDto());

        final IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        final TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());
        Assert.assertEquals(1, shopTheLook.getCollections().get(0).getLooks().size());
        Assert.assertEquals("Expect 1 look product, the other will not be saved", 1,
                shopTheLook.getCollections().get(0).getLooks().iterator().next().getProducts().size());
    }

    @Test
    public void testOverwriteProducts() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(7, "CAT111", 1, 1, 2);

        IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());
        Assert.assertEquals(1, shopTheLook.getCollections().get(0).getLooks().size());
        Assert.assertEquals(2, shopTheLook.getCollections().get(0).getLooks().iterator().next().getProducts().size());

        dto.getCollections().get(0).getLooks().get(0).setProducts(new ArrayList<IntegrationLookProductDto>());
        dto.getCollections().get(0).getLooks().get(0).getProducts()
                .add(new IntegrationLookProductDto("STL07LC01L01LP03"));

        response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());
        Assert.assertEquals(1, shopTheLook.getCollections().get(0).getLooks().size());
        Assert.assertEquals(1, shopTheLook.getCollections().get(0).getLooks().iterator().next().getProducts().size());
        Assert.assertEquals("STL07LC01L01LP03",
                shopTheLook.getCollections().get(0).getLooks().iterator().next().getProducts().iterator().next()
                        .getProductCode());

        final TargetLookModel look = shopTheLook.getCollections().get(0).getLooks().iterator().next();
        TargetLookProductModel product = null;
        try {
            product = lookProductDao.getLookProductForCodeAndLook("STL07LC01L01LP01", look);
        }
        catch (final TargetUnknownIdentifierException e) {
            Assert.assertNotNull(e);
        }
        Assert.assertNull(product);
        try {
            product = lookProductDao.getLookProductForCodeAndLook("STL07LC01L01LP02", look);
        }
        catch (final TargetUnknownIdentifierException e) {
            Assert.assertNotNull(e);
        }
        Assert.assertNull(product);
    }

    @Test
    public void testLookMediaImport() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(8, "CAT111", 1, 1, 0);
        final IntegrationLookDto look = dto.getCollections().get(0).getLooks().get(0);
        look.setStartDate("2016-10-10 11:28:33");
        look.setImages("IMG123");

        final IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        final TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());
        Assert.assertEquals(1, shopTheLook.getCollections().get(0).getLooks().size());
        final TargetLookModel lookModel = shopTheLook.getCollections().get(0).getLooks().iterator().next();
        final Calendar startDate = Calendar.getInstance();
        Assert.assertNotNull(look.getStartDate());
        startDate.setTime(lookModel.getStartDate());
        Assert.assertEquals(2016, startDate.get(Calendar.YEAR));
        Assert.assertEquals(9, startDate.get(Calendar.MONTH));
        Assert.assertEquals(10, startDate.get(Calendar.DAY_OF_MONTH));
        Assert.assertEquals(11, startDate.get(Calendar.HOUR));
        Assert.assertEquals(28, startDate.get(Calendar.MINUTE));
        Assert.assertEquals(33, startDate.get(Calendar.SECOND));
        Assert.assertEquals("Online", lookModel.getImages().getCatalogVersion().getVersion());
        for (final MediaModel media : shopTheLook.getCollections().get(0).getLooks().iterator().next().getImages()
                .getMedias()) {
            Assert.assertEquals("Online", media.getCatalogVersion().getVersion());
        }
    }

    @Test
    public void testLookMediaImportDuplicate()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(9, "CAT111", 1, 2, 0);
        IntegrationLookDto look = dto.getCollections().get(0).getLooks().get(0);
        look.setImages("IMG123");
        look = dto.getCollections().get(0).getLooks().get(1);
        look.setImages("IMG123");

        final IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        final TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());
        Assert.assertEquals(2, shopTheLook.getCollections().get(0).getLooks().size());

        for (final TargetLookModel lookModel : shopTheLook.getCollections().get(0).getLooks()) {
            Assert.assertEquals("Online",
                    lookModel.getImages().getCatalogVersion().getVersion());
            for (final MediaModel media : lookModel.getImages().getMedias()) {
                Assert.assertEquals("Online", media.getCatalogVersion().getVersion());
            }
        }
    }

    @Test
    public void testLookWithSizeOnlyProductVariant()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {
        final IntegrationShopTheLookDto dto = makeDto(10, "CAT111", 1, 1, 2);
        dto.getCollections().get(0).getLooks().get(0).getProducts().get(0).setCode("TEST1001_1");
        dto.getCollections().get(0).getLooks().get(0).getProducts().get(1).setCode("TEST1002_1_1");

        final IntegrationResponseDto response = shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertTrue(response.isSuccessStatus());
        final TargetShopTheLookModel shopTheLook = shopTheLookDao.getShopTheLookForCode(dto.getCode());
        Assert.assertNotNull(shopTheLook);
        Assert.assertEquals(dto.getName(), shopTheLook.getName());
        Assert.assertEquals(1, shopTheLook.getCollections().size());
        Assert.assertEquals(1, shopTheLook.getCollections().get(0).getLooks().size());
        final TargetLookModel look = shopTheLook.getCollections().get(0).getLooks().iterator().next();
        Assert.assertEquals(2, look.getProducts().size());
        Assertions.assertThat(look.getProducts()).onProperty("productCode").contains("TEST1001_1", "TEST1002");
    }

    private IntegrationShopTheLookDto makeDto(final int sequenceNo, final String category, final int noOfCollections,
            final int noOfLooks, final int noOfProducts) {
        final IntegrationShopTheLookDto dto = new IntegrationShopTheLookDto();
        final String code = "STL" + StringUtils.leftPad("" + sequenceNo, 2, '0');

        final String name = "Shop the Look " + sequenceNo;
        dto.setCode(code);
        dto.setName(name);
        dto.setSupercategory(new IntegrationSupercategoryDto(category));
        final List<IntegrationLookCollectionDto> collections = new ArrayList<>();
        for (int i = 0; i < noOfCollections; i++) {
            final IntegrationLookCollectionDto collection = new IntegrationLookCollectionDto();
            collection.setCode(code + "LC" + StringUtils.leftPad("" + (i + 1), 2, '0'));
            collection.setName("Look Collection " + (i + 1));
            final List<IntegrationLookDto> looks = new ArrayList<>();
            for (int j = 0; j < noOfLooks; j++) {
                final IntegrationLookDto look = new IntegrationLookDto();
                look.setCode(collection.getCode() + "L" + StringUtils.leftPad("" + (j + 1), 2, '0'));
                look.setName("Look (j + 1)");
                final List<IntegrationLookProductDto> products = new ArrayList<>();
                for (int k = 0; k < noOfProducts; k++) {
                    products.add(new IntegrationLookProductDto(
                            look.getCode() + "P" + StringUtils.leftPad("" + (k + 1), 2, '0')));
                }
                look.setProducts(products);
                looks.add(look);
            }
            collection.setLooks(looks);
            collections.add(collection);
        }
        dto.setCollections(collections);
        return dto;
    }
}

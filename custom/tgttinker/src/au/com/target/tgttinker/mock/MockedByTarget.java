/**
 * 
 */
package au.com.target.tgttinker.mock;

/**
 * Any class implementing this is one of our mocking classes.
 * 
 */
public interface MockedByTarget {
    /**
     * Check if the mock is currently enabled.
     * 
     * @return true if mock is currently enabled else false.
     */
    boolean isActive();

    /**
     * Turn mocking of this feature on or off.
     * 
     * @param active
     *            true to mock, false to use real implementation
     */
    void setActive(final boolean active);
}

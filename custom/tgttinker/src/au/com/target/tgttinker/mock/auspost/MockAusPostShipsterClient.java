/**
 * 
 */
package au.com.target.tgttinker.mock.auspost;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import au.com.target.tgtauspost.deliveryclub.client.impl.AusPostShipsterClientImpl;
import au.com.target.tgtauspost.deliveryclub.dto.AbstractShipsterResponseDTO;
import au.com.target.tgtauspost.deliveryclub.dto.ShipsterErrorResponseDTO;
import au.com.target.tgtauspost.deliveryclub.dto.ShipsterVerifyEmailResponseDTO;
import au.com.target.tgtauspost.exception.AuspostShipsterClientException;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderRequestDTO;
import au.com.target.tgtauspost.integration.dto.ShipsterConfirmOrderResponseDTO;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 * @author gsing236
 *
 */
public class MockAusPostShipsterClient extends AusPostShipsterClientImpl implements MockedByTarget {

    private boolean active;

    private boolean verifyEmailNotAvailable = false;

    private boolean confirmOrderServiceNotAvailable;

    private boolean confirmOrderTimeOut;

    private boolean verifyEmailMock = false;

    private boolean confirmOrderMock = false;

    private ShipsterVerifyEmailResponseDTO verifyEmailResponse;

    private ShipsterConfirmOrderResponseDTO confirmOrderResponse;

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;

    }

    @Override
    protected ResponseEntity<ShipsterVerifyEmailResponseDTO> verifyEmailRestTemplate(final String email) {

        if (active && verifyEmailMock) {
            if (verifyEmailNotAvailable) {
                throw new RestClientException("Service unavailable by mock");
            }
            else {
                final ResponseEntity<ShipsterVerifyEmailResponseDTO> entityVerifyEmailResponse = new ResponseEntity<>(
                        verifyEmailResponse, getHttpStatus(verifyEmailResponse));
                return entityVerifyEmailResponse;
            }
        }
        else {
            return super.verifyEmailRestTemplate(email);
        }
    }

    @Override
    protected ResponseEntity<ShipsterConfirmOrderResponseDTO> confirmOrderRestTemplate(
            final ShipsterConfirmOrderRequestDTO request)
            throws AuspostShipsterClientException {

        if (active && confirmOrderMock) {
            if (confirmOrderServiceNotAvailable) {
                throw new RestClientException("Service unavailable by mock");
            }
            else if (confirmOrderTimeOut) {
                throw new RestClientException("Service Call timeout");
            }
            else {
                final ResponseEntity<ShipsterConfirmOrderResponseDTO> entityConfirmOrderResponse = new ResponseEntity<>(
                        confirmOrderResponse, getHttpStatus(confirmOrderResponse));
                return entityConfirmOrderResponse;
            }
        }
        else {
            return super.confirmOrderRestTemplate(request);
        }

    }

    private HttpStatus getHttpStatus(final AbstractShipsterResponseDTO responseDTO) {
        if (CollectionUtils.isEmpty(responseDTO.getErrors())) {
            return HttpStatus.OK;
        }
        final ShipsterErrorResponseDTO errorDto = responseDTO.getErrors().get(0);
        final String id = errorDto.getId();
        if (StringUtils.isNumeric(id)) {
            return HttpStatus.valueOf(Integer.valueOf(id).intValue());
        }
        return HttpStatus.BAD_REQUEST;

    }

    public void resetDefault() {
        active = false;
        verifyEmailMock = false;
        confirmOrderMock = false;
        confirmOrderTimeOut = false;
        verifyEmailNotAvailable = false;
        confirmOrderServiceNotAvailable = false;
    }

    public void activateMocks() {
        active = true;
        verifyEmailMock = true;
        verifyEmailNotAvailable = true;
    }

    /**
     * @return the verifyEmailNotAvailable
     */
    public boolean isVerifyEmailNotAvailable() {
        return verifyEmailNotAvailable;
    }

    /**
     * @return the verifyEmailMock
     */
    public boolean isVerifyEmailMock() {
        return verifyEmailMock;
    }

    /**
     * @param verifyEmailNotAvailable
     *            the verifyEmailNotAvailable to set
     */
    public void setVerifyEmailNotAvailable(final boolean verifyEmailNotAvailable) {
        this.verifyEmailNotAvailable = verifyEmailNotAvailable;
    }

    /**
     * @param verifyEmailMock
     *            the verifyEmailMock to set
     */
    public void setVerifyEmailMock(final boolean verifyEmailMock) {
        this.verifyEmailMock = verifyEmailMock;
    }

    /**
     * @return the confirmOrderMock
     */
    public boolean isConfirmOrderMock() {
        return confirmOrderMock;
    }

    /**
     * @param confirmOrderMock
     *            the confirmOrderMock to set
     */
    public void setConfirmOrderMock(final boolean confirmOrderMock) {
        this.confirmOrderMock = confirmOrderMock;
    }

    /**
     * @return the confirmOrderServiceNotAvailable
     */
    public boolean isConfirmOrderServiceNotAvailable() {
        return confirmOrderServiceNotAvailable;
    }

    /**
     * @param confirmOrderServiceNotAvailable
     *            the confirmOrderServiceNotAvailable to set
     */
    public void setConfirmOrderServiceNotAvailable(final boolean confirmOrderServiceNotAvailable) {
        this.confirmOrderServiceNotAvailable = confirmOrderServiceNotAvailable;
    }

    /**
     * @return the verifyEmailResponse
     */
    public ShipsterVerifyEmailResponseDTO getVerifyEmailResponse() {
        return verifyEmailResponse;
    }

    /**
     * @param verifyEmailResponse
     *            the verifyEmailResponse to set
     */
    public void setVerifyEmailResponse(final ShipsterVerifyEmailResponseDTO verifyEmailResponse) {
        this.verifyEmailResponse = verifyEmailResponse;
    }

    /**
     * @return the confirmOrderResponse
     */
    public ShipsterConfirmOrderResponseDTO getConfirmOrderResponse() {
        return confirmOrderResponse;
    }

    /**
     * @param confirmOrderResponse
     *            the confirmOrderResponse to set
     */
    public void setConfirmOrderResponse(final ShipsterConfirmOrderResponseDTO confirmOrderResponse) {
        this.confirmOrderResponse = confirmOrderResponse;
    }

    /**
     * @return the confirmOrderTimeOut
     */
    public boolean isConfirmOrderTimeOut() {
        return confirmOrderTimeOut;
    }

    /**
     * @param confirmOrderTimeOut
     *            the confirmOrderTimeOut to set
     */
    public void setConfirmOrderTimeOut(final boolean confirmOrderTimeOut) {
        this.confirmOrderTimeOut = confirmOrderTimeOut;
    }
}

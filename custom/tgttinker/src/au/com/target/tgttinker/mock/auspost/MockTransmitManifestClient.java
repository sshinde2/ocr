/**
 * 
 */
package au.com.target.tgttinker.mock.auspost;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.integration.dto.StoreDTO;
import au.com.target.tgtauspost.integration.dto.TargetManifestDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.manifest.client.impl.AusPostTransmitManifestClientImpl;


/**
 * Mock for TransmitManifestClient
 * 
 * @author steveb
 *
 */
public class MockTransmitManifestClient extends AusPostTransmitManifestClientImpl implements MockedByTarget {

    private static final Logger LOG = Logger.getLogger(MockTransmitManifestClient.class);

    private Map<String, String> expectedResponsesForManifest;

    public enum TransmitManifestResponseType {
        SUCCESS, ERROR, UNAVAILABLE
    }

    private TransmitManifestResponseType responseType = TransmitManifestResponseType.SUCCESS;

    private AuspostRequestDTO lastRequest;

    private boolean active;

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    @Override
    public ManifestResponseDTO transmitManifest(final AuspostRequestDTO request) {

        if (!active) {
            return super.transmitManifest(request);
        }

        lastRequest = request;

        final String requestJSON = getLastRequestAsJSON();
        if (MapUtils.isNotEmpty(expectedResponsesForManifest)) {
            final String manifestID = getManifestID(request);
            if (expectedResponsesForManifest.containsKey(manifestID)) {
                responseType = MockTransmitManifestClient.TransmitManifestResponseType
                        .valueOf(expectedResponsesForManifest.get(manifestID).toUpperCase());
            }
        }
        LOG.info("MockTransmitManifestClientImpl received request. Response=" + responseType
                + ", Request=" + requestJSON);

        return getManifestResponse();
    }


    /**
     * @param request
     * @return manifest number else return null
     */
    private String getManifestID(final AuspostRequestDTO request) {
        final StoreDTO store = request.getStore();
        if (store != null) {
            final TargetManifestDTO manifest = store.getManifest();
            if (manifest != null) {
                return manifest.getManifestNumber();
            }
        }
        return null;
    }

    /**
     * @return the lastRequest
     */
    public AuspostRequestDTO getLastRequest() {
        return lastRequest;
    }

    /**
     * @param responseType
     *            the responseType to set
     */
    public void setResponseType(final TransmitManifestResponseType responseType) {
        this.responseType = responseType;
    }

    /**
     * Reset this mock
     */
    public void reset() {
        lastRequest = null;
        setResponseType(TransmitManifestResponseType.SUCCESS);
        setExpectedResponsesForManifest(new HashMap<String, String>());
    }


    private ManifestResponseDTO getManifestResponse() {

        final ManifestResponseDTO response;

        switch (responseType) {

            case SUCCESS:
                response = createSuccessResponse();
                break;
            case ERROR:
                response = createErrorResponse();
                break;
            case UNAVAILABLE:
                response = createUnavailableResponse();
                break;
            default:
                throw new IllegalStateException("Unrecognised response type: " + responseType);
        }

        return response;
    }

    private ManifestResponseDTO createSuccessResponse() {
        final ManifestResponseDTO response = new ManifestResponseDTO();
        response.setSuccess(true);
        return response;
    }

    private ManifestResponseDTO createErrorResponse() {
        final ManifestResponseDTO response = new ManifestResponseDTO();
        response.setErrorType(IntegrationError.REMOTE_SERVICE_ERROR);
        response.setErrorCode("ERROR");
        response.setErrorValue("Mock Error");
        response.setSuccess(false);
        return response;
    }

    private ManifestResponseDTO createUnavailableResponse() {
        final ManifestResponseDTO response = new ManifestResponseDTO();
        response.setErrorType(IntegrationError.CONNECTION_ERROR);
        response.setErrorCode("UNAVAILABLE");
        response.setErrorValue("WebMethods not available");
        response.setSuccess(false);
        return response;
    }

    private String getLastRequestAsJSON() {

        final String requestJSON;
        try {
            final ObjectMapper mapper = new ObjectMapper();
            requestJSON = mapper.writeValueAsString(lastRequest);
        }
        catch (final Exception e) {
            throw new IllegalStateException("Could not convert request to JSON");
        }

        return requestJSON;
    }

    /**
     * @return the expectedResponsesForManifest
     */
    public Map<String, String> getExpectedResponsesForManifest() {
        return expectedResponsesForManifest;
    }

    /**
     * @param expectedResponsesForManifest
     *            the expectedResponsesForManifest to set
     */
    public void setExpectedResponsesForManifest(final Map<String, String> expectedResponsesForManifest) {
        this.expectedResponsesForManifest = expectedResponsesForManifest;
    }


}

/**
 * 
 */
package au.com.target.tgttinker.mock.customer;

import au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto;
import au.com.target.tgtmail.dto.TargetCustomerSubscriptionResponseDto;
import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.customersubscription.client.impl.TargetCustomerSubscriptionClientImpl;


/**
 * @author rmcalave
 * 
 */
public class TargetCustomerSubscriptionClientMock extends TargetCustomerSubscriptionClientImpl implements
        MockedByTarget {

    private boolean isActive;

    private Integer responseCode;
    private String responseMessage;
    private TargetCustomerSubscriptionResponseType responseType;

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#isActive()
     */
    @Override
    public boolean isActive() {
        return isActive;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active) {
        this.isActive = active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtmail.client.TargetCustomerSubscriptionClient#sendCustomerSubscription(au.com.target.tgtmail.dto.TargetCustomerSubscriptionRequestDto)
     */
    @Override
    public TargetCustomerSubscriptionResponseDto sendCustomerSubscription(
            final TargetCustomerSubscriptionRequestDto subscriptionDetails) {
        if (!isActive) {
            return super.sendCustomerSubscription(subscriptionDetails);
        }

        final TargetCustomerSubscriptionResponseDto response = new TargetCustomerSubscriptionResponseDto();
        response.setResponseCode(responseCode);
        response.setResponseMessage(responseMessage);
        response.setResponseType(responseType);
        return response;
    }

    /**
     * @return the responseCode
     */
    public Integer getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final Integer responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the responseType
     */
    public TargetCustomerSubscriptionResponseType getResponseType() {
        return responseType;
    }

    /**
     * @param responseType
     *            the responseType to set
     */
    public void setResponseType(final TargetCustomerSubscriptionResponseType responseType) {
        this.responseType = responseType;
    }
}

package au.com.target.tgttinker.mock.customer.sharewishlist;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import au.com.target.tgtcore.integration.dto.TargetIntegrationErrorDto;
import au.com.target.tgtcore.integration.dto.TargetIntegrationResponseDto;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.sharewishlist.client.impl.TargetShareWishListClientImpl;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;


/**
 * @author mjanarth
 *
 */
public class TargetShareWishlistClientMock extends TargetShareWishListClientImpl implements MockedByTarget {

    private static final Logger LOG = Logger.getLogger(TargetShareWishlistClientMock.class);

    private boolean isActive;
    private String responseCode;
    private String responseMessage;
    private String responseType;

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#isActive()
     */
    @Override
    public boolean isActive() {
        return isActive;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active) {
        this.isActive = active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwishlist.sendWishLists.SendWishListClient#sendWishLists(au.com.target.tgtwishlist.data.TargetWishListSendRequestDto)
     */
    @Override
    public TargetIntegrationResponseDto shareWishList(final TargetWishListSendRequestDto request) {
        if (!isActive) {
            return super.shareWishList(request);
        }
        final TargetIntegrationResponseDto response = new TargetIntegrationResponseDto();
        final List<TargetIntegrationErrorDto> errorList = new ArrayList<>();
        if ("success".equalsIgnoreCase(responseType)) {
            response.setSuccess(true);
            LOG.info("The Mock response for share wish list ::" + response);
            return response;
        }
        else {
            final TargetIntegrationErrorDto errorDto = new TargetIntegrationErrorDto();
            errorDto.setErrorCode(responseCode);
            errorDto.setErrorMessage(responseMessage);
            errorList.add(errorDto);
            response.setErrorList(errorList);
            response.setSuccess(false);
            LOG.info("The Mock response for share wish list ::" + response);
            return response;
        }

    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the responseType
     */
    public String getResponseType() {
        return responseType;
    }

    /**
     * @param responseType
     *            the responseType to set
     */
    public void setResponseType(final String responseType) {
        this.responseType = responseType;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

}

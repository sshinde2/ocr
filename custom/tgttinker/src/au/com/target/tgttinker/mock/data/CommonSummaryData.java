/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 *
 */
public class CommonSummaryData
{
    private boolean mockInUse;
    private boolean mockActive;

    public CommonSummaryData()
    {
        mockInUse = false;
        mockActive = false;
    }

    /**
     * @return the mockInUse
     */
    public boolean isMockInUse()
    {
        return mockInUse;
    }

    /**
     * @param mockInUse
     *            the mockInUse to set
     */
    public void setMockInUse(final boolean mockInUse)
    {
        this.mockInUse = mockInUse;
    }

    /**
     * @return the mockActive
     */
    public boolean isMockActive()
    {
        return mockActive;
    }

    /**
     * @param mockActive
     *            the mockActive to set
     */
    public void setMockActive(final boolean mockActive)
    {
        this.mockActive = mockActive;
    }
}

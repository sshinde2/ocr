/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * @author mgazal
 *
 */
public class FluentSummaryData extends CommonSummaryData {

    private boolean batchActive;

    private String createJobResponse;

    private String createBatchResponse;

    private boolean createCategoryActive;

    private boolean updateCategoryActive;

    private boolean searchCategoryActive;

    private String createCategoryResponse;

    private String updateCategoryResponse;

    private String searchCategoryResponse;

    private boolean createProductActive;

    private boolean updateProductActive;

    private boolean searchProductActive;

    private String createProductResponse;

    private String updateProductResponse;

    private String searchProductResponse;

    private boolean createSkuActive;

    private boolean updateSkuActive;

    private boolean searchSkuActive;

    private String createSkuResponse;

    private String updateSkuResponse;

    private String searchSkuResponse;

    private String batchResponse;

    private boolean createFulfilmentActive;

    private String fulfilmentOptionResponse;

    private String fulfilmentOptionMap;

    private String upsertCcAts;

    private String upsertHdAts;

    private String upsertEdAts;

    private String upsertConsolidatedStoreSoh;

    private String upsertStoreSoh;

    private boolean createOrderActive;

    private String createOrderResponse;

    private boolean eventSyncActive;

    private String eventResponse;

    private boolean retrieveFulfilmentsByOrderActive;

    private String fulfilmentsResponse;

    private boolean viewOrderActive;

    private String viewOrderResponse;

    /**
     * @return the batchActive
     */
    public boolean isBatchActive() {
        return batchActive;
    }

    /**
     * @param batchActive
     *            the batchActive to set
     */
    public void setBatchActive(final boolean batchActive) {
        this.batchActive = batchActive;
    }

    /**
     * @return the createJobResponse
     */
    public String getCreateJobResponse() {
        return createJobResponse;
    }

    /**
     * @param createJobResponse
     *            the createJobResponse to set
     */
    public void setCreateJobResponse(final String createJobResponse) {
        this.createJobResponse = createJobResponse;
    }

    /**
     * @return the createBatchResponse
     */
    public String getCreateBatchResponse() {
        return createBatchResponse;
    }

    /**
     * @param createBatchResponse
     *            the createBatchResponse to set
     */
    public void setCreateBatchResponse(final String createBatchResponse) {
        this.createBatchResponse = createBatchResponse;
    }

    /**
     * @return the createCategoryActive
     */
    public boolean isCreateCategoryActive() {
        return createCategoryActive;
    }

    /**
     * @param createCategoryActive
     *            the createCategoryActive to set
     */
    public void setCreateCategoryActive(final boolean createCategoryActive) {
        this.createCategoryActive = createCategoryActive;
    }

    /**
     * @return the updateCategoryActive
     */
    public boolean isUpdateCategoryActive() {
        return updateCategoryActive;
    }

    /**
     * @param updateCategoryActive
     *            the updateCategoryActive to set
     */
    public void setUpdateCategoryActive(final boolean updateCategoryActive) {
        this.updateCategoryActive = updateCategoryActive;
    }

    /**
     * @return the searchCategoryActive
     */
    public boolean isSearchCategoryActive() {
        return searchCategoryActive;
    }

    /**
     * @param searchCategoryActive
     *            the searchCategoryActive to set
     */
    public void setSearchCategoryActive(final boolean searchCategoryActive) {
        this.searchCategoryActive = searchCategoryActive;
    }

    /**
     * @return the createCategoryResponse
     */
    public String getCreateCategoryResponse() {
        return createCategoryResponse;
    }

    /**
     * @param createCategoryResponse
     *            the createCategoryResponse to set
     */
    public void setCreateCategoryResponse(final String createCategoryResponse) {
        this.createCategoryResponse = createCategoryResponse;
    }

    /**
     * @return the updateCategoryResponse
     */
    public String getUpdateCategoryResponse() {
        return updateCategoryResponse;
    }

    /**
     * @param updateCategoryResponse
     *            the updateCategoryResponse to set
     */
    public void setUpdateCategoryResponse(final String updateCategoryResponse) {
        this.updateCategoryResponse = updateCategoryResponse;
    }

    /**
     * @return the searchCategoryResponse
     */
    public String getSearchCategoryResponse() {
        return searchCategoryResponse;
    }

    /**
     * @param searchCategoryResponse
     *            the searchCategoryResponse to set
     */
    public void setSearchCategoryResponse(final String searchCategoryResponse) {
        this.searchCategoryResponse = searchCategoryResponse;
    }

    /**
     * @return the createProductActive
     */
    public boolean isCreateProductActive() {
        return createProductActive;
    }

    /**
     * @param createProductActive
     *            the createProductActive to set
     */
    public void setCreateProductActive(final boolean createProductActive) {
        this.createProductActive = createProductActive;
    }

    /**
     * @return the updateProductActive
     */
    public boolean isUpdateProductActive() {
        return updateProductActive;
    }

    /**
     * @param updateProductActive
     *            the updateProductActive to set
     */
    public void setUpdateProductActive(final boolean updateProductActive) {
        this.updateProductActive = updateProductActive;
    }

    /**
     * @return the searchProductActive
     */
    public boolean isSearchProductActive() {
        return searchProductActive;
    }

    /**
     * @param searchProductActive
     *            the searchProductActive to set
     */
    public void setSearchProductActive(final boolean searchProductActive) {
        this.searchProductActive = searchProductActive;
    }

    /**
     * @return the createProductResponse
     */
    public String getCreateProductResponse() {
        return createProductResponse;
    }

    /**
     * @param createProductResponse
     *            the createProductResponse to set
     */
    public void setCreateProductResponse(final String createProductResponse) {
        this.createProductResponse = createProductResponse;
    }

    /**
     * @return the updateProductResponse
     */
    public String getUpdateProductResponse() {
        return updateProductResponse;
    }

    /**
     * @param updateProductResponse
     *            the updateProductResponse to set
     */
    public void setUpdateProductResponse(final String updateProductResponse) {
        this.updateProductResponse = updateProductResponse;
    }

    /**
     * @return the searchProductResponse
     */
    public String getSearchProductResponse() {
        return searchProductResponse;
    }

    /**
     * @param searchProductResponse
     *            the searchProductResponse to set
     */
    public void setSearchProductResponse(final String searchProductResponse) {
        this.searchProductResponse = searchProductResponse;
    }

    /**
     * @return the createSkuActive
     */
    public boolean isCreateSkuActive() {
        return createSkuActive;
    }

    /**
     * @param createSkuActive
     *            the createSkuActive to set
     */
    public void setCreateSkuActive(final boolean createSkuActive) {
        this.createSkuActive = createSkuActive;
    }

    /**
     * @return the updateSkuActive
     */
    public boolean isUpdateSkuActive() {
        return updateSkuActive;
    }

    /**
     * @param updateSkuActive
     *            the updateSkuActive to set
     */
    public void setUpdateSkuActive(final boolean updateSkuActive) {
        this.updateSkuActive = updateSkuActive;
    }

    /**
     * @return the searchSkuActive
     */
    public boolean isSearchSkuActive() {
        return searchSkuActive;
    }

    /**
     * @param searchSkuActive
     *            the searchSkuActive to set
     */
    public void setSearchSkuActive(final boolean searchSkuActive) {
        this.searchSkuActive = searchSkuActive;
    }

    /**
     * @return the createSkuResponse
     */
    public String getCreateSkuResponse() {
        return createSkuResponse;
    }

    /**
     * @param createSkuResponse
     *            the createSkuResponse to set
     */
    public void setCreateSkuResponse(final String createSkuResponse) {
        this.createSkuResponse = createSkuResponse;
    }

    /**
     * @return the updateSkuResponse
     */
    public String getUpdateSkuResponse() {
        return updateSkuResponse;
    }

    /**
     * @param updateSkuResponse
     *            the updateSkuResponse to set
     */
    public void setUpdateSkuResponse(final String updateSkuResponse) {
        this.updateSkuResponse = updateSkuResponse;
    }

    /**
     * @return the searchSkuResponse
     */
    public String getSearchSkuResponse() {
        return searchSkuResponse;
    }

    /**
     * @param searchSkuResponse
     *            the searchSkuResponse to set
     */
    public void setSearchSkuResponse(final String searchSkuResponse) {
        this.searchSkuResponse = searchSkuResponse;
    }

    /**
     * @return the batchResponse
     */
    public String getBatchResponse() {
        return batchResponse;
    }

    /**
     * @param batchResponse
     *            the batchResponse to set
     */
    public void setBatchResponse(final String batchResponse) {
        this.batchResponse = batchResponse;
    }

    /**
     * @return the createFulfilmentActive
     */
    public boolean isCreateFulfilmentActive() {
        return createFulfilmentActive;
    }

    /**
     * @param createFulfilmentActive
     *            the createFulfilmentActive to set
     */
    public void setCreateFulfilmentActive(final boolean createFulfilmentActive) {
        this.createFulfilmentActive = createFulfilmentActive;
    }

    /**
     * @return the fulfilmentOptionResponse
     */
    public String getFulfilmentOptionResponse() {
        return fulfilmentOptionResponse;
    }

    /**
     * @param fulfilmentOptionResponse
     */
    public void setFulfilmentOptionResponse(final String fulfilmentOptionResponse) {
        this.fulfilmentOptionResponse = fulfilmentOptionResponse;
    }

    /**
     * @return the fulfilmentOptionMap
     */
    public String getFulfilmentOptionMap() {
        return fulfilmentOptionMap;
    }

    /**
     * @param fulfilmentOptionMap
     *            the fulfilmentOptionMap to set
     */
    public void setFulfilmentOptionMap(final String fulfilmentOptionMap) {
        this.fulfilmentOptionMap = fulfilmentOptionMap;
    }

    /**
     * @return the upsertCcAts
     */
    public String getUpsertCcAts() {
        return upsertCcAts;
    }

    /**
     * @param upsertCcAts
     *            the upsertCcAts to set
     */
    public void setUpsertCcAts(final String upsertCcAts) {
        this.upsertCcAts = upsertCcAts;
    }

    /**
     * @return the upsertHdAts
     */
    public String getUpsertHdAts() {
        return upsertHdAts;
    }

    /**
     * @param upsertHdAts
     *            the upsertHdAts to set
     */
    public void setUpsertHdAts(final String upsertHdAts) {
        this.upsertHdAts = upsertHdAts;
    }

    /**
     * @return the upsertEdAts
     */
    public String getUpsertEdAts() {
        return upsertEdAts;
    }

    /**
     * @param upsertEdAts
     *            the upsertEdAts to set
     */
    public void setUpsertEdAts(final String upsertEdAts) {
        this.upsertEdAts = upsertEdAts;
    }

    /**
     * @return the upsertConsolidatedStoreSoh
     */
    public String getUpsertConsolidatedStoreSoh() {
        return upsertConsolidatedStoreSoh;
    }

    /**
     * @param upsertConsolidatedStoreSoh
     *            the upsertConsolidatedStoreSoh to set
     */
    public void setUpsertConsolidatedStoreSoh(final String upsertConsolidatedStoreSoh) {
        this.upsertConsolidatedStoreSoh = upsertConsolidatedStoreSoh;
    }

    /**
     * @return the upsertStoreSoh
     */
    public String getUpsertStoreSoh() {
        return upsertStoreSoh;
    }

    /**
     * @param upsertStoreSoh
     *            the upsertStoreSoh to set
     */
    public void setUpsertStoreSoh(final String upsertStoreSoh) {
        this.upsertStoreSoh = upsertStoreSoh;
    }

    /**
     * @return the createOrderActive
     */
    public boolean isCreateOrderActive() {
        return createOrderActive;
    }

    /**
     * @param createOrderActive
     *            the createOrderActive to set
     */
    public void setCreateOrderActive(final boolean createOrderActive) {
        this.createOrderActive = createOrderActive;
    }

    /**
     * @return the createOrderResponse
     */
    public String getCreateOrderResponse() {
        return createOrderResponse;
    }

    /**
     * @param createOrderResponse
     *            the createOrderResponse to set
     */
    public void setCreateOrderResponse(final String createOrderResponse) {
        this.createOrderResponse = createOrderResponse;
    }



    /**
     * @return the eventSyncActive
     */
    public boolean isEventSyncActive() {
        return eventSyncActive;
    }

    /**
     * @param eventSyncActive
     *            the eventSyncActive to set
     */
    public void setEventSyncActive(final boolean eventSyncActive) {
        this.eventSyncActive = eventSyncActive;
    }

    /**
     * @return the eventResponse
     */
    public String getEventResponse() {
        return eventResponse;
    }

    /**
     * @param eventResponse
     *            the eventResponse to set
     */
    public void setEventResponse(final String eventResponse) {
        this.eventResponse = eventResponse;
    }

    /**
     * @return the retrieveFulfilmentsByOrderActive
     */
    public boolean isRetrieveFulfilmentsByOrderActive() {
        return retrieveFulfilmentsByOrderActive;
    }

    /**
     * @param retrieveFulfilmentsByOrderActive
     *            the retrieveFulfilmentsByOrderActive to set
     */
    public void setRetrieveFulfilmentsByOrderActive(final boolean retrieveFulfilmentsByOrderActive) {
        this.retrieveFulfilmentsByOrderActive = retrieveFulfilmentsByOrderActive;
    }

    /**
     * @return the fulfilmentsResponse
     */
    public String getFulfilmentsResponse() {
        return fulfilmentsResponse;
    }

    /**
     * @param fulfilmentsResponse
     *            the fulfilmentsResponse to set
     */
    public void setFulfilmentsResponse(final String fulfilmentsResponse) {
        this.fulfilmentsResponse = fulfilmentsResponse;
    }

    /**
     * @return the getOrderActive
     */
    public boolean isViewOrderActive() {
        return viewOrderActive;
    }

    /**
     * @param viewOrderActive
     *            the getOrderActive to set
     */
    public void setViewOrderActive(final boolean viewOrderActive) {
        this.viewOrderActive = viewOrderActive;
    }

    /**
     * @return the getOrderResponse
     */
    public String getViewOrderResponse() {
        return viewOrderResponse;
    }

    /**
     * @param getOrderResponse
     *            the getOrderResponse to set
     */
    public void setViewOrderResponse(final String getOrderResponse) {
        this.viewOrderResponse = getOrderResponse;
    }

}

/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * @author dcwillia
 * 
 */
public class NavData {
    private String serviceName;
    private String mockCntrlUrl;

    public NavData(final String serviceName, final String mockCntrlUrl) {
        setServiceName(serviceName);
        setMockCntrlUrl(mockCntrlUrl);
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName
     *            the serviceName to set
     */
    public final void setServiceName(final String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return the mockCntrlUrl
     */
    public String getMockCntrlUrl() {
        return mockCntrlUrl;
    }

    /**
     * @param mockCntrlUrl
     *            the mockCntrlUrl to set
     */
    public final void setMockCntrlUrl(final String mockCntrlUrl) {
        this.mockCntrlUrl = mockCntrlUrl;
    }
}

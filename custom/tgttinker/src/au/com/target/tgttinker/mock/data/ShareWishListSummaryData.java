/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * @author mjanarth
 *
 */
public class ShareWishListSummaryData extends CommonSummaryData {

    private final String mockShareWishListDefault = "Success";
    private String responseCode;
    private String responseMessage;
    private final String[] mockShareWishListResponses = { "Success", "Error" };
    private boolean success;
    private String mockShareWishListResponse;


    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the mockShareWishListResponses
     */
    public String[] getMockShareWishListResponses() {
        return mockShareWishListResponses;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

    /**
     * @return the mockShareWishListDefault
     */
    public String getMockShareWishListDefault() {
        return mockShareWishListDefault;
    }

    /**
     * @return the mockShareWishListResponse
     */
    public String getMockShareWishListResponse() {
        return mockShareWishListResponse;
    }

    /**
     * @param mockShareWishListResponse
     *            the mockShareWishListResponse to set
     */
    public void setMockShareWishListResponse(final String mockShareWishListResponse) {
        this.mockShareWishListResponse = mockShareWishListResponse;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

}

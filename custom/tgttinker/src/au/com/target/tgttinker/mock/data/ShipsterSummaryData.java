/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * @author pvarghe2
 *
 */
public class ShipsterSummaryData extends CommonSummaryData {

    private boolean verifyEmailMock;

    private boolean confirmOrderMock;

    private boolean verifyEmailServiceAvailable;

    private boolean confirmOrderServiceAvailable;

    private boolean confirmOrderTimeOut;

    private String verifyEmailResponse;

    private String confirmOrderResponse;


    /**
     * @return the serviceAvailable
     */
    public boolean isVerifyEmailServiceAvailable() {
        return verifyEmailServiceAvailable;
    }

    /**
     * @param verifyEmailServiceAvailable
     *            the serviceAvailable to set
     */
    public void setVerifyEmailServiceAvailable(final boolean verifyEmailServiceAvailable) {
        this.verifyEmailServiceAvailable = verifyEmailServiceAvailable;
    }

    /**
     * @param verifyEmailMock
     *            the verifyEmailMock to set
     */
    public void setVerifyEmailMock(final boolean verifyEmailMock) {
        this.verifyEmailMock = verifyEmailMock;
    }

    /**
     * @return the verifyEmailMock
     */
    public boolean isVerifyEmailMock() {
        return verifyEmailMock;
    }

    /**
     * @return the confirmOrderServiceAvailable
     */
    public boolean isConfirmOrderServiceAvailable() {
        return confirmOrderServiceAvailable;
    }

    /**
     * @param confirmOrderServiceAvailable
     *            the confirmOrderServiceAvailable to set
     */
    public void setConfirmOrderServiceAvailable(final boolean confirmOrderServiceAvailable) {
        this.confirmOrderServiceAvailable = confirmOrderServiceAvailable;
    }

    /**
     * @return the confirmOrderMock
     */
    public boolean isConfirmOrderMock() {
        return confirmOrderMock;
    }

    /**
     * @param confirmOrderMock
     *            the confirmOrderMock to set
     */
    public void setConfirmOrderMock(final boolean confirmOrderMock) {
        this.confirmOrderMock = confirmOrderMock;
    }

    /**
     * @return the verifyEmailResponse
     */
    public String getVerifyEmailResponse() {
        return verifyEmailResponse;
    }

    /**
     * @param verifyEmailResponse
     *            the verifyEmailResponse to set
     */
    public void setVerifyEmailResponse(final String verifyEmailResponse) {
        this.verifyEmailResponse = verifyEmailResponse;
    }

    /**
     * @return the confirmOrderResponse
     */
    public String getConfirmOrderResponse() {
        return confirmOrderResponse;
    }

    /**
     * @param confirmOrderResponse
     *            the confirmOrderResponse to set
     */
    public void setConfirmOrderResponse(final String confirmOrderResponse) {
        this.confirmOrderResponse = confirmOrderResponse;
    }

    /**
     * @return the confirmOrderTimeOut
     */
    public boolean isConfirmOrderTimeOut() {
        return confirmOrderTimeOut;
    }

    /**
     * @param confirmOrderTimeOut
     *            the confirmOrderTimeOut to set
     */
    public void setConfirmOrderTimeOut(final boolean confirmOrderTimeOut) {
        this.confirmOrderTimeOut = confirmOrderTimeOut;
    }

    public void resetDefault() {
        this.confirmOrderMock = false;
        this.confirmOrderResponse = null;
        this.confirmOrderServiceAvailable = false;
        this.confirmOrderTimeOut = false;
        this.verifyEmailMock = false;
        this.verifyEmailResponse = null;
        this.verifyEmailServiceAvailable = false;
    }
}

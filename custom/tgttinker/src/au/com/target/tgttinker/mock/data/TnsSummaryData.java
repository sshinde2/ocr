/**
 * 
 */
package au.com.target.tgttinker.mock.data;

import au.com.target.tgttinker.mock.payment.TargetCreditCardPaymentMethodMock;



/**
 *
 */
public class TnsSummaryData extends CommonSummaryData {
    private String mockResponse = "";
    private String mockStatus = "";
    private final String[] validMockResponses = TargetCreditCardPaymentMethodMock.VALID_MOCK_RESPONSE
            .toArray(new String[] {});
    private final String mockDefault = TargetCreditCardPaymentMethodMock.MOCK_DEFAULT;

    /**
     * @return the mockResponse
     */
    public String getMockResponse() {
        return mockResponse;
    }

    /**
     * @param mockResponse
     *            the mockResponse to set
     */
    public void setMockResponse(final String mockResponse) {
        this.mockResponse = mockResponse;
    }

    /**
     * @return the mockStatus
     */
    public String getMockStatus() {
        return mockStatus;
    }

    /**
     * @param mockStatus
     *            the mockStatus to set
     */
    public void setMockStatus(final String mockStatus) {
        this.mockStatus = mockStatus;
    }

    /**
     * @return the validMockResponses
     */
    public String[] getValidMockResponses() {
        return validMockResponses;
    }

    /**
     * @return the mockDefault
     */
    public String getMockDefault() {
        return mockDefault;
    }
}

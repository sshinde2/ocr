/**
 * 
 */
package au.com.target.tgttinker.mock.data;

/**
 * POJO for Zip Payment data
 */
public class ZipPaymentSummaryData extends CommonSummaryData {

    private boolean throwException;
    private boolean createCheckoutException;
    private boolean pingActive;
    private boolean createCheckoutActive;
    private String createCheckoutResponse;
    private boolean retrieveCheckoutActive;
    private boolean retrieveCheckoutException;
    private boolean captureRequestActive;
    private boolean captureRequestException;
    private boolean captureTransactionError;
    private String retrieveCheckoutResponse;
    private String captureRequestResponse;
    private String mockRefundResponse;
    private boolean refundTransErrorException;
    private boolean refundRejectException;
    private boolean refundRequestException;
    private boolean refundRequestActive;

    public boolean isThrowException() {
        return throwException;
    }

    public void setThrowException(final boolean throwException) {
        this.throwException = throwException;
    }

    public boolean isCreateCheckoutException() {
        return createCheckoutException;
    }

    public void setCreateCheckoutException(final boolean createCheckoutException) {
        this.createCheckoutException = createCheckoutException;
    }

    public boolean isPingActive() {
        return pingActive;
    }

    public void setPingActive(final boolean pingActive) {
        this.pingActive = pingActive;
    }

    public boolean isCreateCheckoutActive() {
        return createCheckoutActive;
    }

    public void setCreateCheckoutActive(final boolean createCheckoutActive) {
        this.createCheckoutActive = createCheckoutActive;
    }

    public String getCreateCheckoutResponse() {
        return createCheckoutResponse;
    }

    public void setCreateCheckoutResponse(final String createCheckoutResponse) {
        this.createCheckoutResponse = createCheckoutResponse;
    }

    public void setRetrieveCheckoutActive(final boolean retrieveCheckoutActive) {
        this.retrieveCheckoutActive = retrieveCheckoutActive;
    }

    public void setRetrieveCheckoutException(final boolean retrieveCheckoutException) {
        this.retrieveCheckoutException = retrieveCheckoutException;
    }

    public boolean getRetrieveCheckoutException() {
        return retrieveCheckoutException;
    }

    public void setCaptureRequestActive(final boolean captureRequestActive) {
        this.captureRequestActive = captureRequestActive;
    }

    public boolean getCaptureRequestActive() {
        return captureRequestActive;
    }

    public void setCaptureRequestException(final boolean captureRequestException) {
        this.captureRequestException = captureRequestException;
    }

    public boolean getCaptureRequestException() {
        return captureRequestException;
    }

    public void setRetrieveCheckoutResponse(final String retrieveCheckoutResponse) {
        this.retrieveCheckoutResponse = retrieveCheckoutResponse;
    }

    public String getRetrieveCheckoutResponse() {
        return retrieveCheckoutResponse;
    }

    public void setCaptureRequestResponse(final String captureRequestResponse) {
        this.captureRequestResponse = captureRequestResponse;
    }

    public String getCaptureRequestResponse() {
        return captureRequestResponse;
    }

    public boolean isRetrieveCheckoutActive() {
        return retrieveCheckoutActive;
    }

    public void setMockRefundResponse(final String mockRefundResponse) {
        this.mockRefundResponse = mockRefundResponse;
    }

    public String getMockRefundResponse() {
        return mockRefundResponse;
    }

    public void setRefundTransErrorException(final boolean refundTransErrorException) {
        this.refundTransErrorException = refundTransErrorException;
    }

    public boolean getRefundTransErrorException() {
        return refundTransErrorException;
    }

    public void setRefundRejectException(final boolean refundRejectException) {
        this.refundRejectException = refundRejectException;
    }

    public boolean getRefundRejectException() {
        return refundRejectException;
    }

    public void setRefundRequestException(final boolean refundRequestException) {
        this.refundRequestException = refundRequestException;
    }

    public boolean getRefundRequestException() {
        return refundRequestException;
    }

    public void setRefundRequestActive(final boolean refundRequestActive) {
        this.refundRequestActive = refundRequestActive;
    }

    public boolean getRefundRequestActive() {
        return refundRequestActive;
    }

    public boolean isCaptureTransactionError() {
        return captureTransactionError;
    }

    public void setCaptureTransactionError(final boolean captureTransactionError) {
        this.captureTransactionError = captureTransactionError;
    }

}

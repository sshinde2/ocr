/**
 * 
 */
package au.com.target.tgttinker.mock.event;

import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgttinker.mock.facades.FluentMockFacade;


/**
 * @author mgazal
 *
 */
public class MockParametersEventListener extends AbstractEventListener<MockParametersEvent> {

    private static final Logger LOG = Logger.getLogger(MockParametersEventListener.class);

    @Autowired
    private FluentMockFacade fluentMockFacade;

    @Override
    protected void onEvent(final MockParametersEvent event) {
        if (event.getEventType().equals(fluentMockFacade.getEventType())) {
            fluentMockFacade.updateMock(event.getParameterMap());
        }
        else {
            LOG.warn("Unhandled event of type=" + event.getEventType());
        }
    }
}

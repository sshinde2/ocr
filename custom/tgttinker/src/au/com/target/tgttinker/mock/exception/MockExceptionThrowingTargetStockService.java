/**
 * 
 */
package au.com.target.tgttinker.mock.exception;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import au.com.target.tgtcore.stock.impl.TargetStockServiceImpl;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 * @author Nandini
 *
 */
public class MockExceptionThrowingTargetStockService extends TargetStockServiceImpl implements MockedByTarget {

    private boolean active;

    @Override
    public StockLevelModel checkAndGetStockLevel(final ProductModel product, final WarehouseModel warehouse) {
        if (!active) {
            return super.checkAndGetStockLevel(product, warehouse);
        }
        throw new StockLevelNotFoundException("no stock level for product [" + product + "] in warehouse ["
                + warehouse.getName() + "] found.");
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

}

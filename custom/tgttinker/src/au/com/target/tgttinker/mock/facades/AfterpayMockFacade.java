/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.AfterpaySummaryData;


/**
 * @author gsing236
 *
 */
public interface AfterpayMockFacade extends CommonMockFacade {

    @Override
    AfterpaySummaryData getSummary();

    void setThrowException(String parameter);

    void setCapturePaymentException(String parameter);

    void setGetOrderExpection(String parameter);

    void setGetOrderActive(String parameter);

    void setCapturePaymentActive(String parameter);

    void setCreateOrderActive(String parameter);

    void setCreateOrderException(String parameter);

    void mockGetConfigurationResponse(String mock);

    void mockGetOrderResponse(String mock);

    void mockCreateOrderResponse(String mock);

    void mockCapturePaymentResponse(String mock);

    void mockCreateRefundResponse(String mock);

    void resetDefault();

    void setPingActive(String mock);

    void setCreateRefundActive(String mock);

    void setGetPaymentActive(String mock);

    void setGetPaymentException(String mock);

    void setCreateRefundException(String mock);

    void mockGetPaymentResponse(String mock);

    void setGetConfigurationActive(String mock);

    void setGetConfigurationException(String mock);

}

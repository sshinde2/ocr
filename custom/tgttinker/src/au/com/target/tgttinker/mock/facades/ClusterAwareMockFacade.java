/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import javax.servlet.http.HttpServletRequest;

import au.com.target.tgttinker.mock.event.MockParameterEventType;


/**
 * @author mgazal
 *
 */
public interface ClusterAwareMockFacade extends CommonMockFacade {

    /**
     * @param request
     */
    void updateMock(final HttpServletRequest request);

    /**
     * @return {@link MockParameterEventType}
     */
    MockParameterEventType getEventType();
}

/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.CommonSummaryData;




/**
 *
 */
public interface CommonMockFacade {
    CommonSummaryData getSummary();

    void setActive(final boolean enable);
}

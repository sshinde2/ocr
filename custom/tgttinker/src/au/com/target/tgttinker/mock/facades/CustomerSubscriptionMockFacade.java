/**
 * 
 */
package au.com.target.tgttinker.mock.facades;



/**
 * @author rmcalave
 *
 */
public interface CustomerSubscriptionMockFacade extends CommonMockFacade {

    void setMockValues(final Integer responseCode, final String responseMessage, String responseType);
}
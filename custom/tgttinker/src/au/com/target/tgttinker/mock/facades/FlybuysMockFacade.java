/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.FlybuysSummaryData;


/**
 * @author cwijesu1
 * 
 */
public interface FlybuysMockFacade extends CommonMockFacade {

    @Override
    FlybuysSummaryData getSummary();

    void setMockResponse(final String mockResponse, final String AvlbflyBuyPoints, final String consumeResponseCode,
            String refundResponseCode);

}

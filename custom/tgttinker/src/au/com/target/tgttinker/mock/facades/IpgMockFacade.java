/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.IpgSummaryData;



/**
 *
 */
public interface IpgMockFacade extends CommonMockFacade {
    @Override
    IpgSummaryData getSummary();

    void setTokenResponse(String selectedToken);

    void setRefundResponse(String refundResponse, String declineCode, String declineMessage);

    void setSubmitSinglePaymentResponse(String declineCode, String declineMessage);

    /**
     * @param parameter
     */
    void setServiceUnavailable(String parameter);
}

/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.PollConsignmentSummaryData;


/**
 * @author gsing236
 *
 */
public interface PollConsignmentMockFacade extends CommonMockFacade {

    @Override
    PollConsignmentSummaryData getSummary();

    void setMockSuccess(String parameter);

    void setMockServiceUnavailable(String parameter);

    void resetDefault();

}

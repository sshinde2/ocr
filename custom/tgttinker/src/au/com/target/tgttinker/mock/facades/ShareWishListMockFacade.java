/**
 * 
 */
package au.com.target.tgttinker.mock.facades;



/**
 * @author rmcalave
 *
 */
public interface ShareWishListMockFacade extends CommonMockFacade {

    void setMockValues(final String responseCode, final String responseMessage, final String responseType);
}
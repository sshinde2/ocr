/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.StockVisibilitySummaryData;


/**
 * @author rmcalave
 *
 */
public interface StockVisibilityMockFacade extends CommonMockFacade {

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#getSummary()
     */
    @Override
    StockVisibilitySummaryData getSummary();

    void addStockVisibilityItemResponseDto(final String storeNumber, final String itemcode, final String soh);

    void removeStockVisibilityItemResponseDto(final String storeNumber, final String itemcode);

    void resetStockVisibilityItemResponseDto();

    void updateStockVisibilityItemResponseDto(final String storeNumber, final String itemcode, final String soh);

    void setResponseCode(String code);

    void setException(String exception);
}

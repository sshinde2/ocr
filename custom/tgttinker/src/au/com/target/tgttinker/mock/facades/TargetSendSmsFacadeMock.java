/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.SendSmsSummaryData;


/**
 * @author mjanarth
 * 
 */
public interface TargetSendSmsFacadeMock extends CommonMockFacade {


    @Override
    SendSmsSummaryData getSummary();

    /**
     * 
     * @param mockResponseCode
     */
    void setMockResponse(final String mockResponseCode);

}

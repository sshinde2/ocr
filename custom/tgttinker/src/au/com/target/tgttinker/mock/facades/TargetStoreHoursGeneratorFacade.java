/**
 * 
 */
package au.com.target.tgttinker.mock.facades;



/**
 * @author rmcalave
 *
 */
public interface TargetStoreHoursGeneratorFacade {

    void regenerateStoreHours();

}
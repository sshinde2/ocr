/**
 * 
 */
package au.com.target.tgttinker.mock.facades;

import au.com.target.tgttinker.mock.data.TnsSummaryData;


/**
 *
 */
public interface TnsMockFacade extends CommonMockFacade {
    @Override
    TnsSummaryData getSummary();

    void setMockResponse(final String mockResponse, final int times, final String nextMockResponse);
}

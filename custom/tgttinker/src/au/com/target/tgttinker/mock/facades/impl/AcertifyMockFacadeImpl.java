/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgttinker.mock.data.AcertifySummaryData;
import au.com.target.tgttinker.mock.facades.AcertifyMockFacade;
import au.com.target.tgttinker.mock.fraud.provider.AccertifyFraudServiceProviderMock;


/**
 *
 */
public class AcertifyMockFacadeImpl implements AcertifyMockFacade {

    /* (non-Javadoc)
     * @see au.com.target.web.facades.TnsMockFacade#getSummary()
     */
    @Override
    public AcertifySummaryData getSummary() {
        final Object fraudCheckAction = Registry.getApplicationContext().getBean("accertifyFraudServiceProvider");
        final AcertifySummaryData data = new AcertifySummaryData();
        if (fraudCheckAction instanceof AccertifyFraudServiceProviderMock) {
            final AccertifyFraudServiceProviderMock mockedBean = (AccertifyFraudServiceProviderMock)fraudCheckAction;
            data.setMockInUse(true);
            data.setMockActive(mockedBean.isActive());
            data.setMockResponse(mockedBean.getMockResponse());
        }
        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.web.facades.TnsMockFacade#setActive()
     */
    @Override
    public void setActive(final boolean enable) {
        final Object fraudCheckAction = Registry.getApplicationContext().getBean("accertifyFraudServiceProvider");
        if (fraudCheckAction instanceof AccertifyFraudServiceProviderMock) {
            final AccertifyFraudServiceProviderMock mockedBean = (AccertifyFraudServiceProviderMock)fraudCheckAction;
            mockedBean.setActive(enable);
        }
    }

    @Override
    public void setMockResponse(final String mockResponse) {
        final Object fraudCheckAction = Registry.getApplicationContext().getBean("accertifyFraudServiceProvider");
        if (fraudCheckAction instanceof AccertifyFraudServiceProviderMock) {
            final AccertifyFraudServiceProviderMock mockedBean = (AccertifyFraudServiceProviderMock)fraudCheckAction;
            mockedBean.setMockResponse(mockResponse);
        }
    }
}

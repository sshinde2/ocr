/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.servicelayer.event.EventService;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgttinker.mock.event.MockParametersEvent;
import au.com.target.tgttinker.mock.facades.ClusterAwareMockFacade;


/**
 * @author mgazal
 *
 */
public abstract class BaseClusterAwareMockFacade implements ClusterAwareMockFacade {

    @Autowired
    private EventService eventService;

    /**
     * @param parameterMap
     */
    public abstract void updateMock(final Map<String, String> parameterMap);

    @Override
    public void updateMock(final HttpServletRequest request) {
        final Map<String, String> parameterMap = new HashMap<>();
        for (final Entry<String, String[]> parameter : request.getParameterMap().entrySet()) {
            final String value = ArrayUtils.isNotEmpty(parameter.getValue()) ? parameter.getValue()[0] : null;
            parameterMap.put(parameter.getKey(), value);
        }
        updateMock(parameterMap);
        sendEvent(parameterMap);
    }

    public void sendEvent(final Map<String, String> parameterMap) {
        final MockParametersEvent event = new MockParametersEvent(this.getEventType(), parameterMap);
        eventService.publishEvent(event);
    }
}

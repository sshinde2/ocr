/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.customer.TargetCustomerSubscriptionClientMock;
import au.com.target.tgttinker.mock.data.CustomerSubscriptionSummaryData;
import au.com.target.tgttinker.mock.facades.CustomerSubscriptionMockFacade;


/**
 * @author rmcalave
 *
 */
public class CustomerSubscriptionMockFacadeImpl implements CustomerSubscriptionMockFacade {

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#getSummary()
     */
    @Override
    public CustomerSubscriptionSummaryData getSummary() {
        final Object bean = Registry.getApplicationContext().getBean(
                "targetCustomerSubscriptionClient");
        final CustomerSubscriptionSummaryData data = new CustomerSubscriptionSummaryData();

        if (bean instanceof TargetCustomerSubscriptionClientMock) {
            final TargetCustomerSubscriptionClientMock targetCustomerSubscriptionClient = (TargetCustomerSubscriptionClientMock)bean;
            data.setMockInUse(true);
            data.setMockActive(targetCustomerSubscriptionClient.isActive());

            data.setResponseCode(targetCustomerSubscriptionClient.getResponseCode());
            data.setResponseMessage(targetCustomerSubscriptionClient.getResponseMessage());
            data.setResponseType(targetCustomerSubscriptionClient.getResponseType() == null ? null
                    : targetCustomerSubscriptionClient.getResponseType().getResponse());
        }

        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#setActive(boolean)
     */
    @Override
    public void setActive(final boolean enable) {
        final Object targetCustomerSubscriptionClient = Registry.getApplicationContext().getBean(
                "targetCustomerSubscriptionClient");

        if (targetCustomerSubscriptionClient instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)targetCustomerSubscriptionClient;
            mockedBean.setActive(enable);
        }
    }

    @Override
    public void setMockValues(final Integer responseCode, final String responseMessage, final String responseType) {
        final Object bean = Registry.getApplicationContext().getBean(
                "targetCustomerSubscriptionClient");
        if (bean instanceof TargetCustomerSubscriptionClientMock) {
            final TargetCustomerSubscriptionClientMock targetCustomerSubscriptionClient = (TargetCustomerSubscriptionClientMock)bean;
            targetCustomerSubscriptionClient.setResponseCode(responseCode);
            targetCustomerSubscriptionClient.setResponseMessage(responseMessage);
            targetCustomerSubscriptionClient.setResponseType(getResponseType(responseType));
        }
    }

    private TargetCustomerSubscriptionResponseType getResponseType(final String responseTypeString) {
        for (final TargetCustomerSubscriptionResponseType responseType : TargetCustomerSubscriptionResponseType
                .values()) {
            if (responseTypeString.equalsIgnoreCase(responseType.getResponse())) {
                return responseType;
            }
        }

        return null;
    }
}

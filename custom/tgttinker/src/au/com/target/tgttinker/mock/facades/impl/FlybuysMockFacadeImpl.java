/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgttinker.mock.data.FlybuysSummaryData;
import au.com.target.tgttinker.mock.facades.FlybuysMockFacade;
import au.com.target.tgttinker.mock.flybuys.FlybuysClientMock;


/**
 * @author cwijesu1
 * 
 */
public class FlybuysMockFacadeImpl implements FlybuysMockFacade {

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#setActive(boolean)
     */
    @Override
    public void setActive(final boolean enable) {
        final Object mockflybuysClient =
                Registry.getApplicationContext().getBean("flybuysClient");
        if (mockflybuysClient instanceof FlybuysClientMock) {
            final FlybuysClientMock mockedBean = (FlybuysClientMock)mockflybuysClient;
            mockedBean.setActive(enable);

        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.FlybuysMockFacade#getSummary()
     */
    @Override
    public FlybuysSummaryData getSummary() {
        final Object mockflybuysClient =
                Registry.getApplicationContext().getBean("flybuysClient");
        final FlybuysSummaryData data = new FlybuysSummaryData();
        if (mockflybuysClient instanceof FlybuysClientMock) {
            final FlybuysClientMock mockedBean = (FlybuysClientMock)mockflybuysClient;
            data.setMockInUse(true);
            data.setMockActive(mockedBean.isActive());
            data.setMockResponse(mockedBean.getMockResponse());
            data.setMockPoints(mockedBean.getMockAvailablePoints());
            data.setMockConsumeResponse(mockedBean.getMockRedeemRespondeCode());
            data.setMockRefundResponse(mockedBean.getMockRefundRespondeCode());
        }
        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.FlybuysMockFacade#setMockResponse(java.lang.String)
     */
    @Override
    public void setMockResponse(final String mockResponse, final String avlbflyBuyPoints,
            final String consumeResponseCode, final String refundResponseCode) {
        final Object mockflybuysClient =
                Registry.getApplicationContext().getBean("flybuysClient");
        if (mockflybuysClient instanceof FlybuysClientMock) {
            final FlybuysClientMock mockedBean = (FlybuysClientMock)mockflybuysClient;
            mockedBean.setMockResponse(mockResponse, avlbflyBuyPoints, consumeResponseCode, refundResponseCode);

        }

    }



}

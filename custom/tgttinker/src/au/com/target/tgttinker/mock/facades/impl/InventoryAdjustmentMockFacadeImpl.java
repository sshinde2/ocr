/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.data.InventoryAdjustmentSummaryData;
import au.com.target.tgttinker.mock.facades.InventoryAdjustmentMockFacade;
import au.com.target.tgttinker.mock.inventory.TargetInventoryAdjustmentClientMock;


/**
 * @author pthoma20
 *
 */
public class InventoryAdjustmentMockFacadeImpl implements InventoryAdjustmentMockFacade {


    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#getSummary()
     */
    @Override
    public InventoryAdjustmentSummaryData getSummary() {
        final Object bean = Registry.getApplicationContext().getBean(
                "inventoryAdjustmentClient");
        final InventoryAdjustmentSummaryData data = new InventoryAdjustmentSummaryData();

        if (bean instanceof TargetInventoryAdjustmentClientMock) {
            final TargetInventoryAdjustmentClientMock targetInventoryAdjustmentClientMock = (TargetInventoryAdjustmentClientMock)bean;
            data.setMockInUse(true);
            data.setMockActive(targetInventoryAdjustmentClientMock.isActive());
            data.setSuccess(targetInventoryAdjustmentClientMock.isSuccess());
        }

        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#setActive(boolean)
     */
    @Override
    public void setActive(final boolean enable) {
        final Object targetInventoryAdjustmentClientMock = Registry.getApplicationContext().getBean(
                "inventoryAdjustmentClient");

        if (targetInventoryAdjustmentClientMock instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)targetInventoryAdjustmentClientMock;
            mockedBean.setActive(enable);
        }
    }

    @Override
    public void setMockValues(final boolean success) {
        final Object bean = Registry.getApplicationContext().getBean(
                "inventoryAdjustmentClient");
        if (bean instanceof TargetInventoryAdjustmentClientMock) {
            final TargetInventoryAdjustmentClientMock targetInventoryAdjustmentClient = (TargetInventoryAdjustmentClientMock)bean;
            targetInventoryAdjustmentClient.setSuccess(success);
        }
    }

}

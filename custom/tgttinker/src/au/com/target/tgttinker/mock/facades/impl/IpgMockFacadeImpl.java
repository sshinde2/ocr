/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtpaymentprovider.ipg.client.IPGClient;
import au.com.target.tgttinker.mock.data.IpgSummaryData;
import au.com.target.tgttinker.mock.facades.IpgMockFacade;
import au.com.target.tgttinker.mock.payment.IpgClientMock;


public class IpgMockFacadeImpl implements IpgMockFacade {

    private IPGClient ipgClient;

    private final IpgSummaryData summaryData = new IpgSummaryData();

    @Override
    public IpgSummaryData getSummary() {
        if (getIpgClient() instanceof IpgClientMock) {
            final IpgClientMock ipgClientMock = (IpgClientMock)getIpgClient();
            summaryData.setMockInUse(true);
            summaryData.setMockRefundResponse(ipgClientMock.getMockRefundResponse());
            summaryData.setDeclineCode(ipgClientMock.getDeclineCode());
            summaryData.setDeclineMessage(ipgClientMock.getDeclineMessage());
            summaryData.setSinglePayDeclinedCode(ipgClientMock.getSinglePayDeclinedCode());
            summaryData.setSinglePayDeclinedMsg(ipgClientMock.getSinglePayDeclinedMsg());
            summaryData.setServiceUnavailable(ipgClientMock.isServiceUnavailable());
        }
        return summaryData;
    }

    @Override
    public void setActive(final boolean enable) {
        if (getIpgClient() instanceof IpgClientMock) {
            final IpgClientMock ipgClientMock = (IpgClientMock)getIpgClient();
            ipgClientMock.setActive(enable);
            summaryData.setMockActive(enable);
            summaryData.setMockRefundDefault(ipgClientMock.getRefundResponse());
        }
    }

    @Override
    public void setTokenResponse(final String selectedToken) {
        summaryData.setSelectedToken(selectedToken);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.IpgMockFacade#setRefundResponse(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public void setRefundResponse(final String refundResponse, final String declineCode, final String declineMessage) {
        if (getIpgClient() instanceof IpgClientMock) {
            final IpgClientMock ipgClientMock = (IpgClientMock)getIpgClient();
            ipgClientMock.setMockRefundResponse(refundResponse);
            ipgClientMock.setDeclineCode(declineCode);
            ipgClientMock.setDeclineMessage(declineMessage);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.IpgMockFacade#setSubmitSinglePaymentResponse(java.lang.String, java.lang.String)
     */
    @Override
    public void setSubmitSinglePaymentResponse(final String declineCode, final String declineMessage) {
        if (getIpgClient() instanceof IpgClientMock) {
            final IpgClientMock ipgClientMock = (IpgClientMock)getIpgClient();
            ipgClientMock.setSinglePayResponseCode(StringUtils.isEmpty(declineCode) ? "0" : "1");
            ipgClientMock.setSinglePayDeclinedCode(declineCode);
            ipgClientMock.setSinglePayDeclinedMsg(declineMessage);
            ipgClientMock.setMockSubmitSinglePayment(StringUtils.isNotEmpty(declineCode));
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.IpgMockFacade#setServiceUnavailable(java.lang.String)
     */
    @Override
    public void setServiceUnavailable(final String parameter) {
        if (getIpgClient() instanceof IpgClientMock) {
            final IpgClientMock ipgClientMock = (IpgClientMock)getIpgClient();
            final boolean serviceUnavailable = "true".equals(parameter);
            ipgClientMock.setServiceUnavailable(serviceUnavailable);
            if (serviceUnavailable) {
                ipgClientMock.setMockSubmitSinglePayment(serviceUnavailable);
                ipgClientMock.setMockFetchToken(serviceUnavailable);
            }
        }
    }

    /**
     * @return the ipgClient
     */
    public IPGClient getIpgClient() {
        if (ipgClient == null) {
            ipgClient = (IPGClient)Registry.getApplicationContext().getBean("ipgClient");
        }
        return ipgClient;
    }
}

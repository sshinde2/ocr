/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.Config;

import org.springframework.beans.factory.annotation.Value;

import au.com.target.tgtpaymentprovider.paypal.PayPalService;
import au.com.target.tgttinker.mock.data.CommonSummaryData;
import au.com.target.tgttinker.mock.facades.CommonMockFacade;
import au.com.target.tgttinker.mock.payment.PayPalServiceMock;


/**
 * @author pratik
 *
 */
public class PayPalMockFacadeImpl implements CommonMockFacade {

    private static final String PAYPAL_HOSTED_PAYMENT_FORM_BASE_URL = "paypal.hostedPaymentFormBaseUrl";
    private static final String BASE_SECURE_URL_KEY = "tgtstorefront.host.fqdn";
    private static final String PAYMENT_PAYPAL_RETURN = "/checkout/payment-details/paypal-return?PayerID=VBK7PWEXG4C9W&token=";

    @Value("#{configurationService.configuration.getProperty('paypal.hostedPaymentFormBaseUrl')}")
    private String payPalHostedPaymentFormBaseUrl;

    private PayPalService payPalService;

    private final CommonSummaryData summaryData = new CommonSummaryData();

    @Override
    public void setActive(final boolean enable) {
        if (getPayPalService() instanceof PayPalServiceMock) {
            final PayPalServiceMock payPalServiceMock = (PayPalServiceMock)getPayPalService();
            payPalServiceMock.setActive(enable);
            if (enable) {
                Config.setParameter(PAYPAL_HOSTED_PAYMENT_FORM_BASE_URL,
                        Config.getParameter(BASE_SECURE_URL_KEY).concat(PAYMENT_PAYPAL_RETURN));
            }
            else {
                Config.setParameter(PAYPAL_HOSTED_PAYMENT_FORM_BASE_URL, payPalHostedPaymentFormBaseUrl);
            }
        }
    }

    @Override
    public CommonSummaryData getSummary() {
        if (getPayPalService() instanceof PayPalServiceMock) {
            final PayPalServiceMock payPalServiceMock = (PayPalServiceMock)getPayPalService();
            summaryData.setMockActive(payPalServiceMock.isActive());
        }
        return summaryData;
    }

    /**
     * @return the payPalService
     */
    public PayPalService getPayPalService() {
        if (payPalService == null) {
            payPalService = (PayPalService)Registry.getApplicationContext().getBean("payPalService");
        }
        return payPalService;
    }

}

/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtpayment.util.PaymentTransactionHelper;
import au.com.target.tgttinker.mock.data.CommonSummaryData;
import au.com.target.tgttinker.mock.data.PinPadPaymentData;
import au.com.target.tgttinker.mock.facades.PinPadPaymentMockFacade;


/**
 *
 */
public class PinPadPaymentMockFacadeImpl implements PinPadPaymentMockFacade {

    private TargetOrderService targetOrderService;

    private ModelService modelService;


    /* (non-Javadoc)
     * @see au.com.target.web.facades.TnsMockFacade#getSummary()
     */
    @Override
    public CommonSummaryData getSummary() {
        //TODO: To implement Mock
        return null;
    }

    /* (non-Javadoc)
    
     * @see au.com.target.web.facades.TnsMockFacade#setActive()
     */
    @Override
    public void setActive(final boolean enable) {
        // YTODO Auto-generated method stub
    }

    @Override
    public void setMockResponse(final String mockResponse) {
        // YTODO Auto-generated method stub
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.PinPadPaymentMockFacade#updatePayment(au.com.target.tgttinker.web.forms.PinPadPaymentDataForm)
     */
    @Override
    public String updatePayment(final PinPadPaymentData pinPadPaymentData) {

        OrderModel orderModel = null;
        try {
            orderModel = targetOrderService.findOrderModelForOrderId(pinPadPaymentData.getOrderNo());

            if (orderModel == null) {
                return "order not found";
            }
        }
        catch (final Exception e) {
            return "order not found";
        }
        PaymentTransactionEntryModel paymentTransactionModel = null;
        try {
            paymentTransactionModel = PaymentTransactionHelper.getLastPayment(
                    orderModel.getPaymentTransactions(),
                    ImmutableList.of(PaymentTransactionType.CAPTURE), TransactionStatus.REVIEW);
            if (paymentTransactionModel == null) {
                return "Payment not found in review status";
            }
        }
        catch (final Exception e) {
            return "Payment not found in review status";
        }

        if ("Failure".equals(pinPadPaymentData.getRespAscii())) {
            paymentTransactionModel.setTransactionStatus(TransactionStatus.REJECTED.toString());
            paymentTransactionModel.setTransactionStatusDetails(TransactionStatus.REJECTED.toString());

        }
        else {
            paymentTransactionModel.setTransactionStatus(TransactionStatus.ACCEPTED.toString());
            paymentTransactionModel.setTransactionStatusDetails(TransactionStatus.ACCEPTED.toString());

        }
        paymentTransactionModel.setTime(new Date());
        modelService.save(paymentTransactionModel);
        final PinPadPaymentInfoModel pinPadPaymentInfoModel = (PinPadPaymentInfoModel)orderModel.getPaymentInfo();
        pinPadPaymentInfoModel.setAccountType(pinPadPaymentData.getAccountType());
        pinPadPaymentInfoModel.setAuthCode(pinPadPaymentData.getAuthCode());
        pinPadPaymentInfoModel.setJournal(pinPadPaymentData.getJournal());
        pinPadPaymentInfoModel.setJournRoll(pinPadPaymentData.getJournRoll());
        pinPadPaymentInfoModel.setCardType(getCreditCardType(pinPadPaymentData.getCardType()));
        pinPadPaymentInfoModel.setMaskedCardNumber(pinPadPaymentData.getMaskedCardNumber());
        pinPadPaymentInfoModel.setRespAscii(pinPadPaymentData.getRespAscii());
        pinPadPaymentInfoModel.setRrn(pinPadPaymentData.getRrn());

        modelService.save(pinPadPaymentInfoModel);

        return "successfully updated";

    }

    public CreditCardType getCreditCardType(final String creditCardType) {
        if ("amex".equalsIgnoreCase(creditCardType)) {
            return CreditCardType.AMEX;
        }
        else if ("mastercard".equalsIgnoreCase(creditCardType)) {
            return CreditCardType.MASTER;
        }
        else if ("diners_club".equalsIgnoreCase(creditCardType)) {
            return CreditCardType.DINERS;
        }
        else if ("Visa".equalsIgnoreCase(creditCardType)) {
            return CreditCardType.VISA;
        }
        return null;

    }

    /**
     * @param targetOrderService
     *            the targetOrderService to set
     */
    @Required
    public void setTargetOrderService(final TargetOrderService targetOrderService) {
        this.targetOrderService = targetOrderService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

}

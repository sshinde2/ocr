/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.data.CommonSummaryData;
import au.com.target.tgttinker.mock.facades.CommonMockFacade;


/**
 *
 */
public class PosLaybyMockFacadeImpl implements CommonMockFacade {

    /* (non-Javadoc)
     * @see au.com.target.web.facades.TnsMockFacade#getSummary()
     */
    @Override
    public CommonSummaryData getSummary() {
        final Object posLaybyAction = Registry.getApplicationContext().getBean("targetLaybyOrderService");
        final CommonSummaryData data = new CommonSummaryData();
        if (posLaybyAction instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)posLaybyAction;
            data.setMockInUse(true);
            data.setMockActive(mockedBean.isActive());
        }
        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.web.facades.TnsMockFacade#setActive()
     */
    @Override
    public void setActive(final boolean enable) {
        final Object posLaybyAction = Registry.getApplicationContext().getBean("targetLaybyOrderService");
        if (posLaybyAction instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)posLaybyAction;
            mockedBean.setActive(enable);
        }
    }
}

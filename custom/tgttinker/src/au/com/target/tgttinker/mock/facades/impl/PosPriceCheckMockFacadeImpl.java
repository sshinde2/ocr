/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgtsale.product.dto.response.ProductResponseDto;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.data.PosPriceCheckSummaryData;
import au.com.target.tgttinker.mock.facades.PosPriceCheckMockFacade;
import au.com.target.tgttinker.mock.pos.product.TargetPOSProductClientMock;


/**
 * @author rmcalave
 *
 */
public class PosPriceCheckMockFacadeImpl implements PosPriceCheckMockFacade {

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#setActive(boolean)
     */
    @Override
    public void setActive(final boolean enable) {
        final Object targetPOSProductClient = Registry.getApplicationContext().getBean("targetPOSProductClient");

        if (targetPOSProductClient instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)targetPOSProductClient;

            mockedBean.setActive(enable);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.PosPriceCheckMockFacade#getSummary()
     */
    @Override
    public PosPriceCheckSummaryData getSummary() {
        final PosPriceCheckSummaryData summaryData = new PosPriceCheckSummaryData();
        final Object targetPOSProductClient = Registry.getApplicationContext().getBean("targetPOSProductClient");

        if (targetPOSProductClient instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)targetPOSProductClient;
            summaryData.setMockInUse(true);
            summaryData.setMockActive(mockedBean.isActive());
        }

        if (targetPOSProductClient instanceof TargetPOSProductClientMock) {
            final TargetPOSProductClientMock mockedTargetPOSProductClient = (TargetPOSProductClientMock)targetPOSProductClient;

            final ProductResponseDto productResponseDto = mockedTargetPOSProductClient.getMockProductResponseDto();
            if (productResponseDto != null) {
                summaryData.setDescription(productResponseDto.getDescription());
                summaryData.setErrorMessage(productResponseDto.getErrorMessage());
                summaryData.setErrorSource(productResponseDto.getErrorSource());
                summaryData.setItemcode(productResponseDto.getItemcode());
                summaryData.setPrice(productResponseDto.getPrice());
                summaryData.setSuccess(productResponseDto.getSuccess());
                summaryData.setTimeout(productResponseDto.isTimeout());
                summaryData.setWasPrice(productResponseDto.getWasPrice());
            }
        }

        return summaryData;
    }

    /*
     * (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.PosPriceCheckMockFacade#setMockResponse(au.com.target.tgtsale.product.dto.response.ProductResponseDto)
     */
    @Override
    public void setMockResponse(final PosPriceCheckSummaryData posPriceCheckSummaryData) {
        final Object targetPOSProductClient = Registry.getApplicationContext().getBean("targetPOSProductClient");
        if (targetPOSProductClient instanceof TargetPOSProductClientMock) {
            ProductResponseDto productResponseDto = null;
            if (posPriceCheckSummaryData != null) {
                productResponseDto = new ProductResponseDto();
                productResponseDto.setDescription(posPriceCheckSummaryData.getDescription());
                productResponseDto.setErrorMessage(posPriceCheckSummaryData.getErrorMessage());
                productResponseDto.setErrorSource(posPriceCheckSummaryData.getErrorSource());
                productResponseDto.setItemcode(posPriceCheckSummaryData.getItemcode());
                productResponseDto.setPrice(posPriceCheckSummaryData.getPrice());
                productResponseDto.setSuccess(posPriceCheckSummaryData.isSuccess());
                productResponseDto.setTimeout(posPriceCheckSummaryData.isTimeout());
                productResponseDto.setWasPrice(posPriceCheckSummaryData.getWasPrice());
            }

            final TargetPOSProductClientMock mockTargetPOSProductClient = (TargetPOSProductClientMock)targetPOSProductClient;
            mockTargetPOSProductClient.setMockProductResponseDto(productResponseDto);
        }
    }
}

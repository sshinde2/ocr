/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.customer.sharewishlist.TargetShareWishlistClientMock;
import au.com.target.tgttinker.mock.data.ShareWishListSummaryData;
import au.com.target.tgttinker.mock.facades.ShareWishListMockFacade;


/**
 * @author mjanarth
 *
 */
public class ShareWishListMockFacadeImpl implements ShareWishListMockFacade {


    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#getSummary()
     */
    @Override
    public ShareWishListSummaryData getSummary() {
        final Object bean = Registry.getApplicationContext().getBean(
                "shareWishListClient");
        final ShareWishListSummaryData data = new ShareWishListSummaryData();

        if (bean instanceof TargetShareWishlistClientMock) {
            final TargetShareWishlistClientMock targetShareWishlistClient = (TargetShareWishlistClientMock)bean;
            data.setMockInUse(true);
            data.setMockActive(targetShareWishlistClient.isActive());
            data.setMockShareWishListResponse(targetShareWishlistClient.getResponseType());
            data.setResponseCode(targetShareWishlistClient.getResponseCode());
            data.setResponseMessage(targetShareWishlistClient.getResponseMessage());
        }

        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#setActive(boolean)
     */
    @Override
    public void setActive(final boolean enable) {
        final Object targetShareWishlistClient = Registry.getApplicationContext().getBean(
                "shareWishListClient");

        if (targetShareWishlistClient instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)targetShareWishlistClient;
            mockedBean.setActive(enable);
        }
    }

    @Override
    public void setMockValues(final String responseCode, final String responseMessage, final String responseType) {
        final Object bean = Registry.getApplicationContext().getBean(
                "shareWishListClient");
        if (bean instanceof TargetShareWishlistClientMock) {
            final TargetShareWishlistClientMock targetShareWishlistClient = (TargetShareWishlistClientMock)bean;
            targetShareWishlistClient.setResponseCode(responseCode);
            targetShareWishlistClient.setResponseMessage(responseMessage);
            targetShareWishlistClient.setResponseType(responseType);
        }
    }

}

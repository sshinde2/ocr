/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import au.com.target.tgtcore.stockvisibility.client.StockVisibilityClient;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtsale.stock.client.TargetStockUpdateClient;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.data.StockVisibilitySummaryData;
import au.com.target.tgttinker.mock.facades.StockVisibilityMockFacade;
import au.com.target.tgttinker.mock.instore.MockTargetStockUpdateClient;
import au.com.target.tgttinker.mock.stockvisibility.StockVisibilityClientMock;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;


/**
 * @author rmcalave
 *
 */
public class StockVisibilityMockFacadeImpl implements StockVisibilityMockFacade {

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#getSummary()
     */
    @Override
    public StockVisibilitySummaryData getSummary() {
        final StockVisibilitySummaryData data = new StockVisibilitySummaryData();

        final StockVisibilityClient stockVisibilityClient = getStockVisibilityClient();
        if (stockVisibilityClient instanceof StockVisibilityClientMock) {
            final StockVisibilityClientMock mockStockVisibilityClient = (StockVisibilityClientMock)stockVisibilityClient;

            data.setMockInUse(true);
            data.setMockActive(mockStockVisibilityClient.isActive());
            data.setException(mockStockVisibilityClient.getException());
            data.setResponseCode(mockStockVisibilityClient.getResponseCode());
            data.setStockVisibilityItemResponseDtos(mockStockVisibilityClient.getStockVisibilityItemResponseDtoList());
        }

        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#setActive(boolean)
     */
    @Override
    public void setActive(final boolean enable) {
        final StockVisibilityClient stockVisibilityClient = getStockVisibilityClient();
        if (stockVisibilityClient instanceof MockedByTarget) {
            ((MockedByTarget)stockVisibilityClient).setActive(enable);
        }

        final TargetStockUpdateClient targetStockUpdateClient = getTargetStockUpdateClient();
        if (targetStockUpdateClient instanceof MockedByTarget) {
            ((MockedByTarget)targetStockUpdateClient).setActive(enable);
        }
    }

    @Override
    public void addStockVisibilityItemResponseDto(final String storeNumber, final String itemcode, final String soh) {
        final StockVisibilityClient stockVisibilityClient = getStockVisibilityClient();
        if (stockVisibilityClient instanceof StockVisibilityClientMock) {
            final StockVisibilityClientMock mockStockVisibilityClient = (StockVisibilityClientMock)stockVisibilityClient;
            final StockVisibilityItemResponseDto dto = new StockVisibilityItemResponseDto();
            dto.setStoreNumber(storeNumber);
            dto.setCode(itemcode);
            dto.setSoh(soh);
            mockStockVisibilityClient.getStockVisibilityItemResponseDtoList().add(dto);
        }

        final TargetStockUpdateClient targetStockUpdateClient = getTargetStockUpdateClient();
        if (targetStockUpdateClient instanceof MockedByTarget) {
            final MockTargetStockUpdateClient mockTargetStockUpdateClient = (MockTargetStockUpdateClient)targetStockUpdateClient;
            mockTargetStockUpdateClient.addStoreStock(storeNumber, itemcode, soh);
        }
    }

    @Override
    public void removeStockVisibilityItemResponseDto(final String storeNumber, final String itemcode) {
        final StockVisibilityClient stockVisibilityClient = getStockVisibilityClient();
        if (stockVisibilityClient instanceof StockVisibilityClientMock) {
            final StockVisibilityClientMock mockStockVisibilityClient = (StockVisibilityClientMock)stockVisibilityClient;

            final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtos = mockStockVisibilityClient
                    .getStockVisibilityItemResponseDtoList();
            final Collection<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtosToRemove = Collections2
                    .filter(stockVisibilityItemResponseDtos, new Predicate<StockVisibilityItemResponseDto>() {

                        @Override
                        public boolean apply(final StockVisibilityItemResponseDto arg0) {
                            return arg0.getStoreNumber().equals(storeNumber) && arg0.getCode().equals(itemcode);
                        }

                    });

            stockVisibilityItemResponseDtos.removeAll(stockVisibilityItemResponseDtosToRemove);
        }
    }

    @Override
    public void resetStockVisibilityItemResponseDto() {
        final StockVisibilityClient stockVisibilityClient = getStockVisibilityClient();
        if (stockVisibilityClient instanceof StockVisibilityClientMock) {
            final StockVisibilityClientMock mockStockVisibilityClient = (StockVisibilityClientMock)stockVisibilityClient;

            mockStockVisibilityClient
                    .setStockVisibilityItemResponseDtoList(new ArrayList<StockVisibilityItemResponseDto>());
        }
    }

    @Override
    public void updateStockVisibilityItemResponseDto(final String storeNumber, final String itemcode,
            final String soh) {
        final StockVisibilityClient stockVisibilityClient = getStockVisibilityClient();

        final StockVisibilityClientMock mockStockVisibilityClient = (StockVisibilityClientMock)stockVisibilityClient;

        final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtos = mockStockVisibilityClient
                .getStockVisibilityItemResponseDtoList();

        for (final StockVisibilityItemResponseDto stockVisibilityItemResponseDto : stockVisibilityItemResponseDtos) {
            if (stockVisibilityItemResponseDto.getStoreNumber().equals(storeNumber)) {
                stockVisibilityItemResponseDto.setSoh(soh);
            }
        }

        mockStockVisibilityClient
                .setStockVisibilityItemResponseDtoList(stockVisibilityItemResponseDtos);


        final TargetStockUpdateClient targetStockUpdateClient = getTargetStockUpdateClient();
        if (targetStockUpdateClient instanceof MockedByTarget) {
            final MockTargetStockUpdateClient mockTargetStockUpdateClient = (MockTargetStockUpdateClient)targetStockUpdateClient;
            mockTargetStockUpdateClient.addStoreStock(storeNumber, itemcode, soh);
        }
    }

    private StockVisibilityClient getStockVisibilityClient() {
        return Registry.getApplicationContext().getBean("stockVisibilityClient", StockVisibilityClient.class);
    }



    private TargetStockUpdateClient getTargetStockUpdateClient() {
        return Registry.getApplicationContext().getBean("targetStockUpdateClient", TargetStockUpdateClient.class);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.StockVisibilityMockFacade#setReponseCode(java.lang.String)
     */
    @Override
    public void setResponseCode(final String code) {
        final StockVisibilityClient stockVisibilityClient = getStockVisibilityClient();
        if (stockVisibilityClient instanceof MockedByTarget) {
            ((StockVisibilityClientMock)stockVisibilityClient).setResponseCode(code);
        }
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.StockVisibilityMockFacade#setException(java.lang.String)
     */
    @Override
    public void setException(final String exception) {
        final StockVisibilityClient stockVisibilityClient = getStockVisibilityClient();
        if (stockVisibilityClient instanceof MockedByTarget) {
            ((StockVisibilityClientMock)stockVisibilityClient).setException(exception);
        }
    }
}

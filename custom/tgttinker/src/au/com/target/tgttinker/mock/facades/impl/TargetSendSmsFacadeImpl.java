/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.data.SendSmsSummaryData;
import au.com.target.tgttinker.mock.facades.TargetSendSmsFacadeMock;
import au.com.target.tgttinker.mock.sendSms.TargetSendSmsClientMock;


/**
 * @author mjanarth
 * 
 */
public class TargetSendSmsFacadeImpl implements TargetSendSmsFacadeMock {

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#getSummary()
     */
    @Override
    public SendSmsSummaryData getSummary() {
        final Object mockTargetSendSmsClient =
                Registry.getApplicationContext().getBean("targetSendSmsClient");
        final SendSmsSummaryData data = new SendSmsSummaryData();
        if (mockTargetSendSmsClient instanceof TargetSendSmsClientMock) {
            final TargetSendSmsClientMock mockedBean = (TargetSendSmsClientMock)mockTargetSendSmsClient;
            data.setMockInUse(true);
            data.setMockActive(mockedBean.isActive());
            data.setMockResponse(((TargetSendSmsClientMock)mockTargetSendSmsClient)
                    .getMockResponse());

        }
        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.CommonMockFacade#setActive(boolean)
     */
    @Override
    public void setActive(final boolean enable) {
        final Object sensSmsAction = Registry.getApplicationContext().getBean("targetSendSmsClient");
        if (sensSmsAction instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget)sensSmsAction;
            mockedBean.setActive(enable);
        }
    }

    @Override
    public void setMockResponse(final String mockResponse) {
        final Object mockSendSms =
                Registry.getApplicationContext().getBean("targetSendSmsClient");
        if (mockSendSms instanceof TargetSendSmsClientMock) {
            final TargetSendSmsClientMock mockedBean = (TargetSendSmsClientMock)mockSendSms;
            mockedBean.setMockResponse(mockResponse);

        }

    }
}

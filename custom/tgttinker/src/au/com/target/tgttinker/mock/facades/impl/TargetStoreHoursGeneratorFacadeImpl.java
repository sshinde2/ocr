/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.storelocator.TargetOpeningDayService;
import au.com.target.tgttinker.mock.facades.TargetStoreHoursGeneratorFacade;


/**
 * @author rmcalave
 *
 */
public class TargetStoreHoursGeneratorFacadeImpl implements TargetStoreHoursGeneratorFacade {

    private static final Logger LOG = Logger.getLogger(TargetStoreHoursGeneratorFacadeImpl.class);

    private static final String DUMMY_STORE = "dummyStoreno";
    private static final String DUMMY_STORE_COUNTRY = "dummyStorenoCountry";

    private ModelService modelService;
    private FlexibleSearchService flexibleSearchService;
    private TargetOpeningDayService targetOpeningDayService;

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.web.facades.impl.TargetStoreHoursGeneratorFacade#regenerateStoreHours()
     */
    @Override
    public void regenerateStoreHours() {
        removeAllStoreHours();
        createStoreHours();
    }

    private void removeAllStoreHours() {
        LOG.info("Removing all store hours...");

        final SearchResult<OpeningDayModel> result = flexibleSearchService.<OpeningDayModel> search("SELECT "
                + OpeningDayModel.PK + " FROM {" + OpeningDayModel._TYPECODE + "}");
        final List<OpeningDayModel> allOpeningDays = result.getResult();
        if (CollectionUtils.isNotEmpty(allOpeningDays)) {
            modelService.removeAll(allOpeningDays);
        }

        LOG.info("Store hours removed.");
    }

    private void createStoreHours() {
        LOG.info("Creating store hours for Target and Target Country...");

        final LocalDate startOfThisWeek = LocalDate.now().withDayOfWeek(DateTimeConstants.MONDAY);

        final LocalTime targetOpeningTime = new LocalTime(8, 0, 0);
        final LocalTime targetClosingTime = new LocalTime(20, 0, 0);
        final LocalTime targetCountryOpeningTime = new LocalTime(9, 0, 0);
        final LocalTime targetCountryClosingTime = new LocalTime(17, 0, 0);


        final OpeningScheduleModel targetOpeningScheduleExample = new OpeningScheduleModel();
        targetOpeningScheduleExample.setCode("target-hours-0");
        final OpeningScheduleModel targetOpeningSchedule = flexibleSearchService
                .getModelByExample(targetOpeningScheduleExample);

        final OpeningScheduleModel targetCountryOpeningScheduleExample = new OpeningScheduleModel();
        targetCountryOpeningScheduleExample.setCode("target-country-hours-0");
        final OpeningScheduleModel targetCountryOpeningSchedule = flexibleSearchService
                .getModelByExample(targetCountryOpeningScheduleExample);

        for (int i = 0; i < 14; i++) {

            final TargetOpeningDayModel targetOpeningDay = modelService.create(TargetOpeningDayModel.class);
            targetOpeningDay.setOpeningSchedule(targetOpeningSchedule);
            targetOpeningDay.setOpeningTime(startOfThisWeek.plusDays(i).toDateTime(targetOpeningTime).toDate());
            targetOpeningDay.setClosingTime(startOfThisWeek.plusDays(i).toDateTime(targetClosingTime).toDate());
            targetOpeningDay.setTargetOpeningDayId(targetOpeningDayService.getTargetOpeningDayId(DUMMY_STORE + i,
                    targetOpeningDay.getOpeningTime()));
            modelService.save(targetOpeningDay);

            final TargetOpeningDayModel targetCountryOpeningDay = modelService.create(TargetOpeningDayModel.class);
            targetCountryOpeningDay.setOpeningSchedule(targetCountryOpeningSchedule);
            targetCountryOpeningDay.setOpeningTime(startOfThisWeek.plusDays(i).toDateTime(targetCountryOpeningTime)
                    .toDate());
            targetCountryOpeningDay.setClosingTime(startOfThisWeek.plusDays(i).toDateTime(targetCountryClosingTime)
                    .toDate());
            targetCountryOpeningDay.setTargetOpeningDayId(targetOpeningDayService.getTargetOpeningDayId(
                    DUMMY_STORE_COUNTRY + i, targetOpeningDay.getOpeningTime()));
            modelService.save(targetCountryOpeningDay);
        }

        LOG.info("Store hours created.");
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    /**
     * @param targetOpeningDayService
     *            the targetOpeningDayService to set
     */
    @Required
    public void setTargetOpeningDayService(final TargetOpeningDayService targetOpeningDayService) {
        this.targetOpeningDayService = targetOpeningDayService;
    }
}

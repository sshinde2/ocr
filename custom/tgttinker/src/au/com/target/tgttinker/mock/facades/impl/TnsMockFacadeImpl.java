/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import de.hybris.platform.core.Registry;

import au.com.target.tgttinker.mock.data.TnsSummaryData;
import au.com.target.tgttinker.mock.facades.TnsMockFacade;
import au.com.target.tgttinker.mock.payment.TargetCreditCardPaymentMethodMock;


/**
 *
 */
public class TnsMockFacadeImpl implements TnsMockFacade {

    /* (non-Javadoc)
     * @see au.com.target.web.facades.TnsMockFacade#getSummary()
     */
    @Override
    public TnsSummaryData getSummary() {
        final Object targetCreditCardPaymentMethod =
                Registry.getApplicationContext().getBean("targetCreditCardPaymentMethod");
        final TnsSummaryData data = new TnsSummaryData();
        if (targetCreditCardPaymentMethod instanceof TargetCreditCardPaymentMethodMock) {
            final TargetCreditCardPaymentMethodMock mockedBean = (TargetCreditCardPaymentMethodMock)targetCreditCardPaymentMethod;
            data.setMockInUse(true);
            data.setMockActive(mockedBean.isActive());
            data.setMockResponse(mockedBean.getMockResponse());
            final int left = mockedBean.getResponseLeft();
            if (left == 0) {
                data.setMockStatus(data.getMockResponse());
            }
            else {
                data.setMockStatus(data.getMockResponse() + " " + left + " time(s) then "
                        + mockedBean.getNextMockResponse());
            }
        }
        return data;
    }

    /* (non-Javadoc)
     * @see au.com.target.web.facades.TnsMockFacade#setActive()
     */
    @Override
    public void setActive(final boolean enable) {
        final Object targetCreditCardPaymentMethod =
                Registry.getApplicationContext().getBean("targetCreditCardPaymentMethod");
        if (targetCreditCardPaymentMethod instanceof TargetCreditCardPaymentMethodMock) {
            final TargetCreditCardPaymentMethodMock mockedBean = (TargetCreditCardPaymentMethodMock)targetCreditCardPaymentMethod;
            mockedBean.setActive(enable);
        }
    }

    @Override
    public void setMockResponse(final String mockResponse, final int times, final String nextMockResponse) {
        final Object targetCreditCardPaymentMethod =
                Registry.getApplicationContext().getBean("targetCreditCardPaymentMethod");
        if (targetCreditCardPaymentMethod instanceof TargetCreditCardPaymentMethodMock) {
            final TargetCreditCardPaymentMethodMock mockedBean = (TargetCreditCardPaymentMethodMock)targetCreditCardPaymentMethod;
            mockedBean.setMockResponse(mockResponse, times, nextMockResponse);
        }
    }
}

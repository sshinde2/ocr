/**
 * 
 */
package au.com.target.tgttinker.mock.facades.impl;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.BooleanUtils;
import org.codehaus.jackson.map.ObjectMapper;

import au.com.target.tgtpaymentprovider.zippay.client.TargetZippayClient;
import au.com.target.tgtpaymentprovider.zippay.data.response.CheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateChargeResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateCheckoutResponse;
import au.com.target.tgtpaymentprovider.zippay.data.response.CreateRefundResponse;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgttinker.mock.data.ZipPaymentSummaryData;
import au.com.target.tgttinker.mock.facades.ZipPaymentMockFacade;
import au.com.target.tgttinker.mock.payment.TargetAfterpayClientMock;
import au.com.target.tgttinker.mock.payment.TargetZippayClientMock;


/**
 * @author hsing179
 *
 */
public class ZipPaymentMockFacadeImpl implements ZipPaymentMockFacade {

    private TargetZippayClient zippayClient;

    private final ZipPaymentSummaryData summaryData = new ZipPaymentSummaryData();

    public TargetZippayClient getZippayClient() {
        return zippayClient;
    }

    public void setZippayClient(final TargetZippayClient zippayClient) {
        this.zippayClient = zippayClient;
    }

    @Override
    public void setActive(final boolean enable) {
        if (getZippayClient() instanceof MockedByTarget) {
            final MockedByTarget mockedBean = (MockedByTarget) getZippayClient();
            mockedBean.setActive(enable);
            summaryData.setMockActive(enable);
        }
    }

    @Override
    public ZipPaymentSummaryData getSummary() {
        if (summaryData.getCreateCheckoutResponse() == null) {
            summaryData.setCreateCheckoutResponse(readJson("/zippay/createCheckoutResponse.json"));
        }

        if (summaryData.getRetrieveCheckoutResponse() == null) {
            summaryData.setRetrieveCheckoutResponse(readJson("/zippay/retrieveCheckoutResponse.json"));
        }

        if (summaryData.getCaptureRequestResponse() == null) {
            summaryData.setCaptureRequestResponse(readJson("/zippay/captureRequestResponse.json"));
        }

        if (summaryData.getMockRefundResponse() == null) {
            summaryData.setMockRefundResponse(readJson("/zippay/mockRefundResponse.json"));
        }

        return summaryData;
    }

    @Override
    public void setThrowException(final boolean active) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock afterpayClientMock = (TargetZippayClientMock) getZippayClient();
            final boolean throwException = BooleanUtils.toBoolean(active);
            afterpayClientMock.setThrowExpection(throwException);
            summaryData.setThrowException(throwException);
        }
    }

    @Override
    public void setCreateCheckoutException(final boolean active) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setCreateCheckoutException(active);
            summaryData.setCreateCheckoutException(active);
        }
    }

    @Override
    public void setPingActive(final boolean active) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setPingActive(active);
            summaryData.setPingActive(active);
        }
    }

    @Override
    public void setCreateCheckoutActive(boolean active) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setCreateCheckoutActive(active);
            summaryData.setCreateCheckoutActive(active);
        }
    }

    @Override
    public void setMockCreateCheckoutResponse(final String mock) throws IllegalArgumentException {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final CreateCheckoutResponse mockedData = mapper.readValue(mock, CreateCheckoutResponse.class);
            if (getZippayClient() instanceof TargetZippayClientMock) {
                final TargetZippayClientMock zippayClientMock = (TargetZippayClientMock) getZippayClient();
                summaryData.setCreateCheckoutResponse(mock);
                zippayClientMock.setCreateCheckoutResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }
    }

    @Override
    public void resetDefault() {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.resetDefault();
            summaryData.setCreateCheckoutResponse(null);
            summaryData.setRetrieveCheckoutResponse(null);
            summaryData.setCaptureRequestResponse(null);
            summaryData.setMockRefundResponse(null);
        }
    }

    @Override
    public void setMockRetrieveCheckoutResponse(final String retrieveCheckoutResponse) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final CheckoutResponse mockedData = mapper.readValue(retrieveCheckoutResponse, CheckoutResponse.class);
            if (getZippayClient() instanceof TargetZippayClientMock) {
                final TargetZippayClientMock zippayClientMock = (TargetZippayClientMock) getZippayClient();
                summaryData.setRetrieveCheckoutResponse(retrieveCheckoutResponse);
                zippayClientMock.setRetrieveCheckoutResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }
    }

    @Override
    public void setMockCaptureRequestResponse(final String captureRequestResponse) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final CreateChargeResponse mockedData = mapper.readValue(captureRequestResponse, CreateChargeResponse.class);
            if (getZippayClient() instanceof TargetZippayClientMock) {
                final TargetZippayClientMock zippayClientMock = (TargetZippayClientMock) getZippayClient();
                summaryData.setCaptureRequestResponse(captureRequestResponse);
                zippayClientMock.setCaptureRequestResponse(mockedData);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }
    }

    @Override
    public void setRetrieveCheckoutActive(final boolean retrieveCheckoutActive) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setRetrieveCheckoutActive(retrieveCheckoutActive);
            summaryData.setRetrieveCheckoutActive(retrieveCheckoutActive);
        }
    }

    @Override
    public void setRetrieveCheckoutException(final boolean retrieveCheckoutException) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setRetrieveCheckoutException(retrieveCheckoutException);
            summaryData.setRetrieveCheckoutException(retrieveCheckoutException);
        }
    }

    @Override
    public void setCaptureRequestActive(final boolean captureRequestActive) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setCaptureRequestActive(captureRequestActive);
            summaryData.setCaptureRequestActive(captureRequestActive);
        }
    }

    @Override
    public void setCaptureRequestException(final boolean captureRequestException) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setCaptureRequestException(captureRequestException);
            summaryData.setCaptureRequestException(captureRequestException);
        }
    }

    @Override
    public void setRefundRequestActive(final boolean refundRequestActive) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setRefundRequestActive(refundRequestActive);
            summaryData.setRefundRequestActive(refundRequestActive);
        }
    }

    @Override
    public void setRefundRequestException(final boolean refundRequestException) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setRefundRequestException(refundRequestException);
            summaryData.setRefundRequestException(refundRequestException);
        }
    }

    @Override
    public void setRefundRejectException(final boolean refundRejectException) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setRefundRejectException(refundRejectException);
            summaryData.setRefundRejectException(refundRejectException);
        }
    }

    @Override
    public void setRefundTransErrorException(final boolean refundTransErrorException) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setRefundTransErrorException(refundTransErrorException);
            summaryData.setRefundTransErrorException(refundTransErrorException);
        }
    }

    @Override
    public void setMockRefundResponse(final String mockRefundResponse) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            final CreateRefundResponse mockedData = mapper.readValue(mockRefundResponse, CreateRefundResponse.class);
            if (getZippayClient() instanceof TargetZippayClientMock) {
                final TargetZippayClientMock zippayClientMock = (TargetZippayClientMock) getZippayClient();
                zippayClientMock.setMockRefundResponse(mockedData);
                summaryData.setMockRefundResponse(mockRefundResponse);
            }
        }
        catch (final IOException e) {
            throw new IllegalArgumentException("mock data is not in expected format");
        }
    }

    @Override
    public void setCaptureTransactionError(final boolean captureTransactionError) {
        if (getZippayClient() instanceof TargetZippayClientMock) {
            final TargetZippayClientMock mockClient = (TargetZippayClientMock) getZippayClient();
            mockClient.setCaptureTransactionError(captureTransactionError);
            summaryData.setCaptureTransactionError(captureTransactionError);
        }
    }

    private String readJson(final String filename) {
        final InputStream is = AfterpayMockFacadeImpl.class.getResourceAsStream(filename);
        String jsonTxt = null;
        try {
            jsonTxt = IOUtils.toString(is);
        }
        catch (final IOException e) {
            fail("fail to read json");
        }
        return jsonTxt;
    }


}

/**
 * 
 */
package au.com.target.tgttinker.mock.fluent;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtfluent.client.impl.FluentClientImpl;
import au.com.target.tgtfluent.data.Batch;
import au.com.target.tgtfluent.data.BatchResponse;
import au.com.target.tgtfluent.data.Category;
import au.com.target.tgtfluent.data.CategoryResponse;
import au.com.target.tgtfluent.data.Event;
import au.com.target.tgtfluent.data.EventResponse;
import au.com.target.tgtfluent.data.FluentResponse;
import au.com.target.tgtfluent.data.Fulfilment;
import au.com.target.tgtfluent.data.FulfilmentItem;
import au.com.target.tgtfluent.data.FulfilmentOptionAttribute;
import au.com.target.tgtfluent.data.FulfilmentOptionItem;
import au.com.target.tgtfluent.data.FulfilmentOptionRequest;
import au.com.target.tgtfluent.data.FulfilmentOptionResponse;
import au.com.target.tgtfluent.data.FulfilmentPlan;
import au.com.target.tgtfluent.data.FulfilmentsResponse;
import au.com.target.tgtfluent.data.Job;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtfluent.data.OrderResponse;
import au.com.target.tgtfluent.data.Product;
import au.com.target.tgtfluent.data.ProductsResponse;
import au.com.target.tgtfluent.data.Sku;
import au.com.target.tgtfluent.data.SkusResponse;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 * @author mgazal
 *
 */
public class FluentClientMock extends FluentClientImpl implements MockedByTarget {

    private boolean batchActive;

    private FluentResponse createJobResponse;

    private FluentResponse createBatchResponse;

    private boolean createCategoryActive;

    private boolean updateCategoryActive;

    private boolean searchCategoryActive;

    private FluentResponse createCategoryResponse;

    private FluentResponse updateCategoryResponse;

    private CategoryResponse searchCategoryResponse;

    private boolean createProductActive;

    private boolean updateProductActive;

    private boolean searchProductActive;

    private FluentResponse createProductResponse;

    private FluentResponse updateProductResponse;

    private ProductsResponse searchProductResponse;

    private boolean createSkuActive;

    private boolean updateSkuActive;

    private boolean searchSkuActive;

    private FluentResponse createSkuResponse;

    private FluentResponse updateSkuResponse;

    private SkusResponse searchSkuResponse;

    private BatchResponse batchResponse;

    private boolean retrieveFulfilmentsByOrderActive;

    private FulfilmentOptionResponse fulfilmentOptionResponse;

    private Map<String, Map<String, Integer>> fulfilmentOptionMap;

    private boolean createOrderActive;

    private FluentResponse createOrderResponse;

    private boolean eventSyncActive;

    private EventResponse eventResponse;

    private Job jobStub;

    private Batch batchStub;

    private Category categoryStub;

    private Product productStub;

    private Sku skuStub;

    private Order orderStub;

    private Event eventStub;

    private boolean createFulfilmentActive;

    private FulfilmentsResponse fulfilmentsResponse;

    private boolean viewOrderActive;

    private OrderResponse viewOrderResponse;


    @Override
    public FluentResponse createJob(final Job job) {
        if (batchActive) {
            job.setRetailerId("1");
            jobStub = job;
            return createJobResponse;
        }
        return super.createJob(job);
    }

    @Override
    public FluentResponse createBatch(final String jobId, final Batch batch) {
        if (batchActive) {
            batch.setRetailerId("1");
            batchStub = batch;
            return createBatchResponse;
        }
        return super.createBatch(jobId, batch);
    }


    @Override
    public BatchResponse viewBatch(final String jobId, final String batchId, final Map<String, String> queryParams) {
        if (batchActive) {
            return batchResponse;
        }
        return super.viewBatch(jobId, batchId, queryParams);
    }

    @Override
    public FluentResponse createProduct(final Product product) {
        if (createProductActive) {
            productStub = product;
            return createProductResponse;
        }
        return super.createProduct(product);
    }

    @Override
    public FluentResponse updateProduct(final String fluentId, final Product product) {
        if (updateProductActive) {
            productStub = product;
            return updateProductResponse;
        }
        return super.updateProduct(fluentId, product);
    }


    @Override
    public ProductsResponse searchProduct(final String id) {
        if (searchProductActive) {
            return searchProductResponse;
        }
        return super.searchProduct(id);
    }

    @Override
    public FluentResponse createCategory(final Category category) {
        if (createCategoryActive) {
            categoryStub = category;
            return createCategoryResponse;
        }
        return super.createCategory(category);
    }

    @Override
    public FluentResponse updateCategory(final String fluentId, final Category category) {
        if (updateCategoryActive) {
            categoryStub = category;
            return updateCategoryResponse;
        }
        return super.updateCategory(fluentId, category);
    }


    @Override
    public CategoryResponse searchCategory(final String id) {
        if (searchCategoryActive) {
            return searchCategoryResponse;
        }
        return super.searchCategory(id);
    }

    @Override
    public FluentResponse createSku(final String productId, final Sku sku) {
        if (createSkuActive) {
            skuStub = sku;
            return createSkuResponse;
        }
        return super.createSku(productId, sku);
    }

    @Override
    public FluentResponse updateSku(final String productId, final String skuId, final Sku sku) {
        if (updateSkuActive) {
            skuStub = sku;
            return updateSkuResponse;
        }
        return super.updateSku(productId, skuId, sku);
    }

    @Override
    public SkusResponse searchSkus(final String skuRef) {
        if (searchSkuActive) {
            return searchSkuResponse;
        }
        return super.searchSkus(skuRef);
    }

    @Override
    public FluentResponse createFulfilmentOption(final FulfilmentOptionRequest fulfilmentOptionRequest) {
        if (createFulfilmentActive) {
            if (fulfilmentOptionMap != null) {
                final List<String> variants = new ArrayList<>();
                final List<String> deliveryTypes = new ArrayList<>();
                final List<String> locationRefs = new ArrayList<>();
                extractParamsFromRequest(fulfilmentOptionRequest, variants, deliveryTypes, locationRefs);
                processFulfilmentOptionMap(variants, deliveryTypes, locationRefs);
            }
            return fulfilmentOptionResponse;
        }
        return super.createFulfilmentOption(fulfilmentOptionRequest);
    }

    @Override
    public FluentResponse eventSync(final Event event) {
        if (eventSyncActive) {
            event.setAccountId(getAccountId());
            event.setRetailerId("1");
            logJson("eventSync", "request=", event);
            eventStub = event;
            return eventResponse;
        }
        return super.eventSync(event);
    }

    @Override
    public FulfilmentsResponse retrieveFulfilmentsByOrder(final String fluentOrderId) {
        if (retrieveFulfilmentsByOrderActive) {
            return fulfilmentsResponse;
        }
        return super.retrieveFulfilmentsByOrder(fluentOrderId);
    }

    /**
     * @param fulfilmentOptionRequest
     * @param variants
     * @param deliveryTypes
     * @param locationRefs
     */
    private void extractParamsFromRequest(final FulfilmentOptionRequest fulfilmentOptionRequest,
            final List<String> variants, final List<String> deliveryTypes, final List<String> locationRefs) {
        for (final FulfilmentOptionItem item : fulfilmentOptionRequest.getItems()) {
            variants.add(item.getSkuRef());
        }
        for (final FulfilmentOptionAttribute attribute : fulfilmentOptionRequest.getAttributes()) {
            if ("deliveryTypes".equals(attribute.getName())) {
                deliveryTypes.addAll((Collection<? extends String>)attribute.getValue());
            }
            if ("locationRefs".equals(attribute.getName())) {
                locationRefs.addAll((Collection<? extends String>)attribute.getValue());
            }
        }
    }

    /**
     * @param variants
     * @param deliveryTypes
     * @param locationRefs
     */
    private void processFulfilmentOptionMap(final List<String> variants, final List<String> deliveryTypes,
            final List<String> locationRefs) {
        fulfilmentOptionResponse = new FulfilmentOptionResponse();
        final FulfilmentPlan plan = new FulfilmentPlan();
        fulfilmentOptionResponse.setPlans(Arrays.asList(plan));
        final List<Fulfilment> fulfilments = new ArrayList<>(fulfilmentOptionMap.size());
        plan.setFulfilments(fulfilments);

        for (final Entry<String, Map<String, Integer>> option : fulfilmentOptionMap.entrySet()) {
            if (!deliveryTypes.contains(option.getKey()) && !locationRefs.contains(option.getKey())) {
                continue;
            }
            fulfilments.add(getFulFilment(option, variants));
        }
    }

    /**
     * @param option
     * @param variants
     * @return {@link Fulfilment}
     */
    private Fulfilment getFulFilment(final Entry<String, Map<String, Integer>> option, final List<String> variants) {
        final Fulfilment fulfilment = new Fulfilment();
        if (Arrays.asList(TgtCoreConstants.DELIVERY_TYPE.CC, TgtCoreConstants.DELIVERY_TYPE.HD,
                TgtCoreConstants.DELIVERY_TYPE.ED, TgtCoreConstants.DELIVERY_TYPE.CONSOLIDATED_STORES_SOH,
                TgtCoreConstants.DELIVERY_TYPE.PO)
                .contains(option.getKey())) {
            fulfilment.setFulfilmentType(option.getKey());
        }
        else {
            fulfilment.setFulfilmentType(TgtCoreConstants.DELIVERY_TYPE.STORE_SOH);
            fulfilment.setLocationRef(option.getKey());
        }
        fulfilment.setItems(new ArrayList<FulfilmentItem>());
        FulfilmentItem item;
        for (final Entry<String, Integer> stockEntry : option.getValue().entrySet()) {
            if (!variants.contains(stockEntry.getKey())) {
                continue;
            }
            item = new FulfilmentItem();
            item.setSkuRef(stockEntry.getKey());
            item.setAvailableQty(stockEntry.getValue());
            fulfilment.getItems().add(item);
        }
        return fulfilment;
    }

    @Override
    public FluentResponse createOrder(final Order createOrderRequest) {
        if (createOrderActive) {
            orderStub = createOrderRequest;
            return createOrderResponse;
        }
        return super.createOrder(createOrderRequest);
    }

    @Override
    public FluentResponse viewOrder(final String fluentOrderId) {
        if (viewOrderActive) {
            return viewOrderResponse;
        }
        return super.viewOrder(fluentOrderId);
    }

    @Override
    public boolean isActive() {
        return batchActive || createCategoryActive || updateCategoryActive || searchCategoryActive
                || createProductActive || updateProductActive || searchProductActive || createSkuActive
                || updateSkuActive || searchSkuActive || createFulfilmentActive || createOrderActive
                || retrieveFulfilmentsByOrderActive || viewOrderActive;
    }

    @Override
    public void setActive(final boolean active) {
        batchActive = active;

        createCategoryActive = active;
        updateCategoryActive = active;
        searchCategoryActive = active;

        createProductActive = active;
        updateProductActive = active;
        searchProductActive = active;

        createSkuActive = active;
        updateSkuActive = active;
        searchSkuActive = active;

        createFulfilmentActive = active;

        createOrderActive = active;
        eventSyncActive = active;
        viewOrderActive = active;

        retrieveFulfilmentsByOrderActive = active;

    }

    /**
     * @return the batchActive
     */
    public boolean isBatchActive() {
        return batchActive;
    }

    /**
     * @param batchActive
     *            the batchActive to set
     */
    public void setBatchActive(final boolean batchActive) {
        this.batchActive = batchActive;
    }

    /**
     * @return the createJobResponse
     */
    public FluentResponse getCreateJobResponse() {
        return createJobResponse;
    }

    /**
     * @param createJobResponse
     *            the createJobResponse to set
     */
    public void setCreateJobResponse(final FluentResponse createJobResponse) {
        this.createJobResponse = createJobResponse;
    }

    /**
     * @return the createBatchResponse
     */
    public FluentResponse getCreateBatchResponse() {
        return createBatchResponse;
    }

    /**
     * @param createBatchResponse
     *            the createBatchResponse to set
     */
    public void setCreateBatchResponse(final FluentResponse createBatchResponse) {
        this.createBatchResponse = createBatchResponse;
    }

    /**
     * @return the createCategoryActive
     */
    public boolean isCreateCategoryActive() {
        return createCategoryActive;
    }

    /**
     * @param createCategoryActive
     *            the createCategoryActive to set
     */
    public void setCreateCategoryActive(final boolean createCategoryActive) {
        this.createCategoryActive = createCategoryActive;
    }

    /**
     * @return the updateCategoryActive
     */
    public boolean isUpdateCategoryActive() {
        return updateCategoryActive;
    }

    /**
     * @param updateCategoryActive
     *            the updateCategoryActive to set
     */
    public void setUpdateCategoryActive(final boolean updateCategoryActive) {
        this.updateCategoryActive = updateCategoryActive;
    }

    /**
     * @return the searchCategoryActive
     */
    public boolean isSearchCategoryActive() {
        return searchCategoryActive;
    }

    /**
     * @param searchCategoryActive
     *            the searchCategoryActive to set
     */
    public void setSearchCategoryActive(final boolean searchCategoryActive) {
        this.searchCategoryActive = searchCategoryActive;
    }

    /**
     * @return the createProductActive
     */
    public boolean isCreateProductActive() {
        return createProductActive;
    }

    /**
     * @param createProductActive
     *            the createProductActive to set
     */
    public void setCreateProductActive(final boolean createProductActive) {
        this.createProductActive = createProductActive;
    }

    /**
     * @return the updateProductActive
     */
    public boolean isUpdateProductActive() {
        return updateProductActive;
    }

    /**
     * @param updateProductActive
     *            the updateProductActive to set
     */
    public void setUpdateProductActive(final boolean updateProductActive) {
        this.updateProductActive = updateProductActive;
    }

    /**
     * @return the searchProductActive
     */
    public boolean isSearchProductActive() {
        return searchProductActive;
    }

    /**
     * @param searchProductActive
     *            the searchProductActive to set
     */
    public void setSearchProductActive(final boolean searchProductActive) {
        this.searchProductActive = searchProductActive;
    }

    /**
     * @return the createCategoryResponse
     */
    public FluentResponse getCreateCategoryResponse() {
        return createCategoryResponse;
    }

    /**
     * @param createCategoryResponse
     *            the createCategoryResponse to set
     */
    public void setCreateCategoryResponse(final FluentResponse createCategoryResponse) {
        this.createCategoryResponse = createCategoryResponse;
    }

    /**
     * @return the updateCategoryResponse
     */
    public FluentResponse getUpdateCategoryResponse() {
        return updateCategoryResponse;
    }

    /**
     * @param updateCategoryResponse
     *            the updateCategoryResponse to set
     */
    public void setUpdateCategoryResponse(final FluentResponse updateCategoryResponse) {
        this.updateCategoryResponse = updateCategoryResponse;
    }

    /**
     * @return the searchCategoryResponse
     */
    public CategoryResponse getSearchCategoryResponse() {
        return searchCategoryResponse;
    }

    /**
     * @param searchCategoryResponse
     *            the searchCategoryResponse to set
     */
    public void setSearchCategoryResponse(final CategoryResponse searchCategoryResponse) {
        this.searchCategoryResponse = searchCategoryResponse;
    }

    /**
     * @return the createProductResponse
     */
    public FluentResponse getCreateProductResponse() {
        return createProductResponse;
    }

    /**
     * @param createProductResponse
     *            the createProductResponse to set
     */
    public void setCreateProductResponse(final FluentResponse createProductResponse) {
        this.createProductResponse = createProductResponse;
    }

    /**
     * @return the updateProductResponse
     */
    public FluentResponse getUpdateProductResponse() {
        return updateProductResponse;
    }

    /**
     * @param updateProductResponse
     *            the updateProductResponse to set
     */
    public void setUpdateProductResponse(final FluentResponse updateProductResponse) {
        this.updateProductResponse = updateProductResponse;
    }

    /**
     * @return the searchProductResponse
     */
    public ProductsResponse getSearchProductResponse() {
        return searchProductResponse;
    }

    /**
     * @param searchProductResponse
     *            the searchProductResponse to set
     */
    public void setSearchProductResponse(final ProductsResponse searchProductResponse) {
        this.searchProductResponse = searchProductResponse;
    }

    /**
     * @return the createSkuActive
     */
    public boolean isCreateSkuActive() {
        return createSkuActive;
    }

    /**
     * @param createSkuActive
     *            the createSkuActive to set
     */
    public void setCreateSkuActive(final boolean createSkuActive) {
        this.createSkuActive = createSkuActive;
    }

    /**
     * @return the updateSkuActive
     */
    public boolean isUpdateSkuActive() {
        return updateSkuActive;
    }

    /**
     * @param updateSkuActive
     *            the updateSkuActive to set
     */
    public void setUpdateSkuActive(final boolean updateSkuActive) {
        this.updateSkuActive = updateSkuActive;
    }

    /**
     * @return the searchSkuActive
     */
    public boolean isSearchSkuActive() {
        return searchSkuActive;
    }

    /**
     * @param searchSkuActive
     *            the searchSkuActive to set
     */
    public void setSearchSkuActive(final boolean searchSkuActive) {
        this.searchSkuActive = searchSkuActive;
    }

    /**
     * @return the createSkuResponse
     */
    public FluentResponse getCreateSkuResponse() {
        return createSkuResponse;
    }

    /**
     * @param createSkuResponse
     *            the createSkuResponse to set
     */
    public void setCreateSkuResponse(final FluentResponse createSkuResponse) {
        this.createSkuResponse = createSkuResponse;
    }

    /**
     * @return the updateSkuResponse
     */
    public FluentResponse getUpdateSkuResponse() {
        return updateSkuResponse;
    }

    /**
     * @param updateSkuResponse
     *            the updateSkuResponse to set
     */
    public void setUpdateSkuResponse(final FluentResponse updateSkuResponse) {
        this.updateSkuResponse = updateSkuResponse;
    }

    /**
     * @return the searchSkuResponse
     */
    public SkusResponse getSearchSkuResponse() {
        return searchSkuResponse;
    }

    /**
     * @param searchSkuResponse
     *            the searchSkuResponse to set
     */
    public void setSearchSkuResponse(final SkusResponse searchSkuResponse) {
        this.searchSkuResponse = searchSkuResponse;
    }

    /**
     * @return the batchResponse
     */
    public BatchResponse getBatchResponse() {
        return batchResponse;
    }

    /**
     * @param batchResponse
     *            the batchResponse to set
     */
    public void setBatchResponse(final BatchResponse batchResponse) {
        this.batchResponse = batchResponse;
    }

    /**
     * @return the createFulfilmentActive
     */
    public boolean isCreateFulfilmentActive() {
        return createFulfilmentActive;
    }

    /**
     * @param createFulfilmentActive
     *            the createFulfilmentActive to set
     */
    public void setCreateFulfilmentActive(final boolean createFulfilmentActive) {
        this.createFulfilmentActive = createFulfilmentActive;
    }

    /**
     * @return the retrieveFulfilmentsByOrderActive
     */
    public boolean isRetrieveFulfilmentsByOrderActive() {
        return retrieveFulfilmentsByOrderActive;
    }

    /**
     * @param retrieveFulfilmentsByOrderActive
     *            the retrieveFulfilmentsByOrderActive to set
     */
    public void setRetrieveFulfilmentsByOrderActive(final boolean retrieveFulfilmentsByOrderActive) {
        this.retrieveFulfilmentsByOrderActive = retrieveFulfilmentsByOrderActive;
    }

    /**
     * @return the fulfilmentOptionResponse
     */
    public FulfilmentOptionResponse getFulfilmentOptionResponse() {
        return fulfilmentOptionResponse;
    }

    /**
     * @param fulfilmentOptionResponse
     *            the fulfilmentOptionResponse to set
     */
    public void setFulfilmentOptionResponse(final FulfilmentOptionResponse fulfilmentOptionResponse) {
        this.fulfilmentOptionResponse = fulfilmentOptionResponse;
    }

    /**
     * @return the fulfilmentOptionMap
     */
    public Map<String, Map<String, Integer>> getFulfilmentOptionMap() {
        return fulfilmentOptionMap;
    }

    /**
     * @param fulfilmentOptionMap
     *            the fulfilmentOptionMap to set
     */
    public void setFulfilmentOptionMap(final Map<String, Map<String, Integer>> fulfilmentOptionMap) {
        this.fulfilmentOptionMap = fulfilmentOptionMap;
    }

    /**
     * @return the createOrderActive
     */
    public boolean isCreateOrderActive() {
        return createOrderActive;
    }

    /**
     * @param createOrderActive
     *            the createOrderActive to set
     */
    public void setCreateOrderActive(final boolean createOrderActive) {
        this.createOrderActive = createOrderActive;
    }

    /**
     * @return the createOrderResponse
     */
    public FluentResponse getCreateOrderResponse() {
        return createOrderResponse;
    }

    /**
     * @param createOrderResponse
     *            the createOrderResponse to set
     */
    public void setCreateOrderResponse(final FluentResponse createOrderResponse) {
        this.createOrderResponse = createOrderResponse;
    }

    /**
     * @return the eventSyncActive
     */
    public boolean isEventSyncActive() {
        return eventSyncActive;
    }

    /**
     * @param eventSyncActive
     *            the eventSyncActive to set
     */
    public void setEventSyncActive(final boolean eventSyncActive) {
        this.eventSyncActive = eventSyncActive;
    }

    /**
     * @return the eventResponse
     */
    public EventResponse getEventResponse() {
        return eventResponse;
    }

    /**
     * @param eventResponse
     *            the eventResponse to set
     */
    public void setEventResponse(final EventResponse eventResponse) {
        this.eventResponse = eventResponse;
    }

    /**
     * @return the jobStub
     */
    public Job getJobStub() {
        return jobStub;
    }

    /**
     * @return the batchStub
     */
    public Batch getBatchStub() {
        return batchStub;
    }

    /**
     * @return the categoryStub
     */
    public Category getCategoryStub() {
        return categoryStub;
    }

    /**
     * @return the productStub
     */
    public Product getProductStub() {
        return productStub;
    }

    /**
     * @return the skuStub
     */
    public Sku getSkuStub() {
        return skuStub;
    }

    /**
     * @return the orderStub
     */
    public Order getOrderStub() {
        return orderStub;
    }

    /**
     * @return the eventStub
     */
    public Event getEventStub() {
        return eventStub;
    }

    /**
     * @return the fulfilmentsResponse
     */
    public FulfilmentsResponse getFulfilmentsResponse() {
        return fulfilmentsResponse;
    }

    /**
     * @param fulfilmentsResponse
     *            the fulfilmentsResponse to set
     */
    public void setFulfilmentsResponse(final FulfilmentsResponse fulfilmentsResponse) {
        this.fulfilmentsResponse = fulfilmentsResponse;
    }

    /**
     * @return the getOrderActive
     */
    public boolean isViewOrderActive() {
        return viewOrderActive;
    }

    /**
     * @param getOrderActive
     *            the getOrderActive to set
     */
    public void setViewOrderActive(final boolean getOrderActive) {
        this.viewOrderActive = getOrderActive;
    }

    /**
     * @return the getOrderResponse
     */
    public OrderResponse getViewOrderResponse() {
        return viewOrderResponse;
    }

    /**
     * @param viewOrderResponse
     *            the getOrderResponse to set
     */
    public void setViewOrderResponse(final OrderResponse viewOrderResponse) {
        this.viewOrderResponse = viewOrderResponse;
    }

    public void reset() {
        createJobResponse = null;
        createBatchResponse = null;

        createCategoryResponse = null;
        updateCategoryResponse = null;
        searchCategoryResponse = null;

        createProductResponse = null;
        updateProductResponse = null;
        searchProductResponse = null;

        createSkuResponse = null;
        updateSkuResponse = null;
        searchSkuResponse = null;

        batchResponse = null;

        fulfilmentOptionResponse = null;
        fulfilmentOptionMap = null;

        createOrderResponse = null;
        viewOrderResponse = null;

        eventResponse = null;

        jobStub = null;
        batchStub = null;
        categoryStub = null;
        productStub = null;
        skuStub = null;
        orderStub = null;
        eventStub = null;
        fulfilmentOptionResponse = null;
    }

}

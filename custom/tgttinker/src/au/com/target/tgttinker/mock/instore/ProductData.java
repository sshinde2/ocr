/**
 * 
 */
package au.com.target.tgttinker.mock.instore;

/**
 * Product data object to store dummy product information
 * 
 * @author jjayawa1
 *
 */
public class ProductData {
    private String code;
    private String quantity;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final String quantity) {
        this.quantity = quantity;
    }

}

/**
 * 
 */
package au.com.target.tgttinker.mock.payment;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import au.com.target.tgtpaymentprovider.afterpay.client.exception.TargetAfterpayClientException;
import au.com.target.tgtpaymentprovider.afterpay.client.impl.TargetAfterpayClientImpl;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CapturePaymentRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateOrderRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.request.CreateRefundRequest;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CapturePaymentResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.CreateRefundResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetConfigurationResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetOrderResponse;
import au.com.target.tgtpaymentprovider.afterpay.data.response.GetPaymentResponse;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 * @author bhuang3
 *
 */
public class TargetAfterpayClientMock extends TargetAfterpayClientImpl implements MockedByTarget {

    private boolean active = false;

    private boolean throwExpection = false;

    private boolean capturePaymentException = false;

    private boolean getOrderExpection = false;

    private boolean getOrderActive = false;

    private boolean capturePaymentActive = false;

    private boolean createOrderActive = false;

    private boolean createOrderException = false;

    private boolean pingActive = false;

    private boolean getPaymentActive = false;

    private boolean getPaymentException = false;

    private boolean createRefundActive = false;

    private boolean createRefundException = false;

    private boolean capturePaymentActiveOnRetry = false;

    private boolean getConfigurationException = false;

    private boolean getConfigurationActive = false;

    private GetConfigurationResponse getConfigurationResponse;

    private CreateOrderResponse createOrderResponse;
    private GetOrderResponse getOrderResponse;

    private CapturePaymentResponse capturePaymentResponse;

    private GetPaymentResponse getPaymentResponse;

    private CreateRefundResponse createRefundResponse;

    private CreateRefundRequest createRefundRequestCaptor;

    @Override
    public GetConfigurationResponse getConfiguration() throws TargetAfterpayClientException {
        if (getConfigurationActive) {
            if (getConfigurationException) {
                throw new TargetAfterpayClientException("service unavailable");
            }
            else {
                return getConfigurationResponse;
            }
        }
        else {
            return super.getConfiguration();
        }
    }

    @Override
    public boolean isAfterpayConnectionAvailable() {
        if (pingActive) {
            if (throwExpection) {
                return false;
            }
            else {
                return true;
            }
        }
        return super.isAfterpayConnectionAvailable();
    }

    @Override
    public CreateOrderResponse createOrder(final CreateOrderRequest request) throws TargetAfterpayClientException {
        if (createOrderActive) {
            if (createOrderException) {
                throw new TargetAfterpayClientException("service unavailable");
            }
            else {
                return createOrderResponse;
            }

        }
        return super.createOrder(request);
    }



    @Override
    public CapturePaymentResponse capturePayment(final CapturePaymentRequest request)
            throws TargetAfterpayClientException {
        if (capturePaymentActive) {
            if (capturePaymentException) {
                throw new TargetAfterpayClientException("service unavailable");
            }
            else {
                return capturePaymentResponse;
            }

        }
        return super.capturePayment(request);
    }

    @Override
    public GetOrderResponse getOrder(final String afterpayToken) throws TargetAfterpayClientException {
        if (getOrderActive) {
            if (getOrderExpection) {
                throw new TargetAfterpayClientException("service unavailable");
            }
            else {
                return getOrderResponse;
            }

        }
        return super.getOrder(afterpayToken);
    }

    @Override
    public GetPaymentResponse getPayment(final String afterpayToken) throws TargetAfterpayClientException {
        if (getPaymentActive) {
            if (getPaymentException) {
                if (capturePaymentActiveOnRetry) {
                    setCapturePaymentException(false);
                }
                throw new TargetAfterpayClientException("service unavailable");
            }
            else {
                return getPaymentResponse;
            }

        }
        return super.getPayment(afterpayToken);
    }


    @Override
    public CreateRefundResponse createRefund(final String paymentId, final CreateRefundRequest request)
            throws TargetAfterpayClientException {
        if (createRefundActive) {
            if (createRefundException) {
                throw new TargetAfterpayClientException("service unavailable");
            }
            else {
                createRefundRequestCaptor = request;
                setCreateRefundResponseFromRequest(request);
                return createRefundResponse;
            }

        }
        return super.createRefund(paymentId, request);
    }

    @Override
    public boolean isActive() {
        return active;
    }


    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    public void resetDefault() {
        active = false;
        getOrderActive = false;
        capturePaymentActive = false;
        throwExpection = false;
        capturePaymentException = false;
        getOrderExpection = false;
        createOrderActive = false;
        createOrderException = false;
        pingActive = false;
        getPaymentActive = false;
        getPaymentException = false;
        getConfigurationResponse = null;
        createOrderResponse = null;
        getOrderResponse = null;
        capturePaymentResponse = null;
        createRefundActive = false;
        createRefundException = false;
        createRefundResponse = null;
        capturePaymentActiveOnRetry = false;
        getConfigurationActive = false;
        getConfigurationException = false;
    }

    public void activateMocks() {
        active = true;
        getOrderActive = true;
        capturePaymentActive = true;
        createOrderActive = true;
        pingActive = true;
        getPaymentActive = true;
        createRefundActive = true;
        getConfigurationActive = true;
    }

    private void setCreateRefundResponseFromRequest(final CreateRefundRequest request) {
        if (createRefundResponse == null) {
            createRefundResponse = new CreateRefundResponse();
            createRefundResponse.setAmount(request.getAmount());
            createRefundResponse.setRequestId(request.getRequestId());
            createRefundResponse.setMerchantReference(request.getMerchantReference());
            createRefundResponse.setRefundId("dummyRefundId");
            createRefundResponse.setRefundedAt(new DateTime().toString(DateTimeFormat
                    .forPattern("yyyy-MM-dd'T'HH:mm:ss'Z")));
        }
    }


    /**
     * @return the getConfigurationResponse
     */
    public GetConfigurationResponse getGetConfigurationResponse() {
        return getConfigurationResponse;
    }

    /**
     * @param getConfigurationResponse
     *            the getConfigurationResponse to set
     */
    public void setGetConfigurationResponse(final GetConfigurationResponse getConfigurationResponse) {
        this.getConfigurationResponse = getConfigurationResponse;
    }

    /**
     * @return the createOrderResponse
     */
    public CreateOrderResponse getCreateOrderResponse() {
        return createOrderResponse;
    }

    /**
     * @param createOrderResponse
     *            the createOrderResponse to set
     */
    public void setCreateOrderResponse(final CreateOrderResponse createOrderResponse) {
        this.createOrderResponse = createOrderResponse;
    }

    /**
     * @return the throwExpection
     */
    public boolean isThrowExpection() {
        return throwExpection;
    }


    /**
     * @param throwExpection
     *            the throwExpection to set
     */
    public void setThrowExpection(final boolean throwExpection) {
        this.throwExpection = throwExpection;
    }

    /**
     * @return the capturePaymentResponse
     */
    public CapturePaymentResponse getCapturePaymentResponse() {
        return capturePaymentResponse;
    }

    /**
     * @param capturePaymentResponse
     *            the capturePaymentResponse to set
     */
    public void setCapturePaymentResponse(final CapturePaymentResponse capturePaymentResponse) {
        this.capturePaymentResponse = capturePaymentResponse;
    }

    /**
     * @return the getOrderResponse
     */
    public GetOrderResponse getGetOrderResponse() {
        return getOrderResponse;
    }

    /**
     * @param getOrderResponse
     *            the getOrderResponse to set
     */
    public void setGetOrderResponse(final GetOrderResponse getOrderResponse) {
        this.getOrderResponse = getOrderResponse;
    }

    /**
     * @return the capturePaymentExpection
     */
    public boolean isCapturePaymentException() {
        return capturePaymentException;
    }

    /**
     * @param capturePaymentException
     *            the capturePaymentExpection to set
     */
    public void setCapturePaymentException(final boolean capturePaymentException) {
        this.capturePaymentException = capturePaymentException;
    }

    /**
     * @return the getOrderExpection
     */
    public boolean isGetOrderExpection() {
        return getOrderExpection;
    }

    /**
     * @param getOrderExpection
     *            the getOrderExpection to set
     */
    public void setGetOrderExpection(final boolean getOrderExpection) {
        this.getOrderExpection = getOrderExpection;
    }

    /**
     * @return the getOrderActive
     */
    public boolean isGetOrderActive() {
        return getOrderActive;
    }

    /**
     * @return the capturePaymentActive
     */
    public boolean isCapturePaymentActive() {
        return capturePaymentActive;
    }

    /**
     * @param capturePaymentActive
     *            the capturePaymentActive to set
     */
    public void setCapturePaymentActive(final boolean capturePaymentActive) {
        this.capturePaymentActive = capturePaymentActive;
    }

    /**
     * @return the createOrderActive
     */
    public boolean isCreateOrderActive() {
        return createOrderActive;
    }

    /**
     * @param createOrderActive
     *            the createOrderActive to set
     */
    public void setCreateOrderActive(final boolean createOrderActive) {
        this.createOrderActive = createOrderActive;
    }

    /**
     * @param getOrderActive
     *            the getOrderActive to set
     */
    public void setGetOrderActive(final boolean getOrderActive) {
        this.getOrderActive = getOrderActive;
    }

    /**
     * @return the createOrderException
     */
    public boolean isCreateOrderException() {
        return createOrderException;
    }

    /**
     * @param createOrderException
     *            the createOrderException to set
     */
    public void setCreateOrderException(final boolean createOrderException) {
        this.createOrderException = createOrderException;
    }

    /**
     * @return the pingActive
     */
    public boolean isPingActive() {
        return pingActive;
    }

    /**
     * @param pingActive
     *            the pingActive to set
     */
    public void setPingActive(final boolean pingActive) {
        this.pingActive = pingActive;
    }

    /**
     * @return the getPaymentActive
     */
    public boolean isGetPaymentActive() {
        return getPaymentActive;
    }

    /**
     * @param getPaymentActive
     *            the getPaymentActive to set
     */
    public void setGetPaymentActive(final boolean getPaymentActive) {
        this.getPaymentActive = getPaymentActive;
    }

    /**
     * @return the getPaymentException
     */
    public boolean isGetPaymentException() {
        return getPaymentException;
    }

    /**
     * @param getPaymentException
     *            the getPaymentException to set
     */
    public void setGetPaymentException(final boolean getPaymentException) {
        this.getPaymentException = getPaymentException;
    }

    /**
     * @return the getPaymentResponse
     */
    public GetPaymentResponse getGetPaymentResponse() {
        return getPaymentResponse;
    }

    /**
     * @param getPaymentResponse
     *            the getPaymentResponse to set
     */
    public void setGetPaymentResponse(final GetPaymentResponse getPaymentResponse) {
        this.getPaymentResponse = getPaymentResponse;
    }

    /**
     * @return the createRefundActive
     */
    public boolean isCreateRefundActive() {
        return createRefundActive;
    }

    /**
     * @param createRefundActive
     *            the createRefundActive to set
     */
    public void setCreateRefundActive(final boolean createRefundActive) {
        this.createRefundActive = createRefundActive;
    }

    /**
     * @return the createRefundException
     */
    public boolean isCreateRefundException() {
        return createRefundException;
    }

    /**
     * @param createRefundException
     *            the createRefundException to set
     */
    public void setCreateRefundException(final boolean createRefundException) {
        this.createRefundException = createRefundException;
    }

    /**
     * @param capturePaymentActiveOnRetry
     *            the capturePaymentActiveOnRetry to set
     */
    public void setCapturePaymentActiveOnRetry(final boolean capturePaymentActiveOnRetry) {
        this.capturePaymentActiveOnRetry = capturePaymentActiveOnRetry;
    }

    /**
     * @return the createRefundResponse
     */
    public CreateRefundResponse getCreateRefundResponse() {
        return createRefundResponse;
    }

    /**
     * @param createRefundResponse
     *            the createRefundResponse to set
     */
    public void setCreateRefundResponse(final CreateRefundResponse createRefundResponse) {
        this.createRefundResponse = createRefundResponse;
    }

    /**
     * @return the createRefundRequestCaptor
     */
    public CreateRefundRequest getCreateRefundRequestCaptor() {
        return createRefundRequestCaptor;
    }

    /**
     * @param createRefundRequestCaptor
     *            the createRefundRequestCaptor to set
     */
    public void setCreateRefundRequestCaptor(final CreateRefundRequest createRefundRequestCaptor) {
        this.createRefundRequestCaptor = createRefundRequestCaptor;
    }

    /**
     * @return the getConfigurationException
     */
    public boolean isGetConfigurationException() {
        return getConfigurationException;
    }

    /**
     * @param getConfigurationException
     *            the getConfigurationException to set
     */
    public void setGetConfigurationException(final boolean getConfigurationException) {
        this.getConfigurationException = getConfigurationException;
    }

    /**
     * @return the getConfigurationActive
     */
    public boolean isGetConfigurationActive() {
        return getConfigurationActive;
    }

    /**
     * @param getConfigurationActive
     *            the getConfigurationActive to set
     */
    public void setGetConfigurationActive(final boolean getConfigurationActive) {
        this.getConfigurationActive = getConfigurationActive;
    }

}

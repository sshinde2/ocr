package au.com.target.tgttinker.mock.payment;

import de.hybris.platform.payment.commands.result.AbstractResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtpayment.commands.request.AbstractTargetStandaloneRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetCaptureRequest;
import au.com.target.tgtpayment.commands.request.TargetExcessiveRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetFollowOnRefundRequest;
import au.com.target.tgtpayment.commands.request.TargetRetrieveTransactionRequest;
import au.com.target.tgtpayment.commands.request.TargetTokenizeRequest;
import au.com.target.tgtpayment.commands.result.TargetCaptureResult;
import au.com.target.tgtpayment.commands.result.TargetExcessiveRefundResult;
import au.com.target.tgtpayment.commands.result.TargetFollowOnRefundResult;
import au.com.target.tgtpayment.commands.result.TargetRetrieveTransactionResult;
import au.com.target.tgtpayment.commands.result.TargetStandaloneRefundResult;
import au.com.target.tgtpayment.commands.result.TargetTokenizeResult;
import au.com.target.tgtpayment.methods.impl.TargetCreditCardPaymentMethodImpl;
import au.com.target.tgttinker.mock.MockedByTarget;


/**
 */
public class TargetCreditCardPaymentMethodMock extends TargetCreditCardPaymentMethodImpl implements MockedByTarget {
    private static final Logger LOG = Logger.getLogger(TargetCreditCardPaymentMethodMock.class);

    private static final String MOCK_APPROVED = "APPROVED";
    private static final String MOCK_DECLINED = "DECLINED";
    private static final String MOCK_PENDING = "PENDING";
    private static final String MOCK_TIMEOUT = "TIMEOUT";
    private static final String MOCK_SLEEP = "SLEEP_2_MIN";
    //CHECKSTYLE:OFF need the public to be after the private, because it references the private details
    public static final String MOCK_DEFAULT = MOCK_APPROVED;
    public static final List<String> VALID_MOCK_RESPONSE = Collections
            .unmodifiableList(Arrays.asList(MOCK_APPROVED, MOCK_DECLINED, MOCK_PENDING, MOCK_TIMEOUT, MOCK_SLEEP));
    //CHECKSTYLE:ON

    private FlexibleSearchService flexibleSearchService;

    private boolean active = Config.getBoolean("mock.tns.default.active", false);
    private String mockResponse = MOCK_DEFAULT;
    private int responseLeft = 0;
    private String nextMockResponse = MOCK_DEFAULT;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    /**
     * Get the current mock response
     * 
     * @return mock response
     */
    public String getMockResponse() {
        return mockResponse;
    }

    /**
     * @return the responseLeft
     */
    public int getResponseLeft() {
        return responseLeft;
    }

    /**
     * @return the nextMockResponse
     */
    public String getNextMockResponse() {
        return nextMockResponse;
    }

    /**
     * Set the response from future mocking requests
     * 
     * @param mockResponseStr
     *            response to give
     * @param times
     *            number of times to return that response before returning to the default (0 will return this result
     *            until explicitly changed)
     * @param nextMockResponseStr
     *            response to return to after <code>times</code>, means nothing if <code>times</code> is 0
     */
    public void setMockResponse(final String mockResponseStr, final int times, final String nextMockResponseStr) {
        for (final String validResponse : VALID_MOCK_RESPONSE) {
            if (validResponse.equalsIgnoreCase(mockResponseStr)) {
                this.mockResponse = validResponse;
                this.responseLeft = times;
                this.nextMockResponse = nextMockResponseStr;
                return;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TargetCaptureResult capture(final TargetCaptureRequest captureRequest) {
        if (!active) {
            return super.capture(captureRequest);
        }

        Assert.notNull(captureRequest);
        LOG.info("Mocking Credit Card capture");

        final TargetCaptureResult result = new TargetCaptureResult();
        populateResult(result);

        result.setRequestToken(captureRequest.getToken());
        result.setReconciliationId(captureRequest.getTransactionId());
        result.setMerchantTransactionCode(captureRequest.getTransactionId());
        result.setCurrency(captureRequest.getCurrency());
        result.setTotalAmount(captureRequest.getTotalAmount());
        result.setRequestTime(new Date());
        result.setPaymentProvider(getPaymentProvider());
        result.setRequestId(captureRequest.getTransactionId());

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TargetStandaloneRefundResult standaloneRefund(
            final AbstractTargetStandaloneRefundRequest standaloneRefundRequest) {
        if (!active) {
            return super.standaloneRefund(standaloneRefundRequest);
        }

        Assert.notNull(standaloneRefundRequest);
        LOG.info("Mocking Credit Card standaloneRefund");

        final TargetStandaloneRefundResult result = new TargetStandaloneRefundResult();
        populateResult(result);

        result.setRequestToken("dodgy-token");
        result.setReconciliationId(standaloneRefundRequest.getTransactionId());
        result.setMerchantTransactionCode(standaloneRefundRequest.getTransactionId());
        result.setCurrency(standaloneRefundRequest.getCurrency());
        result.setTotalAmount(standaloneRefundRequest.getTotalAmount());
        result.setRequestTime(new Date());
        result.setPaymentProvider(getPaymentProvider());

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TargetFollowOnRefundResult followOnRefund(final TargetFollowOnRefundRequest followOnRefundRequest) {
        if (!active) {
            return super.followOnRefund(followOnRefundRequest);
        }

        Assert.notNull(followOnRefundRequest);
        LOG.info("Mocking Credit Card followOnRefund");

        final TargetFollowOnRefundResult result = new TargetFollowOnRefundResult();
        populateResult(result);

        result.setRequestToken(followOnRefundRequest.getToken());
        result.setReconciliationId(followOnRefundRequest.getTransactionId());
        result.setMerchantTransactionCode(followOnRefundRequest.getTransactionId());
        result.setCurrency(followOnRefundRequest.getCurrency());
        result.setTotalAmount(followOnRefundRequest.getTotalAmount());
        result.setRequestTime(new Date());
        result.setPaymentProvider(getPaymentProvider());

        return result;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public TargetRetrieveTransactionResult retrieveTransaction(
            final TargetRetrieveTransactionRequest retrieveTransactionRequest) {
        if (!active) {
            return super.retrieveTransaction(retrieveTransactionRequest);
        }

        Assert.notNull(retrieveTransactionRequest);
        LOG.info("Mocking Credit Card retrieveTransaction");

        final TargetRetrieveTransactionResult result = new TargetRetrieveTransactionResult();
        populateResult(result);

        result.setRequestToken(retrieveTransactionRequest.getOrderId());
        result.setReconciliationId(retrieveTransactionRequest.getTransactionId());
        result.setMerchantTransactionCode(retrieveTransactionRequest.getTransactionId());
        result.setTotalAmount(getAmtFromPTECode(retrieveTransactionRequest.getTransactionId()));

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TargetExcessiveRefundResult excessiveRefund(final TargetExcessiveRefundRequest retrieveTransactionRequest) {
        if (!active) {
            return super.excessiveRefund(retrieveTransactionRequest);
        }

        Assert.notNull(retrieveTransactionRequest);
        LOG.info("Mocking Credit Card excessiveRefund");

        final TargetExcessiveRefundResult result = new TargetExcessiveRefundResult();
        populateResult(result);

        result.setRequestToken("dodgy-token");
        result.setReconciliationId(retrieveTransactionRequest.getTransactionId());
        result.setMerchantTransactionCode(retrieveTransactionRequest.getTransactionId());
        result.setCurrency(retrieveTransactionRequest.getCurrency());
        result.setTotalAmount(retrieveTransactionRequest.getTotalAmount());
        result.setRequestTime(new Date());

        return result;
    }

    private void populateResult(final AbstractResult result) {
        result.setReconciliationId("mock123");
        result.setRequestId("mock" + System.currentTimeMillis());

        if (MOCK_DECLINED.equals(mockResponse)) {
            LOG.info("TNS mock respose with decline");
            result.setTransactionStatus(TransactionStatus.REJECTED);
            result.setTransactionStatusDetails(TransactionStatusDetails.BANK_DECLINE);
        }
        else if (MOCK_PENDING.equals(mockResponse)) {
            LOG.info("TNS mock respose with pending");
            result.setTransactionStatus(TransactionStatus.REVIEW);
            result.setTransactionStatusDetails(TransactionStatusDetails.REVIEW_NEEDED);
        }
        else if (MOCK_TIMEOUT.equals(mockResponse)) {
            LOG.info("TNS mock respose with a timeout");
            result.setTransactionStatus(TransactionStatus.REJECTED);
            result.setTransactionStatusDetails(TransactionStatusDetails.TIMEOUT);
        }
        else {
            if (MOCK_SLEEP.equals(mockResponse)) {
                LOG.info("TNS mock sleeping for 2 minutes");
                try {
                    Thread.sleep(2L * 60L * 1000L);
                }
                catch (final InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
            LOG.info("TNS mock respose with approved");
            result.setTransactionStatus(TransactionStatus.ACCEPTED);
            result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
        }

        if (responseLeft > 1) {
            responseLeft--;
        }
        else if (responseLeft == 1) {
            responseLeft = 0;
            mockResponse = nextMockResponse;
        }
    }

    private BigDecimal getAmtFromPTECode(final String code) {
        final PaymentTransactionEntryModel example = new PaymentTransactionEntryModel();
        example.setCode(code);
        final List<PaymentTransactionEntryModel> ptems = flexibleSearchService.getModelsByExample(example);
        if (CollectionUtils.isNotEmpty(ptems)) {
            return ptems.get(0).getAmount();
        }
        else {
            LOG.warn("Could not find the actual amount of the order, so mocking at $1.00");
            return BigDecimal.valueOf(1.00d);
        }
    }


    @Override
    public TargetTokenizeResult tokenize(final TargetTokenizeRequest tokenizeRequest) {
        if (!active) {
            return super.tokenize(tokenizeRequest);
        }

        final TargetTokenizeResult result = new TargetTokenizeResult();
        result.setSavedToken("SavedToken");
        return result;
    }


    /**
     * @param flexibleSearchService
     *            the flexibleSearchService to set
     */
    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

}

/**
 * 
 */
package au.com.target.tgttinker.mock.sendSms;

import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;
import au.com.target.tgtwebmethods.exacttarget.client.impl.TargetSendSmsCncNotificationClientImpl;


/**
 * @author mjanarth
 * 
 */
public class TargetSendSmsClientMock extends TargetSendSmsCncNotificationClientImpl implements
        MockedByTarget {


    public static final String MOCK_INVALID = TargetSendSmsResponseType.INVALID.toString();
    public static final String MOCK_OTHERS = TargetSendSmsResponseType.OTHERS.toString();
    public static final String MOCK_SUCCESS = TargetSendSmsResponseType.SUCCESS.toString();
    public static final String MOCK_UNAVAILABLE = TargetSendSmsResponseType.UNAVAILABLE.toString();
    public static final List<String> MOCK_RESPONSE = Collections
            .unmodifiableList(Arrays.asList(MOCK_INVALID, MOCK_OTHERS, MOCK_UNAVAILABLE, MOCK_SUCCESS));

    private boolean active = Config.getBoolean("mock.sendSms.default.active", false);
    private String mockResponse = TargetSendSmsResponseType.SUCCESS.toString();



    /* (non-Javadoc)
     * @see au.com.target.tgtcore.exacttarget.client.TargetSendSmsCncNotificationClient#sendSmsNotification(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public TargetSendSmsResponseDto sendSmsNotification(final String mobileNumber, final String message,
            final String timeZone) {


        if (!active) {
            return super.sendSmsNotification(mobileNumber, message, timeZone);
        }
        else {
            final TargetSendSmsResponseDto response = new TargetSendSmsResponseDto();
            if (MOCK_INVALID.equals(this.mockResponse)) {
                response.setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
                response.setResponseMessage("The request has invalid data");
                response.setResponseType(TargetSendSmsResponseType.INVALID);
            }
            if (MOCK_UNAVAILABLE.equals(this.mockResponse)) {
                response
                        .setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
                response.setResponseMessage("Timed out exception");
                response.setResponseType(TargetSendSmsResponseType.UNAVAILABLE);
            }
            if (MOCK_SUCCESS.equals(this.mockResponse)) {
                response
                        .setResponseCode(Integer.valueOf(0));
                response.setResponseMessage("Succcessfully sent Sms");
                response.setResponseType(TargetSendSmsResponseType.SUCCESS);
            }
            if (MOCK_OTHERS.equals(this.mockResponse)) {
                response
                        .setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.OTHERS_ERRORCODE);
                response.setResponseMessage("Others");
                response.setResponseType(TargetSendSmsResponseType.OTHERS);
            }
            return response;
        }

    }

    /**
     * Get the current mock response
     * 
     * @return mock response
     */
    public String getMockResponse() {
        return mockResponse;
    }

    /**
     * Set the response from future mocking requests
     * 
     * @param mockResponseStr
     *            response to give
     */
    public void setMockResponse(final String mockResponseStr) {
        for (final String validResponse : MOCK_RESPONSE) {
            if (validResponse.equalsIgnoreCase(mockResponseStr)) {
                this.mockResponse = validResponse;
                return;
            }
        }
    }

    @Override
    public boolean isActive() {
        return active;
    }


    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }



}

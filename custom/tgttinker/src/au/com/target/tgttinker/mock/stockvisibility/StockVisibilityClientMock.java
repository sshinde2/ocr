/**
 * 
 */
package au.com.target.tgttinker.mock.stockvisibility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgttinker.mock.MockedByTarget;
import au.com.target.tgtwebmethods.stockvisibility.client.impl.StockVisibilityClientImpl;
import au.com.target.tgtwebmethods.util.StockVisibilityUtil;
import au.com.target.tgtwebmethods.util.WMUtil;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;


/**
 * @author rmcalave
 *
 */
public class StockVisibilityClientMock extends StockVisibilityClientImpl implements MockedByTarget {
    private static final Logger LOG = Logger.getLogger(StockVisibilityClientMock.class);
    private static final String LOGGING_PREFIX = "Store StockVis Request:";
    private static final String LOGGING_RESP = "Store StockVis Response:";

    private boolean active;

    private String responseCode = "0";

    private String exception;

    private List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList;

    public StockVisibilityClientMock() {
        stockVisibilityItemResponseDtoList = new ArrayList<>();
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#isActive()
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttinker.mock.MockedByTarget#setActive(boolean)
     */
    @Override
    public void setActive(final boolean active) {
        this.active = active;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.stockvisibility.client.impl.DefaultStockVisibilityClientImpl#lookupStockForItemsInStores(java.util.List, java.util.List)
     */
    @Override
    public StockVisibilityItemLookupResponseDto lookupStockForItemsInStores(final List<String> storeNumbers,
            final List<String> itemcodes) {
        if (active) {
            LOG.info(LOGGING_PREFIX + " Sending request="
                    + WMUtil.convertToJSONString(StockVisibilityUtil.populateStockVisibilityRequest(storeNumbers,
                            itemcodes)));
            if (StringUtils.isNotEmpty(exception)) {
                LOG.error(LOGGING_PREFIX + "FAILED Unable to get response from mock web service: " + exception);
                return null;
            }

            final StockVisibilityItemLookupResponseDto response = new StockVisibilityItemLookupResponseDto();
            response.setResponseCode(responseCode);
            if ("0".equals(responseCode)) {
                final Collection<StockVisibilityItemResponseDto> storeProductSohDataSetForThisRequest = Collections2
                        .filter(stockVisibilityItemResponseDtoList, new Predicate<StockVisibilityItemResponseDto>() {

                            @Override
                            public boolean apply(final StockVisibilityItemResponseDto arg0) {
                                return storeNumbers.contains(arg0.getStoreNumber())
                                        && itemcodes.contains(arg0.getCode());
                            }

                        });
                response.setItems(new ArrayList(storeProductSohDataSetForThisRequest));
            }
            LOG.info(LOGGING_RESP + " Receiving response=" + WMUtil.convertToJSONString(response));
            return response;
        }
        return super.lookupStockForItemsInStores(storeNumbers, itemcodes);
    }

    /**
     * @return the stockVisibilityItemResponseDtoList
     */
    public List<StockVisibilityItemResponseDto> getStockVisibilityItemResponseDtoList() {
        return stockVisibilityItemResponseDtoList;
    }

    /**
     * @param stockVisibilityItemResponseDtoList
     *            the stockVisibilityItemResponseDtoList to set
     */
    public void setStockVisibilityItemResponseDtoList(
            final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDtoList) {
        this.stockVisibilityItemResponseDtoList = stockVisibilityItemResponseDtoList;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the exception
     */
    public String getException() {
        return exception;
    }

    /**
     * @param exception
     *            the exception to set
     */
    public void setException(final String exception) {
        this.exception = exception;
    }

}

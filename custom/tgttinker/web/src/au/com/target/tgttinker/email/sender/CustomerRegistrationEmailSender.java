/**
 * 
 */
package au.com.target.tgttinker.email.sender;

import de.hybris.platform.commerceservices.model.process.StoreFrontCustomerProcessModel;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgttinker.web.forms.EmailHarnessForm;


/**
 * @author rmcalave
 * 
 */
public class CustomerRegistrationEmailSender extends AbstractEmailSender
{
    private static final Logger LOG = Logger.getLogger(CustomerRegistrationEmailSender.class);

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgttinker.email.sender.EmailSender#send(au.com.target.tgttinker.web.forms.EmailHarnessForm)
     */
    @Override
    public boolean send(final EmailHarnessForm emailHarnessForm)
    {
        final TargetCustomerModel targetCustomerModel = createCustomerModel(emailHarnessForm);
        targetCustomerModel.setFirstname("Firstname");
        targetCustomerModel.setLastname("Lastname");

        final StoreFrontCustomerProcessModel processModel = new StoreFrontCustomerProcessModel();

        processModel.setCustomer(targetCustomerModel);
        processModel.setSite(getCmsSite());

        try
        {
            getHybrisEmailStrategy().sendEmail(processModel, "CustomerRegistrationEmailTemplate");
        }
        catch (final BusinessProcessEmailException e)
        {
            LOG.error("Problem With BusinessProcessEmail ", e);
            return false;
        }

        return true;
    }

}

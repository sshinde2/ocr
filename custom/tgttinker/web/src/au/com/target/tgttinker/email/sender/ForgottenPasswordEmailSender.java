/**
 * 
 */
package au.com.target.tgttinker.email.sender;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.model.process.ForgottenPasswordProcessModel;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgttinker.web.forms.EmailHarnessForm;


/**
 * @author rmcalave
 * 
 */
public class ForgottenPasswordEmailSender extends AbstractEmailSender
{
    private static final Logger LOG = Logger.getLogger(ForgottenPasswordEmailSender.class);

    private UserService userService;
    private CustomerAccountService customerAccountService;

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgttinker.email.sender.EmailSender#send(au.com.target.tgttinker.web.forms.EmailHarnessForm)
     */
    @Override
    public boolean send(final EmailHarnessForm emailHarnessForm)
    {
        TargetCustomerModel updatedCustomerModel = null;

        if (StringUtils.isNotBlank(emailHarnessForm.getForgottenPasswordCustomerEmail()))
        {
            final TargetCustomerModel customerModel = userService.getUserForUID(
                    emailHarnessForm.getForgottenPasswordCustomerEmail(), TargetCustomerModel.class);
            customerAccountService.forgottenPassword(customerModel);

            updatedCustomerModel = userService.getUserForUID(emailHarnessForm.getForgottenPasswordCustomerEmail(),
                    TargetCustomerModel.class);
        }
        else
        {
            updatedCustomerModel = createCustomerModel(emailHarnessForm);
            updatedCustomerModel.setFirstname("Firstname");
            updatedCustomerModel.setLastname("Lastname");
            updatedCustomerModel.setToken("DummyToken");
        }

        final ForgottenPasswordProcessModel forgottenPasswordProcessModel = new ForgottenPasswordProcessModel();
        forgottenPasswordProcessModel.setCustomer(updatedCustomerModel);
        forgottenPasswordProcessModel.setSite(getCmsSite());
        forgottenPasswordProcessModel.setToken(updatedCustomerModel.getToken());


        try
        {
            getHybrisEmailStrategy().sendEmail(forgottenPasswordProcessModel, "ForgottenPasswordEmailTemplate");
        }
        catch (final BusinessProcessEmailException e)
        {
            LOG.error("Problem With BusinessProcessEmail ", e);
            return false;
        }

        return true;
    }

    /**
     * @return the userService
     */
    protected UserService getUserService()
    {
        return userService;
    }

    /**
     * @param userService
     *            the userService to set
     */
    @Required
    public void setUserService(final UserService userService)
    {
        this.userService = userService;
    }

    /**
     * @return the customerAccountService
     */
    protected CustomerAccountService getCustomerAccountService()
    {
        return customerAccountService;
    }

    /**
     * @param customerAccountService
     *            the customerAccountService to set
     */
    @Required
    public void setCustomerAccountService(final CustomerAccountService customerAccountService)
    {
        this.customerAccountService = customerAccountService;
    }
}

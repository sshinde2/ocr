/**
 * 
 */
package au.com.target.tgttinker.email.sender;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgttinker.web.forms.EmailHarnessForm;


/**
 * @author rmcalave
 * 
 */
public class OrderFraudReviewEmailSender extends AbstractEmailSender
{

    private static final Logger LOG = Logger.getLogger(OrderFraudReviewEmailSender.class);

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgttinker.email.sender.EmailSender#send(au.com.target.tgttinker.web.forms.EmailHarnessForm)
     */
    @Override
    public boolean send(final EmailHarnessForm emailHarnessForm)
    {
        final OrderModel order = getOrderService().findOrderModelForOrderId(emailHarnessForm.getOrderNumber());

        final OrderProcessModel processModel = new OrderProcessModel();
        processModel.setOrder(order);

        try
        {
            getHybrisEmailStrategy().sendEmail(processModel, "OrderFraudReviewEmailTemplate");
        }
        catch (final BusinessProcessEmailException e)
        {
            LOG.error("Problem With BusinessProcessEmail ", e);
            return false;
        }

        return true;
    }

}

/**
 * 
 */
package au.com.target.tgttinker.email.sender;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.processengine.model.BusinessProcessParameterModel;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtbusproc.exceptions.BusinessProcessEmailException;
import au.com.target.tgttinker.web.forms.EmailHarnessForm;


/**
 * @author rmcalave
 * 
 */
public class OrderShippingNoticeEmailSender extends AbstractEmailSender
{

    private static final Logger LOG = Logger.getLogger(OrderShippingNoticeEmailSender.class);

    /*
     * (non-Javadoc)
     * 
     * @see au.com.target.tgttinker.email.sender.EmailSender#send(au.com.target.tgttinker.web.forms.EmailHarnessForm)
     */
    @Override
    public boolean send(final EmailHarnessForm emailHarnessForm)
    {
        final OrderModel order = getOrderService().findOrderModelForOrderId(emailHarnessForm.getOrderNumber());

        if (order == null)
        {
            LOG.error("No order found for order code: " + emailHarnessForm.getOrderNumber());
            return false;
        }

        final Set<ConsignmentModel> consignments = order.getConsignments();
        if (CollectionUtils.isEmpty(consignments))
        {
            LOG.error("Order " + emailHarnessForm.getOrderNumber() + " does not contain any consignments");
            return false;
        }

        final OrderProcessModel processModel = new OrderProcessModel();
        processModel.setOrder(order);

        final BusinessProcessParameterModel parameter = new BusinessProcessParameterModel();
        parameter.setName("CONSIGNMENT");
        parameter.setValue(consignments.iterator().next());

        processModel.setContextParameters(Collections.singletonList(parameter));

        try
        {
            getHybrisEmailStrategy().sendEmail(processModel, "OrderShippingNoticeEmailTemplate");
        }
        catch (final BusinessProcessEmailException e)
        {
            LOG.error("Problem With BusinessProcessEmail ", e);
            return false;
        }

        return true;
    }

}

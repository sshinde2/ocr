/**
 * 
 */
package au.com.target.tgttinker.sms.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtbusproc.constants.TgtbusprocConstants;
import au.com.target.tgtbusproc.exceptions.BusinessProcessSmsException;
import au.com.target.tgtbusproc.util.OrderProcessParameterHelper;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.sms.TargetSendSmsService;
import au.com.target.tgtfacades.process.email.data.ClickAndCollectEmailData;
import au.com.target.tgttinker.sms.TargetSmsSendTestService;
import au.com.target.tgttinker.web.forms.SmsHarnessForm;


/**
 * @author bhuang3
 * 
 */
public class TargetSmsSendTestServiceImpl implements TargetSmsSendTestService {

    private static final Logger LOG = Logger.getLogger(TargetSmsSendTestServiceImpl.class);

    private TargetOrderService orderService;

    private TargetSendSmsService targetSendSmsService;

    private OrderProcessParameterHelper orderProcessParameterHelper;

    private TargetBusinessProcessService targetBusinessProcessService;

    private ModelService modelService;

    @Override
    public String generateSmsMessage(final SmsHarnessForm smsHarnessForm) {
        final OrderModel orderModel = orderService.findOrderModelForOrderId(smsHarnessForm.getOrderNumber());
        if (orderModel == null)
        {
            LOG.error("No order found for order code: " + smsHarnessForm.getOrderNumber());
            return null;
        }
        final String processDefinitionName = TgtbusprocConstants.BusinessProcess.SEND_CLICK_AND_COLLECT_NOTIFICATION_PROCESS;
        final String processCode = processDefinitionName + "-" + orderModel.getCode()
                + "-" + System.currentTimeMillis();
        final OrderProcessModel processModel = targetBusinessProcessService.createProcess(processCode,
                processDefinitionName);

        processModel.setOrder(orderModel);
        modelService.save(processModel);

        final ClickAndCollectEmailData clickAndCollectEmailData = new ClickAndCollectEmailData();
        clickAndCollectEmailData.setStoreNumber(smsHarnessForm.getStoreNumber());
        clickAndCollectEmailData.setLetterType(smsHarnessForm.getLetterType());

        orderProcessParameterHelper.setCncNotificationData(processModel, clickAndCollectEmailData);

        try {
            return targetSendSmsService.generateSmsMessageFromVelocityTemplate(processModel,
                    smsHarnessForm.getSmsList());
        }
        catch (final BusinessProcessSmsException e) {
            LOG.error("exception while generate the sms", e);
            return null;
        }

    }


    /**
     * @param orderService
     *            the orderService to set
     */
    @Required
    public void setOrderService(final TargetOrderService orderService)
    {
        this.orderService = orderService;
    }


    /**
     * @param targetSendSmsService
     *            the targetSendSmsService to set
     */
    @Required
    public void setTargetSendSmsService(final TargetSendSmsService targetSendSmsService) {
        this.targetSendSmsService = targetSendSmsService;
    }


    /**
     * @param orderProcessParameterHelper
     *            the orderProcessParameterHelper to set
     */
    @Required
    public void setOrderProcessParameterHelper(final OrderProcessParameterHelper orderProcessParameterHelper) {
        this.orderProcessParameterHelper = orderProcessParameterHelper;
    }


    /**
     * @param targetBusinessProcessService
     *            the targetBusinessProcessService to set
     */
    @Required
    public void setTargetBusinessProcessService(final TargetBusinessProcessService targetBusinessProcessService) {
        this.targetBusinessProcessService = targetBusinessProcessService;
    }


    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }



}

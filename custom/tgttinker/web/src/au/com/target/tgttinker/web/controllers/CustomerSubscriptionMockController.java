/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgtmail.enums.TargetCustomerSubscriptionResponseType;
import au.com.target.tgttinker.mock.facades.CustomerSubscriptionMockFacade;


/**
 * @author rmcalave
 *
 */
@Controller
public class CustomerSubscriptionMockController {

    private static final Logger LOG = Logger.getLogger(CustomerSubscriptionMockController.class);

    private static final String CUST_SUB_ACTION = "customerSubscription.action";
    private static final String SUMMARY_VIEW = "/pages/customerSubscriptionSummary";

    @Autowired
    private CustomerSubscriptionMockFacade customerSubscriptionMockFacade;

    @RequestMapping(value = CUST_SUB_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model)
    {
        model.addAttribute("customerSubscription", customerSubscriptionMockFacade.getSummary());

        model.addAttribute("responseTypes", TargetCustomerSubscriptionResponseType.values());

        return SUMMARY_VIEW;
    }

    @RequestMapping(value = CUST_SUB_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            @RequestParam(value = "enable", required = false) final String enable, final ModelMap model)
    {
        if (enable != null) {
            customerSubscriptionMockFacade.setActive(BooleanUtils.toBoolean(enable));
        }

        final String newResponseCode = request.getParameter("responseCode");
        final String newResponseMessage = request.getParameter("responseMessage");
        final String newResponseType = request.getParameter("responseType");

        if (newResponseCode != null && newResponseMessage != null && newResponseType != null) {
            try {
                customerSubscriptionMockFacade.setMockValues(Integer.valueOf(newResponseCode), newResponseMessage,
                        newResponseType);
            }
            catch (final NumberFormatException ex) {
                LOG.warn("Invalid value for newResponseCode: " + newResponseCode + ", expected an Integer.");
            }
        }

        return getMockSummary(model);
    }
}

/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import au.com.target.tgttinker.email.sender.EmailSender;
import au.com.target.tgttinker.web.forms.EmailHarnessForm;



/**
 * @author asingh78
 * 
 */
@Controller
@Scope("request")
@Lazy(true)
@RequestMapping(value = "/emailHarness")
public class EmailTestHarnessController {
    private static final Logger LOG = Logger.getLogger(EmailTestHarnessController.class);

    @Resource(name = "emailSenders")
    private Map<String, EmailSender> emailSenders;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(final Model model) {
        final Map<String, String> emails = new LinkedHashMap<String, String>();
        emails.put("forgottenPassword", "Forget Password");
        emails.put("customerRegistration", "Customer Registration");
        emails.put("changePasswordConfirmation", "Change Password Confirmation");
        emails.put("changeEmailConfirmation", "Change Email Confirmation");
        emails.put("taxInvoice", "Tax Invoice");
        emails.put("orderRejected", "Order Rejected");
        emails.put("orderCancelledCustomerRequest", "Order Cancelled - Customer Request");
        emails.put("orderCancelledShortStock", "Order Cancelled - Short Stock");
        emails.put("orderConfirmation", "Order Confirmation");
        emails.put("laybyPaymentSuccess", "Lay-by Payment Success");
        emails.put("laybyFinalPayment", "Lay-by Final Payment");
        emails.put("orderFraudReview", "Order Fraud Review");
        emails.put("orderShippingNotice", "Order Shipping Notice");
        emails.put("orderShippingNoticeProgress", "Order Shipping Notice Progress");
        emails.put("orderModification", "Order Modification");
        emails.put("orderPartiallyCancelled", "Order Partially Cancelled");
        emails.put("orderPartiallyCancelledCustomerRequest", "Order Partially Cancelled - Customer Request");
        emails.put("orderRefundProcessed", "Order Refund Processed");
        emails.put("orderExchangeSamePrice", "Order Exchange - Same Price");
        emails.put("orderExchangeDifferentPrice", "Order Exchange - Different Price");
        emails.put("cncNotification", "Cnc Notification");
        emails.put("giftCardReversal", "Gift Card Reversal Failure Notification Email");
        emails.put("balancePaymentDeclined", "Declined Balance Payment Notification Email");

        model.addAttribute("emailList", emails);

        final EmailHarnessForm form = new EmailHarnessForm();
        model.addAttribute("emailHarnessForm", form);

        return new ModelAndView("pages/harness/emailHarnessPage");
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public ModelAndView post(@Valid final EmailHarnessForm emailHarnessForm) {
        LOG.info("selectedEmail = " + emailHarnessForm.getEmails());
        if (StringUtils.isNotBlank(emailHarnessForm.getEmails())) {
            final String[] allEmail = emailHarnessForm.getEmails().split(",");

            for (final String emailType : allEmail) {
                emailSenders.get(emailType).send(emailHarnessForm);
            }
        }

        return new ModelAndView(UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/emailHarness");
    }
}

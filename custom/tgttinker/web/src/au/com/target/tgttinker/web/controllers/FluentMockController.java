/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgttinker.mock.facades.FluentMockFacade;


/**
 * @author mgazal
 *
 */
@Controller
public class FluentMockController {

    private static final String FLUENT_ACTION = "/fluent.action";

    private static final String SUMMARY_VIEW = "/pages/fluentSummary";

    @Autowired
    private FluentMockFacade fluentMockFacade;

    @RequestMapping(value = FLUENT_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {
        model.addAttribute("fluent", fluentMockFacade.getSummary());
        return SUMMARY_VIEW;
    }

    @RequestMapping(value = FLUENT_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            final ModelMap model) {
        fluentMockFacade.updateMock(request);
        return getMockSummary(model);
    }
}

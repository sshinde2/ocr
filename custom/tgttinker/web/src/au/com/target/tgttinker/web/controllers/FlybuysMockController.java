/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgttinker.mock.facades.FlybuysMockFacade;


/**
 * Generic mock functionality.
 */
@Controller
public class FlybuysMockController {

    /** URL to categories action. */
    private static final String FLYBUYS_ACTION = "/flybuy.action";

    /** Categories form view name. */
    private static final String SUMMARY_VIEW = "/pages/flybuysSummary";


    @Autowired
    private FlybuysMockFacade flybuysMockFacade;


    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = FLYBUYS_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model)
    {
        model.addAttribute("flybuys", flybuysMockFacade.getSummary());
        return SUMMARY_VIEW;
    }

    @RequestMapping(value = FLYBUYS_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            @RequestParam(value = "enable", required = false) final String enable, final ModelMap model)
    {
        if (enable != null) {
            flybuysMockFacade.setActive("true".equals(enable));
        }
        final String mockselect = request.getParameter("mockselect");
        final String mockConsumeResponse = request.getParameter("cunsumeResponseSelect");
        final String mockRefundResponse = request.getParameter("refundResponseSelect");
        if (mockselect != null && mockConsumeResponse != null && mockRefundResponse != null)
        {
            if (FlybuysResponseType.SUCCESS.toString().equals(mockselect)) {
                final String pointsAvailable = request.getParameter("AvlbflyBuyPoints");
                flybuysMockFacade.setMockResponse(mockselect, pointsAvailable, mockConsumeResponse, mockRefundResponse);
            }
            else
            {
                flybuysMockFacade.setMockResponse(mockselect, "0", "ERROR", "ERROR");

            }
        }
        return getMockSummary(model);
    }
}

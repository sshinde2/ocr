/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.InventoryAdjustmentMockFacade;


/**
 * @author pthoma20
 *
 */
@Controller
public class InventoryAdjustmentController {

    private static final String INVENTORY_ADJUSTMENT_ACTION = "inventoryadjustment.action";
    private static final String SUMMARY_VIEW = "/pages/inventoryAdjustmentSummary";

    @Autowired
    private InventoryAdjustmentMockFacade inventoryAdjustmentMockFacade;

    @RequestMapping(value = INVENTORY_ADJUSTMENT_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {
        model.addAttribute("inventoryAdjustmentSummary", inventoryAdjustmentMockFacade.getSummary());

        return SUMMARY_VIEW;
    }

    @RequestMapping(value = INVENTORY_ADJUSTMENT_ACTION, method = RequestMethod.POST)
    public String updateMock(
            @RequestParam(value = "enable", required = false) final String enable,
            @RequestParam(value = "success", required = false) final String success,
            final ModelMap model) {
        if (enable != null) {
            inventoryAdjustmentMockFacade.setActive(BooleanUtils.toBoolean(enable));
        }
        inventoryAdjustmentMockFacade.setMockValues(BooleanUtils.toBoolean(success));
        return getMockSummary(model);
    }
}

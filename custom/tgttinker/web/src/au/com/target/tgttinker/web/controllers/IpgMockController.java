/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2010 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.IpgMockFacade;



/**
 * Generic mock functionality.
 */
@Controller
public class IpgMockController
{
    /** URL to categories action. */
    private static final String IPG_ACTION = "/ipg.action";

    /** Categories form view name. */
    private static final String SUMMARY_VIEW = "/pages/ipgSummary";

    @Autowired
    private IpgMockFacade ipgMockFacade;

    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = IPG_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model)
    {
        model.addAttribute("mockRefundResponseList", ipgMockFacade.getSummary().getMockRefundResponses());
        model.addAttribute("ipg", ipgMockFacade.getSummary());
        return SUMMARY_VIEW;
    }


    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = IPG_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            @RequestParam(value = "enable", required = false) final String enable, final ModelMap model)
    {
        if (enable != null) {
            ipgMockFacade.setActive("true".equals(enable));
        }
        ipgMockFacade.setRefundResponse(request.getParameter("mockRefundResponse"),
                request.getParameter("declineCode"), request.getParameter("declineMessage"));
        ipgMockFacade.setSubmitSinglePaymentResponse(
                request.getParameter("singlePayDeclinedCode"), request.getParameter("singlePayDeclinedMsg"));
        ipgMockFacade.setServiceUnavailable(request.getParameter("serviceUnavailable"));
        return getMockSummary(model);
    }
}

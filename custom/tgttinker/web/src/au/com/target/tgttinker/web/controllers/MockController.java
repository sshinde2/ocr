/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2010 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgttinker.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import au.com.target.tgttinker.mock.facades.NavFacade;


/**
 * Generic mock functionality.
 */
@Controller
public class MockController
{
    /** URL to categories action. */
    private static final String MOCK_NAVIGATION = "/nav.action";

    /** Categories form view name. */
    private static final String NAV_VIEW = "/components/navlist";

    @Autowired
    private NavFacade navFacade;

    /**
     * Determines the category tree to given <code>categoryPath</code> param, sets this to the given model reference and
     * returns the categories view.
     * 
     * @param model
     *            reference to model object where the category tree will be set
     * @return categories view
     */
    @RequestMapping(value = MOCK_NAVIGATION)
    public String getMocks(final ModelMap model)
    {
        model.addAttribute("mocks", navFacade.getNavList());
        return NAV_VIEW;

    }
}

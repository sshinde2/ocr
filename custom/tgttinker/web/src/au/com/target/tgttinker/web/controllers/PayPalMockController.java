/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.CommonMockFacade;


/**
 * @author pratik
 *
 */
@Controller
public class PayPalMockController {

    private static final String PAYPAL_ACTION = "/paypal.action";

    private static final String SUMMARY_VIEW = "/pages/paypalSummary";

    @Autowired
    private CommonMockFacade payPalMockFacade;

    @RequestMapping(value = PAYPAL_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {

        model.addAttribute("paypal", payPalMockFacade.getSummary());
        return SUMMARY_VIEW;
    }

    @RequestMapping(value = PAYPAL_ACTION, method = RequestMethod.POST)
    public String updateMock(@RequestParam(value = "enable", required = false) final String enable,
            final ModelMap model) {

        payPalMockFacade.setActive(StringUtils.equals("true", enable));
        return getMockSummary(model);
    }

}

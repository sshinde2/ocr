package au.com.target.tgttinker.web.controllers;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.PinPadPaymentMockFacade;
import au.com.target.tgttinker.web.forms.PinPadPaymentDataForm;


/**
 * PinPad Payment Update Controller
 */
@Controller
public class PinPadPaymentUpdaterController {
    /** URL to categories action. */
    private static final String PINPAD_UPDATER = "/pinPadUpdater";

    private static final String PINPAD_UPDATER_MOCK = "/pinPadUpdater/mock";


    /** Categories form view name. */
    private static final String PINPAD_PAYMENT_UPDATER_PAGE = "/pages/pinPadPaymentUpdater";

    @Autowired
    private PinPadPaymentMockFacade pinPadPaymentMockFacade;

    @RequestMapping(value = PINPAD_UPDATER, method = RequestMethod.GET)
    public String get(final Model model) {
        model.addAttribute("pinPadPaymentDataForm", new PinPadPaymentDataForm());
        return PINPAD_PAYMENT_UPDATER_PAGE;
    }

    /**
     * This method will take the data from the form filled and posted to it and will call the facade layer to make the
     * necessary mocks.
     * 
     * @param model
     * @param pinPadPaymentDataForm
     * @return view
     */
    @RequestMapping(value = PINPAD_UPDATER, method = RequestMethod.POST)
    public String post(final Model model, final PinPadPaymentDataForm pinPadPaymentDataForm) {

        final String msg = pinPadPaymentMockFacade.updatePayment(pinPadPaymentDataForm.getPinPadPaymentData());
        model.addAttribute("Message", msg);
        model.addAttribute(pinPadPaymentDataForm);
        return PINPAD_PAYMENT_UPDATER_PAGE;
    }

    /**
     * This method will accept OrderNo and Mock Status and call facade the update the mock data in the database based on
     * the mock status. Mock status is not mandatory and if not supplied will be considered as Success.
     * 
     * @param model
     * @param orderNo
     * @param mockStatus
     * @return view
     */
    @RequestMapping(value = PINPAD_UPDATER_MOCK, method = RequestMethod.POST)
    public String mockSuccess(final Model model,
            @RequestParam(value = "orderNo", required = true) final String orderNo,
            @RequestParam(value = "mockStatus", required = false) final String mockStatus) {
        final PinPadPaymentDataForm pinPadPaymentDataForm = createDummyPinPadPaymentDataForm(orderNo);
        if (StringUtils.equalsIgnoreCase("Failure", mockStatus)) {
            pinPadPaymentDataForm.setRespAscii("Failure");
        }
        else {
            pinPadPaymentDataForm.setRespAscii("Success");
        }
        final String msg = pinPadPaymentMockFacade.updatePayment(pinPadPaymentDataForm.getPinPadPaymentData());
        model.addAttribute("Message", msg);
        model.addAttribute(pinPadPaymentDataForm);
        return PINPAD_PAYMENT_UPDATER_PAGE;
    }

    private PinPadPaymentDataForm createDummyPinPadPaymentDataForm(final String orderNo) {
        final PinPadPaymentDataForm pinPadPaymentDataForm = new PinPadPaymentDataForm();
        pinPadPaymentDataForm.setOrderNo(orderNo);
        pinPadPaymentDataForm.setAccountType("DUMMY ACCOUNT");
        pinPadPaymentDataForm.setCardType("visa");
        pinPadPaymentDataForm.setJournal("Dummy Journal");
        pinPadPaymentDataForm.setJournRoll("Dummy JournRoll");
        pinPadPaymentDataForm.setMaskedCardNumber("1234XXXXXXXX123");
        pinPadPaymentDataForm.setAuthCode("200");
        pinPadPaymentDataForm.setRrn("Dummy RRN");
        return pinPadPaymentDataForm;
    }
}

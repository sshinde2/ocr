/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.PollConsignmentMockFacade;


/**
 * @author gsing236
 *
 */
@Controller
public class PollConsignmentMockController {

    private static final String POLL_CONSIGNMENT_ACTION = "/pollConsignment.action";

    private static final String SUMMARY_VIEW = "/pages/pollConsignmentSummary";

    @Autowired
    private PollConsignmentMockFacade pollConsignmentMockFacade;

    @RequestMapping(value = POLL_CONSIGNMENT_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {
        model.addAttribute("pollConsignment", pollConsignmentMockFacade.getSummary());
        return SUMMARY_VIEW;
    }


    @RequestMapping(value = POLL_CONSIGNMENT_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            @RequestParam(value = "enable", required = false) final String enable,
            final ModelMap model) {
        if (enable != null) {
            final boolean enableFlag = BooleanUtils.toBoolean(enable);
            pollConsignmentMockFacade.setActive(enableFlag);
            // if mock not enabled then reset all data
            if (!enableFlag) {
                pollConsignmentMockFacade.resetDefault();
            }
        }

        pollConsignmentMockFacade.setMockSuccess(request.getParameter("success"));

        pollConsignmentMockFacade.setMockServiceUnavailable(request.getParameter("serviceUnavailable"));

        return getMockSummary(model);
    }

}

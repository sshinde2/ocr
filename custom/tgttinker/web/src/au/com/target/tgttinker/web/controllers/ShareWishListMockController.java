/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.impl.ShareWishListMockFacadeImpl;


/**
 * @author mjanarth
 *
 */
@Controller
public class ShareWishListMockController {

    private static final String SHARE_WISHLIST_ACTION = "sharewishlist.action";
    private static final String SUMMARY_VIEW = "/pages/shareWishlistSummary";

    @Autowired
    private ShareWishListMockFacadeImpl shareWishListMockFacade;

    @RequestMapping(value = SHARE_WISHLIST_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model)
    {
        model.addAttribute("shareWishListSummary", shareWishListMockFacade.getSummary());
        model.addAttribute("shareWishListSummaryResponses", shareWishListMockFacade.getSummary()
                .getMockShareWishListResponses());

        return SUMMARY_VIEW;
    }

    @RequestMapping(value = SHARE_WISHLIST_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            @RequestParam(value = "enable", required = false) final String enable, final ModelMap model)
    {
        if (enable != null) {
            shareWishListMockFacade.setActive(BooleanUtils.toBoolean(enable));
        }
        final String newResponseCode = request.getParameter("responseCode");
        final String newResponseMessage = request.getParameter("responseMessage");
        final String responseType = request.getParameter("mockShareWishListResponse");
        shareWishListMockFacade.setMockValues(newResponseCode, newResponseMessage, responseType);
        return getMockSummary(model);
    }
}

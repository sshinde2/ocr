/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgttinker.mock.data.StockVisibilitySummaryData;
import au.com.target.tgttinker.mock.facades.StockVisibilityMockFacade;


/**
 * @author rmcalave
 *
 */
@Controller
@RequestMapping(value = "/stockVisibility")
public class StockVisibilityMockController {

    @Autowired
    private StockVisibilityMockFacade stockVisibilityMockFacade;

    @RequestMapping(method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model) {
        final StockVisibilitySummaryData data = stockVisibilityMockFacade.getSummary();
        model.addAttribute("stockVisibilitySummaryData", data);

        return "/pages/stockVisibilitySummary";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String updateMock(@RequestParam(value = "enable", required = false) final boolean enable,
            final ModelMap model) {
        stockVisibilityMockFacade.setActive(enable);

        return getMockSummary(model);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addRecord(final String storeNumber, final String itemcode, final String soh, final ModelMap model) {
        stockVisibilityMockFacade.addStockVisibilityItemResponseDto(storeNumber, itemcode, soh);

        return getMockSummary(model);
    }

    @RequestMapping(value = "/update-code-exception", method = RequestMethod.POST)
    public String setCode(@RequestParam(value = "responseCode", required = false) final String responseCode,
            @RequestParam(value = "exception", required = false) final String exception,
            final ModelMap model) {
        stockVisibilityMockFacade.setException(exception);
        stockVisibilityMockFacade.setResponseCode(responseCode);


        return getMockSummary(model);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String updateRecord(final String storeNumber, final String itemcode, final String soh,
            final ModelMap model) {
        stockVisibilityMockFacade.updateStockVisibilityItemResponseDto(storeNumber, itemcode, soh);

        return getMockSummary(model);
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String removeRecord(final String storeNumber, final String itemcode, final ModelMap model) {
        stockVisibilityMockFacade.removeStockVisibilityItemResponseDto(storeNumber, itemcode);

        return getMockSummary(model);
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public String resetRecords(final ModelMap model) {
        stockVisibilityMockFacade.resetStockVisibilityItemResponseDto();

        return getMockSummary(model);
    }

    @ResponseBody
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    public List<StockVisibilityItemResponseDto> getStockVisibilityItemResponses() {
        final StockVisibilitySummaryData data = stockVisibilityMockFacade.getSummary();

        return data.getStockVisibilityItemResponseDtos();
    }

    @ResponseBody
    @RequestMapping(value = "json", method = RequestMethod.PUT, consumes = {
            MediaType.APPLICATION_JSON_VALUE })
    public List<StockVisibilityItemResponseDto> setStockVisibilityItemResponses(
            @RequestBody final List<StockVisibilityItemResponseDto> stockVisibilityItemResponses) {
        stockVisibilityMockFacade.resetStockVisibilityItemResponseDto();
        for (final StockVisibilityItemResponseDto stockVisibilityItemResponse : stockVisibilityItemResponses) {
            stockVisibilityMockFacade.addStockVisibilityItemResponseDto(stockVisibilityItemResponse.getStoreNumber(),
                    stockVisibilityItemResponse.getCode(), stockVisibilityItemResponse.getSoh());
        }

        return getStockVisibilityItemResponses();
    }
}

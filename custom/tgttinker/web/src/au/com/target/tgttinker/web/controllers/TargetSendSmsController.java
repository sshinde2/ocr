/**
 * 
 */
package au.com.target.tgttinker.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import au.com.target.tgttinker.mock.facades.TargetSendSmsFacadeMock;


/**
 * @author mjanarth
 * 
 */
@Controller
public class TargetSendSmsController {

    private static final String SENDSMS_ACTION = "/sendSms.action";

    private static final String SUMMARY_VIEW = "/pages/sendSmsNotification";

    @Autowired
    private TargetSendSmsFacadeMock sendSmsFacadeMock;



    /**
     * Gets the valid mock summary
     * 
     * @param model
     * @return String
     */
    @RequestMapping(value = SENDSMS_ACTION, method = RequestMethod.GET)
    public String getMockSummary(final ModelMap model)
    {
        model.addAttribute("sendSms", sendSmsFacadeMock.getSummary());
        return SUMMARY_VIEW;
    }


    /**
     * Update the mock response type
     * 
     * @param request
     * @param enable
     * @param model
     * @return string
     */
    @RequestMapping(value = SENDSMS_ACTION, method = RequestMethod.POST)
    public String updateMock(final HttpServletRequest request,
            @RequestParam(value = "enable", required = false) final String enable, final ModelMap model)
    {
        if (enable != null) {
            sendSmsFacadeMock.setActive("true".equals(enable));
        }
        final String mockselect = request.getParameter("mockselect");
        if (mockselect != null)
        {
            sendSmsFacadeMock.setMockResponse(mockselect);
        }
        return getMockSummary(model);
    }
}

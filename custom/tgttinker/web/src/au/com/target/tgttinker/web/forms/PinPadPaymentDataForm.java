/**
 * 
 */
package au.com.target.tgttinker.web.forms;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;

import au.com.target.tgttinker.mock.data.PinPadPaymentData;


/**
 * 
 * @author pthoma20
 * 
 */
public class PinPadPaymentDataForm {
    private String orderNo;
    private String rrn;
    private String maskedCardNumber;
    private String cardType;
    private String respAscii;
    private String accountType;
    private String journRoll;
    private String authCode;
    private String journal;

    /**
     * @return the rrn
     */
    public String getRrn() {
        return rrn;
    }

    /**
     * @param rrn
     *            the rrn to set
     */
    public void setRrn(final String rrn) {
        this.rrn = rrn;
    }

    /**
     * @return the maskedCardNumber
     */
    public String getMaskedCardNumber() {
        return maskedCardNumber;
    }

    /**
     * @param maskedCardNumber
     *            the maskedCardNumber to set
     */
    public void setMaskedCardNumber(final String maskedCardNumber) {
        this.maskedCardNumber = maskedCardNumber;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType
     *            the cardType to set
     */
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the respAscii
     */
    public String getRespAscii() {
        return respAscii;
    }

    /**
     * @param respAscii
     *            the respAscii to set
     */
    public void setRespAscii(final String respAscii) {
        this.respAscii = respAscii;
    }

    /**
     * @return the accountType
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * @param accountType
     *            the accountType to set
     */
    public void setAccountType(final String accountType) {
        this.accountType = accountType;
    }

    /**
     * @return the journRoll
     */
    public String getJournRoll() {
        return journRoll;
    }

    /**
     * @param journRoll
     *            the journRoll to set
     */
    public void setJournRoll(final String journRoll) {
        this.journRoll = journRoll;
    }

    /**
     * @return the authCode
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * @param authCode
     *            the authCode to set
     */
    public void setAuthCode(final String authCode) {
        this.authCode = authCode;
    }

    /**
     * @return the journal
     */
    public String getJournal() {
        return journal;
    }

    /**
     * @param journal
     *            the journal to set
     */
    public void setJournal(final String journal) {
        this.journal = journal;
    }

    /**
     * @return the orderNo
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo
     *            the orderNo to set
     */
    public void setOrderNo(final String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return {@link PinPadPaymentData}
     */
    public PinPadPaymentData getPinPadPaymentData() {
        final PinPadPaymentData data = new PinPadPaymentData();
        try {
            BeanUtils.copyProperties(data, this);
        }
        catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalStateException(e);
        }
        return data;
    }

}

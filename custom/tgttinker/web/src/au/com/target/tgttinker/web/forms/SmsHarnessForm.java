/**
 * 
 */
package au.com.target.tgttinker.web.forms;

/**
 * @author bhuang3
 * 
 */
public class SmsHarnessForm {

    private String smsList;
    private String orderNumber;
    private String letterType;
    private String storeNumber;

    /**
     * @return the smsList
     */
    public String getSmsList() {
        return smsList;
    }

    /**
     * @param smsList
     *            the smsList to set
     */
    public void setSmsList(final String smsList) {
        this.smsList = smsList;
    }

    /**
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber
     *            the orderNumber to set
     */
    public void setOrderNumber(final String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the letterType
     */
    public String getLetterType() {
        return letterType;
    }

    /**
     * @param letterType
     *            the letterType to set
     */
    public void setLetterType(final String letterType) {
        this.letterType = letterType;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }


}

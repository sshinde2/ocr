<!--
	Defines the layout of all pages by sitemesh mechanism. Decorates each page by a header and navigation section.
-->

<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
    <head>
        <title><decorator:title default="hybris Spring MVC Store" /></title>
        <decorator:head />
        <%@ include file="/includes/style.jsp" %>
    </head>
    <body>
        <%@include file="/includes/navigation.jsp" %>
        <div id="content">
            <h1><decorator:title default="hybris Spring MVC Store" /></h1>
            <decorator:body />
        </div>
    </body>
</html>
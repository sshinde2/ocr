<!--
	Container for categories tree, user info. Also renders navigation links to cart content, login form. 
-->



<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="navigation">
    <c:url value="${request.contextPath}/" var="homeurl"/>
    <a href="${homeurl}">Home</a><br/>
    <br/>
    <jsp:include page="/nav.action"/>
    <br/>
</div>

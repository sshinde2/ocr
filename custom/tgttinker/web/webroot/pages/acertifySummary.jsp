<!--
	Container for a categories_loop widget.  
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Accertify Mock status screen</title></head>
<body>

<c:url value="${request.contextPath}/acertify.action" var="acertifyurl"/>

<div id="status">
	<p>
	    <c:url value="${request.contextPath}/acertify.action" var="acertifyurl"/>
	    <c:choose>
			<c:when test="${acertify.mockActive}">
		      Active
               <form:form id='enablemock' method="POST" action="${acertifyurl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Inctive
              <c:if test="${acertify.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${acertifyurl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
		</c:otherwise>
		</c:choose>
	</p>
    <c:if test="${acertify.mockActive && acertify.validMockResponses != null && fn:length(acertify.validMockResponses) > 0}">
        <p>
	        Current mock response is ${acertify.mockResponse}<br />
	        <form:form id='enablemock' method="POST" action="${acertifyurl}">
	            <select name="mockselect">
	                <c:forEach items="${acertify.validMockResponses}" var="mockresponse">
	                    <option value="${mockresponse}" ${mockresponse eq acertify.mockResponse ? 'selected' : ''}>${mockresponse}</option>
	                </c:forEach>
	            </select>
	           <button type="submit">update</button>
	        </form:form>
        </p>
    </c:if> 
</div>

</body>
</html>

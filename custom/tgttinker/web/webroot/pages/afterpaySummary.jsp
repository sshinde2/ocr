<!--
    Container for a categories_loop widget.
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Afterpay Mock status screen</title></head>
<body>

<script type="text/javascript">
    function tickCheckbox() {
    	// ping
        var pingException = document.getElementById('pingException');
        var pingActive = document.getElementById('pingActive');
        autoCheckMockCheckbox(pingActive, pingException);

        // create oder
    	var createOrderException = document.getElementById('createOrderException');
        var createOrderMock = document.getElementById('createOrderMock');
        autoCheckMockCheckbox(createOrderMock, createOrderException);

        // get order
        var getOrderException = document.getElementById('getOrderException');
        var getOrderMock = document.getElementById('getOrderMock');
        autoCheckMockCheckbox(getOrderMock, getOrderException);

        // capture payment
        var capturePaymentException = document.getElementById('capturePaymentException');
        var capturePaymentMock = document.getElementById('capturePaymentMock');
        autoCheckMockCheckbox(capturePaymentMock, capturePaymentException);

        // get payment
        var getPaymentException = document.getElementById('getPaymentException');
        var getPaymentMock = document.getElementById('getPaymentMock');
        autoCheckMockCheckbox(getPaymentMock, getPaymentException);

        // create refund
        var createRefundException = document.getElementById('createRefundException');
        var createRefundMock = document.getElementById('createRefundMock');
        autoCheckMockCheckbox(createRefundMock, createRefundException);

        // get configuration
        var getConfigurationException = document.getElementById('getConfigurationException');
        var getConfigurationMock = document.getElementById('getConfigurationMock');
        autoCheckMockCheckbox(getConfigurationMock, getConfigurationException);
    }

    function autoCheckMockCheckbox(mock, exception) {
    	if(exception.checked) {
    		mock.checked = true;
        } else {
        	mock.checked = false;
        }
    }
</script>
<div id="status">
    <p>
        <c:url value="${request.contextPath}/afterpay.action" var="afterpayUrl"/>
        <c:choose>
            <c:when test="${afterpay.mockActive}">
              Active
               <form:form id='enablemock' method="POST" action="?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
            </c:when>
            <c:otherwise>
            Inactive
                <form:form id='enablemock' method="POST" action="?enable=true">
                    <button type="submit">Enable</button>
                </form:form>
            </c:otherwise>
        </c:choose>
    </p>
	    <c:if test="${afterpay.mockActive}">
	        <form:form id="setMockValues" method="POST" action="${afterpayUrl}" commandName="afterpay">
	           <table border="2">
	               <tr>
	                   <th style="width:200">API</th>
	                   <th style="width:250">Mock response (Yes/No) <span style="font-weight: normal"> (* if selecting this true, remember to change below json as per your scenario)</span></th>
	                   <th style="width:250">Throw Exception (Yes/No) <span style="font-weight: normal"> (* if selecting this true, make sure 'Mock Response' of respective operation is also selected as true)</span></th>
	               </tr>
	               <tr>
                       <td>GetConfiguration</td>
                       <td align="center"> <form:checkbox id="getConfigurationMock" path="getConfigurationActive" /> <br/><br/></td>
                       <td align="center"><form:checkbox id="getConfigurationException" path="getConfigurationException"  onchange="tickCheckbox()"/> <br/><br/></td>
                   </tr>
	               <tr>
	                   <td>Ping</td>
	                   <td align="center"> <form:checkbox id="pingActive" path="pingActive" /> <br/><br/></td>
	                   <td align="center"><form:checkbox id="pingException" path="throwException"  onchange="tickCheckbox()"/> <br/><br/></td>
	               </tr>
	               <tr>
                       <td>Create order</td>
                       <td align="center"> <form:checkbox id="createOrderMock" path="createOrderActive" /> <br/><br/></td>
                       <td align="center"><form:checkbox id="createOrderException" path="createOrderException" onchange="tickCheckbox()" /> <br/><br/></td>
                   </tr>
                   <tr>
                       <td>Get order</td>
                       <td align="center"> <form:checkbox id="getOrderMock" path="getOrderActive" /> <br/><br/></td>
                       <td align="center"><form:checkbox id="getOrderException" path="getOrderExpection" onchange="tickCheckbox()" /> <br/><br/></td>
                   </tr>
                   <tr>
                       <td>Capture payment</td>
                       <td align="center"> <form:checkbox id="capturePaymentMock" path="capturePaymentActive" /> <br/><br/></td>
                       <td align="center"><form:checkbox id="capturePaymentException" path="capturePaymentException" onchange="tickCheckbox()" /> <br/><br/></td>
                   </tr>
                   <tr>
                       <td>Get payment</td>
                       <td align="center"> <form:checkbox id="getPaymentMock" path="getPaymentActive" /> <br/><br/></td>
                       <td align="center"><form:checkbox id="getPaymentException" path="getPaymentException" onchange="tickCheckbox()" /> <br/><br/></td>
                   </tr>
                   <tr>
                       <td>Create refund</td>
                       <td align="center"> <form:checkbox id="createRefundMock" path="createRefundActive" /> <br/><br/></td>
                       <td align="center"><form:checkbox id="createRefundException" path="createRefundException" onchange="tickCheckbox()" /> <br/><br/></td>
                   </tr>
	           </table>
	           <br/><br/>
	           <button type="submit">Update</button>
	           <br/><br/>
		        <table>
		           <tr>
		               <td style="width:350"><b>Mocked response for getConfiguration</b></td><br/>
		               <td style="width:100"><form:textarea rows="15" cols="35" path="mockedGetConfigurationResponse" /></td>
		           </tr>
		        </table>
	            <br/><br/>
	             <table>
                   <tr>
                       <td style="width:350"><b>Mocked response for CreateOrder</b></td><br/>
                       <td style="width:100"><form:textarea rows="5" cols="65" path="mockedCreateOrderResponse" /></td>
                   </tr>
                </table>
                <br/><br/>
                <table>
                   <tr>
                       <td style="width:350"><b>Mocked response for getOrder</b></td><br/>
                       <td style="width:100"><form:textarea rows="35" cols="90" path="mockedGetOrderResponse" /></td>
                   </tr>
                </table>
                <br/><br/>
                <table>
                   <tr>
                       <td style="width:350"><b>Mocked response for capturePayment</b></td><br/>
                       <td style="width:100"><form:textarea rows="60" cols="90" path="mockedCapturePaymentResponse" /></td>
                   </tr>
                </table>
                <br/><br/>
                <table>
                   <tr>
                       <td style="width:350"><b>Mocked response for getPayment</b></td><br/>
                       <td style="width:100"><form:textarea rows="75" cols="65" path="mockedGetPaymentResponse" /></td>
                   </tr>
                </table>
                <table>
                   <tr>
                       <td style="width:350"><b>Mocked response for createRefund</b></td><br/>
                       <td style="width:100"><form:textarea rows="15" cols="65" path="mockedCreateRefundResponse" /></td>
                   </tr>
                </table>
                <br/><br/>
	        </form:form>
	    </c:if>
</div>
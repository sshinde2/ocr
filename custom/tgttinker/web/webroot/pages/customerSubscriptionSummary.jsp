<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Customer Subscription Mock status screen</title></head>
<body>

<c:url value="${request.contextPath}/customerSubscription.action" var="customerSubscriptionUrl"/>

<div id="status">
	<p>
	    <c:url value="${request.contextPath}/customerSubscription.action" var="customerSubscriptionUrl"/>
	    <c:choose>
			<c:when test="${customerSubscription.mockActive}">
		      Active
               <form:form id='enablemock' method="POST" action="${customerSubscriptionUrl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Inactive
              <c:if test="${customerSubscription.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${customerSubscriptionUrl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
			</c:otherwise>
		</c:choose>
	</p>
	
	<c:if test="${customerSubscription.mockActive}">
		<form:form id="setMockValues" method="POST" action="${customerSubscriptionUrl}" commandName="customerSubscription">
			<label>Response Code: </label><form:input path="responseCode" /><br />
			<label>Response Message: </label><form:input path="responseMessage" /><br />
			<label>Response Type: </label>
			<form:select path="responseType" items="${responseTypes}" itemLabel="response" itemValue="response" /><br />
			
			<button type="submit">Update</button>
		</form:form>
		<label>Note : Already Exists response type is not applicable for Mumshub personal details update</<label>
	</c:if>
</div>

</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
	<title>Fluent Mock status screen</title>
	<style type="text/css">
	th, td {
		padding: 10px !important;
	}
	</style>
</head>
<body>
	<c:url value="${request.contextPath}/fluent.action" var="fluentUrl"/>
	<form:form id="setMockValues" method="POST" action="${fluentUrl}" commandName="fluent">
		<table border="1">
			<tr>
				<th>API</th>
				<th>Mock Active?</th>
				<th>Mock Response</th>
				<th>Sample</th>
			</tr>
			<tr valign="middle">
				<td>Batch (Location)</td>
				<td align="center"><form:checkbox path="batchActive" /></td>
				<td>
					createJobResponse:<br>
					<form:textarea rows="10" cols="30" path="createJobResponse" /><br><br>
					createBatchResponse:<br>
					<form:textarea rows="10" cols="30" path="createBatchResponse" /><br><br>
					batchResponse:<br>
					<form:textarea rows="10" cols="30" path="batchResponse" />
				</td>
				<td rowspan="15" valign="top">
					<pre>
					<ul class="list-group">
						<li class="list-group-item">
Sample success response for most APIs:
{
	"id": 1
}
						</li>
						<li class="list-group-item">
Sample failure response for all APIs:
{
	"errors": [
		{
			"code": "500",
			"message": "Unknown error"
		}
	]
}
						</li>
						<li class="list-group-item">
Sample Create Fulfilment(Stock Lookup) response:
{
	"CC": {
		"P4010_blue_L": 10,
		"P4010_blue_M": 20
	},
	"HD": {
		"P4010_blue_L": 10,
		"P4010_blue_M": 20
	},
	"ED": {
		"P4010_blue_L": 10,
		"P4010_blue_M": 20
	},
	"PO": {
		"P4010_blue_L": 0,
		"P4010_blue_M": 0
	},
	"CONSOLIDATED_STORES_SOH": {
		"P4010_blue_L": 10,
		"P4010_blue_M": 20
	},
	"5001": {
		"P4010_blue_L": 10,
		"P4010_blue_M": 20
	}
}

Set this in fulfilmentOptionMap and leave fulfilmentOptionResponse blank.

To simulate errors and for other more complex scenarios for stock lookup 
you may need to populate fulfilmentOptionResponse
						</li>
						<li class="list-group-item">
Sample: Create Fulfilments by Order:
{
	"orderId": "11",
	"fulfilments": [
		{
			"orderId": "11",
			"fulfilmentId": 6,
			"deliveryType": "STANDARD",
			"eta": "3H",
			"createdOn": "2018-04-23T23:40:50.763+0000",
			"expiryTime": null,
			"status": "SYSTEM_REJECTED",
			"fulfilmentRef": null,
			"fulfilmentType": "CC_PFS",
			"items": [
				{
					"fulfilmentItemId": 6,
					"fulfilmentItemRef": null,
					"orderItemId": 9,
					"orderItemRef": "009179",
					"requestedQty": 2,
					"confirmedQty": 0,
					"filledQty": 0,
					"rejectedQty": 2,
					"skuRef": "009179"
				}
			],
			"fromAddress": null,
			"toAddress": {
				"locationRef": "5001",
				"companyName": "Target",
				"name": null,
				"street": "Cnr. Brougham & Moorabool Streets",
				"city": "Geelong",
				"postcode": "3220",
				"state": "VIC",
				"country": "Australia"
			},
			"consignment": null
		}
	]
}
Possible value:
fulfilmentType: CC_PFS, CC_PFDC, HD_PFS, HD_PFDC
deliveryType: STANDARD, EXPRESS, OVERNIGHT, 3HOURS

Status Map(fluent -> Hybris):
CREATED -> CREATED
AWAITING_WAVE -> SENT_TO_WAREHOUSE
SENT_TO_WAREHOUSE -> SENT_TO_WAREHOUSE
CONFIRMED_BY_WAREHOUSE -> SENT_TO_WAREHOUSE
ASSSIGNED -> WAVED
PICK -> WAVED
PACK -> WAVED
FULFILLED -> PICKED
PARTIALLY_FULFILLED -> PICKED
SHIPPED -> SHIPPED
PACK -> WAVED
REJECTED -> CANCELLED
SYSTEM_EXPIRED -> CANCELLED
EXPIRED -> CANCELLED
CANCELLED -> CANCELLED
						</li>
						<li  class="list-group-item">
Sample: Get Order API

{
  "orderId": "355",
  "orderRef": "68599004",
  "status": "PENDING",
  "type": "CC",
  "createdOn": "2018-06-18T23:31:01.033+0000",
  "customer": {
	"customerId": 436,
	"customerRef": "12763002",
	"firstName": "Test",
	"lastName": "Mock",
	"email": "Test.Mock@target.com.au",
	"mobile": null
  },
  "retailer": {
	"retailerId": "2"
  },
  "fulfilments": [
	{
	  "orderId": "355",
	  "fulfilmentId": "281",
	  "fulfilmentRef": null,
	  "status": "SYSTEM_REJECTED",
	  "fulfilmentType": "CC_PFS"
	}
  ],
  "items": [
	{
	  "imageUrlRef": "/medias/static_content/product/images/large/19/84/A401984.jpg",
	  "requestedQty": 1,
	  "skuId": "258985",
	  "skuRef": "47041509",
	  "skuPrice": 49,
	  "skuTaxPrice": null,
	  "totalPrice": 49,
	  "totalTaxPrice": null,
	  "taxType": null,
	  "currency": "AUD",
	  "productId": "61247"
	}
  ],
  "fulfilmentChoice": {
	"fulfilmentType": "CC_PFS",
	"fulfilmentPrice": 3,
	"fulfilmentTaxPrice": null,
	"currency": "AUD",
	"pickupLocationRef": "5001",
	"deliveryType": "STANDARD",
	"deliveryInstruction": null,
	"address": {
	  "locationRef": "5001",
	  "companyName": "Geelong",
	  "name": null,
	  "street": "Cnr. Brougham & Moorabool Streets",
	  "city": "Geelong",
	  "postcode": "3220",
	  "state": "VIC",
	  "country": "Australia"
	}
  }
}
						</li>
					</ul>
					</pre>
				</td>
			</tr>
			<tr valign="middle">
				<td>Create Category</td>
				<td align="center"><form:checkbox path="createCategoryActive" /></td>
				<td><form:textarea rows="10" cols="30" path="createCategoryResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Update Category</td>
				<td align="center"><form:checkbox path="updateCategoryActive" /></td>
				<td><form:textarea rows="10" cols="30" path="updateCategoryResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Search Category</td>
				<td align="center"><form:checkbox path="searchCategoryActive" /></td>
				<td><form:textarea rows="10" cols="30" path="searchCategoryResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Create Product</td>
				<td align="center"><form:checkbox path="createProductActive" /></td>
				<td><form:textarea rows="10" cols="30" path="createProductResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Update Product</td>
				<td align="center"><form:checkbox path="updateProductActive" /></td>
				<td><form:textarea rows="10" cols="30" path="updateProductResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Search Product</td>
				<td align="center"><form:checkbox path="searchProductActive" /></td>
				<td><form:textarea rows="10" cols="30" path="searchProductResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Create SKU</td>
				<td align="center"><form:checkbox path="createSkuActive" /></td>
				<td><form:textarea rows="10" cols="30" path="createSkuResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Update SKU</td>
				<td align="center"><form:checkbox path="updateSkuActive" /></td>
				<td><form:textarea rows="10" cols="30" path="updateSkuResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Search SKU</td>
				<td align="center"><form:checkbox path="searchSkuActive" /></td>
				<td><form:textarea rows="10" cols="30" path="searchSkuResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Create Fulfilment(Stock Lookup)</td>
				<td align="center"><form:checkbox path="createFulfilmentActive" /></td>
				<td>
					fulfilmentOptionMap:<br>
					<form:textarea rows="10" cols="30" path="fulfilmentOptionMap" /><br><br>
					upsertCcAts:<br>
					<form:textarea rows="10" cols="30" path="upsertCcAts" /><br><br>
					upsertHdAts:<br>
					<form:textarea rows="10" cols="30" path="upsertHdAts" /><br><br>
					upsertEdAts:<br>
					<form:textarea rows="10" cols="30" path="upsertEdAts" /><br><br>
					upsertConsolidatedStoreSoh:<br>
					<form:textarea rows="10" cols="30" path="upsertConsolidatedStoreSoh" /><br><br>
					fulfilmentOptionResponse:<br>
					<form:textarea rows="10" cols="30" path="fulfilmentOptionResponse" />
				</td>
			</tr>
			<tr valign="middle">
				<td>Create Order</td>
				<td align="center"><form:checkbox path="createOrderActive" /></td>
				<td><form:textarea rows="10" cols="30" path="createOrderResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Event Sync</td>
				<td align="center"><form:checkbox path="eventSyncActive" /></td>
				<td><form:textarea rows="10" cols="30" path="eventResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Retrieve Fulfilments</td>
				<td align="center"><form:checkbox path="retrieveFulfilmentsByOrderActive" /></td>
				<td><form:textarea rows="10" cols="30" path="fulfilmentsResponse" /></td>
			</tr>
			<tr valign="middle">
				<td>Get Order</td>
				<td align="center"><form:checkbox path="viewOrderActive" /></td>
				<td><form:textarea rows="10" cols="30" path="viewOrderResponse" /></td>
			</tr>
		</table>
		<button type="submit">Update</button>
	</form:form>
</body>
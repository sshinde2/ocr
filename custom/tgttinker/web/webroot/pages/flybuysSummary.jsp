<!--
	Container for a categories_loop widget.  
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Flybuys Mock status screen</title>
</head>
<body>

<c:url value="${request.contextPath}/flybuy.action" var="flybuyurl"/>


<div id="status">
	<p>
	    <c:url value="${request.contextPath}/flybuy.action" var="flybuyurl"/>
	    <c:choose>
			<c:when test="${flybuys.mockActive}">
		      Mock is Currently Active
               <form:form id='enablemock' method="POST" action="${flybuyurl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Mock is Currently Inactive
              <c:if test="${flybuys.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${flybuyurl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
		</c:otherwise>
		</c:choose>
	</p>
	<c:if test="${flybuys.mockActive}">
	<form:form id='enablemock' method="POST" action="${flybuyurl}">
	<br />
<b>Set options for Authenticate response   </b> <br /><br />
	Current Authenticate response is:&nbsp;<select name="mockselect">
	                <c:forEach items="${flybuys.validMockResponses}" var="mockresponse">
	                    <option value="${mockresponse}" ${mockresponse eq flybuys.mockResponse ? 'selected' : ''}>${mockresponse}</option>
	                </c:forEach>
     </select><br /><br />
     Available Flybuy points:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="AvlbflyBuyPoints" value="${flybuys.mockPoints}"><br />

<br />
<b>Set options for consume response    </b> <br /><br />
	 Current Consume response is: :&nbsp;<select name="cunsumeResponseSelect">
	                <c:forEach items="${flybuys.validMockResponses}" var="consumeResponse">
	                    <option value="${consumeResponse}" ${consumeResponse eq flybuys.mockConsumeResponse ? 'selected' : ''}>${consumeResponse}</option>
	                </c:forEach>
     </select><br />
<br /><br />
<b>Set options for refund response    </b> <br /><br />
	 Current refund response is: :&nbsp;<select name="refundResponseSelect">
	                <c:forEach items="${flybuys.validMockResponses}" var="refundResponse">
	                    <option value="${refundResponse}" ${refundResponse eq flybuys.mockRefundResponse ? 'selected' : ''}>${refundResponse}</option>
	                </c:forEach>
     </select><br />
     <br />
	<button type="submit">update</button>
	</form:form>
	
	</c:if>
</div>


</body>
</html>
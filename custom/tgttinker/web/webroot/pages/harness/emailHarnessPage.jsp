<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<form:form method="post" commandName="emailHarnessForm" action="/tgttinker/emailHarness/send">
	
	<form:errors id="" path="*" element="div" />
	
	To address: <form:input path="toAddress" /><br /><br />
	<form:select path="emails" items="${emailList}" multiple="true" size="25" />
	
	<div id="orderShippingFields" style="border: 1px solid;">
		<h3>For Order Shipping Notification</h3>
		Order Number: <form:input path="orderNumber" /><br />
		Product Code: <form:input path="productCode" /><br />
		User ID for Forgotten Password: <form:input path="forgottenPasswordCustomerEmail" />
		<h3>For CNC Notification</h3>
		CNC Notification Letter Type: <form:input path="letterType" /><br />
		CNC Selected Store Number: <form:input path="storeNumber" />
	</div>
	<br />
	
	<input type="submit" value="Send email" />
</form:form>
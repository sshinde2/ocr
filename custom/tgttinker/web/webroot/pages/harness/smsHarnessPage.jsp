<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<form:form method="post" commandName="smsHarnessForm" action="/tgttinker/smsHarness/send">
	
	<form:errors id="" path="*" element="div" />
	
	
	<form:select path="smsList" items="${smsList}" multiple="false" size="25" />
	
	<div id="orderShippingFields" style="border: 1px solid;">
		Order Number: <form:input path="orderNumber" /><br />
		<h3>For CNC Notification</h3>
		CNC Notification Letter Type: <form:input path="letterType" /><br />
		CNC Selected Store Number: <form:input path="storeNumber" />
	</div>
	<br />
	
	<input type="submit" value="Generate sms" />
</form:form>

<div id="smsMessage">
	${smsMessage}
</div>
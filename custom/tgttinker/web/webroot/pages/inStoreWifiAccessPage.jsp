<!--
	Instore wifi access page.
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head><title>STORE WIFI ACCESS PAGE</title></head>
<body>

<div id="status">
	<p>
		<a href="/~/instore/wifi?switch_url=/tgttinker/access-wifi&ap_mac=e8:ba:70:31:55:90&client_mac=18:87:96:8f:cc:50&wlan=Target%20WiFi&redirect=http://www.google.com">Go to instore wifi page with google redirect url</a>
	</p>
	<table border="1">
	<tr>
	<td>Redirect Url</td><td>${redirectUrl}</td>
	</tr>
	<tr>
	<td>Button Clicked</td><td>${buttonClicked}</td>
	</tr>
	</table>
</div>

</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Inventory Adjustment mock status screen</title></head>
<body>

<c:url value="${request.contextPath}/inventoryadjustment.action" var="inventoryAdjustmentUrl"/>

<div id="status">
	<p>
	    <c:url value="${request.contextPath}/inventoryadjustment.action" var="inventoryAdjustmentUrl"/>
	    <c:choose>
			<c:when test="${inventoryAdjustmentSummary.mockActive}">
		      Active
               <form:form id='enablemock' method="POST" action="${inventoryAdjustmentUrl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Inactive
              <c:if test="${inventoryAdjustmentSummary.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${inventoryAdjustmentUrl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
			</c:otherwise>
		</c:choose>		
	</p>
	
	 <c:if test="${inventoryAdjustmentSummary.mockActive}">   
	 <br/>
	 <c:choose>
			<c:when test="${inventoryAdjustmentSummary.success}">
		      Will Succeed Currently
               <form:form id='enablemock' method="POST" action="${inventoryAdjustmentUrl}?enable=true&success=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Make It Fail</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Will Fail Currently
              <c:if test="${inventoryAdjustmentSummary.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${inventoryAdjustmentUrl}?enable=true&success=true">
                        <button type="submit">Make It Work</button>
                    </form:form>
                </c:if>
			</c:otherwise>
		</c:choose>		                 
    </c:if> 
</div>

</body>
</html>

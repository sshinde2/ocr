<!--
	Container for a categories_loop widget.  
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>IPG Mock status screen</title></head>
<body>

<c:url value="${request.contextPath}/ipg.action" var="ipgUrl"/>

<div id="status">
	<p>
	    <c:url value="${request.contextPath}/ipg.action" var="ipgUrl"/>
	    <c:choose>
			<c:when test="${ipg.mockActive}">
		      Active
               <form:form id='enablemock' method="POST" action="${tnsurl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Inactive
                    <form:form id='enablemock' method="POST" action="${tnsurl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
		</c:otherwise>
		</c:choose>
	</p>
    <c:if test="${ipg.mockActive}">   
    	<form:form id="setMockValues" method="POST" action="${ipgUrl}" commandName="ipg">  
    	 <B><form:checkbox path="serviceUnavailable" /> Service Unavailable</B> <br/><br/>
    	 <B>SubmitSinglePayment (Credit Card) Response (Don't add decline code and message for Success):</B>
    	 <br/>
                <label>Decline Code</label> <form:input path="singlePayDeclinedCode" />
                <label>Decline Message</label><form:input path="singlePayDeclinedMsg" /> 
                
        <br/><Br/>        
        <b>Current Refund response is ${ipg.mockRefundDefault} </b> 
              <form:select path="mockRefundResponse" items="${mockRefundResponseList}"  /><br />
                <label>Decline Code</label> <form:input path="declineCode" />
   				<label>Decline Message</label><form:input path="declineMessage" />
   				<p>(Don't add decline code and message for Success)</p><br/> 	  
   				
   				<br/><br/>
                <button type="submit">Update All</button>      				
   		</form:form>
        <p>
            <B>Decline code example:</B><br/>
	        <span>"13" = BANK_DECLINE </span><br/>
			<span>"14" = INCORRECT_CARD_NUMBER_OR_TYPE </span><br/>
			<span>"23" = BANK_DECLINE </span><br/>
			<span>"24" = BANK_DECLINE </span><br/>
			<span>"33" = INVALID_CARD_EXPIRATION_DATE </span><br/>		
			<span>"34" = BANK_DECLINE </span><br/>
			<span>"35" = BANK_DECLINE </span><br/>
			<span>"36" = BANK_DECLINE </span><br/>
			<span>"37" = BANK_DECLINE </span><br/>
			<span>"38" = BANK_DECLINE </span><br/>
			<span>"39" = BANK_DECLINE </span><br/>
			<span>"40" = BANK_DECLINE </span><br/>
			<span>"41" = BANK_DECLINE </span><br/>
			<span>"42" = BANK_DECLINE </span><br/>
			<span>"43" = BANK_DECLINE </span><br/>
			<span>"44" = BANK_DECLINE </span><br/>
			<span>"51" = INSUFFICIENT_FUNDS </span><br/>
			<span>"52" = BANK_DECLINE </span><br/>
			<span>"53" = BANK_DECLINE </span><br/>
			<span>"54" = INVALID_CARD_EXPIRATION_DATE </span><br/>
			<span>"55" = INVALID_CVN </span><br/>
			<span>"56" = INCORRECT_CARD_NUMBER_OR_TYPE </span><br/>
			<span>"57" = BANK_DECLINE </span><br/>
			<span>"59" = STOLEN_OR_LOST_CARD </span><br/>
			<span>"61" = CREDIT_LIMIT_REACHED </span><br/>
			<span>"62" = BANK_DECLINE </span><br/>
			<span>"63" = BANK_DECLINE </span><br/>
			<span>"64" = AMOUNTS_MUST_MATCH </span><br/>
			<span>"65" = CARD_USED_TOO_RECENTLY </span><br/>
			<span>"66" = BANK_DECLINE </span><br/>
			<span>"67" = BANK_DECLINE </span><br/>
			<span>//68 stands for timeout, however ipg treats this as declined, detail reason will be saved into declined details</span><br/>
			<span>"68" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"75" = BANK_DECLINE </span><br/>
			<span>"90" = TRANSACTION_ALREADY_SETTLED_OR_REVERSED </span><br/>
			<span>"91" = COMMUNICATION_PROBLEM </span><br/>
			<span>"92" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"93" = DUPLICATE_TRANSACTION </span><br/>
			<span>"94" = BANK_DECLINE </span><br/>
			<span>"95" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"96" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"97" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"98" = GENERAL_SYSTEM_ERROR)</span><br/>
			<span>"99" = UNKNOWN_CODE </span><br/>
			<span>"100" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"180" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"182" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"183" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"184" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"190" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"191" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"192" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"200" = GENERAL_SYSTEM_ERROR </span><br/>
			<span>"201" = GENERAL_SYSTEM_ERROR </span><br/>
		</p>        
    </c:if> 
</div>

</body>
</html>

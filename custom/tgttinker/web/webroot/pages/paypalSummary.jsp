<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>PayPal Refunds Mock Status Screen</title></head>
<body>

<c:url value="${request.contextPath}/paypal.action" var="paypalUrl"/>

<div id="status">
	<p> 
		<c:choose>
			<c:when test="${paypal.mockActive}">
				Active
				<form:form id="enableMock" method="POST" action="${paypalUrl}?enable=false">
					<input type="hidden" name="enable" id="enable" value="false" />
					<button type="submit">Disable</button>
				</form:form>
			</c:when>
			<c:otherwise>
				Inactive
				<form:form id='enableMock' method="POST" action="${paypalUrl}?enable=true">
					<button type="submit">Enable</button>
				</form:form>
			</c:otherwise>
		</c:choose>
	</p>
</div>
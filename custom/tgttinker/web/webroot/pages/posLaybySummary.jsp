<!--
	Container for a categories_loop widget.  
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Pos Layby Mock status screen</title></head>
<body>

<c:url value="${request.contextPath}/poslayby.action" var="poslaybyurl"/>

<div id="status">
	<p>
	    <c:choose>
			<c:when test="${poslayby.mockActive}">
		      Active
               <form:form id='enablemock' method="POST" action="${poslaybyurl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Inctive
              <c:if test="${poslayby.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${poslaybyurl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
		</c:otherwise>
		</c:choose>
	</p>
</div>

</body>
</html>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Send SMS Mock status screen</title></head>
<body>

<c:url value="${request.contextPath}/sendSms.action" var="sendSmsUrl"/>

<div id="status">
    <p>
       <c:url value="${request.contextPath}/sendSms.action" var="sendSmsUrl"/>
        <c:choose>
            <c:when test="${sendSms.mockActive}">
              Active
               <form:form id='enablemock' method="POST" action="${sendSmsUrl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
            </c:when>
            <c:otherwise>
            Inctive
              <c:if test="${sendSms.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${sendSmsUrl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
        </c:otherwise>
        </c:choose>
    </p>

   <c:if test="${sendSms.mockActive && sendSms.validMockResponses != null && fn:length(sendSms.validMockResponses) > 0}">
        <p>
            Current mock response is ${sendSms.mockResponse}<br />
            <form:form id='enablemock' method="POST" action="${sendSmsUrl}">
                <select name="mockselect">
                    <c:forEach items="${sendSms.validMockResponses}" var="mockresponse">            
                        <option value="${mockresponse}" ${mockresponse eq sendSms.mockResponse ? 'selected' : ''}>${mockresponse}</option>
                    </c:forEach>
                </select>
               <button type="submit">update</button>
            </form:form>
        </p>
    </c:if>  
</div>

</body>
</html>

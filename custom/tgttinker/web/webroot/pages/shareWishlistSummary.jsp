<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Customer Share WishList Mock status screen</title></head>
<body>

<c:url value="${request.contextPath}/sharewishlist.action" var="shareWishListUrl"/>

<div id="status">
	<p>
	    <c:url value="${request.contextPath}/sharewishlist.action" var="shareWishListUrl"/>
	    <c:choose>
			<c:when test="${shareWishListSummary.mockActive}">
		      Active
               <form:form id='enablemock' method="POST" action="${shareWishListUrl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Inactive
              <c:if test="${shareWishListSummary.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${shareWishListUrl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
			</c:otherwise>
		</c:choose>		
	</p>
	 <c:if test="${shareWishListSummary.mockActive}">   
	 <b>Current ShareWishList response is ${shareWishListSummary.mockShareWishListResponse} </b> 
	 <br/>
	 <form:form id="setMockValues" method="POST" action="${shareWishListUrl}" commandName="shareWishListSummary">  
          <form:select path="mockShareWishListResponse" items="${shareWishListSummaryResponses}"  /><br/><br/>
             <label>Error Code</label> <form:input path="responseCode" />
   			 <label>Error Message</label><form:input path="responseMessage" />
   			  <p>(Don't add Error code and message for Success)</p><br/> 	     				
   				<br/><br/>
                <button type="submit">Update All</button>      				
   		</form:form>                    
    </c:if> 
</div>

</body>
</html>

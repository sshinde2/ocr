<!--
    Container for a categories_loop widget.  
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shipster Mock status screen</title>
<script type="text/javascript">

function pushJSON(mock){
	if(mock == 'confirmOrder'){
		data = JSON.parse($('input[name=confirmOrder]:checked').val());
		url = '/tgttinker/shipster/confirmOrder';
	} else{
		data = JSON.parse($('input[name=verifyEmail]:checked').val());
		url = '/tgttinker/shipster/verifyEmail';
	}
	$.ajax({
	    headers: { 
	        'Content-Type': 'application/json'
	    },
	    'type': 'POST',
	    'url': url,
	    'data': JSON.stringify(data)
	    });
}
</script>
</head>
<body>

<div id="status">
    <p>
        <c:url value="${request.contextPath}/shipster" var="shipsterurl"/>
        <c:choose>
            <c:when test="${shipster.mockActive}">
              Active
               <form:form id='enablemock' method="POST" action="?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
            </c:when>
            <c:otherwise>
            Inactive
                <form:form id='enablemock' method="POST" action="?enable=true">
                    <button type="submit">Enable</button>
                </form:form>
            </c:otherwise>
        </c:choose>
    </p>
        <c:if test="${shipster.mockActive}">
            <form:form id="updateVerifyEmail" method="POST" action="${shipsterurl}/updateVerifyEmail">
                Mock Verify Email Enable : <input id="verifyEmailMockEnable" name="verifyEmailMockEnable" type="checkbox" ${shipster.verifyEmailMock?'checked':''} onchange="this.form.submit();"/>
                <c:if test="${shipster.verifyEmailMock}">
                    Service Available : <input id="verifyEmailServiceAvailable" name="verifyEmailServiceAvailable" type="checkbox" ${shipster.verifyEmailServiceAvailable?'checked':''} onchange="this.form.submit();"/>
                </c:if>
            </form:form>
            <c:if test="${shipster.verifyEmailMock and shipster.verifyEmailServiceAvailable}">
                <form:form id="verifyEmail" method="POST" action="${shipsterurl}/verifyEmail">
                
                    Valid Shipster Member : <input name="verifyEmail" type="radio" value='{"verified":"true"}' ${shipster.verifyEmailResponse == '{"verified":"true"}'?'checked':'' }/><br/>
                    
                    Invalid Shipster Member : <input name="verifyEmail" type="radio" value='{"verified":"false"}' ${shipster.verifyEmailResponse == '{"verified":"false"}'?'checked':'' }/><br/>
                    
                    402 - Not existent or inactive subscriber: <input name="verifyEmail" type="radio" 
                                                                value='{"errors":[{"id":"402","code":"PAYMENT_REQUIRED","detail":"User not found"}]}' 
                                                                ${shipster.verifyEmailResponse == '{"errors":[{"id":"402","code":"PAYMENT_REQUIRED","detail":"User not found"}]}'?'checked':'' }/><br/>
                                                                
                    400 - Empty or missing email addresses URL parameter : <input name="verifyEmail" type="radio" 
                                                                            value='{"errors":[{"id":"400","code":"BAD_REQUEST","detail":"Email parameter is mandatory and cannot be empty"}]}' 
                                                                            ${shipster.verifyEmailResponse == '{"errors":[{"id":"400","code":"BAD_REQUEST","detail":"Email parameter is mandatory and cannot be empty"}]}'?'checked':'' }/><br/>
                                                                            
                    401 - Missing Auth Key : <input name="verifyEmail" type="radio" 
                                               value='{"errors":[{"id":"401","code":"UNAUTHORIZED","detail":"No API key found in request"}]}'
                                               ${shipster.verifyEmailResponse == '{"errors":[{"id":"401","code":"UNAUTHORIZED","detail":"No API key found in request"}]}'?'checked':'' }/><br/>
                                               
                    403 - Invalid Auth Credential : <input name="verifyEmail" type="radio" 
                                                       value='{"errors":[{"id":"403","code":"FORBIDDEN","detail":"Invalid authentication credentials"}]}'
                                                        ${shipster.verifyEmailResponse == '{"errors":[{"id":"403","code":"FORBIDDEN","detail":"Invalid authentication credentials"}]}'?'checked':'' }/><br/>
                                                        
                    <input type="button" value="update" onclick="pushJSON('verifyEmail')"/>
                    
                </form:form>
            </c:if>
            <form:form id="updateConfirmOrder" method="POST" action="${shipsterurl}/updateConfirmOrder">
            
                Mock Confirm Order Enable : <input id="confirmOrderMockEnable" name="confirmOrderMockEnable" type="checkbox" ${shipster.confirmOrderMock?'checked':''} onchange="this.form.submit();"/>
                <c:if test="${shipster.confirmOrderMock}">
                    Service Available : <input id="confirmOrderServiceAvailable" name="confirmOrderServiceAvailable" type="checkbox" ${shipster.confirmOrderServiceAvailable?'checked':''} onchange="this.form.submit();"/>
                    <c:if test="${shipster.confirmOrderServiceAvailable}">
                        Time Out : <input id="confirmOrderTimeOut" name="confirmOrderTimeOut" type="checkbox" ${shipster.confirmOrderTimeOut?'checked':''} onchange="this.form.submit();"/>
                    </c:if>
                </c:if>                
            </form:form>
            <c:if test="${shipster.confirmOrderMock and shipster.confirmOrderServiceAvailable and not shipster.confirmOrderTimeOut}">
                <form:form id="confirmOrder" method="POST" action="${shipsterurl}/confirmOrder">
                    Success Order Confirmation : <input name="confirmOrder" type="radio" value='{"submitted":"true"}' ${shipster.confirmOrderResponse == '{"submitted":"true"}'?'checked':'' } /><br/>
                    
                    Failure Order Confirmation : <input name="confirmOrder" type="radio" value='{"submitted":"false"}' ${shipster.confirmOrderResponse == '{"submitted":"false"}'?'checked':'' }/><br/>
                    
                    Not existent or inactive subscriber: <input name="confirmOrder" type="radio" 
                                                                value='{"errors":[{"id":"402","code":"PAYMENT_REQUIRED","detail":"User not found"}]}' 
                                                                ${shipster.confirmOrderResponse == '{"errors":[{"id":"402","code":"PAYMENT_REQUIRED","detail":"User not found"}]}'?'checked':'' } /><br/>
                                                                
                    400 - Validation failed/Bad Request : <input name="confirmOrder" type="radio" 
                                                     value='{"errors":[{"id":"400","code":"BAD_REQUEST","detail":"Email parameter is mandatory and cannot be empty"}]}' 
                                                     ${shipster.confirmOrderResponse == '{"errors":[{"id":"400","code":"BAD_REQUEST","detail":"Email parameter is mandatory and cannot be empty"}]}'?'checked':'' } /><br/>
                    
                    422 - Validation failed : <input name="confirmOrder" type="radio" 
                                                     value='{"errors":[{"id":"422","code":"UNPROCESSABLE_ENTITY","detail":"Order already exist in the system"}]}' 
                                                     ${shipster.confirmOrderResponse == '{"errors":[{"id":"422","code":"UNPROCESSABLE_ENTITY","detail":"Order already exist in the system"}]}'?'checked':'' } /><br/>
                                                     
                    429 - Too Many Request : <input name="confirmOrder" type="radio" 
                                                    value='{"errors":[{"id":"429","code":"TOO_MANY_REQUESTS","detail":"The number of requests per second has exceeded the rate limit and your responses will be throttled temporarily."}]}'
                                                    ${shipster.confirmOrderResponse == '{"errors":[{"id":"429","code":"TOO_MANY_REQUESTS","detail":"The number of requests per second has exceeded the rate limit and your responses will be throttled temporarily."}]}'?'checked':'' } /><br/>
                                                    
                    401 - Missing Auth Key : <input name="confirmOrder" type="radio" 
                                                    value='{"errors":[{"id":"401","code":"UNAUTHORIZED","detail":"No API key found in request"}]}'
                                                    ${shipster.confirmOrderResponse == '{"errors":[{"id":"401","code":"UNAUTHORIZED","detail":"No API key found in request"}]}'?'checked':'' } /><br/>
                                                    
                    403 - Invalid Auth Credential : <input name="confirmOrder" type="radio" 
                                                           value='{"errors":[{"id":"403","code":"FORBIDDEN","detail":"Invalid authentication credentials"}]}'
                                                           ${shipster.confirmOrderResponse == '{"errors":[{"id":"403","code":"FORBIDDEN","detail":"Invalid authentication credentials"}]}'?'checked':'' } /><br/>
                                                           
                    <input type="button" value="update" onclick="pushJSON('confirmOrder')"/>
                </form:form>
            </c:if>            
        </c:if>
        
        
 </div>
 </body>
 </html>       
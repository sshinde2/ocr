<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>Stock Visibility Mock Status Screen</title></head>

<c:url value="${request.contextPath}/stockVisibility" var="stockVisibilityUrl"/>
<c:choose>
    <c:when test="${stockVisibilitySummaryData.mockActive}">
        <c:set var="enabled" value="true" />
        <c:set var="labelClass" value="label-success" />
        <c:set var="labelValue" value="Active" />
        <c:set var="buttonValue" value="Disable" />
    </c:when>
    <c:otherwise>
        <c:set var="enabled" value="false" />
        <c:set var="labelClass" value="label-danger" />
        <c:set var="labelValue" value="Inactive" />
        <c:set var="buttonValue" value="Enable" />
    </c:otherwise>
</c:choose>

<body>
<div id="status">
    <h4>
        <span class="label ${labelClass}">${labelValue}</span>
    </h4>
    <p> 
        <form:form id="enableMock" method="POST" action="${stockVisibilityUrl}?enable=${not enabled}">
            <c:if test="${enabled}">
                <input type="hidden" name="enable" id="enable" value="false" />
            </c:if> 
            <button type="submit" class="btn btn-default">
                <c:out value="${buttonValue}" />
            </button>
        </form:form>
    </p>
    <p> 
        <form:form id="exceptionAndCode" method="POST" action="${stockVisibilityUrl}/update-code-exception">
            Response Code: <input name="responseCode"  value="${stockVisibilitySummaryData.responseCode}" /> <br/>
            Exception: <input name="exception" value="${stockVisibilitySummaryData.exception}" />
            <button type="submit" class="btn btn-default">Update Response Code/Exception</button>
        </form:form>
    </p>

    <c:if test="${enabled}">
        <div class="page-header">
            <h1>Stock Levels</h1>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Store number</th>
                    <th>Item code</th>
                    <th colspan="2">Stock on hand</th>
                    <th />
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${stockVisibilitySummaryData.stockVisibilityItemResponseDtos}" var="stockVisibilityItemResponseDto">
                    <tr>
                        
                            <td>${stockVisibilityItemResponseDto.storeNumber}</td>
                            <td>${stockVisibilityItemResponseDto.code}</td>
                            <td>
                                <form:form id="updateRecord" method="POST" action="${stockVisibilityUrl}/update">
                                    <input id="stockOnHand" name="soh" value="${stockVisibilityItemResponseDto.soh}" />
                                    <input id="storeNumber" name="storeNumber" type="hidden" value="${stockVisibilityItemResponseDto.storeNumber}" />
                                    <input id="itemcode" name="itemcode" type="hidden" value="${stockVisibilityItemResponseDto.code}" />
                                    <button type="submit" class="btn btn-default">Update</button>
                                </form:form>
                            </td>
                            <td>
                                <form:form id="removeRecord" method="POST" action="${stockVisibilityUrl}/remove">
                                    <input id="storeNumber" name="storeNumber" type="hidden" value="${stockVisibilityItemResponseDto.storeNumber}" />
                                    <input id="itemcode" name="itemcode" type="hidden" value="${stockVisibilityItemResponseDto.code}" />
                                    <button type="submit" class="btn btn-default">Remove</button>
                                </form:form>
                            </td>
                    </tr>
                </c:forEach>
                <tr>
                    <form:form id="addRecord" method="POST" action="${stockVisibilityUrl}/add">
                        <td><input id="storeNumber" name="storeNumber" type="text"/></td>
                        <td><input id="itemcode" name="itemcode" type="text"/></td>
                        <td><input id="soh" name="soh" type="text"/></td>
                        <td colspan="2"><button type="submit" class="btn btn-default">Add</button></td>
                    </form:form>
                </tr>
            </tbody>
        </table>
        <p> 
            <form:form id="resetRecords" method="POST" action="${stockVisibilityUrl}/reset">
                <button type="submit" class="btn btn-default">Remove all</button>
            </form:form>
        </p>
    </c:if>
</div>
</body>
</html>
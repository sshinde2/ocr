<!--
    Http session manager page
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>Http Session Manager</title>
</head>
<body>

<c:url value="${request.contextPath}/targetHttpSessionExpire.action" var="expireSession"/>
<c:url value="${request.contextPath}/targetHttpSessionSearch.action" var="searchSession"/>


<div id="status">

	<form:form id="searchSession" method="POST" action="${searchSession}">
    <input type="text" name="searchSession" value="${sessionId}"><br /> <br />
	<button type="submit">Search</button>
	</form:form>
	<c:if test="${sessionSearch}">	
		<c:choose>
			<c:when test="${not empty sessionId}">   
				<b>Session=${sessionId} was found</b> <br /><br/>
				<form:form id="expireSession" method="POST" action="${expireSession}">
				    <input type="hidden" name="expireSession" id="expireSession" value="${sessionId}" />
				    <button type="submit">Expire Session</button>
				</form:form>
				<h2>Session values</h2>
				<br/>Session Creation Time: ${sessionCreationTime}
				<br/>Session Last Accessed Time: ${sessionLastAccessedTime}
				<br/>${sessionValues}
			</c:when>
			<c:otherwise>
	            <br/>Session was not found <br/>
			</c:otherwise>
		</c:choose>
	</c:if>
	<c:if test="${isExpired}">
	   <br/><b>Session is expired </b>
	</c:if>
</div>

</body>
</html>
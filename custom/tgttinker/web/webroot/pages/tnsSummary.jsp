<!--
	Container for a categories_loop widget.  
-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head><title>TNS Mock status screen</title></head>
<body>

<c:url value="${request.contextPath}/tns.action" var="tnsurl"/>

<div id="status">
	<p>
	    <c:url value="${request.contextPath}/tns.action" var="tnsurl"/>
	    <c:choose>
			<c:when test="${tns.mockActive}">
		      Active
               <form:form id='enablemock' method="POST" action="${tnsurl}?enable=false">
                   <input type="hidden" name="enable" id="enable" value="false" />
                   <button type="submit">Disable</button>
               </form:form>
			</c:when>
			<c:otherwise>
            Inactive
              <c:if test="${tns.mockInUse}">
                    <form:form id='enablemock' method="POST" action="${tnsurl}?enable=true">
                        <button type="submit">Enable</button>
                    </form:form>
                </c:if>
		</c:otherwise>
		</c:choose>
	</p>
    <c:if test="${tns.mockActive && tns.validMockResponses != null && fn:length(tns.validMockResponses) > 0}">
        <p>
	        Current mock response is ${tns.mockStatus}<br />
	        <form:form id='enablemock' method="POST" action="${tnsurl}">
	            <select name="mockselect">
	                <c:forEach items="${tns.validMockResponses}" var="mockresponse">
	                    <option value="${mockresponse}" ${mockresponse eq tns.mockResponse ? 'selected' : ''}>${mockresponse}</option>
	                </c:forEach>
	            </select><br />
	            Times:&nbsp;<input type="number" name="times" id="times" value="0"><br />&nbsp;(0 means until it is changed explicitly)
                <select name="nextmockselect">
                    <c:forEach items="${tns.validMockResponses}" var="mockresponse">
                        <option value="${mockresponse}" ${mockresponse eq tns.mockDefault ? 'selected' : ''}>${mockresponse}</option>
                    </c:forEach>
                </select><br />
	           <button type="submit">update</button>
	        </form:form>
        </p>
        
        <p>Note the limitations of mocked TNS payments.  You can never pass this detail to TNS, ie refunds, pending retry must all be mocked</p>
    </c:if> 
</div>

</body>
</html>

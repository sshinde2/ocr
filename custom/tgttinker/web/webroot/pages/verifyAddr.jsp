<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<title>QAS Address Verification Mock status screen</title>
</head>
<body>
	<div id="status">
		<p>
			<c:choose>
				<c:when test="${mockActive}">Active <br/><br/>
				Guide: key in "ServiceNotAvailable","RequestTimeOut","TooManyMatchesFound" to simulate according scenarios, all other input will return no result.
				</c:when>
				<c:otherwise>Inactive</c:otherwise>
			</c:choose>
			<form:form imethod="POST" action="verifyAddress.action">
				<c:if test="${mockInUse}">
					<c:choose>
						<c:when test="${mockActive}">
							<input type="hidden" name="enable" value="false" />
							<button type="submit">Disable</button>
						</c:when>
						<c:otherwise>
							<input type="hidden" name="enable" value="true" />
							<button type="submit">Enable</button>
						</c:otherwise>
					</c:choose>
				</c:if>
			</form:form>

		</p>
	</div>
</body>
</html>

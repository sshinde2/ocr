IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[zonedeliverymodevalues]') AND name = N'ModeIDX_1202')
DROP INDEX [ModeIDX_1202] ON [dbo].[zonedeliverymodevalues] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[regions]') AND name = N'Region_Country_35')
DROP INDEX [Region_Country_35] ON [dbo].[regions] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[categories]') AND name = N'codeVersionIDX_142')
DROP INDEX [codeVersionIDX_142] ON [dbo].[categories]  WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cronjobs]') AND name = N'IdxJob_501')
DROP INDEX [IdxJob_501] ON [dbo].[cronjobs] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[triggerscj]') AND name = N'cronJobRelIDX_502')
DROP INDEX [cronJobRelIDX_502] ON [dbo].[triggerscj] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[promotionaction]') AND name = N'promotionResultRelDX_5015')
DROP INDEX [promotionResultRelDX_5015] ON [dbo].[promotionaction] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[joblogs]') AND name = N'cronJobRelIDX_504')
DROP INDEX [cronJobRelIDX_504] ON [dbo].[joblogs] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[catalogversions]') AND name = N'catalogRelIDX_601')
DROP INDEX [catalogRelIDX_601] ON [dbo].[catalogversions] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[promotionresult]') AND name = N'orderRelIDX_5013')
DROP INDEX [orderRelIDX_5013] ON [dbo].[promotionresult] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[savedvalues]') AND name = N'modifiedItemRelIDX_334')
DROP INDEX [modifiedItemRelIDX_334] ON [dbo].[savedvalues] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[comments]') AND name = N'reply_parent_1140')
DROP INDEX [reply_parent_1140] ON [dbo].[comments] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[comments]') AND name = N'reply_comment_1140')
DROP INDEX [reply_comment_1140] ON [dbo].[comments] WITH (ONLINE = OFF);

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[comments]') AND name = N'comment_component_1140')
DROP INDEX [comment_component_1140] ON [dbo].[comments] WITH (ONLINE = OFF);
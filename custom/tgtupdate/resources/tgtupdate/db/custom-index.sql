IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cat2prodrel]') AND name = N'IDX_cat2prodrel_qualifier_languagepk_SourcePK__INC_SequenceNumber')
CREATE NONCLUSTERED INDEX [IDX_cat2prodrel_qualifier_languagepk_SourcePK__INC_SequenceNumber] ON [dbo].[cat2prodrel] 
(
    [Qualifier] ASC,
    [languagepk] ASC,
    [SourcePK] ASC
)
INCLUDE ( [SequenceNumber]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];


IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cat2prodrel]') AND name = N'IDX_cat2prodrel_TargetPK_language_pkQualifier_INC_RSequenceNumber')
CREATE NONCLUSTERED INDEX [IDX_cat2prodrel_TargetPK_language_pkQualifier_INC_RSequenceNumber] ON [dbo].[cat2prodrel] 
(
    [TargetPK] ASC,
    [languagepk] ASC,
    [Qualifier] ASC
)
INCLUDE ( [SourcePK],
[RSequenceNumber]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];


IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cat2prodrel]') AND name = N'IDX_cat2prodrel_SourcePK_TypePkString_TargetPK_INC_PK')
CREATE NONCLUSTERED INDEX [IDX_cat2prodrel_SourcePK_TypePkString_TargetPK_INC_PK] ON [dbo].[cat2prodrel] 
(
    [SourcePK] ASC,
    [TypePkString] ASC,
    [TargetPK] ASC
)
INCLUDE ( [PK]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[stocklevelhistoryentry]') AND name = N'IDX_StockLevel_CreatedTS')
CREATE NONCLUSTERED INDEX IDX_StockLevel_CreatedTS ON dbo.stocklevelhistoryentry 
(
    [createdTS] ASC
)
WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[medias]') AND name = N'TGT_IDX_TypePkStringsourceitem')
CREATE NONCLUSTERED INDEX [TGT_IDX_TypePkStringsourceitem] ON [dbo].[medias] 
(
    [TypePkString] ASC,   
    [p_sourceitem] ASC
   
)
INCLUDE ( [PK],[createdTS]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[discounts]') AND name = N'TGT_IDX_TypePkStringVouchercode')
CREATE NONCLUSTERED INDEX [TGT_IDX_TypePkStringVouchercode] ON [dbo].[discounts] 
(
	[TypePkString] ASC,   
	[p_vouchercode] ASC
    
)
INCLUDE ( [PK]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY];


IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[promotion]') AND name = N'TGT_IDX_promotion_promotiongroup_enabled_TypePkString_enddate_startdate_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_promotion_promotiongroup_enabled_TypePkString_enddate_startdate_includes] ON [dbo].[promotion]
(
    [p_promotiongroup] ASC,
    [p_enabled] ASC,
    [TypePkString] ASC,
    [p_enddate] ASC,
    [p_startdate] ASC
   
)
INCLUDE ([PK], [p_priority]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[orders]') AND name = N'TGT_IDX_orders_originalversion_TypePkString_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_orders_originalversion_TypePkString_includes] ON [dbo].[orders]
(
    [p_originalversion] ASC,
    [TypePkString] ASC
)
INCLUDE ([PK], [userpk], [code]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[orders]') AND name = N'TGT_IDX_orders_TypePkString_statuspk_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_orders_TypePkString_statuspk_includes] ON [dbo].[orders]
(
   
    [TypePkString] ASC,
    [statuspk] ASC
)
INCLUDE ([PK]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[products]') AND name = N'TGT_IDX_products_catalogversion_TypePkString_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_products_catalogversion_TypePkString_includes] ON [dbo].[products]
(
    [p_catalogversion] ASC,
    [TypePkString] ASC
)
INCLUDE ([PK]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[processes]') AND name = N'TGT_IDX_processes_TypePkString_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_processes_TypePkString_includes] ON [dbo].[processes]
(
    [TypePkString] ASC
)
INCLUDE ([PK], [createdTS], [p_order]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[medias]') AND name = N'TGT_IDX_medias_mediacontainer_mediaformat_TypePkString_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_medias_mediacontainer_mediaformat_TypePkString_includes] ON [dbo].[medias]
(
    [p_mediacontainer] ASC,
    [p_mediaformat] ASC,
    [TypePkString] ASC
)
INCLUDE ([PK]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[orderentries]') AND name = N'TGT_IDX_orderentries_TypePkString_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_orderentries_TypePkString_includes] ON [dbo].[orderentries]
(
    [orderpk] ASC,
    [TypePkString] ASC
)
INCLUDE ([PK],[createdTS],[entrynumber])WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[medias]') AND name = N'TGT_IDX_catalogversionTypePkString')
CREATE NONCLUSTERED INDEX [TGT_IDX_catalogversionTypePkString] ON [dbo].[medias]
(
    [p_catalogversion] ASC,
    [TypePkString] ASC
)
INCLUDE ([PK],[createdTS],[code]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[carts]') AND name = N'TGT_IDX_TypePkStringUserpk')
CREATE NONCLUSTERED INDEX [TGT_IDX_TypePkStringUserpk] ON [dbo].[carts]
(
	[TypePkString] ASC,    
	[userpk] ASC
    
)
INCLUDE ([PK]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[medias]') AND name = N'TGT_IDX_medias_mediaContainer_typePkString_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_medias_mediaContainer_typePkString_includes] ON [dbo].[medias]
(
    [p_mediacontainer] ASC,
    [TypePkString] ASC
)
INCLUDE ([PK],[createdTS]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[medias]') AND name = N'TGT_IDX_medias_typePkString_ownerPkString_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_medias_typePkString_ownerPkString_includes] ON [dbo].[medias]
(
    [TypePkString] ASC,
    [OwnerPkString] ASC
)
INCLUDE ([PK],[createdTS]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[medias]') AND name = N'TGT_IDX_medias_typePkString_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_medias_typePkString_includes] ON [dbo].[medias]
(
    [TypePkString] ASC   
)
INCLUDE ([PK],[modifiedTS]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[medias]') AND name = N'TGT_IDX_medias_mediaContainer_typePkString_catalogVersion_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_medias_mediaContainer_typePkString_catalogVersion_includes] ON [dbo].[medias]
(
    [p_mediacontainer] ASC,
    [TypePkString] ASC,
    [p_catalogversion] ASC
)
INCLUDE ([PK],[createdTS]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[orders]') AND name = N'TGT_IDX_orders_createdTS_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_orders_createdTS_includes] ON [dbo].[orders]
(
   
    [createdTS] ASC
)
INCLUDE ([PK],[code],[p_originalversion]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[orderhistoryentries]') AND name = N'TGT_IDX_orderhistoryentries_orderPos_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_orderhistoryentries_orderPos_includes] ON [dbo].[orderhistoryentries]
(
   
    [p_orderpos] ASC
)
INCLUDE ([p_previousorderversion]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[consignmententries]') AND name = N'TGT_IDX_consignmententries_typePkString_consignment_includes')
CREATE NONCLUSTERED INDEX [TGT_IDX_consignmententries_typePkString_consignment_includes] ON [dbo].[consignmententries]
(
   
    [TypePkString] ASC,
    [p_consignment] ASC
)
INCLUDE ([PK],[createdTS]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[composedtypes]') AND name = N'TGT_IDX_composedtypes_internalCode')
CREATE NONCLUSTERED INDEX [TGT_IDX_composedtypes_internalCode] ON [dbo].[composedtypes]
(
   
    [InternalCode] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[cmspage]') AND name = N'TGT_IDX_cmspage_label_typePkString_catalogVersion')
CREATE NONCLUSTERED INDEX [TGT_IDX_cmspage_label_typePkString_catalogVersion] ON [dbo].[cmspage]
(
    [p_label] ASC,
    [TypePkString] ASC,
    [p_catalogversion] ASC
    
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[categories]') AND name = N'TGT_IDX_categories_fluentid')
CREATE NONCLUSTERED INDEX [TGT_IDX_categories_fluentid] ON [dbo].[categories]
(
    [p_fluentid] ASC,
    [TypePkString] ASC,
    [p_catalogversion] ASC
) WHERE p_fluentid IS NOT NULL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[products]') AND name = N'TGT_IDX_products_fluentid')
CREATE NONCLUSTERED INDEX [TGT_IDX_products_fluentid] ON [dbo].[products]
(
    [p_fluentid] ASC,
    [TypePkString] ASC,
    [p_catalogversion] ASC
) WHERE p_fluentid IS NOT NULL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[consignments]') AND name = N'TGT_IDX_TypePkString_pwarehouse')
CREATE NONCLUSTERED INDEX [TGT_IDX_TypePkString_pwarehouse] ON [dbo].[consignments] 
(
    [TypePkString] ASC,
    [p_warehouse] ASC,
    [createdTS] ASC
)
INCLUDE ([PK],[p_order]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[consignments]') AND name = N'TGT_IDX_consignments_pstatus')
CREATE NONCLUSTERED INDEX [TGT_IDX_consignments_pstatus] ON [dbo].[consignments]
(
    [p_status]
)
INCLUDE ([PK],[p_warehouse])  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,FILLFACTOR = 80) ON [PRIMARY];

/**
 * 
 */
package au.com.target.tgtupdate.dao;

import au.com.target.tgtupdate.model.TargetUpdatesModel;


/**
 * @author pratik
 *
 */
public interface TargetUpdatesDao {

    /**
     * Method to fetch TargetUpdatesModel by Id.
     * 
     * @param id
     * @return TargetUpdatesModel
     */
    TargetUpdatesModel getTargetUpdatesModelById(String id);
}

/**
 * 
 */
package au.com.target.tgtupdate.ddl;



/**
 * @author htan3
 *
 */
public interface TargetDbScriptExecutor {

    /**
     * execute custom ddl script.
     * 
     * @param scriptPath
     * @return true if the script run successfully.
     */
    boolean executeDdl(final String scriptPath);
}

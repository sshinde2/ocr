/**
 * 
 */
package au.com.target.tgtupdate.jobs;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.jobs.exceptions.CronJobAbortException;
import au.com.target.tgtcore.jobs.util.CronJobUtilityService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.sizegroup.service.TargetSizeOrderingService;
import au.com.target.tgtupdate.dao.TargetUpdateProductDao;
import au.com.target.tgtupdate.model.TargetBatchUpdateCronJobModel;


/**
 * Cronjob class to update Target Product Size and Size group
 * 
 * @author pthoma20
 *
 */
public class AssociateTargetProductSizeAndGroupCronJob extends
        AbstractJobPerformable<TargetBatchUpdateCronJobModel> {

    private static final Logger LOG = Logger.getLogger(AssociateTargetProductSizeAndGroupCronJob.class);

    private static final String ASC_PDT_SIZE_SIZEGRP = "AssociatePdtSizeAndGrpCronJob: ";

    private CronJobUtilityService cronJobUtilityService;

    private CatalogVersionService catalogVersionService;

    private String inputDataFile;

    private TargetUpdateProductDao targetUpdateProductDao;

    private TargetSizeOrderingService targetSizeOrderingService;

    @Override
    public PerformResult perform(final TargetBatchUpdateCronJobModel cronjob) {
        LOG.info(ASC_PDT_SIZE_SIZEGRP + "Started Cronjob = AssociateTargetProductSizeAndGroup");
        try {
            process(cronjob);
        }
        catch (final CronJobAbortException cronJobException) {
            LOG.error(cronJobException.getMessage());
            return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
        }
        catch (final Exception e) {
            LOG.error("Exception while executing the update Job", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        }

        LOG.info(ASC_PDT_SIZE_SIZEGRP + "Finished Cronjob = AssociateTargetProductSizeAndGroup");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    @Override
    public boolean isAbortable() {
        return true;
    }


    private void process(final TargetBatchUpdateCronJobModel cronjob)
            throws IOException, InterruptedException, CronJobAbortException {
        String line;
        int counter = 0;

        try (FileInputStream fis = new FileInputStream(inputDataFile);
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(fis, "UTF-8"))) {
            //data from file..
            final Map<String, String> sizeVariantSizeGroupMap = new HashMap<String, String>();
            while ((line = br.readLine()) != null) {
                //ignore first line - as it is the heading
                if (counter == 0) {
                    counter++;
                    continue;
                }
                // parse the line.
                parseLineToProductSizeSizeGroupDto(line, sizeVariantSizeGroupMap);

                if (sizeVariantSizeGroupMap.size() >= cronjob.getBatchSize()) {
                    processBatch(sizeVariantSizeGroupMap);
                    sizeVariantSizeGroupMap.clear();
                    checkWhetherCronJobNeedsToWaitOrIsAborted(cronjob);
                }
                counter++;
            }
            if (MapUtils.isNotEmpty(sizeVariantSizeGroupMap)) {
                processBatch(sizeVariantSizeGroupMap);
            }
        }
        catch (final UnsupportedEncodingException | FileNotFoundException e) {
            LOG.error(ASC_PDT_SIZE_SIZEGRP + " Error Obtaining File", e);
            throw e;
        }
        catch (final IOException e) {
            LOG.error(ASC_PDT_SIZE_SIZEGRP + " Error reading from File", e);
        }
    }

    private void checkWhetherCronJobNeedsToWaitOrIsAborted(final TargetBatchUpdateCronJobModel cronjob)
            throws InterruptedException, CronJobAbortException {
        //check whether there is a running sync job
        final CronJobModel runningSyncJob = cronJobUtilityService.getRunningSyncJob();
        if (runningSyncJob != null) {
            LOG.info(MessageFormat.format(ASC_PDT_SIZE_SIZEGRP + "Sync Job {0} with code {1} is active ",
                    runningSyncJob.getJob().getCode(),
                    runningSyncJob.getCode()));
            //wait for the sync job to finish
            cronJobUtilityService.waitForSyncCronJobToFinish(cronjob, runningSyncJob,
                    Long.valueOf(cronjob.getSyncJobStatusPollFrequency()).intValue());
        }

        //check whether the batch job has been aborted.
        cronJobUtilityService.checkIfCurrentJobIsAborted(cronjob);
    }

    private void processBatch(final Map<String, String> sizeVariantSizeGroupMap) {

        logBatchInfo(sizeVariantSizeGroupMap);
        final List<TargetSizeVariantProductModel> pdtsToProcess = targetUpdateProductDao
                .findSizeVariantsWhichDoesNotHaveProductSizeAssociation(
                        sizeVariantSizeGroupMap.keySet(),
                        getStagedProductCatalog());

        if (CollectionUtils.isEmpty(pdtsToProcess)) {
            LOG.info(ASC_PDT_SIZE_SIZEGRP + "No size variants found which needs to be processed");
            return;
        }
        final List<AbstractTargetVariantProductModel> pdtModelsToSave = new ArrayList<>();
        for (final TargetSizeVariantProductModel sizeVariant : pdtsToProcess) {

            final String sizeVariantCode = sizeVariant.getCode();
            //get the sizeGroup against the size.
            if (!sizeVariantSizeGroupMap.containsKey(sizeVariantCode)) {
                continue;
            }
            final String sizeGroup = sizeVariantSizeGroupMap.get(sizeVariantCode);
            TargetSizeGroupModel targetSizeGroupModel = null;
            try {
                targetSizeGroupModel = targetSizeOrderingService
                        .getTargetSizeGroupByName(sizeGroup, sizeVariantCode, true);
                if (targetSizeGroupModel == null) {
                    LOG.error(MessageFormat.format(
                            ASC_PDT_SIZE_SIZEGRP
                                    + "Error SizeGroup returned is null for sizeGroup={0}. For a product={1}",
                            sizeGroup, sizeVariantCode));
                }
            }
            catch (final Exception ex) {
                LOG.error(MessageFormat.format(
                        ASC_PDT_SIZE_SIZEGRP + "Error Getting sizeGroup={0}. For a product={1}", sizeGroup,
                        sizeVariantCode), ex);
                continue;
            }
            //get the colour variant and make the association.
            final ProductModel productModel = sizeVariant.getBaseProduct();
            if (!(productModel instanceof TargetColourVariantProductModel)) {
                LOG.error(MessageFormat.format(
                        ASC_PDT_SIZE_SIZEGRP + "Retrieved baseProduct for product={0} is not a colourvariant",
                        sizeVariantCode));
                continue;
            }

            final TargetColourVariantProductModel colourVariant = (TargetColourVariantProductModel)productModel;
            colourVariant.setSizeGroup(targetSizeGroupModel);
            pdtModelsToSave.add(colourVariant);
            final TargetProductSizeModel targetProductSize;
            try {
                targetProductSize = targetSizeOrderingService.getTargetProductSize(
                        sizeVariant.getSize(), targetSizeGroupModel, true, sizeVariantCode);
                if (targetProductSize == null) {
                    LOG.error(MessageFormat
                            .format(
                                    ASC_PDT_SIZE_SIZEGRP
                                            + "Error Getting TargetProductSizeGroup={0} - For a product={1} size={2} group={3}",
                                    targetProductSize, sizeVariantCode, sizeVariant.getSize(), sizeGroup));
                    continue;
                }
            }
            catch (final Exception ex) {
                LOG.error(MessageFormat.format(
                        ASC_PDT_SIZE_SIZEGRP
                                + "Error Getting TargetProductSizeGroup For a product={0} size={1} group={2}",
                        sizeVariantCode, sizeVariant.getSize(), sizeGroup), ex);
                continue;

            }
            sizeVariant.setProductSize(targetProductSize);
            pdtModelsToSave.add(sizeVariant);
        }

        try {
            modelService.saveAll(pdtModelsToSave);
        }
        catch (final Exception ex) {
            LOG.error(MessageFormat.format(
                    ASC_PDT_SIZE_SIZEGRP + "Error Processing Batch - between startId={0} and endId={1}",
                    pdtsToProcess.get(0).getCode(),
                    pdtsToProcess.get(pdtsToProcess.size() - 1).getCode()));
            LOG.error(ASC_PDT_SIZE_SIZEGRP + "Error Processing Batch - between {0}", ex);
            savePdtsIndividual(pdtModelsToSave);
        }
    }

    private void logBatchInfo(final Map<String, String> sizeVariantSizeGroupMap) {
        final String firstKey = (String)sizeVariantSizeGroupMap.keySet().toArray()[0];
        final String lastKey = (String)sizeVariantSizeGroupMap.keySet().toArray()[sizeVariantSizeGroupMap.size() - 1];
        LOG.info(MessageFormat.format(ASC_PDT_SIZE_SIZEGRP
                + "Starting to Process Batch For Variants From File {0} to {1} with a total of {2}",
                firstKey, lastKey, String.valueOf(sizeVariantSizeGroupMap.size())));
    }

    private void savePdtsIndividual(final List<AbstractTargetVariantProductModel> pdtModelsToSave) {
        LOG.info(ASC_PDT_SIZE_SIZEGRP + "Trying to save products individually");
        for (final AbstractTargetVariantProductModel pdtModel : pdtModelsToSave) {
            if (pdtModel == null) {
                LOG.error(ASC_PDT_SIZE_SIZEGRP + "ProductModel for saving is null");
                continue;
            }
            try {
                modelService.save(pdtModel);
            }
            catch (final Exception ex) {
                LOG.error(ASC_PDT_SIZE_SIZEGRP + "Saving Model failed for productCode=" + pdtModel.getCode(), ex);
            }
        }
    }

    private void parseLineToProductSizeSizeGroupDto(final String line,
            final Map<String, String> sizeVariantSizeGroupMap) {
        if (StringUtils.isBlank(line)) {
            LOG.error(ASC_PDT_SIZE_SIZEGRP + "Parsed line is null. line=" + line);
            return;
        }
        //The below regex is for splitting the words out based on comma and ignoring the comma which appears inside a quote.
        final String[] splitLine = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        if (splitLine == null || splitLine.length < 4) {
            LOG.debug(ASC_PDT_SIZE_SIZEGRP + " Split line is invalid. line=" + line);
            return;
        }
        final String size = formatField(splitLine[3]);
        if (StringUtils.isEmpty(size)) {
            LOG.debug(ASC_PDT_SIZE_SIZEGRP + " Skipping line as the record has no size. line=" + line);
            return;
        }
        final String sizeVariantCode = formatField(splitLine[0]);
        final String sizeGroup = formatField(splitLine[2]);
        if (StringUtils.isEmpty(sizeVariantCode) || StringUtils.isEmpty(sizeGroup)) {
            LOG.debug(ASC_PDT_SIZE_SIZEGRP + " Invalid variant code or size group. line=" + line);
            return;
        }
        sizeVariantSizeGroupMap.put(sizeVariantCode, sizeGroup);
    }

    private String formatField(final String input) {
        if (input == null) {
            return null;
        }
        //the below regex will remove the quotes from the front and back of a  field if it exists
        return StringUtils.trim(input.replaceAll("^\"|\"$", ""));
    }

    private CatalogVersionModel getStagedProductCatalog() {
        return catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION);
    }

    /**
     * @param cronJobUtilityService
     *            the cronJobUtilityService to set
     */
    @Required
    public void setCronJobUtilityService(final CronJobUtilityService cronJobUtilityService) {
        this.cronJobUtilityService = cronJobUtilityService;
    }


    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }

    /**
     * @param targetUpdateProductDao
     *            the targetUpdateProductDao to set
     */
    @Required
    public void setTargetUpdateProductDao(final TargetUpdateProductDao targetUpdateProductDao) {
        this.targetUpdateProductDao = targetUpdateProductDao;
    }

    /**
     * @param inputDataFile
     *            the inputDataFile to set
     */
    @Required
    public void setInputDataFile(final String inputDataFile) {
        this.inputDataFile = inputDataFile;
    }

    /**
     * @param targetSizeOrderingService
     *            the targetSizeOrderingService to set
     */
    @Required
    public void setTargetSizeOrderingService(final TargetSizeOrderingService targetSizeOrderingService) {
        this.targetSizeOrderingService = targetSizeOrderingService;
    }
}

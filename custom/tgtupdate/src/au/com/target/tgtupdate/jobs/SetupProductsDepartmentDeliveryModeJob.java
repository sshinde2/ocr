/**
 * 
 */
package au.com.target.tgtupdate.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.jobs.exceptions.CronJobAbortException;
import au.com.target.tgtcore.jobs.util.CronJobUtilityService;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtupdate.processor.TgtUpdateDepartmentDeliveryModeProcessor;


/**
 * This is the cron job class which will get a fixed products and calls the api for updating department and delivery
 * mode. This also enables the abort feature in case the user needs to abort the job in between.
 * 
 * @author pthoma20
 * 
 */
public class SetupProductsDepartmentDeliveryModeJob extends AbstractJobPerformable<CronJobModel> {


    private static final Logger LOG = Logger.getLogger(SetupProductsDepartmentDeliveryModeJob.class);

    private TargetProductSearchService targetProductSearchService;

    private TgtUpdateDepartmentDeliveryModeProcessor tgtUpdateDepartmentDeliveryModeProcessor;

    private int batchSize;

    private int syncJobStatusPollFrequency;

    private CronJobUtilityService cronJobUtilityService;

    /* (non-Javadoc)
     * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CronJobModel cronJob) {

        LOG.info(" CRON JOB - Update Department And Delivery Mode Started");
        try {
            performProcessJob(cronJob);
        }
        catch (final CronJobAbortException cronJobException) {
            LOG.error(cronJobException.getMessage());
            return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
        }
        catch (final Exception e) {
            LOG.error("Exception while executing the update Job", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        }

        LOG.info(" CRON JOB - Update Department And Delivery Mode Completed");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * Method fetches the products in batch and iterates over the products calling the update Department and
     * DeliveryMode api for the first list of products. Afte processing each batch checks whether the cron job has been
     * aborted as well as checks whether a sync job is running in parallel.
     * 
     * @param departmentUpdateCronJob
     * @throws CronJobAbortException
     * @throws InterruptedException
     */
    private void performProcessJob(final CronJobModel departmentUpdateCronJob) throws CronJobAbortException,
            InterruptedException {
        final int batchCursorPosition = 0;
        int batchCount = 1;
        List<TargetProductModel> targetProductModelList = null;
        //Batch Logic which will Loop and get the products in predefined batch and update them them.
        do {
            //Get all Target Products from Staged Catalog
            targetProductModelList = targetProductSearchService
                    .getAllTargetProductsWithNoDepartment(TgtCoreConstants.Catalog.OFFLINE_VERSION,
                            batchCursorPosition, batchSize);

            if (Collections.isEmpty(targetProductModelList)) {
                if (batchCount == 1) {
                    LOG.error("No Products where fetched ");
                }
                break;
            }

            LOG.info("Executing Batch number (" + (batchCount++) + ") of size - " + targetProductModelList.size());
            tgtUpdateDepartmentDeliveryModeProcessor.processDepartmentDeliveryModeUpdate(targetProductModelList);

            //check whether there is a running sync job
            final CronJobModel runningSyncJob = cronJobUtilityService.getRunningSyncJob();

            //check whether the batch job has been aborted.
            cronJobUtilityService.checkIfCurrentJobIsAborted(departmentUpdateCronJob);

            if (runningSyncJob != null) {
                LOG.info(MessageFormat.format("Sync Job {0} with code {1} is active ",
                        runningSyncJob.getJob().getCode(),
                        runningSyncJob.getCode()));
                //wait for the sync job to finish
                cronJobUtilityService.waitForSyncCronJobToFinish(departmentUpdateCronJob, runningSyncJob,
                        syncJobStatusPollFrequency);
            }
        }
        while (CollectionUtils.isNotEmpty(targetProductModelList));

    }


    @Override
    public boolean isAbortable() {
        return true;
    }

    /**
     * @param targetProductSearchService
     *            the targetProductSearchService to set
     */
    @Required
    public void setTargetProductSearchService(final TargetProductSearchService targetProductSearchService) {
        this.targetProductSearchService = targetProductSearchService;
    }


    /**
     * @param tgtUpdateDepartmentDeliveryModeProcessor
     *            the tgtUpdateDepartmentDeliveryModeProcessor to set
     */
    @Required
    public void setTgtUpdateDepartmentDeliveryModeProcessor(
            final TgtUpdateDepartmentDeliveryModeProcessor tgtUpdateDepartmentDeliveryModeProcessor) {
        this.tgtUpdateDepartmentDeliveryModeProcessor = tgtUpdateDepartmentDeliveryModeProcessor;
    }

    /**
     * @param batchSize
     *            the batchSize to set
     */
    @Required
    public void setBatchSize(final int batchSize) {
        this.batchSize = batchSize;
    }

    /**
     * @param syncJobStatusPollFrequency
     *            the syncJobStatusPollFrequency to set
     */
    @Required
    public void setSyncJobStatusPollFrequency(final int syncJobStatusPollFrequency) {
        this.syncJobStatusPollFrequency = syncJobStatusPollFrequency;
    }

    /**
     * @param cronJobUtilityService
     *            the cronJobUtilityService to set
     */
    @Required
    public void setCronJobUtilityService(final CronJobUtilityService cronJobUtilityService) {
        this.cronJobUtilityService = cronJobUtilityService;
    }

}

/**
 * 
 */
package au.com.target.tgtupdate.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.variants.model.VariantProductModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtupdate.dao.TargetUpdateProductDao;
import au.com.target.tgtupdate.model.TargetCheckedColourVariantToSetOnlineFromDateModel;
import au.com.target.tgtutility.util.TargetDateUtil;


/**
 * @author Vivek
 *
 */
public class TargetInitializeOnlineFromDateJob extends AbstractJobPerformable<CronJobModel> {

    private static final Logger LOG = Logger.getLogger(TargetInitializeOnlineFromDateJob.class);

    private int noOfDaysToDirectlyFillFromCreationDate;

    private TargetUpdateProductDao targetUpdateProductDao;

    private TargetProductSearchService targetProductSearchService;

    @Override
    public PerformResult perform(final CronJobModel cronJob) {

        final List<TargetColourVariantProductModel> colourVariants = targetUpdateProductDao
                .getColourVariantsWithNullOnlineFromDate();

        boolean isAnyProductUpdated = false;

        if (CollectionUtils.isEmpty(colourVariants)) {
            LOG.info("INFO-ONLINEFROM-INITIALIZE-COMPLETED: All products has been initilized for online from date.");
            return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
        }

        LOG.info("Starting 'TargetInitializeOnlineFromDateJob' to populate fromDate for existing products.");

        for (final TargetColourVariantProductModel variant : colourVariants) {

            final BigDecimal noOfDays = new BigDecimal(noOfDaysToDirectlyFillFromCreationDate);
            final BigDecimal ageOfProduct = new BigDecimal(getDateDiffFromCurrentAndCreationDate(variant));

            // if product is older than configured days, straight forward fill the onlineFrom
            if (ageOfProduct.compareTo(noOfDays) >= 1) {
                isAnyProductUpdated = true;
                fillFromDateOfVariantAndAllSubVariants(variant);
            }
            else {
                // otherwise use the logic to judge the newness of the product
                final boolean isProductNew = targetProductSearchService.isProductAvailableToSell(variant);
                if (isProductNew) {
                    isAnyProductUpdated = true;
                    fillFromDateOfVariantAndAllSubVariants(variant);
                }
            }

            markVariantAsProcessed(variant);
        }

        if (!isAnyProductUpdated) {
            LOG.info("No products found to initialize online from date hence not doing anything in current execution.");
        }

        LOG.info("Successfully finished 'TargetInitializeOnlineFromDateJob' to populate fromDate for existing products.");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    /**
     * Setting up the fromDate as creationDate for current colour variant and all its sub-variants.
     *
     * @param colourVariant
     *            the colour variant
     */
    private void fillFromDateOfVariantAndAllSubVariants(final TargetColourVariantProductModel colourVariant) {

        Assert.notNull(colourVariant, "Colour Variant can't be null.");

        final Date dateToSet = TargetDateUtil.getDateWithMidnightTime(colourVariant.getCreationtime());
        final Collection<VariantProductModel> subVariants = colourVariant.getVariants();

        if (CollectionUtils.isNotEmpty(subVariants)) {
            for (final VariantProductModel variant : subVariants) {
                variant.setOnlineDate(dateToSet);
                modelService.save(variant);
            }
        }

        colourVariant.setOnlineDate(dateToSet);
        modelService.save(colourVariant);

        if (LOG.isDebugEnabled()) {
            LOG.debug(MessageFormat.format("Online Date for Colour Variant Product: {0} is set to {1}",
                    colourVariant.getCode(), colourVariant.getOnlineDate()));
        }
    }

    private long getDateDiffFromCurrentAndCreationDate(final TargetColourVariantProductModel variant) {
        final BigDecimal currentTime = new BigDecimal(Calendar.getInstance().getTime().getTime());
        final BigDecimal variantCreationTime = new BigDecimal(variant.getCreationtime().getTime());
        return ((currentTime.subtract(variantCreationTime)).divide(new BigDecimal(1000 * 60 * 60 * 24), 2,
                RoundingMode.CEILING)).longValue();
    }

    private void markVariantAsProcessed(final TargetColourVariantProductModel variant) {
        final TargetCheckedColourVariantToSetOnlineFromDateModel record = modelService
                .create(TargetCheckedColourVariantToSetOnlineFromDateModel.class);
        record.setColourVariantPK(getVariantPkAsString(variant));
        modelService.save(record);
    }

    protected String getVariantPkAsString(final TargetColourVariantProductModel variant) {
        return variant.getPk().toString();
    }

    @Override
    public boolean isAbortable() {
        return true;
    }

    @Required
    public void setTargetUpdateProductDao(final TargetUpdateProductDao targetUpdateProductDao) {
        this.targetUpdateProductDao = targetUpdateProductDao;
    }

    @Required
    public void setTargetProductSearchService(final TargetProductSearchService targetProductSearchService) {
        this.targetProductSearchService = targetProductSearchService;
    }

    @Required
    public void setNoOfDaysToDirectlyFillFromCreationDate(final int noOfDaysToDirectlyFillFromCreationDate) {
        this.noOfDaysToDirectlyFillFromCreationDate = noOfDaysToDirectlyFillFromCreationDate;
    }
}

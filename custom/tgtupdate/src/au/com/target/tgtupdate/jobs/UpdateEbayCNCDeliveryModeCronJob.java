/**
 * 
 */
package au.com.target.tgtupdate.jobs;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.fest.util.Collections;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.jobs.exceptions.CronJobAbortException;
import au.com.target.tgtcore.jobs.util.CronJobUtilityService;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtupdate.dao.TargetUpdateProductDao;
import au.com.target.tgtupdate.model.TargetBatchUpdateCronJobModel;


/**
 * Cron job class to update ebay cnc delivery mode on products which are available for ebay and has cnc delivery mode
 * 
 * @author siddharam
 *
 */
public class UpdateEbayCNCDeliveryModeCronJob extends
        AbstractJobPerformable<TargetBatchUpdateCronJobModel> {

    private static final Logger LOG = Logger.getLogger(UpdateEbayCNCDeliveryModeCronJob.class);


    private static final String CNC_DELIVERY_MODE = "click-and-collect";

    private static final String EBAY_CNC_DELIVERY_MODE = "ebay-click-and-collect";

    private TargetUpdateProductDao targetUpdateProductDao;

    private CronJobUtilityService cronJobUtilityService;

    private TargetDeliveryService targetDeliveryService;

    private CatalogVersionService catalogVersionService;

    @Override
    public PerformResult perform(final TargetBatchUpdateCronJobModel cronjob) {
        LOG.info("UpdateEbayCNCDeliveryModeCronJob:updating ebay cnc delivery mode on products");
        try {
            processJob(cronjob);
        }
        catch (final CronJobAbortException cronJobException) {
            LOG.error(cronJobException.getMessage());
            return new PerformResult(CronJobResult.UNKNOWN, CronJobStatus.ABORTED);
        }
        catch (final Exception e) {
            LOG.error("Exception while executing the update Job", e);
            return new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
        }

        LOG.info(" UpdateEbayCNCDeliveryModeCronJob: Update Ebay CNC Delivery Mode Completed");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    @Override
    public boolean isAbortable()
    {
        return true;
    }

    /**
     * @param cronjob
     * @throws CronJobAbortException
     * @throws InterruptedException
     */
    private void processJob(final TargetBatchUpdateCronJobModel cronjob) throws CronJobAbortException,
            InterruptedException {
        int batchCount = 1;
        List<TargetProductModel> targetProductModelList = null;
        int countSaveFailedProducts = 0;
        final DeliveryModeModel ebayCncDeliverMode = targetDeliveryService
                .getDeliveryModeForCode(EBAY_CNC_DELIVERY_MODE);
        //Batch Logic which will Loop and get the products in predefined batch and update them.
        do {
            targetProductModelList = targetUpdateProductDao.getCncAndEbayAvailableProducts(CNC_DELIVERY_MODE,
                    EBAY_CNC_DELIVERY_MODE,
                    cronjob.getBatchSize(), catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                            TgtCoreConstants.Catalog.OFFLINE_VERSION));

            if (Collections.isEmpty(targetProductModelList)) {
                if (batchCount == 1) {
                    LOG.error("UpdateEbayCNCDeliveryModeCronJob:No Products where fetched ");
                }
                break;
            }
            LOG.info("Executing Batch number (" + (batchCount++) + ") of size - " + targetProductModelList.size());
            countSaveFailedProducts = updateEbayCNCDeliveryMode(targetProductModelList, ebayCncDeliverMode);

            if (countSaveFailedProducts == targetProductModelList.size()) {
                LOG.error("UpdateEbayCNCDeliveryModeCronJob:Terminating cronjob as returned products"
                        + " can not be saved due to model saving issues");
                break;
            }
            //check whether there is a running sync job
            final CronJobModel runningSyncJob = cronJobUtilityService.getRunningSyncJob();

            //check whether the batch job has been aborted.
            cronJobUtilityService.checkIfCurrentJobIsAborted(cronjob);

            if (runningSyncJob != null) {
                LOG.info(MessageFormat.format("Sync Job {0} with code {1} is active ",
                        runningSyncJob.getJob().getCode(),
                        runningSyncJob.getCode()));
                //wait for the sync job to finish
                cronJobUtilityService.waitForSyncCronJobToFinish(cronjob, runningSyncJob,
                        Long.valueOf(cronjob.getSyncJobStatusPollFrequency()).intValue());
            }
        }
        while (CollectionUtils.isNotEmpty(targetProductModelList));
    }

    /**
     * Update ebay-cnc delivery mode
     * 
     * @param targetProductModelList
     * @param ebayCncDeliverMode
     * @return save failure counts
     */
    private int updateEbayCNCDeliveryMode(final List<TargetProductModel> targetProductModelList,
            final DeliveryModeModel ebayCncDeliverMode) {
        int countSaveFailures = 0;
        for (final TargetProductModel productModel : targetProductModelList) {
            if (CollectionUtils.isNotEmpty(productModel.getDeliveryModes())) {
                final Set<DeliveryModeModel> deliverModes = new HashSet<>();
                deliverModes.addAll(productModel.getDeliveryModes());
                deliverModes.add(ebayCncDeliverMode);
                try {
                    productModel.setDeliveryModes(deliverModes);
                    modelService.save(productModel);
                }
                catch (final Exception e) {
                    countSaveFailures++;
                    LOG.error("UpdateEbayCNCDeliveryModeCronJob: Exception occured while saving the product:"
                            + productModel.getCode()
                            + " and exception is :" + e.getMessage());
                }
            }
        }
        return countSaveFailures;
    }

    /**
     * @param targetUpdateProductDao
     *            the targetUpdateProductDao to set
     */
    @Required
    public void setTargetUpdateProductDao(final TargetUpdateProductDao targetUpdateProductDao) {
        this.targetUpdateProductDao = targetUpdateProductDao;
    }

    /**
     * @param cronJobUtilityService
     *            the cronJobUtilityService to set
     */
    @Required
    public void setCronJobUtilityService(final CronJobUtilityService cronJobUtilityService) {
        this.cronJobUtilityService = cronJobUtilityService;
    }

    /**
     * @param targetDeliveryService
     *            the targetDeliveryService to set
     */
    @Required
    public void setTargetDeliveryService(final TargetDeliveryService targetDeliveryService) {
        this.targetDeliveryService = targetDeliveryService;
    }

    /**
     * @param catalogVersionService
     *            the catalogVersionService to set
     */
    @Required
    public void setCatalogVersionService(final CatalogVersionService catalogVersionService) {
        this.catalogVersionService = catalogVersionService;
    }
}

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 * 
 */
package au.com.target.tgtupdate.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtupdate.constants.TgtupdateConstants;
import au.com.target.tgtupdate.dao.TargetUpdatesDao;
import au.com.target.tgtupdate.ddl.TargetDbScriptExecutor;
import au.com.target.tgtupdate.model.TargetUpdatesModel;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtupdateConstants.EXTENSIONNAME)
public class UpdateSystemSetup extends AbstractSystemSetup {

    @Resource
    private ModelService modelService;

    @Resource
    private TargetDbScriptExecutor targetDbScriptExecutor;

    @Resource
    private TargetUpdatesDao targetUpdatesDao;

    @Resource
    private ConfigurationService configurationService;

    @Resource
    private TargetSharedConfigService targetSharedConfigService;

    private String sharedConfigurationEntries;

    /**
     * This method will be called by system creator during initialisation and system update. Be sure that this method
     * can be called repeatedly.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
    public void createEssentialData(final SystemSetupContext context) {
        createCustomIndexes();
        updateTargetSharedConfig();
    }


    protected void updateTargetSharedConfig() {
        if (StringUtils.isNotBlank(sharedConfigurationEntries)) {
            final List<String> sharedConfigs = Arrays.asList(StringUtils.split(sharedConfigurationEntries, ','));
            final Map sharedConfigEntries = new HashMap<String, String>();
            for (final String eachSharedConfig : sharedConfigs) {
                final String sharedConfigKey = configurationService.getConfiguration().getString(eachSharedConfig);
                if (StringUtils.isNotBlank(sharedConfigKey)) {
                    sharedConfigEntries.put(eachSharedConfig, sharedConfigKey);
                }
                else {
                    sharedConfigEntries.put(eachSharedConfig, null);
                }
            }
            targetSharedConfigService.setSharedConfigs(sharedConfigEntries);
        }


    }

    /**
     * This method will be called during the system update only.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.UPDATE)
    public void createProjectData(final SystemSetupContext context) {
        boolean isContentCatalogSyncRequired = false;
        final boolean isProductCatalogSyncRequired = false;
        final boolean isValidationEngineReloadRequired = false;

        importImpex(context, "OCR-19039 display afterpayPaymentInfo in cscockpit",
                "/tgtcs/import/impex/projectdata_tgtcs_ui_components.impex");

        importImpex(context, "OCR-17560 feature switch for product end of life cross sell",
                "/tgtupdate/featureswitch/feature_productendoflife_crosssell.impex");
        importImpex(context, "OCR-17560 shared config for product end of life cross sell items per page",
                "/tgtupdate/sharedconfig/productendoflife_crosssell_itemsperpage.impex");
        importImpex(context, "OCR-19110 feature switch for australia post delivery club",
                "/tgtupdate/featureswitch/feature-aus-post-delivery-club.impex");

        importImpex(context, "OMS-14 Ebay Click and Collect sales application config update",
                "/tgtupdate/salesapplicationconfig/update-ebay-sales-application-config.impex");

        importImpex(context,
                "OMS-14 Ebay Click and Collect target point of service eligibleForEbayClickAndCollect flag update",
                "/tgtupdate/targetpointofservice/update-eligibleForEbayClickAndCollect-flag.impex");

        importImpex(context, "OCR-19110 rename delivery club to shipster",
                "/tgtupdate/featureswitch/feature-rename-to-shipster.impex");

        importImpex(context, "OCR-19211 feature switch for FIS2 store stock",
                "/tgtupdate/featureswitch/feature_fis_store_stock_visibility.impex");

        importImpex(context, "OMS-28 batch job to feed stores to fluent",
                "/tgtupdate/fluent/location-feed-job.impex");

        importImpex(context, "OCR-19426 cronjob for extract consignments",
                "/tgtupdate/jobs/targetExtractUnshippedConsignmentsJob.impex");

        importImpex(context, "OCR-19426 Changed the cronjob trigger ",
                "/tgtupdate/physicalgiftcard/physical-giftcard-cronjob-trigger.impex");

        importImpex(context, "OMS-80 job to check status of location batch updates to Fluent",
                "/tgtupdate/fluent/location-feed-status-check-job.impex");

        importImpex(context,
                "OCR-20304 update express delivery fee for preOrders",
                "/tgtupdate/preorder/preOrder-delivery-fee.impex");

        if (importImpex(context, "OCR-20258: Create content slot for scripts on SPC",
                "/tgtupdate/content/spc-create-script-slot.impex")) {
            isContentCatalogSyncRequired = true;
        }

        importImpex(context, "OMS-329 add apigateway usergroup and user",
                "/tgtupdate/fluent/apigatewaygroup.impex");

        importImpex(context, "OCR-20433 Remove old auspost notification",
                "/tgtupdate/featureswitch/remove-old-auspost-notification.impex");

        importImpex(context, "OMS-347 Ensure Cancel is only available to authorised managers",
                "/tgtupdate/user-groups/cs-usergroup.impex");

        importImpex(context, "OCR-20550 Create the Cronjob to pick the parked preorders",
                "/tgtupdate/fulfilment/preOrderFulfilment-job-triggers.impex");

        importImpex(context, "OMS-590 add fluentLocationRef to warehouseModel",
                "/tgtupdate/fluent/warehouse-fluentLocationRef.impex");

        importImpex(context, "NOCR update onlinePos address for routing",
                "/tgtupdate/featureswitch/updateOnlinePosAddress.impex");

        importImpex(context, "OMS-584 add parcelCount & statusDate to consignment in cscockpit",
                "/tgtcs/import/impex/projectdata_tgtcs_ui_components.impex");

        importImpex(context,
                "OCR-20664 creating Zip payment feature switch",
                "/tgtupdate/featureswitch/feature-payment-zip.impex");

        if (importImpex(context, "OCR-20810 Add zip payment modal",
                "/tgtupdate/content/zippayment-modal.impex")) {
            isContentCatalogSyncRequired = true;
        }

        if (importImpex(context, "OCR-20776 Add zip-payment SPC modal (MoreInfo)",
                "/tgtupdate/content/zippayment-spc-modal.impex")) {
            isContentCatalogSyncRequired = true;
        }

        importImpex(context, "OCR-20741 CS cockpit changes to show zip payment details",
                "/tgtcs/import/impex/projectdata_tgtcs_ui_components.impex");

        importImpex(context, "OCR-21046 add zip payment mode",
                "/tgtupdate/payment/zippay.impex");

        importImpex(context, "OCR-20774 CS cockpit changes to show product id under consignments",
                "/tgtcs/import/impex/projectdata_tgtcs_ui_components.impex");

        importImpex(context, "OCR-20874 Fix operator precedence in the orderProcessCleanupJob",
                "/tgtupdate/jobs/orderProcessCleanupJob.impex");

        importImpex(context, "OCR-21227 Toll consignment extract constants",
                "/tgtupdate/sharedconfig/toll-consignment-extract-constants.impex");

        importImpex(context,
                "OCR-21209 Allow custom dimensions for bulky warehouses in OFC",
                "/tgtupdate/sharedconfig/big-and-bulky-dimensions-config.impex");

        importImpex(context, "OMS-435 job to update products online date",
                "/tgtupdate/fluent/update-product-online-date-job.impex");

        importImpex(context, "OMS-958 Fail When Stock Check Errors",
                "/tgtupdate/featureswitch/feature-error-on-stockCheckFailure.impex");

        importImpex(context, "OCR-21189 IPG Credential on file change",
                "/tgtupdate/featureswitch/feature-ipg-credential-on-file.impex");

        importImpex(context, "OCR-21325 Auspos manifest file optimization",
                "/tgtupdate/featureswitch/feature-auspost-manifest-optimization.impex");

        importImpex(context, "OMS-996 diplay shippit tracking id in cs cockpit",
                "/tgtcs/import/impex/projectdata_tgtcs_ui_components.impex");

        // add new impex updates before this line

        if (isContentCatalogSyncRequired) {
            executeCatalogSyncJob(context, "targetContentCatalog");
        }

        if (isProductCatalogSyncRequired) {
            executeCatalogSyncJob(context, "targetProductCatalog");
        }

        if (isValidationEngineReloadRequired) {
            final ValidationService validationService = Registry.getApplicationContext().getBean(
                    "validationService", ValidationService.class);
            validationService.reloadValidationEngine();
        }

        // do not add any new impex here, add it above the previous comment
    }

    /**
     * This method will first check if impexId already exists, if it doesn't exist then it will insert the impexId and
     * import the impex file and return a true. If impex is not imported then it will return false.
     * 
     * @param context
     * @param id
     * @param impexLocation
     * @return boolean, true - if impex is imported, false - if impex is not imported.
     */
    protected boolean importImpex(final SystemSetupContext context, final String id, final String impexLocation) {
        TargetUpdatesModel updatesModel = targetUpdatesDao.getTargetUpdatesModelById(id);
        if (updatesModel == null) {
            updatesModel = modelService.create(TargetUpdatesModel.class);
            updatesModel.setUpdateName(id);
            updatesModel.setDeploymentDate(new Date());
            importImpexFile(context, impexLocation);
            modelService.save(updatesModel);
            logInfo(context, "Created Target Update for update: '" + id + "' and imported impex from: '"
                    + impexLocation + "'");
            return true;
        }
        return false;
    }

    /**
     * Method to create custom indexes
     */
    private void createCustomIndexes() {
        targetDbScriptExecutor.executeDdl("/tgtupdate/db/custom-index.sql");
        targetDbScriptExecutor.executeDdl("/tgtupdate/db/custom-drop-indexes.sql");
    }

    /**
     * No initialization options required.
     */
    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        return null;
    }


    /**
     * @param sharedConfigurationEntries
     *            the sharedConfigurationEntries to set
     */
    public void setSharedConfigurationEntries(final String sharedConfigurationEntries) {
        this.sharedConfigurationEntries = sharedConfigurationEntries;
    }

}

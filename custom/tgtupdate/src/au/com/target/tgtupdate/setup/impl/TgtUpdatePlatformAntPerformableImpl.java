/**
 * 
 */
package au.com.target.tgtupdate.setup.impl;

import de.hybris.ant.taskdefs.UpdatePlatformAntPerformableImpl;
import de.hybris.platform.util.JspContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;


/**
 * @author pthoma20
 *
 *         This class will be responsible for updating the specified extensions while the system update runs The
 *         extensions are configurable in the buildcallbacks.xml for tgtupdate.
 */
public class TgtUpdatePlatformAntPerformableImpl extends UpdatePlatformAntPerformableImpl {

    private final Logger log = Logger.getLogger(TgtUpdatePlatformAntPerformableImpl.class);

    private List<String> selectedExtensionsForUpdate;


    public TgtUpdatePlatformAntPerformableImpl(final String tenantID)
    {
        super(tenantID);
    }

    public TgtUpdatePlatformAntPerformableImpl()
    {
        super();
    }

    @Override
    protected JspContext createJspContext()
    {
        log.info("TGTUPDATESYSTEM- Started Target update System");
        final Map<String, String> requestParams = new HashMap<String, String>();
        requestParams.put("init", "Go");
        requestParams.put("initmethod", "update");
        requestParams.put("clearhmc", "true");
        requestParams.put("essential", "true");
        requestParams.put("localizetypes", "true");

        if (CollectionUtils.isNotEmpty(selectedExtensionsForUpdate)) {
            createProjectDataForExtensions(requestParams);
        }

        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameters(requestParams);

        final MockHttpServletResponse response = new MockHttpServletResponse();
        return new JspContext(null, request, response);
    }

    private void createProjectDataForExtensions(final Map<String, String> requestParams) {
        for (final String tgtExtensionForUpdate : selectedExtensionsForUpdate) {
            if (StringUtils.isBlank(tgtExtensionForUpdate)) {
                continue;
            }
            requestParams.put((new StringBuilder(tgtExtensionForUpdate)).append("_")
                    .append("sample").toString(), "true");
            log.info("TGTUPDATESYSTEM- Extension Added" + tgtExtensionForUpdate);
        }
    }

    /**
     * @param selectedExtensionsForUpdate
     *            the selectedExtensionsForUpdate to set
     */
    @Required
    public void setSelectedExtensionsForUpdate(final List<String> selectedExtensionsForUpdate) {
        this.selectedExtensionsForUpdate = selectedExtensionsForUpdate;
    }



}

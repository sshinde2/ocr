/**
 * 
 */
package au.com.target.tgtupdate.ddl.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDbScriptExecutorImplTest {

    @InjectMocks
    private final TargetDbScriptExecutorImpl targetDbScriptExecutor = new TargetDbScriptExecutorImpl();

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Test
    public void testExecuteDdl() {
        final String[] s = { "1", "2" };

        final boolean status = targetDbScriptExecutor.executeDdl("/test/test-create2index.sql");

        assertTrue(status);
        verify(jdbcTemplate).batchUpdate(s);
    }

    @Test
    public void testExecuteDdlWithError() {
        final String[] s = { "1", "2" };
        final DataAccessException dataAccessException = mock(DataAccessException.class);
        when(jdbcTemplate.batchUpdate(s)).thenThrow(dataAccessException);

        final boolean status = targetDbScriptExecutor.executeDdl("/test/test-create2index.sql");

        assertFalse(status);
    }

    @Test
    public void testExecuteDdlWithEmptyArray() {
        final boolean status = targetDbScriptExecutor.executeDdl("/test/test-create0index.sql");

        verifyZeroInteractions(jdbcTemplate);
        assertFalse(status);
    }

    @Test
    public void testExecuteDdlWithNonExistFile() {
        final boolean status = targetDbScriptExecutor.executeDdl("/test/non-exist.sql");

        verifyZeroInteractions(jdbcTemplate);
        assertFalse(status);
    }

}

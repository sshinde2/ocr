/**
 * 
 */
package au.com.target.tgtupdate.jobs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants.Enumerations.CronJobStatus;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.jobs.exceptions.CronJobAbortException;
import au.com.target.tgtcore.jobs.util.CronJobUtilityService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.sizegroup.service.TargetSizeOrderingService;
import au.com.target.tgtupdate.dao.TargetUpdateProductDao;
import au.com.target.tgtupdate.model.TargetBatchUpdateCronJobModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AssociateTargetProductSizeAndGroupCronJobTest {

    @Mock
    private CronJobUtilityService cronJobUtilityService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private TargetUpdateProductDao targetUpdateProductDao;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetSizeOrderingService targetSizeOrderingService;

    @InjectMocks
    private final AssociateTargetProductSizeAndGroupCronJob job = new AssociateTargetProductSizeAndGroupCronJob();

    private CatalogVersionModel stagedCatalogVersion;

    private final TargetBatchUpdateCronJobModel targetBatchUpdate = Mockito.mock(TargetBatchUpdateCronJobModel.class);

    private TargetSizeGroupModel targetSizeGroupWomenModel;

    private Map<String, TargetProductSizeModel> productSizeMap;


    @Before
    public void setup() {
        stagedCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        BDDMockito.given(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).willReturn(stagedCatalogVersion);
        BDDMockito.given(Integer.valueOf(targetBatchUpdate.getBatchSize())).willReturn(Integer.valueOf(50));

        final IllegalArgumentException exception = new IllegalArgumentException();
        BDDMockito.given(targetSizeOrderingService.getTargetSizeGroupByName("mens size", "P1002_Black_M", true))
                .willThrow(exception);

        BDDMockito.given(targetSizeOrderingService.getTargetSizeGroupByName("mens size", "P1009_King", true))
                .willThrow(exception);

        BDDMockito.given(targetSizeOrderingService.getTargetSizeGroupByName("mens size", "P1009_Queen", true))
                .willThrow(exception);

        targetSizeGroupWomenModel = Mockito.mock(TargetSizeGroupModel.class);
        BDDMockito.given(targetSizeOrderingService.getTargetSizeGroupByName("womens size", "P1000", true))
                .willReturn(targetSizeGroupWomenModel);

        BDDMockito.given(targetSizeOrderingService.getTargetSizeGroupByName("womens size", "P1000_Black_L", true))
                .willReturn(targetSizeGroupWomenModel);

        BDDMockito.given(targetSizeOrderingService.getTargetSizeGroupByName("womens size", "P1000_Black_M", true))
                .willReturn(targetSizeGroupWomenModel);

        BDDMockito.given(targetSizeOrderingService.getTargetSizeGroupByName("womens size", "P1000_Black_S", true))
                .willReturn(targetSizeGroupWomenModel);

        BDDMockito.given(targetSizeOrderingService.getTargetSizeGroupByName("womens size", "P1001_Blue_M", true))
                .willReturn(targetSizeGroupWomenModel);

        setupProductSize();

    }

    @Test
    public void testParseWhenFileIsMissing() {
        job.setInputDataFile("Dummy");
        final PerformResult performResult = job.perform(targetBatchUpdate);
        Assertions.assertThat(performResult.getResult().getCode()).isEqualTo(CronJobResult.ERROR.getCode());
        Assertions.assertThat(performResult.getStatus().getCode()).isEqualTo(CronJobStatus.FINISHED);
    }


    @Test
    public void testParseWhenCronJobIsAborted() throws CronJobAbortException {
        job.setInputDataFile(getTestFilePath());
        BDDMockito.given(Integer.valueOf(targetBatchUpdate.getBatchSize())).willReturn(Integer.valueOf(2));
        BDDMockito.doThrow(new CronJobAbortException("Aborted")).when(cronJobUtilityService)
                .checkIfCurrentJobIsAborted(targetBatchUpdate);

        final PerformResult performResult = job.perform(targetBatchUpdate);

        Assertions.assertThat(performResult.getResult().getCode()).isEqualTo(CronJobResult.UNKNOWN.getCode());
        Assertions.assertThat(performResult.getStatus().getCode()).isEqualTo(CronJobStatus.ABORTED);
    }

    @Test
    public void testBatchExecution() throws CronJobAbortException {
        job.setInputDataFile(getTestFilePath());
        BDDMockito.given(Integer.valueOf(targetBatchUpdate.getBatchSize())).willReturn(Integer.valueOf(2));

        job.perform(targetBatchUpdate);
        Mockito.verify(targetUpdateProductDao, Mockito.times(3))
                .findSizeVariantsWhichDoesNotHaveProductSizeAssociation(
                        Mockito.any(Set.class),
                        Mockito.any(CatalogVersionModel.class));
    }

    @Test
    public void testCronJobWaitForCatalogSyncIsInvoked() throws CronJobAbortException, InterruptedException

    {
        job.setInputDataFile(getTestFilePath());
        BDDMockito.given(Integer.valueOf(targetBatchUpdate.getBatchSize())).willReturn(Integer.valueOf(2));
        BDDMockito.given(Long.valueOf(targetBatchUpdate.getSyncJobStatusPollFrequency()))
                .willReturn(Long.valueOf(1000L));
        final CronJobModel runningSyncJob = Mockito.mock(CronJobModel.class);
        final JobModel syncJobModel = Mockito.mock(JobModel.class);
        BDDMockito.given(runningSyncJob.getJob()).willReturn(syncJobModel);
        BDDMockito.given(cronJobUtilityService.getRunningSyncJob()).willReturn(runningSyncJob);
        BDDMockito.given(syncJobModel.getCode()).willReturn("222");
        job.perform(targetBatchUpdate);
        Mockito.verify(cronJobUtilityService, Mockito.times(3)).waitForSyncCronJobToFinish(targetBatchUpdate,
                runningSyncJob, 1000);
    }


    @Test
    public void testParseWhetherInvalidRecordsAreOmitted() {

        job.setInputDataFile(getTestFilePath());

        final ArgumentCaptor<HashSet> productCodes = ArgumentCaptor
                .forClass(HashSet.class);
        final ArgumentCaptor<CatalogVersionModel> catalogVersion = ArgumentCaptor
                .forClass(CatalogVersionModel.class);

        job.perform(targetBatchUpdate);


        Mockito.verify(targetUpdateProductDao).findSizeVariantsWhichDoesNotHaveProductSizeAssociation(
                productCodes.capture(),
                catalogVersion.capture());

        final Set validProducts = productCodes.getValue();
        final Map<String, String> validCodes = getListOfValidProducts();

        validProducts.containsAll(validCodes.keySet());
        Assertions.assertThat(validProducts).hasSize(6);

    }


    @Test
    public void testSaveTargetProductSizeWhenOneOfTheGroupErrors() {

        job.setInputDataFile(getTestFilePath());

        final Map<String, String> productsFound = getListOfValidProducts();
        final List<TargetSizeVariantProductModel> listOfProducts = getSizeVariantsModel(productsFound);
        BDDMockito.given(
                targetUpdateProductDao.findSizeVariantsWhichDoesNotHaveProductSizeAssociation(productsFound.keySet(),
                        stagedCatalogVersion))
                .willReturn(listOfProducts);

        final ArgumentCaptor<ArrayList> pdtsBeingSaved = ArgumentCaptor
                .forClass(ArrayList.class);

        job.perform(targetBatchUpdate);

        Mockito.verify(modelService).saveAll(pdtsBeingSaved.capture());
        final List<AbstractTargetVariantProductModel> productsSaved = pdtsBeingSaved.getValue();

        Assertions.assertThat(productsSaved).isNotNull().isNotEmpty().hasSize(8);

        for (final AbstractTargetVariantProductModel pdtSaved : productsSaved) {
            if (pdtSaved instanceof TargetColourVariantProductModel) {
                Assertions.assertThat(((TargetColourVariantProductModel)pdtSaved).getSizeGroup())
                        .isEqualTo(targetSizeGroupWomenModel);
            }
            else {
                final TargetSizeVariantProductModel sizeVariant = (TargetSizeVariantProductModel)pdtSaved;
                Assertions.assertThat(sizeVariant.getProductSize())
                        .isEqualTo(productSizeMap.get(sizeVariant.getSize()));
            }
        }

    }

    @Test
    public void testSaveTargetProductSizeWhenOneOfTheTargetProductSizeAndGroupCreationErrors() {

        job.setInputDataFile(getTestFilePath());

        final Map<String, String> productsFound = getListOfValidProducts();
        final List<TargetSizeVariantProductModel> listOfProducts = getSizeVariantsModel(productsFound);
        BDDMockito.given(
                targetUpdateProductDao.findSizeVariantsWhichDoesNotHaveProductSizeAssociation(productsFound.keySet(),
                        stagedCatalogVersion))
                .willReturn(listOfProducts);

        final ArgumentCaptor<ArrayList> pdtsBeingSaved = ArgumentCaptor
                .forClass(ArrayList.class);


        BDDMockito.given(targetSizeOrderingService
                .getTargetProductSize("S", targetSizeGroupWomenModel, true, "P1000_Black_S"))
                .willThrow(new IllegalArgumentException());
        productSizeMap.remove("s");

        job.perform(targetBatchUpdate);

        Mockito.verify(modelService).saveAll(pdtsBeingSaved.capture());
        final List<AbstractTargetVariantProductModel> productsSaved = pdtsBeingSaved.getValue();

        Assertions.assertThat(productsSaved).isNotNull().isNotEmpty().hasSize(7);

    }


    private String getTestFilePath() {
        String currentLocation = System.getProperty("user.dir");
        if (StringUtils.contains(currentLocation, "platform")) {
            currentLocation = currentLocation + "/../custom/tgtupdate/";
        }
        return currentLocation + "/resources/test/test-step-pdtsizegrp-dataFile.csv";
    }

    private Map<String, String> getListOfValidProducts() {
        final Map<String, String> validCodesWithSize = new HashMap<String, String>();
        validCodesWithSize.put("P1000_Black_L", "L");
        validCodesWithSize.put("P1000_Black_M", "M");
        validCodesWithSize.put("P1000_Black_S", "S");
        validCodesWithSize.put("P1001_Blue_M", "M");
        validCodesWithSize.put("P1002_Black_M", "M");
        validCodesWithSize.put("P1009_Queen", "Queen");
        return validCodesWithSize;
    }

    private List<TargetSizeVariantProductModel> getSizeVariantsModel(final Map<String, String> sizeVariants) {
        final List<TargetSizeVariantProductModel> sizeVariantsList = new ArrayList<>();
        final TargetColourVariantProductModel colourVariantModel = new TargetColourVariantProductModel();
        colourVariantModel.setCode("P1000_black");
        for (final String sizeVariant : sizeVariants.keySet()) {
            final TargetSizeVariantProductModel sizeVariantModel = new TargetSizeVariantProductModel();
            sizeVariantModel.setCode(sizeVariant);
            sizeVariantModel.setBaseProduct(colourVariantModel);
            sizeVariantModel.setSize(sizeVariants.get(sizeVariant));
            sizeVariantsList.add(sizeVariantModel);
        }

        return sizeVariantsList;
    }

    private void setupProductSize() {
        productSizeMap = new HashMap<String, TargetProductSizeModel>();
        final TargetProductSizeModel productSizeWomenL = Mockito.mock(TargetProductSizeModel.class);
        BDDMockito.given(targetSizeOrderingService
                .getTargetProductSize("L", targetSizeGroupWomenModel, true, "P1000_Black_L"))
                .willReturn(productSizeWomenL);
        productSizeMap.put("L", productSizeWomenL);
        final TargetProductSizeModel productSizeWomenM = Mockito.mock(TargetProductSizeModel.class);
        BDDMockito.given(targetSizeOrderingService
                .getTargetProductSize("M", targetSizeGroupWomenModel, true, "P1000_Black_M"))
                .willReturn(productSizeWomenM);
        productSizeMap.put("M", productSizeWomenM);
        BDDMockito.given(targetSizeOrderingService
                .getTargetProductSize("M", targetSizeGroupWomenModel, true, "P1001_Blue_M"))
                .willReturn(productSizeWomenM);
        productSizeMap.put("M", productSizeWomenM);
        final TargetProductSizeModel productSizeWomenS = Mockito.mock(TargetProductSizeModel.class);
        BDDMockito.given(targetSizeOrderingService
                .getTargetProductSize("S", targetSizeGroupWomenModel, true, "P1000_Black_S"))
                .willReturn(productSizeWomenS);
        productSizeMap.put("S", productSizeWomenS);

    }

}

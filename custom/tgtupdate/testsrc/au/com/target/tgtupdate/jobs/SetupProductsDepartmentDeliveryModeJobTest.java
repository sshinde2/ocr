/**
 * 
 */
package au.com.target.tgtupdate.jobs;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.cronjob.model.JobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.jobs.exceptions.CronJobAbortException;
import au.com.target.tgtcore.jobs.util.CronJobUtilityService;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtcore.util.TargetProductMockHelper;
import au.com.target.tgtupdate.processor.TgtUpdateDepartmentDeliveryModeProcessor;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SetupProductsDepartmentDeliveryModeJobTest {


    @Mock
    private TargetProductSearchService targetProductSearchService;

    @Mock
    private ModelService modelService;

    @Captor
    private ArgumentCaptor<List<TargetProductModel>> argCaptor;

    @InjectMocks
    private final SetupProductsDepartmentDeliveryModeJob setupProductsDepartmentDeliveryModeJob = new SetupProductsDepartmentDeliveryModeJob();

    @Mock
    private TgtUpdateDepartmentDeliveryModeProcessor tgtUpdateDepartmentDeliveryModeProcessor;

    @Mock
    private CronJobUtilityService cronJobUtilityService;


    private final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);

    private TargetProductMockHelper targetProductMockHelper;

    private final List<TargetProductModel> targetProductModelList = new ArrayList<>();


    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        targetProductMockHelper = new TargetProductMockHelper(catalogVersionModel);


        TargetProductModel tgtProductModel = null;
        for (int i = 0; i < 50; i++) {
            tgtProductModel = targetProductMockHelper.createTargetProduct(Integer.valueOf(457),
                    "BP" + i, 0, 2, null,
                    Boolean.TRUE,
                    Boolean.TRUE);
            targetProductModelList.add(tgtProductModel);
        }
    }

    /**
     * Test method for {@link au.com.target.tgtupdate.jobs.SetupProductsDepartmentDeliveryModeJob#perform(CronJobModel)}
     * .
     */
    @Test
    public final void testIdealScenario() {


        setupProductsDepartmentDeliveryModeJob.setBatchSize(50);

        final CronJobModel cronJobModel = Mockito.mock(CronJobModel.class);

        when(targetProductSearchService.getAllTargetProductsWithNoDepartment("Staged", 0, 50)).thenReturn(
                targetProductModelList, targetProductModelList, null);


        when(cronJobModel.getRequestAbort()).thenReturn(Boolean.FALSE);

        final CronJobModel cronJob = Mockito.mock(CronJobModel.class);

        when(cronJobUtilityService.getRunningSyncJob()).thenReturn(cronJob);
        final JobModel jobModel = Mockito.mock(JobModel.class);
        when(cronJob.getJob()).thenReturn(jobModel);
        when(jobModel.getCode()).thenReturn("Sync");

        final PerformResult performResult = setupProductsDepartmentDeliveryModeJob.perform(cronJobModel);

        //Verify 2 Batches where executed
        Mockito.verify(tgtUpdateDepartmentDeliveryModeProcessor, Mockito.times(2)).processDepartmentDeliveryModeUpdate(
                targetProductModelList);


        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.SUCCESS.getCode());

        Assert.assertEquals(performResult.getStatus().getCode(), CronJobStatus.FINISHED.getCode());

    }

    /**
     * Test method for {@link au.com.target.tgtupdate.jobs.SetupProductsDepartmentDeliveryModeJob#perform(CronJobModel)}
     * .
     * 
     * @throws CronJobAbortException
     */
    @Test
    public final void testAbortScenario() throws CronJobAbortException {

        setupProductsDepartmentDeliveryModeJob.setBatchSize(50);

        final CronJobModel cronJobModel = Mockito.mock(CronJobModel.class);

        when(targetProductSearchService.getAllTargetProductsWithNoDepartment("Staged", 0, 50)).thenReturn(
                targetProductModelList, targetProductModelList, null);

        Mockito.doThrow(new CronJobAbortException("Aborted")).when(cronJobUtilityService)
                .checkIfCurrentJobIsAborted(cronJobModel);

        final PerformResult performResult = setupProductsDepartmentDeliveryModeJob.perform(cronJobModel);

        //Verify 1 Batches where executed
        Mockito.verify(tgtUpdateDepartmentDeliveryModeProcessor, Mockito.times(1)).processDepartmentDeliveryModeUpdate(
                targetProductModelList);


        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.UNKNOWN.getCode());

        Assert.assertEquals(performResult.getStatus().getCode(), CronJobStatus.ABORTED.getCode());
    }

}

/**
 * 
 */
package au.com.target.tgtupdate.jobs;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtupdate.dao.TargetUpdateProductDao;
import au.com.target.tgtupdate.model.TargetCheckedColourVariantToSetOnlineFromDateModel;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetInitializeOnlineFromDateJobTest {

    class MyTargetInitializeOnlineFromDateJob extends TargetInitializeOnlineFromDateJob {

        @Override
        protected String getVariantPkAsString(final TargetColourVariantProductModel variant) {
            return "12345678";
        }
    }

    @InjectMocks
    private final TargetInitializeOnlineFromDateJob targetInitializeOnlineFromDateJob = new MyTargetInitializeOnlineFromDateJob();

    @Mock
    private ModelService modelService;

    @Mock
    private TargetUpdateProductDao targetUpdateProductDao;

    @Mock
    private TargetProductSearchService targetProductSearchService;

    @Mock
    private TargetColourVariantProductModel colourVariant;

    @Mock
    private TargetSizeVariantProductModel sizeVariant;

    @Before
    public void setUp() {
        targetInitializeOnlineFromDateJob.setNoOfDaysToDirectlyFillFromCreationDate(180);
        Mockito.doReturn(new TargetCheckedColourVariantToSetOnlineFromDateModel()).when(modelService)
                .create(TargetCheckedColourVariantToSetOnlineFromDateModel.class);
    }

    @Test
    public void testNoColourVariant() {
        final CronJobModel cronJobModel = Mockito.mock(CronJobModel.class);
        Mockito.when(targetUpdateProductDao.getColourVariantsWithNullOnlineFromDate()).thenReturn(null);
        final PerformResult performResult = targetInitializeOnlineFromDateJob.perform(cronJobModel);
        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.SUCCESS.getCode());
        Mockito.verify(modelService, Mockito.never()).save(colourVariant);
    }

    @Test
    public void testColourVariantOlderThanSixMonths() {
        final CronJobModel cronJobModel = Mockito.mock(CronJobModel.class);
        final List<TargetColourVariantProductModel> colourVariantModels = new ArrayList<>();
        colourVariantModels.add(colourVariant);
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -182);
        Mockito.when(colourVariant.getCreationtime()).thenReturn(calendar.getTime());
        Mockito.when(targetUpdateProductDao.getColourVariantsWithNullOnlineFromDate()).thenReturn(colourVariantModels);

        final PerformResult performResult = targetInitializeOnlineFromDateJob.perform(cronJobModel);

        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.SUCCESS.getCode());
        Mockito.verify(colourVariant).getVariants();
        Mockito.verify(colourVariant).setOnlineDate(Mockito.any(Date.class));
        Mockito.verify(modelService).save(colourVariant);
    }

    @Test
    public void testColourVariantNewerThanSixMonthsAndDoesntQualifyForNewness() {
        final CronJobModel cronJobModel = Mockito.mock(CronJobModel.class);
        final List<TargetColourVariantProductModel> colourVariantModels = new ArrayList<>();
        colourVariantModels.add(colourVariant);
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -179);
        Mockito.when(colourVariant.getCreationtime()).thenReturn(calendar.getTime());
        Mockito.when(targetUpdateProductDao.getColourVariantsWithNullOnlineFromDate()).thenReturn(colourVariantModels);
        Mockito.when(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(colourVariant))).thenReturn(
                Boolean.FALSE);

        final PerformResult performResult = targetInitializeOnlineFromDateJob.perform(cronJobModel);

        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.SUCCESS.getCode());
        Mockito.verify(colourVariant, Mockito.never()).getVariants();
        Mockito.verify(colourVariant, Mockito.never()).setOnlineDate(calendar.getTime());
        Mockito.verify(modelService, Mockito.never()).save(colourVariant);
    }

    @Test
    public void testColourVariantNewerThanSixMonthsAndQualifyForNewness() {
        final CronJobModel cronJobModel = Mockito.mock(CronJobModel.class);
        final List<TargetColourVariantProductModel> colourVariantModels = new ArrayList<>();
        colourVariantModels.add(colourVariant);
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -179);
        Mockito.when(colourVariant.getCreationtime()).thenReturn(calendar.getTime());
        Mockito.when(targetUpdateProductDao.getColourVariantsWithNullOnlineFromDate()).thenReturn(colourVariantModels);
        Mockito.when(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(colourVariant))).thenReturn(
                Boolean.TRUE);

        final PerformResult performResult = targetInitializeOnlineFromDateJob.perform(cronJobModel);

        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.SUCCESS.getCode());
        Mockito.verify(colourVariant).getVariants();
        Mockito.verify(colourVariant).setOnlineDate(Mockito.any(Date.class));
        Mockito.verify(sizeVariant, Mockito.never()).setOnlineDate(calendar.getTime());
        Mockito.verify(modelService).save(colourVariant);
    }

    @Test
    public void testColourVariantNewerThanSixMonthsAndQualifyForNewnessWithSubVariants() {
        final CronJobModel cronJobModel = Mockito.mock(CronJobModel.class);
        final List<TargetColourVariantProductModel> colourVariantModels = new ArrayList<>();
        colourVariantModels.add(colourVariant);
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -179);
        Mockito.when(colourVariant.getCreationtime()).thenReturn(calendar.getTime());
        Mockito.when(sizeVariant.getCreationtime()).thenReturn(calendar.getTime());
        final List<VariantProductModel> subVariants = new ArrayList<>();
        subVariants.add(sizeVariant);
        Mockito.when(colourVariant.getVariants()).thenReturn(subVariants);
        Mockito.when(targetUpdateProductDao.getColourVariantsWithNullOnlineFromDate()).thenReturn(colourVariantModels);
        Mockito.when(Boolean.valueOf(targetProductSearchService.isProductAvailableToSell(colourVariant))).thenReturn(
                Boolean.TRUE);

        final PerformResult performResult = targetInitializeOnlineFromDateJob.perform(cronJobModel);

        Assert.assertEquals(performResult.getResult().getCode(), CronJobResult.SUCCESS.getCode());
        Mockito.verify(colourVariant).getVariants();
        Mockito.verify(colourVariant).setOnlineDate(Mockito.any(Date.class));
        Mockito.verify(sizeVariant).setOnlineDate(Mockito.any(Date.class));
        Mockito.verify(modelService).save(colourVariant);
    }
}

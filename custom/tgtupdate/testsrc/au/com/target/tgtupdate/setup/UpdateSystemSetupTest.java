/**
 * 
 */
package au.com.target.tgtupdate.setup;

import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtupdate.dao.TargetUpdatesDao;
import au.com.target.tgtupdate.ddl.TargetDbScriptExecutor;
import au.com.target.tgtupdate.model.TargetUpdatesModel;


/**
 * @author htan3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UpdateSystemSetupTest {

    private static final String TEST_ID = "test";

    private static final String TEST_LOCATION = "testLocation";

    @InjectMocks
    @Spy
    private final UpdateSystemSetup updateSystemSetup = new UpdateSystemSetup();

    @Mock
    private TargetDbScriptExecutor targetDbScriptExecutor;

    @Mock
    private TargetUpdatesDao targetUpdatesDao;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetUpdatesModel updatesModel;

    @Mock
    private SystemSetupContext context;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    @Mock
    private Configuration configuration;

    @Test
    public void testCreateEssentialData() {
        updateSystemSetup.createEssentialData(context);

        verify(targetDbScriptExecutor).executeDdl("/tgtupdate/db/custom-index.sql");
    }

    @Test
    public void testImportImpexWhenImpexIsNotAlreadyImported() {
        Mockito.when(targetUpdatesDao.getTargetUpdatesModelById(TEST_ID)).thenReturn(null);
        Mockito.when(modelService.create(TargetUpdatesModel.class)).thenReturn(updatesModel);
        Mockito.doNothing().when(updateSystemSetup).importImpexFile(context, TEST_LOCATION);

        Assert.assertTrue(updateSystemSetup.importImpex(context, TEST_ID, TEST_LOCATION));
        Mockito.verify(updateSystemSetup).importImpexFile(context, TEST_LOCATION);
    }

    @Test
    public void testImportImpexWhenImpexIsAlreadyImported() {
        Mockito.when(targetUpdatesDao.getTargetUpdatesModelById(TEST_ID)).thenReturn(updatesModel);
        Assert.assertFalse(updateSystemSetup.importImpex(context, TEST_ID, TEST_LOCATION));
    }

    @Test
    public void testUpdateTargetSharedConfig() {
        updateSystemSetup.setSharedConfigurationEntries("db.type.system.name");
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        updateSystemSetup.updateTargetSharedConfig();
        verify(configurationService).getConfiguration();
        final ArgumentCaptor<Map> captor = ArgumentCaptor.forClass(Map.class);
        verify(targetSharedConfigService).setSharedConfigs(captor.capture());
        Assert.assertTrue((captor.getValue().get(Integer.valueOf(0))) == null);

    }

    @Test
    public void testUpdateTargetSharedConfigWithEmptyConfig() {
        updateSystemSetup.setSharedConfigurationEntries("");
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        updateSystemSetup.updateTargetSharedConfig();
        verify(configurationService, Mockito.never()).getConfiguration();
    }
}

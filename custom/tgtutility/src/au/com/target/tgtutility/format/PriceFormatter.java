/**
 * 
 */
package au.com.target.tgtutility.format;

import java.text.DecimalFormat;


public final class PriceFormatter {
    private static final DecimalFormat TWO_DF = new DecimalFormat("#.##");
    private static final String DOLLAR_SYMBOL = "$";
    static {
        TWO_DF.setMinimumFractionDigits(2);
    }

    private PriceFormatter() {

    }

    public static String twoDecimalFormat(final double price) {
        return DOLLAR_SYMBOL + TWO_DF.format(price);
    }
}

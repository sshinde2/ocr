/**
 * 
 */
package au.com.target.tgtutility.http.factory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author salexa10
 *
 */
public class CustomHeaderFactory {

    private Map<String, String> headerParameters;

    /**
     * Create the custom headers.
     * 
     * @return List<BasicHeader>
     */
    public List<BasicHeader> createHeader() {
        if (MapUtils.isEmpty(headerParameters)) {
            return Collections.emptyList();
        }
        final Set<Entry<String, String>> parameters = headerParameters.entrySet();
        final List<BasicHeader> customHeaders = new ArrayList<>();
        for (final Entry<String, String> entry : parameters) {
            customHeaders.add(new BasicHeader(entry.getKey(), entry.getValue()));
        }
        return customHeaders;
    }

    /**
     * @param headerParameters
     *            the headerParameters to set
     */
    @Required
    public void setHeaderParameters(final Map<String, String> headerParameters) {
        this.headerParameters = headerParameters;
    }
}

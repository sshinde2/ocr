/**
 * 
 */
package au.com.target.tgtutility.http.factory;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author pvarghe2
 *
 */
public class UserAgentHeaderFactory {

    private String headerFormat;

    private Map<String, String> headerParameters;


    /**
     * Create the header based on header format.
     * 
     * @return UserAgent Header
     */
    public BasicHeader createHeader() {
        if (StringUtils.isEmpty(headerFormat) || MapUtils.isEmpty(headerParameters)) {
            return null;
        }
        String userAgentHeaderString = headerFormat;
        final Set<Entry<String, String>> parameters = headerParameters.entrySet();
        for (final Entry<String, String> entry : parameters) {
            userAgentHeaderString = userAgentHeaderString.replaceAll(entry.getKey(), entry.getValue());
        }
        return new BasicHeader(HttpHeaders.USER_AGENT,
                userAgentHeaderString);
    }

    /**
     * @param headerFormat
     *            the headerFormat to set
     */
    @Required
    public void setHeaderFormat(final String headerFormat) {
        this.headerFormat = headerFormat;
    }

    /**
     * @param headerParameters
     *            the headerParameters to set
     */
    @Required
    public void setHeaderParameters(final Map<String, String> headerParameters) {
        this.headerParameters = headerParameters;
    }
}

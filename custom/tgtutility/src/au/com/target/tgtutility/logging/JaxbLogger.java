/**
 * 
 */
package au.com.target.tgtutility.logging;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;


/**
 * Log the xml representing JAXB objects
 * 
 */
public final class JaxbLogger {

    /**
     * Static utility classes must not be instantiable.
     */
    private JaxbLogger() {
        // Prevent instantiation
    }

    /**
     * Log the xml at default level assuming this is a JAXB object
     * 
     * @param logger
     * @param jaxbObject
     */
    public static void logXml(final Logger logger, final Object jaxbObject) {
        logXmlInfo(logger, jaxbObject);
    }

    /**
     * Log the xml at info level assuming this is a JAXB object
     * 
     * @param logger
     * @param jaxbObject
     */
    public static void logXmlInfo(final Logger logger, final Object jaxbObject) {
        logXmlAtLevel(logger, jaxbObject, false);
    }

    /**
     * Log the xml at debug level assuming this is a JAXB object
     * 
     * @param logger
     * @param jaxbObject
     */
    public static void logXmlDebug(final Logger logger, final Object jaxbObject) {
        logXmlAtLevel(logger, jaxbObject, true);
    }



    private static void logXmlAtLevel(final Logger logger, final Object jaxbObject, final boolean debug) {

        if (debug && !logger.isDebugEnabled()) {
            return;
        }

        try {

            final JAXBContext context = JAXBContext.newInstance(jaxbObject.getClass());
            final Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            final StringWriter writer = new StringWriter();
            m.marshal(jaxbObject, writer);

            if (debug) {
                logger.debug(writer.toString());
            }
            else {
                logger.info(writer.toString());
            }
        }
        catch (final JAXBException e) {
            // Ignore it, we're just logging
            logger.debug("JAXBException while trying to log xml", e);
        }

    }


}

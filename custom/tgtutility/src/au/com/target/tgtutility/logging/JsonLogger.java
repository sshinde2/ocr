/**
 * 
 */
package au.com.target.tgtutility.logging;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;


/**
 * Log json object
 * 
 * @author pratik
 *
 */
public final class JsonLogger {

    /**
     * Static utility classes should not be instantiable.
     */
    private JsonLogger() {
        // Prevent instantiation of class
    }

    /**
     * Utility Method to log JSON object at any level.
     * 
     * @param level
     * @param logger
     * @param formattedAction
     * @param jsonObject
     */
    public static void logJson(final Level level, final Logger logger, final String formattedAction,
            final Object jsonObject) {
        logJsonAtLevel(level, logger, formattedAction, jsonObject);
    }

    /**
     * Method to log JSON object at any level
     * 
     * @param logger
     * @param formattedAction
     * @param jsonObject
     */
    private static void logJsonAtLevel(final Level level, final Logger logger, final String formattedAction,
            final Object jsonObject) {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
        try {
            final String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
            logger.log(level, formattedAction + jsonString);
        }
        catch (final JsonGenerationException e) {
            logger.error("JsonGenerationException while trying to log json object", e);
        }
        catch (final JsonMappingException e) {
            logger.error("JsonMappingException while trying to log json object", e);
        }
        catch (final IOException e) {
            logger.error("IOException while trying to log json object", e);
        }

    }

    /**
     * Method to log json object at info level.
     * 
     * @param logger
     * @param jsonObject
     */
    public static void logJson(final Logger logger, final Object jsonObject) {
        logJsonInfo(logger, jsonObject);
    }

    /**
     * Method to log json object at info level.
     * 
     * @param logger
     * @param jsonObject
     */
    public static void logJsonInfo(final Logger logger, final Object jsonObject) {
        logJsonAtLevel(logger, jsonObject, false);
    }

    /**
     * Method to log json object at debug level.
     * 
     * @param logger
     * @param jsonObject
     */
    public static void logJsonDebug(final Logger logger, final Object jsonObject) {
        logJsonAtLevel(logger, jsonObject, true);
    }

    /**
     * Method to log json, logged at debug level if debug is true else at info.
     * 
     * @param logger
     * @param jsonObject
     * @param debug
     */
    private static void logJsonAtLevel(final Logger logger, final Object jsonObject, final boolean debug) {

        if (debug && !logger.isDebugEnabled()) {
            return;
        }

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
        try {
            final String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);

            if (debug) {
                logger.debug(jsonString);
            }
            else {
                logger.info(jsonString);
            }
        }
        catch (final JsonGenerationException e) {
            logger.debug("JsonGenerationException while trying to log json object", e);
        }
        catch (final JsonMappingException e) {
            logger.debug("JsonMappingException while trying to log json object", e);
        }
        catch (final IOException e) {
            logger.debug("IOException while trying to log json object", e);
        }

    }

}

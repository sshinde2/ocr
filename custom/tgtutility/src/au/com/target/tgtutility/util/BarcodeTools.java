package au.com.target.tgtutility.util;

import org.apache.commons.lang.StringUtils;



/**
 * 
 * @author jspence8
 * 
 */
public final class BarcodeTools {
    private BarcodeTools() {
    }

    /**
     * Finds the check digit using +3 +1 weighted algorithm, ie will work for all EAN barcodes used in Target / Target
     * Country
     * 
     * @param barcode
     * @return single digit
     */
    public static char findEANCheckDigit(final String barcode) {
        final int oddEven = barcode.length() % 2;
        int checkDig = 0;
        for (int cnt = 0; cnt < barcode.length(); cnt++) {
            checkDig += (Character.digit(barcode.charAt(cnt), 10)
                    * ((cnt % 2 == oddEven) ? 1 : 3));
        }
        checkDig = (10 - (checkDig % 10)) % 10;

        return Character.forDigit(checkDig, 10);
    }

    /**
     * Tests a check digit and returns true if it is correct. This function work for all EAN barcodes used in Target /
     * Target Country.
     * 
     * @param barcode
     * @return true if check sum is valid
     */
    public static boolean testEANCheckDigit(final String barcode) {
        int len = barcode.length();

        if (len > 13 || len == 0) {
            return (false);
        }

        final String temp = barcode.substring(0, --len);
        return (findEANCheckDigit(temp) == barcode.charAt(len));
    }


    /**
     * Finds the check digit using +2 +1 weighted algorithm, ie will work for credit cards (and flybuys)
     * 
     * @param cardNum
     * @return single digit
     */
    public static char findCreditCardCheckDigit(final String cardNum) {
        final int oddEven = cardNum.length() % 2;
        int checkDig = 0;
        for (int cnt = 0; cnt < cardNum.length(); cnt++) {
            final int weighted = (Character.digit(cardNum.charAt(cnt), 10)
                    * ((cnt % 2 == oddEven) ? 1 : 2));

            // with a 2 weighting we cannot get more than 18, so no point doing 
            // elaborate for loop to pick up each digit, just do it the simple way
            checkDig += weighted / 10;
            checkDig += weighted % 10;
        }
        checkDig = (10 - (checkDig % 10)) % 10;

        return Character.forDigit(checkDig, 10);
    }

    /**
     * Trim leading and trailing white spaces and Pad with leading zeros if short of required length.
     *
     * @param ean
     *            the ean
     * @return the string
     */
    public static String getCleansedEan(final String ean) {

        if (StringUtils.isEmpty(ean)) {
            return StringUtils.EMPTY;
        }

        // trimming leading and trailing white spaces
        String cleansedEan = StringTools.trimAllWhiteSpaces(ean);

        // padding with leading zeroes if short of required length
        cleansedEan = StringTools.padWithLeadingZeros(cleansedEan);

        return cleansedEan;
    }

    /**
     * Check that Ean must be Numberic and pass Checksum validation
     * 
     * @param ean
     * @return boolean
     */
    public static boolean isValidEan(final String ean) {

        if (StringUtils.isEmpty(ean)) {
            return true;
        }

        if (StringUtils.isNumeric(ean) && testEANCheckDigit(ean)) {
            return true;
        }
        return false;
    }
}

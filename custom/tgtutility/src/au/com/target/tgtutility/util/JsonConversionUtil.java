/**
 * 
 */
package au.com.target.tgtutility.util;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


/**
 * Utility class for all extensions for manipulating java objects to it's JSON representation.
 * 
 * @author bbaral1
 *
 */
public final class JsonConversionUtil {

    private static final Logger LOGGER = Logger.getLogger(JsonConversionUtil.class);

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().setSerializationInclusion(Inclusion.NON_NULL);

    //Default constructor to avoid instantiation of the object.
    private JsonConversionUtil() {

    }

    public static String convertToJsonString(final Object inputObject) {
        String jsonString = null;
        try {
            if (inputObject != null) {
                jsonString = OBJECT_MAPPER.writeValueAsString(inputObject);
            }
        }
        catch (final IOException ioException) {
            LOGGER.error("Error in converting object to JSON");
        }
        return jsonString;
    }

    /**
     * @param inputObject
     * @return pretty string
     */
    public static String convertToPrettyJsonString(final Object inputObject) {
        OBJECT_MAPPER.enable(SerializationConfig.Feature.INDENT_OUTPUT);
        try {
            return convertToJsonString(inputObject);
        }
        finally {
            OBJECT_MAPPER.disable(SerializationConfig.Feature.INDENT_OUTPUT);
        }
    }

    /**
     * @param jsonString
     * @param type
     * @return unmarshalled object
     */
    public static <T> T convertToObject(final String jsonString, final Class<T> type) {
        if (StringUtils.isBlank(jsonString)) {
            return null;
        }
        try {
            return OBJECT_MAPPER.readValue(jsonString, type);
        }
        catch (final IOException e) {
            LOGGER.error("Error in converting JSON to object");
            return null;
        }
    }
}

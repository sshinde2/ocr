/**
 * 
 */
package au.com.target.tgtutility.util;

import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorMessageFormat;
import au.com.target.tgtutility.constants.TgtutilityConstants.InfoMessageFormat;


/**
 * Splunk log message formatter.
 */
public final class SplunkLogFormatter {

    /**
     * Static utility classes must not be instantiable.
     */
    private SplunkLogFormatter()
    {
        // Prevent instantiation
    }

    /**
     * Returns formatted generic error message.
     *
     * @param message
     *            the most specific error description
     * @param errorCode
     *            the error code, one of defined at
     *            {@link au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode}
     * @return formatted error message
     */
    public static String formatMessage(final String message, final String errorCode) {
        return String.format(ErrorMessageFormat.GENERIC_MESSAGE_FORMAT, message, errorCode);
    }

    /**
     * Returns formatted generic error message.
     *
     * @param message
     *            the most specific error description
     * @param errorCode
     *            the error code, one of defined at
     *            {@link au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode}
     * @param additionalInfo
     *            the error message supplemented information
     * @return formatted error message
     */
    public static String formatMessage(final String message, final String errorCode, final String additionalInfo) {
        return String.format(ErrorMessageFormat.EXTENDED_MESSAGE_FORMAT, message, errorCode, additionalInfo);
    }

    /**
     * Returns formatted order error message.
     *
     * @param message
     *            the most specific error description
     * @param errorCode
     *            the error code, one of defined at
     *            {@link au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode}
     * @param orderCode
     *            the order code
     * @return formatted error message
     */
    public static String formatOrderMessage(final String message, final String errorCode, final String orderCode) {
        return String.format(ErrorMessageFormat.ORDER_MESSAGE_FORMAT, message, errorCode, orderCode);
    }

    /**
     * Returns formatted info message
     * 
     * @param message
     *            - the most specific information description
     * @param errorCode
     *            - the information code, one of defined at
     *            {@link au.com.target.tgtutility.constants.TgtutilityConstants.InfoCode}
     * @return formatted information message
     */
    public static String formatInfoMessage(final String message, final String errorCode) {
        return String.format(InfoMessageFormat.GENERIC_MESSAGE_FORMAT, message, errorCode);
    }
}
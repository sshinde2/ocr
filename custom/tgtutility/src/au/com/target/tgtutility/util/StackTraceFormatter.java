/**
 * 
 */
package au.com.target.tgtutility.util;

/**
 * Stack Trace message formatter.
 */
public final class StackTraceFormatter {

    /**
     * Static utility classes must not be instantiable.
     */
    private StackTraceFormatter() {

        // Prevent instantiation
    }

    /**
     * Returns formatted stack trace from stack Elements
     * 
     * @param stckElement
     *            the most specific error description
     * @return formatted stack message
     */
    public static String stackTraceToString(final StackTraceElement[] stckElement) {
        String strTrace = "";
        for (final StackTraceElement stck : stckElement) {
            if (null != stck) {
                strTrace = strTrace + "\n" + stck.toString();
            }
        }
        return strTrace;
    }

}

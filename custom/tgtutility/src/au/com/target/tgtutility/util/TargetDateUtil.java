/**
 * 
 */
package au.com.target.tgtutility.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.LocalDate;


/**
 * @author Rahul
 *
 */
public class TargetDateUtil {

    private static final Logger LOG = Logger.getLogger(TargetDateUtil.class);

    private static final String DATE_FORMAT_FOR_COMPASION = "yyyy-MM-dd";

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    private static final String DATE_FORMAT_TIMEZONE = "dd/MM/yyyy hh:mmaaa z";

    private TargetDateUtil() {
        // private constructor
    }

    /**
     * Get date formatted as long ms since 1970
     * 
     * @param date
     * @return string
     */
    public static String getDateFormattedAsString(final Date date) {

        if (date == null) {
            return null;
        }

        return Long.toString(date.getTime());
    }

    /**
     * Gets the date with midnight time for the given date. If the given date is null, will give the result for the
     * current date.
     *
     * @return the current date with midnight time
     */
    public static Date getDateWithMidnightTime(Date date) {
        if (null == date) {
            date = new Date();
        }

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    /**
     * Gets the last millisecond of the day for the given date. If the given date is null, will give the result for the
     * current date.
     *
     * @return the current date with midnight time
     */
    public static Date getLastMilliSecondOfDate(final Date inputDate) {
        if (inputDate == null) {
            return null;
        }

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(inputDate);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);

        return calendar.getTime();
    }

    /**
     * Gets the first second of current date.
     * 
     * @param inputDate
     * @return the first second of current date
     */
    public static String getDateStringForComparison(final Date inputDate) {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_FOR_COMPASION);
        return sdf.format(inputDate);
    }

    public static String getDateAsString(final Date inputDate, final String format) {

        if (inputDate == null) {
            return null;
        }
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(inputDate);
    }


    public static int getDifferenceBetweenDateInDay(final Date from, final Date to) {
        return Days.daysBetween(new LocalDate(to.getTime()), new LocalDate(from.getTime())).getDays();
    }

    /**
     * Method to parse the string to date from GMT time zone.
     * 
     * @param inputDate
     * @return the the date
     */
    public static Date getStringAsDate(final String inputDate) {

        if (StringUtils.isBlank(inputDate)) {
            return null;
        }

        final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone(TimeZoneUtils.TIMEZONE_GMT));

        try {
            return sdf.parse(inputDate);
        }
        catch (final ParseException exception) {
            LOG.warn("Error while Parsing inputDate " + inputDate, exception);
            return null;
        }
    }

    /**
     * Method to parse the String with specified format to Date in system time zone.
     * 
     * @param inputDate
     * @param format
     * @return date
     */
    public static Date getStringAsDate(final String inputDate, final String format) {

        if (StringUtils.isBlank(inputDate)) {
            return null;
        }

        final SimpleDateFormat sdf = new SimpleDateFormat(format);

        try {
            return sdf.parse(inputDate);
        }
        catch (final ParseException exception) {
            LOG.warn("Error while Parsing inputDate " + inputDate, exception);
            return null;
        }
    }



    /**
     * Method to parse the String with specified format to Date in the given time zone
     * 
     * @param inputDate
     * @param format
     * @param timeZone
     * @return String
     */
    public static String getDateAsString(final Date inputDate, final String format, final String timeZone) {

        if (inputDate == null) {
            return null;
        }
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        if (StringUtils.isNotEmpty(timeZone)) {
            sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        return sdf.format(inputDate);
    }


    /**
     * Method to convert time in millis to Date
     * 
     * time in millis will be converted to the corresponding java util date
     * 
     * @param timeInMillis
     * @return date
     */
    public static Date getDateForTimeInMillis(final String timeInMillis) {
        try {
            return new Date(Long.parseLong(timeInMillis));
        }
        catch (final NumberFormatException ex) {
            return null;
        }
    }

    /**
     * Method to get the next week date after the input date. This does not take into account public holidays. Returned
     * date will have midnight time.
     * 
     * @param inputDate
     * @return date
     */
    public static Date getNextWeekDate(final Date date) {

        final LocalDate localDate = LocalDate.fromDateFields(date);

        if (localDate.getDayOfWeek() == DateTimeConstants.FRIDAY) {
            return localDate.plusDays(3).toDate();
        }
        else if (localDate.getDayOfWeek() == DateTimeConstants.SATURDAY) {
            return localDate.plusDays(2).toDate();
        }
        else {
            return localDate.plusDays(1).toDate();
        }

    }

    /**
     * Method to check if the input date is a Week day.
     * 
     * @param inputDate
     * @return boolean
     */
    public static boolean isWeekDay(final Date inputDate) {

        final LocalDate localDate = LocalDate.fromDateFields(inputDate);

        return !(localDate.getDayOfWeek() == DateTimeConstants.SATURDAY
                || localDate.getDayOfWeek() == DateTimeConstants.SUNDAY);

    }


    public static String getCurrentFormattedDate() {
        return getDateAsString(new Date(), DATE_FORMAT_TIMEZONE);
    }

    /**
     * Method to add a number of days to the input date.
     * 
     * @param inputDate
     * @param numberOfDays
     * @return date
     */
    public static Date addDays(final Date inputDate, final int numberOfDays) {

        if (inputDate == null) {
            return null;
        }

        final Calendar cal = Calendar.getInstance();
        cal.setTime(inputDate);
        cal.add(Calendar.DATE, numberOfDays);
        return cal.getTime();
    }

}
/**
 * 
 */
package au.com.target.tgtutility.util;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.config.AbstractFactoryBean;

import au.com.target.tgtutility.http.factory.CustomHeaderFactory;
import au.com.target.tgtutility.http.factory.HttpClientFactory;
import au.com.target.tgtutility.http.factory.UserAgentHeaderFactory;


/**
 * @author bhuang3
 *
 */
public class TargetHttpClientFactoryBean extends AbstractFactoryBean<CloseableHttpClient> {

    private static final int MAX_NUMBER_OF_TOTAL_CONNECTIONS = 200;

    private static final int MAX_CONNECTIONS_PER_ROUTE_ALLOWED = 20;

    private int connectTimeout;

    private int readTimeout;

    private UsernamePasswordCredentials credentials;

    private String authKey;

    private HttpClientFactory httpClientFactory;

    private UserAgentHeaderFactory userAgentHeaderFactory;

    private String[] protocols;

    private String[] cipherSuites;

    private CustomHeaderFactory customHeaderFactory;

    private String authBearerKey;

    @Override
    public Class<?> getObjectType() {
        return CloseableHttpClient.class;
    }

    @Override
    protected CloseableHttpClient createInstance() throws Exception {
        final SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
        sslcontext.init(null, null, null);

        final PoolingHttpClientConnectionManager connectionManager = createHttpClientConnectionManager(sslcontext);

        connectionManager.setMaxTotal(MAX_NUMBER_OF_TOTAL_CONNECTIONS);
        connectionManager.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE_ALLOWED);
        final RequestConfig config = RequestConfig.custom().setConnectTimeout(connectTimeout)
                .setConnectionRequestTimeout(connectTimeout)
                .setSocketTimeout(readTimeout).build();

        final List<Header> defaultHeaders = new ArrayList<>();
        if (StringUtils.isNoneBlank(authKey)) {
            defaultHeaders.add(new BasicHeader("AUTH-KEY", authKey));
        }

        if (StringUtils.isNoneBlank(authBearerKey)) {
            defaultHeaders.add(new BasicHeader("Authorization", "Bearer " + authBearerKey));
        }
        if (userAgentHeaderFactory != null) {
            addUserAgentHeader(defaultHeaders);
        }

        if (customHeaderFactory != null) {
            addCustomHeader(defaultHeaders);
        }

        final HttpClientBuilder httpClientBuilder = httpClientFactory.createHttpClientBuilderWithProxy()
                .setDefaultRequestConfig(config)
                .setSslcontext(sslcontext)
                .setConnectionManager(connectionManager);
        if (!defaultHeaders.isEmpty()) {
            httpClientBuilder.setDefaultHeaders(defaultHeaders);
        }
        if (credentials != null) {
            httpClientBuilder.setDefaultCredentialsProvider(createBasicCredentialsProvider());
        }

        final CloseableHttpClient httpClient = httpClientBuilder.build();
        return httpClient;
    }

    protected void addUserAgentHeader(final List<Header> defaultHeaders) {
        if (userAgentHeaderFactory != null) {
            final BasicHeader header = userAgentHeaderFactory.createHeader();
            if (header != null) {
                defaultHeaders.add(header);
            }
        }
    }

    protected void addCustomHeader(final List<Header> defaultHeaders) {
        if (customHeaderFactory != null) {
            final List<BasicHeader> headers = customHeaderFactory.createHeader();
            if (headers != null) {
                defaultHeaders.addAll(headers);
            }
        }
    }


    private BasicCredentialsProvider createBasicCredentialsProvider() {
        BasicCredentialsProvider provider = null;
        if (credentials != null) {
            provider = new BasicCredentialsProvider();
            provider.setCredentials(AuthScope.ANY, credentials);
        }
        return provider;
    }

    private PoolingHttpClientConnectionManager createHttpClientConnectionManager(final SSLContext sslContext) {
        if (ArrayUtils.isEmpty(protocols) || ArrayUtils.isEmpty(cipherSuites)) {
            return new PoolingHttpClientConnectionManager();
        }

        final SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,
                protocols, cipherSuites,
                SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);

        final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslConnectionSocketFactory).build();

        return new PoolingHttpClientConnectionManager(registry);
    }

    /**
     * @param connectTimeout
     *            the connectTimeout to set
     */
    @Required
    public void setConnectTimeout(final int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    /**
     * @param readTimeout
     *            the readTimeout to set
     */
    @Required
    public void setReadTimeout(final int readTimeout) {
        this.readTimeout = readTimeout;
    }

    /**
     * @param credentials
     *            the credentials to set
     */
    public void setCredentials(final UsernamePasswordCredentials credentials) {
        this.credentials = credentials;
    }

    /**
     * @param authKey
     *            the authKey to set
     */
    public void setAuthKey(final String authKey) {
        this.authKey = authKey;
    }

    /**
     * @param httpClientFactory
     *            the httpClientFactory to set
     */
    @Required
    public void setHttpClientFactory(final HttpClientFactory httpClientFactory) {
        this.httpClientFactory = httpClientFactory;
    }

    /**
     * @param userAgentHeaderFactory
     *            the userAgentHeaderFactory to set
     */
    public void setUserAgentHeaderFactory(final UserAgentHeaderFactory userAgentHeaderFactory) {
        this.userAgentHeaderFactory = userAgentHeaderFactory;
    }

    /**
     * @param authBearerKey
     *            the authBearerKey to set
     */
    public void setAuthBearerKey(final String authBearerKey) {
        this.authBearerKey = authBearerKey;
    }

    /**
     * @param customHeaderFactory
     *            the customHeaderFactory to set
     */
    public void setCustomHeaderFactory(final CustomHeaderFactory customHeaderFactory) {
        this.customHeaderFactory = customHeaderFactory;
    }


    /**
     * @param protocols
     *            the protocols to set
     */
    public void setProtocols(final String[] protocols) {
        this.protocols = protocols;
    }

    /**
     * @param cipherSuites
     *            the cipherSuites to set
     */
    public void setCipherSuites(final String[] cipherSuites) {
        this.cipherSuites = cipherSuites;
    }

}

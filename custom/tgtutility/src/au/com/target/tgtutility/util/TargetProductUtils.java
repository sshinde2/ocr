/**
 * 
 */
package au.com.target.tgtutility.util;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetProductModel;


/**
 * Utilities for products.
 * 
 * @author jjayawa1
 *
 */
public final class TargetProductUtils {

    private TargetProductUtils() {
        //Empty constructor
    }

    /**
     * Gets the base product from any variant product.
     * 
     * @param targetVariantProduct
     * @return TargetProductModel
     */
    public static TargetProductModel getBaseTargetProduct(
            final AbstractTargetVariantProductModel targetVariantProduct) {
        final ProductModel product = targetVariantProduct.getBaseProduct();

        if (product != null) {
            if (product instanceof TargetProductModel) {
                return (TargetProductModel)product;
            }
            else if (product instanceof AbstractTargetVariantProductModel) {
                return getBaseTargetProduct((AbstractTargetVariantProductModel)product);
            }
        }

        return null;
    }

    /**
     * Return the colour variant for the given variant product
     * 
     * @param targetVariantProduct
     * @return TargetColourVariantProductModel or null if it has none
     */
    public static TargetColourVariantProductModel getColourVariantProduct(
            final AbstractTargetVariantProductModel targetVariantProduct) {

        if (targetVariantProduct == null) {
            return null;
        }

        if (targetVariantProduct instanceof TargetColourVariantProductModel) {
            return (TargetColourVariantProductModel)targetVariantProduct;
        }

        final ProductModel product = targetVariantProduct.getBaseProduct();
        if (product instanceof TargetColourVariantProductModel) {
            return (TargetColourVariantProductModel)product;
        }

        return null;
    }

    public static boolean checkIfStockDataHasStock(final StockData stockData) {
        if (stockData != null) {
            if (stockData.getStockLevel() != null && stockData.getStockLevel().longValue() > 0L) {
                return true;

            }
            else if (stockData.getStockLevelStatus() != null
                    && StockLevelStatus.OUTOFSTOCK != stockData.getStockLevelStatus()) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkIfStockDataExists(final StockData stockData) {
        return stockData != null && stockData.getStockLevel() != null;
    }

    public static boolean isProductOpenForPreOrder(final ProductModel product) {

        if (product instanceof AbstractTargetVariantProductModel) {
            final TargetColourVariantProductModel colourVariantProduct = getColourVariantProduct(
                    (AbstractTargetVariantProductModel)product);

            if (colourVariantProduct == null) {
                return false;
            }
            final DateTime preOrderStartDateTime = new DateTime(colourVariantProduct.getPreOrderStartDateTime());
            final DateTime preOrderEndDateTime = new DateTime(colourVariantProduct.getPreOrderEndDateTime());

            if (preOrderStartDateTime.isBeforeNow() && preOrderEndDateTime.isAfterNow()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check whether a product is embargoed.
     * 
     * @param product
     * @return true if product is emabargoed
     */
    public static boolean isProductEmbargoed(final ProductModel product) {
        if (product instanceof AbstractTargetVariantProductModel) {
            final TargetColourVariantProductModel colourVariantProduct = getColourVariantProduct(
                    (AbstractTargetVariantProductModel)product);
            if (colourVariantProduct == null || colourVariantProduct.getNormalSaleStartDateTime() == null) {
                return false;
            }

            // normal Sale start date == embargo release date for ODBMS
            final DateTime embargoReleaseDate = new DateTime(colourVariantProduct.getNormalSaleStartDateTime());

            if (embargoReleaseDate.isAfterNow()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method will retrieves normal sale start date and time.
     * 
     * @param productModel
     * @return Date
     */
    public static Date getNormalSaleStartDateTime(final ProductModel productModel) {

        if (productModel instanceof AbstractTargetVariantProductModel) {

            final TargetColourVariantProductModel targetColourVariantProductModel = getColourVariantProduct(
                    (AbstractTargetVariantProductModel)productModel);

            if (targetColourVariantProductModel != null
                    && targetColourVariantProductModel.getNormalSaleStartDateTime() != null) {
                return targetColourVariantProductModel.getNormalSaleStartDateTime();

            }
        }

        return null;
    }


    /**
     * Method will retrieves preorder end date
     * 
     * @param variantModel
     * @return Date
     */
    public static Date getPreOrderEndDate(final AbstractTargetVariantProductModel variantModel) {

        final TargetColourVariantProductModel targetColourVariantProductModel = getColourVariantProduct(
                variantModel);
        if (targetColourVariantProductModel != null) {
            return targetColourVariantProductModel.getPreOrderEndDateTime();
        }
        return null;
    }

    public static List<AbstractTargetVariantProductModel> getAllSellableVariants(final ProductModel product) {
        final List<AbstractTargetVariantProductModel> variantProductList = new ArrayList<>();
        final Collection<VariantProductModel> subVariants = product.getVariants();
        if (CollectionUtils.isEmpty(subVariants) && product instanceof AbstractTargetVariantProductModel) {
            variantProductList.add((AbstractTargetVariantProductModel)product);
        }
        else {
            for (final VariantProductModel subVariant : subVariants) {
                variantProductList.addAll(getAllSellableVariants(subVariant));
            }
        }
        return variantProductList;
    }

    public static Collection<String> getAllSellableVariantCodes(final ProductModel product) {
        final List<AbstractTargetVariantProductModel> variantsList = getAllSellableVariants(product);
        final Collection<String> variants = new ArrayList();

        for (final VariantProductModel variant : variantsList) {
            variants.add(variant.getCode());
        }

        return variants;
    }
}

/**
 * 
 */
package au.com.target.tgtutility.util;

import java.util.regex.Pattern;


/**
 * Common Rules for form / data validation across all projects
 * 
 * @author Benoit Vanalderweireldt
 * 
 */
public interface TargetValidationCommon {

    public interface Name {
        int MIN_SIZE = 2;
        int MAX_SIZE = 32;
        int FULLNAME_MAX_SIZE = 64;
        String NAME_VALIDATOR = "\\s*(\\p{L}+((-|'|\\s)\\p{L}+)*)\\s*";
        Pattern PATTERN = Pattern.compile(NAME_VALIDATOR);
    }

    /**
     * 
     * User First Name Please see for common name rules
     * {@link au.com.target.tgtutility.util.TargetValidationCommon.Name}
     * 
     */
    public interface FirstName {
        Pattern PATTERN = TargetValidationCommon.Name.PATTERN;
    }

    public interface RecipientFirstName extends FirstName {
        // empty }
    }


    /**
     * 
     * User Last Name Please see for common name rules {@link au.com.target.tgtutility.util.TargetValidationCommon.Name}
     * 
     */
    public interface LastName {
        Pattern PATTERN = TargetValidationCommon.Name.PATTERN;
    }

    public interface FullName {
        Pattern PATTERN = TargetValidationCommon.Name.PATTERN;
    }

    public interface RecipientLastName extends LastName {
        // empty }
    }

    /**
     * 
     * User Address Line
     * 
     */
    public interface MessageText {
        int MIN_SIZE = 2;
        int MAX_SIZE = 120;
        Pattern PATTERN = Pattern.compile("^.*$");
    }

    /**
     * 
     * User Email
     * 
     */
    public interface Email {
        String ATOM = "(([a-zA-Z0-9!#$%&\\;'*+/=?^_`{|}~-]|\\s)\\.?)";
        String LOCAL = "(?!\\.)(?=.*[^\\.]@)[a-zA-Z0-9!#$%&\\;'*+/=?^_`.{|}~-]{1,64}";
        String HOSTNAME_START = "[a-zA-Z0-9]";
        String HOSTNAME_BODY = "[a-zA-Z0-9-\\.]{0,253}";
        String HOSTNAME_END = "[a-zA-Z0-9]*(\\.[A-Za-z0-9]+)";//check for . validation
        String HOSTNAME = HOSTNAME_START + HOSTNAME_BODY + HOSTNAME_END;
        String IPV4_DOMAIN = "\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\]";
        String IPV6_DOMAIN = "\\[(I|i)(P|p)(V|v)6([0-9a-z]{0,4}:){2,7}[0-9a-z]{0,4}\\]";//Minimum IPV6 address would be :: (mean 0:0:0:0:0:0:0:0)
        String IP_DOMAIN = IPV4_DOMAIN + "|" + IPV6_DOMAIN;
        String MAIL_VALIDATOR =
                "^" + LOCAL + "@"
                        + "(" + HOSTNAME
                        + "|"
                        + IP_DOMAIN
                        + ")$";
        Pattern PATTERN = Pattern.compile(MAIL_VALIDATOR);
    }

    public interface RecipientEmail extends Email {
        // empty }
    }

    /**
     * 
     * User password
     * 
     */
    public interface Password {
        int MIN_SIZE = 6;
        int MAX_SIZE = 32;
        Pattern PATTERN = Pattern.compile("^[^~\\s]*$");

    }

    /**
     * 
     * User Address Line
     * 
     */
    public interface AddressLine {
        int MIN_SIZE = 2;
        int MAX_SIZE = 30;
        Pattern PATTERN = Pattern.compile("^[\\p{L}\\d\\s-\'/]*$");
    }

    public interface AddressLine1 {
        final Pattern PATTERN = TargetValidationCommon.AddressLine.PATTERN;
    }

    public interface AddressLine2 {
        Pattern PATTERN = TargetValidationCommon.AddressLine.PATTERN;
    }

    public interface CitySuburb {
        int MIN_SIZE = 1;
        int MAX_SIZE = 64;
        Pattern PATTERN = Pattern.compile("^[\\p{L} ]*$");
    }


    public interface PostCode {
        int SIZE = 4;
        Pattern PATTERN = Pattern.compile("^[\\d]*$");
    }

    public interface InternationalPostCode {
        int MIN_SIZE = 2;
        int MAX_SIZE = 10;
        Pattern PATTERN = Pattern.compile("^[a-zA-Z\\d -]*$");
    }

    public interface Phone {
        int MIN_SIZE_MIGRATED = 8; // Min Size after cleaning -> 12345678
        int MAX_SIZE = 16; // Max Size -> 1234567890123456
        int MIN_SIZE_NZ = 8;
        int MIN_SIZE_AU = 9;
        int MIN_SIZE = MIN_SIZE_AU; // Min Size after cleaning -> 12345678
        Pattern PATTERN = Pattern.compile("^\\s*?\\+?[- \\(\\)\\d]*$");
        Pattern CLEAN_REGEX = Pattern.compile("[^\\d\\+]\\D*"); // Used to clean entry to count '+' at first if available and only digits
        Pattern VALID_REGEX = Pattern.compile("\\+?\\d+"); // Used to match entry '+' at first if available and only digits
    }

    public interface MobilePhone {
        int MIN_SIZE = 10; // Min Size -> 0412345678
        int MAX_SIZE = 16; // Max size -> 1234567890123456
        Pattern PATTERN = Pattern.compile("^\\s*?\\(?\\+?(61|610|0)?\\)?[45]\\)?[- \\d]*$"); // Basic check for australia mobile phone, always start off digit 4 or 5
        Pattern CLEAN_REGEX = TargetValidationCommon.Phone.CLEAN_REGEX; // Used to clean entry to count '+' at first if available and only digits
        Pattern VALID_REGEX = TargetValidationCommon.Phone.VALID_REGEX; // Used to match entry '+' at first if available and only digits
    }

    public interface Voucher {
        int MIN_SIZE = 3;
        int MAX_SIZE = 32;
        Pattern PATTERN = Pattern.compile("^[A-Z\\d-]{" + MIN_SIZE + "," + MAX_SIZE + "}$"); // Eg. 345-9FC7-4S7L-7H4H or HOMESALE
    }

    public interface AbnOrAcn {
        int MIN_SIZE = 9;
        int MAX_SIZE = 11;
        Pattern PATTERN = Pattern.compile("^[\\d]*$");
    }


    /**
     * 
     * 
     * DONT'T DELETE it is used into default validator
     * 
     * 
     */
    public interface Default {
        Pattern PATTERN = Pattern.compile("^.*$");
    }
}

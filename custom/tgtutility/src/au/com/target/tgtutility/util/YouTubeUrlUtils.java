/**
 * 
 */
package au.com.target.tgtutility.util;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * A utility class for operations on YouTube URLs.
 * 
 * @author rmcalave
 */
public final class YouTubeUrlUtils {

    private static final Logger LOG = Logger.getLogger(YouTubeUrlUtils.class);

    /** Base URL for embedded YouTube URLs. {0} is a placeholder for the video ID. */
    private static final String YOUTUBE_EMBED_BASE_URL = "http://www.youtube.com/embed/{0}?rel=0";

    /**
     * Regex pattern to match full YouTube URLs. Valid URL types are of the format
     * 'http://www.youtube.com/watch?v=<videoCode>'.<br />
     * 
     * URLs are considered valid with or without the following: 'http://', 'www.', country modifiers (.com.au, .co.uk,
     * etc), additional query string parameters
     */
    private static final Pattern VALID_FULL_YOUTUBE_URL_PATTERN = Pattern.compile(".*youtube.co.*/watch\\?v=.*");

    private static final Pattern VALID_FULL_EMBED_URL_PATTERN = Pattern.compile(".*youtube.co.*/v/.*");

    /**
     * Regex pattern to match short YouTube URLs. Valid URL types are of the format: youtu.be/<videoCode>.<br />
     * 
     * URLs are considered valid with or without the following: 'http://', 'www.'.
     */
    private static final Pattern VALID_SHORT_YOUTUBE_URL_PATTERN = Pattern.compile(".*youtu.be/.*");

    private static final Pattern VALID_EMBED_YOUTUBE_URL_PATTERN = Pattern.compile(".*youtube.co.*/embed/.*");

    private static final Pattern VALID_VERY_SHORT_YOUTUBE_URL_PATTERN = Pattern.compile(".*y2u.be/.*");



    private YouTubeUrlUtils() {
        // prevent construction
    }

    /**
     * Builds a YouTube URL suitable for embedding based on a standard full or short YouTube URL.<br />
     * 
     * If the provided youtubeUrl is already an embed URL, it will just be returned as is (with '?rel=0' appended if it
     * is not already present).<br />
     * 
     * If the provided youtubeUrl is valid, then a YouTube URL with the following format will be returned:
     * 'http://www.youtube.com/embed/<videoId>?rel=0'.<br />
     * 
     * An empty String will be returned when youtubeUrl is null or blank, youtubeUrl is not a valid YouTube URL or the
     * video ID could not be extracted from youtubeUrl.
     * 
     * 
     * @param youtubeUrl
     *            A standard full or short YouTube URL.
     * @return A YouTube URL suitable for embedding on a web page.
     * @see #YOUTUBE_EMBED_BASE_URL
     * @see #VALID_FULL_YOUTUBE_URL_PATTERN
     * @see #VALID_SHORT_YOUTUBE_URL_PATTERN
     */
    public static String buildYoutubeEmbeddedUrl(final String youtubeUrl) {
        if (StringUtils.isBlank(youtubeUrl)) {
            return "";
        }

        String videoId = null;

        if (VALID_FULL_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            videoId = StringUtils.substringAfter(youtubeUrl, "watch?v=");
            videoId = StringUtils.substringBefore(videoId, "&");
        }
        else if (VALID_SHORT_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            videoId = StringUtils.substringAfterLast(youtubeUrl, "/");
        }
        else if (VALID_EMBED_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            if (!youtubeUrl.contains("?rel=0")) {
                return youtubeUrl.concat("?rel=0");
            }

            return youtubeUrl;
        }
        else {
            LOG.info("Provided YouTube URL ('" + youtubeUrl + "') does not appear to be valid.");
            return "";
        }

        if (StringUtils.isBlank(videoId)) {
            LOG.info("Could not extract video code from provided YouTube URL: " + youtubeUrl + ". Check the format.");
            return "";
        }

        return StringUtils.replace(YOUTUBE_EMBED_BASE_URL, "{0}", videoId);
    }

    public static String getVideoId(final String youtubeUrl) {
        if (StringUtils.isBlank(youtubeUrl)) {
            return "";
        }
        String videoId = null;

        if (VALID_FULL_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            videoId = StringUtils.substringAfter(youtubeUrl, "watch?v=");
            videoId = StringUtils.substringBefore(videoId, "&");
        }
        else if (VALID_SHORT_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()
                || VALID_VERY_SHORT_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            videoId = StringUtils.substringAfterLast(youtubeUrl, "/");
        }
        else if (VALID_FULL_EMBED_URL_PATTERN.matcher(youtubeUrl).matches()) {
            videoId = StringUtils.substringAfter(youtubeUrl, "/v/");
            videoId = StringUtils.substringBefore(videoId, "?");
        }
        else if (VALID_EMBED_YOUTUBE_URL_PATTERN.matcher(youtubeUrl).matches()) {
            if (!youtubeUrl.contains("?rel=0")) {
                videoId = StringUtils.substringAfterLast(youtubeUrl, "/");
            }
            else {
                videoId = StringUtils.substringAfterLast(youtubeUrl, "/");
                videoId = StringUtils.substringBefore(videoId, "?");
            }

        }
        else {
            LOG.info("Provided YouTube URL ('" + youtubeUrl + "') does not appear to be valid.");
            return "";
        }

        if (StringUtils.isBlank(videoId)) {
            LOG.info("Could not extract video code from provided YouTube URL: " + youtubeUrl + ". Check the format.");
            return "";
        }

        return videoId;
    }
}

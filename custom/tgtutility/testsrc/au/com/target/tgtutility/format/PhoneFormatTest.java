/**
 * 
 */
package au.com.target.tgtutility.format;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;


/**
 * @author Benoit Vanalderweireldt
 * 
 */
@UnitTest
public class PhoneFormatTest {

    @Test
    public void testFormatPhone() {
        Assert.assertEquals("02 1234 5678", PhoneFormat.formatNumber("+61212345678"));
    }

    @Test
    public void testFormatMobile() {
        Assert.assertEquals("0412 345 678", PhoneFormat.formatNumber("+61412345678"));
    }

    @Test
    public void testFormatMobileWithoutPlus() {
        Assert.assertEquals("61412345678", PhoneFormat.formatNumber("61412345678"));
    }

    @Test
    public void testFormatInter() {
        Assert.assertEquals("+33238465894", PhoneFormat.formatNumber("+33238465894"));
    }

    @Test
    public void testFormatInterTriangulate() {
        Assert.assertEquals("+33 238 465 894", PhoneFormat.formatNumber("+33 238 465 894"));
    }

    @Test
    public void testFormatFrench() {
        Assert.assertEquals("+33 2 38 46 58 94", PhoneFormat.formatNumber("+33 2 38 46 58 94"));
    }

    @Test
    public void testFormatVIetnam() {
        Assert.assertEquals("+941635339203", PhoneFormat.formatNumber("+941635339203"));
    }

    @Test
    public void testFormatAusPhone() {
        Assert.assertEquals("02 3846 5894", PhoneFormat.formatNumber("+61238465894"));
    }

    @Test
    public void testFormatAusMobile() {
        Assert.assertEquals("0401 234 456", PhoneFormat.formatNumber("+61401234456"));
    }

    @Test
    public void testFormatNull() {
        Assert.assertEquals(null, PhoneFormat.formatNumber(null));
    }

    @Test
    public void testFormatEmpty() {
        Assert.assertEquals(StringUtils.EMPTY, PhoneFormat.formatNumber(StringUtils.EMPTY));
    }

    @Test
    public void testFormatMobileForSms() {
        Assert.assertEquals("61412345678", PhoneFormat.validateAndformatPhoneNumberForSms("+61412345678"));
    }

    @Test
    public void testFormatMobileForSmsForNumber61412345678() {
        Assert.assertEquals("61412345678", PhoneFormat.validateAndformatPhoneNumberForSms("61412345678"));
    }

    @Test
    public void testFormatMobileForSmsForNumber0412345678() {
        Assert.assertEquals("61412345678", PhoneFormat.validateAndformatPhoneNumberForSms("0412345678"));
    }

    @Test
    public void testFormatMobileForSmsForNumber412345678() {
        Assert.assertEquals("61412345678", PhoneFormat.validateAndformatPhoneNumberForSms("412345678"));
    }

    @Test
    public void testFormatMobileForSmsForNumber610412345678() {
        Assert.assertEquals("61412345678", PhoneFormat.validateAndformatPhoneNumberForSms("610412345678"));
    }

    @Test
    public void testFormatMobileForSmsWithAusPhone() {
        Assert.assertEquals(null, PhoneFormat.validateAndformatPhoneNumberForSms("+61312345678"));
    }

    @Test
    public void testFormatMobileForSmsWithEmptyNumbert() {
        Assert.assertEquals(null, PhoneFormat.validateAndformatPhoneNumberForSms(StringUtils.EMPTY));
    }

}

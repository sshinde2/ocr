package au.com.target.tgtutility.http.factory;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicHeader;
import org.junit.Test;


/**
 * @author salexa10
 *
 */
@UnitTest
public class CustomHeaderFactoryTest {

    private final CustomHeaderFactory factory = new CustomHeaderFactory();

    @Test
    public void testNullHeaderParamter() {
        factory.setHeaderParameters(null);
        factory.createHeader();
        assertThat(factory.createHeader()).isEmpty();
    }

    @Test
    public void testCreateHeader() {
        factory.setHeaderParameters(createHeaderParameters());
        final List<BasicHeader> headers = factory.createHeader();
        assertThat(headers).isNotNull();
        assertThat(headers).onProperty("name").containsExactly("Zip-Version");
        assertThat(headers).onProperty("value").containsExactly("2017-03-01");
    }

    private Map<String, String> createHeaderParameters() {
        final Map<String, String> parameters = new HashMap<>();
        parameters.put("Zip-Version", "2017-03-01");
        return parameters;
    }
}

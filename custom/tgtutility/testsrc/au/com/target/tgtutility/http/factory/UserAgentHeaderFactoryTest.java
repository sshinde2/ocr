package au.com.target.tgtutility.http.factory;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHeaders;
import org.apache.http.message.BasicHeader;
import org.junit.Test;


@UnitTest
public class UserAgentHeaderFactoryTest {

    private final UserAgentHeaderFactory factory = new UserAgentHeaderFactory();

    @Test
    public void testNullHeaderParamter() {
        factory.setHeaderFormat("1:%%1%%");
        factory.setHeaderParameters(null);
        factory.createHeader();
        assertThat(factory.createHeader()).isNull();
    }

    @Test
    public void testNullHeaderFormat() {
        factory.setHeaderFormat(null);
        factory.setHeaderParameters(createHeaderParameters());
        assertThat(factory.createHeader()).isNull();
    }

    @Test
    public void testCreateHeader() {
        factory.setHeaderFormat("1:%%1%%,2:%%2%%");
        factory.setHeaderParameters(createHeaderParameters());
        final BasicHeader header = factory.createHeader();
        assertThat(header).isNotNull();
        assertThat(header.getName()).isEqualTo(HttpHeaders.USER_AGENT);
        assertThat(header.getValue()).isEqualTo("1:1,2:2");

    }

    private Map<String, String> createHeaderParameters() {
        final Map<String, String> parameters = new HashMap<>();
        parameters.put("%%1%%", "1");
        parameters.put("%%2%%", "2");
        return parameters;
    }
}

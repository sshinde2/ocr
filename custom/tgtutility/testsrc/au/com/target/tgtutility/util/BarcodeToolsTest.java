package au.com.target.tgtutility.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;


@UnitTest
public class BarcodeToolsTest {

    @Test
    public void testFindEANCheckDigit() throws Exception {
        assertEquals('5', BarcodeTools.findEANCheckDigit("5"));
        assertEquals('9', BarcodeTools.findEANCheckDigit("15915"));
        assertEquals('9', BarcodeTools.findEANCheckDigit("0015915"));
        assertEquals('7', BarcodeTools.findEANCheckDigit("930067500142"));
    }

    @Test
    public void testTestEANCheckDigit() throws Exception {
        assertTrue(BarcodeTools.testEANCheckDigit("55"));
        assertTrue(BarcodeTools.testEANCheckDigit("159159"));
        assertTrue(BarcodeTools.testEANCheckDigit("00159159"));
        assertTrue(BarcodeTools.testEANCheckDigit("9300675001427"));

        assertFalse(BarcodeTools.testEANCheckDigit("54"));
        assertFalse(BarcodeTools.testEANCheckDigit("159150"));
        assertFalse(BarcodeTools.testEANCheckDigit("00159158"));
        assertFalse(BarcodeTools.testEANCheckDigit("9300675001423"));

        assertFalse(BarcodeTools.testEANCheckDigit(""));
        assertFalse(BarcodeTools.testEANCheckDigit("123456789132456"));
    }

    @Test
    public void testFindCreditCardCheckDigit() throws Exception {
        assertEquals('6', BarcodeTools.findCreditCardCheckDigit("512345678901234"));
        assertEquals('0', BarcodeTools.findCreditCardCheckDigit("600894321861691"));
    }

    @Test
    public void testgetCleansedEan() {
        assertEquals("00001234", BarcodeTools.getCleansedEan(" 1234 "));
        assertEquals("0123 456", BarcodeTools.getCleansedEan(" 123 456  "));
        assertEquals("00123 456 789", BarcodeTools.getCleansedEan("  123 456 789 "));

        assertEquals("00123456", BarcodeTools.getCleansedEan("      123456     "));
        assertEquals("12345678", BarcodeTools.getCleansedEan("      12345678     "));
        assertEquals("0001234567890", BarcodeTools.getCleansedEan("      1234567890     "));
        assertEquals("1234567890123", BarcodeTools.getCleansedEan("      1234567890123     "));
        assertEquals("123456789012345", BarcodeTools.getCleansedEan("      123456789012345     "));

        assertEquals("12345678", BarcodeTools.getCleansedEan("12345678"));
        assertEquals("1234567890123", BarcodeTools.getCleansedEan("1234567890123"));
        assertEquals("123456789012345", BarcodeTools.getCleansedEan("123456789012345"));
        assertEquals("0012345678912", BarcodeTools.getCleansedEan("12345678912"));
        assertEquals("123456789123", BarcodeTools.getCleansedEan("123456789123"));
        assertEquals("1234567891234", BarcodeTools.getCleansedEan("1234567891234"));
    }

    @Test
    public void testIsValidEan() {
        assertFalse(BarcodeTools.isValidEan("  55 "));
        assertTrue(BarcodeTools.isValidEan("159159"));
        assertTrue(BarcodeTools.isValidEan("00159159"));
        assertTrue(BarcodeTools.isValidEan("9300675001427"));

        assertFalse(BarcodeTools.isValidEan("  56 "));
        assertFalse(BarcodeTools.isValidEan("159 159"));
        assertFalse(BarcodeTools.isValidEan("testEan"));
        assertFalse(BarcodeTools.isValidEan("930067500142729"));
        assertFalse(BarcodeTools.isValidEan("9300675 001427"));
        assertFalse(BarcodeTools.isValidEan("9300675001428"));
    }

}

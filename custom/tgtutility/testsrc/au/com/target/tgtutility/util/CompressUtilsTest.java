/**
 * 
 */
package au.com.target.tgtutility.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.Assert;
import org.junit.Test;



/**
 * @author phi.tran
 * 
 */
@UnitTest
public class CompressUtilsTest {

    private static String property = "java.io.tmpdir";
    private static String tempDir = System.getProperty(property);


    @Test
    public void testZipTxtFile() throws IOException {

        final File input = new File(tempDir + "/" + "the-file-name.txt");


        final PrintWriter writer;
        writer = new PrintWriter(input, "UTF-8");
        writer.println("This is the first line");
        writer.println("This is the second line");
        writer.close();

        CompressUtils.compressToZipFormat(tempDir + "/" + "the-file-name.txt", tempDir);

        final File zipfile = new File(tempDir + "/" + "the-file-name.zip");

        Assert.assertEquals(Boolean.valueOf(zipfile.exists()), Boolean.TRUE);

        zipfile.delete();
        input.delete();
    }

    @Test
    public void testZipFileNotExisted() throws IOException {
        CompressUtils.compressToZipFormat(tempDir + "/" + "the-file-name.csv", tempDir);
    }

    @Test
    public void testZipFileZipFileIsExited() throws IOException {

        final File input = new File(tempDir + "/" + "the-file-name.csv");

        final PrintWriter writer;
        writer = new PrintWriter(input, "UTF-8");
        writer.println("de_DE Montag, 1. August 2011 11:58:36 Deutschland");
        writer.println("de_DE Montag, 1. August 2011 11:58:36 MESZ");
        writer.close();

        final File zipTestFile = new File(tempDir + "/" + "the-file-name.zip");
        zipTestFile.createNewFile();

        CompressUtils.compressToZipFormat(tempDir + "/" + "the-file-name.csv", tempDir);

        final File zipfile = new File(tempDir + "/" + "the-file-name.zip");

        Assert.assertEquals(Boolean.valueOf(zipfile.exists()), Boolean.TRUE);

        zipfile.delete();

        input.delete();

    }

    @Test
    public void testZipFileNulle() throws IOException {
        CompressUtils.compressToZipFormat(null, null);

    }

    @Test
    public void testZipFileEmpty() throws IOException {

        final File input = new File(tempDir + "/" + "the-file-name.csv");

        input.createNewFile();

        CompressUtils.compressToZipFormat(tempDir + "/" + "the-file-name.csv", tempDir);

        final File zipfile = new File(tempDir + "/" + "the-file-name.zip");

        Assert.assertEquals(Boolean.valueOf(zipfile.exists()), Boolean.TRUE);

        zipfile.delete();
        input.delete();

    }

    @Test
    public void testZipFileInputZipFile() throws IOException {

        final File input = new File(tempDir + "/" + "the-file-name.csv");


        final PrintWriter writer;
        writer = new PrintWriter(input, "UTF-8");
        writer.println("de_DE Montag, 1. August 2011 11:58:36 Deutschland");
        writer.println("de_DE Montag, 1. August 2011 11:58:36 MESZ");
        writer.close();

        CompressUtils.compressToZipFormat(tempDir + "/" + "the-file-name.csv", tempDir);

        final File zipfile = new File(tempDir + "/" + "the-file-name.zip");

        CompressUtils.compressToZipFormat(tempDir + "/" + "the-file-name.zip", tempDir);

        Assert.assertEquals(Boolean.valueOf(zipfile.exists()), Boolean.TRUE);

        input.delete();
        zipfile.delete();

    }

}

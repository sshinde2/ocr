/**
 * 
 */
package au.com.target.tgtutility.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.ListUtils;
import org.fest.assertions.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;


/**
 * @author pthoma20
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExcludeHttpRequestPathHelperTest {

    @Test
    public void testWhenExcludedPathIsEmpty() {
        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        Assertions.assertThat(ExcludeHttpRequestPathHelper.isExcluded(request, ListUtils.EMPTY_LIST)).isFalse();
    }

    @Test
    public void testWhenForwardedPathIsAnExcludedUrl() {

        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        BDDMockito.given(request.getAttribute("javax.servlet.forward.request_uri"))
                .willReturn("/checkout/your-address");
        Assertions.assertThat(
                ExcludeHttpRequestPathHelper.isExcluded(request, Arrays.asList("/checkout", "/modal")))
                .isTrue();
    }

    @Test
    public void testWhenRequestPathIsAnExcludedUrl() {

        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        BDDMockito.given(request.getServletPath())
                .willReturn("/modal/faqs");
        Assertions.assertThat(
                ExcludeHttpRequestPathHelper.isExcluded(request, Arrays.asList("/checkout", "/modal")))
                .isTrue();
    }

    @Test
    public void testWhenRequestPathIsNotAnExcludedUrl() {

        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        BDDMockito.given(request.getServletPath())
                .willReturn("/p/P1000");
        Assertions.assertThat(
                ExcludeHttpRequestPathHelper.isExcluded(request, Arrays.asList("/checkout", "/modal")))
                .isFalse();
    }

    @Test
    public void testWhenForwardedPathIsNotAnExcludedUrl() {

        final HttpServletRequest request = Mockito.mock(MockHttpServletRequest.class);
        BDDMockito.given(request.getAttribute("javax.servlet.forward.request_uri"))
        .willReturn("/p/P2000");
        Assertions.assertThat(
                ExcludeHttpRequestPathHelper.isExcluded(request, Arrays.asList("/checkout", "/modal")))
                .isFalse();
    }

}

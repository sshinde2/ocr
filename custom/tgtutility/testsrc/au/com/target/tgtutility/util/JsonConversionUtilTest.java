/**
 * 
 */
package au.com.target.tgtutility.util;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;


/**
 * @author mgazal
 *
 */
@UnitTest
public class JsonConversionUtilTest {

    @Test
    public void testConvertToJsonString() {
        final Map<String, String> object = new LinkedHashMap<>();
        object.put("name", "Tony");
        object.put("occupation", "Gun Runner");
        object.put("age", "43");
        assertThat(JsonConversionUtil.convertToJsonString(object))
                .isEqualTo("{\"name\":\"Tony\",\"occupation\":\"Gun Runner\",\"age\":\"43\"}");
    }

    @Test
    public void testConvertToObject() {
        final Map object = JsonConversionUtil
                .convertToObject("{\"age\":\"43\",\"occupation\":\"Gun Runner\",\"name\":\"Tony\"}", Map.class);
        assertThat(object).isNotEmpty().hasSize(3);
        assertThat(object.keySet()).containsOnly("name", "occupation", "age");
        assertThat(object.values()).containsOnly("Tony", "Gun Runner", "43");
    }

    @Test
    public void testConvertToPrettyJsonString() {
        final Map<String, String> object = new LinkedHashMap<>();
        object.put("name", "Tony");
        object.put("occupation", "Gun Runner");
        object.put("age", "43");
        assertThat(JsonConversionUtil.convertToPrettyJsonString(object))
                .isEqualTo("{\n" +
                        "  \"name\" : \"Tony\",\n" +
                        "  \"occupation\" : \"Gun Runner\",\n" +
                        "  \"age\" : \"43\"\n" +
                        "}");
    }
}

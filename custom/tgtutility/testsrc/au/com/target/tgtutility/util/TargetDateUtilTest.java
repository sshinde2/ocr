package au.com.target.tgtutility.util;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.time.DateUtils;
import org.fest.assertions.Assertions;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeFieldType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


@UnitTest
public class TargetDateUtilTest {

    private Date monday;
    private Date tuesday;
    private Date wednesday;
    private Date thursday;
    private Date friday;
    private Date saturday;
    private Date sunday;
    private Date nextMonday;

    @Before
    public void setup() {

        final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            monday = dateformat.parse("2018-01-01T05:00:00");
            tuesday = dateformat.parse("2018-01-02T00:00:00");
            wednesday = dateformat.parse("2018-01-3T00:00:00");
            thursday = dateformat.parse("2018-01-04T00:00:00");
            friday = dateformat.parse("2018-01-05T00:00:00");
            saturday = dateformat.parse("2018-01-6T10:30:55");
            sunday = dateformat.parse("2018-01-07T10:30:55");
            nextMonday = dateformat.parse("2018-01-08T00:00:00");
        }
        catch (final ParseException e) {
            //Ignore. Unreachable.
        }

    }

    //CHECKSTYLE:OFF
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON



    @Test
    public void testGetDateFormattedAsStringNullDate() {

        final String date = TargetDateUtil.getDateFormattedAsString(null);
        assertThat(date).isNull();
    }

    @Test
    public void testGetDateFormattedAsString() {

        final Date test = new Date(999999);
        final String date = TargetDateUtil.getDateFormattedAsString(test);
        assertThat(date).isNotNull().isEqualTo("999999");
    }

    @Test
    public void testGetDateStringForComparison() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2015);
        calendar.set(Calendar.MONTH, 0);// month starts with 0
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        assertThat(TargetDateUtil.getDateStringForComparison(calendar.getTime())).isEqualTo("2015-01-01");
    }

    @Test
    public void testGetDateWithMidnightTime() {
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss SSS");

        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.YEAR, 2000);
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MINUTE, 1);
        calendar.set(Calendar.SECOND, 1);
        calendar.set(Calendar.MILLISECOND, 1);

        final Date actDate = calendar.getTime();
        assertThat(sdf.format(actDate)).as("01:01:01 001");

        final Date expDate = TargetDateUtil.getDateWithMidnightTime(actDate);

        assertThat(DateUtils.isSameDay(expDate, actDate)).isTrue();
        assertThat(sdf.format(expDate)).as("00:00:00 000");
    }

    @Test
    public void testGetDateWithMidnightTimeNull() {
        final Date actDate = new Date();
        final Date expDate = TargetDateUtil.getDateWithMidnightTime(null);

        assertThat(DateUtils.isSameDay(expDate, actDate)).isTrue();

        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss SSS");
        assertThat(sdf.format(expDate)).as("00:00:00 000");
    }

    @Test
    public void testDifferenceTwoDateWhenDiffMoreThanOneYear() {
        final Date date1 = new Date();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -368);
        final Date date2 = calendar.getTime();
        assertThat(TargetDateUtil.getDifferenceBetweenDateInDay(date1, date2)).isEqualTo(368);
    }

    @Test
    public void testDifferenceTwoDateWhenDiffLessThanOneYear() {
        final Date date1 = new Date();
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -5);
        final Date date2 = calendar.getTime();
        assertThat(TargetDateUtil.getDifferenceBetweenDateInDay(date1, date2)).isEqualTo(5);
    }

    @Test
    public void testGetStringAsDateNull() {
        assertThat(TargetDateUtil.getStringAsDate(null)).isNull();
    }

    @Test
    public void testGetStringAsDateEmpty() {
        assertThat(TargetDateUtil.getStringAsDate("")).isNull();
    }

    @Test
    public void testGetStringAsDateBlank() {
        assertThat(TargetDateUtil.getStringAsDate("     ")).isNull();
    }

    @Test
    public void testGetStringAsDate() {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone(TimeZoneUtils.TIMEZONE_GMT));
        calendar.set(Calendar.YEAR, 2015);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 30);
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 20);
        calendar.set(Calendar.SECOND, 30);

        final Date parsedDate = TargetDateUtil.getStringAsDate("2015-12-30T10:20:30");

        //comparing date and time by ignoring milisecs
        final DateTimeComparator dtComparator = DateTimeComparator.getInstance(DateTimeFieldType.secondOfDay());
        assertThat(dtComparator.compare(parsedDate, calendar.getTime())).isEqualTo(0);
    }

    @Test
    public void testGetStringAsDateWithFormat() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2015);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 30);
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 20);
        calendar.set(Calendar.SECOND, 30);
        calendar.set(Calendar.MILLISECOND, 300);

        final Date parsedDate = TargetDateUtil.getStringAsDate("2015-12-30 10:20:30.300", "yyyy-MM-dd HH:mm:ss.S");

        final DateTimeComparator dtComparator = DateTimeComparator.getInstance();
        assertThat(dtComparator.compare(parsedDate, calendar.getTime())).isEqualTo(0);
    }

    @Test
    public void testGetDateforTimeinMillis() {
        final String timeInMilliSeconds = "969662700000";
        final Date date = TargetDateUtil.getDateForTimeInMillis(timeInMilliSeconds);
        assertThat(date).isNotNull();
    }

    @Test
    public void testGetDateforTimeStampForInvalidTimeStamp() {
        final String timeInMilliSeconds = "9696627A00000";
        final Date date = TargetDateUtil.getDateForTimeInMillis(timeInMilliSeconds);
        Assertions.assertThat(date).isNull();
    }

    @Test
    public void testGetCurrentFormattedDate() {
        final String date = TargetDateUtil.getCurrentFormattedDate();
        assertThat(date.length()).isGreaterThan(20);
    }


    @Test
    public void testgetDateAsStringWithTimeZone() {
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.set(Calendar.YEAR, 2015);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 30);
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 20);
        calendar.set(Calendar.SECOND, 30);
        calendar.set(Calendar.MILLISECOND, 0);
        final String date = TargetDateUtil.getDateAsString(calendar.getTime(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                TimeZoneUtils.TIMEZONE_UTC);
        assertThat(date).isNotNull();
        assertThat(date).isEqualTo("2015-12-30T10:20:30.000Z");

    }

    @Test
    public void testgetDateAsStringWithDiffTimeZone() {
        final Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CST"));
        calendar.set(Calendar.YEAR, 2015);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 30);
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 20);
        calendar.set(Calendar.SECOND, 30);
        calendar.set(Calendar.MILLISECOND, 0);
        final String date = TargetDateUtil.getDateAsString(calendar.getTime(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                TimeZoneUtils.TIMEZONE_GMT);
        assertThat(date).isNotNull();
        assertThat(date).isEqualTo("2015-12-30T16:20:30.000Z");

    }

    @Test
    public void testgetDateAsStringWithNullDate() {
        final String date = TargetDateUtil.getDateAsString(null, "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                TimeZoneUtils.TIMEZONE_UTC);
        assertThat(date).isNull();

    }

    @Test
    public void testGetNextWeekDateThrowsIllegalArgumentExceptionForNullDate() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("The date must not be null");
        TargetDateUtil.getNextWeekDate(null);
    }

    @Test
    public void testGetNextWeekDateReturnsCorrectDate() throws ParseException {

        Date nextDate;

        nextDate = TargetDateUtil.getNextWeekDate(monday);
        assertThat(tuesday.compareTo(nextDate)).isEqualTo(0);

        nextDate = TargetDateUtil.getNextWeekDate(tuesday);
        assertThat(wednesday.compareTo(nextDate)).isEqualTo(0);

        nextDate = TargetDateUtil.getNextWeekDate(wednesday);
        assertThat(thursday.compareTo(nextDate)).isEqualTo(0);


        nextDate = TargetDateUtil.getNextWeekDate(thursday);
        assertThat(friday.compareTo(nextDate)).isEqualTo(0);

        nextDate = TargetDateUtil.getNextWeekDate(friday);
        assertThat(nextMonday.compareTo(nextDate)).isEqualTo(0);

        nextDate = TargetDateUtil.getNextWeekDate(saturday);
        assertThat(nextMonday.compareTo(nextDate)).isEqualTo(0);

        nextDate = TargetDateUtil.getNextWeekDate(sunday);
        assertThat(nextMonday.compareTo(nextDate)).isEqualTo(0);


    }

    @Test
    public void testIsWeekDayThrowsIllegalArgumentExceptionForNullDate() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("The date must not be null");
        TargetDateUtil.isWeekDay(null);
    }

    @Test
    public void testIsWeekDayReturnsCorrectValuesForWeekdaysAndWeekends() throws ParseException {

        assertThat(TargetDateUtil.isWeekDay(monday)).isEqualTo(true);
        assertThat(TargetDateUtil.isWeekDay(tuesday)).isEqualTo(true);
        assertThat(TargetDateUtil.isWeekDay(wednesday)).isEqualTo(true);
        assertThat(TargetDateUtil.isWeekDay(thursday)).isEqualTo(true);
        assertThat(TargetDateUtil.isWeekDay(friday)).isEqualTo(true);
        assertThat(TargetDateUtil.isWeekDay(saturday)).isEqualTo(false);
        assertThat(TargetDateUtil.isWeekDay(sunday)).isEqualTo(false);

    }

    @Test
    public void testAddDaysReturnsNextDate() {
        assertThat(TargetDateUtil.addDays(saturday, 1)).isEqualTo(sunday);
    }

    @Test
    public void testAddDaysReturnsNullWhenCalledWithANullDate() {
        assertThat(TargetDateUtil.addDays(null, 1)).isEqualTo(null);
    }


    @Test
    public void testGetLastMilliSecondOfDateReturnsNullWhenCalledWithANullDate() {
        assertThat(TargetDateUtil.getLastMilliSecondOfDate(null)).isEqualTo(null);
    }

    @Test
    public void testGetLastMilliSecondOfDateReturnsCorrectValues() throws ParseException {

        final SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        try {
            final Date lastMilliSecondOfTuesday = dateformat.parse("2018-01-02T23:59:59.999");
            final Date lastMilliSecondOfSaturday = dateformat.parse("2018-01-06T23:59:59.999");
            assertThat(TargetDateUtil.getLastMilliSecondOfDate(tuesday)).isEqualTo(lastMilliSecondOfTuesday);
            assertThat(TargetDateUtil.getLastMilliSecondOfDate(saturday)).isEqualTo(lastMilliSecondOfSaturday);
        }
        catch (final ParseException e) {
            //Ignore. Unreachable.
        }



    }

}

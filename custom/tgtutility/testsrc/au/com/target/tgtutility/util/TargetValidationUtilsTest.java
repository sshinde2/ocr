package au.com.target.tgtutility.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;



/**
 * Unit test for {@link TargetValidationUtils}
 */
@UnitTest
public final class TargetValidationUtilsTest {

    @Test
    public void testMatchExpression() {
        Assert.assertTrue("Not match",
                TargetValidationUtils.matchRegularExpression("123456789", Pattern.compile("^(\\d|\\+|\\-|\\(|\\))*$"),
                        8,
                        16, false));
    }

    @Test
    public void testMatchExpressionFalse() {
        Assert.assertFalse("Match",
                TargetValidationUtils.matchRegularExpression("ABC123456", Pattern.compile("^(\\d|\\+|\\-|\\(|\\))*$"),
                        8,
                        16, false));
    }

    @Test
    public void testOptionalTrue() {
        Assert.assertTrue(TargetValidationUtils.checkOptional("12345abc", false));
    }

    @Test
    public void testOptionalFalse() {
        Assert.assertFalse(TargetValidationUtils.checkOptional("", false));
    }

}

/**
 * 
 */
package au.com.target.tgtutility.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Test;


/**
 * @author bhuang3
 * 
 */
@UnitTest
public class TimeZoneUtilsTest {

    @Test
    public void testGetTimeZoneFromStateForVic() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/Victoria"), TimeZoneUtils.getTimeZoneFromState("vic"));
    }

    @Test
    public void testGetTimeZoneFromStateForVictoria() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/Victoria"), TimeZoneUtils.getTimeZoneFromState("victoria"));
    }

    @Test
    public void testGetTimeZoneFromStateForNSW() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/NSW"), TimeZoneUtils.getTimeZoneFromState("nsw"));
    }

    @Test
    public void testGetTimeZoneFromStateForNewSouthWales() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/NSW"),
                TimeZoneUtils.getTimeZoneFromState("New South Wales"));
    }

    @Test
    public void testGetTimeZoneFromStateForQld() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/Queensland"),
                TimeZoneUtils.getTimeZoneFromState("qld"));
    }

    @Test
    public void testGetTimeZoneFromStateForQueensland() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/Queensland"),
                TimeZoneUtils.getTimeZoneFromState("Queensland"));
    }

    @Test
    public void testGetTimeZoneFromStateForSouthAus() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/South"),
                TimeZoneUtils.getTimeZoneFromState("South Australia"));
    }

    @Test
    public void testGetTimeZoneFromStateForSA() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/South"),
                TimeZoneUtils.getTimeZoneFromState("Sa"));
    }

    @Test
    public void testGetTimeZoneFromStateForTAS() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/Tasmania"),
                TimeZoneUtils.getTimeZoneFromState("TAS"));
    }

    @Test
    public void testGetTimeZoneFromStateForTasmania() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/Tasmania"),
                TimeZoneUtils.getTimeZoneFromState("Tasmania"));
    }

    @Test
    public void testGetTimeZoneFromStateForWA() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/West"),
                TimeZoneUtils.getTimeZoneFromState("WA"));
    }

    @Test
    public void testGetTimeZoneFromStateForWesternAustralia() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/West"),
                TimeZoneUtils.getTimeZoneFromState("Western Australia"));
    }

    @Test
    public void testGetTimeZoneFromStateForAustralianCapitalTerritory() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/ACT"),
                TimeZoneUtils.getTimeZoneFromState("Australian Capital Territory"));
    }

    @Test
    public void testGetTimeZoneFromStateForACT() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/ACT"),
                TimeZoneUtils.getTimeZoneFromState("ACT"));
    }

    @Test
    public void testGetTimeZoneFromStateForNT() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/North"),
                TimeZoneUtils.getTimeZoneFromState("NT"));
    }

    @Test
    public void testGetTimeZoneFromStateForNorthernTerritory() {
        Assert.assertEquals(TimeZone.getTimeZone("Australia/North"),
                TimeZoneUtils.getTimeZoneFromState("Northern Territory"));
    }

    @Test
    public void testFormatTimeZoneOffsetForSmsWithVic() {
        Assert.assertEquals("+1000",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Australia/Victoria")));
    }

    @Test
    public void testFormatTimeZoneOffsetForSmsWithNT() {
        Assert.assertEquals("+0930",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Australia/North")));
    }

    @Test
    public void testFormatTimeZoneOffsetForSmsWithNsw() {
        Assert.assertEquals("+1000",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Australia/NSW")));
    }

    @Test
    public void testFormatTimeZoneOffsetForSmsWithQld() {
        Assert.assertEquals("+1000",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Australia/Queensland")));
    }

    @Test
    public void testFormatTimeZoneOffsetForSmsWithSa() {
        Assert.assertEquals("+0930",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Australia/South")));
    }

    @Test
    public void testFormatTimeZoneOffsetForSmsWithTas() {
        Assert.assertEquals("+1000",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Australia/Tasmania")));
    }

    @Test
    public void testFormatTimeZoneOffsetForSmsWithWa() {
        Assert.assertEquals("+0800",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Australia/West")));
    }

    @Test
    public void testFormatTimeZoneOffsetForSmsWithACT() {
        Assert.assertEquals("+1000",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Australia/ACT")));
    }

    @Test
    public void testFormatTimeZoneOffsetForSms() {
        Assert.assertEquals("-0600",
                TimeZoneUtils.formatTimeZoneOffsetForSms(TimeZone.getTimeZone("Mexico/General")));
    }

}

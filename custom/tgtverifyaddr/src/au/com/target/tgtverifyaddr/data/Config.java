/**
 * 
 */
package au.com.target.tgtverifyaddr.data;


/**
 * @author vmuthura
 * 
 */
public class Config
{

    private String wsdlUrl = null;
    private String qName = null;
    private String serviceName = null;
    private String userName = null;
    private String password = null;

    private boolean flatten = true;
    private String engineType = null;
    private String intensity = null;
    private String layout = null;
    private String formattedLayout = null;
    private String localisation = null;
    private String promptset = null;
    private int threshold = 50;
    private int timeout = 60;
    private String requestTag = null;
    private boolean formattedAddressInPicklist = true;

    /**
     * @return the wsdlUrl
     */
    public String getWsdlUrl()
    {
        return wsdlUrl;
    }

    /**
     * @param wsdlUrl
     *            the wsdlUrl to set
     */
    public void setWsdlUrl(final String wsdlUrl)
    {
        this.wsdlUrl = wsdlUrl;
    }

    /**
     * @return the qName
     */
    public String getqName()
    {
        return qName;
    }

    /**
     * @param qName
     *            the qName to set
     */
    public void setqName(final String qName)
    {
        this.qName = qName;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName()
    {
        return serviceName;
    }

    /**
     * @param serviceName
     *            the serviceName to set
     */
    public void setServiceName(final String serviceName)
    {
        this.serviceName = serviceName;
    }

    /**
     * @return the flatten
     */
    public boolean isFlatten()
    {
        return flatten;
    }

    /**
     * @param flatten
     *            the flatten to set
     */
    public void setFlatten(final boolean flatten)
    {
        this.flatten = flatten;
    }

    /**
     * @return the engineType
     */
    public String getEngineType()
    {
        return engineType;
    }

    /**
     * @param engineType
     *            the engineType to set
     */
    public void setEngineType(final String engineType)
    {
        this.engineType = engineType;
    }

    /**
     * @return the intensity
     */
    public String getIntensity()
    {
        return intensity;
    }

    /**
     * @param intensity
     *            the intensity to set
     */
    public void setIntensity(final String intensity)
    {
        this.intensity = intensity;
    }

    /**
     * @return the layout
     */
    public String getLayout()
    {
        return layout;
    }

    /**
     * @param layout
     *            the layout to set
     */
    public void setLayout(final String layout)
    {
        this.layout = layout;
    }

    /**
     * @return the localisation
     */
    public String getLocalisation()
    {
        return localisation;
    }

    /**
     * @param localisation
     *            the localisation to set
     */
    public void setLocalisation(final String localisation)
    {
        this.localisation = localisation;
    }

    /**
     * @return the promptset
     */
    public String getPromptset()
    {
        return promptset;
    }

    /**
     * @param promptset
     *            the promptset to set
     */
    public void setPromptset(final String promptset)
    {
        this.promptset = promptset;
    }

    /**
     * @return the requestTag
     */
    public String getRequestTag()
    {
        return requestTag;
    }

    /**
     * @param requestTag
     *            the requestTag to set
     */
    public void setRequestTag(final String requestTag)
    {
        this.requestTag = requestTag;
    }

    /**
     * @return the formattedAddressInPicklist
     */
    public boolean isFormattedAddressInPicklist()
    {
        return formattedAddressInPicklist;
    }

    /**
     * @param formattedAddressInPicklist
     *            the formattedAddressInPicklist to set
     */
    public void setFormattedAddressInPicklist(final boolean formattedAddressInPicklist)
    {
        this.formattedAddressInPicklist = formattedAddressInPicklist;
    }

    /**
     * @return the threshold
     */
    public int getThreshold()
    {
        return threshold;
    }

    /**
     * @param threshold
     *            the threshold to set
     */
    public void setThreshold(final int threshold)
    {
        this.threshold = threshold;
    }

    /**
     * @return the timeout
     */
    public int getTimeout()
    {
        return timeout;
    }

    /**
     * @param timeout
     *            the timeout to set
     */
    public void setTimeout(final int timeout)
    {
        this.timeout = timeout;
    }

    /**
     * @return the userName
     */
    public String getUserName()
    {
        return userName;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(final String userName)
    {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(final String password)
    {
        this.password = password;
    }

    /**
     * @return the formattedLayout
     */
    public String getFormattedLayout() {
        return formattedLayout;
    }

    /**
     * @param formattedLayout
     *            the formattedLayout to set
     */
    public void setFormattedLayout(final String formattedLayout) {
        this.formattedLayout = formattedLayout;
    }

}

/**
 * 
 */
package au.com.target.tgtverifyaddr.data;



/**
 * @author bjames4
 * 
 */
public class FormattedAddressData {

    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String postcode;

    // True if any of the lines have been truncated 
    private Boolean truncated;

    /**
     * @return the truncated
     */

    public Boolean getTruncated() {
        return truncated;
    }

    /**
     * @param truncated
     *            the truncated to set
     */
    public void setTruncated(final Boolean truncated) {
        this.truncated = truncated;
    }

    /**
     * @return the addressLine1
     */
    public String getAddressLine1() {
        return addressLine1;
    }

    /**
     * @param addressLine1
     *            the addressLine1 to set
     */
    public void setAddressLine1(final String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    /**
     * @return the addressLine2
     */
    public String getAddressLine2() {
        return addressLine2;
    }

    /**
     * @param addressLine2
     *            the addressLine2 to set
     */
    public void setAddressLine2(final String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode
     *            the postcode to set
     */
    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }
}

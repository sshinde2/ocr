/**
 * 
 */
package au.com.target.tgtverifyaddr.exception;

/**
 * @author vmuthuraman
 * 
 */
public class AddressVerificationException extends Exception
{
    /**
     * @param message
     *            Meaningful message to accompany the exception
     */
    public AddressVerificationException(final String message)
    {
        super(message);
    }

    /**
     * @param exception
     *            Exception to be wrapped
     */
    public AddressVerificationException(final Exception exception)
    {
        super(exception);
    }

    /**
     * @param message
     *            Meaningful message to accompany the exception
     * @param exception
     *            Exception to be wrapped
     */
    public AddressVerificationException(final String message, final Exception exception)
    {
        super(message, exception);
    }
}

/**
 * 
 */
package au.com.target.tgtverifyaddr.exception;


/**
 * @author vmuthura
 * 
 */
public class InitializationFailedException extends AddressVerificationException
{

    /**
     * @param exception
     *            Exception to be wrapped
     */
    public InitializationFailedException(final Exception exception)
    {
        super(exception);
    }
}

/**
 * 
 */
package au.com.target.tgtverifyaddr.exception;

/**
 * @author vmuthura
 * 
 */
public class TooManyMatchesFoundException extends AddressVerificationException
{
    /**
     * @param message
     *            Meaningful message to accompany the exception
     */
    public TooManyMatchesFoundException(final String message)
    {
        super(message);
    }

    /**
     * @param exception
     *            Exception to be wrapped
     */
    public TooManyMatchesFoundException(final Exception exception)
    {
        super(exception);
    }

    /**
     * @param message
     *            Meaningful message to accompany the exception
     * @param exception
     *            Exception to be wrapped
     */
    public TooManyMatchesFoundException(final String message, final Exception exception)
    {
        super(message, exception);
    }

}

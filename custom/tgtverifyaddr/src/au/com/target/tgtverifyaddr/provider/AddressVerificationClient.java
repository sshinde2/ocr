/**
 * 
 */
package au.com.target.tgtverifyaddr.provider;

//CHECKSTYLE:OFF

import java.util.List;

import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.InitializationFailedException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;


//CHECKSTYLE:ON

/**
 * @author vmuthura
 * 
 */
public interface AddressVerificationClient
{

    /**
     * @throws InitializationFailedException
     *             - if the initialization failed
     */
    void init() throws InitializationFailedException;

    /**
     * @param countryId
     *            - Country code {@link CountryEnum}
     * @return boolean - True, if there is a valid license
     * @throws ServiceNotAvailableException
     *             - if the service is not reachable
     */
    boolean isSearchAvailable(final String countryId) throws ServiceNotAvailableException;

    /**
     * @param address
     *            - Address to be validated
     * @param countryId
     *            - Country code {@link CountryEnum}
     * @return - a List of suggestions or the formatted address
     * @throws ServiceNotAvailableException
     *             - if the service is not reachable
     * @throws RequestTimeOutException
     *             - if the request times out
     * @throws TooManyMatchesFoundException
     *             - if the matches exceed the threshold
     */
    List<AddressData> search(final String address, final String countryId) throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException;

    /**
     * 
     * @param address
     * @param countryId
     * @param searchType
     * @return - a List of suggestions or the formatted address
     * @throws ServiceNotAvailableException
     * @throws RequestTimeOutException
     * @throws TooManyMatchesFoundException
     */
    List<AddressData> search(final String address, final String countryId, final String searchType)
            throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException;

    /**
     * @param unformattedAddress
     *            as AddressData
     * @return formatted address as String
     * @throws ServiceNotAvailableException
     *             - on null response from QAS
     */
    FormattedAddressData formatAddress(AddressData unformattedAddress) throws ServiceNotAvailableException;

}
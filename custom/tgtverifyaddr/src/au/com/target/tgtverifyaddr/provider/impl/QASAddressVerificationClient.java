/**
 * 
 */
package au.com.target.tgtverifyaddr.provider.impl;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Holder;
import javax.xml.ws.Service;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.qas.ondemand_2011_03.Address;
import com.qas.ondemand_2011_03.AddressLineType;
import com.qas.ondemand_2011_03.EngineEnumType;
import com.qas.ondemand_2011_03.EngineIntensityType;
import com.qas.ondemand_2011_03.EngineType;
import com.qas.ondemand_2011_03.PicklistEntryType;
import com.qas.ondemand_2011_03.PromptSetType;
import com.qas.ondemand_2011_03.QAAuthentication;
import com.qas.ondemand_2011_03.QACanSearch;
import com.qas.ondemand_2011_03.QAGetAddress;
import com.qas.ondemand_2011_03.QAInformation;
import com.qas.ondemand_2011_03.QAPicklistType;
import com.qas.ondemand_2011_03.QAPortType;
import com.qas.ondemand_2011_03.QAQueryHeader;
import com.qas.ondemand_2011_03.QASearch;
import com.qas.ondemand_2011_03.QASearchOk;
import com.qas.ondemand_2011_03.QASearchResult;

import au.com.target.tgtverifyaddr.constants.TgtverifyaddrConstants;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.Config;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.InitializationFailedException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;
import au.com.target.tgtverifyaddr.provider.AddressVerificationClient;


/**
 * @author vmuthura
 * 
 */
public class QASAddressVerificationClient implements AddressVerificationClient {
    private static final Logger LOG = Logger.getLogger(QASAddressVerificationClient.class);

    @Autowired(required = true)
    private Config config;

    private QAPortType qaPortType = null;
    private EngineType singlelineEngineType = null;
    private EngineType intuitiveEngineType = null;
    private QAQueryHeader qaQueryHeader = null;

    private final Holder<QAInformation> holder = new Holder<>();


    @Override
    public void init() throws InitializationFailedException {
        qaPortType = initQAPortType();

        final QAAuthentication qaAuthentication = new QAAuthentication();
        qaAuthentication.setUsername(config.getUserName());
        qaAuthentication.setPassword(config.getPassword());

        qaQueryHeader = new QAQueryHeader();
        qaQueryHeader.setQAAuthentication(qaAuthentication);

        singlelineEngineType = initEngine(EngineEnumType.SINGLELINE);
        intuitiveEngineType = initEngine(EngineEnumType.INTUITIVE);

    }

    @Override
    public boolean isSearchAvailable(final String countryId) throws ServiceNotAvailableException {
        QASearchOk qaSearchOk = null;
        try {
            if (qaPortType == null) {
                init();
            }

            final QACanSearch param = new QACanSearch();
            param.setCountry(countryId);
            param.setEngine(singlelineEngineType);
            param.setLayout(config.getLayout());
            param.setLocalisation(config.getLocalisation());

            qaSearchOk = qaPortType.doCanSearch(param, qaQueryHeader, holder);
        }
        catch (final Exception e) {
            LOG.error("ERR-QAS-SERVICENOTAVAILABLE - Error communicating with QAS for isSearchAvailable: ", e);
        }

        if (qaSearchOk == null) {
            throw new ServiceNotAvailableException("Received null reply from the QAS service");
        }

        if (!qaSearchOk.isIsOk()) {
            LOG.warn("Trying to search for address in " + countryId + " returned: " + qaSearchOk.getErrorMessage());
        }

        return qaSearchOk.isIsOk();
    }

    @Override
    public List<AddressData> search(final String address, final String countryId) throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        return search(address, countryId, EngineEnumType.SINGLELINE.value());
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtverifyaddr.provider.AddressVerificationClient#search(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public List<AddressData> search(final String address, final String countryId, final String searchType)
            throws ServiceNotAvailableException, RequestTimeOutException, TooManyMatchesFoundException {
        if (qaPortType == null) {
            try {
                init();
            }
            catch (final InitializationFailedException e) {
                LOG.error("Cannot initialize QAS client", e);
                throw new ServiceNotAvailableException(e.getMessage());
            }
        }
        EngineType engineType = singlelineEngineType;
        if (EngineEnumType.INTUITIVE.value().equalsIgnoreCase(searchType)) {
            engineType = intuitiveEngineType;
        }
        return searchAddress(address, countryId, engineType);
    }

    @Override
    public FormattedAddressData formatAddress(final AddressData unformattedAddress)
            throws ServiceNotAvailableException {
        final QAGetAddress qaGetAddress = new QAGetAddress();
        qaGetAddress.setLayout(config.getFormattedLayout());
        qaGetAddress.setMoniker(unformattedAddress.getMoniker());

        Address formattedAddress = null;
        try {
            if (qaPortType == null) {
                init();
            }
            formattedAddress = qaPortType.doGetAddress(qaGetAddress, qaQueryHeader, holder);
        }
        catch (final Exception ex) {
            LOG.error("ERR-QAS-SERVICENOTAVAILABLE - Error communicating with QAS for doGetAddress: ", ex);
            throw new ServiceNotAvailableException(ex.getMessage());
        }

        if (formattedAddress == null) {
            throw new ServiceNotAvailableException("Received null reply from the QAS service");
        }

        final FormattedAddressData formattedAddressData = new FormattedAddressData();
        final List<String> addressLines = new ArrayList<>();
        boolean truncated = false;
        for (final AddressLineType line : formattedAddress.getQAAddress().getAddressLine()) {
            addressLines.add(line.getLine());
            if (TgtverifyaddrConstants.ADDRESS_LINE_1.equalsIgnoreCase(line.getLabel())) {
                formattedAddressData.setAddressLine1(line.getLine());
            }
            else if (TgtverifyaddrConstants.ADDRESS_LINE_2.equalsIgnoreCase(line.getLabel())) {
                formattedAddressData.setAddressLine2(line.getLine());
            }
            else if (TgtverifyaddrConstants.ADDRESS_LINE_3.equalsIgnoreCase(line.getLabel())) {
                final String[] tokens = line.getLine().split("  ");
                if (tokens != null && tokens.length == 3) {
                    formattedAddressData.setCity(tokens[0]);
                    formattedAddressData.setState(tokens[1]);
                    formattedAddressData.setPostcode(tokens[2]);
                }
            }

            if (line.isTruncated()) {
                truncated = true;
            }
        }
        formattedAddressData.setTruncated(Boolean.valueOf(truncated));

        checkAndUpdateAddressLine1(formattedAddressData);

        return formattedAddressData;
    }

    /**
     * @param formattedAddressData
     */
    private void checkAndUpdateAddressLine1(final FormattedAddressData formattedAddressData) {
        final String addressLine1 = formattedAddressData.getAddressLine1();
        final String addressLine2 = formattedAddressData.getAddressLine2();
        if (StringUtils.isEmpty(addressLine1)) {
            if (StringUtils.isNotEmpty(addressLine2)) {
                LOG.warn("QAS returns bad data, AddressLine1 is empty, replace it with AddressLine2:"
                        + addressLine2);
                formattedAddressData.setAddressLine1(addressLine2);
                formattedAddressData.setAddressLine2(null);
            }
            else {
                LOG.error("QAS returns bad data, no AddressLine1 or AddressLine2");
            }
        }
    }

    private List<AddressData> searchAddress(final String address, final String countryId, final EngineType engineType)
            throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final List<AddressData> suggestions = new ArrayList<>();

        QASearchResult searchResult = null;
        try {
            final QASearch param = new QASearch();
            param.setCountry(countryId);
            param.setEngine(engineType);
            param.setLayout(config.getLayout());
            param.setLocalisation(config.getLocalisation());
            param.setFormattedAddressInPicklist(Boolean.valueOf(config.isFormattedAddressInPicklist()));
            param.setRequestTag("");
            param.setSearch(address);

            searchResult = qaPortType.doSearch(param, qaQueryHeader, holder);
        }
        catch (final Exception ex) {
            LOG.error("ERR-QAS-SERVICENOTAVAILABLE - Error communicating with QAS for doSearch: ", ex);
            throw new ServiceNotAvailableException(ex.getMessage());
        }

        if (searchResult == null) {
            throw new ServiceNotAvailableException("Received null reply from the QAS service");
        }

        final QAPicklistType qaPickList = searchResult.getQAPicklist();
        if (qaPickList.isTimeout()) {
            throw new RequestTimeOutException("Request to service timed out");
        }
        //MaxMatches returns a valid picklist, but with a warning(Search cancelled (too many matches)) as an entry.
        if (qaPickList.isOverThreshold() || qaPickList.isMaxMatches()) {
            throw new TooManyMatchesFoundException("Too many matches found");
        }

        AddressData addressData;
        for (final PicklistEntryType pickListEntry : qaPickList.getPicklistEntry()) {
            if (StringUtils.isNotEmpty(pickListEntry.getMoniker())) {
                addressData = new AddressData(pickListEntry.getMoniker(), pickListEntry.getPartialAddress(),
                        pickListEntry
                                .getPostcode());
                addressData.setScore(pickListEntry.getScore().intValue());
                suggestions.add(addressData);
            }
        }

        return suggestions;
    }

    /**
     * @throws InitializationFailedException
     */
    protected QAPortType initQAPortType() throws InitializationFailedException {
        final URL url;
        try {
            url = new URL(config.getWsdlUrl());
            final QName qname = new QName(config.getqName(), config.getServiceName());
            final Service service = Service.create(url, qname);
            return service.getPort(QAPortType.class);
        }
        catch (final Exception e) {
            LOG.error("Failed to init QAS service.", e);
            throw new InitializationFailedException(e);
        }
    }

    /**
     * 
     */
    private EngineType initEngine(final EngineEnumType value) {
        final EngineType engineType = new EngineType();
        engineType.setPromptSet(PromptSetType.fromValue(config.getPromptset()));
        engineType.setIntensity(EngineIntensityType.fromValue(config.getIntensity()));
        engineType.setFlatten(Boolean.valueOf(config.isFlatten()));
        engineType.setThreshold(Integer.valueOf(config.getThreshold()));
        engineType.setTimeout(Integer.valueOf(config.getTimeout()));
        engineType.setValue(value);
        return engineType;
    }

    /**
     * @return the config
     */
    public Config getConfig() {
        return config;
    }

    /**
     * @return the qaPortType
     */
    public QAPortType getQaPortType() {
        return qaPortType;
    }

    /**
     * @return the singlelineEngineType
     */
    public EngineType getSinglelineEngineType() {
        return singlelineEngineType;
    }

    /**
     * @return the intuitiveEngineType
     */
    public EngineType getIntuitiveEngineType() {
        return intuitiveEngineType;
    }

    /**
     * @return the qaQueryHeader
     */
    public QAQueryHeader getQaQueryHeader() {
        return qaQueryHeader;
    }

    /**
     * @return the holder
     */
    public Holder<QAInformation> getHolder() {
        return holder;
    }


}

/**
 * 
 */
package au.com.target.tgtverifyaddr.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtverifyaddr.TargetAddressVerificationService;
import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.Config;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;
import au.com.target.tgtverifyaddr.provider.AddressVerificationClient;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetAddressVerificationServiceImplTest {

    @Mock
    private Config config;
    @Mock
    private AddressVerificationClient qasAddressVerificationClient;
    @InjectMocks
    private final TargetAddressVerificationService targetAddressVerificationService = new TargetAddressVerificationServiceImpl();

    /**
     * Test method for {@link TargetAddressVerificationServiceImpl#verifyAddress(java.lang.String, CountryEnum)} .
     * 
     * @throws NoMatchFoundException
     *             - If no match is found
     * @throws RequestTimeOutException
     *             - If the server fails to respond with in timeout
     * @throws TooManyMatchesFoundException
     *             - If the response exceeds the threshold
     * @throws ServiceNotAvailableException
     *             - If the service fails to respond
     */
    @Test(expected = IllegalArgumentException.class)
    public void testVerifyAddressWithNullAddress() throws NoMatchFoundException, RequestTimeOutException,
            TooManyMatchesFoundException, ServiceNotAvailableException {
        targetAddressVerificationService.verifyAddress(null, CountryEnum.AUSTRALIA);
    }

    /**
     * @throws NoMatchFoundException
     *             - If no match is found
     * @throws RequestTimeOutException
     *             - If the server fails to respond with in timeout
     * @throws TooManyMatchesFoundException
     *             - If the response exceeds the threshold
     * @throws ServiceNotAvailableException
     *             - If the service fails to respond
     */
    @Test(expected = IllegalArgumentException.class)
    public void testVerifyAddressWithNullCountry() throws NoMatchFoundException, RequestTimeOutException,
            TooManyMatchesFoundException, ServiceNotAvailableException {
        targetAddressVerificationService.verifyAddress(null, CountryEnum.AUSTRALIA);
    }

    /**
     * @throws NoMatchFoundException
     *             - If no match is found
     * @throws RequestTimeOutException
     *             - If the server fails to respond with in timeout
     * @throws TooManyMatchesFoundException
     *             - If the response exceeds the threshold
     * @throws ServiceNotAvailableException
     *             - If the service fails to respond
     */
    @Test(expected = IllegalArgumentException.class)
    public void testVerifyAddressWithNullArguments() throws NoMatchFoundException, RequestTimeOutException,
            TooManyMatchesFoundException, ServiceNotAvailableException {
        targetAddressVerificationService.verifyAddress(null, null);
    }

    /**
     * @throws NoMatchFoundException
     *             - If no match is found
     * @throws RequestTimeOutException
     *             - If the server fails to respond with in timeout
     * @throws TooManyMatchesFoundException
     *             - If the response exceeds the threshold
     * @throws ServiceNotAvailableException
     *             - If the service fails to respond
     */
    @Test(expected = NoMatchFoundException.class)
    public void testVerifyAddress() throws NoMatchFoundException, RequestTimeOutException,
            TooManyMatchesFoundException,
            ServiceNotAvailableException {
        given(qasAddressVerificationClient.search(anyString(), anyString())).willReturn(new ArrayList<AddressData>());
        targetAddressVerificationService.verifyAddress("12 thompson road", CountryEnum.AUSTRALIA);
        verify(qasAddressVerificationClient).search("12 thompson road", CountryEnum.AUSTRALIA.getCode());
    }

    @Test(expected = NoMatchFoundException.class)
    public void testSearchAddress() throws NoMatchFoundException, RequestTimeOutException,
            TooManyMatchesFoundException,
            ServiceNotAvailableException {
        given(qasAddressVerificationClient.search(anyString(), anyString())).willReturn(new ArrayList<AddressData>());
        targetAddressVerificationService.searchAddress("12 thompson road", CountryEnum.AUSTRALIA);
        verify(qasAddressVerificationClient).search("12 thompson road", CountryEnum.AUSTRALIA.getCode(), "Intuitive");
    }

    /**
     * test format address with null address
     * 
     * @throws ServiceNotAvailableException
     *             - on null response
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFormatAddressWithNullAddress() throws ServiceNotAvailableException {
        targetAddressVerificationService.formatAddress(null);
    }

    /**
     * test format address with empty address
     * 
     * @throws ServiceNotAvailableException
     *             - on null response
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFormatAddressWithEmptyAddress() throws ServiceNotAvailableException {
        targetAddressVerificationService.formatAddress(new AddressData(null, null, null));
    }

    /**
     * test format address with invalid moniker
     * 
     * @throws ServiceNotAvailableException
     *             - on null response
     */
    @Test(expected = IllegalArgumentException.class)
    public void testFormatAddressWithInvalidMoniker() throws ServiceNotAvailableException {
        targetAddressVerificationService.formatAddress(new AddressData("", null, null));
    }

    @Test
    public void itShouldReturnFalseIfTooManyMatchesFoundWhenCheckAddress() throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final TooManyMatchesFoundException ex = mock(TooManyMatchesFoundException.class);
        when(qasAddressVerificationClient.search("too many", CountryEnum.AUSTRALIA.getCode())).thenThrow(ex);
        assertFalse(targetAddressVerificationService.isAddressVerified("too many"));
    }

    @Test
    public void itShouldReturnFalseIfSystemUnavailable() throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final ServiceNotAvailableException ex = mock(ServiceNotAvailableException.class);
        when(qasAddressVerificationClient.search("SystemUnavailable", CountryEnum.AUSTRALIA.getCode())).thenThrow(ex);
        assertFalse(targetAddressVerificationService.isAddressVerified("SystemUnavailable"));
    }

    @Test
    public void itShouldReturnFalseIfRequestTimeout() throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final RequestTimeOutException ex = mock(RequestTimeOutException.class);
        when(qasAddressVerificationClient.search("RequestTimeout", CountryEnum.AUSTRALIA.getCode())).thenThrow(ex);
        assertFalse(targetAddressVerificationService.isAddressVerified("RequestTimeout"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldReturnFalseIfMultipleResultReturned() throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final List<AddressData> list = mock(List.class);
        when(list.size()).thenReturn(2);
        when(qasAddressVerificationClient.search("2", CountryEnum.AUSTRALIA.getCode())).thenReturn(list);
        assertFalse(targetAddressVerificationService.isAddressVerified("2"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldReturnFalseIfSingleResultReturnedButAddressIsDifferent() throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final List<AddressData> list = mock(List.class);
        when(list.size()).thenReturn(1);
        final AddressData addressData = new AddressData("A", "different", "3000");
        addressData.setScore(90);
        when(list.get(0)).thenReturn(addressData);
        when(qasAddressVerificationClient.search("different", CountryEnum.AUSTRALIA.getCode(), "Intuitive"))
                .thenReturn(list);
        assertFalse(targetAddressVerificationService.isAddressVerified("different"));
    }

    @SuppressWarnings("boxing")
    @Test
    public void itShouldReturnTrueIfSingleResultReturnedButAddressIsSame() throws ServiceNotAvailableException,
            RequestTimeOutException, TooManyMatchesFoundException {
        final List<AddressData> list = mock(List.class);
        when(list.size()).thenReturn(1);
        final AddressData addressData = new AddressData("1", "same", "3000");
        addressData.setScore(96);
        when(list.get(0)).thenReturn(addressData);
        when(qasAddressVerificationClient.search("same", CountryEnum.AUSTRALIA.getCode(), "Intuitive"))
                .thenReturn(list);
        assertTrue(targetAddressVerificationService.isAddressVerified("same"));
    }
}

package au.com.target.tgtverifyaddr.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtverifyaddr.data.AddressData;
import au.com.target.tgtverifyaddr.data.Config;
import au.com.target.tgtverifyaddr.data.CountryEnum;
import au.com.target.tgtverifyaddr.data.FormattedAddressData;
import au.com.target.tgtverifyaddr.exception.InitializationFailedException;
import au.com.target.tgtverifyaddr.exception.NoMatchFoundException;
import au.com.target.tgtverifyaddr.exception.RequestTimeOutException;
import au.com.target.tgtverifyaddr.exception.ServiceNotAvailableException;
import au.com.target.tgtverifyaddr.exception.TooManyMatchesFoundException;
import au.com.target.tgtverifyaddr.provider.AddressVerificationClient;
import au.com.target.tgtverifyaddr.provider.impl.QASAddressVerificationClient;


/**
 * End-to-end Integration tests for QAS service, with mocked config.
 * 
 * @author bjames4
 * 
 */
// Remove after we have sufficient credits on QAS
@Ignore
@RunWith(MockitoJUnitRunner.class)
public class TargetAddressVerificationServiceIntegrationTest {
    @Mock
    private Config config;

    @InjectMocks
    private final AddressVerificationClient addressVerificationClient = new QASAddressVerificationClient();

    private final TargetAddressVerificationServiceImpl qas = new TargetAddressVerificationServiceImpl();

    /**
     * @throws InitializationFailedException
     *             - If the initialization fails
     */
    @Before
    public void init() throws InitializationFailedException {
        //        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        //        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        //        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        //        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

        when(config.getWsdlUrl())
                .thenReturn("https://ws3.ondemand.qas.com/ProOnDemand/V3/ProOnDemandService.asmx?WSDL");
        when(config.getqName()).thenReturn("http://www.qas.com/OnDemand-2011-03");
        when(config.getServiceName()).thenReturn("QASOnDemandIntermediary");
        when(config.getUserName()).thenReturn("a38ccd4c-8c5");
        when(config.getPassword()).thenReturn("3k5m363xaZ");
        when(Boolean.valueOf(config.isFlatten())).thenReturn(Boolean.TRUE);
        when(config.getEngineType()).thenReturn("Intuitive");
        when(config.getIntensity()).thenReturn("Close");
        when(config.getLayout()).thenReturn("QADefault");
        when(config.getFormattedLayout()).thenReturn("TargetAustralia");
        when(config.getLocalisation()).thenReturn("");
        when(config.getPromptset()).thenReturn("Default");
        when(Integer.valueOf(config.getThreshold())).thenReturn(Integer.valueOf(50));
        when(Integer.valueOf(config.getTimeout())).thenReturn(Integer.valueOf(2000));
        when(config.getRequestTag()).thenReturn(null);
        when(Boolean.valueOf(config.isFormattedAddressInPicklist())).thenReturn(Boolean.TRUE);

        addressVerificationClient.init();
        qas.setAddressVerificationClient(addressVerificationClient);
    }

    /**
     * Tests for an exact match
     */
    @Test
    public void testExactMatchWithoutPostcode() {

        try {
            final List<AddressData> match = qas.verifyAddress("49 Streeton Close", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests formatted address, for an exact match
     */
    @Test
    public void testFormattedAddressForExactMatchWithoutPostcode() {

        try {
            final List<AddressData> match = qas.verifyAddress("49 Streeton Close", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
            assertTrue("Expecting Close to be abbreviated as Cl",
                    qas.formatAddress(match.get(0)).getAddressLine1().contains("Cl"));
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match, with postcode supplied
     */
    @Test
    public void testExactMatchWithPostcode() {
        try {
            final List<AddressData> match = qas.searchAddress("53 Plantation Road 3214", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests formatted address for an exact match, with postcode supplied
     */
    @Test
    public void testFormattedAddressForExactMatchWithPostcode() {
        try {
            final List<AddressData> match = qas.searchAddress("53 Plantation Road 3214", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
            final FormattedAddressData formatAddress = qas.formatAddress(match.get(0));
            assertTrue("Expecting Road to be abbreviated as Rd",
                    formatAddress.getAddressLine1().contains("Rd"));
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match, with wildcard
     */
    @Test
    public void testExactMatchWithCharacterWildcard() {
        try {
            final List<AddressData> match = qas.searchAddress("12 ?arden rd", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match, with a typo
     */
    @Test
    public void testExactMatchTypo() {
        try {
            final List<AddressData> match = qas.verifyAddress("10 perhaw st,castlemaine", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match, with road abbreviated as Rd
     */
    @Test
    public void testExactMatchWithRoadAbbreviated() {
        try {
            final List<AddressData> match = qas.verifyAddress("53 Plantation Rd, 3214", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises(set of units)
     */
    @Test
    public void testExactMatchSubPremises() {
        try {
            final List<AddressData> match = qas.verifyAddress("9/18 Ridge Street, North Sydney", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests formatted address for an exact match at sub premises(set of units)
     */
    @Test
    public void testFormattedAddressForExactMatchSubPremises() {
        try {
            final List<AddressData> match = qas.verifyAddress("9/18 Ridge Street, North Sydney", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
            assertTrue("Expecting Street to be abbreviated as St",
                    qas.formatAddress(match.get(0)).getAddressLine1().contains("St"));
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises(set of units)
     */
    @Test
    public void testExactMatchSubPremisesUnit() {
        try {
            final List<AddressData> match = qas.verifyAddress("Unit 9 8 Trenerry Crescent", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises(set of units)
     */
    @Test
    public void testFormattedAddressForExactMatchSubPremisesUnit() {
        try {
            final List<AddressData> match = qas.verifyAddress("Unit 9 8 Trenerry Crescent", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
            assertTrue("Expecting Cresent to be abbreviated as Cr", qas.formatAddress(match.get(0)).getAddressLine1()
                    .contains("Cr"));
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises, Unit abbreviated as U
     */
    @Test
    public void testExactMatchSubPremisesAbbreviatedUnit() {
        try {
            final List<AddressData> match = qas.verifyAddress("U 9 18 Ridge Street, North Sydney",
                    CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises, street abbreviated as St
     */
    @Test
    public void testExactMatchSubPremisesAbbreviatedStreet() {
        try {
            final List<AddressData> match = qas.verifyAddress("U 9 18 Ridge St, North Sydney", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises, multilevel
     */
    @Test
    public void testExactMatchSubPremisesMultiLevel() {
        try {
            final List<AddressData> match = qas
                    .verifyAddress("Flat 4 Level 1 51 Rhyll-Newhaven Road", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises, flat abbreviated as F
     */
    @Test
    public void testExactMatchSubPremisesMultiLevelAbbreviatedFlat() {
        try {
            final List<AddressData> match = qas.verifyAddress("F 4 Level 1 51 Rhyll-Newhaven Road",
                    CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises, flat abbreviated as F, level as L
     */
    @Test
    public void testExactMatchSubPremisesMultiLevelAbbreviatedFlatAndLevel() {
        try {
            final List<AddressData> match = qas.verifyAddress("F 4 L 1 51 Rhyll-Newhaven Road", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for an exact match at sub premises, flat abbreviated as F, floor as Fl
     */
    @Test
    public void testExactMatchSubPremisesMultiLevelAbbreviatedFlatAndFloor() {
        try {
            final List<AddressData> match = qas.verifyAddress("F 4 Fl 1 51 Rhyll-Newhaven Road", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 1, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for match at lot
     */
    @Test
    public void testMatchLot() {
        try {
            final List<AddressData> match = qas.searchAddress("Lot 6 Daisy Hill Road", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertEquals("Expecting exact match", 5, match.size());
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for match less than threshold
     */
    @Test
    public void testMatchListLessThanThreshold() {
        try {
            final List<AddressData> match = qas.verifyAddress("14 Streeton Close, 3214", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertTrue("Expecting match greater than 50", match.size() < 50);
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for match greater than threshold
     */
    @Test
    public void testMatchListGreaterThanThreshold() {
        try {
            final List<AddressData> match = qas.verifyAddress("Plantation Road, 3214", CountryEnum.AUSTRALIA);
            assertNotNull("Expecting non null resultset", match);
            assertTrue("Expecting match greater than 50", match.size() > 50);
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            assertNotNull("Expecting non null TooManyMatchesFoundException object", e);
            assertEquals("Expecting 'Too many matches found'", "Too many matches found", e.getMessage());
        }
        catch (final ServiceNotAvailableException e) {
            fail("Not expecting ServiceNotAvailableException");
        }
    }

    /**
     * Tests for zero matches
     * 
     * @throws ServiceNotAvailableException
     * @throws TooManyMatchesFoundException
     * @throws RequestTimeOutException
     * @throws NoMatchFoundException
     */
    @Test(expected = NoMatchFoundException.class)
    public void testNoMatches() throws NoMatchFoundException, RequestTimeOutException, TooManyMatchesFoundException,
            ServiceNotAvailableException {
        qas.verifyAddress("Can't find this address", CountryEnum.AUSTRALIA);
    }

    /**
     * Tests for zero matches
     * 
     * @throws TooManyMatchesFoundException
     * @throws NoMatchFoundException
     * @throws RequestTimeOutException
     * @throws ServiceNotAvailableException
     */
    @Test(expected = NoMatchFoundException.class)
    public void testAnotherNoMatches() throws ServiceNotAvailableException, RequestTimeOutException,
            NoMatchFoundException, TooManyMatchesFoundException {
        qas.searchAddress("fdsfadfsdfdsfdsfd", CountryEnum.AUSTRALIA);
    }

    /**
     * Tests for invalid credentials
     */
    @Test
    public void testInvalidCredentials() {
        try {
            when(config.getUserName()).thenReturn("unknown");
            addressVerificationClient.init();
            qas.verifyAddress("352 Plantation Road", CountryEnum.AUSTRALIA);
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final ServiceNotAvailableException e) {
            assertTrue("Expecting Authentication failure",
                    e.getMessage().contains("Authentication failure (User name: unknown)"));
        }
        catch (final Exception e) {
            assertNotNull("Expecting non null exception", e);
        }
    }

    /**
     * Test for malformed url
     */
    @Test
    public void testMalformedURL() {
        try {
            config.setWsdlUrl("https//unknown");
            when(config.getWsdlUrl())
                    .thenReturn("https://unknown");
            addressVerificationClient.init();
            qas.verifyAddress("352 Plantation Road", CountryEnum.AUSTRALIA);
        }
        catch (final NoMatchFoundException e) {
            fail("Not expecting NoMatchFoundException");
        }
        catch (final RequestTimeOutException e) {
            fail("Not expecting RequestTimeOutException");
        }
        catch (final TooManyMatchesFoundException e) {
            fail("Not expecting TooManyMatchesFoundException");
        }
        catch (final Exception e) {
            assertNotNull("Expecting non null Exception", e);
        }
    }

    @Test
    public void verifyExactMatch() {
        assertTrue(qas.isAddressVerified("24 Alamanda Blvd, Point Cook VIC 3030"));
        assertTrue(qas.isAddressVerified("24 Alamanda bvd, Point Cook VIC 3030"));
        assertFalse(qas.isAddressVerified("24 Alamanda Ct, Point Cook VIC 3030"));
    }
}

<link rel="stylesheet" type="text/css" href="css/order-email.css">
#set ( $base = ${ctx.get("parentContext")})
#set ( $order = ${ctx.get("parentContext").order})
#set ( $currentConsignment = ${ctx.get("parentContext").consignment})
#set ( $shippedConsignments = ${ctx.get("parentContext").shippedConsignments})
#set ( $sentDigitalConsignments = ${ctx.get("parentContext").sentDigitalConsignments})
#set ( $inProgressEntries = ${ctx.get("parentContext").inProgressEntries})
#set ( $dateUtils = ${ctx.get("parentContext").dateTool})
#set ( $modifications = ${ctx.get("parentContext").currentlyCancelledItems})
#set ( $refundTotalAmount = ${ctx.get("parentContext").refundAmount})

#macro( getAppliedPromotionForShippedEntry $appliedProductPromotions $entryNumber )
    #if (!$appliedProductPromotions.isEmpty())
        #foreach( $promotion in $appliedProductPromotions)
            #set ($displayed = false)
            #foreach ($consumedEntry in $promotion.consumedEntries)
                #if (!$displayed && ($consumedEntry.orderEntryNumber == $entryNumber))
                    #set ($displayed = true)
                    <p class="app-deal">${promotion.description}</p>
                #end
            #end
         #end
    #end
#end

#macro( getRecipientForShippedEntry $entryNo )

    #foreach($orderEntry in $order.entries)
        #if($orderEntry.entryNumber.intValue() == $entryNo.intValue())
            #if($orderEntry.recipients)
                #foreach( $recipient in $orderEntry.recipients )
                    <span class="small bd">Recipient: ${recipient.firstName} ${recipient.lastName}</span><br />
                    <span class="small">Email: ${recipient.recipientEmailAddress}</span><br />
                #end
            #end
        #end
    #end
#end
#macro( entryItemsDetails $entry $quantity )
    <span><strong>${entry.product.name}</strong></span><br />
    <span>Item Code: ${entry.product.code}</span><br />
    <span>QTY: ${quantity}</span><br />
    <span>Unit Price: ${entry.basePrice.formattedValue}</span><br />

    #if($entry.product.giftCard != true &&  $entry.product.size && $entry.product.size != '' )
        <span>Size: $entry.product.size</span><br />
    #end

    #if($entry.product.giftCard != true &&  $entry.product.swatch && $entry.product.swatch != '' )
        <span>Color: $entry.product.swatch</span><br />
    #end

    #getRecipientForShippedEntry( $entry.entryNumber )
    #getAppliedPromotionForShippedEntry( $order.appliedProductPromotions $entry.entryNumber )
#end

#macro( consigmentShipmentInfo  $orderConsignment )
    #if($orderConsignment)
        #if($orderConsignment.isDigitalDelivery == true)
                <span><strong>Status:<span class="sent-status"> Sent </span></strong></span><br />
                <span><strong>Date Sent: </strong>$dateUtils.format('dd/MM/yyyy', $orderConsignment.statusDate)</span><br />
                <span class="tiny">Allow one business day for email delivery.</span>
        #elseif($orderConsignment.statusDisplay eq 'Shipped')
                <span><strong>Status: <span class="shipped-status"> $orderConsignment.statusDisplay </span></strong></span><br />
                <span><strong>Date Shipped: </strong>$dateUtils.format('dd/MM/yyyy', $orderConsignment.statusDate)</span><br />
                <span>
                    <strong>Delivery Time: </strong>
                    #if($base.isShortDeliveryTime == true)
                        Next business day. <br /> <span class="tiny">For qualifying orders and locations only.</span>
                    #else
                        Orders will typically take 3-9 business days, subject to availability.
                    #end
                </span> <br />
                #if($orderConsignment.trackingID)
                    <span><strong>Tracking ID: </strong>$orderConsignment.trackingCode</span> <br />
                    <a href="$orderConsignment.trackingID" class="track-delivery-button">
                        <div class="track-delivery-button-text"><br />&nbsp;Track this Delivery<br />&nbsp;</div>
                    </a><br/>
                    <span class="tiny">Allow 24-48 hours before tracking order.</span>
                #end

        #end
    #end
#end


#macro( cancelledItemsShipmentInfo  $itemData )
    #if($itemData)
        <span><strong>Status: <span class="out-of-stock">Insufficient Stock</span></strong></span><br />

         #if($itemData.product.giftCard)
            <span><strong>Date Shipped: </strong>$dateUtils.format('dd/MM/yyyy', $itemData.date)</span><br />
            <span class="tiny">Please contact Customer Support on $base.targetContactNumber to organise your refund.</span>
         #else
            <span><strong>Date Processed: </strong>$dateUtils.format('dd/MM/yyyy', $itemData.date)</span><br />
            <span class="tiny">Please allow up to seven business days for the refund to finalise.</span>
         #end
    #end
#end

#macro(tableHead $title)

    <tr>
        <td>
            <table width="100%" align="center" class="table-max-width-margin">
                <tbody width="100%">
                    <tr>
                        <td class="order-items-head">$title</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
#end

#if($currentConsignment)

    #if($currentConsignment.isDigitalDelivery == true)
            #tableHead('Sent')
    #else
            #tableHead($currentConsignment.statusDisplay)
    #end
    <tr>
        <td>
            <table class="columns-table">
                <tr>
                    <td valign="top" width="50%" class="columns-table-container">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    #foreach( $entry in $currentConsignment.entries )
                                        #if($entry.shippedQuantity > 0)
                                             #entryItemsDetails( $entry.orderEntry $entry.shippedQuantity)
                                        #end
                                    <br />
                                    <br />
                                    #end
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="top" width="50%" class="columns-table-container">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                #consigmentShipmentInfo( $currentConsignment)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

#end

## for partial cancellation email
#if($modifications && $modifications.size() >0)
    #tableHead('Cancelled')
    <tr>
        <td>
            <table class="columns-table">
                #foreach( $entry in $modifications )
                    <tr>
                        <td valign="top" width="50%" class="columns-table-container">
                            <table width="100%">
                                <tr>
                                    <td valign="top">
                                    #entryItemsDetails( $entry $entry.quantity)
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="top" width="50%" class="columns-table-container">
                            <table width="100%">
                                <tr>
                                    <td valign="top">
                                    #cancelledItemsShipmentInfo( $entry)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                #end
            </table>
        </td>
    </tr>

    #if($order.cancelledProducts && $order.cancelledProducts.size() >0)
        <tr class="info">
            <td class="gap">
            </td>
        </tr>
        <tr>
            <td>
                <table class="columns-table">
                    #foreach( $entry in $order.cancelledProducts )
                        <tr>
                            <td valign="top" width="50%" class="columns-table-container">
                                <table width="100%">
                                    <tr>
                                        <td valign="top">
                                            #entryItemsDetails( $entry $entry.quantity)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="top" width="50%" class="columns-table-container">
                                <table width="100%">
                                    <tr>
                                        <td valign="top">
                                        #cancelledItemsShipmentInfo( $entry)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    #end
                </table>
            </td>
        </tr>
    #end
    <tr class="info">
        <td class="gap">
        </td>
    </tr>
#end


#if($shippedConsignments && $shippedConsignments.size() > 0)

    #if($currentConsignment)
        #if($currentConsignment.isDigitalDelivery == true)
            #tableHead('Shipped')
        #end
    #else
        #tableHead('Shipped')
    #end
<tr>
    <td>
        <table class="columns-table">
            #foreach($consignment in $shippedConsignments)
                <tr>
                    <td valign="top" width="50%" class="columns-table-container">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    #foreach( $entry in $consignment.entries )
                                        #if($entry.shippedQuantity > 0)
                                            #entryItemsDetails( $entry.orderEntry $entry.shippedQuantity)
                                        #end
                                    <br />
                                    #end
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="top" width="50%" class="columns-table-container">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    #consigmentShipmentInfo( $consignment)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            #end
        </table>
    </td>
</tr>
#end

#if($sentDigitalConsignments && $sentDigitalConsignments.size() > 0)
    #tableHead('Sent')

<tr>
    <td>
        <table class="columns-table">
            #foreach($consignment in $sentDigitalConsignments)
                <tr>
                    <td valign="top" width="50%" class="columns-table-container">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    #foreach( $entry in $consignment.entries )
                                        #if($entry.shippedQuantity > 0)
                                        #entryItemsDetails( $entry.orderEntry $entry.shippedQuantity)
                                        #end
                                    <br />
                                    #end
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="top" width="50%" class="columns-table-container">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    #consigmentShipmentInfo( $consignment)
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            #end
        </table>
    </td>
</tr>
#end


        
#if($inProgressEntries && $inProgressEntries.size() > 0)
    #tableHead('In progress')
    <tr class="info">
        <td>
            <br /> We will send you an email with delivery tracking details as soon as your item(s) are shipped.
            #if($order.hasPhysicalGiftCards == true) Your Gift Card will be sent separately via registered post.
            #end
            #if($order.hasDigitalProducts == true) Your e-Gift Cards will be sent directly to the recipient email address.
            #end
            <br /> <br />
        </td>
    </tr>
    #foreach( $entry in $inProgressEntries )
    <tr class="info">
        <td>
            #entryItemsDetails( $entry $entry.quantity)
            <br />
        </td>
    </tr>
    #end
#end

## not required to display for partial cancellation email since we are displaying above
#if(!$modifications)
    #if($order.cancelledProducts && $order.cancelledProducts.size() >0)
        #tableHead('Cancelled')
         <tr>
            <td>
                <table class="columns-table">
                    #foreach( $entry in $order.cancelledProducts )
                        <tr>
                            <td valign="top" width="50%" class="columns-table-container">
                             <table width="100%">
                                <tr>
                                    <td valign="top">
                                        #entryItemsDetails( $entry $entry.quantity)
                                    </td>
                                </tr>
                            </table>
                            </td>
                            <td align="top" width="50%" class="columns-table-container">
                                <table width="100%">
                                    <tr>
                                        <td valign="top">
                                        #cancelledItemsShipmentInfo( $entry)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    #end
                </table>
            </td>
         </tr>
    #end
#end

<tr>
    <td>
        <!--[if (gte mso 9)]><table width="600" align="center"><tr><td><![endif]-->
        <table class="table-cell-spacing total-text-align bgcolor-gray table-max-width-margin" width="100%">
            <tbody>
                #if($order.productDiscounts && $order.productDiscounts.value > 0)
                    <tr class="bgcolor-green">
                        <td class="text-left-align">
                            <span>Item Savings</span>
                        </td>
                        <td class="text-right-align">
                            <span>-&nbsp;$order.productDiscounts.formattedValue</span>
                        </td>
                    </tr>
                #end
                #if($order.subTotal)
                    <tr>
                        <td class="text-left-align">
                            <span class="bold">Sub Total</span>
                        </td>
                        <td class="text-right-align">
                            <span class="bold">${order.subTotal.formattedValue}</span>
                        </td>
                    </tr>
                #end
                #set($delType = $order.deliveryMode.name + " Fee" )
                #if($order.deliveryCost)
                    <tr>
                        <td class="text-left-align">
                            <span>$delType</span>
                        </td>
                        <td class="text-right-align">
                            <span>${order.deliveryCost.formattedValue}</span>
                        </td>
                    </tr>
                #end
                #if($order.orderDiscounts && $order.orderDiscounts.value > 0)
                    <tr class="bgcolor-green">
                        <td class="text-left-align">
                            #if($order.flybuysDiscountData && $order.flybuysDiscountData.value.value > 0)
                            <span>${ctx.get("parentContext").formatedFlybuysPoints}&nbsp;flybuys points</span>
                            #else
                            <span>Vouchers</span>
                            #end
                        </td>
                        <td class="text-right-align">
                            <span>-&nbsp;$order.orderDiscounts.formattedValue</span>
                        </td>
                    </tr>
                #end

                <tr class="bgcolor-green">
                        <td class="bold text-left-align">
                            <h3>Total</h3>
                        </td>
                        <td class="bold text-right-align">
                            <h3>${order.totalPrice.formattedValue}</h3>
                        </td>
                </tr>

                #if(!$order.tradeMeOrder && $order.totalTax)
                    <tr>
                        <td class="text-left-align">
                            <span>incl GST</span>
                        </td>
                        <td class="text-right-align">
                            <span>${order.totalTax.formattedValue}</span>
                        </td>
                    </tr>
                #end
                #if($order.containsPreOrderItems && $order.preOrderDepositAmount)
                    <tr class="bgcolor-green">
                        <td class="text-left-align">
                            <span class="bold">Deposit paid</span>
                        </td>
                        <td class="text-right-align">
                            <span class="bold">${order.preOrderDepositAmount.formattedValue}</span>
                        </td>
                    </tr>


                    <tr>
                        <td class="text-left-align">
                            <span class="bold">Balance Remaining</span><br />
                            <span>Deducted when item ships</span>
                        </td>
                        <td class="text-right-align">
                            <span><b>${order.preOrderOutstandingAmount.formattedValue}</b></span>
                        </td>
                    </tr>
                #end
                #if($order.totalDiscounts && $order.totalDiscounts.value > 0)
                    <tr class="bgcolor-green">
                        <td class="text-left-align">
                            <span class="bold">Total Savings</span>
                        </td>
                        <td class="bold text-right-align">
                             <span>$order.totalDiscounts.formattedValue</span>
                        </td>
                    </tr>
                #end
            </tbody>
        </table>
        <!--[if (gte mso 9)]></td></tr></table><![endif]-->
    </td>
</tr>
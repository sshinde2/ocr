## messageSource=classpath:/tgtwebcore/messages/email-orderNotification_en.properties
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
#set ( $dateUtils = ${ctx.dateTool})
#macro(tableHead $title)
<tr>
    <td>
        <table width="100%" align="center" class="table-max-width-margin">
        <tbody width="100%">
            <tr>
                <td class="order-items-head">$title</td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
#end

#macro( getAppliedPromotionForShippedEntry $appliedProductPromotions $entryNumber )
    #if (!$appliedProductPromotions.isEmpty())
        #foreach( $promotion in $appliedProductPromotions)
            #set ($displayed = false)
            #foreach ($consumedEntry in $promotion.consumedEntries)
                #if (!$displayed && ($consumedEntry.orderEntryNumber == $entryNumber))
                    #set ($displayed = true)
                <p class="app-deal">${promotion.description}</p>
                #end
            #end
        #end
    #end
#end

#macro( getRecipientForShippedEntry $entryNo )
    #foreach($orderEntry in $ctx.order.entries)
        #if($orderEntry.entryNumber.intValue() == $entryNo.intValue())
            #if($orderEntry.recipients)
                #foreach( $recipient in $orderEntry.recipients )
                <span class="small bd">Recipient: ${recipient.firstName} ${recipient.lastName}</span><br />
                <span class="small">Email: ${recipient.recipientEmailAddress}</span><br />
                #end
            #end
        #end
    #end
#end

#macro( entryItemsDetails $entry $shippedQuantity )
    <span><strong>${entry.product.name}</strong></span><br />
    <span>Item Code: ${entry.product.code}</span><br />

    #if($entry.product.giftCard != true && $entry.product.size && $entry.product.size != '' )
        <span>Size: $entry.product.size</span><br />
    #end

    #if($entry.product.giftCard != true && $entry.product.swatch && $entry.product.swatch != '' )
        <span>Color: $entry.product.swatch</span><br />
    #end

    #if($shippedQuantity)
        <span>QTY: ${shippedQuantity}</span><br />
    #end

    <span>Unit Price: ${entry.basePrice.formattedValue}</span><br />


    #if($entry.discountPrice && $entry.discountPrice.value > 0)
        <span class="app-deal" >Savings: ${entry.discountPrice.formattedValue}</span><br />
    #end

    <span>Item Total: ${entry.totalPrice.formattedValue}</span><br />

    #getRecipientForShippedEntry( $entry.entryNumber )
    #getAppliedPromotionForShippedEntry( $ctx.order.appliedProductPromotions $entry.entryNumber )
#end

#macro( consigmentShipmentInfo  $orderConsignment )
            <span><strong>Status:<span class="sent-status"> Sent </span></strong></span><br />
            <span><strong>Date Sent: </strong>$dateUtils.format('dd/MM/yyyy', $orderConsignment.statusDate)</span><br />
            <span class="tiny">Allow one business day for email delivery.</span>
#end

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="css/order-email.css">
    </head>
    <body>
        <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">

            <tr>
                <td>
                    <!--[if (gte mso 9)]><table width="600" align="center"><tr><td><![endif]-->

                    <table align="center" class="max bc">

                        ${ctx.cmsSlotContents.EmailHeaderContentSlot}

                        ${ctx.cmsSlotContents.EmailMarketingContentSlot1}

                        <tr>
                            <td class="greeting-text"> ${ctx.messages.getMessage('orderReadyToCollectGreetingText', ${ctx.displayName})}
                            </td>
                        </tr>
                        <tr>
                            <td class="gap">
                            </td>
                        </tr>

                        ${ctx.cmsSlotContents.EmailContentSlot1}

                        <tr>
                            <td class="delivery-address-head">
                                    Click & Collect from:
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="bold">${ctx.selectedStore.type} &ndash; ${ctx.selectedStore.name}</span> <br />
                                #if(${ctx.selectedStore.targetAddressData.building})
                                    <span>${ctx.selectedStore.targetAddressData.building}</span> <br />
                                #end

                                <span>$ctx.order.deliveryAddress.line1</span><br />

                                #if($ctx.order.deliveryAddress.line2 && $ctx.order.deliveryAddress.line2 != "")
                                    <span>$ctx.order.deliveryAddress.line2</span><br />
                                #end

                                <span>$ctx.order.deliveryAddress.town $ctx.order.deliveryAddress.state $ctx.order.deliveryAddress.postalCode</span><br />

                                #if(${ctx.selectedStore.targetAddressData.phone} )
                                    <span>Phone: ${ctx.selectedStore.targetAddressData.phone}</span><br />
                                #end
                            </td>
                        </tr>
                        <tr>
                            <td class="gap"></td>
                        </tr>

                        #if(!${ctx.order.ebayOrder} && !${ctx.order.guestCustomer})
                            <tr>
                                <td class="login-text">
                                <a class="link" href="${ctx.emailBaseUrl}/my-account">Login</a> anytime to check the status of your order.
                                </td>
                            </tr>
                        #end

                        <tr>
                        <td class="gap"></td>
                        </tr>
                        #tableHead('Ready to Collect')
                        <tr>
                            <td>
                                <table class="columns-table">
                                    <tr>
                                        <td valign="top" width="50%" class="columns-table-container">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top" class="x-large">
                                                    ${ctx.messages.getMessage('readyToCollectPrintInvoiceText')}
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td align="top" width="50%" class="columns-table-container">
                                            <table width="100%">
                                                <tr>
                                                    <td valign="top">
                                                    <img src="${ctx.emailBaseUrl}${ctx.order.barcodePngUrl}" width="224" height="105" class="bc"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td class="gap"></td>
                        </tr>
                        <tr>
                            <td class="gap"></td>
                        </tr>


                        #if($ctx.shippedConsignments && $ctx.shippedConsignments.size() >0)
                                #foreach($consignment in $ctx.shippedConsignments)
                                    <tr>
                                        <td valign="top">
                                            #foreach( $entry in $consignment.entries )
                                                #if($entry.shippedQuantity && $entry.shippedQuantity >0)
                                                    #entryItemsDetails( $entry.orderEntry $entry.shippedQuantity)
                                                #end
                                            <br />
                                            #end
                                        </td>
                                    </tr>
                                #end
                        #end


                        ${ctx.cmsSlotContents.EmailContentSlot2}


                        #if($ctx.sentDigitalConsignments && $ctx.sentDigitalConsignments.size() >0 )
                            #tableHead('Sent')

                            <tr>
                                <td>
                                    <table class="columns-table">
                                        #foreach($consignment in $ctx.sentDigitalConsignments)
                                            <tr>
                                                <td valign="top" width="50%" class="columns-table-container">
                                                    <table width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                #foreach( $entry in $consignment.entries )
                                                                    #if($entry.shippedQuantity && $entry.shippedQuantity >0)
                                                                        #entryItemsDetails( $entry.orderEntry $entry.shippedQuantity)
                                                                    #end
                                                                <br />
                                                                #end
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="top" width="50%" class="columns-table-container">
                                                    <table width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                #consigmentShipmentInfo( $consignment)
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                         #end
                                    </table>
                                </td>
                            </tr>
                        #end

                        <tr>
                            <td>
                                <!--[if (gte mso 9)]><table width="600" align="center"><tr><td><![endif]-->
                                <table class="table-cell-spacing total-text-align bgcolor-gray table-max-width-margin" width="100%">
                                    <tbody>
                                        #if($ctx.order.productDiscounts && $ctx.order.productDiscounts.value > 0)
                                            <tr class="bgcolor-green">
                                                <td class="text-left-align">
                                                    <span>Item Savings</span>
                                                </td>
                                                <td class="text-right-align">
                                                    <span>-&nbsp;$ctx.order.productDiscounts.formattedValue</span>
                                                </td>
                                            </tr>
                                        #end
                                        #if($ctx.order.subTotal)
                                            <tr>
                                                <td class="text-left-align">
                                                    <span class="bold">Sub Total</span>
                                                </td>
                                                <td class="text-right-align">
                                                    <span class="bold">${ctx.order.subTotal.formattedValue}</span>
                                                </td>
                                            </tr>
                                        #end
                                        #set($delType = $ctx.order.deliveryMode.name + " Fee" )
                                        #if($ctx.order.deliveryCost)
                                            <tr>
                                                <td class="text-left-align">
                                                    <span>$delType</span>
                                                </td>
                                                <td class="text-right-align">
                                                    <span>${ctx.order.deliveryCost.formattedValue}</span>
                                                </td>
                                            </tr>
                                        #end
                                        #if($ctx.order.orderDiscounts && $ctx.order.orderDiscounts.value > 0)
                                            <tr class="bgcolor-green">
                                                <td class="text-left-align">
                                                    #if($ctx.order.flybuysDiscountData && $ctx.order.flybuysDiscountData.value.value > 0)
                                                    <span>${ctx.formatedFlybuysPoints}&nbsp;flybuys points</span>
                                                    #else
                                                    <span>Vouchers</span>
                                                    #end
                                                </td>
                                                <td class="text-right-align">
                                                    <span>-&nbsp;$ctx.order.orderDiscounts.formattedValue</span>
                                                </td>
                                            </tr>
                                        #end
            
                                        <tr class="bgcolor-green">
                                                <td class="bold text-left-align">
                                                    <h3>Total</h3>
                                                </td>
                                                <td class="bold text-right-align">
                                                    <h3>${ctx.order.totalPrice.formattedValue}</h3>
                                                </td>
                                        </tr>
            
                                        #if(!$ctx.order.tradeMeOrder && $ctx.order.totalTax)
                                            <tr>
                                                <td class="text-left-align">
                                                    <span>incl GST</span>
                                                </td>
                                                <td class="text-right-align">
                                                    <span>${ctx.order.totalTax.formattedValue}</span>
                                                </td>
                                            </tr>
                                        #end
                                        #if($ctx.order.preOrderDepositAmount)
                                            <tr class="bgcolor-green">
                                                <td class="text-left-align">
                                                    <span class="bold">Deposit paid</span>
                                                </td>
                                                <td class="text-right-align">
                                                    <span class="bold">${ctx.order.preOrderDepositAmount.formattedValue}</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-left-align">
                                                    <span class="bold">Balance Remaining</span><br />
                                                    <span>Deducted when item ships</span>
                                                </td>
                                                <td class="text-right-align">
                                                    <span><b>${ctx.order.preOrderOutstandingAmount.formattedValue}</b></span>
                                                </td>
                                            </tr>
                                        #end
                                        #if($ctx.order.totalDiscounts && $ctx.order.totalDiscounts.value > 0)
                                            <tr class="bgcolor-green">
                                                <td class="text-left-align">
                                                    <span class="bold">Total Savings</span>
                                                </td>
                                                <td class="bold text-right-align">
                                                     <span>$ctx.order.totalDiscounts.formattedValue</span>
                                                </td>
                                            </tr>
                                        #end
                                    </tbody>
                                </table>
                                <!--[if (gte mso 9)]></td></tr></table><![endif]-->
                            </td>
                        </tr>
                        <tr>
                            <td class="info">${ctx.messages.getMessage('taxInvoiceAttached')}
                            </td>
                        </tr>
                        <tr>
                            <td class="gap">
                            </td>
                        </tr>
                        ${ctx.cmsSlotContents.EmailContentSlot3}
                        ${ctx.cmsSlotContents.EmailMarketingContentSlot2}

                    </table>

                    <!--[if (gte mso 9)]></td></tr></table><![endif]-->
                </td>
            </tr>
        </table>
</body>
</html>
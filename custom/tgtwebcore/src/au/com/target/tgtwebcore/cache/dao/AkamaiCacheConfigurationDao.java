/**
 * 
 */
package au.com.target.tgtwebcore.cache.dao;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.AkamaiCacheConfigurationModel;



/**
 * @author rmcalave
 * 
 */
public interface AkamaiCacheConfigurationDao {

    /**
     * Get an Akamai cache configuration by page type.
     * 
     * @param pageType
     *            The page type to retrieve the configuration for
     * @return The Akamai cache configuration
     * @throws TargetUnknownIdentifierException
     *             if no results were found
     * @throws TargetAmbiguousIdentifierException
     *             if more than one result was found
     */
    AkamaiCacheConfigurationModel getAkamaiCacheConfigurationByPageType(CachedPageType pageType)
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;

}
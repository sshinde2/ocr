/**
 * 
 */
package au.com.target.tgtwebcore.cache.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import org.apache.commons.lang.Validate;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtwebcore.cache.dao.AkamaiCacheConfigurationDao;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.AkamaiCacheConfigurationModel;


/**
 * @author rmcalave
 * 
 */
public class AkamaiCacheConfigurationDaoImpl extends DefaultGenericDao<AkamaiCacheConfigurationModel> implements
        AkamaiCacheConfigurationDao {

    private static final String QUERY_GET_CONFIG_BY_PAGE_TYPE = "select {" + AkamaiCacheConfigurationModel.PK
            + "} from {" + AkamaiCacheConfigurationModel._TYPECODE + "} where {"
            + AkamaiCacheConfigurationModel.CACHEDPAGETYPE + "} = ?" + AkamaiCacheConfigurationModel.CACHEDPAGETYPE;

    public AkamaiCacheConfigurationDaoImpl() {
        super(AkamaiCacheConfigurationModel._TYPECODE);
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.cache.dao.impl.AkamaiCacheConfigurationDao#getAkamaiCacheConfigurationByPageType(au.com.target.tgtwebcore.enums.PageType)
     */
    @Override
    public AkamaiCacheConfigurationModel getAkamaiCacheConfigurationByPageType(final CachedPageType pageType)
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        Validate.notNull(pageType, "pageType must not be null");

        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_GET_CONFIG_BY_PAGE_TYPE);
        query.addQueryParameter(AkamaiCacheConfigurationModel.CACHEDPAGETYPE, pageType);

        final SearchResult<AkamaiCacheConfigurationModel> akamaiCacheConfigurations = getFlexibleSearchService()
                .search(query);

        TargetServicesUtil.validateIfSingleResult(akamaiCacheConfigurations.getResult(),
                AkamaiCacheConfigurationModel.class,
                AkamaiCacheConfigurationModel.CACHEDPAGETYPE,
                pageType);
        return akamaiCacheConfigurations.getResult().get(0);
    }
}

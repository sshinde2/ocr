/**
 * 
 */
package au.com.target.tgtwebcore.cache.service;

import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.AkamaiCacheConfigurationModel;



/**
 * @author rmcalave
 * 
 */
public interface AkamaiCacheConfigurationService {

    /**
     * Get the AkamaiCacheConfigurationModel for the given CachedPageType.
     * 
     * @param pageType
     *            The page type to get the AkamaiCacheConfigurationModel for
     * @return The AkamaiCacheConfigurationModel for the CachedPageType, or null if none was found
     */
    AkamaiCacheConfigurationModel getAkamaiCacheConfiguration(CachedPageType pageType);

}
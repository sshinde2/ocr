/**
 * 
 */
package au.com.target.tgtwebcore.cache.service.impl;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtwebcore.cache.dao.AkamaiCacheConfigurationDao;
import au.com.target.tgtwebcore.cache.service.AkamaiCacheConfigurationService;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.AkamaiCacheConfigurationModel;


/**
 * @author rmcalave
 * 
 */
public class AkamaiCacheConfigurationServiceImpl implements AkamaiCacheConfigurationService {
    private static final Logger LOG = Logger.getLogger(AkamaiCacheConfigurationServiceImpl.class);

    private AkamaiCacheConfigurationDao akamaiCacheConfigurationDao;

    /* (non-Javadoc)
     * @see au.com.target.tgtwebcore.cache.service.impl.AkamaiCacheConfigurationService#getAkamaiCacheConfiguration(au.com.target.tgtwebcore.enums.PageType)
     */
    @Override
    public AkamaiCacheConfigurationModel getAkamaiCacheConfiguration(final CachedPageType pageType) {
        Validate.notNull(pageType, "pageType must not be null");

        try {
            return getAkamaiCacheConfigurationDao().getAkamaiCacheConfigurationByPageType(pageType);
        }
        catch (final TargetUnknownIdentifierException ex) {
            return null;
        }
        catch (final TargetAmbiguousIdentifierException ex) {
            LOG.warn("More than one AkamaiCacheConfigurationModel found for CachedPageType " + pageType.toString());
            return null;
        }
    }

    /**
     * @return the akamaiCacheConfigurationDao
     */
    protected AkamaiCacheConfigurationDao getAkamaiCacheConfigurationDao() {
        return akamaiCacheConfigurationDao;
    }

    /**
     * @param akamaiCacheConfigurationDao
     *            the akamaiCacheConfigurationDao to set
     */
    @Required
    public void setAkamaiCacheConfigurationDao(final AkamaiCacheConfigurationDao akamaiCacheConfigurationDao) {
        this.akamaiCacheConfigurationDao = akamaiCacheConfigurationDao;
    }
}

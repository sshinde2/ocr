/**
 * 
 */
package au.com.target.tgtwebcore.evaluator;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.restrictions.CMSCategoryRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.impl.CMSCategoryRestrictionEvaluator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;


/**
 * @author vmuthura
 * 
 */
public class TargetCMSCategoryRestrictionEvaluator extends CMSCategoryRestrictionEvaluator
{
    private static final Logger LOG = Logger.getLogger(TargetCMSCategoryRestrictionEvaluator.class);

    @SuppressWarnings("deprecation")
    @Override
    public boolean evaluate(final CMSCategoryRestrictionModel categoryRestrictionModel, final RestrictionData context)
    {
        if (context == null) {
            return true;
        }
        List<CategoryModel> categories = new ArrayList<>();
        if (context.hasProduct())
        {
            final ProductModel productModel = context.getProduct();
            final Collection<CategoryModel> productCategories = getSuperCategories(productModel);
            categories = new ArrayList<CategoryModel>(productCategories);
            if (categoryRestrictionModel.isRecursive()) {
                for (final CategoryModel category : productCategories) {
                    categories.addAll(category.getAllSupercategories());
                }
            }
        }
        else if (context.hasCategory()) {
            final CategoryModel category = context.getCategory();
            categories.add(category);
            if (categoryRestrictionModel.isRecursive()) {
                categories.addAll(category.getAllSupercategories());
            }
        }
        else
        {
            LOG.debug("Could not evaluate TargetCMSCategoryRestriction. RestrictionData contains neither a category or a product. Returning false.");
            return false;
        }
        final List<String> codes = getCategoryCodes(categories);
        for (final String code : categoryRestrictionModel.getCategoryCodes()) {
            if (codes.contains(code)) {
                return true;
            }
        }
        return false;
    }

    private Collection<CategoryModel> getSuperCategories(final ProductModel product)
    {
        Assert.notNull(product);

        if (product instanceof VariantProductModel)
        {
            final ProductModel baseProduct = ((VariantProductModel)product).getBaseProduct();

            return baseProduct != null ? getSuperCategories(baseProduct) : product.getSupercategories();
        }
        else
        {
            return product.getSupercategories();
        }
    }
}

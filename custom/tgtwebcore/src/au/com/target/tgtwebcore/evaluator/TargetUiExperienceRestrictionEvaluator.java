/**
 * 
 */
package au.com.target.tgtwebcore.evaluator;


import de.hybris.platform.acceleratorcms.model.restrictions.CMSUiExperienceRestrictionModel;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;
import de.hybris.platform.servicelayer.session.SessionService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * @author asingh78
 * 
 */
public class TargetUiExperienceRestrictionEvaluator implements CMSRestrictionEvaluator<CMSUiExperienceRestrictionModel>
{
    private static final Logger LOG = Logger.getLogger(TargetUiExperienceRestrictionEvaluator.class);

    private UiExperienceService uiExperienceService;

    private SessionService sessionService;

    protected UiExperienceService getUiExperienceService()
    {
        return uiExperienceService;
    }

    @Required
    public void setUiExperienceService(final UiExperienceService uiExperienceService)
    {
        this.uiExperienceService = uiExperienceService;
    }

    @Override
    public boolean evaluate(final CMSUiExperienceRestrictionModel restriction, final RestrictionData context)
    {
        final UiExperienceLevel uiExperienceLevel = (UiExperienceLevel)sessionService.getAttribute("device");
        if (uiExperienceLevel != null)
        {
            return uiExperienceLevel.equals(restriction.getUiExperience());
        }

        LOG.warn("Could not evaluate CMSUiExperienceRestriction. No experience level set. Returning false.");
        return false;
    }

    /**
     * @param sessionService
     *            the sessionService to set
     */
    @Required
    public void setSessionService(final SessionService sessionService)
    {
        this.sessionService = sessionService;
    }
}
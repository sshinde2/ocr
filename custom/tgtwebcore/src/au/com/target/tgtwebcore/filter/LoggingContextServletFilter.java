/**
 * 
 */
package au.com.target.tgtwebcore.filter;

import static au.com.target.tgtwebcore.constants.TgtwebcoreConstants.LOG_IDENTIFIER_REQUEST_USERAGENT;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;


/**
 * Set the JSESSIONID value into the log4j MDC so it appears in the logs
 * 
 */
public class LoggingContextServletFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(LoggingContextServletFilter.class.getName());

    private static final String LOG_IDENTIFIER_TOMCAT_SESSION_ID = "TomcatSessionId";

    @Override
    public void destroy() {
        // empty
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {


        setLoggingContext(request);


        chain.doFilter(request, response);
    }

    /**
     * Set the jsessionid into the Mapped Diagnostic Context - the default logging configuration is set up to print this
     * item if it is defined: %X{TomcatSessionId}
     * 
     */
    private void setLoggingContext(final ServletRequest request) {
        if (request instanceof HttpServletRequest) {
            final HttpServletRequest httpRequest = (HttpServletRequest)request;
            try {
                MDC.put(LOG_IDENTIFIER_TOMCAT_SESSION_ID, httpRequest.getSession().getId());
                if (httpRequest.getHeader(LOG_IDENTIFIER_REQUEST_USERAGENT) != null) {
                    MDC.put(LOG_IDENTIFIER_REQUEST_USERAGENT, httpRequest.getHeader(LOG_IDENTIFIER_REQUEST_USERAGENT));
                }
            }
            catch (final Exception e) {
                LOG.error(SplunkLogFormatter.formatMessage(
                        "Error extracting TomcatSessionId",
                        TgtutilityConstants.ErrorCode.ERR_TGTSTORE_SESSION_ID, ""), e);
            }
        }
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(final FilterConfig arg0) throws ServletException {
        // empty

    }
}

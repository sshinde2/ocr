package au.com.target.tgtwebcore.jalo.cms2.model;

import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSBrandRestrictionModel;


public class BrandRestrictionDescription extends AbstractDynamicAttributeHandler<String, CMSBrandRestrictionModel>
{
    @Override
    public String get(final CMSBrandRestrictionModel model)
    {
        final Collection<BrandModel> brands = model.getBrands();
        final StringBuilder result = new StringBuilder();
        if (CollectionUtils.isNotEmpty(brands))
        {
            final String localizedString = Localization
                    .getLocalizedString("type.CMSBrandRestriction.description.text");
            result.append(localizedString == null ? "Page only applies on brands:" : localizedString);
            for (final BrandModel brand : brands)
            {
                result.append(" ").append(brand.getName()).append(" (").append(brand.getCode()).append(");");
            }
        }
        return result.toString();
    }

}

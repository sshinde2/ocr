package au.com.target.tgtwebcore.jalo.cms2.restrictions;

import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.localization.Localization;

import java.util.Collection;

import au.com.target.tgtcore.jalo.Brand;


@SuppressWarnings("deprecation")
public class CMSBrandRestriction extends GeneratedCMSBrandRestriction
{

    @Override
    protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes)
            throws JaloBusinessException
    {
        // business code placed here will be executed before the item is created
        // then create the item
        final Item item = super.createItem(ctx, type, allAttributes);
        // business code placed here will be executed after the item was created
        // and return the item
        return item;
    }

    /**
     * @deprecated use
     *             {@link au.com.target.tgtwebcore.model.cms2.restrictions.CMSBrandRestrictionModel#getDescription()}
     *             instead.
     */
    @Deprecated
    @Override
    public String getDescription(final SessionContext ctx)
    {
        final Collection<Brand> brands = getBrands();
        final StringBuilder result = new StringBuilder();
        if (!brands.isEmpty())
        {
            final String localizedString = Localization.getLocalizedString("type.CMSBrandRestriction.description.text");
            result.append(localizedString == null ? "Page only applies on brands:" : localizedString);
            for (final Brand brand : brands)
            {
                result.append(" ").append(brand.getName(ctx)).append(" (").append(brand.getCode()).append(");");
            }
        }
        return result.toString();
    }
}

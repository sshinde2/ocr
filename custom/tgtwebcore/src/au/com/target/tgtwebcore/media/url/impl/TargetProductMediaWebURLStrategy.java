/**
 *
 */
package au.com.target.tgtwebcore.media.url.impl;

import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService;
import de.hybris.platform.media.url.impl.LocalMediaWebURLStrategy;

import org.springframework.beans.factory.annotation.Required;


/**
 * @author rsamuel3
 *
 */
public class TargetProductMediaWebURLStrategy extends LocalMediaWebURLStrategy {

    private boolean prettyUrlEnabled;

    @Override
    public String getDownloadUrlForMedia(
            final MediaStorageConfigService.MediaFolderConfig config,
            final MediaSource mediaSource) {
        final StringBuilder url = new StringBuilder(getUrlForMedia(config,
                mediaSource));
        if (!url.toString().isEmpty()) {
            if (this.prettyUrlEnabled) {
                url.append("?");
            }
            else {
                url.append("&");
            }
            url.append("attachment").append("=").append("true");
        }
        return url.toString();
    }

    /**
     * @param prettyUrlEnabled
     *            the prettyUrlEnabled to set
     */
    @Override
    @Required
    public void setPrettyUrlEnabled(final boolean prettyUrlEnabled) {
        super.setPrettyUrlEnabled(prettyUrlEnabled);
        this.prettyUrlEnabled = prettyUrlEnabled;
    }

}

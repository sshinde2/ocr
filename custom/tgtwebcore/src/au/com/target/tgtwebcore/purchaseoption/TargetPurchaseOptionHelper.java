/**
 * 
 */
package au.com.target.tgtwebcore.purchaseoption;

import java.util.List;

import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;


/**
 * @author asingh78
 * 
 */

public interface TargetPurchaseOptionHelper {
    /**
     * 
     * @param purchaseOptionCode
     * @return purchaseOption
     */
    public PurchaseOptionModel getPurchaseOptionModel(final String purchaseOptionCode);

    /**
     * Return the PurchaseOptionConfigModel link to PurchaseOptionModel from a PurchaseOptionModel code
     * 
     * @param purchaseOptionCode
     * @return PurchaseOptionConfigModel
     */
    public PurchaseOptionConfigModel getPurchaseOptionConfigModelFromOptionCode(final String purchaseOptionCode);

    /**
     * 
     * @return PurchaseOptionConfigModel
     */
    public PurchaseOptionConfigModel getCurrentPurchaseOptionConfigModel();

    /**
     * 
     * @return PurchaseOptionModel
     */
    public PurchaseOptionModel getCurrentPurchaseOptionModel();

    /**
     * 
     * @return all purchase option
     */
    public List<PurchaseOptionModel> getAllPurchaseOptionModel();

}

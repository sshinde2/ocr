/**
 * 
 */
package au.com.target.tgtwebcore.purchaseoption.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtlayby.model.PurchaseOptionConfigModel;
import au.com.target.tgtlayby.model.PurchaseOptionModel;
import au.com.target.tgtlayby.purchase.PurchaseOptionConfigService;
import au.com.target.tgtlayby.purchase.PurchaseOptionService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;


/**
 * @author asingh78
 * 
 */
public class TargetPurchaseOptionHelperImpl implements TargetPurchaseOptionHelper {

    private static final Logger LOG = Logger.getLogger(TargetPurchaseOptionHelperImpl.class);

    private PurchaseOptionService purchaseOptionService;

    private PurchaseOptionConfigService purchaseOptionConfigService;

    private String buyNow;

    @Override
    @Deprecated
    public PurchaseOptionModel getPurchaseOptionModel(final String purchaseOptionCode) {
        PurchaseOptionModel purchaseOptionModel = null;
        try {
            if (null != purchaseOptionCode) {
                purchaseOptionModel = purchaseOptionService.getPurchaseOptionForCode(purchaseOptionCode);
            }

        }
        catch (final TargetUnknownIdentifierException e) {
            LOG.error("There is no purchase option with " + purchaseOptionCode, e);
            return null;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            LOG.error("more than one purchase option with " + purchaseOptionCode, e);
            return null;
        }

        return purchaseOptionModel;
    }

    @Override
    @Deprecated
    public PurchaseOptionConfigModel getPurchaseOptionConfigModelFromOptionCode(final String purchaseOptionCode) {
        final PurchaseOptionModel purchaseOptionModel = this.getPurchaseOptionModel(purchaseOptionCode);
        try {
            return purchaseOptionConfigService.getActivePurchaseOptionConfigByPurchaseOption(purchaseOptionModel);
        }
        catch (final TargetUnknownIdentifierException e) {
            return null;
        }
    }

    @Override
    public PurchaseOptionModel getCurrentPurchaseOptionModel() {
        return purchaseOptionService.getCurrentPurchaseOption();
    }

    @Override
    public PurchaseOptionConfigModel getCurrentPurchaseOptionConfigModel() {
        return this.getPurchaseOptionConfigModelFromOptionCode(buyNow.toLowerCase());
    }

    /**
     * 
     * @param purchaseOptionService
     */
    @Required
    public void setPurchaseOptionService(final PurchaseOptionService purchaseOptionService) {
        this.purchaseOptionService = purchaseOptionService;
    }

    /**
     * @return the purchaseOptionConfigService
     */
    public PurchaseOptionConfigService getPurchaseOptionConfigService() {
        return purchaseOptionConfigService;
    }

    /**
     * @param purchaseOptionConfigService
     *            the purchaseOptionConfigService to set
     */
    public void setPurchaseOptionConfigService(final PurchaseOptionConfigService purchaseOptionConfigService) {
        this.purchaseOptionConfigService = purchaseOptionConfigService;
    }

    /**
     * @return the purchaseOptionService
     */
    public PurchaseOptionService getPurchaseOptionService() {
        return purchaseOptionService;
    }

    /**
     * @param buyNow
     *            the buyNow to set
     */
    @Required
    public void setBuyNow(final String buyNow) {
        this.buyNow = buyNow;
    }

    @Override
    @Deprecated
    public List<PurchaseOptionModel> getAllPurchaseOptionModel() {
        final ArrayList<PurchaseOptionModel> allPurchaseOption = new ArrayList<>();
        allPurchaseOption.add(getCurrentPurchaseOptionModel());
        return allPurchaseOption;
    }
}

package au.com.target.tgtwebcore.servicelayer.data;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.SizeTypeModel;


public interface TargetCMSRestrictionData extends RestrictionData {

    void setBrand(BrandModel brand);

    BrandModel getBrand();

    boolean hasBrand();

    void setSizeType(SizeTypeModel sizeType);

    SizeTypeModel getSizeType();

    boolean hasSizeType();

}

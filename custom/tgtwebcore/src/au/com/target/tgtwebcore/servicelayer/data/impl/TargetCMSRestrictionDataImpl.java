package au.com.target.tgtwebcore.servicelayer.data.impl;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.data.impl.DefaultRestrictionData;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;


public class TargetCMSRestrictionDataImpl extends DefaultRestrictionData implements TargetCMSRestrictionData {

    private BrandModel brand;
    private SizeTypeModel sizeType;

    public TargetCMSRestrictionDataImpl() {
        // Default constructor
    }

    public TargetCMSRestrictionDataImpl(final RestrictionData restrictionData) {
        this.setCatalog(restrictionData.getCatalog());
        this.setCategory(restrictionData.getCategory());
        this.setProduct(restrictionData.getProduct());
    }

    @Override
    public void setBrand(final BrandModel brand) {
        this.brand = brand;
    }

    @Override
    public BrandModel getBrand() {
        return brand;
    }

    @Override
    public boolean hasBrand() {
        return brand != null;
    }

    @Override
    public void setSizeType(final SizeTypeModel sizeType) {
        this.sizeType = sizeType;
    }

    @Override
    public SizeTypeModel getSizeType() {
        return sizeType;
    }

    @Override
    public boolean hasSizeType() {
        return sizeType != null;
    }


}

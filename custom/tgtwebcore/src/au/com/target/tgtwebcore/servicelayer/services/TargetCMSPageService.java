package au.com.target.tgtwebcore.servicelayer.services;

import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtwebcore.model.cms2.pages.BrandPageModel;
import au.com.target.tgtwebcore.model.cms2.pages.SizePageModel;


public interface TargetCMSPageService extends CMSPageService {

    /**
     * Gets the page for brand.
     * 
     * @param brandModel
     *            the brand
     * @return the page for brand
     * @throws CMSItemNotFoundException
     *             thrown when item was not found
     */
    BrandPageModel getPageForBrand(BrandModel brandModel) throws CMSItemNotFoundException;

    /**
     * Gets the page for brand code.
     * 
     * @param brandCode
     *            the brand code
     * @return the page for brand code
     * @throws CMSItemNotFoundException
     *             thrown when item was not found
     */
    BrandPageModel getPageForBrandCode(String brandCode) throws CMSItemNotFoundException;

    /**
     * Gets the page for Size Chart by sizeCode
     * 
     * @param sizeCode
     * @return SizePageModel
     * @throws CMSItemNotFoundException
     */
    SizePageModel getPageForSizeCode(String sizeCode) throws CMSItemNotFoundException;

    /**
     * Gets the page for Size Chart by SizeTypeModel
     * 
     * @param sizeTypeModel
     * @return SizePageModel
     * @throws CMSItemNotFoundException
     */
    SizePageModel getPageForSize(SizeTypeModel sizeTypeModel) throws CMSItemNotFoundException;

    /**
     * Get the page for ID then evaluates restrictions
     * 
     * @param id
     *            The page ID
     * @return The page if it's visible after restrictions, null if not
     * @throws CMSItemNotFoundException
     *             If page with ID not found, is not a ContentPageModel, or is not visible after evaluating restrictions
     * @see #getPageForId(String)
     */
    ContentPageModel getPageForIdWithRestrictions(final String id) throws CMSItemNotFoundException;
}

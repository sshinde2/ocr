package au.com.target.tgtwebcore.servicelayer.services.evaluator.impl;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;

import org.apache.log4j.Logger;

import au.com.target.tgtwebcore.model.cms2.restrictions.CMSBrandRestrictionModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;


public class CMSBrandRestrictionEvaluator implements CMSRestrictionEvaluator<CMSBrandRestrictionModel> {

    private static final Logger LOG = Logger.getLogger(CMSBrandRestrictionEvaluator.class);

    @Override
    public boolean evaluate(final CMSBrandRestrictionModel brandRestrictionModel, final RestrictionData restrictionData) {
        if (restrictionData == null)
        {
            return true;
        }
        if (!(restrictionData instanceof TargetCMSRestrictionData)) {
            return false;
        }
        final TargetCMSRestrictionData targetCMSRestrictionData = (TargetCMSRestrictionData)restrictionData;
        if (targetCMSRestrictionData.hasBrand())
        {
            return brandRestrictionModel.getBrands().contains(targetCMSRestrictionData.getBrand());
        }
        else
        {
            LOG.warn("Could not evaluate CMSBrandRestriction. RestrictionData contains no brand. Returning false.");
            return false;
        }
    }
}

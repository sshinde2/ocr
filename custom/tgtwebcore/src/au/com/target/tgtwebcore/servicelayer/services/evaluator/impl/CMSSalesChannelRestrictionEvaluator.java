/**
 * 
 */
package au.com.target.tgtwebcore.servicelayer.services.evaluator.impl;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;
import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.salesapplication.TargetSalesApplicationService;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSSalesChannelRestrictionModel;


/**
 * @author Nandini
 *
 */
public class CMSSalesChannelRestrictionEvaluator implements CMSRestrictionEvaluator<CMSSalesChannelRestrictionModel> {

    private static final Logger LOG = Logger.getLogger(CMSSalesChannelRestrictionEvaluator.class);

    private TargetSalesApplicationService targetSalesApplicationService;

    @Override
    public boolean evaluate(final CMSSalesChannelRestrictionModel restriction, final RestrictionData context) {

        final Collection<SalesApplication> restrictedSalesApps = restriction.getSalesApplications();
        if (CollectionUtils.isEmpty(restrictedSalesApps)) {
            return true;
        }
        final SalesApplication salesApplication = targetSalesApplicationService.getCurrentSalesApplication();
        if (null != salesApplication && restrictedSalesApps.contains(salesApplication)) {
            return true;
        }
        LOG.warn("Could not evaluate CMSSalesChannelRestriction. No Sales Channel set. Returning false.");
        return false;
    }

    /**
     * @param targetSalesApplicationService
     *            the targetSalesApplicationService to set
     */
    @Required
    public void setTargetSalesApplicationService(final TargetSalesApplicationService targetSalesApplicationService) {
        this.targetSalesApplicationService = targetSalesApplicationService;
    }

}

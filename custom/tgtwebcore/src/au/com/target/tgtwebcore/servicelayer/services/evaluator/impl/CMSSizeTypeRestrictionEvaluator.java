package au.com.target.tgtwebcore.servicelayer.services.evaluator.impl;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.CMSRestrictionEvaluator;

import org.apache.log4j.Logger;

import au.com.target.tgtwebcore.model.cms2.restrictions.CMSSizeTypeRestrictionModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;


public class CMSSizeTypeRestrictionEvaluator implements CMSRestrictionEvaluator<CMSSizeTypeRestrictionModel> {

    private static final Logger LOG = Logger.getLogger(CMSSizeTypeRestrictionEvaluator.class);

    @Override
    public boolean evaluate(final CMSSizeTypeRestrictionModel sizeTypeRestrictionModel,
            final RestrictionData restrictionData) {
        if (restrictionData == null)
        {
            return true;
        }
        if (!(restrictionData instanceof TargetCMSRestrictionData)) {
            return false;
        }
        final TargetCMSRestrictionData targetCMSRestrictionData = (TargetCMSRestrictionData)restrictionData;
        if (targetCMSRestrictionData.hasSizeType())
        {
            return sizeTypeRestrictionModel.getSizeTypes().contains(targetCMSRestrictionData.getSizeType());
        }
        else
        {
            LOG.warn("Could not evaluate CMSSizeTypeRestriction. RestrictionData contains no SizeType. Returning false.");
            return false;
        }
    }
}

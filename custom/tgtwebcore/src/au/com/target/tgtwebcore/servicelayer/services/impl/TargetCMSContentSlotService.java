/**
 *
 */
package au.com.target.tgtwebcore.servicelayer.services.impl;

import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSContentSlotService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;
import au.com.target.tgtwebcore.servicelayer.data.impl.TargetCMSRestrictionDataImpl;


/**
 * @author rmcalave
 *
 */
public class TargetCMSContentSlotService extends DefaultCMSContentSlotService {

    private static final Logger LOG = Logger.getLogger(TargetCMSContentSlotService.class);

    private BrandService brandService;

    /* (non-Javadoc)
     * @see de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSContentSlotService#populate(javax.servlet.http.HttpServletRequest)
     */
    @Override
    protected RestrictionData populate(final HttpServletRequest request) {
        final RestrictionData restrictionData = super.populate(request);

        final Object brand = request.getAttribute("currentBrandCode");

        if (brand instanceof String) {
            final String brandCode = brand.toString();

            if (StringUtils.isNotEmpty(brandCode)) {
                final TargetCMSRestrictionData targetRestrictionData = new TargetCMSRestrictionDataImpl(restrictionData);

                final BrandModel brandModel = brandService.getBrandForCode(brandCode);
                if (brandModel != null) {
                    targetRestrictionData.setBrand(brandModel);
                }
                else {
                    LOG.info("Could not find brand with code <" + brandCode + ">");
                }

                return targetRestrictionData;
            }
        }

        return restrictionData;
    }

    /**
     * @param brandService
     *            the brandService to set
     */
    @Required
    public void setBrandService(final BrandService brandService) {
        this.brandService = brandService;
    }

}

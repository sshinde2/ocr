package au.com.target.tgtwebcore.servicelayer.services.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.servicelayer.data.CMSDataFactory;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.core.model.product.ProductModel;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;


@RunWith(MockitoJUnitRunner.class)
public class TargetCMSContentSlotServiceTest {
    @Mock
    private BrandService mockBrandService;

    @Mock
    private CMSDataFactory cmsDataFactory;

    @InjectMocks
    private final TargetCMSContentSlotService targetCMSContentSlotService = new TargetCMSContentSlotService();

    @Test
    public void testPopulateWithoutBrand() {
        final String catalogId = "A catalog ID";
        final String currentCategoryCode = "A category code";
        final String currentProductCode = "A product code";

        final HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        given(mockHttpServletRequest.getAttribute("catalogId")).willReturn(catalogId);
        given(mockHttpServletRequest.getAttribute("currentCategoryCode")).willReturn(currentCategoryCode);
        given(mockHttpServletRequest.getAttribute("currentProductCode")).willReturn(currentProductCode);

        final RestrictionData mockRestrictionData = mock(RestrictionData.class);
        given(cmsDataFactory.createRestrictionData(currentCategoryCode, currentProductCode, catalogId)).willReturn(
                mockRestrictionData);

        final RestrictionData result = targetCMSContentSlotService.populate(mockHttpServletRequest);

        assertThat(result).isEqualTo(mockRestrictionData);
        verifyZeroInteractions(mockRestrictionData);
    }

    @Test
    public void testPopulateWithBrandNotString() {
        final String catalogId = "A catalog ID";
        final String currentCategoryCode = "A category code";
        final String currentProductCode = "A product code";

        final Object mockBrand = mock(Object.class);

        final HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        given(mockHttpServletRequest.getAttribute("catalogId")).willReturn(catalogId);
        given(mockHttpServletRequest.getAttribute("currentCategoryCode")).willReturn(currentCategoryCode);
        given(mockHttpServletRequest.getAttribute("currentProductCode")).willReturn(currentProductCode);
        given(mockHttpServletRequest.getAttribute("currentBrandCode")).willReturn(mockBrand);

        final RestrictionData mockRestrictionData = mock(RestrictionData.class);
        given(cmsDataFactory.createRestrictionData(currentCategoryCode, currentProductCode, catalogId)).willReturn(
                mockRestrictionData);

        final RestrictionData result = targetCMSContentSlotService.populate(mockHttpServletRequest);

        assertThat(result).isEqualTo(mockRestrictionData);
        verifyZeroInteractions(mockRestrictionData);
    }

    @Test
    public void testPopulateWithBlankBrand() {
        final String catalogId = "A catalog ID";
        final String currentCategoryCode = "A category code";
        final String currentProductCode = "A product code";

        final String brand = "";

        final HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        given(mockHttpServletRequest.getAttribute("catalogId")).willReturn(catalogId);
        given(mockHttpServletRequest.getAttribute("currentCategoryCode")).willReturn(currentCategoryCode);
        given(mockHttpServletRequest.getAttribute("currentProductCode")).willReturn(currentProductCode);
        given(mockHttpServletRequest.getAttribute("currentBrandCode")).willReturn(brand);

        final RestrictionData mockRestrictionData = mock(RestrictionData.class);
        given(cmsDataFactory.createRestrictionData(currentCategoryCode, currentProductCode, catalogId)).willReturn(
                mockRestrictionData);

        final RestrictionData result = targetCMSContentSlotService.populate(mockHttpServletRequest);

        assertThat(result).isEqualTo(mockRestrictionData);
        verifyZeroInteractions(mockRestrictionData);
    }

    @Test
    public void testPopulateNotExistingBrand() {
        final String catalogId = "A catalog ID";
        final String currentCategoryCode = "A category code";
        final String currentProductCode = "A product code";

        final String brand = "lego";

        final HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        given(mockHttpServletRequest.getAttribute("catalogId")).willReturn(catalogId);
        given(mockHttpServletRequest.getAttribute("currentCategoryCode")).willReturn(currentCategoryCode);
        given(mockHttpServletRequest.getAttribute("currentProductCode")).willReturn(currentProductCode);
        given(mockHttpServletRequest.getAttribute("currentBrandCode")).willReturn(brand);

        final CatalogModel mockCatalog = mock(CatalogModel.class);
        final CategoryModel mockCategory = mock(CategoryModel.class);
        final ProductModel mockProduct = mock(ProductModel.class);

        final RestrictionData mockRestrictionData = mock(RestrictionData.class);
        given(mockRestrictionData.getCatalog()).willReturn(mockCatalog);
        given(mockRestrictionData.getCategory()).willReturn(mockCategory);
        given(mockRestrictionData.getProduct()).willReturn(mockProduct);

        given(cmsDataFactory.createRestrictionData(currentCategoryCode, currentProductCode, catalogId)).willReturn(
                mockRestrictionData);

        final RestrictionData result = targetCMSContentSlotService.populate(mockHttpServletRequest);

        assertThat(result).isInstanceOf(TargetCMSRestrictionData.class);

        assertThat(result.getCatalog()).isEqualTo(mockRestrictionData.getCatalog());
        assertThat(result.getCategory()).isEqualTo(mockRestrictionData.getCategory());
        assertThat(result.getProduct()).isEqualTo(mockRestrictionData.getProduct());
    }

    @Test
    public void testPopulate() {
        final String catalogId = "A catalog ID";
        final String currentCategoryCode = "A category code";
        final String currentProductCode = "A product code";

        final String brand = "lego";

        final HttpServletRequest mockHttpServletRequest = mock(HttpServletRequest.class);
        given(mockHttpServletRequest.getAttribute("catalogId")).willReturn(catalogId);
        given(mockHttpServletRequest.getAttribute("currentCategoryCode")).willReturn(currentCategoryCode);
        given(mockHttpServletRequest.getAttribute("currentProductCode")).willReturn(currentProductCode);
        given(mockHttpServletRequest.getAttribute("currentBrandCode")).willReturn(brand);

        final CatalogModel mockCatalog = mock(CatalogModel.class);
        final CategoryModel mockCategory = mock(CategoryModel.class);
        final ProductModel mockProduct = mock(ProductModel.class);

        final RestrictionData mockRestrictionData = mock(RestrictionData.class);
        given(mockRestrictionData.getCatalog()).willReturn(mockCatalog);
        given(mockRestrictionData.getCategory()).willReturn(mockCategory);
        given(mockRestrictionData.getProduct()).willReturn(mockProduct);

        final BrandModel mockBrand = mock(BrandModel.class);
        given(mockBrandService.getBrandForCode(brand)).willReturn(mockBrand);

        given(cmsDataFactory.createRestrictionData(currentCategoryCode, currentProductCode, catalogId)).willReturn(
                mockRestrictionData);

        final RestrictionData result = targetCMSContentSlotService.populate(mockHttpServletRequest);

        assertThat(result).isInstanceOf(TargetCMSRestrictionData.class);

        assertThat(result.getCatalog()).isEqualTo(mockRestrictionData.getCatalog());
        assertThat(result.getCategory()).isEqualTo(mockRestrictionData.getCategory());
        assertThat(result.getProduct()).isEqualTo(mockRestrictionData.getProduct());

        assertThat(((TargetCMSRestrictionData)result).getBrand()).isEqualTo(mockBrand);
    }
}

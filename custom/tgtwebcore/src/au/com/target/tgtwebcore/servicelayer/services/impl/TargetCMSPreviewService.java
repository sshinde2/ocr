package au.com.target.tgtwebcore.servicelayer.services.impl;

import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.cms2.servicelayer.services.impl.DefaultCMSPreviewService;
import de.hybris.platform.servicelayer.model.ModelService;

import au.com.target.tgtwebcore.model.cms2.preview.TargetPreviewDataModel;


public class TargetCMSPreviewService extends DefaultCMSPreviewService {

    @Override
    public PreviewDataModel clonePreviewData(final PreviewDataModel original) {
        if (original == null) {
            return new TargetPreviewDataModel();
        }

        //refreshing 
        final ModelService modelService = getModelService();

        if (!modelService.isNew(original) && !modelService.isRemoved(original))
        {
            modelService.refresh(original);
        }

        return getModelService().clone(original, TargetPreviewDataModel.class);
    }
}

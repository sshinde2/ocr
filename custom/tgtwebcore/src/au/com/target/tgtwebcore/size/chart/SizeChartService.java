/**
 * 
 */
package au.com.target.tgtwebcore.size.chart;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtwebcore.model.SizeChartModel;



/**
 * @author Benoit Vanalderweireldt
 * 
 */
public interface SizeChartService {
    /**
     * 
     * @param sizeName
     * @return SizeChartModel
     * @throws TargetAmbiguousIdentifierException
     * @throws TargetUnknownIdentifierException
     */
    SizeChartModel getSizeChartBySizeName(String sizeName) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException;
}

/**
 * 
 */
package au.com.target.tgtwebcore.size.chart.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.util.TargetServicesUtil;
import au.com.target.tgtwebcore.model.SizeChartModel;
import au.com.target.tgtwebcore.size.chart.dao.SizeChartDao;



/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class SizeChartDaoImpl extends DefaultGenericDao<SizeChartModel> implements SizeChartDao {

    private static final String QUERY_GET_SIZE_CHART_BY_INSENSITIVE_SIZE_NAME = "select * FROM {"
            + SizeChartModel._TYPECODE + " AS sc} WHERE LOWER({sc." + SizeChartModel.SIZE + "}) like LOWER(?"
            + SizeChartModel.SIZE + ")";

    public SizeChartDaoImpl() {
        super(SizeChartModel._TYPECODE);
    }

    @Override
    public SizeChartModel getSizeChartBySizeName(final String sizeName) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {

        final FlexibleSearchQuery query = new FlexibleSearchQuery(QUERY_GET_SIZE_CHART_BY_INSENSITIVE_SIZE_NAME);
        query.addQueryParameter(SizeChartModel.SIZE, sizeName);

        final SearchResult<SizeChartModel> sizeChartModels = getFlexibleSearchService().search(query);

        TargetServicesUtil.validateIfSingleResult(sizeChartModels.getResult(), SizeChartModel.class,
                SizeChartModel.SIZE,
                sizeName);
        return sizeChartModels.getResult().get(0);
    }
}

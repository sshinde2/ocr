/**
 * 
 */
package au.com.target.tgtwebcore.size.chart.impl;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtwebcore.model.SizeChartModel;
import au.com.target.tgtwebcore.size.chart.SizeChartService;
import au.com.target.tgtwebcore.size.chart.dao.SizeChartDao;



/**
 * @author Benoit Vanalderweireldt
 * 
 */
public class SizeChartServiceImpl implements SizeChartService {

    private SizeChartDao sizeChartDao;

    @Override
    public SizeChartModel getSizeChartBySizeName(final String sizeName) throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        return sizeChartDao.getSizeChartBySizeName(sizeName);
    }

    /**
     * @param sizeChartDao
     *            the sizeChartDao to set
     */
    public void setSizeChartDao(final SizeChartDao sizeChartDao) {
        this.sizeChartDao = sizeChartDao;
    }

}

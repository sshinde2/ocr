/**
 * 
 */
package au.com.target.tgtwebcore.userreviews;

import au.com.target.tgtwebcore.userreviews.data.TargetUserReviewRequest;
import au.com.target.tgtwebcore.userreviews.data.TargetUserReviewResponse;


/**
 * @author knemalik
 * 
 */
public interface TargetUserReviewService {

    /**
     * Gets the user review content.
     * 
     * @param targetUserReviewReqParams
     *            the targetUserReviewReqParams
     * 
     * @return TargetUserReviewResponse
     */
    TargetUserReviewResponse getUserReviewsContent(TargetUserReviewRequest targetUserReviewReqParams);
}

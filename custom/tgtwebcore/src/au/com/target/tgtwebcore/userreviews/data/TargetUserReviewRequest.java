/**
 * 
 */
package au.com.target.tgtwebcore.userreviews.data;

/**
 * @author knemalik
 * 
 */
public class TargetUserReviewRequest {
    private String userAgent;
    private String baseURI;
    private String pageURI;
    private String product;

    /**
     * @return the userAgent
     */
    public String getUserAgent() {
        return userAgent;
    }

    /**
     * @param userAgent
     *            the userAgent to set
     */
    public void setUserAgent(final String userAgent) {
        this.userAgent = userAgent;
    }

    /**
     * @return the baseURI
     */
    public String getBaseURI() {
        return baseURI;
    }

    /**
     * @param baseURI
     *            the baseURI to set
     */
    public void setBaseURI(final String baseURI) {
        this.baseURI = baseURI;
    }

    /**
     * @return the pageURI
     */
    public String getPageURI() {
        return pageURI;
    }

    /**
     * @param pageURI
     *            the pageURI to set
     */
    public void setPageURI(final String pageURI) {
        this.pageURI = pageURI;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(final String product) {
        this.product = product;
    }

}

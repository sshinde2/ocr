package au.com.target.tgtwebcore.cache.dao.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.AkamaiCacheConfigurationModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AkamaiCacheConfigurationDaoImplTest {
    @Mock
    private FlexibleSearchService mockFlexibleSearchService;
    @InjectMocks
    private final AkamaiCacheConfigurationDaoImpl akamaiCacheConfigurationDaoImpl = new AkamaiCacheConfigurationDaoImpl();

    @Test(expected = IllegalArgumentException.class)
    public void testGetAkamaiCacheConfigurationByPageTypeWithNullPageType() throws Exception {
        akamaiCacheConfigurationDaoImpl.getAkamaiCacheConfigurationByPageType(null);
    }

    @Test
    public void testGetAkamaiCacheConfigurationByPageType() throws Exception {
        final AkamaiCacheConfigurationModel mockAkamaiCacheConfigurationModel = Mockito
                .mock(AkamaiCacheConfigurationModel.class);

        final SearchResult mockSearchResult = Mockito.mock(SearchResult.class);
        BDDMockito.given(mockSearchResult.getResult()).willReturn(
                Collections.singletonList(mockAkamaiCacheConfigurationModel));

        BDDMockito.given(mockFlexibleSearchService.search(Mockito.<FlexibleSearchQuery> any())).willReturn(
                mockSearchResult);

        final AkamaiCacheConfigurationModel searchResult = akamaiCacheConfigurationDaoImpl
                .getAkamaiCacheConfigurationByPageType(CachedPageType.HOME);

        Assert.assertEquals(mockAkamaiCacheConfigurationModel, searchResult);
    }

    @Test(expected = TargetUnknownIdentifierException.class)
    public void testGetAkamaiCacheConfigurationByPageTypeNoMatches() throws Exception {
        final SearchResult mockSearchResult = Mockito.mock(SearchResult.class);

        BDDMockito.given(mockFlexibleSearchService.search(Mockito.<FlexibleSearchQuery> any())).willReturn(
                mockSearchResult);

        akamaiCacheConfigurationDaoImpl.getAkamaiCacheConfigurationByPageType(CachedPageType.HOME);
    }

    @Test(expected = TargetAmbiguousIdentifierException.class)
    public void testGetAkamaiCacheConfigurationByPageTypeTooManyMatches() throws Exception {
        final AkamaiCacheConfigurationModel mockAkamaiCacheConfigurationModel = Mockito
                .mock(AkamaiCacheConfigurationModel.class);
        final AkamaiCacheConfigurationModel mockAkamaiCacheConfigurationModel2 = Mockito
                .mock(AkamaiCacheConfigurationModel.class);

        final List<AkamaiCacheConfigurationModel> akamaiCacheConfigurationList = new ArrayList<>();
        akamaiCacheConfigurationList.add(mockAkamaiCacheConfigurationModel);
        akamaiCacheConfigurationList.add(mockAkamaiCacheConfigurationModel2);

        final SearchResult mockSearchResult = Mockito.mock(SearchResult.class);
        BDDMockito.given(mockSearchResult.getResult()).willReturn(akamaiCacheConfigurationList);

        BDDMockito.given(mockFlexibleSearchService.search(Mockito.<FlexibleSearchQuery> any())).willReturn(
                mockSearchResult);

        akamaiCacheConfigurationDaoImpl.getAkamaiCacheConfigurationByPageType(CachedPageType.HOME);
    }
}

package au.com.target.tgtwebcore.cache.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtwebcore.cache.dao.AkamaiCacheConfigurationDao;
import au.com.target.tgtwebcore.enums.CachedPageType;
import au.com.target.tgtwebcore.model.AkamaiCacheConfigurationModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AkamaiCacheConfigurationServiceImplTest {

    @Mock
    private AkamaiCacheConfigurationDao mockAkamaiCacheConfigurationDao;
    @InjectMocks
    private final AkamaiCacheConfigurationServiceImpl akamaiCacheConfigurationServiceImpl = new AkamaiCacheConfigurationServiceImpl();

    @Test(expected = IllegalArgumentException.class)
    public void testGetAkamaiCacheConfigurationWithNullPageType() {
        akamaiCacheConfigurationServiceImpl.getAkamaiCacheConfiguration(null);
    }

    @Test
    public void testGetAkamaiCacheConfigurationWithNoResults() throws Exception {
        BDDMockito.given(mockAkamaiCacheConfigurationDao.getAkamaiCacheConfigurationByPageType(CachedPageType.PRODUCT))
                .willThrow(new TargetUnknownIdentifierException("Not found!"));

        final AkamaiCacheConfigurationModel result = akamaiCacheConfigurationServiceImpl
                .getAkamaiCacheConfiguration(CachedPageType.PRODUCT);

        Assert.assertNull(result);
    }

    @Test
    public void testGetAkamaiCacheConfigurationWithTooManyResults() throws Exception {
        BDDMockito.given(mockAkamaiCacheConfigurationDao.getAkamaiCacheConfigurationByPageType(CachedPageType.PRODUCT))
                .willThrow(new TargetAmbiguousIdentifierException("Too many!"));

        final AkamaiCacheConfigurationModel result = akamaiCacheConfigurationServiceImpl
                .getAkamaiCacheConfiguration(CachedPageType.PRODUCT);

        Assert.assertNull(result);
    }

    @Test
    public void testGetAkamaiCacheConfiguration() throws Exception {
        final AkamaiCacheConfigurationModel mockAkamaiCacheConfigurationModel = Mockito
                .mock(AkamaiCacheConfigurationModel.class);
        BDDMockito.given(mockAkamaiCacheConfigurationDao.getAkamaiCacheConfigurationByPageType(CachedPageType.PRODUCT))
                .willReturn(mockAkamaiCacheConfigurationModel);

        final AkamaiCacheConfigurationModel result = akamaiCacheConfigurationServiceImpl
                .getAkamaiCacheConfiguration(CachedPageType.PRODUCT);

        Assert.assertEquals(mockAkamaiCacheConfigurationModel, result);
    }
}

/**
 * 
 */
package au.com.target.tgtwebcore.evaluator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.restrictions.CMSCategoryRestrictionModel;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;
import de.hybris.platform.cms2.servicelayer.services.evaluator.impl.CMSCategoryRestrictionEvaluator;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;

import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;


/**
 * @author vmuthura
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCMSCategoryRestrictionEvaluatorTest {

    private CMSCategoryRestrictionEvaluator evaluator;
    private CMSCategoryRestrictionModel restrictionModel;
    private RestrictionData restrictionData;
    private CategoryModel category;

    private TargetColourVariantProductModel colourVariant;

    @SuppressWarnings("deprecation")
    @Before
    public void setUp() {
        evaluator = new TargetCMSCategoryRestrictionEvaluator();

        restrictionModel = mock(CMSCategoryRestrictionModel.class);
        restrictionData = mock(RestrictionData.class);
        colourVariant = mock(TargetColourVariantProductModel.class);
        final ProductModel baseProduct = mock(ProductModel.class);
        category = mock(CategoryModel.class);

        when(Boolean.valueOf(restrictionModel.isRecursive())).thenReturn(Boolean.TRUE);
        when(restrictionModel.getCategoryCodes()).thenReturn(ImmutableList.of("W12345"));

        when(Boolean.valueOf(restrictionData.hasProduct())).thenReturn(Boolean.TRUE);
        when(Boolean.valueOf(restrictionData.hasCategory())).thenReturn(Boolean.FALSE);
        when(restrictionData.getProduct()).thenReturn(colourVariant);

        when(colourVariant.getBaseProduct()).thenReturn(baseProduct);
        when(colourVariant.getSupercategories()).thenReturn(new LinkedList());

        when(baseProduct.getSupercategories()).thenReturn(ImmutableList.of(category));
    }

    @Test
    public void testCategoryRestrictionForRestrictedCategory() {
        when(category.getCode()).thenReturn("W12345");
        assertTrue(evaluator.evaluate(restrictionModel, restrictionData));
    }

    @Test
    public void testCategoryRestrictionForNotRestrictedCategory() {
        when(category.getCode()).thenReturn("W54321");
        assertFalse(evaluator.evaluate(restrictionModel, restrictionData));
    }

    @Test
    public void testCategoryRestrictionForColourVariant() {
        final TargetSizeVariantProductModel sizeVariant = mock(TargetSizeVariantProductModel.class);

        when(sizeVariant.getBaseProduct()).thenReturn(colourVariant);
        when(sizeVariant.getSupercategories()).thenReturn(new LinkedList());
        when(category.getCode()).thenReturn("W12345");
        when(restrictionData.getProduct()).thenReturn(sizeVariant);

        assertTrue(evaluator.evaluate(restrictionModel, restrictionData));
    }
}

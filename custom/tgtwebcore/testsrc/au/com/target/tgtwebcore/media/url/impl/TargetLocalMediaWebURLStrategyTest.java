/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtwebcore.media.url.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.media.Media;
import de.hybris.platform.media.MediaSource;
import de.hybris.platform.media.impl.JaloMediaSource;
import de.hybris.platform.media.storage.MediaStorageConfigService.MediaFolderConfig;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


@UnitTest
public class TargetLocalMediaWebURLStrategyTest
{

    private final TargetLocalMediaWebURLStrategy strategy = new TargetLocalMediaWebURLStrategy()
    {
        @Override
        protected String getTenantId()
        {
            return "master";
        }
    };
    @Mock
    private JaloMediaSource mockMedia;
    @Mock
    private Media jaloMedia;
    @Mock
    private MediaFolderConfig mockFolderConfig;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        given(mockMedia.getSource()).willReturn(jaloMedia);
        given(jaloMedia.getRealFileName()).willReturn("realFilename.jpg");
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenGettingUrlForMediaAndFolderQualifierIsNull()
    {
        // given
        final MediaFolderConfig folderConfig = null;

        try
        {
            // when
            strategy.getUrlForMedia(folderConfig, mockMedia);
            fail("Should throw IllegalArgumentException");
        }
        catch (final IllegalArgumentException e)
        {
            // then
            assertThat(e).hasMessage("Folder config is required to perform this operation");
        }
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenGettingUrlForMediaAndMediaIsNull()
    {
        // given
        final MediaSource media = null;

        try
        {
            // when
            strategy.getUrlForMedia(mockFolderConfig, media);
            fail("Should throw IllegalArgumentException");
        }
        catch (final IllegalArgumentException e)
        {
            // then
            assertThat(e).hasMessage("MediaSource is required to perform this operation");
        }
    }



    @Test
    public void shouldReturnValidDownloadUrlForGivenFolder()
    {
        // given
        given(mockFolderConfig.getFolderQualifier()).willReturn("root");
        given(mockMedia.getLocation()).willReturn("h01/h02/foo.jpg");
        given(mockMedia.getSize()).willReturn(Long.valueOf(12345));
        given(mockMedia.getLocationHash()).willReturn("qwerty12345");
        given(mockMedia.getMime()).willReturn("image/jpeg");

        // when
        final String urlForMedia = strategy.getDownloadUrlForMedia(mockFolderConfig, mockMedia);

        // then
        assertThat(urlForMedia).isNotNull();
        assertThat(urlForMedia)
                .isEqualTo(
                        "/medias/sys_master/h01/h02/foo.jpg?attachment=true");
    }

    @Test
    public void shouldReturnValidPrettyUrlForRootMediaFolder() throws Exception
    {
        // given
        given(mockFolderConfig.getFolderQualifier()).willReturn("root");
        given(mockMedia.getLocation()).willReturn("h01/h02/foo.jpg");
        given(mockMedia.getSize()).willReturn(Long.valueOf(12345));
        given(mockMedia.getLocationHash()).willReturn("qwerty12345");
        given(mockMedia.getMime()).willReturn("image/jpeg");

        // when
        strategy.setPrettyUrlEnabled(true);
        final String urlForMedia = strategy.getUrlForMedia(mockFolderConfig, mockMedia);

        // then
        assertThat(urlForMedia).isEqualTo("/medias/sys_master/h01/h02/foo.jpg");
    }

}

package au.com.target.tgtwebcore.servicelayer.services.evaluator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSBrandRestrictionModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;


/**
 * Unit test for {@link CMSBrandRestrictionEvaluator}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CMSBrandRestrictionEvaluatorTest {

    private final CMSBrandRestrictionEvaluator cmsBrandRestrictionEvaluator = new CMSBrandRestrictionEvaluator();

    @Test
    public void testEvaluateWithNullRestrictionData() {
        final CMSBrandRestrictionModel brandRestrictionModel = Mockito.mock(CMSBrandRestrictionModel.class);
        final boolean result = cmsBrandRestrictionEvaluator.evaluate(brandRestrictionModel, null);
        Assert.assertTrue(result);
    }

    @Test
    public void testEvaluateWithNonTargetRestrictionData() {
        final CMSBrandRestrictionModel brandRestrictionModel = Mockito.mock(CMSBrandRestrictionModel.class);
        final RestrictionData restrictionData = Mockito.mock(RestrictionData.class);
        final boolean result = cmsBrandRestrictionEvaluator.evaluate(brandRestrictionModel, restrictionData);
        Assert.assertFalse(result);
    }

    @Test
    public void testEvaluateForRestrictionDataWithoutBrand() {
        final CMSBrandRestrictionModel brandRestrictionModel = Mockito.mock(CMSBrandRestrictionModel.class);
        final RestrictionData restrictionData = Mockito.mock(TargetCMSRestrictionData.class);
        final boolean result = cmsBrandRestrictionEvaluator.evaluate(brandRestrictionModel, restrictionData);
        Assert.assertFalse(result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEvaluateWithoutRestrictedBrand() {
        final CMSBrandRestrictionModel brandRestrictionModel = Mockito.mock(CMSBrandRestrictionModel.class);
        final TargetCMSRestrictionData restrictionData = Mockito.mock(TargetCMSRestrictionData.class);

        final BrandModel targetBrand = Mockito.mock(BrandModel.class);
        final BrandModel appleBrand = Mockito.mock(BrandModel.class);

        BDDMockito.given(restrictionData.getBrand()).willReturn(targetBrand);
        BDDMockito.given(restrictionData.hasBrand()).willReturn(Boolean.TRUE);
        BDDMockito.given(brandRestrictionModel.getBrands()).willReturn(Collections.singletonList(appleBrand));

        final boolean result = cmsBrandRestrictionEvaluator.evaluate(brandRestrictionModel, restrictionData);
        Assert.assertFalse(result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEvaluateWithRestrictedBrand() {
        final CMSBrandRestrictionModel brandRestrictionModel = Mockito.mock(CMSBrandRestrictionModel.class);
        final TargetCMSRestrictionData restrictionData = Mockito.mock(TargetCMSRestrictionData.class);

        final BrandModel targetBrand = Mockito.mock(BrandModel.class);

        BDDMockito.given(restrictionData.getBrand()).willReturn(targetBrand);
        BDDMockito.given(restrictionData.hasBrand()).willReturn(Boolean.TRUE);
        BDDMockito.given(brandRestrictionModel.getBrands()).willReturn(Collections.singletonList(targetBrand));

        final boolean result = cmsBrandRestrictionEvaluator.evaluate(brandRestrictionModel, restrictionData);
        Assert.assertTrue(result);
    }
}

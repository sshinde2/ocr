package au.com.target.tgtwebcore.servicelayer.services.evaluator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.servicelayer.data.RestrictionData;

import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtwebcore.model.cms2.restrictions.CMSSizeTypeRestrictionModel;
import au.com.target.tgtwebcore.servicelayer.data.TargetCMSRestrictionData;


/**
 * Unit test for {@link CMSSizeTypeRestrictionEvaluator}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CMSSizeTypeRestrictionEvaluatorTest {

    private final CMSSizeTypeRestrictionEvaluator cmsSizeTypeRestrictionEvaluator = new CMSSizeTypeRestrictionEvaluator();

    @Test
    public void testEvaluateWithNullRestrictionData() {
        final CMSSizeTypeRestrictionModel sizeTypeRestrictionModel = Mockito.mock(CMSSizeTypeRestrictionModel.class);
        final boolean result = cmsSizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, null);
        Assert.assertTrue(result);
    }

    @Test
    public void testEvaluateWithNonTargetRestrictionData() {
        final CMSSizeTypeRestrictionModel sizeTypeRestrictionModel = Mockito.mock(CMSSizeTypeRestrictionModel.class);
        final RestrictionData restrictionData = Mockito.mock(RestrictionData.class);
        final boolean result = cmsSizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, restrictionData);
        Assert.assertFalse(result);
    }

    @Test
    public void testEvaluateForRestrictionDataWithoutSizeType() {
        final CMSSizeTypeRestrictionModel sizeTypeRestrictionModel = Mockito.mock(CMSSizeTypeRestrictionModel.class);
        final RestrictionData restrictionData = Mockito.mock(TargetCMSRestrictionData.class);
        final boolean result = cmsSizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, restrictionData);
        Assert.assertFalse(result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEvaluateWithoutRestrictedSizeType() {
        final CMSSizeTypeRestrictionModel sizeTypeRestrictionModel = Mockito.mock(CMSSizeTypeRestrictionModel.class);
        final TargetCMSRestrictionData restrictionData = Mockito.mock(TargetCMSRestrictionData.class);

        final SizeTypeModel targetSizeType1 = Mockito.mock(SizeTypeModel.class);
        final SizeTypeModel targetSizeType2 = Mockito.mock(SizeTypeModel.class);

        BDDMockito.given(restrictionData.getSizeType()).willReturn(targetSizeType1);
        BDDMockito.given(restrictionData.hasSizeType()).willReturn(Boolean.TRUE);
        BDDMockito.given(sizeTypeRestrictionModel.getSizeTypes())
                .willReturn(Collections.singletonList(targetSizeType2));

        final boolean result = cmsSizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, restrictionData);
        Assert.assertFalse(result);
    }

    @SuppressWarnings("boxing")
    @Test
    public void testEvaluateWithRestrictedSizeType() {
        final CMSSizeTypeRestrictionModel sizeTypeRestrictionModel = Mockito.mock(CMSSizeTypeRestrictionModel.class);
        final TargetCMSRestrictionData restrictionData = Mockito.mock(TargetCMSRestrictionData.class);

        final SizeTypeModel targetSizeType = Mockito.mock(SizeTypeModel.class);

        BDDMockito.given(restrictionData.getSizeType()).willReturn(targetSizeType);
        BDDMockito.given(restrictionData.hasSizeType()).willReturn(Boolean.TRUE);
        BDDMockito.given(sizeTypeRestrictionModel.getSizeTypes()).willReturn(Collections.singletonList(targetSizeType));

        final boolean result = cmsSizeTypeRestrictionEvaluator.evaluate(sizeTypeRestrictionModel, restrictionData);
        Assert.assertTrue(result);
    }
}

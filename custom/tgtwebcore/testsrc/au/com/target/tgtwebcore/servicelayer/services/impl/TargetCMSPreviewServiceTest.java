package au.com.target.tgtwebcore.servicelayer.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.preview.PreviewDataModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwebcore.model.cms2.preview.TargetPreviewDataModel;


/**
 * Unit test for {@link TargetCMSPreviewService}
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCMSPreviewServiceTest {

    @InjectMocks
    private final TargetCMSPreviewService targetCMSPreviewService = new TargetCMSPreviewService();

    @Mock
    private ModelService modelService;

    @Test
    public void testClonePreviewDataForNull() {
        final PreviewDataModel previewModel = targetCMSPreviewService.clonePreviewData(null);
        Assert.assertTrue(previewModel instanceof TargetPreviewDataModel);
    }

    @Test
    public void testClonePreviewDataForNewPreviewData() {
        final PreviewDataModel original = Mockito.mock(PreviewDataModel.class);
        Mockito.when(Boolean.valueOf(modelService.isNew(original))).thenReturn(Boolean.TRUE);
        Mockito.when(Boolean.valueOf(modelService.isRemoved(original))).thenReturn(Boolean.FALSE);
        targetCMSPreviewService.clonePreviewData(original);
        Mockito.verify(modelService, Mockito.never()).refresh(original);
        Mockito.verify(modelService).clone(original, TargetPreviewDataModel.class);
    }

    @Test
    public void testClonePreviewDataForRemovedPreviewData() {
        final PreviewDataModel original = Mockito.mock(PreviewDataModel.class);
        Mockito.when(Boolean.valueOf(modelService.isNew(original))).thenReturn(Boolean.FALSE);
        Mockito.when(Boolean.valueOf(modelService.isRemoved(original))).thenReturn(Boolean.TRUE);
        targetCMSPreviewService.clonePreviewData(original);
        Mockito.verify(modelService, Mockito.never()).refresh(original);
        Mockito.verify(modelService).clone(original, TargetPreviewDataModel.class);
    }

    @Test
    public void testClonePreviewDataForModifiedPreviewData() {
        final PreviewDataModel original = Mockito.mock(PreviewDataModel.class);
        Mockito.when(Boolean.valueOf(modelService.isNew(original))).thenReturn(Boolean.FALSE);
        Mockito.when(Boolean.valueOf(modelService.isRemoved(original))).thenReturn(Boolean.FALSE);
        targetCMSPreviewService.clonePreviewData(original);
        Mockito.verify(modelService).refresh(original);
        Mockito.verify(modelService).clone(original, TargetPreviewDataModel.class);
    }

}

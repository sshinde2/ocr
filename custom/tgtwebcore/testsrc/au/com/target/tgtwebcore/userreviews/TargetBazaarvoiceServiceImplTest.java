/**
 *
 */
package au.com.target.tgtwebcore.userreviews;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import junit.framework.Assert;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtwebcore.userreviews.data.TargetUserReviewRequest;
import au.com.target.tgtwebcore.userreviews.data.TargetUserReviewResponse;
import au.com.target.tgtwebcore.userreviews.impl.TargetBazaarvoiceServiceImpl;

import com.bazaarvoice.seo.sdk.BVUIContent;
import com.bazaarvoice.seo.sdk.config.BVConfiguration;
import com.bazaarvoice.seo.sdk.model.BVParameters;
import com.bazaarvoice.seo.sdk.validation.BVDefaultValidator;
import com.bazaarvoice.seo.sdk.validation.BVValidator;


/**
 * @author knemalik
 * 
 */
@UnitTest
public class TargetBazaarvoiceServiceImplTest {

    private static final String BV_SDK_ENABLED = "tgtwebcore.bvseo.sdk.enabled";

    @Mock
    private BVUIContent bvUIContent;

    @Mock
    private BVParameters bvParameters;

    @Mock
    private BVConfiguration bvConfiguration;

    @Mock
    private BVDefaultValidator bvDefaultValidator;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    private String validUserAgent;
    private String baseURI;
    private String pageURI;
    private String testProduct;
    private String rootFolder;
    private String cloudKey;
    private String crawlerAgentPattern;

    private class TargetBazaarvoiceMockServiceImpl extends TargetBazaarvoiceServiceImpl {
        @Override
        public BVUIContent getBVManagedUIContentOutput(final BVConfiguration bvConfig) {
            return bvUIContent;
        }

        @Override
        public BVValidator getBVValidator() {
            return bvDefaultValidator;
        }
    }

    @InjectMocks
    private final TargetBazaarvoiceMockServiceImpl targetUserReviewServiceImpl = new TargetBazaarvoiceMockServiceImpl();


    private void setupDataForBotTests() {
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvDefaultValidator.validate(Mockito.any(BVConfiguration.class), Mockito.any(BVParameters.class)))
                .thenReturn("");
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn("Content");
        Mockito.when(bvUIContent.getAggregateRating(Mockito.any(BVParameters.class))).thenReturn(null);
        Mockito.when(bvUIContent.getReviews(Mockito.any(BVParameters.class))).thenReturn(null);
    }

    @Before
    public void prepare()
    {
        MockitoAnnotations.initMocks(this);
        testProduct = "P56090499";
        cloudKey = "targetaustralia-976bff0df6147427b9f6caaf2ca6541d";
        rootFolder = "6939-en_au";
        validUserAgent = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)";
        baseURI = "http://www.target.com.au/p/girls-chalk-board-tulle-pinafore/P56090499";
        pageURI = "http://www.target.com.au/p/girls-chalk-board-tulle-pinafore/P56090499";
        crawlerAgentPattern = "msnbot|google|teoma|bingbot|yandexbot|yahoo|TargetAuClient";
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        Mockito.when(configuration.getString(BV_SDK_ENABLED)).thenReturn("true");
    }

    @Test
    public void testGetUserReviewsContentWithDefaultConfig() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();

        setupDataForBotTests();

        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsContentWithDefaultConfigAndBotUser() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        bvParamReq.setUserAgent(validUserAgent);

        setupDataForBotTests();
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        Assert.assertTrue(userReviewResponse.getStatus());
        Assert.assertEquals("Content", userReviewResponse.getContent());

        Assert.assertNull(userReviewResponse.getValidationErrorMessage());
    }

    @Test
    public void testGetUserReviewsContentWithDefaultConfigAndNullUserAgent() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        bvParamReq.setUserAgent(null);

        setupDataForBotTests();
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsContentWithDefaultConfigAndInvalidUserAgent() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        bvParamReq.setUserAgent("Mozilla/5.0 (compatible; Rougebot/2.1; +http://www.rouge.com/bot.html)");

        setupDataForBotTests();
        Mockito.when(bvDefaultValidator.validate(Mockito.any(BVConfiguration.class), Mockito.any(BVParameters.class)))
                .thenReturn("");
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn("Content");
        Mockito.when(bvUIContent.getAggregateRating(Mockito.any(BVParameters.class))).thenReturn("Aggregate rating");
        Mockito.when(bvUIContent.getReviews(Mockito.any(BVParameters.class))).thenReturn("Reviews");

        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsContentWithCustomConfig() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final BVParameters bvParam = new BVParameters();

        bvParam.setUserAgent(validUserAgent);
        bvParam.setUserAgent(baseURI);
        bvParam.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        Mockito.verify(bvUIContent, Mockito.times(0)).getAggregateRating(Mockito.any(BVParameters.class));
        Mockito.verify(bvUIContent, Mockito.times(0)).getContent(Mockito.any(BVParameters.class));
        Mockito.verify(bvUIContent, Mockito.times(0)).getReviews(Mockito.any(BVParameters.class));
        Assert.assertNull(userReviewResponse.getValidationErrorMessage());

    }

    @Test
    public void testGetUserReviewsWithSDKFalse() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setUserAgent(baseURI);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(configuration.getString(BV_SDK_ENABLED)).thenReturn("false");
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        Mockito.verify(bvUIContent, Mockito.times(0)).getAggregateRating(Mockito.any(BVParameters.class));
        Mockito.verify(bvUIContent, Mockito.times(0)).getContent(Mockito.any(BVParameters.class));
        Mockito.verify(bvUIContent, Mockito.times(0)).getReviews(Mockito.any(BVParameters.class));
        Assert.assertFalse(userReviewResponse.getStatus());

    }

    @Test
    public void testGetUserReviewsReturnsExecutionTimeoutBotWarning() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "EXECUTION_TIMEOUT_BOT is less than the minimum value allowed. Minimum value of";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsExecutionTimeoutWarning() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "EXECUTION_TIMEOUT is set to 0 ms; JavaScript-only Display";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsExecutionTimeoutError() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "Execution timed out, exceeded";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsExecutionTimeoutBotError() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "Execution timed out for search bot, exceeded";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR0000() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "There was an error please verify the exception stack trace for further info.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR0002() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "There was an error retrieving the property or the property is not set properly.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR0003() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "The getAggregateRating method is only supported when schema.org is configured for structured markup.  Please submit a support ticket at spark.bazaarvoice.com to have your Bazaarvoice configuration updated to use schema.org for structured markup.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR0004() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "There was an error loading bvconfig properties please check the exception.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR0005() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "There was an error loading bvclient properties please check the exception.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR0006() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "Cannot add empty or null values to configuration.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR0008() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "Property for Bazaarvoice server configuration is not configured properly.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR0009() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "Property for Bazaarvoice client configuration is not configured properly.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR00012() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "The resource to the URL or file is currently unavailable.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR00013() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "BV SEO Content for this product did not include verbose reviews.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR00019() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "Unable to reach the host please check your network.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR00020() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "CLOUD_KEY is not configured in BVConfiguration.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsReturnsERR00024() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String errorContString = "Charset is not configured properly. BV-SEO-SDK will load default charset and continue.;";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn(errorContString);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsWithBlankContent() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn("");
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsWithBlankReviews() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getReviews(Mockito.any(BVParameters.class))).thenReturn("");
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsWithNullRating() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(pageURI);
        bvParamReq.setProduct(testProduct);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvUIContent.getAggregateRating(Mockito.any(BVParameters.class))).thenReturn(null);
        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertFalse(userReviewResponse.getStatus());
    }

    @Test
    public void testGetUserReviewsRequestParamsSettingToBVParams() {
        final TargetUserReviewRequest bvParamReq = new TargetUserReviewRequest();
        final String mockPageURI = "http://mocktestdomail/p/girls-chalk-board-tulle-pinafore/P56090499";
        final String mockBaseURL = "http://mock/base/url";
        bvParamReq.setUserAgent(validUserAgent);
        bvParamReq.setPageURI(mockPageURI);
        bvParamReq.setBaseURI(mockBaseURL);
        targetUserReviewServiceImpl.setRootFolder(rootFolder);
        targetUserReviewServiceImpl.setCloudKey(cloudKey);
        targetUserReviewServiceImpl.setCrawlerAgentPattern(crawlerAgentPattern);
        Mockito.when(bvDefaultValidator.validate(Mockito.any(BVConfiguration.class), Mockito.any(BVParameters.class)))
                .thenReturn("");
        Mockito.when(bvUIContent.getContent(Mockito.any(BVParameters.class))).thenReturn("Content");


        final TargetUserReviewResponse userReviewResponse = targetUserReviewServiceImpl
                .getUserReviewsContent(bvParamReq);
        final ArgumentCaptor<BVParameters> paramsCaptor = ArgumentCaptor.forClass(BVParameters.class);
        Mockito.verify(bvUIContent).getContent(paramsCaptor.capture());
        final BVParameters capturedParams = paramsCaptor.getValue();
        Assert.assertNotNull(capturedParams);
        Assert.assertEquals(validUserAgent, capturedParams.getUserAgent());
        Assert.assertEquals(mockPageURI, capturedParams.getPageURI());
        Assert.assertTrue(userReviewResponse.getStatus());
    }


}

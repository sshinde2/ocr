/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtwebcore;



/**
 * Simple test class to demonstrate how to include utility classes to your webmodule.
 */
public final class TgtwebcoreWebHelper {
    private TgtwebcoreWebHelper() {
        // prevent construction
    }

    public static final String getTestOutput() {
        return "testoutput";
    }
}

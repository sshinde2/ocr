/**
 * 
 */
package au.com.target.tgtwebmethods.ca;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;


/**
 * @author bhuang3
 * 
 */
public interface TargetCaShippingService {

    /**
     * Submit delivery order status for complete update.
     *
     * @param orderModel
     *            the order model
     * @return return true if get the no error response from service, else return the false
     */
    boolean submitDeliveryOrderStatusForCompleteUpdate(OrderModel orderModel);

    /**
     * Submit delivery consignmentModel status for complete update.
     * 
     * @param consignmentModel
     * @return true if get the no error response from service, else return the false
     */
    boolean submitDeliveryConsignmentStatus(ConsignmentModel consignmentModel);

    /**
     * Submit cnc order status for ready for pickup update.
     *
     * @param orderModel
     *            the order model
     * @return return true if get the no error response from service, else return the false
     */
    boolean submitCncOrderStatusForReadyForPickupUpdate(OrderModel orderModel);

    /**
     * Submit cnc order status for pickedup update.
     *
     * @param orderModel
     *            the order model
     * @return return true if get the no error response from service, else return the false
     */
    boolean submitCncOrderStatusForPickedupUpdate(OrderModel orderModel);

}

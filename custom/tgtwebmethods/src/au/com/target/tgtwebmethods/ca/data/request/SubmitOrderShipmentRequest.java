/**
 * 
 */
package au.com.target.tgtwebmethods.ca.data.request;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import au.com.target.tgtwebmethods.ca.data.types.ShipmentOrder;


/**
 * @author bhuang3
 * 
 */
@XmlRootElement(name = "submitOrderShipmentRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class SubmitOrderShipmentRequest {

    @XmlElement
    private String clientId;

    @XmlElement
    private String salesChannel;

    @XmlElementWrapper(name = "shipmentOrders")
    @XmlElement(name = "shipmentOrder")
    private List<ShipmentOrder> shipmentList;

    /**
     * @return the clientId
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * @param clientId
     *            the clientId to set
     */
    public void setClientId(final String clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the salesChannel
     */
    public String getSalesChannel() {
        return salesChannel;
    }

    /**
     * @param salesChannel
     *            the salesChannel to set
     */
    public void setSalesChannel(final String salesChannel) {
        this.salesChannel = salesChannel;
    }

    /**
     * @return the shipmentList
     */
    public List<ShipmentOrder> getShipmentList() {
        return shipmentList;
    }

    /**
     * @param shipmentList
     *            the shipmentList to set
     */
    public void setShipmentList(final List<ShipmentOrder> shipmentList) {
        this.shipmentList = shipmentList;
    }


}

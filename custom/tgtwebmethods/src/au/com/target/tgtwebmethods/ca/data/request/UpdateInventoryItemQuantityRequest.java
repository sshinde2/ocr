/**
 * 
 */
package au.com.target.tgtwebmethods.ca.data.request;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import au.com.target.tgtwebmethods.ca.data.types.UpdateQuantityProduct;



/**
 * @author bhuang3
 * 
 */

@XmlRootElement(name = "updateStockRequest")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateInventoryItemQuantityRequest {

    @XmlElement
    private String clientid;

    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    private List<UpdateQuantityProduct> productList;

    /**
     * @return the clientid
     */

    public String getClientid() {
        return clientid;
    }

    /**
     * @param clientid
     *            the clientid to set
     */
    public void setClientid(final String clientid) {
        this.clientid = clientid;
    }

    /**
     * @return the productList
     */
    public List<UpdateQuantityProduct> getProductList() {
        return productList;
    }

    /**
     * @param productList
     *            the productList to set
     */
    public void setProductList(final List<UpdateQuantityProduct> productList) {
        this.productList = productList;
    }


}

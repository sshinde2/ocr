/**
 * 
 */
package au.com.target.tgtwebmethods.ca.data.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import au.com.target.tgtwebmethods.ca.data.types.UpdateQuantityProduct;


/**
 * @author bhuang3
 * 
 */
@XmlRootElement(name = "updateStockResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpdateInventoryItemQuantityResponse {
    //
    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    private List<UpdateQuantityProduct> productList;

    /**
     * @return the productList
     */
    public List<UpdateQuantityProduct> getProductList() {
        return productList;
    }

    /**
     * @param productList
     *            the productList to set
     */
    public void setProductList(final List<UpdateQuantityProduct> productList) {
        this.productList = productList;
    }



}

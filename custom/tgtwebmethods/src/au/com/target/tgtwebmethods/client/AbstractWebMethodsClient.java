/**
 * 
 */
package au.com.target.tgtwebmethods.client;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtfulfilment.integration.dto.BaseResponseDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtutility.http.factory.HttpClientFactory;
import au.com.target.tgtwebmethods.client.util.IntegrationErrorCodeProcessor;
import au.com.target.tgtwebmethods.rest.ErrorDTO;
import au.com.target.tgtwebmethods.rest.JSONResponse;
import au.com.target.tgtwebmethods.util.WMUtil;


/**
 * @author rmcalave
 * 
 */
public abstract class AbstractWebMethodsClient {
    protected static final String HEADER_CONTENT_TYPE = "Content-Type";
    protected static final String HEADER_CONTENT_TYPE_FORM_URLENCODED = "application/x-www-form-urlencoded";
    protected static final String HEADER_CONTENT_TYPE_JSON = "application/json";
    private static final String LOGGING_PREFIX = "ENDPOINT: ";

    private static final Logger LOG = Logger.getLogger(AbstractWebMethodsClient.class);

    private String webmethodsBaseUrl;
    private String webmethodsSecureBaseUrl;
    private HttpClientFactory httpClientFactory;

    /**
     * Send the request payload to a REST service end point that accepts JSON.
     * 
     * @param request
     * @param prototypeResponse
     * @return JSONResponse
     */
    protected <ResponseType extends JSONResponse> ResponseType send(final Object request,
            final BaseResponseDTO prototypeResponse, final Class<ResponseType> responseType) {

        ResponseType jsonResponse = null;
        if (BooleanUtils.isFalse(isPresendSuccess(request, prototypeResponse))) {
            return jsonResponse;
        }

        try {
            jsonResponse = getRestTemplate().postForObject(
                    getWebmethodsSecureBaseUrl() + getServiceEndpoint(), request,
                    responseType);
            if (jsonResponse != null) {
                LOG.info(LOGGING_PREFIX + "Received response=" + WMUtil.convertToJSONString(jsonResponse));
                if (!jsonResponse.isSuccess()) {
                    final List<ErrorDTO> errors = jsonResponse.getErrors();
                    if (CollectionUtils.isNotEmpty(errors)) {
                        IntegrationErrorCodeProcessor.getFailedResponse(
                                prototypeResponse, LOGGING_PREFIX,
                                jsonResponse);
                        return jsonResponse;
                    }
                    logError(prototypeResponse, IntegrationError.REMOTE_SERVICE_ERROR);
                }
            }
            else {
                logError(prototypeResponse, IntegrationError.COMMUNICATION_ERROR);
            }
        }
        catch (final RestClientException rce) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to get response from REST web service", rce);
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    IntegrationError.CONNECTION_ERROR, "RestClientException", rce.getMessage());
        }
        catch (final HttpMessageNotReadableException nre) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to unmarshall response from REST web service", nre);
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    IntegrationError.COMMUNICATION_ERROR, "HttpMessageNotReadableException",
                    nre.getMessage());
        }
        catch (final Exception e) {
            LOG.error(LOGGING_PREFIX + "FAILED with unexpected exception", e);
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    IntegrationError.COMMUNICATION_ERROR, e.getClass().getSimpleName(), e.getMessage());
        }
        return jsonResponse;
    }

    protected <ResponseType extends JSONResponse> ResponseType sendStream(final Object request,
            final BaseResponseDTO prototypeResponse, final Class<ResponseType> responseType) {

        ResponseType jsonResponse = null;
        if (BooleanUtils.isFalse(isPresendSuccess(request, prototypeResponse))) {
            return jsonResponse;
        }

        try {
            jsonResponse = getRestTemplate().postForObject(
                    getWebmethodsSecureBaseUrl() + getServiceEndpoint(), request,
                    responseType);
            if (jsonResponse != null) {
                LOG.info(LOGGING_PREFIX + "Received response=" + WMUtil.convertToJSONString(jsonResponse));
                if (!jsonResponse.isSuccess()) {
                    final List<ErrorDTO> errors = jsonResponse.getErrors();
                    if (CollectionUtils.isNotEmpty(errors)) {
                        IntegrationErrorCodeProcessor.getFailedResponse(
                                prototypeResponse, LOGGING_PREFIX,
                                jsonResponse);
                        return jsonResponse;
                    }
                    logError(prototypeResponse, IntegrationError.REMOTE_SERVICE_ERROR);
                }
            }
            else {
                logError(prototypeResponse, IntegrationError.COMMUNICATION_ERROR);
            }
        }
        catch (final RestClientException rce) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to get response from REST web service", rce);
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    IntegrationError.CONNECTION_ERROR, "RestClientException", rce.getMessage());
        }
        catch (final HttpMessageNotReadableException nre) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to unmarshall response from REST web service", nre);
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    IntegrationError.COMMUNICATION_ERROR, "HttpMessageNotReadableException",
                    nre.getMessage());
        }
        catch (final Exception e) {
            LOG.error(LOGGING_PREFIX + "FAILED with unexpected exception", e);
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    IntegrationError.COMMUNICATION_ERROR, e.getClass().getSimpleName(), e.getMessage());
        }
        return jsonResponse;
    }

    /**
     * @param prototypeResponse
     */
    private Boolean isPresendSuccess(final Object request, final BaseResponseDTO prototypeResponse) {
        Assert.notNull(request, "Request DTO cannot be null");
        Assert.notNull(prototypeResponse, "Response object cannot be null");

        if (getRestTemplate() == null) {
            LOG.error(LOGGING_PREFIX + "No REST template defined");
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    IntegrationError.CONNECTION_ERROR, "NOT_SPECIFIED",
                    "No REST template defined");
            return Boolean.FALSE;
        }

        if (StringUtils.isEmpty(getServiceEndpoint())) {
            LOG.error(LOGGING_PREFIX + "No Service endpoint defined");
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    IntegrationError.CONNECTION_ERROR, "NOT_SPECIFIED",
                    "No Service endpoint defined");
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    /**
     * @param prototypeResponse
     * @param errorType
     */
    private void logError(final BaseResponseDTO prototypeResponse, final IntegrationError errorType) {
        if (errorType.compareTo(IntegrationError.REMOTE_SERVICE_ERROR) == 0) {
            LOG.error(LOGGING_PREFIX + "FAILED without an ERROR");
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    errorType, "NOT_SPECIFIED",
                    "Request failed");
        }
        else if (errorType.compareTo(IntegrationError.COMMUNICATION_ERROR) == 0) {
            IntegrationErrorCodeProcessor.getFailedResponse(prototypeResponse,
                    errorType, "JSONResponseNull",
                    "The JSONResponse is Null");
        }

    }

    /**
     * @return the serviceEndpoint
     */
    protected abstract String getServiceEndpoint();

    /**
     * @return RestTemplate
     */
    protected abstract RestTemplate getRestTemplate();

    /**
     * @return the webmethodsBaseUrl
     */
    protected String getWebmethodsBaseUrl() {
        return webmethodsBaseUrl;
    }

    /**
     * @param webmethodsBaseUrl
     *            the webmethodsBaseUrl to set
     */
    @Required
    public void setWebmethodsBaseUrl(final String webmethodsBaseUrl) {
        this.webmethodsBaseUrl = webmethodsBaseUrl;
    }

    /**
     * @return the webmethodsSecureBaseUrl
     */
    protected String getWebmethodsSecureBaseUrl() {
        return webmethodsSecureBaseUrl;
    }

    /**
     * @param webmethodsSecureBaseUrl
     *            the webmethodsSecureBaseUrl to set
     */
    @Required
    public void setWebmethodsSecureBaseUrl(final String webmethodsSecureBaseUrl) {
        this.webmethodsSecureBaseUrl = webmethodsSecureBaseUrl;
    }

    /**
     * @param httpClientFactory
     *            the httpClientFactory to set
     */
    @Required
    public void setHttpClientFactory(final HttpClientFactory httpClientFactory) {
        this.httpClientFactory = httpClientFactory;
    }

    /**
     * @return the httpClient
     */
    protected HttpClient getHttpClient() {
        return httpClientFactory.createHttpClient();
    }
}

/**
 * 
 */
package au.com.target.tgtwebmethods.client.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtfulfilment.client.PollConsignmentWarehouseRestClient;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.client.util.IntegrationErrorCodeProcessor;
import au.com.target.tgtwebmethods.rest.JSONResponse;
import au.com.target.tgtwebmethods.util.WMUtil;


/**
 * @author gsing236
 *
 */
public class WMPollConsignmentWarehouseRestClient extends AbstractWebMethodsClient
        implements PollConsignmentWarehouseRestClient {

    private static final Logger LOG = Logger.getLogger(WMPollConsignmentWarehouseRestClient.class);
    private static final String LOGGING_PREFIX = "WMPollonsignmentWarehouseRest client: ";

    private RestTemplate restTemplate;

    private String serviceEndpoint;

    @Override
    public PollWarehouseResponse pollConsignments(final PollWarehouseRequest request) {

        LOG.info(LOGGING_PREFIX + "Sending request=" + WMUtil.convertToJSONString(request));

        final PollWarehouseResponse response = new PollWarehouseResponse();
        final JSONResponse jsonResponse = sendStream(request, response, JSONResponse.class);
        if (jsonResponse != null) {
            if (jsonResponse.isSuccess()) {
                return (PollWarehouseResponse)IntegrationErrorCodeProcessor.getSuccessResponse(response);
            }
            else {
                return (PollWarehouseResponse)IntegrationErrorCodeProcessor.getFailedResponse(response, LOGGING_PREFIX,
                        jsonResponse);
            }
        }

        return response;
    }

    @Override
    protected String getServiceEndpoint() {
        return serviceEndpoint;
    }

    @Override
    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * @param restTemplate
     *            the restTemplate to set
     */
    @Required
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * @param serviceEndpoint
     *            the serviceEndpoint to set
     */
    @Required
    public void setServiceEndpoint(final String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

}

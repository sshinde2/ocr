/**
 * 
 */
package au.com.target.tgtwebmethods.client.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtfulfilment.client.SendToWarehouseRestClient;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.client.util.IntegrationErrorCodeProcessor;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;
import au.com.target.tgtwebmethods.logger.WMSendToWarehouseLogger;
import au.com.target.tgtwebmethods.rest.JSONResponse;
import au.com.target.tgtwebmethods.util.WMUtil;


/**
 * Web methods implementation of SendToWarehouseRestClient
 * 
 * @author jjayawa1
 *
 */
public class WMSendToWarehouseRestClient extends AbstractWebMethodsClient implements SendToWarehouseRestClient {
    private static final Logger LOG = Logger.getLogger(WMSendToWarehouseRestClient.class);
    private static final String LOGGING_PREFIX = "WMSendToWarehouse client: ";

    private RestTemplate restTemplate;

    private String serviceEndpoint;

    private Map<String, List<String>> digitalErrorCodesMapping;

    private WMSendToWarehouseLogger wmSendToWarehouseLogger;

    /**
     * {@inheritDoc}
     */
    @Override
    public SendToWarehouseResponse sendConsignment(final SendToWarehouseRequest sendToWarehouseRequest) {
        LOG.info(LOGGING_PREFIX + "Sending request=" + WMUtil.convertToJSONString(sendToWarehouseRequest));

        final SendToWarehouseResponse response = new SendToWarehouseResponse();
        final JSONResponse jsonResponse = sendStream(sendToWarehouseRequest, response, JSONResponse.class);
        if (jsonResponse != null) {
            if (jsonResponse.isSuccess()) {
                LOG.info(LOGGING_PREFIX + "SUCCEEDED ");
                return (SendToWarehouseResponse)IntegrationErrorCodeProcessor.getSuccessResponse(response);
            }
            else {
                return processErroneousResponse(sendToWarehouseRequest, response);
            }
        }
        return response;
    }

    /**
     * Processes erroneous response code against digital error codes, if allowed then response is success else log error
     * and response is failure
     * 
     * @param sendToWarehouseRequest
     * @param response
     */
    protected SendToWarehouseResponse processErroneousResponse(final SendToWarehouseRequest sendToWarehouseRequest,
            final SendToWarehouseResponse response) {
        final String integrationErrorCode = IntegrationErrorCodeProcessor.getIntegrationErrorCode(response);
        final String errorValue = response.getErrorValue();
        if (doesResponseErrorCodeMatchErrorType(integrationErrorCode,
                TgtwebmethodsConstants.DigitalErrorType.ACCEPT)) {
            wmSendToWarehouseLogger.logMessage(LOG, sendToWarehouseRequest, integrationErrorCode, errorValue,
                    false, false);
            return (SendToWarehouseResponse)IntegrationErrorCodeProcessor.getSuccessResponse(response);
        }
        else if (doesResponseErrorCodeMatchErrorType(integrationErrorCode,
                TgtwebmethodsConstants.DigitalErrorType.INFORM_VENDOR)) {
            wmSendToWarehouseLogger.logMessage(LOG, sendToWarehouseRequest, integrationErrorCode, errorValue,
                    true, true);
        }
        else {
            // adding consignment details in all kinds of errors
            wmSendToWarehouseLogger.logMessage(LOG, sendToWarehouseRequest, integrationErrorCode, errorValue,
                    true, true);
        }

        return response;
    }

    private boolean doesResponseErrorCodeMatchErrorType(final String errorCode, final String errorType) {
        final List<String> digitalErrorCodes = digitalErrorCodesMapping.get(errorType);
        return digitalErrorCodes != null && digitalErrorCodes.contains(errorCode);
    }

    /**
     * @param restTemplate
     *            the restTemplate to set
     */
    @Required
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * @param serviceEndpoint
     *            the serviceEndpoint to set
     */
    @Required
    public void setServiceEndpoint(final String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return serviceEndpoint;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * @param digitalErrorCodesMapping
     *            the digitalErrorCodesMapping to set
     */
    @Required
    public void setDigitalErrorCodesMapping(final Map<String, List<String>> digitalErrorCodesMapping) {
        this.digitalErrorCodesMapping = digitalErrorCodesMapping;
    }

    /**
     * @param wmSendToWarehouseLogger
     *            the wmSendToWarehouseLogger to set
     */
    @Required
    public void setWmSendToWarehouseLogger(final WMSendToWarehouseLogger wmSendToWarehouseLogger) {
        this.wmSendToWarehouseLogger = wmSendToWarehouseLogger;
    }

}

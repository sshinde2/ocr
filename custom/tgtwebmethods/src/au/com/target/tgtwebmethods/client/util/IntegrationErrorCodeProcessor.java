/**
 * 
 */
package au.com.target.tgtwebmethods.client.util;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtfulfilment.integration.dto.BaseResponseDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtwebmethods.rest.ErrorDTO;
import au.com.target.tgtwebmethods.rest.JSONResponse;


/**
 * Common integration error code processing.
 * 
 * @author rsamuel3
 *
 */
public final class IntegrationErrorCodeProcessor {

    private static final Logger LOG = Logger.getLogger(IntegrationErrorCodeProcessor.class);

    private IntegrationErrorCodeProcessor() {
        // Empty constructor.
    }

    /**
     * Get failed response.
     * 
     * @param response
     * @param errorType
     * @param errorCode
     * @param message
     * @return BaseResponseDTO
     */
    public static BaseResponseDTO getFailedResponse(final BaseResponseDTO response, final IntegrationError errorType,
            final String errorCode,
            final String message) {
        response.setErrorType(errorType);
        response.setErrorCode(errorCode);
        response.setErrorValue(message);
        response.setSuccess(false);
        return response;
    }

    /**
     * setup the baseResponse for the different error codes received
     * 
     * @param response
     * @param loggingPrefix
     * @param jsonResponse
     * @return populated baseResponseDTO
     */
    public static BaseResponseDTO getFailedResponse(final BaseResponseDTO response, final String loggingPrefix,
            final JSONResponse jsonResponse) {
        final List<ErrorDTO> errors = jsonResponse.getErrors();
        final int responseCode = jsonResponse.getResponseCode();
        switch (responseCode) {
            case -3:
                response.setErrorType(IntegrationError.CONNECTION_ERROR);
                break;
            case -6:
                response.setErrorType(IntegrationError.REMOTE_SERVICE_ERROR);
                break;
            default:
                response.setErrorType(IntegrationError.MIDDLEWARE_ERROR);
        }
        final StringBuilder errorMessages = new StringBuilder();
        final StringBuilder errorCodes = new StringBuilder();
        if (CollectionUtils.isNotEmpty(errors)) {
            for (final ErrorDTO error : errors) {
                final String code = error.getCode();
                errorCodes.append(code).append(",");
                final String value = error.getValue();
                LOG.error(loggingPrefix + "FAILED with ERROR_CODE=" + code
                        + ", ERROR_VALUE=" + value);
                errorMessages.append(code).append(":").append(value).append(",");
            }
        }
        response.setErrorCode(responseCode != 0 ? Integer.toString(responseCode)
                : errorCodes
                        .toString());
        response.setErrorValue(errorMessages.toString());
        response.setSuccess(false);
        return response;
    }

    public static BaseResponseDTO getSuccessResponse(final BaseResponseDTO response) {
        response.setSuccess(true);
        return response;
    }

    public static String getIntegrationErrorCode(final BaseResponseDTO response) {
        if (response.getErrorValue() != null) {
            final String[] errors = response.getErrorValue().split(",");
            if (errors.length > 0) {
                final String integrationErrorCode = errors[0].split(":")[0];
                return integrationErrorCode;
            }
        }
        return StringUtils.EMPTY;
    }
}

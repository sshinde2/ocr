/**
 * 
 */
package au.com.target.tgtwebmethods.community.client.impl;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.community.client.TargetCommunityDonationClient;
import au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @author rmcalave
 * 
 */
public class TargetCommunityDonationClientImpl extends AbstractWebMethodsClient implements
        TargetCommunityDonationClient {
    protected static final Logger LOG = Logger.getLogger(TargetCommunityDonationClientImpl.class);
    private static final String ERROR_CONNECTERROR = "Unable to get response from rest web service";
    private static final String ERROR_MESSAGE_NOT_READABLE = "The Response unmarshall failure ";
    private static final String ERROR_MESSAGE_ILLEGAL_RESPONSE = "The Response is not boolean value ";
    private String communityDonationsUrl;

    private RestTemplate restTemplate;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.community.client.TargetCommunityDonationClient#sendDonationRequestData(au.com.target.tgtcore.community.dto.TargetCommunityDonationRequestDto)
     */
    @Override
    public boolean sendDonationRequestData(final TargetCommunityDonationRequestDto request) {
        final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
        final String requestPayload = gson.toJson(request);
        try {
            final ResponseEntity<String> response = restTemplate.exchange(getWebmethodsBaseUrl()
                    + communityDonationsUrl,
                    HttpMethod.POST, new HttpEntity(requestPayload), String.class);
            if (BooleanUtils.toBoolean(response.getBody(), "true", "false")) {
                return true;
            }
            return true;
        }
        catch (final IllegalArgumentException ie) {
            LOG.error(ERROR_MESSAGE_ILLEGAL_RESPONSE, ie);
            return false;
        }
        catch (final RestClientException rce) {
            LOG.error(ERROR_CONNECTERROR, rce);
            return false;
        }
        catch (final HttpMessageNotReadableException nre) {
            LOG.error(ERROR_MESSAGE_NOT_READABLE, nre);
            return false;
        }

    }

    /**
     * @param communityDonationsUrl
     *            the communityDonationsUrl to set
     */
    @Required
    public void setCommunityDonationsUrl(final String communityDonationsUrl) {
        this.communityDonationsUrl = communityDonationsUrl;
    }

    /**
     * @param restTemplate
     *            the restTemplate to set
     */
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return communityDonationsUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }
}

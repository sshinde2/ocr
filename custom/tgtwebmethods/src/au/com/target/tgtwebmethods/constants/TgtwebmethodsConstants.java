/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtwebmethods.constants;

/**
 * Global class for all Tgtwebmethods constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtwebmethodsConstants extends GeneratedTgtwebmethodsConstants {
    public static final String EXTENSIONNAME = "tgtwebmethods";

    private TgtwebmethodsConstants() {
        //empty to avoid instantiating this constant class
    }

    public interface ExactTargetErrorCodesAndMessages {

        String ERROR_REQUEST_MOBILENO_NULLOREMPTY = "The Mobile Number cannot be null or empty";
        String ERROR_REQUEST_MESSAGE_NULLOREMPTY = "The Sms text cannot be null or empty";
        String ERROR_REQUEST_TIMEZONE_NULLOREMPTY = "The time zone cannot be null or empty";
        String ERROR_MESSAGE_NOT_READABLE = "The response is null";

    }

    public interface CommonErrorCodes {
        Integer INVALID_ERRORCODE = Integer.valueOf(-1);
        Integer UNAVAILABLE_ERRORCODE = Integer.valueOf(-2);
        Integer OTHERS_ERRORCODE = Integer.valueOf(-3);
    }

    public interface CustomerSubsriptionErrorCodesAndMessages {
        String ERROR_CUSTOMER_SUBSCRIPTION_SERVICE_NOT_INITIALISED = "customerSubscriptionServicePort was not initialised at system startup, customer Subscription is currently unavailable.";
        String ERROR_REQUEST_EMAILID = "The email id  cannot be null or empty";
    }

    public interface PartnerFulFillmentConstants {

        String TYPE_PICKUP = "Pickup";

        String STATUS_COMPLETE = "Complete";
        String STATUS_READYFORPICKUP = "ReadyForPickup";

        String SHIPMENT_FULL = "Full";
        String SHIPMENT_PARTIAL = "Partial";
    }

    public interface DigitalErrorType {
        public static final String ACCEPT = "ACCEPT";
        public static final String INFORM_VENDOR = "INFORM_VENDOR";
    }

    // implement here constants used by this extension
}

/**
 * 
 */
package au.com.target.tgtwebmethods.customersubscription.factory;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.ws.WebServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.customermgmt.customersingleview.v1.customersingleviewservice.TGTCECustomerMgmtWSProviderV1CustomerSingleViewService;


/**
 * @author mjanarth
 * 
 */
public class CustomerSubscriptionServiceProviderFactory {

    private static final Logger LOG = Logger.getLogger(CustomerSubscriptionServiceProviderFactory.class);

    private String webmethodsBaseUrl;
    private String customerSubscriptionServiceWsdlUrl;


    /**
     * 
     * @return TGTCECustomerMgmtWSProviderV1CustomerSingleViewService
     */
    public TGTCECustomerMgmtWSProviderV1CustomerSingleViewService getInstance() {
        TGTCECustomerMgmtWSProviderV1CustomerSingleViewService customerSubscriptionServiceProvider = null;

        try {
            final URL url = new URL(webmethodsBaseUrl + customerSubscriptionServiceWsdlUrl);
            customerSubscriptionServiceProvider = new TGTCECustomerMgmtWSProviderV1CustomerSingleViewService(
                    url);
        }
        catch (final MalformedURLException ex) {
            LOG.error("Malformed URL for  Customer Subscription  Service WSDL: ", ex);
        }
        catch (final WebServiceException ex) {
            LOG.error("Web Service Exception while creating the customerSubscriptionServiceProvider: ", ex);
        }

        return customerSubscriptionServiceProvider;
    }

    /**
     * @param webmethodsBaseUrl
     *            the webmethodsBaseUrl to set
     */
    @Required
    public void setWebmethodsBaseUrl(final String webmethodsBaseUrl) {
        this.webmethodsBaseUrl = webmethodsBaseUrl;
    }

    /**
     * @param customerSubscriptionServiceWsdlUrl
     *            the customerSubscriptionServiceWsdlUrl to set
     */
    @Required
    public void setCustomerSubscriptionServiceWsdlUrl(final String customerSubscriptionServiceWsdlUrl) {
        this.customerSubscriptionServiceWsdlUrl = customerSubscriptionServiceWsdlUrl;
    }



}

/**
 * 
 */
package au.com.target.tgtwebmethods.exacttarget.client.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.sms.client.TargetSendSmsClient;
import au.com.target.tgtcore.sms.dto.TargetSendSmsResponseDto;
import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;
import au.com.target.tgtwebmethods.exacttarget.client.util.TargetSendSmsErrorCodeProcessor;
import au.com.target.tgtwebmethods.exacttarget.data.request.ExactTargetSendSmsRequest;
import au.com.target.tgtwebmethods.exacttarget.data.response.ExactTargetSendSmsResponse;


/**
 * @author mjanarth
 * 
 */
public class TargetSendSmsCncNotificationClientImpl extends AbstractWebMethodsClient implements
        TargetSendSmsClient {


    private RestTemplate exactTargetWebmethodRestTemplate;
    private String exactTargetSendSmsUrl;

    private TargetSendSmsErrorCodeProcessor targetSendSmsErrorCodeProcessor;


    /* (non-Javadoc)
     * @see au.com.target.tgtcore.exacttarget.client.TargetSendSmsCncNotificationClient#sendSMSNotification(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public TargetSendSmsResponseDto sendSmsNotification(final String mobileNumber, final String message,
            final String timeZone) {

        ExactTargetSendSmsRequest exactTargetrequest = null;
        ExactTargetSendSmsResponse exactTargetResponse = null;
        TargetSendSmsResponseDto responseDto = new TargetSendSmsResponseDto();

        if (StringUtils.isEmpty(message)) {
            responseDto.setResponseType(TargetSendSmsResponseType.INVALID);
            responseDto.setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
            responseDto
                    .setResponseMessage(TgtwebmethodsConstants.
                    ExactTargetErrorCodesAndMessages.ERROR_REQUEST_MESSAGE_NULLOREMPTY);
            return responseDto;
        }

        if (StringUtils.isEmpty(mobileNumber)) {
            responseDto.setResponseType(TargetSendSmsResponseType.INVALID);
            responseDto.setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
            responseDto
                    .setResponseMessage(TgtwebmethodsConstants.
                    ExactTargetErrorCodesAndMessages.ERROR_REQUEST_MOBILENO_NULLOREMPTY);
            return responseDto;
        }
        if (StringUtils.isEmpty(timeZone)) {
            responseDto.setResponseType(TargetSendSmsResponseType.INVALID);
            responseDto.setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.INVALID_ERRORCODE);
            responseDto
                    .setResponseMessage(TgtwebmethodsConstants.
                    ExactTargetErrorCodesAndMessages.ERROR_REQUEST_TIMEZONE_NULLOREMPTY);
            return responseDto;
        }
        exactTargetrequest = createSendSmsRequest(mobileNumber, message, timeZone);
        ResponseEntity<ExactTargetSendSmsResponse> responseEntity = null;
        try {
            responseEntity = exactTargetWebmethodRestTemplate.exchange(getWebmethodsBaseUrl()
                    + exactTargetSendSmsUrl, HttpMethod.POST, new HttpEntity(
                    exactTargetrequest), ExactTargetSendSmsResponse.class);
            exactTargetResponse = responseEntity.getBody();
            responseDto = createSendSmsResponseDto(exactTargetResponse);
            return responseDto;
        }

        catch (final RestClientException rce) {
            responseDto.setResponseType(TargetSendSmsResponseType.UNAVAILABLE);
            responseDto.setResponseMessage(rce.getMessage());
            responseDto.setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.UNAVAILABLE_ERRORCODE);
            return responseDto;
        }
        catch (final HttpMessageNotReadableException ex) {
            responseDto.setResponseType(TargetSendSmsResponseType.OTHERS);
            responseDto.setResponseMessage(ex.getMessage());
            responseDto.setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.OTHERS_ERRORCODE);
            return responseDto;
        }

    }


    /**
     * @param exactTargetSendSmsUrl
     *            the exactTargetSendSmsUrl to set
     */
    @Required
    public void setExactTargetSendSmsUrl(final String exactTargetSendSmsUrl) {
        this.exactTargetSendSmsUrl = exactTargetSendSmsUrl;
    }


    /**
     * @param exactTargetWebmethodRestTemplate
     *            the exactTargetWebmethodRestTemplate to set
     */
    @Required
    public void setExactTargetWebmethodRestTemplate(final RestTemplate exactTargetWebmethodRestTemplate) {
        this.exactTargetWebmethodRestTemplate = exactTargetWebmethodRestTemplate;
    }


    /**
     * @param targetSendSmsErrorCodeProcessor
     *            the targetSendSmsErrorCodeProcessor to set
     */
    @Required
    public void setTargetSendSmsErrorCodeProcessor(final TargetSendSmsErrorCodeProcessor targetSendSmsErrorCodeProcessor) {
        this.targetSendSmsErrorCodeProcessor = targetSendSmsErrorCodeProcessor;
    }

    /**
     * Create Request
     * 
     * @param mobileNumber
     * @param message
     * @param timeZone
     * @return ExactTargetSendSmsRequest
     */
    private ExactTargetSendSmsRequest createSendSmsRequest(final String mobileNumber, final String message,
            final String timeZone) {

        final ExactTargetSendSmsRequest request = new ExactTargetSendSmsRequest();
        request.setMessage(message);
        request.setMobileNumber(mobileNumber);
        request.setTimeZone(timeZone);
        return request;

    }

    /**
     * Creates Response
     * 
     * @param exactTargetResponse
     * @return TargetSendSmsResponseDto
     */
    private TargetSendSmsResponseDto createSendSmsResponseDto(final ExactTargetSendSmsResponse exactTargetResponse) {
        final TargetSendSmsResponseDto targetResponse = new TargetSendSmsResponseDto();

        if (null == exactTargetResponse) {
            targetResponse.setResponseType(TargetSendSmsResponseType.OTHERS);
            targetResponse.setResponseCode(TgtwebmethodsConstants.CommonErrorCodes.OTHERS_ERRORCODE);
            targetResponse
                    .setResponseMessage(TgtwebmethodsConstants.
                    ExactTargetErrorCodesAndMessages.ERROR_MESSAGE_NOT_READABLE);
            return targetResponse;
        }

        targetResponse.setResponseType(targetSendSmsErrorCodeProcessor
                .getExactTargetResponseTypeForCode(exactTargetResponse.getResponseCode()));
        targetResponse.setResponseCode(Integer.valueOf(exactTargetResponse.getResponseCode()));
        targetResponse.setResponseMessage(exactTargetResponse.getResponseMessage());
        return targetResponse;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return exactTargetSendSmsUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return exactTargetWebmethodRestTemplate;
    }

}

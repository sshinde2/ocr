/**
 * 
 */
package au.com.target.tgtwebmethods.exacttarget.client.util;

import org.apache.commons.lang.ArrayUtils;

import au.com.target.tgtcore.sms.enums.TargetSendSmsResponseType;


/**
 * @author mjanarth
 * 
 */
public class TargetSendSmsErrorCodeProcessor {

    private static final String SUCCESS_CODE = "0";
    private static final String[] UNAVAILABLE_CODES = { "-2" };
    private static final String[] INVALID_CODES = { "-999", "-1", "-3" };


    /**
     * Get the response type for corresponding error code
     * 
     * @param responseCode
     * @return ExactTargetResponseType
     */
    public TargetSendSmsResponseType getExactTargetResponseTypeForCode(final String responseCode) {
        if (SUCCESS_CODE.equals(responseCode)) {
            return TargetSendSmsResponseType.SUCCESS;
        }

        if (ArrayUtils.contains(UNAVAILABLE_CODES, responseCode)) {
            return TargetSendSmsResponseType.UNAVAILABLE;
        }

        if (ArrayUtils.contains(INVALID_CODES, responseCode)) {
            return TargetSendSmsResponseType.INVALID;
        }
        return TargetSendSmsResponseType.OTHERS;
    }
}

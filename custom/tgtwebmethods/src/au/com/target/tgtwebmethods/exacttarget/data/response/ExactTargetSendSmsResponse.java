/**
 * 
 */
package au.com.target.tgtwebmethods.exacttarget.data.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mjanarth
 * 
 */

@XmlRootElement(name = "exactTargetSendSmsResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExactTargetSendSmsResponse {


    @XmlElement
    private String responseMessage;

    @XmlElement
    private String responseCode;


    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage
     *            the responseMessage to set
     */
    public void setResponseMessage(final String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode
     *            the responseCode to set
     */
    public void setResponseCode(final String responseCode) {
        this.responseCode = responseCode;
    }


}

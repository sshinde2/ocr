/**
 * 
 */
package au.com.target.tgtwebmethods.exception;

import au.com.target.tgtwebcore.exception.TargetIntegrationException;


/**
 * @author rmcalave
 * 
 */
public class TargetWebMethodsClientException extends TargetIntegrationException
{

    /**
     * @param message
     * @param cause
     */
    public TargetWebMethodsClientException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    /**
     * @param message
     */
    public TargetWebMethodsClientException(final String message)
    {
        super(message);
    }

    /**
     * @param cause
     */
    public TargetWebMethodsClientException(final Throwable cause)
    {
        super(cause);
    }

}

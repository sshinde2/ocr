/**
 * 
 */
package au.com.target.tgtwebmethods.flybuys.client.util;

import org.apache.commons.lang.ArrayUtils;

import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;


/**
 * @author rmcalave
 * 
 */
public class FlybuysErrorCodeProcessor {

    private static final String SUCCESS_CODE = "0";
    private static final String[] UNAVAILABLE_CODES = { "-2", "90411015", "90412004", };
    private static final String[] FLYBUYS_OTHER_ERROR_CODES = { "-3", "1", "2", "3", "5", "6", "8", "9", "10", "11",
            "12", "13", "14", "15", "16", "17", "100", "90412001", "90411016", "90412005", "90412006" };
    private static final String[] INVALID_CODES = { "-1", "4", "90412000" };

    public FlybuysResponseType getFlybuysResponseTypeForCode(final String responseCode) {
        if (SUCCESS_CODE.equals(responseCode)) {
            return FlybuysResponseType.SUCCESS;
        }

        if (ArrayUtils.contains(FLYBUYS_OTHER_ERROR_CODES, responseCode)) {
            return FlybuysResponseType.FLYBUYS_OTHER_ERROR;
        }

        if (ArrayUtils.contains(UNAVAILABLE_CODES, responseCode)) {
            return FlybuysResponseType.UNAVAILABLE;
        }

        if (ArrayUtils.contains(INVALID_CODES, responseCode)) {
            return FlybuysResponseType.INVALID;
        }

        return FlybuysResponseType.UNAVAILABLE;
    }
}

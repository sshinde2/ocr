/**
 * 
 */
package au.com.target.tgtwebmethods.flybuys.factory;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.ws.WebServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.services.services.flybuysrewards.v1.TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService;


/**
 * @author rmcalave
 * 
 */
public class FlybuysRewardsServiceProviderFactory {

    private static final Logger LOG = Logger.getLogger(FlybuysRewardsServiceProviderFactory.class);

    private String webmethodsBaseUrl;
    private String flybuysRewardsServiceWsdlUrl;

    /**
     * Create an instance of <code>TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService</code> with a
     * URL based on {@link #webmethodsBaseUrl} + {@link #flybuysRewardsServiceWsdlUrl}.
     * 
     * This is to ensure that if something goes wrong during creation (like the WSDL being unavailable) that the error
     * doesn't cause the application to fail to start.
     * 
     * @return a new instance of <code>TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService</code>
     */
    public TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService getInstance() {
        TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService flybuysRewardsServiceProvider = null;

        try {
            final URL url = new URL(webmethodsBaseUrl + flybuysRewardsServiceWsdlUrl);
            flybuysRewardsServiceProvider = new TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService(
                    url);
        }
        catch (final MalformedURLException ex) {
            LOG.error("Malformed URL for flybuys Rewards Service WSDL: ", ex);
        }
        catch (final WebServiceException ex) {
            LOG.error("Web Service Exception while creating the flybuysRewardsServiceProvider: ", ex);
        }

        return flybuysRewardsServiceProvider;
    }

    /**
     * @param webmethodsBaseUrl
     *            the webmethodsBaseUrl to set
     */
    @Required
    public void setWebmethodsBaseUrl(final String webmethodsBaseUrl) {
        this.webmethodsBaseUrl = webmethodsBaseUrl;
    }

    /**
     * @param flybuysRewardsServiceWsdlUrl
     *            the flybuysRewardsServiceWsdlUrl to set
     */
    @Required
    public void setFlybuysRewardsServiceWsdlUrl(final String flybuysRewardsServiceWsdlUrl) {
        this.flybuysRewardsServiceWsdlUrl = flybuysRewardsServiceWsdlUrl;
    }
}

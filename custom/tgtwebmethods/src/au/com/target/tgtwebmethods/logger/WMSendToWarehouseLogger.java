/**
 * 
 */
package au.com.target.tgtwebmethods.logger;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtwebmethods.util.WMUtil;


/**
 * WMSendToWarehouseLogger class to log message using SendToWarehouseRequest object.
 * 
 * @author pratik
 *
 */
public class WMSendToWarehouseLogger {

    private static final String FAILURE_MESSAGE = "WMSendToWarehouse: orderCode={0}, consignmentCode={1}, warehouseId={2}, errorCode={3}, errorMessage={4}";
    private static final String PRODUCT_INFO = ", consignmentEntries={0}";

    public void logMessage(final Logger logger, final SendToWarehouseRequest sendToWarehouseRequest,
            final String errorCode, final String errorValue, final boolean requireConsignmentDetails,
            final boolean isErrorMessage) {
        if (isErrorMessage) {
            logger.error(getLogMessage(sendToWarehouseRequest, errorCode, errorValue, requireConsignmentDetails));
        }
        else {
            logger.info(getLogMessage(sendToWarehouseRequest, errorCode, errorValue, requireConsignmentDetails));
        }
    }

    protected String getLogMessage(final SendToWarehouseRequest sendToWarehouseRequest, final String errorCode,
            final String errorValue, final boolean requireConsignmentDetails) {

        final String orderId = (sendToWarehouseRequest.getConsignment() != null
                && sendToWarehouseRequest.getConsignment().getOrder() != null)
                        ? sendToWarehouseRequest.getConsignment().getOrder().getId() : StringUtils.EMPTY;

        final String consignmentId = sendToWarehouseRequest.getConsignment() != null
                ? sendToWarehouseRequest.getConsignment().getId() : StringUtils.EMPTY;

        final String warehouseId = sendToWarehouseRequest.getConsignment() != null
                ? sendToWarehouseRequest.getConsignment().getWarehouseId() : StringUtils.EMPTY;

        final StringBuilder logMessage = new StringBuilder();
        logMessage.append(
                MessageFormat.format(FAILURE_MESSAGE, orderId, consignmentId, warehouseId, errorCode, errorValue));

        if (requireConsignmentDetails) {
            final String productCodes = getProductCodes(sendToWarehouseRequest);
            logMessage.append(MessageFormat.format(PRODUCT_INFO, productCodes));
        }

        return logMessage.toString();
    }

    private String getProductCodes(final SendToWarehouseRequest sendToWarehouseRequest) {
        final StringBuilder entries = new StringBuilder("{");
        final List<ConsignmentEntryDTO> consignmentEntries = sendToWarehouseRequest.getConsignment() != null
                ? sendToWarehouseRequest.getConsignment().getConsignmentEntries() : null;

        if (CollectionUtils.isNotEmpty(consignmentEntries)) {
            for (final ConsignmentEntryDTO consignmentEntryDto : consignmentEntries) {
                if (consignmentEntryDto.getProduct() != null) {
                    entries.append(WMUtil.convertToJSONString(consignmentEntryDto.getProduct())).append(",");
                }
            }
            entries.setCharAt(entries.length() - 1, '}');
            return entries.toString();
        }
        else {
            return StringUtils.EMPTY;
        }
    }

}

/**
 * 
 */
package au.com.target.tgtwebmethods.manifest.client.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtauspost.integration.dto.AuspostRequestDTO;
import au.com.target.tgtauspost.manifest.client.TransmitManifestClient;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.client.util.IntegrationErrorCodeProcessor;
import au.com.target.tgtwebmethods.rest.ManifestJSONResponse;
import au.com.target.tgtwebmethods.util.WMUtil;


/**
 * TransmitManifestClient implementation for Australia Post
 * 
 * @author jjayawa1
 *
 */
public class AusPostTransmitManifestClientImpl extends AbstractWebMethodsClient implements TransmitManifestClient {
    private static final Logger LOG = Logger.getLogger(AusPostTransmitManifestClientImpl.class);
    private static final String LOGGING_PREFIX = "AusPost Manifest Transmission: ";

    private RestTemplate restTemplate;
    private String serviceEndpoint;

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.manifest.client.TransmitManifestClient#transmitManifest(au.com.target.tgtfulfilment.dto.ManifestRequestDTO)
     */
    @Override
    public ManifestResponseDTO transmitManifest(final AuspostRequestDTO request) {

        LOG.info(LOGGING_PREFIX + "Sending request=" + WMUtil.convertToJSONString(request));

        ManifestJSONResponse jsonResponse = null;

        final ManifestResponseDTO response = new ManifestResponseDTO();
        jsonResponse = send(request, response, ManifestJSONResponse.class);

        if (jsonResponse != null) {
            if (jsonResponse.isSuccess()) {
                LOG.info(LOGGING_PREFIX + "SUCCEEDED and GENERATED_FILE_NAME=" + jsonResponse.getFilename());
                return (ManifestResponseDTO)IntegrationErrorCodeProcessor.getSuccessResponse(response);
            }
        }

        return response;
    }

    /**
     * @param restTemplate
     *            the restTemplate to set
     */
    @Required
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * @param serviceEndpoint
     *            the serviceEndpoint to set
     */
    @Required
    public void setServiceEndpoint(final String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return serviceEndpoint;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }

}

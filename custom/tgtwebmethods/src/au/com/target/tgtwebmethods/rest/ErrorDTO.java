/**
 * 
 */
package au.com.target.tgtwebmethods.rest;

/**
 * ERROR DTO from response.
 * 
 * @author jjayawa1
 *
 */
public class ErrorDTO {
    private String code;
    private String value;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }

}

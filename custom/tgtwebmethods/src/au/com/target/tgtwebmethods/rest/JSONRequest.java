/**
 * 
 */
package au.com.target.tgtwebmethods.rest;

/**
 * Wrapper to call REST API which accepts a String using web methods.
 * 
 * @author jjayawa1
 *
 */
public class JSONRequest {
    private Object jsonString;

    /**
     * @return the jsonString
     */
    public Object getJsonString() {
        return jsonString;
    }

    /**
     * @param jsonString
     *            the jsonString to set
     */
    public void setJsonString(final Object jsonString) {
        this.jsonString = jsonString;
    }

}

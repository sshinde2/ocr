/**
 * 
 */
package au.com.target.tgtwebmethods.rest;



/**
 * @author jjayawa1
 *
 */
public class LabelJSONResponse extends JSONResponse {

    private byte[] attachment;

    /**
     * @return the attachment
     */
    public byte[] getAttachment() {
        return attachment;
    }

    /**
     * @param attachment
     *            the attachment to set
     */
    public void setAttachment(final byte[] attachment) {
        this.attachment = attachment;
    }
}

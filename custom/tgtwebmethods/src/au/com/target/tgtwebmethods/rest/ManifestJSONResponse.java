/**
 * 
 */
package au.com.target.tgtwebmethods.rest;

/**
 * @author jjayawa1
 *
 */
public class ManifestJSONResponse extends JSONResponse {
    private ErrorDTO error;
    private String filename;

    /**
     * @return the error
     */
    public ErrorDTO getError() {
        return error;
    }

    /**
     * @param error
     *            the error to set
     */
    public void setError(final ErrorDTO error) {
        this.error = error;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename
     *            the filename to set
     */
    public void setFilename(final String filename) {
        this.filename = filename;
    }
}

package au.com.target.tgtwebmethods.setup;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 * 
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TgtwebmethodsConstants.EXTENSIONNAME)
public class WebMethodsSystemSetup extends AbstractSystemSetup {

    @Override
    public List<SystemSetupParameter> getInitializationOptions() {
        return null;
    }

    /**
     * This method will be called during the system initialization.
     * 
     * @param context
     *            the context provides the selected parameters and values
     */
    @SystemSetup(type = Type.PROJECT, process = Process.INIT)
    public void createProjectData(final SystemSetupContext context) {
        importImpexFile(context, "/tgtwebmethods/import/webmethods_user.impex");
    }

}

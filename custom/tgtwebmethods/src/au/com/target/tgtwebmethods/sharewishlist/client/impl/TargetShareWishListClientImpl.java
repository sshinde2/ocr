/**
 * 
 */
package au.com.target.tgtwebmethods.sharewishlist.client.impl;

import java.util.Collections;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.integration.dto.TargetIntegrationErrorDto;
import au.com.target.tgtcore.integration.dto.TargetIntegrationResponseDto;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.util.WMUtil;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;
import au.com.target.tgtwishlist.sharewishlist.client.ShareWishListClient;


/**
 * @author mjanarth
 *
 */
public class TargetShareWishListClientImpl extends AbstractWebMethodsClient implements ShareWishListClient {

    private static final Logger LOG = Logger.getLogger(TargetShareWishListClientImpl.class);
    private static final String LOGGING_PREFIX = "WISHLIST : action=shareWishList ";

    private RestTemplate restTemplate;

    private String shareWishListServiceUrl;

    /* (non-Javadoc)
     * @see au.com.target.tgtwishlist.sharewishlist.client.ShareWishListClient#sendWishList(au.com.target.tgtwishlist.data.TargetWishListSendRequestDto)
     */
    @Override
    public TargetIntegrationResponseDto shareWishList(final TargetWishListSendRequestDto request) {
        Assert.notNull(request, "Request DTO cannot be null");
        LOG.info(LOGGING_PREFIX + "Sending request=" + request);
        TargetIntegrationResponseDto jsonResponse = null;
        try {
            if (getRestTemplate() == null) {
                LOG.error(LOGGING_PREFIX + "No REST template defined");
                jsonResponse = buildFailedResponse(false, "No REST template defined");
                return jsonResponse;
            }

            if (StringUtils.isEmpty(getServiceEndpoint())) {
                LOG.error(LOGGING_PREFIX + "No Service endpoint defined");
                jsonResponse = buildFailedResponse(false, "No Service endpoint defined");
                return jsonResponse;
            }

            jsonResponse = getRestTemplate().postForObject(
                    getWebmethodsSecureBaseUrl() + getServiceEndpoint(), request,
                    TargetIntegrationResponseDto.class);
            if (jsonResponse != null) {
                LOG.info(LOGGING_PREFIX + "Received response=" + WMUtil.convertToJSONString(jsonResponse));
                return jsonResponse;
            }

            LOG.info(LOGGING_PREFIX + "Received response= The JSONResponse is Null");
            jsonResponse = buildFailedResponse(false, "The JSONResponse is Null");
        }
        catch (final RestClientException rce) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to get response from REST web service", rce);
            jsonResponse = buildFailedResponse(false, rce.getMessage());
        }
        catch (final HttpMessageNotReadableException nre) {
            LOG.error(LOGGING_PREFIX + "FAILED Unable to unmarshall response from REST web service", nre);
            jsonResponse = buildFailedResponse(false, nre.getMessage());
        }
        catch (final Exception e) {
            LOG.error(LOGGING_PREFIX + "FAILED with unexpected exception", e);
            jsonResponse = buildFailedResponse(false, e.getMessage());
        }
        return jsonResponse;
    }

    /**
     * @return TargetWishListSendResponseDto
     */
    private TargetIntegrationResponseDto buildFailedResponse(final boolean success, final String errorMessage) {
        final TargetIntegrationResponseDto jsonResponse;
        jsonResponse = new TargetIntegrationResponseDto();
        jsonResponse.setSuccess(success);
        final TargetIntegrationErrorDto error = new TargetIntegrationErrorDto();
        error.setErrorMessage(errorMessage);
        jsonResponse.setErrorList(Collections.singletonList(error));
        return jsonResponse;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return shareWishListServiceUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * @param shareWishListServiceUrl
     *            the shareWishListServiceUrl to set
     */
    @Required
    public void setShareWishListServiceUrl(final String shareWishListServiceUrl) {
        this.shareWishListServiceUrl = shareWishListServiceUrl;
    }

    /**
     * @param restTemplate
     *            the restTemplate to set
     */
    @Required
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

}

/**
 * 
 */
package au.com.target.tgtwebmethods.stock.client.impl;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtsale.stock.client.TargetStockUpdateClient;
import au.com.target.tgtsale.stock.dto.request.StockUpdateDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.exception.TargetWebMethodsClientException;


/**
 * @author rmcalave
 * 
 */
public class TargetStockUpdateClientImpl extends AbstractWebMethodsClient implements TargetStockUpdateClient
{
    private static final Logger LOG = Logger.getLogger(TargetStockUpdateClientImpl.class);

    private static JAXBContext stockUpdateDtoJaxbContext = null;
    private static JAXBContext stockUpdateResponseDtoJaxbContext = null;

    private static final String PARAM_NAME_PAYLOAD = "payload";

    private String storeStockServiceUrl;

    static
    {
        try
        {
            stockUpdateDtoJaxbContext = JAXBContext.newInstance(StockUpdateDto.class);
            stockUpdateResponseDtoJaxbContext = JAXBContext.newInstance(StockUpdateResponseDto.class);
        }
        catch (final JAXBException e)
        {
            LOG.error("Unable to create Jaxb context", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * au.com.target.tgtcore.stock.client.TargetStockUpdateClient#getStockLevelsFromPos(au.com.target.tgtcore.stock.dto
     * .StockUpdateDto)
     */
    @Override
    public StockUpdateResponseDto getStockLevelsFromPos(final StockUpdateDto stockUpdateDto)
            throws TargetWebMethodsClientException
    {
        LOG.info("Received call to get stock level from POS");
        final String stockUpdatePayload = marshalStockUpdateRequest(stockUpdateDto);
        final String stockUpdateResponse = callStoreStockService(stockUpdatePayload);
        return unmarshalStockUpdateResponse(stockUpdateResponse);
    }

    protected String marshalStockUpdateRequest(final StockUpdateDto stockUpdateDto)
            throws TargetWebMethodsClientException
    {
        try
        {
            final Marshaller stockUpdateMarshaller = stockUpdateDtoJaxbContext.createMarshaller();

            final StringWriter payloadWriter = new StringWriter();
            stockUpdateMarshaller.marshal(stockUpdateDto, payloadWriter);
            return payloadWriter.toString();
        }
        catch (final JAXBException ex)
        {
            LOG.error(SplunkLogFormatter.formatMessage("Unable to create marshaller for StockUpdateDto",
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS), ex);
            throw new TargetWebMethodsClientException(ex);
        }
    }

    protected String callStoreStockService(final String payload) throws TargetWebMethodsClientException
    {
        try
        {
            LOG.info("Begin communicating with web methods store stock service");
            final HttpPut httpput = new HttpPut(getWebmethodsBaseUrl() + storeStockServiceUrl);

            final List<NameValuePair> params = new ArrayList<>();
            if (LOG.isDebugEnabled()) {
                LOG.debug("Payload for call to store stock service : \n" + payload);
            }
            params.add(new BasicNameValuePair(PARAM_NAME_PAYLOAD, payload));

            final UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params);
            httpput.setEntity(entity);
            httpput.addHeader(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_FORM_URLENCODED);

            final ResponseHandler<String> handler = new ResponseHandler<String>()
            {

                /*
                 * (non-Javadoc)
                 * 
                 * @see org.apache.http.client.ResponseHandler#handleResponse(org.apache.http.HttpResponse)
                 */
                @Override
                public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException
                {
                    final HttpEntity entity = response.getEntity();
                    if (entity != null)
                    {
                        return EntityUtils.toString(entity);
                    }
                    else
                    {
                        return null;
                    }
                }

            };
            LOG.info("Sending call to web methods store stock service");
            final String response = getHttpClient().execute(httpput, handler);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Response from call to store stock service : \n" + response);
            }
            return response;
        }
        catch (final ClientProtocolException ex)
        {
            LOG.error(SplunkLogFormatter.formatMessage("Communication with WebMethods failed",
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS), ex);
            throw new TargetWebMethodsClientException(ex);
        }
        catch (final IOException ex)
        {
            LOG.error(SplunkLogFormatter.formatMessage("Communication with WebMethods failed",
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS), ex);
            throw new TargetWebMethodsClientException(ex);
        }
    }

    protected StockUpdateResponseDto unmarshalStockUpdateResponse(final String stockUpdateResponse)
            throws TargetWebMethodsClientException
    {
        try
        {
            LOG.info("Begin unmarshalling process for StockUpdateResponseDto");
            final Unmarshaller stockUpdateResponseUnmarshaller = stockUpdateResponseDtoJaxbContext.createUnmarshaller();

            final StringReader payloadReader = new StringReader(stockUpdateResponse);
            LOG.info("Unmarshalling StockUpdateResponseDto");
            final StockUpdateResponseDto responseDto = (StockUpdateResponseDto)stockUpdateResponseUnmarshaller
                    .unmarshal(payloadReader);
            return responseDto;
        }
        catch (final JAXBException ex)
        {
            LOG.error(SplunkLogFormatter.formatMessage("Unable to create unmarshaller for StockUpdateResponseDto",
                    TgtutilityConstants.ErrorCode.ERR_TGTWEBMETHODS), ex);
            throw new TargetWebMethodsClientException(ex);
        }
    }

    /**
     * @param storeStockServiceUrl
     *            the storeStockServiceUrl to set
     */
    @Required
    public void setStoreStockServiceUrl(final String storeStockServiceUrl)
    {
        this.storeStockServiceUrl = storeStockServiceUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getServiceEndpoint()
     */
    @Override
    protected String getServiceEndpoint() {
        return storeStockServiceUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwebmethods.client.AbstractWebMethodsClient#getRestTemplate()
     */
    @Override
    protected RestTemplate getRestTemplate() {
        return null;
    }

}

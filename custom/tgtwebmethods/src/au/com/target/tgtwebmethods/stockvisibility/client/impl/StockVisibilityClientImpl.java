/**
 * 
 */
package au.com.target.tgtwebmethods.stockvisibility.client.impl;

import de.hybris.platform.jalo.JaloSession;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.stockvisibility.client.StockVisibilityClient;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtwebmethods.client.AbstractWebMethodsClient;
import au.com.target.tgtwebmethods.stockvisibility.data.request.StockVisibilityRequest;
import au.com.target.tgtwebmethods.stockvisibility.data.response.StockVisibilityResponse;
import au.com.target.tgtwebmethods.util.StockVisibilityUtil;
import au.com.target.tgtwebmethods.util.WMUtil;


/**
 * @author mjanarth
 *
 */
public class StockVisibilityClientImpl extends AbstractWebMethodsClient implements StockVisibilityClient {

    private static final Logger LOG = Logger.getLogger(StockVisibilityClientImpl.class);
    private static final String LOGGING_PREFIX = "Store StockVis Request:";
    private static final String LOGGING_RESP = "Store StockVis Response:";
    private RestTemplate restTemplate;
    private String serviceEndpoint;
    private HttpHeaders jsonHttpHeader;

    /* (non-Javadoc)
     * @see au.com.target.tgtcore.stockvisibility.client.StockVisibilityClient#lookupStockForItemsInStores(java.util.List, java.util.List)
     */
    @Override
    public StockVisibilityItemLookupResponseDto lookupStockForItemsInStores(final List<String> storeNumbers,
            final List<String> itemcodes) {

        StockVisibilityItemLookupResponseDto response = null;
        if (!(validateConnection())) {
            return response;
        }
        final StockVisibilityRequest request = StockVisibilityUtil.populateStockVisibilityRequest(storeNumbers,
                itemcodes);
        if (null == request) {
            return response;
        }
        final StockVisibilityResponse stockVisibilityResponseDto = postRequest(request);
        response = StockVisibilityUtil.populateStockVisibilityResponse(stockVisibilityResponseDto);
        return response;
    }

    /**
     * Post the request to the webmethods url
     * 
     * @return StockVisibilityItemResponseDto
     */
    protected StockVisibilityResponse postRequest(final StockVisibilityRequest request) {
        StockVisibilityResponse response = new StockVisibilityResponse();
        String jaloSessionId = StringUtils.EMPTY;
        if (JaloSession.hasCurrentSession()) {
            final JaloSession jaloSession = JaloSession.getCurrentSession();
            jaloSessionId = jaloSession.getSessionID();
        }
        try {
            LOG.info(LOGGING_PREFIX + " sessionId=" + jaloSessionId + ", Sending request="
                    + WMUtil.convertToJSONString(request));
            final ResponseEntity<StockVisibilityResponse> responseEntity = restTemplate.exchange(
                    getWebmethodsSecureBaseUrl() + getServiceEndpoint(), HttpMethod.POST,
                    new HttpEntity<>(request, jsonHttpHeader), StockVisibilityResponse.class);
            response = responseEntity.getBody();
            LOG.info(LOGGING_RESP + " sessionId=" + jaloSessionId + ", Receiving response="
                    + WMUtil.convertToJSONString(response));
            return response;
        }
        catch (final RestClientException rce) {
            //TODO response needs to be populated for error scenarios
            LOG.error(LOGGING_RESP + " sessionId=" + jaloSessionId
                    + ", FAILED Unable to get response from REST web service", rce);
        }
        catch (final HttpMessageNotReadableException nre) {
            //TODO response needs to be populated for error scenarios
            LOG.error(LOGGING_RESP + " sessionId=" + jaloSessionId
                    + ", FAILED Unable to unmarshall response from REST web service", nre);
        }
        catch (final Exception ex) {
            //TODO response needs to be populated for error scenarios
            LOG.error(LOGGING_RESP + " sessionId=" + jaloSessionId
                    + ", FAILED Unable to get response from REST web service", ex);
        }
        return null;
    }

    /**
     * Validates the service end point and rest template
     * 
     * @return boolean
     */
    private boolean validateConnection() {

        if (StringUtils.isBlank(getServiceEndpoint())) {
            LOG.error("Service endpoint is empty");
            return false;
        }
        return true;
    }

    /**
     * @return the restTemplate
     */
    @Override
    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * @param restTemplate
     *            the restTemplate to set
     */
    @Required
    public void setRestTemplate(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * @return the serviceEndpoint
     */
    @Override
    public String getServiceEndpoint() {
        return serviceEndpoint;
    }

    /**
     * @param serviceEndpoint
     *            the serviceEndpoint to set
     */
    @Required
    public void setServiceEndpoint(final String serviceEndpoint) {
        this.serviceEndpoint = serviceEndpoint;
    }

    /**
     * @param jsonHttpHeader
     *            the jsonHttpHeader to set
     */
    @Required
    public void setJsonHttpHeader(final HttpHeaders jsonHttpHeader) {
        this.jsonHttpHeader = jsonHttpHeader;
    }

}

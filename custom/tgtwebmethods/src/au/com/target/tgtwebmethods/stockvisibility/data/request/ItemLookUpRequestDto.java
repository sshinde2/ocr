package au.com.target.tgtwebmethods.stockvisibility.data.request;

import java.util.List;


public class ItemLookUpRequestDto implements java.io.Serializable {

    private List<ItemDto> items;
    private List<StoreDto> stores;

    /**
     * @return the items
     */
    public List<ItemDto> getItems() {
        return items;
    }

    /**
     * @param items
     *            the items to set
     */
    public void setItems(final List<ItemDto> items) {
        this.items = items;
    }

    /**
     * @return the stores
     */
    public List<StoreDto> getStores() {
        return stores;
    }

    /**
     * @param stores
     *            the stores to set
     */
    public void setStores(final List<StoreDto> stores) {
        this.stores = stores;
    }



}
/**
 * 
 */
package au.com.target.tgtwebmethods.stockvisibility.data.request;

import java.io.Serializable;



/**
 * @author mjanarth
 *
 */
public class StockVisibilityRequest implements Serializable {

    private ItemLookUpRequestDto itemLookup;

    /**
     * @return the itemLookup
     */
    public ItemLookUpRequestDto getItemLookup() {
        return itemLookup;
    }

    /**
     * @param itemLookup
     *            the itemLookup to set
     */
    public void setItemLookup(final ItemLookUpRequestDto itemLookup) {
        this.itemLookup = itemLookup;
    }


}

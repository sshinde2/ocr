package au.com.target.tgtwebmethods.stockvisibility.data.response;

public class ItemStockDto implements java.io.Serializable {

    private String storeNumber;
    private String soh;
    private String code;

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the soh
     */
    public String getSoh() {
        return soh;
    }

    /**
     * @param soh
     *            the soh to set
     */
    public void setSoh(final String soh) {
        this.soh = soh;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }


}
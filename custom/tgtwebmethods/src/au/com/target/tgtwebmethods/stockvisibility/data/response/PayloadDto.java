package au.com.target.tgtwebmethods.stockvisibility.data.response;


public class PayloadDto implements java.io.Serializable {

    private ItemLookUpResponseDto itemLookupResponse;

    /**
     * @return the itemLookupResponse
     */
    public ItemLookUpResponseDto getItemLookupResponse() {
        return itemLookupResponse;
    }

    /**
     * @param itemLookupResponse
     *            the itemLookupResponse to set
     */
    public void setItemLookupResponse(final ItemLookUpResponseDto itemLookupResponse) {
        this.itemLookupResponse = itemLookupResponse;
    }



}
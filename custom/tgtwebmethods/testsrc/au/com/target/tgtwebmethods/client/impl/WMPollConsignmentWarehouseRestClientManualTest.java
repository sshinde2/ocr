/**
 * 
 */
package au.com.target.tgtwebmethods.client.impl;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.ManualTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfulfilment.client.PollConsignmentWarehouseRestClient;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.pollwarehouse.PollWarehouseResponse;


/**
 * @author gsing236
 *
 */
@ManualTest
public class WMPollConsignmentWarehouseRestClientManualTest extends ServicelayerTransactionalTest {

    @Resource
    private PollConsignmentWarehouseRestClient pollWarehouseRestClient;

    private PollWarehouseRequest request;

    @Before
    public void setUp() {
        request = new PollWarehouseRequest();
        final ArrayList<String> consignmentNumbers = new ArrayList<>();
        consignmentNumbers.add("a1111");
        consignmentNumbers.add("b1111");
        request.setConsignmentRequestNumbers(consignmentNumbers);
    }

    @Test
    public void testSuccess() {
        final PollWarehouseResponse response = pollWarehouseRestClient.pollConsignments(request);
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getErrorCode()).isNull();
        assertThat(response.getErrorType()).isNull();
        assertThat(response.getErrorValue()).isNull();
    }
}

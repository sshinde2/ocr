/**
 * 
 */
package au.com.target.tgtwebmethods.client.impl;

import de.hybris.bootstrap.annotations.ManualTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.CustomerDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.OrderDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ProductDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.RecipientDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;


/**
 * Checks whether a connection can be established to web methods for InComm.
 * 
 * @author jjayawa1
 *
 */
@ManualTest
public class WMSendToWarehouseRestClientManualTest extends ServicelayerTransactionalTest {

    @Resource
    private WMSendToWarehouseRestClient wmSendToWarehouseRestClient;

    private final SendToWarehouseRequest request = new SendToWarehouseRequest();

    private final Random random = new Random();

    @Before
    public void setupManifestRequestData() {
        final ConsignmentDTO consignment = new ConsignmentDTO();
        final long orderId = random.nextLong();
        consignment.setId("a" + orderId);
        consignment.setWarehouseId("Incomm");

        final CustomerDTO customer = new CustomerDTO();
        customer.setFirstName("Jane");
        customer.setLastName("Doe");
        customer.setEmailAddress("jane.doe@test.com");
        customer.setCountry("AU");

        final OrderDTO order = new OrderDTO();
        order.setId(Long.toString(orderId));
        order.setCustomer(customer);

        final RecipientDTO recipient1 = new RecipientDTO();
        recipient1.setFirstName("James");
        recipient1.setLastName("Doe");
        recipient1.setEmailAddress("james.doe@test.com");
        recipient1.setMessage("Just Saying Hello1");

        final RecipientDTO recipient2 = new RecipientDTO();
        recipient2.setFirstName("Joe");
        recipient2.setLastName("Doe");
        recipient2.setEmailAddress("joe.doe@test.com");
        recipient2.setMessage("Just Saying Hello2");

        final List<RecipientDTO> recipients = new ArrayList<>();
        recipients.add(recipient1);
        recipients.add(recipient2);

        final ProductDTO product = new ProductDTO();
        product.setId("PGC1009_GOLF2_10");
        product.setName("Incomm Venue 1 Golf");
        product.setDenomination("$10");
        product.setBrandId("venue1golfaus");
        product.setGiftCardStyle("");

        final ConsignmentEntryDTO consignmentEntry = new ConsignmentEntryDTO();
        consignmentEntry.setProduct(product);
        consignmentEntry.setRecipients(recipients);

        final List<ConsignmentEntryDTO> consignmentEntries = new ArrayList<>();
        consignmentEntries.add(consignmentEntry);

        consignment.setConsignmentEntries(consignmentEntries);
        consignment.setOrder(order);
        request.setConsignment(consignment);
    }

    @Test
    public void testSuccess() {
        final SendToWarehouseResponse response = wmSendToWarehouseRestClient.sendConsignment(request);
        Assert.assertNotNull(response);
        Assert.assertTrue(response.isSuccess());
    }

    @Test
    public void testError() {
        final SendToWarehouseResponse response = wmSendToWarehouseRestClient
                .sendConsignment(new SendToWarehouseRequest());
        Assert.assertNotNull(response);
        Assert.assertFalse(response.isSuccess());
        Assert.assertNotNull(response.getErrorValue());
        Assert.assertEquals("Request failed", response.getErrorValue());
        Assert.assertNotNull(response.getErrorCode());
    }
}

/**
 * 
 */
package au.com.target.tgtwebmethods.client.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ConsignmentEntryDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.CustomerDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.OrderDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.ProductDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.RecipientDTO;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseRequest;
import au.com.target.tgtfulfilment.integration.dto.sendtowarehouse.SendToWarehouseResponse;
import au.com.target.tgtwebmethods.constants.TgtwebmethodsConstants;
import au.com.target.tgtwebmethods.logger.WMSendToWarehouseLogger;
import au.com.target.tgtwebmethods.rest.ErrorDTO;
import au.com.target.tgtwebmethods.rest.JSONResponse;
import au.com.target.tgtwebmethods.util.WMUtil;
import junit.framework.Assert;


/**
 * @author pratik
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class WMSendToWarehouseRestClientTest {

    private static final Logger LOG = Logger.getLogger(WMSendToWarehouseRestClient.class);

    private static final String END_POINT = "END_POINT";
    private static final String BASE_URL = "BASE_URL";

    @InjectMocks
    private final WMSendToWarehouseRestClient restClient = new WMSendToWarehouseRestClient();

    @Mock
    private RestTemplate restTemplate;

    private Map<String, List<String>> digitalErrorCodesMapping;

    private JSONResponse jsonResponse;

    private SendToWarehouseRequest request;

    private SendToWarehouseResponse actualResponse;

    @Mock
    private WMSendToWarehouseLogger wmSendToWarehouselogger;

    @Before
    public void setUp() {
        restClient.setServiceEndpoint(END_POINT);
        restClient.setWebmethodsSecureBaseUrl(BASE_URL);

        digitalErrorCodesMapping = new HashMap<String, List<String>>();

        digitalErrorCodesMapping.put(TgtwebmethodsConstants.DigitalErrorType.ACCEPT,
                Collections.singletonList("103"));
        digitalErrorCodesMapping.put(TgtwebmethodsConstants.DigitalErrorType.INFORM_VENDOR,
                Collections.singletonList("400208"));
        restClient.setDigitalErrorCodesMapping(digitalErrorCodesMapping);
        createMockRequest();
    }

    @Test
    public void testSendConsignmentWithSuccessResponse() {
        jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(true);
        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
                .willReturn(jsonResponse);
        actualResponse = restClient.sendConsignment(request);
        Assert.assertTrue(actualResponse.isSuccess());
        Mockito.verifyZeroInteractions(wmSendToWarehouselogger);
    }

    @Test
    public void testSendConsignmentForAllowedErrorCodes() {
        jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setResponseCode(-6);

        final ErrorDTO error = new ErrorDTO();
        error.setCode("103");
        error.setValue("Allowed error type");
        jsonResponse.setErrors(Collections.singletonList(error));

        final String errorValue = error.getCode() + ":" + error.getValue() + ",";

        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
                .willReturn(jsonResponse);
        actualResponse = restClient.sendConsignment(request);

        Assert.assertTrue(actualResponse.isSuccess());
        Mockito.verify(wmSendToWarehouselogger).logMessage(LOG, request, "103", errorValue, false, false);
    }

    @Test
    public void testSendConsignmentForErrorCodesToInformVendor() {
        jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setResponseCode(-6);

        final ErrorDTO error = new ErrorDTO();
        error.setCode("400208");
        error.setValue("InformVendor error type");
        jsonResponse.setErrors(Collections.singletonList(error));

        final String errorValue = error.getCode() + ":" + error.getValue() + ",";

        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
                .willReturn(jsonResponse);
        actualResponse = restClient.sendConsignment(request);

        Assert.assertFalse(actualResponse.isSuccess());
        Mockito.verify(wmSendToWarehouselogger).logMessage(LOG, request, "400208", errorValue, true, true);
    }

    @Test
    public void testSendConsignmentForErrorCodesUnknown() {
        jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setResponseCode(-6);

        final ErrorDTO error = new ErrorDTO();
        error.setCode("500");
        error.setValue("Unknown error type");
        jsonResponse.setErrors(Collections.singletonList(error));

        final String errorValue = error.getCode() + ":" + error.getValue() + ",";

        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
                .willReturn(jsonResponse);
        actualResponse = restClient.sendConsignment(request);

        Assert.assertFalse(actualResponse.isSuccess());
        Mockito.verify(wmSendToWarehouselogger).logMessage(LOG, request, "500", errorValue, true, true);
    }

    @Test
    public void testSendConsignmentForInvalidRequest() {
        jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setResponseCode(-6);

        final ErrorDTO error = new ErrorDTO();
        error.setCode("500");
        error.setValue("Unknown error type");
        jsonResponse.setErrors(Collections.singletonList(error));

        final String errorValue = error.getCode() + ":" + error.getValue() + ",";

        BDDMockito.given(
                restTemplate.postForObject(Mockito.anyString(), Mockito.any(), Mockito.any(Class.class)))
                .willReturn(jsonResponse);
        final SendToWarehouseRequest emptyRequest = new SendToWarehouseRequest();
        actualResponse = restClient.sendConsignment(emptyRequest);

        Assert.assertFalse(actualResponse.isSuccess());
        Mockito.verify(wmSendToWarehouselogger).logMessage(LOG, emptyRequest, "500", errorValue, true, true);
    }

    private void createMockRequest() {
        request = new SendToWarehouseRequest();
        final ConsignmentDTO consignment = new ConsignmentDTO();
        final long orderId = 1234;
        consignment.setId("a" + orderId);
        consignment.setWarehouseId("Incomm");

        final CustomerDTO customer = new CustomerDTO();
        customer.setFirstName("Jane");
        customer.setLastName("Doe");
        customer.setEmailAddress("jane.doe@test.com");
        customer.setCountry("AU");

        final OrderDTO order = new OrderDTO();
        order.setId(Long.toString(orderId));
        order.setCustomer(customer);

        final RecipientDTO recipient1 = new RecipientDTO();
        recipient1.setFirstName("James");
        recipient1.setLastName("Doe");
        recipient1.setEmailAddress("james.doe@test.com");
        recipient1.setMessage("Just Saying Hello1");

        final RecipientDTO recipient2 = new RecipientDTO();
        recipient2.setFirstName("Joe");
        recipient2.setLastName("Doe");
        recipient2.setEmailAddress("joe.doe@test.com");
        recipient2.setMessage("Just Saying Hello2");

        final List<RecipientDTO> recipients = new ArrayList<>();
        recipients.add(recipient1);
        recipients.add(recipient2);

        final List<ConsignmentEntryDTO> consignmentEntries = new ArrayList<>();

        final ProductDTO product = new ProductDTO();
        product.setId("PGC1009_GOLF2_10");
        product.setName("Incomm Venue 1 Golf");
        product.setDenomination("$10");
        product.setBrandId("venue1golfaus");
        product.setGiftCardStyle("");

        final ConsignmentEntryDTO consignmentEntry = new ConsignmentEntryDTO();
        consignmentEntry.setProduct(product);
        consignmentEntry.setRecipients(recipients);

        consignmentEntries.add(consignmentEntry);

        consignment.setConsignmentEntries(consignmentEntries);
        consignment.setOrder(order);
        request.setConsignment(consignment);

        WMUtil.convertToJSONString(request.getConsignment().getConsignmentEntries().get(0).getProduct());
    }

}

/**
 * 
 */
package au.com.target.tgtwebmethods.client.util;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.util.StringUtils;

import au.com.target.tgtfulfilment.integration.dto.BaseResponseDTO;
import au.com.target.tgtfulfilment.integration.dto.IntegrationError;
import au.com.target.tgtwebmethods.rest.ErrorDTO;
import au.com.target.tgtwebmethods.rest.JSONResponse;


/**
 * @author rsamuel3
 *
 */
@UnitTest
public class IntegrationErrorCodeProcessorTest {
    @Test
    public void testGetFailedResponse() {
        final BaseResponseDTO response = new BaseResponseDTO();
        final JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setResponseCode(-1);
        jsonResponse.setErrors(buildErrors(true, "blank", "mind"));
        IntegrationErrorCodeProcessor.getFailedResponse(response, "String to log", jsonResponse);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccess()).isFalse();
        Assertions.assertThat(response.getErrorCode()).isNotNull().isNotEmpty().isEqualTo("-1");
        Assertions.assertThat(response.getErrorValue()).isNotNull().isNotEmpty().isEqualTo("0:blank,1:mind,");
        Assertions.assertThat(response.getErrorType()).isNotNull().isEqualTo(IntegrationError.MIDDLEWARE_ERROR);
    }

    @Test
    public void testGetFailedResponseNullResponseCode() {
        final BaseResponseDTO response = new BaseResponseDTO();
        final JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setErrors(buildErrors(true, "blank", "mind"));
        IntegrationErrorCodeProcessor.getFailedResponse(response, "String to log", jsonResponse);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccess()).isFalse();
        Assertions.assertThat(response.getErrorCode()).isNotNull().isNotEmpty().isEqualTo("0,1,");
        Assertions.assertThat(response.getErrorValue()).isNotNull().isNotEmpty().isEqualTo("0:blank,1:mind,");
        Assertions.assertThat(response.getErrorType()).isNotNull().isEqualTo(IntegrationError.MIDDLEWARE_ERROR);
    }

    @Test
    public void testGetFailedResponseNullResponseCodeMinus3() {
        final BaseResponseDTO response = new BaseResponseDTO();
        final JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setResponseCode(-3);
        jsonResponse.setErrors(buildErrors(true, "blank", "mind"));
        IntegrationErrorCodeProcessor.getFailedResponse(response, "String to log", jsonResponse);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccess()).isFalse();
        Assertions.assertThat(response.getErrorCode()).isNotNull().isNotEmpty().isEqualTo("-3");
        Assertions.assertThat(response.getErrorValue()).isNotNull().isNotEmpty().isEqualTo("0:blank,1:mind,");
        Assertions.assertThat(response.getErrorType()).isNotNull().isEqualTo(IntegrationError.CONNECTION_ERROR);
    }

    @Test
    public void testGetFailedResponseNullResponseCodeEmptyErrors() {
        final BaseResponseDTO response = new BaseResponseDTO();
        final JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setResponseCode(1);
        jsonResponse.setErrors(new ArrayList<ErrorDTO>());
        IntegrationErrorCodeProcessor.getFailedResponse(response, "String to log", jsonResponse);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccess()).isFalse();
        Assertions.assertThat(response.getErrorCode()).isNotNull().isNotEmpty().isEqualTo("1");
        Assertions.assertThat(response.getErrorValue()).isNotNull().isEmpty();
        Assertions.assertThat(response.getErrorType()).isNotNull().isEqualTo(IntegrationError.MIDDLEWARE_ERROR);
    }

    @Test
    public void testGetFailedResponseNullResponseCodeMinus6() {
        final BaseResponseDTO response = new BaseResponseDTO();
        final JSONResponse jsonResponse = new JSONResponse();
        jsonResponse.setSuccess(false);
        jsonResponse.setResponseCode(-6);
        jsonResponse.setErrors(buildErrors(true, "blank", "mind"));
        IntegrationErrorCodeProcessor.getFailedResponse(response, "String to log", jsonResponse);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccess()).isFalse();
        Assertions.assertThat(response.getErrorCode()).isNotNull().isNotEmpty().isEqualTo("-6");
        Assertions.assertThat(response.getErrorValue()).isNotNull().isNotEmpty().isEqualTo("0:blank,1:mind,");
        Assertions.assertThat(response.getErrorType()).isNotNull().isEqualTo(IntegrationError.REMOTE_SERVICE_ERROR);
    }

    @Test
    public void testGetIntegrationErrorCodeForNoErrors() {
        Assert.assertTrue(
                StringUtils.isEmpty(IntegrationErrorCodeProcessor.getIntegrationErrorCode(new BaseResponseDTO())));
    }

    @Test
    public void testGetIntegrationErrorCodeForSingleError() {
        final BaseResponseDTO response = new BaseResponseDTO();
        response.setErrorValue("103:Some error message");
        Assert.assertEquals("103", (IntegrationErrorCodeProcessor.getIntegrationErrorCode(response)));
    }

    @Test
    public void testGetIntegrationErrorCodeForMultipleErrors() {
        final BaseResponseDTO response = new BaseResponseDTO();
        response.setErrorValue("103:Some error message,104:SomeOther error");
        Assert.assertEquals("103", (IntegrationErrorCodeProcessor.getIntegrationErrorCode(response)));
    }

    /**
     * @param b
     * @param errors
     * @return List of ErrorDTO's
     */
    private List<ErrorDTO> buildErrors(final boolean b, final String... errors) {
        final List<ErrorDTO> errorDTOs = new ArrayList<>();
        int i = 0;
        for (final String error : errors) {
            final ErrorDTO errorDTO = new ErrorDTO();
            errorDTO.setCode(Integer.toString(i++));
            errorDTO.setValue(error);
            errorDTOs.add(errorDTO);
        }
        return errorDTOs;
    }
}

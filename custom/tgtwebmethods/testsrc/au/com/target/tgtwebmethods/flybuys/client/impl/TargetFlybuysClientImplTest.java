package au.com.target.tgtwebmethods.flybuys.client.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.sun.xml.internal.ws.client.BindingProviderProperties;
import com.sun.xml.internal.ws.client.ClientTransportException;

import au.com.target.services.services.flybuysrewards.v1.FlybuysAuthenticationRequest;
import au.com.target.services.services.flybuysrewards.v1.FlybuysAuthenticationResponse;
import au.com.target.services.services.flybuysrewards.v1.FlybuysConsumeRequest;
import au.com.target.services.services.flybuysrewards.v1.FlybuysConsumeResponse;
import au.com.target.services.services.flybuysrewards.v1.FlybuysRedemptionTier;
import au.com.target.services.services.flybuysrewards.v1.FlybuysRefundRequest;
import au.com.target.services.services.flybuysrewards.v1.FlybuysRefundResponse;
import au.com.target.services.services.flybuysrewards.v1.FlybuysRewardsServicePortType;
import au.com.target.services.services.flybuysrewards.v1.ObjectFactory;
import au.com.target.services.services.flybuysrewards.v1.RedeemTiers;
import au.com.target.services.services.flybuysrewards.v1.TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysAuthenticateResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysConsumeResponseDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRedeemTierDto;
import au.com.target.tgtcore.flybuys.dto.response.FlybuysRefundResponseDto;
import au.com.target.tgtcore.flybuys.enums.FlybuysResponseType;
import au.com.target.tgtwebmethods.flybuys.client.util.FlybuysErrorCodeProcessor;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetFlybuysClientImplTest {
    private static final String WM_USERNAME = "dwmol";
    private static final String WM_PASSWORD = "password :)";
    private static final Integer CONNECT_TIMEOUT = Integer.valueOf(20000);
    private static final Integer REQUEST_TIMEOUT = Integer.valueOf(60000);

    @Mock
    private TGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsService mockFlybuysRewardsServiceProvider;

    @Mock(extraInterfaces = { BindingProvider.class })
    private FlybuysRewardsServicePortType mockFlybuysRewardsServicePort;

    @Mock
    private ObjectFactory mockObjectFactory;

    @Mock
    private UsernamePasswordCredentials mockWmCredentials;

    @Mock
    private FlybuysErrorCodeProcessor mockFlybuysErrorCodeProcessor;

    @Mock
    private FlybuysAuthenticationRequest mockFlybuysAuthenticationRequest;

    @Mock
    private FlybuysAuthenticationResponse mockFlybuysAuthenticationResponse;

    @Mock
    private FlybuysConsumeRequest mockFlybuysConsumeRequest;

    @Mock
    private FlybuysConsumeResponse mockFlybuysConsumeResponse;

    @Mock
    private FlybuysRefundRequest mockFlybuysRefundRequest;

    @Mock
    private FlybuysRefundResponse mockFlybuysRefundResponse;

    @InjectMocks
    private final TargetFlybuysClientImpl targetFlybuysClientImpl = new TargetFlybuysClientImpl();

    private Map<String, Object> requestContextMap;

    @Before
    public void setUp() {
        requestContextMap = new HashMap<String, Object>();

        given(
                mockFlybuysRewardsServiceProvider
                        .getTGTINFlybuysRewardsServicesEnterpriseWsProviderV1FlybuysRewardsServicePort()).willReturn(
                                mockFlybuysRewardsServicePort);
        given(((BindingProvider)mockFlybuysRewardsServicePort).getRequestContext()).willReturn(requestContextMap);
        given(mockObjectFactory.createFlybuysAuthenticationRequest()).willReturn(mockFlybuysAuthenticationRequest);
        given(mockObjectFactory.createFlybuysConsumeRequest()).willReturn(mockFlybuysConsumeRequest);
        given(mockObjectFactory.createFlybuysRefundRequest()).willReturn(mockFlybuysRefundRequest);

        given(mockWmCredentials.getUserName()).willReturn(WM_USERNAME);
        given(mockWmCredentials.getPassword()).willReturn(WM_PASSWORD);

        targetFlybuysClientImpl.setConnectTimeoutMilliseconds(CONNECT_TIMEOUT);
        targetFlybuysClientImpl.setRequestTimeoutMilliseconds(REQUEST_TIMEOUT);
    }

    @Test
    public void testFlybuysAuthenticateWithNonSuccessResponse() throws Exception {
        final String flybuysNumber = "6008947000003419";
        final Date dob = DateTime.now().withTimeAtStartOfDay().toDate();
        final String postcode = "3000";

        final String responseCode = "1";
        final String responseMessasge = "This is an invalid response";

        given(mockFlybuysRewardsServicePort.authenticateFlybuysCustomer(mockFlybuysAuthenticationRequest)).willReturn(
                mockFlybuysAuthenticationResponse);

        given(mockFlybuysErrorCodeProcessor.getFlybuysResponseTypeForCode(responseCode)).willReturn(
                FlybuysResponseType.INVALID);

        final JAXBElement<String> mockResponseMessageElement = mock(JAXBElement.class);
        given(mockResponseMessageElement.getValue()).willReturn(responseMessasge);

        given(mockFlybuysAuthenticationResponse.getResponseCode()).willReturn(responseCode);
        given(mockFlybuysAuthenticationResponse.getResponseMessage()).willReturn(mockResponseMessageElement);

        final FlybuysAuthenticateResponseDto result = targetFlybuysClientImpl.flybuysAuthenticate(flybuysNumber, dob,
                postcode);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.INVALID);
        assertThat(result.getErrorMessage()).isEqualTo(responseMessasge);

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testFlybuysAuthenticateWithSuccessResponseNoTiers() throws Exception {
        final String flybuysNumber = "6008947000003419";
        final Date dob = DateTime.now().withTimeAtStartOfDay().toDate();
        final String postcode = "3000";

        final String responseCode = "0";
        final String availablePoints = "18763";
        final String securityToken = "awesomesecuritytoken";

        given(mockFlybuysRewardsServicePort.authenticateFlybuysCustomer(mockFlybuysAuthenticationRequest)).willReturn(
                mockFlybuysAuthenticationResponse);

        given(mockFlybuysErrorCodeProcessor.getFlybuysResponseTypeForCode(responseCode)).willReturn(
                FlybuysResponseType.SUCCESS);

        final JAXBElement<String> mockAvailablePoints = mock(JAXBElement.class);
        given(mockAvailablePoints.getValue()).willReturn(availablePoints);

        final JAXBElement<String> mockSecurityToken = mock(JAXBElement.class);
        given(mockSecurityToken.getValue()).willReturn(securityToken);

        given(mockFlybuysAuthenticationResponse.getResponseCode()).willReturn(responseCode);
        given(mockFlybuysAuthenticationResponse.getAvailPoints()).willReturn(mockAvailablePoints);
        given(mockFlybuysAuthenticationResponse.getSecurityToken()).willReturn(mockSecurityToken);

        final FlybuysAuthenticateResponseDto result = targetFlybuysClientImpl.flybuysAuthenticate(flybuysNumber, dob,
                postcode);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.SUCCESS);
        assertThat(result.getRedeemTiers()).isEmpty();
        assertThat(result.getAvailPoints()).isEqualTo(Integer.valueOf(availablePoints));
        assertThat(result.getSecurityToken()).isEqualTo(securityToken);
        assertThat(result.getErrorMessage()).isNullOrEmpty();

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testFlybuysAuthenticateWithSuccessResponseOneTier() throws Exception {
        final String flybuysNumber = "6008947000003419";
        final Date dob = DateTime.now().withTimeAtStartOfDay().toDate();
        final String postcode = "3000";

        final String responseCode = "0";
        final String availablePoints = "18763";
        final String securityToken = "awesomesecuritytoken";

        given(mockFlybuysRewardsServicePort.authenticateFlybuysCustomer(mockFlybuysAuthenticationRequest)).willReturn(
                mockFlybuysAuthenticationResponse);

        given(mockFlybuysErrorCodeProcessor.getFlybuysResponseTypeForCode(responseCode)).willReturn(
                FlybuysResponseType.SUCCESS);

        final JAXBElement<String> mockAvailablePoints = mock(JAXBElement.class);
        given(mockAvailablePoints.getValue()).willReturn(availablePoints);

        final JAXBElement<String> mockSecurityToken = mock(JAXBElement.class);
        given(mockSecurityToken.getValue()).willReturn(securityToken);

        final FlybuysRedemptionTier redeemTier1 = new FlybuysRedemptionTier();
        redeemTier1.setDollars("10.00");
        redeemTier1.setPoints("2000");
        redeemTier1.setRedeemCode("CKLP");

        final List<FlybuysRedemptionTier> redeemTiersList = new ArrayList<>();
        redeemTiersList.add(redeemTier1);

        final RedeemTiers redeemTiers = mock(RedeemTiers.class);
        given(redeemTiers.getTier()).willReturn(redeemTiersList);

        final JAXBElement<RedeemTiers> redeemTiersElement = mock(JAXBElement.class);
        given(redeemTiersElement.getValue()).willReturn(redeemTiers);

        given(mockFlybuysAuthenticationResponse.getResponseCode()).willReturn(responseCode);
        given(mockFlybuysAuthenticationResponse.getAvailPoints()).willReturn(mockAvailablePoints);
        given(mockFlybuysAuthenticationResponse.getSecurityToken()).willReturn(mockSecurityToken);
        given(mockFlybuysAuthenticationResponse.getRedeemTiers()).willReturn(redeemTiersElement);

        final FlybuysAuthenticateResponseDto result = targetFlybuysClientImpl.flybuysAuthenticate(flybuysNumber, dob,
                postcode);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.SUCCESS);
        assertThat(result.getAvailPoints()).isEqualTo(Integer.valueOf(availablePoints));
        assertThat(result.getSecurityToken()).isEqualTo(securityToken);
        assertThat(result.getErrorMessage()).isNullOrEmpty();

        assertThat(result.getRedeemTiers()).hasSize(1);
        final FlybuysRedeemTierDto targetRedeemTier = result.getRedeemTiers().iterator().next();
        assertThat(targetRedeemTier.getDollarAmt()).isEqualByComparingTo(BigDecimal.valueOf(10.00));
        assertThat(targetRedeemTier.getPoints()).isEqualTo(2000);
        assertThat(targetRedeemTier.getRedeemCode()).isEqualTo("CKLP");

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testFlybuysAuthenticateWithNullServiceProvider() throws Exception {
        final String flybuysNumber = "6008947000003419";
        final Date dob = DateTime.now().withTimeAtStartOfDay().toDate();
        final String postcode = "3000";

        targetFlybuysClientImpl.setFlybuysRewardsServiceProvider(null);

        final FlybuysAuthenticateResponseDto result = targetFlybuysClientImpl.flybuysAuthenticate(flybuysNumber, dob,
                postcode);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);
        assertThat(result.getErrorMessage()).isNotEmpty();
    }

    @Test
    public void testFlybuysAuthenticateWithSoapFaultException() throws Exception {
        final String flybuysNumber = "6008947000003419";
        final Date dob = DateTime.now().withTimeAtStartOfDay().toDate();
        final String postcode = "3000";

        final String exceptionMessage = "Exception message";

        final SOAPFaultException ex = mock(SOAPFaultException.class);
        given(ex.getMessage()).willReturn(exceptionMessage);
        given(mockFlybuysRewardsServicePort.authenticateFlybuysCustomer(mockFlybuysAuthenticationRequest))
                .willThrow(ex);

        final FlybuysAuthenticateResponseDto result = targetFlybuysClientImpl.flybuysAuthenticate(flybuysNumber, dob,
                postcode);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);
        assertThat(result.getErrorMessage()).isEqualTo(exceptionMessage);

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testFlybuysAuthenticateWithClientTransportException() throws Exception {
        final String flybuysNumber = "6008947000003419";
        final Date dob = DateTime.now().withTimeAtStartOfDay().toDate();
        final String postcode = "3000";

        final String exceptionMessage = "Exception message";

        final ClientTransportException ex = mock(ClientTransportException.class);
        given(ex.getMessage()).willReturn(exceptionMessage);
        given(mockFlybuysRewardsServicePort.authenticateFlybuysCustomer(mockFlybuysAuthenticationRequest))
                .willThrow(ex);

        final FlybuysAuthenticateResponseDto result = targetFlybuysClientImpl.flybuysAuthenticate(flybuysNumber, dob,
                postcode);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);
        assertThat(result.getErrorMessage()).isEqualTo(exceptionMessage);

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testConsumeFlybuysPointsWithNullServiceProvider() {
        final String flybuysCardNumber = "6008947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "5599";
        final Integer points = Integer.valueOf(2000);
        final BigDecimal dollars = new BigDecimal("10.00");
        final String redeemCode = "CQKT";
        final String transactionId = "12345678";

        targetFlybuysClientImpl.setFlybuysRewardsServiceProvider(null);

        final FlybuysConsumeResponseDto result = targetFlybuysClientImpl.consumeFlybuysPoints(flybuysCardNumber,
                securityToken, storeNumber, points, dollars, redeemCode, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);
    }

    @Test
    public void testConsumeFlybuysPointsWithSoapFaultException() {
        final String flybuysCardNumber = "6008947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "5599";
        final Integer points = Integer.valueOf(2000);
        final BigDecimal dollars = new BigDecimal("10.00");
        final String redeemCode = "CQKT";
        final String transactionId = "12345678";

        final String exceptionMessage = "Exception message";

        final SOAPFaultException ex = mock(SOAPFaultException.class);
        given(ex.getMessage()).willReturn(exceptionMessage);
        given(mockFlybuysRewardsServicePort.consumeFlybuysPoints(mockFlybuysConsumeRequest))
                .willThrow(ex);

        final FlybuysConsumeResponseDto result = targetFlybuysClientImpl.consumeFlybuysPoints(flybuysCardNumber,
                securityToken, storeNumber, points, dollars, redeemCode, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testConsumeFlybuysPointsWithClientTransportException() {
        final String flybuysCardNumber = "6008947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "5599";
        final Integer points = Integer.valueOf(2000);
        final BigDecimal dollars = new BigDecimal("10.00");
        final String redeemCode = "CQKT";
        final String transactionId = "12345678";

        final String exceptionMessage = "Exception message";

        final ClientTransportException ex = mock(ClientTransportException.class);
        given(ex.getMessage()).willReturn(exceptionMessage);
        given(mockFlybuysRewardsServicePort.consumeFlybuysPoints(mockFlybuysConsumeRequest))
                .willThrow(ex);

        final FlybuysConsumeResponseDto result = targetFlybuysClientImpl.consumeFlybuysPoints(flybuysCardNumber,
                securityToken, storeNumber, points, dollars, redeemCode, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testConsumeFlybuysPointsWithSuccess() {
        final String flybuysCardNumber = "6008947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "5599";
        final Integer points = Integer.valueOf(2000);
        final BigDecimal dollars = new BigDecimal("10.00");
        final String redeemCode = "CQKT";
        final String transactionId = "12345678";

        final String responseCode = "0";
        final String confirmationCode = "thisisaconfirmationcode";

        given(mockFlybuysConsumeResponse.getResponseCode()).willReturn(responseCode);

        final JAXBElement<String> mockConfirmationCodeMessageElement = mock(JAXBElement.class);
        given(mockConfirmationCodeMessageElement.getValue()).willReturn(confirmationCode);
        given(mockFlybuysConsumeResponse.getResponseMessage()).willReturn(mockConfirmationCodeMessageElement);

        given(mockFlybuysConsumeResponse.getConfirmationCode()).willReturn(mockConfirmationCodeMessageElement);

        given(mockFlybuysErrorCodeProcessor.getFlybuysResponseTypeForCode(responseCode)).willReturn(
                FlybuysResponseType.SUCCESS);

        given(mockFlybuysRewardsServicePort.consumeFlybuysPoints(mockFlybuysConsumeRequest)).willReturn(
                mockFlybuysConsumeResponse);

        final FlybuysConsumeResponseDto result = targetFlybuysClientImpl.consumeFlybuysPoints(flybuysCardNumber,
                securityToken, storeNumber, points, dollars, redeemCode, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.SUCCESS);
        assertThat(result.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));
        assertThat(result.getConfimationCode()).isEqualTo(confirmationCode);

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testConsumeFlybuysPointsWithNonSuccess() {
        final String flybuysCardNumber = "6008947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "5599";
        final Integer points = Integer.valueOf(2000);
        final BigDecimal dollars = new BigDecimal("10.00");
        final String redeemCode = "CQKT";
        final String transactionId = "12345678";

        final String responseCode = "1";
        final String responseMessasge = "This is an invalid response";

        given(mockFlybuysConsumeResponse.getResponseCode()).willReturn(responseCode);

        final JAXBElement<String> mockResponseMessageElement = mock(JAXBElement.class);
        given(mockResponseMessageElement.getValue()).willReturn(responseMessasge);
        given(mockFlybuysConsumeResponse.getResponseMessage()).willReturn(mockResponseMessageElement);

        given(mockFlybuysErrorCodeProcessor.getFlybuysResponseTypeForCode(responseCode)).willReturn(
                FlybuysResponseType.INVALID);

        given(mockFlybuysRewardsServicePort.consumeFlybuysPoints(mockFlybuysConsumeRequest)).willReturn(
                mockFlybuysConsumeResponse);

        final FlybuysConsumeResponseDto result = targetFlybuysClientImpl.consumeFlybuysPoints(flybuysCardNumber,
                securityToken, storeNumber, points, dollars, redeemCode, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.INVALID);
        assertThat(result.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }


    @Test
    public void testFlybuysRefundPointsWithSuccess() {
        final String flybuysCardNumber = "7708947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "9988";
        final Integer points = Integer.valueOf(3000);
        final BigDecimal dollars = new BigDecimal("45.00");
        final String timeStamp = new Timestamp(new Date().getTime()).toString();
        final String transactionId = "12345678";

        final String responseCode = "0";
        final String confirmationCode = "thisisaconfirmationcode";

        given(mockFlybuysRefundResponse.getResponseCode()).willReturn(responseCode);
        final JAXBElement<String> mockConfirmationCodeMessageElement = mock(JAXBElement.class);
        given(mockConfirmationCodeMessageElement.getValue()).willReturn(confirmationCode);
        given(mockFlybuysRefundResponse.getResponseMessage()).willReturn(mockConfirmationCodeMessageElement);
        given(mockFlybuysRefundResponse.getConfirmationCode()).willReturn(mockConfirmationCodeMessageElement);
        given(mockFlybuysErrorCodeProcessor.getFlybuysResponseTypeForCode(responseCode)).willReturn(
                FlybuysResponseType.SUCCESS);

        given(mockFlybuysRewardsServicePort.refundFlybuysPoints(mockFlybuysRefundRequest)).willReturn(
                mockFlybuysRefundResponse);

        final FlybuysRefundResponseDto result = targetFlybuysClientImpl.refundFlybuysPoints(flybuysCardNumber,
                securityToken,
                storeNumber, points, dollars, timeStamp, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.SUCCESS);
        assertThat(result.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));
        assertThat(result.getConfimationCode()).isEqualTo(confirmationCode);
        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testFlybuysRefundPointsNonSuccess() {
        final String flybuysCardNumber = "7708947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "9988";
        final Integer points = Integer.valueOf(3000);
        final BigDecimal dollars = new BigDecimal("45.00");
        final String timeStamp = new Timestamp(new Date().getTime()).toString();
        final String transactionId = "12345678";

        final String responseCode = "1";
        final String responseMessasge = "Unavailable";

        given(mockFlybuysRefundResponse.getResponseCode()).willReturn(responseCode);
        final JAXBElement<String> mockResponseMessageElement = mock(JAXBElement.class);
        given(mockResponseMessageElement.getValue()).willReturn(responseMessasge);
        given(mockFlybuysRefundResponse.getResponseMessage()).willReturn(mockResponseMessageElement);
        given(mockFlybuysErrorCodeProcessor.getFlybuysResponseTypeForCode(responseCode)).willReturn(
                FlybuysResponseType.UNAVAILABLE);
        given(mockFlybuysRewardsServicePort.refundFlybuysPoints(mockFlybuysRefundRequest)).willReturn(
                mockFlybuysRefundResponse);
        final FlybuysRefundResponseDto result = targetFlybuysClientImpl.refundFlybuysPoints(flybuysCardNumber,
                securityToken,
                storeNumber, points, dollars, timeStamp, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);
        assertThat(result.getResponseCode()).isEqualTo(Integer.valueOf(responseCode));
        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testFlybuysRefundPointsWithNullServiceProvider() {
        final String flybuysCardNumber = "6008947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "5599";
        final Integer points = Integer.valueOf(2000);
        final BigDecimal dollars = new BigDecimal("10.00");
        final String redeemCode = "CQKT";
        final String transactionId = "12345678";

        targetFlybuysClientImpl.setFlybuysRewardsServiceProvider(null);

        final FlybuysRefundResponseDto result = targetFlybuysClientImpl.refundFlybuysPoints(flybuysCardNumber,
                securityToken, storeNumber, points, dollars, redeemCode, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);
    }

    @Test
    public void testFlybuysRefundPointsWithSoapFaultException() {
        final String flybuysCardNumber = "6008947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "5599";
        final Integer points = Integer.valueOf(2000);
        final BigDecimal dollars = new BigDecimal("10.00");
        final String redeemCode = "CQKT";
        final String transactionId = "12345678";

        final String exceptionMessage = "Exception message";

        final SOAPFaultException ex = mock(SOAPFaultException.class);
        given(ex.getMessage()).willReturn(exceptionMessage);
        given(mockFlybuysRewardsServicePort.refundFlybuysPoints(mockFlybuysRefundRequest))
                .willThrow(ex);

        final FlybuysRefundResponseDto result = targetFlybuysClientImpl.refundFlybuysPoints(flybuysCardNumber,
                securityToken, storeNumber, points, dollars, redeemCode, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }

    @Test
    public void testFlybuysRefundPointsWithClientTransportException() {
        final String flybuysCardNumber = "6008947000003419";
        final String securityToken = "thisisasecuritytoken";
        final String storeNumber = "5599";
        final Integer points = Integer.valueOf(2000);
        final BigDecimal dollars = new BigDecimal("10.00");
        final String redeemCode = "CQKT";
        final String transactionId = "12345678";

        final String exceptionMessage = "Exception message";

        final ClientTransportException ex = mock(ClientTransportException.class);
        given(ex.getMessage()).willReturn(exceptionMessage);
        given(mockFlybuysRewardsServicePort.refundFlybuysPoints(mockFlybuysRefundRequest)).willThrow(ex);

        final FlybuysRefundResponseDto result = targetFlybuysClientImpl.refundFlybuysPoints(flybuysCardNumber,
                securityToken, storeNumber, points, dollars, redeemCode, transactionId);

        assertThat(result.getResponse()).isEqualTo(FlybuysResponseType.UNAVAILABLE);

        assertThat(requestContextMap.get(BindingProvider.USERNAME_PROPERTY)).isEqualTo(WM_USERNAME);
        assertThat(requestContextMap.get(BindingProvider.PASSWORD_PROPERTY)).isEqualTo(WM_PASSWORD);
        assertThat(requestContextMap.get(BindingProviderProperties.CONNECT_TIMEOUT)).isEqualTo(CONNECT_TIMEOUT);
        assertThat(requestContextMap.get(BindingProviderProperties.REQUEST_TIMEOUT)).isEqualTo(REQUEST_TIMEOUT);
    }


}

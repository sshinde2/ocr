/**
 * 
 */
package au.com.target.tgtwebmethods.sharewishlist.client.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.integration.dto.TargetIntegrationResponseDto;
import au.com.target.tgtwishlist.data.TargetCustomerWishListDto;
import au.com.target.tgtwishlist.data.TargetCustomerWishListProductsDto;
import au.com.target.tgtwishlist.data.TargetRecipientDto;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetShareWishListClientImplTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private TargetWishListSendRequestDto requestDto;

    private final TargetShareWishListClientImpl targetShareWishListClient = new TargetShareWishListClientImpl();

    @Before
    public void setUp() {
        targetShareWishListClient.setShareWishListServiceUrl("test.url");
        targetShareWishListClient.setRestTemplate(restTemplate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testShareWishListWithNullRequest() {
        targetShareWishListClient.shareWishList(null);
    }

    @Test
    public void testShareWishListWithRestTemplateNull() {
        targetShareWishListClient.setRestTemplate(null);
        final TargetIntegrationResponseDto responseDto = targetShareWishListClient.shareWishList(requestDto);
        Assertions.assertThat(responseDto).isNotNull();
        Assertions.assertThat(responseDto.isSuccess()).isFalse();
        Assertions.assertThat(responseDto.getErrorList()).isNotNull().isNotEmpty().hasSize(1);
        Assertions.assertThat(responseDto.getErrorList().get(0).getErrorMessage()).isNotNull().isNotEmpty()
                .isEqualTo("No REST template defined");
    }

    @Test
    public void testShareWishListWithServiceEndpointNull() {
        targetShareWishListClient.setShareWishListServiceUrl(null);
        final TargetIntegrationResponseDto responseDto = targetShareWishListClient.shareWishList(requestDto);
        Assertions.assertThat(responseDto).isNotNull();
        Assertions.assertThat(responseDto.isSuccess()).isFalse();
        Assertions.assertThat(responseDto.getErrorList()).isNotNull().isNotEmpty().hasSize(1);
        Assertions.assertThat(responseDto.getErrorList().get(0).getErrorMessage()).isNotNull().isNotEmpty()
                .isEqualTo("No Service endpoint defined");
    }

    @Test
    public void testShareWishListWithJsonResponseNull() {
        final TargetWishListSendRequestDto request = buildRequest();
        BDDMockito.when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(),
                Mockito.eq(TargetIntegrationResponseDto.class))).thenReturn(null);
        final TargetIntegrationResponseDto responseDto = targetShareWishListClient.shareWishList(request);
        Assertions.assertThat(responseDto).isNotNull();
        Assertions.assertThat(responseDto.isSuccess()).isFalse();
        Assertions.assertThat(responseDto.getErrorList()).isNotNull().isNotEmpty().hasSize(1);
        Assertions.assertThat(responseDto.getErrorList().get(0).getErrorMessage()).isNotNull().isNotEmpty()
                .isEqualTo("The JSONResponse is Null");

        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.eq(request),
                Mockito.eq(TargetIntegrationResponseDto.class));
    }


    @Test
    public void testShareWishListWithJsonResponseWithValidResponse() {
        final TargetWishListSendRequestDto request = buildRequest();
        BDDMockito.when(restTemplate.postForObject(Mockito.anyString(), Mockito.anyString(),
                Mockito.eq(TargetIntegrationResponseDto.class))).thenReturn(createValidIntegrationResponse());
        final TargetIntegrationResponseDto responseDto = targetShareWishListClient.shareWishList(request);
        Assertions.assertThat(responseDto).isNotNull();
        Assertions.assertThat(responseDto.isSuccess()).isTrue();
        Assertions.assertThat(responseDto.getErrorList()).isNull();

        Mockito.verify(restTemplate).postForObject(Mockito.anyString(), Mockito.eq(request),
                Mockito.eq(TargetIntegrationResponseDto.class));
    }

    /**
     * @return TargetIntegrationResponseDto
     */
    private TargetIntegrationResponseDto createValidIntegrationResponse() {
        final TargetIntegrationResponseDto responseDto = new TargetIntegrationResponseDto();
        responseDto.setSuccess(true);
        return responseDto;
    }

    /**
     * 
     */
    protected TargetWishListSendRequestDto buildRequest() {
        final TargetWishListSendRequestDto requestWishList = new TargetWishListSendRequestDto();
        final TargetCustomerWishListDto customer = new TargetCustomerWishListDto();
        customer.setCcme(Boolean.toString(true));
        customer.setEmailAddress("john.smith@test.com");
        customer.setFirstName("John");
        customer.setLastName("Smith");
        customer.setNotShownCount("+7");
        customer.setAccessUrl("http://www.target.com.au/favouritesForJohnSmith");
        final List<TargetCustomerWishListProductsDto> products = populateProducts();
        customer.setRecipients(populateRecipient());
        customer.setProducts(products);
        requestWishList.setCustomer(customer);
        return requestWishList;
    }

    /**
     * @return List<TargetRecipientDto>
     */
    private List<TargetRecipientDto> populateRecipient() {
        final List<TargetRecipientDto> recipientDtos = new ArrayList<>();
        final TargetRecipientDto recipient = new TargetRecipientDto();
        recipient.setEmailAddress("jane.doe@test.com");
        recipient.setName("Jane Doe");
        recipient.setMessage("Hello Jane, I would like to share my favourites with you");
        recipientDtos.add(recipient);
        return recipientDtos;
    }

    /**
     * @return list of TargetCustomerWishListProducts
     */
    private List<TargetCustomerWishListProductsDto> populateProducts() {
        final List<TargetCustomerWishListProductsDto> products = new ArrayList<>();
        final TargetCustomerWishListProductsDto wishlist1 = new TargetCustomerWishListProductsDto();
        wishlist1.setBaseCode("P1000");
        wishlist1.setColour("blue");
        wishlist1.setVariantCode("P1000_blue_M");
        wishlist1.setName("T - Shirt");
        wishlist1.setImageURL("https://www.target.com.au/media/P1000_blue_M.jpg");
        wishlist1.setProductURL("https://www.target.com.au/p/P1000_blue_M");
        wishlist1.setColour("blue");
        wishlist1.setSize("4");
        wishlist1.setPrice("$39.99");
        products.add(wishlist1);

        final TargetCustomerWishListProductsDto wishlist2 = new TargetCustomerWishListProductsDto();
        wishlist2.setBaseCode("P1002");
        wishlist2.setName("Pants");
        wishlist2.setImageURL("https://www.target.com.au/media/p1002.jpg");
        wishlist2.setProductURL("https://www.target.com.au/p/P1002");
        wishlist2.setColour("no colour");
        wishlist2.setSize("10");
        wishlist2.setPrice("$10");
        products.add(wishlist2);

        return products;
    }
}

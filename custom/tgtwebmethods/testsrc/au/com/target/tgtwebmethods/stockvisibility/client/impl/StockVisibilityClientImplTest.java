/**
 * 
 */
package au.com.target.tgtwebmethods.stockvisibility.client.impl;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.ItemLookUpResponseDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.ItemStockDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.PayloadDto;
import au.com.target.tgtwebmethods.stockvisibility.data.response.StockVisibilityResponse;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StockVisibilityClientImplTest {

    private static final String WEBMETHODS_BASE_URL = "http://webmethods.notreal";
    private static final String SERVICE_ENDPOINT = "/endpoint";

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final StockVisibilityClientImpl stockVisibilityClient = new StockVisibilityClientImpl();


    @Before
    public void setup() {
        stockVisibilityClient.setServiceEndpoint(SERVICE_ENDPOINT);
        stockVisibilityClient.setWebmethodsSecureBaseUrl(WEBMETHODS_BASE_URL);
    }

    @Test
    public void testLookupStockForItemsInStoresBlankEndpoint() {
        stockVisibilityClient.setServiceEndpoint("");
        final List<String> stores = new ArrayList<>();
        stores.add("5317");
        stores.add("5352");
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000_Pink_S");
        final StockVisibilityItemLookupResponseDto response = stockVisibilityClient.lookupStockForItemsInStores(stores,
                productCodes);
        Assert.assertNull(response);
    }

    @Test
    public void testLookupStockForItemsInStoresNullReq() {
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000_Pink_S");
        final StockVisibilityItemLookupResponseDto response = stockVisibilityClient.lookupStockForItemsInStores(null,
                productCodes);
        Assert.assertNull(response);
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testLookupStockForItemsInStoresEmptyProductCodes() {
        final List<String> productCodes = new ArrayList<>();
        final List<String> stores = new ArrayList<>();
        stores.add("5617");
        final StockVisibilityItemLookupResponseDto response = stockVisibilityClient.lookupStockForItemsInStores(stores,
                productCodes);
        Assert.assertNull(response);
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    @Test
    public void testLookupStockForItemsInStoresException() {
        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        final RuntimeException exception = new RuntimeException("test");
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + SERVICE_ENDPOINT), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(StockVisibilityResponse.class))).willThrow(exception);
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000_Pink_S");
        final List<String> stores = new ArrayList<>();
        stores.add("5317");
        stores.add("5352");
        final StockVisibilityItemLookupResponseDto actualResponse = stockVisibilityClient
                .lookupStockForItemsInStores(stores,
                        productCodes);
        Assert.assertNull(actualResponse);
    }

    @Test
    public void testLookupStockForItemsInStores() {

        final ArgumentCaptor<HttpEntity> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
        final ResponseEntity<StockVisibilityResponse> mockResponseEntity = mock(ResponseEntity.class);
        given(restTemplate.exchange(eq(WEBMETHODS_BASE_URL + SERVICE_ENDPOINT), eq(HttpMethod.POST),
                httpEntityCaptor.capture(), eq(StockVisibilityResponse.class))).willReturn(mockResponseEntity);
        final List<String> productCodes = new ArrayList<>();
        productCodes.add("P1000_Pink_S");
        final List<String> stores = new ArrayList<>();
        stores.add("5317");
        stores.add("5352");
        final StockVisibilityResponse response = populateWebmethodResponse();
        given(mockResponseEntity.getBody()).willReturn(response);
        final StockVisibilityItemLookupResponseDto actualResponse = stockVisibilityClient
                .lookupStockForItemsInStores(stores,
                        productCodes);
        Assert.assertNotNull(actualResponse);
        final List<ItemStockDto> list = response.getPayload().getItemLookupResponse().getItem();
        Assert.assertEquals(list.size(), actualResponse.getItems().size());
        Assert.assertEquals(list.get(0).getCode(), actualResponse.getItems().get(0).getCode());
        Assert.assertEquals(list.get(0).getSoh(), actualResponse.getItems().get(0).getSoh());
        Assert.assertEquals(list.get(0).getStoreNumber(), actualResponse.getItems().get(0).getStoreNumber());
        Assert.assertEquals(list.get(1).getCode(), actualResponse.getItems().get(1).getCode());
        Assert.assertEquals(list.get(1).getSoh(), actualResponse.getItems().get(1).getSoh());
        Assert.assertEquals(list.get(1).getStoreNumber(), actualResponse.getItems().get(1).getStoreNumber());
        Mockito.verify(restTemplate).exchange(WEBMETHODS_BASE_URL + SERVICE_ENDPOINT, HttpMethod.POST,
                httpEntityCaptor.getValue(), StockVisibilityResponse.class);
        Mockito.verifyNoMoreInteractions(restTemplate);
    }

    private StockVisibilityResponse populateWebmethodResponse() {
        final StockVisibilityResponse webmethodsResponse = new StockVisibilityResponse();
        webmethodsResponse.setResponseCode("0");
        webmethodsResponse.setResponseMessage("Success");
        final PayloadDto payload = new PayloadDto();
        final ItemLookUpResponseDto itemLookUpResponseDto = new ItemLookUpResponseDto();
        final List<ItemStockDto> list = new ArrayList<>();
        final ItemStockDto itemStockDto = new ItemStockDto();
        itemStockDto.setCode("P1000_Pink_S");
        itemStockDto.setSoh("95");
        itemStockDto.setStoreNumber("5317");
        final ItemStockDto itemStockDto1 = new ItemStockDto();
        itemStockDto1.setCode("P1000_Pink_S");
        itemStockDto1.setSoh("77");
        itemStockDto1.setStoreNumber("5352");
        list.add(itemStockDto);
        list.add(itemStockDto1);
        itemLookUpResponseDto.setItem(list);
        payload.setItemLookupResponse(itemLookUpResponseDto);
        webmethodsResponse.setPayload(payload);
        return webmethodsResponse;
    }
}

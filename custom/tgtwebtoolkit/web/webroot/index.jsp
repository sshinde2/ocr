<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<%--
		Choose the production or development css  
	--%>
	<c:choose>
		<c:when test="${param.env eq 'development'}">
			<link href="/tgtwebtoolkit/sample/build/development/css/app.css" rel="stylesheet" type="text/css" />
		</c:when>			
		<c:otherwise>
			<link href="/tgtwebtoolkit/sample/build/production/css/app.css" rel="stylesheet" type="text/css" />
		</c:otherwise>
	</c:choose>
	<body>
	
		<h1>tgtwebtoolkit extension for hybris</h1>
		Open the console to view network. Then try the <a href="${param.env ne 'development' ? '?env=development' : '?'}">${param.env ne 'development' ? 'development' : 'production'} view</a>.		
		
		<%--
			Choose the production or development javascript  
		--%>
		<c:choose>
			<c:when test="${param.env eq 'development'}">
				<script>
				var require = {
					baseUrl: "/tgtwebtoolkit/sample/src/js/",
					urlArgs: 'bust=' + (new Date()).getTime(),
					paths: {
				   	jquery: 'lib/jquery'
				   }
				};
				</script>
				<script src="/tgtwebtoolkit/sample/src/js/require.js" data-main="app"></script>
			</c:when>			
			<c:otherwise>
				<script>
				var require = {
					baseUrl: "/tgtwebtoolkit/sample/build/production/js/",
					//To get timely, correct error triggers in IE, force a define/shim exports check.
					enforceDefine: true,
				   paths: {
				       jquery: [
				           'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min',
				           //If the CDN location fails, load from this location
				           'lib/jquery'
				       ]
				   }					
				};
				</script>
				<script src="/tgtwebtoolkit/sample/build/production/js/app.js" data-main="app"></script>
			</c:otherwise>
		</c:choose>
	</body>
</html>
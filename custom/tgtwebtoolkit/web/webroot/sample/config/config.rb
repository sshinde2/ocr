# Require any additional compass plugins here.
require 'digest'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = (environment == :production) ? "build/production/css" : "build/development/css" 
sass_dir = "src/scss"
images_dir = "images"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = (environment == :production) ? :compressed : :compact

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false

is_prod = (environment == :production)

# Increment the deploy_version before every release to force cache busting.
asset_cache_buster do |http_path, real_path|
	if File.exists?(real_path) && is_prod
		md5 = Digest::MD5.hexdigest(real_path.read())
		{:path => http_path, :query => md5[1..8]}		
	end
end

asset_host do |asset|
	if is_prod	
		"http://assets%d.example.com" % (asset.hash % 4)
	end
end
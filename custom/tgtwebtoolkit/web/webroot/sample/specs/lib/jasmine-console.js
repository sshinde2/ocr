(function() {
    if (! jasmine) {
        throw new Exception("jasmine library does not exist in global namespace!");
    }

    /**
     * Basic reporter that outputs spec results to the browser console.
     * Useful if you need to test an html page and don't want the TrivialReporter
     * markup mucking things up.
     *
     * Usage:
     *
     * jasmine.getEnv().addReporter(new jasmine.ConsoleReporter());
     * jasmine.getEnv().execute();
     */
    var ConsoleReporter = function() {
        this.started = function() {
            this.output = [];
        };
        this.finished = function() {
            console.log( "[Specs]", this.output.join('') );
        };
        this.flush = function() {
            this.finished();
            this.started();
        }
    };

    ConsoleReporter.prototype = {
        reportRunnerResults: function(runner) {
            this.flush();
            var dur = (new Date()).getTime() - this.start_time;
            var failed = this.executed_specs - this.passed_specs;
            var spec_str = this.executed_specs + (this.executed_specs === 1 ? " spec, " : " specs, ");
            var fail_str = failed + (failed === 1 ? " failure in " : " failures in ");
            this.log(spec_str + fail_str + (dur/1000) + "s.");
            this.flush();
            this.showFailure();
            this.finished();
        },       

        reportRunnerStarting: function(runner) {
            this.started();
            this.failures = [];
            this.start_time = (new Date()).getTime();
            this.executed_specs = 0;
            this.passed_specs = 0;
            this.log("Jasmine Runner Started.")
            this.flush();            
        },

        reportSpecResults: function(spec) {
            if( ! spec.results().skipped) {
                var resultText = "X";

                if (spec.results().passed()) {
                    this.passed_specs++;
                    resultText = ".";
                } else {
                    this.failures.push(spec);
                }
                this.log(resultText);
            } else {
                this.log('_');
            }  
        },

        reportSpecStarting: function(spec) {
            this.executed_specs++;           
        },

        showFailure: function() {

            for (var j = 0; j < this.failures.length; j++) {
                var spec = this.failures[j];
                var resultItems = spec.results().getItems();

                this.log( (spec.results().passed() ? "PASS:" : "FAIL:" ) )
                this.line().line().pad();
                this.log( spec.getFullName() );
                this.line().pad();

                for (var i = 0; i < resultItems.length; i++) {
                    var result = resultItems[i];

                    if (result.type == 'expect' && result.passed && !result.passed()) {
                        this.log( result.message );
                        this.line().pad();

                        if( result.trace.stack ) {
                            this.log( result.trace.stack );
                        }
                    }
                }
                this.line();
                this.flush();
            }
        },

        log: function(str) {
            this.output.push(str);
            return this;
        },

        pad: function() {
            return this.log("  ");
        },

        line: function() {
            return this.log("\n");
        }
    };
  

    // export public
    jasmine.ConsoleReporter = ConsoleReporter;
})();

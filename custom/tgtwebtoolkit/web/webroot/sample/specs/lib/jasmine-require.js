define(['lib/lodash'], function(_) {

	var exports = {};
	
	/*
	 *  createRequireJsContext
	 */
	exports.createRequireJsContext = function(stubs) {

	  var map = {};
	
	  _.each(stubs, function (value, key) {
	    var stubname = 'stub' + key;
	
	    map[key] = stubname;
	  });
	
	
	  var context =  require.config({
	    context: Math.floor(Math.random() * 1000000),
	    baseUrl: '../src/js/',
		urlArgs: 'bust=' + (new Date()).getTime(),
	    map: {
	      "*": map
	    }
	  });
	
	  _.each(stubs, function (value, key) {
	    var stubname = 'stub' + key;
	
	    define(stubname, function () {
	      return value;
	    });
	
	  });
	
	  return context;
	
	};
	
	/*
	 *  beforeEachDependencyLoader
	 */
	exports.beforeEachDependencyInjector = function( deps, stubs ) {
		return function() {
			var that = this,
				flag = false;
			
			stubs = stubs || {};
			
			// Create the require context that will use the stubs
			var requireContext = exports.createRequireJsContext( stubs );
			
			requireContext(deps, function () {
				var args = arguments;
				
				_.each(deps, function(name, i){
					that[name] = args[i]
				});
				flag = true;
			});
			
			waitsFor(function() {
				return flag;
			});
		}
	}
	
	return exports;
	
});

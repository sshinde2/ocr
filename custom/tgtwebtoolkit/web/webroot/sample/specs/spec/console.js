define(['lib/jasmine-require'], function( jasmineRequireUtil ) {
		
	describe("console", function () {
		
		// Declare stubs.
		var stubs = {
			window: {}
		}		
		
		// Populate this.console
		beforeEach( jasmineRequireUtil.beforeEachDependencyInjector( ['console'], stubs ) );
		
		it('has all the console functions regardless of browser', function(){
			var console = this.console;
			expect(console.assert).toEqual(jasmine.any(Function));
			expect(console.clear).toEqual(jasmine.any(Function));
			expect(console.count).toEqual(jasmine.any(Function));
			expect(console.debug).toEqual(jasmine.any(Function));
			expect(console.dirxml).toEqual(jasmine.any(Function));
			expect(console.error).toEqual(jasmine.any(Function));
			expect(console.exception).toEqual(jasmine.any(Function));
			expect(console.group).toEqual(jasmine.any(Function));
			expect(console.groupCollapsed).toEqual(jasmine.any(Function));
			expect(console.groupEnd).toEqual(jasmine.any(Function));
			expect(console.info).toEqual(jasmine.any(Function));
			expect(console.log).toEqual(jasmine.any(Function));
			expect(console.markTimeline).toEqual(jasmine.any(Function));
			expect(console.profile).toEqual(jasmine.any(Function));
			expect(console.profileEnd).toEqual(jasmine.any(Function));
			expect(console.table).toEqual(jasmine.any(Function));
			expect(console.time).toEqual(jasmine.any(Function));
			expect(console.timeEnd).toEqual(jasmine.any(Function));
			expect(console.timeStamp).toEqual(jasmine.any(Function));
			expect(console.trace).toEqual(jasmine.any(Function));
			expect(console.warn).toEqual(jasmine.any(Function));
		});	
		
		
		afterEach(function(){
			delete this.console;
		});
					
	});

});
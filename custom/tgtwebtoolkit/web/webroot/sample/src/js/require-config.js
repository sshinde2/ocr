var require = {
	//To get timely, correct error triggers in IE, force a define/shim exports check.
	enforceDefine: true,
    paths: {
        jquery: [
            'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min',
            //If the CDN location fails, load from this location
            'lib/jquery-1.8.3.js'
        ]
    }
};
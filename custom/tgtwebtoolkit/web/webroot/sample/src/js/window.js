/*
 * AMD module wrapper for the global window object 
 */
define(function(){
 	return typeof window !== 'undefined' ? window : {};
});
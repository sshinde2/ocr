/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package au.com.target.tgtwishlist.constants;

/**
 * Global class for all Tgtwishlist constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("deprecation")
public final class TgtwishlistConstants extends GeneratedTgtwishlistConstants {
    public static final String EXTENSIONNAME = "tgtwishlist";

    private TgtwishlistConstants() {

    }


}

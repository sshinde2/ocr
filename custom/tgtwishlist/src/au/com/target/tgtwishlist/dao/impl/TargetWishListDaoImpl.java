/**
 *
 */
package au.com.target.tgtwishlist.dao.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.target.tgtwishlist.dao.TargetWishListDao;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author pthoma20
 *
 */
public class TargetWishListDaoImpl extends DefaultGenericDao<TargetWishListModel> implements TargetWishListDao {

    /**
     * 
     */
    private static final char CLOSING_BRACES = '}';
    /**
     * 
     */
    private static final char OPEN_BRACES = '{';

    /**
     *
     */
    public TargetWishListDaoImpl() {
        super(TargetWishListModel._TYPECODE);
    }

    /*
     * * (non-Javadoc)
     *
     * au.com.target.tgtwishlist.dao.TargetWishListDao#getWishListByUserAndType(de.hybris.platform.core.model.user.
     * UserModel, au.com.target.tgtwishlist.enums.WishListTypeEnum)
     */
    @Override
    public List<TargetWishListModel> getWishListByUserAndType(final UserModel user, final WishListTypeEnum type) {
        ServicesUtil.validateParameterNotNullStandardMessage("user", user);
        ServicesUtil.validateParameterNotNullStandardMessage("type", type);
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(TargetWishListModel.USER, user);
        params.put(TargetWishListModel.TYPE, type);
        final List<TargetWishListModel> targetWishListModelList = find(params);
        return targetWishListModelList;
    }

    /*
     * (non-Javadoc)
     *
     * @see au.com.target.tgtwishlist.dao.TargetWishListDao#getWishListByNameUserAndType(java.lang.String,
     * de.hybris.platform.core.model.user.UserModel, au.com.target.tgtwishlist.enums.WishListTypeEnum)
     */
    @Override
    public TargetWishListModel getWishListByNameUserAndType(final String name, final UserModel user,
            final WishListTypeEnum type) {
        ServicesUtil.validateParameterNotNullStandardMessage("user", user);
        ServicesUtil.validateParameterNotNullStandardMessage("type", type);
        ServicesUtil.validateParameterNotNullStandardMessage("name", name);
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put(TargetWishListModel.USER, user);
        params.put(TargetWishListModel.TYPE, type);
        params.put(TargetWishListModel.NAME, name);
        final List<TargetWishListModel> targetWishListModelList = find(params);
        final String inputParams = "User " + user.getPk() + " type=" + type.getCode() + " name=" + name;
        ServicesUtil.validateIfSingleResult(targetWishListModelList, "No WishList found for " + inputParams,
                "More than one wish list found for" + inputParams);
        return targetWishListModelList.get(0);
    }

    @Override
    public List<TargetWishListEntryModel> findOldestWishlistEntryForAWishList(final TargetWishListModel wishlist,
            final int noOfEntries) {
        final StringBuilder query = new StringBuilder(
                "SELECT {pk} FROM {" + TargetWishListEntryModel._TYPECODE + "} WHERE {");
        query.append("wishlist} = ?wishlist order by {addedDate} ");
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query.toString());
        fQuery.addQueryParameter("wishlist", wishlist);
        fQuery.setCount(noOfEntries);
        final SearchResult<TargetWishListEntryModel> result = getFlexibleSearchService().search(fQuery);
        final List<TargetWishListEntryModel> entries = result.getResult();
        return entries;
    }

    @Override
    public List<TargetWishListEntryModel> findWishListEntriesWithVariant(final List<String> variantCodes) {
        final StringBuilder query = new StringBuilder("SELECT {pk} FROM").append(OPEN_BRACES)
                .append(TargetWishListEntryModel._TYPECODE).append(CLOSING_BRACES).append("WHERE").append(OPEN_BRACES)
                .append(TargetWishListEntryModel.SELECTEDVARIANT).append(CLOSING_BRACES).append("IN (?")
                .append(TargetWishListEntryModel.SELECTEDVARIANT + ")");
        final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query.toString());
        flexibleSearchQuery.addQueryParameter(TargetWishListEntryModel.SELECTEDVARIANT, variantCodes);
        final SearchResult<TargetWishListEntryModel> searchResult = getFlexibleSearchService()
                .search(flexibleSearchQuery);
        return searchResult.getResult();
    }

}

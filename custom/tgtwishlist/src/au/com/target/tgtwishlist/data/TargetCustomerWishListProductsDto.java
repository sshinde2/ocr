/**
 * 
 */
package au.com.target.tgtwishlist.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author rsamuel3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class TargetCustomerWishListProductsDto {
    private String baseCode;
    private String variantCode;
    private String name;
    private String imageURL;
    private String productURL;
    private String colour;
    private String size;
    private String price;

    /**
     * @return the baseCode
     */
    public String getBaseCode() {
        return baseCode;
    }

    /**
     * @param baseCode
     *            the baseCode to set
     */
    public void setBaseCode(final String baseCode) {
        this.baseCode = baseCode;
    }

    /**
     * @return the variantCode
     */
    public String getVariantCode() {
        return variantCode;
    }

    /**
     * @param variantCode
     *            the variantCode to set
     */
    public void setVariantCode(final String variantCode) {
        this.variantCode = variantCode;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the imageURL
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * @param imageURL
     *            the imageURL to set
     */
    public void setImageURL(final String imageURL) {
        this.imageURL = imageURL;
    }

    /**
     * @return the productURL
     */
    public String getProductURL() {
        return productURL;
    }

    /**
     * @param productURL
     *            the productURL to set
     */
    public void setProductURL(final String productURL) {
        this.productURL = productURL;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }

    /**
     * @param colour
     *            the colour to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final String price) {
        this.price = price;
    }


    @Override
    public String toString() {
        final StringBuilder product = new StringBuilder();
        product.append("{baseCode=").append(getBaseCode()).append(" ,variantCode=").append(getVariantCode())
                .append(" ,name=").append(getName()).append(" ,imageURL=").append(getImageURL())
                .append(" ,productURL=").append(getProductURL()).append(" ,colour=").append(getColour())
                .append(" ,size=").append(getSize()).append(" ,price=").append(getPrice()).append("}");
        return product.toString();
    }
}

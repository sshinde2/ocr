/**
 *
 */
package au.com.target.tgtwishlist.exception;

import de.hybris.platform.servicelayer.exceptions.SystemException;


/**
 * @author pthoma20
 *
 */
public class AddToWishListException extends SystemException {
    /**
     * @param message
     *            Meaningful message to accompany the exception
     */
    public AddToWishListException(final String message) {
        super(message);
    }

    /**
     * @param exception
     *            Exception to be wrapped
     */
    public AddToWishListException(final Exception exception) {
        super(exception);
    }

    /**
     * @param message
     *            Meaningful message to accompany the exception
     * @param exception
     *            Exception to be wrapped
     */
    public AddToWishListException(final String message, final Exception exception) {
        super(message, exception);
    }
}

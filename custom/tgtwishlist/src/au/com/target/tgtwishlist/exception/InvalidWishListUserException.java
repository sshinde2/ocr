/**
 *
 */
package au.com.target.tgtwishlist.exception;

import de.hybris.platform.servicelayer.exceptions.SystemException;


/**
 * @author pthoma20
 *
 */
public class InvalidWishListUserException extends SystemException {
    /**
     * @param message
     *            Meaningful message to accompany the exception
     */
    public InvalidWishListUserException(final String message) {
        super(message);
    }

    /**
     * @param exception
     *            Exception to be wrapped
     */
    public InvalidWishListUserException(final Exception exception) {
        super(exception);
    }

    /**
     * @param message
     *            Meaningful message to accompany the exception
     * @param exception
     *            Exception to be wrapped
     */
    public InvalidWishListUserException(final String message, final Exception exception) {
        super(message, exception);
    }
}

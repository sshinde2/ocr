/**
 * 
 */
package au.com.target.tgtwishlist.logger;

import de.hybris.platform.commerceservices.enums.SalesApplication;

import java.text.MessageFormat;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import au.com.target.tgtcore.integration.dto.TargetIntegrationErrorDto;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;


/**
 * @author pthoma20
 *
 */
public class TgtWishListLogger {

    /**
     * 
     */
    private static final String SAVE_ALL_WISH_LIST = "saveAllWishList";
    public static final String LOG_PREFIX = "WISHLIST : ";
    /**
     * 
     */
    private static final String COMMA = ", ";

    /**
     * 
     */
    private static final String ACTION = "action=";

    public static final String FAVOURITES_LIST_NOT_FOUND = "No Favourites List Found :";

    public static final String NO_FAVOURITES_LIST_NO_UPDATES = "No Favourites List Found or no items to remove :";

    public static final String REMOVE_FAV_LIST_ACTION = "removefromWishlist";

    public static final String ADD_FAV_LIST_ACTION = "addToWishList";

    public static final String UPDATE_FAV_LIST_ACTION = "updateWishList";

    public static final String SYNC_FAV_LIST_ACTION = "syncWishList";

    public static final String SHARE_FAV_LIST_ACTION = "shareWishList";

    public static final String RETRIEVE_LIST_ACTION = "retrieveListAction";

    public static final String REPLACE_LIST_ACTION = "replaceWishListWithLatest";

    public static final String INVALID_WISHLIST_USER = LOG_PREFIX
            + "action={0}, Current User with userId={1} is not eligible to perform this action on list. ";
    /**
     * 
     */

    private static final String SUCCESS_LOG = LOG_PREFIX
            + "action={0}, code= {1}, type= {2}, userId= {3}, for a salesApplication= {4}";


    private static final String USER_ID = " userId=";
    private static final String WISHLIST_TYPE = "type=";
    private static final String WISHLIST_CODE = "code=";
    private static final String WISHLIST_NAME = "name=";
    private static final String WISHLIST_ENTRY_SELECTED_VARIANT = "selectedVariant=";
    private static final String WISHLIST_ENTRY_PRODUCT_CODE = "baseProductCode=";
    private static final String SALES_APPLICATION = "salesApplication=";
    private static final String ADD_TO_LIST_SUCCESS = " Added to Wish List :";
    public static final String DUPLICATE_WISH_LIST_ENTRIES = "Duplicate Wish List Entries exist for same product :";
    public static final String INVALID_WISHLIST_ENTRY_INSTANCE = "WishListEntry not an instance of TargetWishListEntryModel :";
    public static final String INVALID_BASE_PRODUCT_INSTANCE = "Base Product Code not an instance of  Target Product Model :";
    public static final String BASE_PRODUCT_NOT_FOUND = "Product Cannot Be Found/Ambiguous product Found";
    public static final String ADD_TO_LIST_FAILED = " Add to list failed";
    public static final String UNEXPECTED_SYNCH_LIST_ERROR = "Unexpected Error while Synching list for current user :";
    public static final String UNEXPECTED_REMOVE_FAV_LIST_ERROR = "Unexpected Error while removing favourties for current user :";

    /**
     * Log Add to List
     * 
     * @param logger
     * @param wishListcode
     * @param userId
     * @param wishListType
     * @param productCode
     * @param wishListSelectedVariant
     * @param salesApplication
     */
    public static void logAddToList(final Logger logger, final String action, final String wishListcode,
            final String userId, final WishListTypeEnum wishListType, final String productCode,
            final String wishListSelectedVariant,
            final SalesApplication salesApplication) {
        logger.info(LOG_PREFIX + ACTION + action + COMMA + ADD_TO_LIST_SUCCESS + COMMA + WISHLIST_CODE
                + wishListcode + COMMA + WISHLIST_TYPE
                + wishListType + COMMA + USER_ID + userId + COMMA + WISHLIST_ENTRY_PRODUCT_CODE + productCode + COMMA
                + WISHLIST_ENTRY_SELECTED_VARIANT + wishListSelectedVariant + COMMA + SALES_APPLICATION
                + salesApplication);
    }

    public static void logSuccess(final Logger logger, final String action, final String wishListcode,
            final String userId, final WishListTypeEnum wishListType,
            final SalesApplication salesApplication) {
        logger.info(MessageFormat.format(SUCCESS_LOG, action, wishListcode, wishListType.getCode(), userId,
                salesApplication));
    }

    /**
     * Log a warning when favourites List is not found
     * 
     * @param logger
     * @param action
     * @param listName
     * @param userId
     * @param wishListType
     */
    public static void logFavouritesListNotFound(final Logger logger, final String action, final String listName,
            final String userId,
            final WishListTypeEnum wishListType) {
        logger.warn(LOG_PREFIX + action + WISHLIST_NAME + listName + COMMA
                + USER_ID + userId + COMMA + WISHLIST_TYPE + wishListType);
    }

    /**
     * Log error when the base Product Code is invalid.
     * 
     * @param logger
     * @param message
     * @param productCode
     */
    public static void logInvalidBaseProductCode(final Logger logger, final String action, final String message,
            final String productCode) {
        logger.error(LOG_PREFIX + ACTION + action + COMMA + message + ": " + WISHLIST_ENTRY_PRODUCT_CODE
                + productCode);
    }

    /**
     * Log an erro when the base product code is not found.
     * 
     * @param logger
     * @param productCode
     * @param ex
     */
    public static void logBaseProductNotFound(final Logger logger, final String action, final String productCode,
            final Exception ex) {
        logger.error(LOG_PREFIX + ACTION + action + COMMA + BASE_PRODUCT_NOT_FOUND + COMMA
                + WISHLIST_ENTRY_PRODUCT_CODE
                + productCode, ex);
    }

    /**
     * Log an error when Duplicate wish List Entry for a base product is found.
     * 
     * @param logger
     * @param wishListcode
     * @param productCode
     */
    public static void logDuplicateWishListEntries(final Logger logger, final String wishListcode,
            final String productCode) {
        logger.error(LOG_PREFIX + DUPLICATE_WISH_LIST_ENTRIES + ":" + WISHLIST_CODE + wishListcode + COMMA
                + WISHLIST_ENTRY_PRODUCT_CODE
                + productCode);
    }

    /**
     * Log an error when the wish list entry is not of the type TargetWishListEntry
     * 
     * @param logger
     * @param wishListcode
     * @param productCode
     */
    public static void logWishListEntryNotOfTypeTargetWishListEntryModel(final Logger logger, final String action,
            final String wishListcode,
            final String productCode) {
        logger.error(LOG_PREFIX + ACTION + action + COMMA + INVALID_WISHLIST_ENTRY_INSTANCE + ":" + WISHLIST_CODE
                + wishListcode + COMMA
                + WISHLIST_ENTRY_PRODUCT_CODE
                + productCode);
    }

    /**
     * Log an error when the add to list fails as a part of the synch.
     * 
     * @param logger
     * @param wishListcode
     * @param productCode
     * @param selectedVariant
     * @param ex
     */
    public static void logAddToListFailureSynch(final Logger logger, final String action, final String wishListcode,
            final String productCode, final String selectedVariant, final Exception ex) {
        logger.error(LOG_PREFIX + ACTION + action + COMMA + ADD_TO_LIST_FAILED + ": " + WISHLIST_CODE + wishListcode
                + COMMA + WISHLIST_ENTRY_PRODUCT_CODE + productCode + COMMA + WISHLIST_ENTRY_SELECTED_VARIANT
                + selectedVariant, ex);
    }

    /**
     * Log an error when add to list is invoked for a non eligible user
     * 
     * @param logger
     * @param userId
     */
    public static void logInvalidWishListUser(final Logger logger, final String action, final String userId) {
        logger.error(MessageFormat.format(INVALID_WISHLIST_USER, action, userId));
    }

    /**
     * Log an error when synching of favourites list fails.
     * 
     * @param logger
     * @param error
     * @param userId
     */
    public static void logFavListUnExpectedError(final Logger logger, final String action, final String error,
            final String userId) {
        logger.error(LOG_PREFIX + ACTION + action + COMMA + error + USER_ID + userId);
    }

    /**
     * Log an error when synching of favourites list fails.
     * 
     * @param logger
     * @param userId
     * @param error
     * @param exception
     */
    public static void logFavListUnExpectedError(final Logger logger, final String action, final String error,
            final String userId,
            final Exception exception) {
        logger.error(LOG_PREFIX + ACTION + action + COMMA + error + USER_ID + userId, exception);
    }

    /**
     * Log an error when sharing a wishlist fails.
     * 
     * @param logger
     * @param responseCode
     */
    public static void logWishListShareFailure(final Logger logger, final String responseCode,
            final List<TargetIntegrationErrorDto> errors) {
        final StringBuilder errorMessage = new StringBuilder();
        if (errors != null) {
            for (final TargetIntegrationErrorDto error : errors) {
                errorMessage.append(error.getErrorCode()).append(" - Message:").append(error.getErrorMessage());
            }
        }
        logger.error(LOG_PREFIX + ACTION + SHARE_FAV_LIST_ACTION + ", RespnoseCode: " + responseCode
                + ", ErrorMessage:" + errorMessage);
    }

    public static void logWishListsSaveAllFailure(final Logger logger, final List<String> wishListPKs,
            final Exception exception) {
        logger.error(
                LOG_PREFIX + ACTION + SAVE_ALL_WISH_LIST + "with WishList PKs : "
                        + StringUtils.join(wishListPKs, COMMA),
                exception);
    }

    public static void logWishListsReplaceAllFailure(final Logger logger, final String wishListPK,
            final Exception exception) {
        logger.error(
                LOG_PREFIX + ACTION + REPLACE_LIST_ACTION + "with WishList PK : "
                        + wishListPK,
                exception);
    }
}

/**
 *
 */
package au.com.target.tgtwishlist.service;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.wishlist2.Wishlist2Service;

import java.util.List;

import au.com.target.tgtwishlist.data.TargetWishListEntryData;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author pthoma20
 *
 */
public interface TargetWishListService extends Wishlist2Service {
    /**
     * This will return the targetWishListModel based on the listname, wishlist type and the user. If the list type is
     * favourite the listname which will get be defaulted to Favourites.
     *
     * @param listName
     * @param wishListType
     * @param user
     * @return targetWishListModel
     */
    TargetWishListModel retrieveWishList(String listName, UserModel user, WishListTypeEnum wishListType);

    /**
     * This will add the product to the wishList if the product is not there in the wishlist. If it is already present,
     * it will update the wishlist with the product if the addedTimeInMillis is greater than the existing entry
     *
     * @param targetWishListModel
     * @param targetWishListDataEntry
     * @param action
     * @param salesApplication
     * @return targetWishListModel
     */
    public TargetWishListModel addProductToWishList(final TargetWishListModel targetWishListModel,
            final TargetWishListEntryData targetWishListDataEntry, final String action,
            SalesApplication salesApplication);


    /**
     * This method will verify whether the list exists - if yes, it will retrieve that list if not then will go ahead
     * and create the list. If the list type is favourite the listname which will get be defaulted to Favourites.
     *
     * @param listName
     * @param userModel
     * @param wishListType
     * @return targetWishListModel
     */
    TargetWishListModel verifyAndCreateList(String listName, UserModel userModel, WishListTypeEnum wishListType);


    /**
     * This method is used to sync the favourites from the users store to the server to ensure that we can display the
     * updated list on any device used by the user
     *
     * @param userModel
     * @param wishlists
     * @param salesApplication
     * @return TargetWishListModel
     */
    TargetWishListModel syncWishListForUser(UserModel userModel, List<TargetWishListEntryData> wishlists,
            SalesApplication salesApplication);

    /**
     * This method is used to remove the favourites for the users from the list
     *
     * @param userModel
     * @param wishlistEntries
     * @param salesApplication
     * @return TargetWishListModel
     */
    TargetWishListModel removeWishListEntriesForUser(UserModel userModel,
            List<TargetWishListEntryData> wishlistEntries,
            SalesApplication salesApplication);

    /**
     * This method will call the webmethods client to send the wish list. It will return the sucess/failure status.
     * 
     * @param targetWishListSendRequestDto
     */
    boolean shareWishList(TargetWishListSendRequestDto targetWishListSendRequestDto);

    /**
     * Find the list of wishlists that have the selected list of variant codes
     * 
     * @param variantCodes
     * @return List of wishlistModel with the variant Code
     */
    List<TargetWishListEntryModel> getAllWishListsWithVariantCode(List<String> variantCodes);

    /**
     * update the wishlists saved by customers if the variants have moved to a new Base product
     * 
     * @param newBaseProductCode
     * @param variantCodes
     * 
     */
    void updateWishListsWithNewBaseProductCode(String newBaseProductCode, List<String> variantCodes);

    /**
     * 
     * @param userModel
     * @param wishlists
     * @param salesApplication
     */
    void replaceFavouritesWithLatest(UserModel userModel, List<TargetWishListEntryData> wishlists,
            SalesApplication salesApplication);

}

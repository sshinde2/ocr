/**
 *
 */
package au.com.target.tgtwishlist.service.impl;

import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.wishlist2.impl.DefaultWishlist2Service;
import de.hybris.platform.wishlist2.model.Wishlist2EntryModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.integration.dto.TargetIntegrationResponseDto;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwishlist.dao.TargetWishListDao;
import au.com.target.tgtwishlist.data.TargetWishListEntryData;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.enums.WishListVisibilityEnum;
import au.com.target.tgtwishlist.exception.AddToWishListException;
import au.com.target.tgtwishlist.logger.TgtWishListLogger;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;
import au.com.target.tgtwishlist.model.TargetWishListModel;
import au.com.target.tgtwishlist.service.TargetWishListService;
import au.com.target.tgtwishlist.sharewishlist.client.ShareWishListClient;


/**
 * @author pthoma20
 *
 */
public class TargetWishListServiceImpl extends DefaultWishlist2Service implements TargetWishListService {

    protected static final String FAVOURITE_LIST_NAME = "Favourites";

    protected static final Logger LOG = Logger.getLogger(TargetWishListServiceImpl.class);

    private TargetWishListDao targetWishListDao;

    private TargetProductService targetProductService;

    private Integer maxListSize;

    private ShareWishListClient shareWishListClient;


    @Override
    public TargetWishListModel retrieveWishList(String listName, final UserModel user,
            final WishListTypeEnum wishListType) {

        try {
            listName = getListName(listName, wishListType);
            return targetWishListDao.getWishListByNameUserAndType(listName, user,
                    wishListType);
        }
        catch (final UnknownIdentifierException ex) {
            TgtWishListLogger.logFavouritesListNotFound(LOG, TgtWishListLogger.FAVOURITES_LIST_NOT_FOUND, listName,
                    String.valueOf(user.getPk()), wishListType);
            return null;
        }
    }

    @Override
    public TargetWishListModel addProductToWishList(final TargetWishListModel targetWishListModel,
            final TargetWishListEntryData targetWishListDataEntry, final String action,
            final SalesApplication salesApplication) {
        final Date addedDate = getAddedDate(targetWishListDataEntry.getTimeStamp());

        final TargetProductModel productModel = getProductForCode(targetWishListDataEntry.getBaseProductCode(),
                action);

        final Wishlist2EntryModel wishListEntry2Model = getWishlistEntryForProduct(targetWishListModel, productModel);

        //if product is not found then it will create the wish list entry with the product.
        if (wishListEntry2Model == null) {
            removeOldestWishListEntryIfLimitExceeded(targetWishListModel);
            createWishListEntry(productModel, targetWishListDataEntry.getSelectedVariantCode(), addedDate,
                    targetWishListModel);
        }
        //if the product is of the type target wish entry model update the product.
        else if (wishListEntry2Model instanceof TargetWishListEntryModel) {
            final TargetWishListEntryModel existingEntry = (TargetWishListEntryModel)wishListEntry2Model;
            updateWishListEntryIfNeeded(existingEntry, targetWishListDataEntry.getSelectedVariantCode(), addedDate);
        }
        else {

            TgtWishListLogger.logWishListEntryNotOfTypeTargetWishListEntryModel(LOG, action,
                    targetWishListModel.getCode(),
                    productModel.getCode());
            throw new AddToWishListException(TgtWishListLogger.INVALID_WISHLIST_ENTRY_INSTANCE);
        }

        getModelService().refresh(targetWishListModel);
        final String userId = targetWishListModel.getUser() != null
                ? String.valueOf(targetWishListModel.getUser().getPk()) : null;

        TgtWishListLogger.logAddToList(LOG, action, targetWishListModel.getCode(), userId,
                targetWishListModel.getType(),
                productModel.getCode(), targetWishListDataEntry.getSelectedVariantCode(), salesApplication);

        return targetWishListModel;
    }

    private TargetProductModel getProductForCode(final String baseProductCode, final String action) {
        ProductModel productModel;
        try {
            productModel = targetProductService.getProductForCode(baseProductCode);
            if (!(productModel instanceof TargetProductModel)) {

                TgtWishListLogger.logInvalidBaseProductCode(LOG, action,
                        TgtWishListLogger.INVALID_BASE_PRODUCT_INSTANCE,
                        baseProductCode);
                throw new AddToWishListException(TgtWishListLogger.INVALID_BASE_PRODUCT_INSTANCE);
            }
        }
        catch (final UnknownIdentifierException | AmbiguousIdentifierException ex) {

            TgtWishListLogger.logBaseProductNotFound(LOG, action, baseProductCode, ex);
            throw new AddToWishListException(TgtWishListLogger.BASE_PRODUCT_NOT_FOUND, ex);
        }
        return (TargetProductModel)productModel;
    }


    private Wishlist2EntryModel getWishlistEntryForProduct(final TargetWishListModel targetWishListModel,
            final TargetProductModel productModel) {
        Wishlist2EntryModel wishListEntry2Model = null;
        try {
            wishListEntry2Model = getWishlistEntryForProduct(productModel, targetWishListModel);
        }
        catch (final UnknownIdentifierException exception) {
            wishListEntry2Model = null;
        }
        catch (final AmbiguousIdentifierException ex) {

            TgtWishListLogger.logDuplicateWishListEntries(LOG,
                    targetWishListModel.getCode(), productModel.getCode());

            throw new AddToWishListException(TgtWishListLogger.DUPLICATE_WISH_LIST_ENTRIES);
        }
        return wishListEntry2Model;
    }

    private void removeOldestWishListEntryIfLimitExceeded(final TargetWishListModel targetWishListModel) {
        if (CollectionUtils.isEmpty(targetWishListModel.getEntries())
                || targetWishListModel.getEntries().size() < maxListSize.intValue()) {
            return;
        }
        final int noOfEntries = targetWishListModel.getEntries().size() - maxListSize.intValue() + 1;
        final List<TargetWishListEntryModel> targetWishListEntryModelList = targetWishListDao
                .findOldestWishlistEntryForAWishList(targetWishListModel, noOfEntries);

        for (final TargetWishListEntryModel targetWishListEntryModel : targetWishListEntryModelList) {
            getModelService().remove(targetWishListEntryModel);
        }
        getModelService().refresh(targetWishListModel);
    }


    private TargetWishListModel createWishList(final String listName, final UserModel userModel,
            final WishListTypeEnum wishListType) {
        final TargetWishListModel targetWishListModel = getModelService().create(TargetWishListModel.class);
        targetWishListModel.setName(getListName(listName, wishListType));
        targetWishListModel.setType(wishListType);
        targetWishListModel.setVisibility(WishListVisibilityEnum.PRIVATE);
        targetWishListModel.setUser(userModel);
        getModelService().save(targetWishListModel);
        return targetWishListModel;
    }



    @Override
    public TargetWishListModel verifyAndCreateList(final String listName, final UserModel userModel,
            final WishListTypeEnum wishListType) {

        TargetWishListModel targetWishListModel = retrieveWishList(listName, userModel, wishListType);
        if (targetWishListModel == null) {
            targetWishListModel = createWishList(listName, userModel, wishListType);
        }
        return targetWishListModel;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * au.com.target.tgtwishlist.service.TargetWishListService#syncWishListForUser(de.hybris.platform.core.model.user
     * .UserModel, java.util.List)
     */
    @Override
    public TargetWishListModel syncWishListForUser(final UserModel userModel,
            final List<TargetWishListEntryData> wishlists, final SalesApplication salesApplication) {
        Assert.notNull(userModel, "User cannot be null");

        final TargetWishListModel targetWishListModel = verifyAndCreateList(StringUtils.EMPTY, userModel,
                WishListTypeEnum.FAVOURITE);

        if (CollectionUtils.isEmpty(wishlists)) {
            return targetWishListModel;
        }
        else {
            for (final TargetWishListEntryData wishList : wishlists) {
                final String selectedVariantCode = wishList.getSelectedVariantCode();
                try {
                    addProductToWishList(targetWishListModel, wishList, TgtWishListLogger.SYNC_FAV_LIST_ACTION,
                            salesApplication);
                }
                catch (final AddToWishListException ex) {
                    TgtWishListLogger.logAddToListFailureSynch(LOG, TgtWishListLogger.SYNC_FAV_LIST_ACTION,
                            targetWishListModel.getCode(),
                            wishList.getBaseProductCode(), selectedVariantCode, ex);
                }
            }
        }
        return targetWishListModel;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwishlist.service.TargetWishListService#removeWishListForUser(de.hybris.platform.core.model.user.UserModel, java.util.List, de.hybris.platform.commerceservices.enums.SalesApplication)
     */
    @Override
    public TargetWishListModel removeWishListEntriesForUser(final UserModel userModel,
            final List<TargetWishListEntryData> wishlistEntries,
            final SalesApplication salesApplication) {

        final TargetWishListModel targetWishListModel = retrieveWishList(StringUtils.EMPTY, userModel,
                WishListTypeEnum.FAVOURITE);

        if (targetWishListModel == null || CollectionUtils.isEmpty(wishlistEntries)) {
            TgtWishListLogger.logFavouritesListNotFound(LOG, TgtWishListLogger.NO_FAVOURITES_LIST_NO_UPDATES,
                    StringUtils.EMPTY, String.valueOf(userModel.getPk()), WishListTypeEnum.FAVOURITE);
            return targetWishListModel;
        }

        for (final TargetWishListEntryData targetWishlistEntryData : wishlistEntries) {
            try {
                final TargetProductModel productModel = getProductForCode(targetWishlistEntryData.getBaseProductCode(),
                        TgtWishListLogger.REMOVE_FAV_LIST_ACTION);
                removeWishlistEntryForProduct(productModel, targetWishListModel);
            }
            catch (final Exception e) {
                TgtWishListLogger.logFavListUnExpectedError(LOG, TgtWishListLogger.REMOVE_FAV_LIST_ACTION,
                        TgtWishListLogger.UNEXPECTED_REMOVE_FAV_LIST_ERROR,
                        String.valueOf(userModel.getPk()), e);
            }
        }

        getModelService().refresh(targetWishListModel);

        TgtWishListLogger.logSuccess(LOG, TgtWishListLogger.REMOVE_FAV_LIST_ACTION, targetWishListModel.getCode(),
                String.valueOf(userModel.getPk()), WishListTypeEnum.FAVOURITE, salesApplication);

        return targetWishListModel;
    }

    private Date getAddedDate(final String addedTimeInMillis) {
        final Date addedDate = TargetDateUtil.getDateForTimeInMillis(addedTimeInMillis);
        if (null == addedDate) {
            return new Date();
        }
        return addedDate;
    }


    private void createWishListEntry(final ProductModel productModel, final String sellableVariant,
            final Date addedDate,
            final TargetWishListModel targetWishListModel) {
        final TargetWishListEntryModel wishListEntryModel = createWishListEntryModel(productModel, sellableVariant,
                addedDate, targetWishListModel);
        getModelService().save(wishListEntryModel);
    }

    /**
     * @param productModel
     * @param sellableVariant
     * @param addedDate
     * @param targetWishListModel
     * @return wishListEntryModel
     */
    private TargetWishListEntryModel createWishListEntryModel(final ProductModel productModel,
            final String sellableVariant, final Date addedDate, final TargetWishListModel targetWishListModel) {
        final TargetWishListEntryModel wishListEntryModel = getModelService().create(TargetWishListEntryModel.class);
        wishListEntryModel.setProduct(productModel);
        wishListEntryModel.setSelectedVariant(sellableVariant);
        wishListEntryModel.setAddedDate(addedDate);
        wishListEntryModel.setWishlist(targetWishListModel);
        return wishListEntryModel;
    }

    private void updateWishListEntryIfNeeded(final TargetWishListEntryModel existingEntry,
            final String selectedVariant,
            final Date addedDate) {
        //if the existing entry is newer than the add to list product call then ignore it else update the existing entry
        if (existingEntry.getAddedDate().compareTo(addedDate) < 1) {
            existingEntry.setAddedDate(addedDate);
            existingEntry.setSelectedVariant(selectedVariant);
            getModelService().save(existingEntry);
        }
    }

    private String getListName(final String listName, final WishListTypeEnum wishListType) {
        if (wishListType.equals(WishListTypeEnum.FAVOURITE)) {
            return FAVOURITE_LIST_NAME;
        }
        return listName;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwishlist.service.TargetWishListService#shareWishListForUser(au.com.target.tgtwishlist.data.TargetWishListSendRequestDto)
     */
    @Override
    public boolean shareWishList(final TargetWishListSendRequestDto targetWishListSendRequestDto) {

        final TargetIntegrationResponseDto targetIntegrationResponseDto = shareWishListClient
                .shareWishList(targetWishListSendRequestDto);
        if (targetIntegrationResponseDto == null) {
            TgtWishListLogger.logFavListUnExpectedError(LOG, TgtWishListLogger.SHARE_FAV_LIST_ACTION,
                    "Response from webmethods is null", StringUtils.EMPTY);
            return false;
        }
        else if (targetIntegrationResponseDto.isSuccess()) {
            return true;
        }

        TgtWishListLogger.logWishListShareFailure(LOG, targetIntegrationResponseDto.getResponseCode(),
                targetIntegrationResponseDto.getErrorList());
        return false;
    }

    @Override
    public List<TargetWishListEntryModel> getAllWishListsWithVariantCode(final List<String> variantCodes) {
        return targetWishListDao.findWishListEntriesWithVariant(variantCodes);
    }

    @Override
    public void updateWishListsWithNewBaseProductCode(final String newBaseProductCode,
            final List<String> variantCodes) {
        final List<TargetWishListEntryModel> wishlistToChange = getAllWishListsWithVariantCode(variantCodes);
        final List<String> wishlistToChangePKs = new ArrayList<String>();
        try {
            if (CollectionUtils.isNotEmpty(wishlistToChange)) {
                for (final TargetWishListEntryModel wishListEntry : wishlistToChange) {
                    wishlistToChangePKs.add(wishListEntry.getPk().getLongValueAsString());
                    wishListEntry.setProduct(targetProductService.getProductForCode(newBaseProductCode));
                }
                getModelService().saveAll(wishlistToChange);
            }
        }
        catch (final ModelSavingException mse) {
            TgtWishListLogger.logWishListsSaveAllFailure(LOG, wishlistToChangePKs, mse);
        }

    }

    /**
     * @param targetWishListDao
     *            the targetWishListDao to set
     */
    @Required
    public void setTargetWishListDao(final TargetWishListDao targetWishListDao) {
        this.targetWishListDao = targetWishListDao;
    }

    /**
     * @param targetProductService
     *            the targetProductService to set
     */
    @Required
    public void setTargetProductService(final TargetProductService targetProductService) {
        this.targetProductService = targetProductService;
    }

    /**
     * @param maxListSize
     *            the maxListSize to set
     */
    @Required
    public void setMaxListSize(final Integer maxListSize) {
        this.maxListSize = maxListSize;
    }

    /**
     * @param shareWishListClient
     *            the shareWishListClient to set
     */
    @Required
    public void setShareWishListClient(final ShareWishListClient shareWishListClient) {
        this.shareWishListClient = shareWishListClient;
    }

    @Override
    public void replaceFavouritesWithLatest(final UserModel userModel,
            final List<TargetWishListEntryData> wishlistEntries,
            final SalesApplication salesApplication) {
        final TargetWishListModel targetWishListModel = retrieveWishList(StringUtils.EMPTY, userModel,
                WishListTypeEnum.FAVOURITE);
        if (targetWishListModel == null) {
            TgtWishListLogger.logFavouritesListNotFound(LOG, TgtWishListLogger.NO_FAVOURITES_LIST_NO_UPDATES,
                    StringUtils.EMPTY, String.valueOf(userModel.getPk()), WishListTypeEnum.FAVOURITE);
            return;
        }
        final List<TargetWishListEntryModel> targetWishListEntryModels = new ArrayList<>();
        final List<Wishlist2EntryModel> wishlist2EntryModels = targetWishListModel.getEntries();
        getModelService().removeAll(wishlist2EntryModels);
        try {
            for (final TargetWishListEntryData wishList : wishlistEntries) {
                final String selectedVariantCode = wishList.getSelectedVariantCode();
                final Date addedDate = getAddedDate(wishList.getTimeStamp());
                final TargetProductModel productModel = getProductForCode(wishList.getBaseProductCode(),
                        TgtWishListLogger.REPLACE_LIST_ACTION);
                final TargetWishListEntryModel targetWishListEntryModel = createWishListEntryModel(productModel,
                        selectedVariantCode, addedDate, targetWishListModel);
                targetWishListEntryModels
                        .add(targetWishListEntryModel);
            }
            getModelService().saveAll(targetWishListEntryModels);
        }
        catch (final ModelSavingException ex) {
            TgtWishListLogger.logWishListsReplaceAllFailure(LOG, targetWishListModel.getPk().getLongValueAsString(),
                    ex);
        }
    }
}

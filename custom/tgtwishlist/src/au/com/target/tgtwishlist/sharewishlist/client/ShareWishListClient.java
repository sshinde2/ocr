/**
 * 
 */
package au.com.target.tgtwishlist.sharewishlist.client;

import au.com.target.tgtcore.integration.dto.TargetIntegrationResponseDto;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;


/**
 * Interface to send the wish list to friends
 * 
 * @author rsamuel3
 *
 */
public interface ShareWishListClient {

    /**
     * share the wishlist to a friend
     * 
     * @param request
     * @return TargetWishListSendResponseDto
     */
    TargetIntegrationResponseDto shareWishList(TargetWishListSendRequestDto request);
}

/**
 * 
 */
package au.com.target.tgtwishlist.sharewishlist.client.impl;

import au.com.target.tgtcore.integration.dto.TargetIntegrationResponseDto;
import au.com.target.tgtwishlist.data.TargetWishListSendRequestDto;
import au.com.target.tgtwishlist.sharewishlist.client.ShareWishListClient;


/**
 * @author rsamuel3
 *
 */
public class DefaultShareWishListClientImpl implements ShareWishListClient {

    /* (non-Javadoc)
     * @see au.com.target.tgtwishlist.ShareWishListClient.ShareWishListClient#sendWishLists(au.com.target.tgtwishlist.data.TargetWishListSendRequestDto)
     */
    @Override
    public TargetIntegrationResponseDto shareWishList(final TargetWishListSendRequestDto request) {
        return null;
    }

}

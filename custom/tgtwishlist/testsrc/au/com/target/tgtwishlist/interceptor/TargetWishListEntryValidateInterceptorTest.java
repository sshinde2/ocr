/**
 *
 */
package au.com.target.tgtwishlist.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtwishlist.interceptor.TargetWishListEntryValidateInterceptor.ErrorMessages;
import au.com.target.tgtwishlist.model.TargetWishListEntryModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class TargetWishListEntryValidateInterceptorTest {
    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    @InjectMocks
    private final TargetWishListEntryValidateInterceptor wishListEntryValidateInterceptor = new TargetWishListEntryValidateInterceptor();


    @Test
    public void testProductNotPresent() throws InterceptorException {
        final TargetWishListEntryModel mockModel = Mockito.mock(TargetWishListEntryModel.class);
        try {
            wishListEntryValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX + ErrorMessages.WISHLIST_ENTRY_VALIDATION_ERROR
                                    + ErrorMessages.PRODUCT_NULL);
        }
    }

    @Test
    public void testProductNotOnlineVersion() throws InterceptorException {
        final TargetWishListEntryModel mockWishListEntryModel = Mockito.mock(TargetWishListEntryModel.class);
        final TargetProductModel targetProductModel = Mockito.mock(TargetProductModel.class);
        final CatalogVersionModel onlineCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        final CatalogVersionModel stagedCatalogVersion = Mockito.mock(CatalogVersionModel.class);
        final CatalogVersionService catalogVersionService = Mockito.mock(CatalogVersionService.class);
        BDDMockito.given(onlineCatalogVersion.getPk()).willReturn(PK.fromLong(1111));
        BDDMockito.given(stagedCatalogVersion.getPk()).willReturn(PK.fromLong(2222));
        wishListEntryValidateInterceptor.setCatalogVersionService(catalogVersionService);
        BDDMockito.given(
                catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.ONLINE_VERSION))
                .willReturn(onlineCatalogVersion);
        BDDMockito.given(mockWishListEntryModel.getProduct()).willReturn(targetProductModel);
        BDDMockito.given(targetProductModel.getCatalogVersion()).willReturn(stagedCatalogVersion);
        try {
            wishListEntryValidateInterceptor.onValidate(mockWishListEntryModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.WISHLIST_ENTRY_VALIDATION_ERROR
                            + ErrorMessages.PRODUCT_SHOULD_BE_ONLINE_VERSION);
        }
    }


}

/**
 *
 */
package au.com.target.tgtwishlist.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.fest.assertions.Assertions;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.model.TargetWishListModel;


/**
 * @author pthoma20
 *
 */
@UnitTest
public class TargetWishListPrepareInterceptorTest {

    private final TargetWishListPrepareInterceptor prepareInterceptor = new TargetWishListPrepareInterceptor();

    @Test
    public void testPrepareInterceptorWhenCodeNotPresent() throws InterceptorException {
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        final UserModel userModel = Mockito.mock(TargetCustomerModel.class);
        targetWishListModel.setUser(userModel);
        final PK userPk = PK.fromLong(1111);
        BDDMockito.given(userModel.getPk()).willReturn(userPk);
        targetWishListModel.setType(WishListTypeEnum.FAVOURITE);
        targetWishListModel.setName("listName");
        prepareInterceptor.onPrepare(targetWishListModel, null);
        Assertions.assertThat(targetWishListModel.getCode()).isEqualTo(userPk + "-listName-FAVOURITE");


    }

    @Test
    public void testPrepareInterceptorWhenCodePresent() throws InterceptorException {
        final TargetWishListModel targetWishListModel = new TargetWishListModel();
        targetWishListModel.setCode("abcd");
        prepareInterceptor.onPrepare(targetWishListModel, null);
        Assertions.assertThat(targetWishListModel.getCode()).isEqualTo("abcd");
    }

}

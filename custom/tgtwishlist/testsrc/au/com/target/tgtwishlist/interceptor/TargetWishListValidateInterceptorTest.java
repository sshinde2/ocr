/**
 *
 */
package au.com.target.tgtwishlist.interceptor;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.util.Arrays;

import org.fest.assertions.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtwishlist.enums.WishListTypeEnum;
import au.com.target.tgtwishlist.interceptor.TargetWishListValidateInterceptor.ErrorMessages;
import au.com.target.tgtwishlist.model.TargetWishListModel;



/**
 * @author pthoma20
 *
 */
@UnitTest
public class TargetWishListValidateInterceptorTest {

    private static final String EXCEPTION_MESSAGE_PREFIX = "[null]:";

    @InjectMocks
    private final TargetWishListValidateInterceptor wishListValidateInterceptor = new TargetWishListValidateInterceptor();


    @Test
    public void testUserNotPresent() throws InterceptorException {
        final TargetWishListModel mockModel = Mockito.mock(TargetWishListModel.class);
        try {
            wishListValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX + ErrorMessages.WISHLIST_VALIDATION_ERROR
                                    + ErrorMessages.USER_IS_REQUIRED);
        }
    }


    @Test
    public void testWishListUserToBeTargetCustomer() throws InterceptorException {
        final TargetWishListModel mockModel = Mockito.mock(TargetWishListModel.class);
        final UserModel userMockModel = Mockito.mock(UserModel.class);
        BDDMockito.given(mockModel.getUser()).willReturn(userMockModel);
        try {
            wishListValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.WISHLIST_VALIDATION_ERROR
                            + ErrorMessages.USER_SHOULD_BE_TARGETCUSTOMER);
        }
    }

    @Test
    public void testTypeIsRequired() throws InterceptorException {
        final TargetWishListModel mockModel = Mockito.mock(TargetWishListModel.class);
        final TargetCustomerModel targetCustomerMockModel = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(mockModel.getUser()).willReturn(targetCustomerMockModel);
        try {
            wishListValidateInterceptor.onValidate(mockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage())
                    .isEqualTo(
                            EXCEPTION_MESSAGE_PREFIX + ErrorMessages.WISHLIST_VALIDATION_ERROR
                                    + ErrorMessages.TYPE_IS_REQUIRED);
        }
    }

    @Test
    public void testOnlyOneFavouritesListForUser() throws InterceptorException {
        final TargetWishListModel wishListMockModel = Mockito.mock(TargetWishListModel.class);
        final TargetCustomerModel targetCustomerMockModel = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(wishListMockModel.getUser()).willReturn(targetCustomerMockModel);
        final PK wishListPk = PK.fromLong(111);
        BDDMockito.given(wishListMockModel.getPk()).willReturn(wishListPk);
        BDDMockito.given(wishListMockModel.getType()).willReturn(WishListTypeEnum.FAVOURITE);

        final TargetWishListModel duplicateFavouriteWishListModel = Mockito.mock(TargetWishListModel.class);
        BDDMockito.given(duplicateFavouriteWishListModel.getType()).willReturn(WishListTypeEnum.FAVOURITE);
        final PK duplicateWishListPk = PK.fromLong(222);
        BDDMockito.given(duplicateFavouriteWishListModel.getPk()).willReturn(duplicateWishListPk);
        BDDMockito.given(targetCustomerMockModel.getWishlist())
                .willReturn(Arrays.asList((Wishlist2Model)duplicateFavouriteWishListModel));

        try {
            wishListValidateInterceptor.onValidate(wishListMockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.WISHLIST_VALIDATION_ERROR
                            + ErrorMessages.FAVOURITES_LIST_ALREADY_EXIST_FOR_USER);
        }
    }

    @Test
    public void testVisibilityOfFavouritesList() throws InterceptorException {
        final TargetWishListModel wishListMockModel = Mockito.mock(TargetWishListModel.class);
        final TargetCustomerModel targetCustomerMockModel = Mockito.mock(TargetCustomerModel.class);
        BDDMockito.given(wishListMockModel.getUser()).willReturn(targetCustomerMockModel);
        BDDMockito.given(wishListMockModel.getType()).willReturn(WishListTypeEnum.FAVOURITE);
        try {
            wishListValidateInterceptor.onValidate(wishListMockModel, null);
            Assert.fail("Expected InterceptorException to be thrown.");
        }
        catch (final InterceptorException ex) {
            Assertions.assertThat(ex.getMessage()).isEqualTo(
                    EXCEPTION_MESSAGE_PREFIX + ErrorMessages.WISHLIST_VALIDATION_ERROR
                            + ErrorMessages.FAVOURITES_LIST_NON_PRIVATE_VALIDATION_ERROR);
        }
    }

}

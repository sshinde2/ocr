package au.com.target.tgtws.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;


/**
 * @author ayushman
 *
 */
public class CurrencyConversionResponseLoggingAspect {
    public static final String ERROR_MESSAGE_FORMAT = "ERROR Message: [ %s ],   Class: [ %s ], Exception Class: [ %s]";
    public static final String LOG_FORMAT_SUCCESS = "Response: [ %s ], Class Name: [ %s]";
    private static final Logger LOG = Logger.getLogger(CurrencyConversionResponseLoggingAspect.class);
    private boolean isResponseError = false;

    /**
     * After call for Currency Factor throw an exception, this method will log down the error message in specific format
     * 
     * @param joinPoint
     * @param error
     */
    public void afterThrowing(final JoinPoint joinPoint, final Throwable error) {
        isResponseError = true;
        if (error instanceof Exception) {
            final LogInfo logInfo = getLogDetails(joinPoint, error);
            LOG.info(String.format(ERROR_MESSAGE_FORMAT, error.getMessage(), logInfo.action,
                    logInfo.exceptionClass));
        }

    }

    /**
     * 
     * @param joinPoint
     */
    public void afterAction(final JoinPoint joinPoint) {
        final LogInfo logInfo = getLogDetails(joinPoint, null);
        if (Boolean.FALSE.equals(Boolean.valueOf(isResponseError))) {
            LOG.info(String.format(LOG_FORMAT_SUCCESS, "Succesfully got response for Currency Conversion Factor",
                    logInfo.action));
        }
    }

    /**
     * @param joinPoint
     * @return LogInfo
     */
    private LogInfo getLogDetails(final JoinPoint joinPoint, final Throwable error) {
        final LogInfo logInfo = new LogInfo();

        final Object target = joinPoint.getTarget();
        if (target != null) {
            logInfo.action = target.getClass().getSimpleName();
        }
        if (error != null) {
            logInfo.exceptionClass = error.getClass().getSimpleName();
        }
        return logInfo;
    }

    private class LogInfo {

        private String action = "n/a";
        private String exceptionClass = "n/a";
    }

}
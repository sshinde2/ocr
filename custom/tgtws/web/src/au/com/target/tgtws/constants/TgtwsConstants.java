/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtws.constants;

/**
 * Global class for all CommerceWebServices constants. You can add global constants for your extension into this class.
 */
public final class TgtwsConstants
{
    public static final String EXTENSIONNAME = "tgtws";

    // implement here constants used by this extension
    public static final String CONTINUE_URL = "session_continue_url";
    public static final String CONTINUE_URL_PAGE = "session_continue_url_page";
    public static final String OPTIONS_SEPARATOR = ",";

    public static final String HTTP_REQUEST_PARAM_LANGUAGE = "lang";
    public static final String HTTP_REQUEST_PARAM_CURRENCY = "curr";

    private TgtwsConstants() {
        // prevent construction
    }
}

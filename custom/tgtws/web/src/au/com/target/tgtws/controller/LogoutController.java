/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtws.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * @author KKW
 * 
 */
@Controller
public class LogoutController extends BaseController
{

    /**
     * Spring security logout successful redirect.
     */
    @RequestMapping(value = "/v1/logoutsuccessful", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK, reason = "Log out successful.")
    public void logoutSuccessful()
    {
        //NOPMD
    }
}

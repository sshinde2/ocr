/**
 * 
 */
package au.com.target.tgtws.controller.activation;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtws.controller.BaseController;
import au.com.target.tgtwsfacades.activation.ActivationIntegrationFacade;
import au.com.target.tgtwsfacades.integration.dto.IntegrationActivationDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * @author mmaki
 * 
 */

@Controller
@RequestMapping(value = "/v1/{baseSiteId}/activationapi")
public class ActivationController extends BaseController {
    private static final Logger LOG = Logger.getLogger(ActivationController.class);

    @Autowired
    private ActivationIntegrationFacade activationIntegrationFacade;

    /**
     * 
     * @param code
     * @param dto
     * @return IntegrationResponseDto entity with result of the persist operation
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/activationservice/{code}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto updateProductActivation(@PathVariable final String code,
            @RequestBody final IntegrationActivationDto dto) {

        if (code == null || !code.equals(dto.getVariantCode())) {
            final IntegrationResponseDto response = new IntegrationResponseDto(code);
            response.setSuccessStatus(false);
            response.addMessage("Requested product code is different to the one supplied with data payload.");
            return response;
        }

        LOG.info("Updating approvalStatus with productCode: " + code);
        return activationIntegrationFacade.updateApprovalStatus(dto);
    }
}

/**
 * 
 */
package au.com.target.tgtws.controller.cnc;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtjms.util.JmsMessageHelper;
import au.com.target.tgtutility.logging.JsonLogger;
import au.com.target.tgtws.controller.BaseController;
import au.com.target.tgtwsfacades.cnc.ClickAndCollectNotificationFacade;
import au.com.target.tgtwsfacades.integration.dto.ClickAndCollectNotificationRequestDto;
import au.com.target.tgtwsfacades.integration.dto.ClickAndCollectNotificationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationClickAndCollectNotificationDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationClickAndCollectNotificationResponseDto;


/**
 * @author rmcalave
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/orders/cnc")
public class ClickAndCollectNotificationController extends BaseController {

    private static final Logger LOG = Logger.getLogger(ClickAndCollectNotificationController.class);

    @Autowired
    private ClickAndCollectNotificationFacade clickAndCollectNotificationFacade;

    @ResponseBody
    @Secured("ROLE_WM_MODIFY_ORDERGROUP")
    @RequestMapping(value = "notify", method = RequestMethod.PUT)
    public IntegrationClickAndCollectNotificationResponseDto notifyCncAvailability(
            @RequestBody final IntegrationClickAndCollectNotificationDto notificationDto) {

        JsonLogger.logJson(LOG, notificationDto);
        final IntegrationClickAndCollectNotificationResponseDto responseDto = new IntegrationClickAndCollectNotificationResponseDto();

        final String posLaybyNumber = getPosLaybyNumber(notificationDto.getLaybyNumber());
        final String storeNumber = notificationDto.getStoreNumber();
        final String letterType = notificationDto.getLetterType();

        final boolean cncNotificationTriggered = clickAndCollectNotificationFacade.triggerCNCNotification(
                posLaybyNumber, storeNumber, letterType);

        responseDto.setSuccess(cncNotificationTriggered);

        return responseDto;
    }

    @Secured("ROLE_WM_MODIFY_ORDERGROUP")
    @RequestMapping(value = "pickedup", method = RequestMethod.PUT)
    @ResponseBody
    public ClickAndCollectNotificationResponseDto notifyCncPickedup(
            @RequestBody final ClickAndCollectNotificationRequestDto notificationDto) {

        JsonLogger.logJson(LOG, notificationDto);
        return clickAndCollectNotificationFacade.notifyCncPickedUp(getPosLaybyNumber(notificationDto.getOrderNumber()),
                notificationDto.getStoreNumber());
    }

    @Secured("ROLE_WM_MODIFY_ORDERGROUP")
    @RequestMapping(value = "returnedtofloor", method = RequestMethod.PUT)
    @ResponseBody
    public ClickAndCollectNotificationResponseDto notifyCncReturnedToFloor(
            @RequestBody final ClickAndCollectNotificationRequestDto notificationDto) {

        JsonLogger.logJson(LOG, notificationDto);
        return clickAndCollectNotificationFacade.notifyCncReturnedToFloor(
                getPosLaybyNumber(notificationDto.getOrderNumber()), notificationDto.getStoreNumber());
    }


    /**
     * Gets the pos layby number.
     *
     * @param laybyNumber
     * @return the pos layby number
     */
    protected String getPosLaybyNumber(final String laybyNumber) {
        return JmsMessageHelper.getLaybyNumForHybris(laybyNumber);
    }

}

/**
 * 
 */
package au.com.target.tgtws.controller.fastline;

import java.text.MessageFormat;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode;
import au.com.target.tgtutility.logging.JaxbLogger;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.PickConfirmOrder;
import au.com.target.tgtwsfacades.pickconfirm.PickConfirmIntegrationFacade;


/**
 * @author mmaki
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/pickconfirmapi")
public class PickConfirmController {

    protected static final String PICK_CONF_FAILURE_MESSAGE = "Requested order number is different to the one supplied with data payload.";

    private static final Logger LOG = Logger.getLogger(PickConfirmController.class);

    @Autowired
    private PickConfirmIntegrationFacade pickConfirmIntegrationFacade;

    /**
     * Updates the order for a pick confirm from fastline
     * 
     * @param dto
     * @return List of IntegrationResponses to the ones that have failed
     */
    @Secured("ROLE_ESBGROUPONLINE")
    @RequestMapping(value = "/pickconfirm/{code}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto updatePickConfirm(@PathVariable final String code,
            @RequestBody final PickConfirmOrder dto) {

        JaxbLogger.logXml(LOG, dto);

        LOG.info(MessageFormat.format("INFO-FASTLINE-PICKCONFIRM Pick confirm for order {0}", code));

        if (code == null || !code.equals(dto.getOrderNumber())) {
            final IntegrationResponseDto response = new IntegrationResponseDto(code);
            response.setSuccessStatus(false);
            response.addMessage(PICK_CONF_FAILURE_MESSAGE);
            LOG.error(SplunkLogFormatter.formatOrderMessage(
                    "Order number provided as a URL parameter differ from the one provided in message payload. "
                            + "URL parameter: " + code + ", message payload: " + dto.getOrderNumber(),
                    ErrorCode.ERR_TGTFULLFILLMENT, code));
            return response;
        }

        return pickConfirmIntegrationFacade.handlePickConfirm(dto);
    }
}

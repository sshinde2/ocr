/**
 * 
 */
package au.com.target.tgtws.controller.fastline;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtutility.logging.JaxbLogger;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.ShipConfirm;
import au.com.target.tgtwsfacades.shipconfirm.ShipConfirmIntegrationFacade;


/**
 * @author Olivier Lamy
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/shipconfirmapi")
public class ShipConfirmController {

    private static final Logger LOG = Logger.getLogger(ShipConfirmController.class);

    @Autowired
    private ShipConfirmIntegrationFacade shipConfirmIntegrationFacade;

    /**
     * 
     * 
     * @param shipConfirm
     * @return IntegrationResponseDto
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/shipconfirm", method = RequestMethod.PUT)
    @ResponseBody
    public List<IntegrationResponseDto> shipConfirm(@RequestBody final ShipConfirm shipConfirm) {

        JaxbLogger.logXml(LOG, shipConfirm);

        LOG.info("INFO-FASTLINE-SHIPCONFIRM Ship confirm order received");
        return shipConfirmIntegrationFacade.handleShipConfirm(shipConfirm);
    }
}

/**
 * 
 */
package au.com.target.tgtws.controller.fluent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtfluent.data.Order;
import au.com.target.tgtws.controller.BaseController;


/**
 * @author fkhan4
 *
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/orders")
public class FluentOrderUpdateController extends BaseController {

    private static final Logger LOG = Logger.getLogger(FluentOrderUpdateController.class);

    @Autowired
    private FluentOrderService fluentOrderService;

    @Secured("ROLE_APIGATEWAYGROUP")
    @RequestMapping(value = "/update/{eventId}", method = RequestMethod.PUT)
    public ResponseEntity updateOrder(@PathVariable final String eventId, @RequestBody final Order order) {
        LOG.info("FLUENT_ORDER_UPDATE eventId=" + eventId + ", FLUENT_ORDERID=" + order.getOrderId()
                + ", HYBRIS_ORDERID="
                + order.getOrderRef()
                + ", ORDER_STATUS=" + order.getStatus());
        try {
            fluentOrderService.updateOrder(order);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (final FluentOrderException e) {
            LOG.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        catch (final FluentErrorException e) {
            LOG.info(e.getMessage());
            return new ResponseEntity<>(HttpStatus.FAILED_DEPENDENCY);
        }
    }
}

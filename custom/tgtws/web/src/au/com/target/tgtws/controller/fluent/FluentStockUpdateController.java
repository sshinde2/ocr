/**
 * 
 */
package au.com.target.tgtws.controller.fluent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtws.controller.BaseController;
import au.com.target.tgtwsfacades.fluent.ProductUpdateFacade;


/**
 * @author mmaki
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/fluentstockupdateapi")
public class FluentStockUpdateController extends BaseController {

    @Autowired
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Autowired
    private ProductUpdateFacade productUpdateFacade;

    /**
     * Set onlineDate for applicable products
     * 
     */
    @Secured("ROLE_ESBGROUPPRICE")
    @RequestMapping(value = "/setonlinedate", method = RequestMethod.PUT)
    @ResponseBody
    public void setOnlineDate() {
        if (!targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)) {
            return;
        }
        productUpdateFacade.setOnlineDateForApplicableProducts();
    }
}

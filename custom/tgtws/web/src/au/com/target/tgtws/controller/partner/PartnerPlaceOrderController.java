/**
 * 
 */
package au.com.target.tgtws.controller.partner;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtebay.dto.EbayTargetOrderDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderResponseDTO;
import au.com.target.tgtebay.dto.EbayTargetOrderResponsesDTO;
import au.com.target.tgtebay.dto.EbayTargetOrdersDTO;
import au.com.target.tgtebay.order.TargetPartnerOrderService;
import au.com.target.tgtutility.logging.JaxbLogger;
import au.com.target.tgtws.controller.BaseController;


/**
 * Service to place partner orders in hybris.
 * 
 * @author jjayawa1
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/orders/eBay")
public class PartnerPlaceOrderController extends BaseController {
    private static final Logger LOG = Logger.getLogger(PartnerPlaceOrderController.class);

    private static final String SUCCESS = "SUCCESS";
    private static final String FAILED = "FAILED";

    private static final String ERROR_MESSAGE_CREATE_ORDER_FAILED = "FAILED : Create partner order partnerOrderNumber={0}, salesApplication={1}, createdDate={2}";
    private static final String INFO_MESSAGE_CREATE_ORDER_INPROGRESS = "INPROGRESS : Create partner order partnerOrderNumber={0}, salesApplication={1}, createdDate={2}";
    private static final String INFO_MESSAGE_CREATE_ORDER_SUCCESS = "SUCCESS : Created partner order partnerOrderNumber={0}, hybrisOrderNumber={1}, createdDate={2}";

    @Resource(name = "targetPartnerOrderService")
    private TargetPartnerOrderService targetPartnerOrderService;

    /**
     * Service method to create an order.
     * 
     * @param orders
     * @return EbayOrderResponsesDTO
     * @throws JAXBException
     */
    @Secured("ROLE_WM_MODIFY_ORDERGROUP")
    @RequestMapping(value = "createOrder", method = RequestMethod.PUT)
    @ResponseBody
    public EbayTargetOrderResponsesDTO createPartnerOrder(@RequestBody final EbayTargetOrdersDTO orders)
            throws JAXBException, ParseException {

        JaxbLogger.logXml(LOG, orders);

        final EbayTargetOrderResponsesDTO ebayOrderCreationResponsesDTO = new EbayTargetOrderResponsesDTO();
        final List<EbayTargetOrderResponseDTO> ebayOrderCreationResponses = new ArrayList<>();

        for (final EbayTargetOrderDTO order : orders.geteBayOrders()) {
            LOG.info(MessageFormat.format(INFO_MESSAGE_CREATE_ORDER_INPROGRESS, order.geteBayOrderNumber(),
                    order.getSalesChannel(), order.getCreatedDate()));

            final EbayTargetOrderResponseDTO ebayOrderCreationResponse = new EbayTargetOrderResponseDTO();
            try {
                final String orderNumber = targetPartnerOrderService.createPartnerOrders(order);
                if (StringUtils.isNotEmpty(orderNumber)) {
                    ebayOrderCreationResponse.setOrderNumber(orderNumber);
                    ebayOrderCreationResponse.setOrderStatus(SUCCESS);
                    ebayOrderCreationResponses.add(ebayOrderCreationResponse);
                    LOG.info(MessageFormat.format(INFO_MESSAGE_CREATE_ORDER_SUCCESS, order.geteBayOrderNumber(),
                            orderNumber, order.getCreatedDate()));
                }
                else {
                    ebayOrderCreationResponse.setOrderStatus(FAILED);
                    ebayOrderCreationResponses.add(ebayOrderCreationResponse);
                    LOG.error("PartnerOrderImport: " + MessageFormat.format(ERROR_MESSAGE_CREATE_ORDER_FAILED,
                            order.geteBayOrderNumber(),
                            order.getSalesChannel(), order.getCreatedDate()));
                }
            }
            catch (final Exception e) {
                ebayOrderCreationResponse.setOrderStatus(FAILED);
                ebayOrderCreationResponses.add(ebayOrderCreationResponse);
                LOG.error(
                        "PartnerOrderImport: "
                                + MessageFormat.format(ERROR_MESSAGE_CREATE_ORDER_FAILED, order.geteBayOrderNumber(),
                                        order.getSalesChannel(), order.getCreatedDate()) + " " + e.getMessage(), e);
            }
        }

        ebayOrderCreationResponsesDTO.setOrderResponses(ebayOrderCreationResponses);
        return ebayOrderCreationResponsesDTO;
    }

}

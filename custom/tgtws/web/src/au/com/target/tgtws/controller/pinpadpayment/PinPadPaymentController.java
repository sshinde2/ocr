/**
 * 
 */
package au.com.target.tgtws.controller.pinpadpayment;

import java.text.MessageFormat;
import java.util.Collections;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtshareddto.eftwrapper.dto.EftPaymentDto;
import au.com.target.tgtshareddto.eftwrapper.dto.HybrisResponseDto;
import au.com.target.tgtutility.constants.TgtutilityConstants;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwsfacades.pinpad.PinPadPaymentFacade;


/**
 * @author rsamuel3
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/pinpadpayment")
public class PinPadPaymentController {

    private static final Logger LOG = Logger.getLogger(PinPadPaymentController.class);
    /**
     * 
     */
    private static final String INVALID_PAYLOAD = "Payment details are empty or the requested code {0} is different to the one supplied with data payload: {1}";

    @Autowired
    private PinPadPaymentFacade pinPadPaymentFacade;

    /**
     * sample { "eftPayment" : { "orderId" : "00019001", "paymentDetails" : { "authCode" : "098987345",
     * "journalRoll":"This is the journal Roll", "pan" : "5424234324234324", "rrn" : "234", "respAscii": "0",
     * "transactionSuccess" : "true", "cardType" : "5", "cardName" : "visa", "accountType" : "2", "journal" :
     * "This is the journal" } } }
     * 
     * @param orderId
     * @param paymentDto
     * @return responseDto with a success of true if everything went well and false otherwise
     */
    @Secured("ROLE_KIOSKGROUP")
    @RequestMapping(value = "/updatePayment/{orderId}", method = RequestMethod.PUT)
    @ResponseBody
    public HybrisResponseDto updatePinPadPayment(@PathVariable final String orderId,
            @RequestBody final EftPaymentDto paymentDto) {
        final HybrisResponseDto responseDto = new HybrisResponseDto(orderId);
        if (paymentDto == null || !StringUtils.equalsIgnoreCase(orderId, paymentDto.getOrderId())) {
            String orderIdpayload = null;
            if (paymentDto != null) {
                orderIdpayload = paymentDto.getOrderId();
            }
            responseDto.setSuccessStatus(false);
            final String message = SplunkLogFormatter
                    .formatMessage(MessageFormat.format(
                            INVALID_PAYLOAD, orderId, orderIdpayload),
                            TgtutilityConstants.ErrorCode.ERR_PINPADPAYMENT);
            LOG.error(message);
            responseDto.setMessages(Collections.singletonList(message));
            return responseDto;
        }
        LOG.info("Updating the payment details from PinPad for the order :" + orderId);
        return pinPadPaymentFacade.updatePinPadPayment(paymentDto);
    }
}

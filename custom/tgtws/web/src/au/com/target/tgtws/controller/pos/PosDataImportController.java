/**
 * 
 */
package au.com.target.tgtws.controller.pos;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtsale.integration.dto.IntegrationPosProductItemDto;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductsDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.pos.PosPriceImportIntegrationFacade;


/**
 * @author fkratoch
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/posdataimportapi")
public class PosDataImportController {
    private static final Logger LOG = LoggerFactory.getLogger(PosDataImportController.class);

    @Autowired
    private PosPriceImportIntegrationFacade posPriceImportIntegrationFacade;

    @Secured("ROLE_ESBGROUPPRICE")
    @RequestMapping(value = "/priceimport", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto putPosPriceData(@RequestBody final IntegrationPosProductsDto dtos) {

        if (dtos == null || CollectionUtils.isEmpty(dtos.getProducts())) {
            final IntegrationResponseDto response = new IntegrationResponseDto("");
            response.setSuccessStatus(false);
            response.addMessage("INFO-PRICEUPDATE-EMPTYLIST - The object was null or the list was empty. ");
            return response;
        }
        final List<IntegrationPosProductItemDto> products = dtos.getProducts();

        LOG.info("Importing POS pricing information for {} products", Integer.valueOf(products.size()));
        return posPriceImportIntegrationFacade.importProductPriceFromPos(products);
    }
}

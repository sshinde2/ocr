/**
 * 
 */
package au.com.target.tgtws.controller.productimport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtwsfacades.deals.TargetDealCategoryImportFacade;
import au.com.target.tgtwsfacades.deals.dto.TargetDealCategoryDTO;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationTargetProductCategoryDto;
import au.com.target.tgtwsfacades.productimport.CategoryImportIntegrationFacade;


/**
 * @author rmcalave
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/categoryimportapi")
public class CategoryImportController {

    @Autowired
    private CategoryImportIntegrationFacade categoryImportIntegrationFacade;

    @Autowired
    private TargetDealCategoryImportFacade targetDealCategoryImportFacade;

    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/categoryimport/{categoryCode}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto putTargetProductCategory(@PathVariable final String categoryCode,
            @RequestBody final IntegrationTargetProductCategoryDto dto) {

        if (!dto.getCode().equalsIgnoreCase(categoryCode)) {
            final IntegrationResponseDto response = new IntegrationResponseDto(
                    dto.getCode());
            response.setSuccessStatus(false);
            response.addMessage(categoryCode
                    + " - Requested category code is different to the one supplied with data payload: "
                    + dto.getCode());
            return response;
        }

        return categoryImportIntegrationFacade.persistTargetCategory(dto);
    }

    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/dealcategory/{categoryCode}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto putTargetDealCategory(@PathVariable final String categoryCode,
            @RequestBody final TargetDealCategoryDTO dto) {

        if (!dto.getCode().equalsIgnoreCase(categoryCode)) {
            final IntegrationResponseDto response = new IntegrationResponseDto(dto.getCode());
            response.setSuccessStatus(false);
            response.addMessage(categoryCode
                    + " - Requested deal category code is different to the one supplied with data payload: "
                    + dto.getCode());
            return response;
        }

        return targetDealCategoryImportFacade.persistTargetDealCategory(dto);
    }
}

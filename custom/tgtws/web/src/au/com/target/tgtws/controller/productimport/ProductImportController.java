/**
 * 
 */
package au.com.target.tgtws.controller.productimport;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferenceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferencesDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.productimport.ProductImportIntegrationFacade;


/**
 * @author fkratoch
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/productimportapi")
public class ProductImportController {
    private static final Logger LOG = Logger.getLogger(ProductImportController.class);

    @Autowired
    private ProductImportIntegrationFacade productImportIntegrationFacade;

    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/productimport/{productCode}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto putTargetProduct(@PathVariable final String productCode,
            @RequestBody final IntegrationProductDto dto) {

        if (!dto.getProductCode().equalsIgnoreCase(productCode)) {
            final IntegrationResponseDto response = new IntegrationResponseDto(
                    dto.getProductCode());
            response.setSuccessStatus(false);
            response.addMessage(productCode
                    + " - Requested product code is different to the one supplied with data payload: "
                    + dto.getProductCode());
            return response;
        }

        LOG.info("Persisting details of TargetProduct with productCode: " + dto.getVariantCode()
                + " for base product: " + productCode);
        return productImportIntegrationFacade.persistTargetProduct(dto);
    }

    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/productreferences", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto putTargetProductReferences(@RequestBody final IntegrationProductReferencesDto dto) {

        LOG.info("Creating product references");
        return productImportIntegrationFacade.generateProductCrossReferences(dto);
    }

    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/productreference", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto putTargetProductReference(@RequestBody final IntegrationProductReferenceDto dto) {

        LOG.info("Creating product references");
        return productImportIntegrationFacade.generateProductCrossReference(dto);
    }
}

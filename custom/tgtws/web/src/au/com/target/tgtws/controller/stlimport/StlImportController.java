package au.com.target.tgtws.controller.stlimport;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationShopTheLookDto;
import au.com.target.tgtwsfacades.stlimport.ShopTheLookImportIntegrationFacade;


/**
 * Exposes API to import Shop the Look data.
 * 
 * @author mgazal
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/stlimportapi")
public class StlImportController {

    private static final Logger LOG = Logger.getLogger(StlImportController.class);

    @Autowired
    private ShopTheLookImportIntegrationFacade shopTheLookImportIntegrationFacade;

    @Secured("ROLE_ESBGROUPSTL")
    @RequestMapping(value = "/stlimport/{code}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto putStl(@PathVariable final String code,
            @RequestBody final IntegrationShopTheLookDto dto) {
        if (!dto.getCode().equalsIgnoreCase(code)) {
            final IntegrationResponseDto response = new IntegrationResponseDto(dto.getCode());
            response.setSuccessStatus(false);
            response.addMessage(
                    code + " - Requested shop the look code is different to the one supplied with data payload: "
                            + dto.getCode());
            return response;
        }

        LOG.info("Persisting details of TargetLookCollection with code: " + dto.getCode());
        return shopTheLookImportIntegrationFacade.persistTargetShopTheLook(dto);
    }
}

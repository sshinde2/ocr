/**
 * 
 */
package au.com.target.tgtws.controller.stockupdate;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtebay.stockupdate.StockUpdateToPartnerFacade;
import au.com.target.tgtws.controller.BaseController;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateResponseDto;
import au.com.target.tgtwsfacades.integration.dto.StockUpdate;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateProductDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateResponseDto;
import au.com.target.tgtwsfacades.stockupdate.StockUpdateIntegrationFacade;


/**
 * @author mmaki
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/stockupdateapi")
public class StockUpdateController extends BaseController {

    private static final Logger LOG = Logger.getLogger(StockUpdateController.class);

    @Autowired
    private StockUpdateIntegrationFacade stockUpdateIntegrationFacade;
    @Autowired
    private StockUpdateToPartnerFacade stockUpdateToPartnerFacade;

    /**
     * Updates the availability of the product
     * 
     * @param productCode
     * @param dto
     * @return IntegrationStockUpdateResponseDto entity with result of the update operation
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetstockupdate/{productCode}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationStockUpdateResponseDto updateStockLevel(@PathVariable final String productCode,
            @RequestBody final IntegrationStockUpdateDto dto) {

        IntegrationStockUpdateResponseDto response = null;

        if (productCode == null || !(productCode.equals(dto.getItemCode())
                || productCode.equals(dto.getItemPrimaryBarcode()))) {
            response = new IntegrationStockUpdateResponseDto();
            response.setItemCode(productCode);
            response.setItemPrimaryBarcode(dto.getItemPrimaryBarcode());
            response.setTransactionDate(dto.getTransactionDate());
            response.setTotalQuantityAvailable(dto.getTotalQuantityAvailable());
            response.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_ERROR);
            response.setMessage("INFO-SOHPRODUCTCODE Requested product code or ean is different to the one supplied with data payload.");
            return response;
        }

        LOG.info("INFO-SOHUPDATE Updating stock level, product=" + dto.getItemCode() + ", availability="
                + dto.getTotalQuantityAvailable() + ", warehouse=" + dto.getWarehouse() + ", ean="
                + dto.getItemPrimaryBarcode());

        response = stockUpdateIntegrationFacade.updateStock(dto);
        return response;
    }

    /**
     * 
     * @param productCode
     * @param dto
     * @return IntegrationStockUpdateResponseDto entity with result of the update operation
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetstockadjustment/{productCode}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationStockUpdateResponseDto adjustStockLevel(@PathVariable final String productCode,
            @RequestBody final IntegrationStockUpdateDto dto) {

        IntegrationStockUpdateResponseDto response = null;

        if (productCode == null
                || !(productCode.equals(dto.getItemCode()) || productCode.equals(dto.getItemPrimaryBarcode()))) {
            response = new IntegrationStockUpdateResponseDto();
            response.setItemCode(productCode);
            response.setItemPrimaryBarcode(dto.getItemPrimaryBarcode());
            response.setTransactionDate(dto.getTransactionDate());
            response.setTransactionNumber(dto.getTransactionNumber());
            response.setAdjustmentQuantity(dto.getAdjustmentQuantity());
            response.setStatusCode(IntegrationStockUpdateResponseDto.STATUS_ERROR);
            response.setMessage("INFO-ADJPRODUCTCODE Requested product code or EAN is different to the one supplied with data payload.");
            return response;
        }

        LOG.info("INFO-ADJUPDATE Adjusting stock level for product=" + dto.getItemCode() + ", adjustment="
                + dto.getAdjustmentQuantity() + ", warehouse=" + dto.getWarehouse() + ", EAN="
                + dto.getItemPrimaryBarcode());

        response = stockUpdateIntegrationFacade.adjustStock(dto);

        if (StringUtils.isNotEmpty(dto.getItemCode())) {
            stockUpdateToPartnerFacade.updateAvailableStock(dto.getItemCode());
        }

        return response;
    }

    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetstockupdatebatch", method = RequestMethod.PUT)
    @ResponseBody
    public TargetStockUpdateResponseDto updateStockLevels(@RequestBody final TargetStockUpdateDto dto) {
        LOG.info("INFO-SOHUPDATE Updating stock level for products, transactonNumber="
                + dto.getTransactionNumber() + ", transaction date=" + dto.getTransactionDate() + ", warehouse="
                + dto.getWarehouse() + ", comment="
                + dto.getComment());
        logDebugInfo(dto);
        return stockUpdateIntegrationFacade.updateStockBatch(dto);
    }

    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetstockadjustmentbatch", method = RequestMethod.PUT)
    @ResponseBody
    public TargetStockUpdateResponseDto adjustStockLevels(@RequestBody final TargetStockUpdateDto dto) {
        LOG.info("INFO-ADJUPDATE Adjusting stock level for products, transactonNumber="
                + dto.getTransactionNumber() + ", transaction date=" + dto.getTransactionDate() + ", warehouse="
                + dto.getWarehouse() + ", comment="
                + dto.getComment());
        logDebugInfo(dto);
        return stockUpdateIntegrationFacade.adjustStockBatch(dto);
    }

    /**
     * 
     * @param dto
     * @return reponse dto
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetsohstockadjustment", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto adjustSohStockLevel(@RequestBody final StockUpdate dto) {

        IntegrationResponseDto response = null;

        LOG.info("INFO-SOH-ADJUPDATE Adjusting soh stock level for product: " + dto);

        response = stockUpdateIntegrationFacade.updateStock(dto);

        stockUpdateToPartnerFacade.updateAvailableStock(dto.getProductCode());

        return response;
    }

    private void logDebugInfo(final TargetStockUpdateDto dto) {
        if (LOG.isDebugEnabled()) {
            final StringBuilder tempItemsInfo = new StringBuilder(StringUtils.EMPTY);
            if (CollectionUtils.isNotEmpty(dto.getItems())) {
                for (final TargetStockUpdateProductDto item : dto.getItems()) {
                    tempItemsInfo.append("\nitemCode=" + item.getItemCode() + " itemPrimaryBarcode="
                            + item.getItemPrimaryBarcode() + " totalQuantityAvailable="
                            + item.getTotalQuantityAvailable()
                            + " adjustmentQuantity=" + item.getAdjustmentQuantity());
                }
            }
            LOG.debug("Received the stock update. transactionNumber=" + dto.getTransactionNumber()
                    + " transactionDate="
                    + dto.getTransactionDate() + " warehouse=" + dto.getWarehouse() + " comment=" + dto.getComment()
                    + " items size=" + dto.getItems().size() + " items:" + tempItemsInfo.toString());
        }
    }

}

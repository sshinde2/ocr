/**
 * 
 */
package au.com.target.tgtws.controller.storelocator;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.target.tgtws.controller.BaseController;
import au.com.target.tgtwsfacades.integration.dto.IntegrationPointOfServiceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.storelocator.TargetStoreLocatorIntegrationFacade;


/**
 * @author fkratoch
 * 
 */
@Controller
@RequestMapping(value = "/v1/{baseSiteId}/storelocatorapi")
public class StoreLocatorController extends BaseController {
    private static final Logger LOG = Logger.getLogger(StoreLocatorController.class);

    @Autowired
    private TargetStoreLocatorIntegrationFacade targetStoreLocatorIntegrationFacade;

    /**
     * 
     * @param storeNumber
     * @return IntegrationPointOfService entity for the selected storeNumber
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetpointofservice/{storeNumber}", method = RequestMethod.GET)
    @ResponseBody
    public IntegrationPointOfServiceDto getPointOfService(@PathVariable final Integer storeNumber) {
        LOG.info("Retrieving details of TargetPointOfService with storeNumber: " + storeNumber);
        return targetStoreLocatorIntegrationFacade.getPointOfService(storeNumber);
    }

    /**
     * 
     * @param storeNumber
     * @param dto
     * @return IntegrationResponseDto entity with result of the persist operation
     */
    @Secured("ROLE_ESBGROUP")
    @RequestMapping(value = "/targetpointofservice/{storeNumber}", method = RequestMethod.PUT)
    @ResponseBody
    public IntegrationResponseDto putPointOfService(@PathVariable final Integer storeNumber,
            @RequestBody final IntegrationPointOfServiceDto dto) {

        if (!dto.getStoreNumber().equals(storeNumber)) {
            final IntegrationResponseDto response = new IntegrationResponseDto(dto.getStoreName(), dto.getStoreName());
            response.setSuccessStatus(false);
            response.addMessage("Requested storeNumber is different to the one supplied with data payload.");
            return response;
        }


        LOG.info("Persisting details of TargetPointOfService with storeNumber: " + storeNumber);
        return targetStoreLocatorIntegrationFacade.persistPointOfService(dto);
    }
}

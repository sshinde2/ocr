/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtws.conv;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.converters.populator.SolrSearchQueryEncoderPopulator;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;

import org.springframework.beans.factory.annotation.Required;

import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


/**
 * SearchQuery data converter renders a {@link SolrSearchQueryData} as
 * 
 * <pre>
 * {@code 
 * <currentQuery>a:relevance</currentQuery>
 * }
 * </pre>
 */
public class SearchQueryDataConverter extends AbstractRedirectableConverter
{
    private SolrSearchQueryEncoderPopulator targetSolrSearchQueryEncoder;

    protected SolrSearchQueryEncoderPopulator getTargetSolrSearchQueryEncoder()
    {
        return targetSolrSearchQueryEncoder;
    }

    @Required
    public void setTargetSolrSearchQueryEncoder(final SolrSearchQueryEncoderPopulator solrSearchQueryEncoder)
    {
        this.targetSolrSearchQueryEncoder = solrSearchQueryEncoder;
    }

    @Override
    public boolean canConvert(final Class type)
    {
        return type == SolrSearchQueryData.class;
    }

    @Override
    public void marshal(final Object source, final HierarchicalStreamWriter writer, final MarshallingContext context)
    {
        final SearchQueryData queryData = new SearchQueryData();
        getTargetSolrSearchQueryEncoder().populate((SolrSearchQueryData)source, queryData);
        writer.setValue(queryData.getValue());
    }

    @Override
    public Object unmarshal(final HierarchicalStreamReader reader, final UnmarshallingContext context)
    {
        return getTargetConverter().unmarshal(reader, context);
    }

    @Override
    public Class getConvertedClass()
    {
        return SolrSearchQueryData.class;
    }
}

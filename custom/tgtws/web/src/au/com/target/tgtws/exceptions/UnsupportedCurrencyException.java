/**
 * 
 */
package au.com.target.tgtws.exceptions;

import de.hybris.platform.core.model.c2l.CurrencyModel;

import javax.servlet.ServletException;


/**
 * @author krzysztof.kwiatosz
 * 
 */
public class UnsupportedCurrencyException extends ServletException
{

    private CurrencyModel currency;

    /**
     * @param currencyToSet
     */
    public UnsupportedCurrencyException(final CurrencyModel currencyToSet)
    {
        super("Currency " + currencyToSet + " is not supported by the current base store");
        this.currency = currencyToSet;
    }

    /**
     * @param msg
     */
    public UnsupportedCurrencyException(final String msg)
    {
        super(msg);
    }

    /**
     * @return the currency
     */
    public CurrencyModel getCurrency()
    {
        return currency;
    }
}

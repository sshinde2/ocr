/**
 * 
 */
package au.com.target.tgtws.exceptions;

import de.hybris.platform.core.model.c2l.LanguageModel;

import javax.servlet.ServletException;


/**
 * @author krzysztof.kwiatosz
 * 
 */
public class UnsupportedLanguageException extends ServletException
{

    private LanguageModel language;

    /**
     * @param languageToSet
     */
    public UnsupportedLanguageException(final LanguageModel languageToSet)
    {
        super("Language " + languageToSet + " is not supported by the current base store");
        this.language = languageToSet;
    }

    /**
     * @param msg
     */
    public UnsupportedLanguageException(final String msg)
    {
        super(msg);
    }

    /**
     * @return the language
     */
    public LanguageModel getLanguage()
    {
        return language;
    }
}

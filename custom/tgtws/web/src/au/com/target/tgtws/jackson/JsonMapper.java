/**
 * 
 */
package au.com.target.tgtws.jackson;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;


/**
 * Setup config
 * 
 * @author rsamuel3
 * 
 */
public class JsonMapper extends ObjectMapper {

    /**
     * 
     */
    public JsonMapper() {
        super();
        this.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
        this.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
    }

}

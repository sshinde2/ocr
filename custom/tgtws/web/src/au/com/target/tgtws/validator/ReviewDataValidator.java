/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtws.validator;

import de.hybris.platform.commercefacades.product.data.ReviewData;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component("reviewValidator")
public class ReviewDataValidator implements Validator
{

    @Override
    public boolean supports(final Class<?> clazz)
    {
        return ReviewData.class.equals(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors)
    {
        ValidationUtils.rejectIfEmpty(errors, "headline", "field.required");
        ValidationUtils.rejectIfEmpty(errors, "comment", "field.required");

    }

}

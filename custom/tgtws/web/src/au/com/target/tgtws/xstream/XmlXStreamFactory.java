/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2012 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package au.com.target.tgtws.xstream;

import org.springframework.beans.factory.FactoryBean;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.ArrayMapper;
import com.thoughtworks.xstream.mapper.MapperWrapper;


/**
 * 
 * {@link XStream} instance factory
 */
public class XmlXStreamFactory implements FactoryBean {

    private static XStream xmlInstance = null;

    private final SingletonGetter<XStream> instanceGetter = new SingletonGetter<XStream>() {

        @Override
        XStream getInstanceProperty() {
            return xmlInstance;
        }

        @Override
        void setInstanceProperty(final XStream prop) {
            xmlInstance = prop;
        }

        @Override
        XStream create() {
            return getObjectInternal();
        }
    };

    @Override
    public Object getObject() throws Exception {
        return instanceGetter.get();
    }


    protected XStream getObjectInternal() {
        //        final StaxDriver driver = new StaxDriver()
        //        {
        //            @Override
        //            public StaxWriter createStaxWriter(final XMLStreamWriter out) throws XMLStreamException
        //            {
        //                out.writeStartDocument("UTF-8", "1.0");
        //                return createStaxWriter(out, false);
        //            }
        //        };

        final XStream stream = new XStream() {
            @Override
            protected MapperWrapper wrapMapper(final MapperWrapper next) {
                return createMapperWrapper(next);
            }
        };
        return stream;
    }


    /**
     * Due to schema compatibility requirements, customizes a {@link MapperWrapper} for arrays to don't generate a
     * -array suffixes.
     * 
     */
    protected MapperWrapper createMapperWrapper(final MapperWrapper parent) {
        final MapperWrapper m = new ArrayMapper(parent) {

            @Override
            public String aliasForSystemAttribute(final String attribute) {
                return "class".equals(attribute) ? null : attribute;
            }
        };
        return m;
    }

    @Override
    public Class getObjectType() {
        return XStream.class;
    }




    @Override
    public boolean isSingleton() {
        return true;
    }

}

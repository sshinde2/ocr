/**
 * 
 */
package au.com.target.tgtws.controller.cnc;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.cnc.ClickAndCollectNotificationFacade;
import au.com.target.tgtwsfacades.integration.dto.ClickAndCollectNotificationRequestDto;
import au.com.target.tgtwsfacades.integration.dto.ClickAndCollectNotificationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationClickAndCollectNotificationDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationClickAndCollectNotificationResponseDto;


/**
 * @author knemalik
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ClickAndCollectNotificationControllerTest {

    private static final String STORE_NUMBER = "5599";
    private static final String LETTER_TYPE = "12345";
    private static final String LAYBUYNUMBER = "00000000";
    private static final String ORDER_NUMBER = "00000000";

    @Mock
    private ClickAndCollectNotificationFacade clickAndCollectNotificationFacade;

    private final IntegrationClickAndCollectNotificationDto requestDto = new IntegrationClickAndCollectNotificationDto();

    @InjectMocks
    private final MyClickAndCollectNotificationController controller = new MyClickAndCollectNotificationController();

    public class MyClickAndCollectNotificationController extends ClickAndCollectNotificationController {

        @Override
        protected String getPosLaybyNumber(final String laybyNumber) {
            return LAYBUYNUMBER;
        }
    }

    private final ClickAndCollectNotificationRequestDto cncRequestDto = new ClickAndCollectNotificationRequestDto();


    @Before
    public void setup() {
        requestDto.setLaybyNumber(LAYBUYNUMBER);
        requestDto.setLetterType(LETTER_TYPE);
        requestDto.setStoreNumber(STORE_NUMBER);

        cncRequestDto.setOrderNumber(ORDER_NUMBER);
        cncRequestDto.setStoreNumber(STORE_NUMBER);
    }

    @Test
    public void testNotifyCncAvailabilityWithSuccess() {
        when(Boolean.valueOf(clickAndCollectNotificationFacade.triggerCNCNotification(LAYBUYNUMBER, STORE_NUMBER,
                LETTER_TYPE))).thenReturn(Boolean.TRUE);

        final IntegrationClickAndCollectNotificationResponseDto response = controller.notifyCncAvailability(requestDto);
        Assert.assertTrue(response.isSuccess());
    }

    @Test
    public void testNotifyCncAvailabilityWithFailure() {
        when(Boolean.valueOf(clickAndCollectNotificationFacade.triggerCNCNotification(LAYBUYNUMBER, STORE_NUMBER,
                LETTER_TYPE))).thenReturn(Boolean.FALSE);

        final IntegrationClickAndCollectNotificationResponseDto response = controller.notifyCncAvailability(requestDto);
        Assert.assertFalse(response.isSuccess());
    }

    @Test
    public void testNotifyCncPickedWhenSuccess() {
        final ClickAndCollectNotificationResponseDto expectedCncResponseDto = new ClickAndCollectNotificationResponseDto();
        expectedCncResponseDto.setSuccess(true);

        when(clickAndCollectNotificationFacade.notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER))
                .thenReturn(expectedCncResponseDto);
        final ClickAndCollectNotificationResponseDto actualCncResponseDto = controller.notifyCncPickedup(cncRequestDto);
        Mockito.verify(clickAndCollectNotificationFacade).notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER);
        Assert.assertTrue(actualCncResponseDto.isSuccess());
    }

    @Test
    public void testNotifyCncPickedWhenFailure() {
        final ClickAndCollectNotificationResponseDto expectedCncResponseDto = new ClickAndCollectNotificationResponseDto();
        expectedCncResponseDto.setSuccess(false);

        when(clickAndCollectNotificationFacade.notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER))
                .thenReturn(expectedCncResponseDto);
        final ClickAndCollectNotificationResponseDto actualCncResponseDto = controller.notifyCncPickedup(cncRequestDto);
        Mockito.verify(clickAndCollectNotificationFacade).notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER);
        Assert.assertFalse(actualCncResponseDto.isSuccess());
    }

    @Test
    public void testNotifyCncReturnedToFloorWhenSuccess() {
        final ClickAndCollectNotificationResponseDto expectedCncResponseDto = new ClickAndCollectNotificationResponseDto();
        expectedCncResponseDto.setSuccess(true);

        when(clickAndCollectNotificationFacade.notifyCncReturnedToFloor(ORDER_NUMBER, STORE_NUMBER))
                .thenReturn(expectedCncResponseDto);
        final ClickAndCollectNotificationResponseDto actualCncResponseDto = controller
                .notifyCncReturnedToFloor(cncRequestDto);
        Mockito.verify(clickAndCollectNotificationFacade).notifyCncReturnedToFloor(ORDER_NUMBER, STORE_NUMBER);
        Assert.assertTrue(actualCncResponseDto.isSuccess());
    }

    @Test
    public void testNotifyCncReturnedToFloorWhenFailure() {
        final ClickAndCollectNotificationResponseDto expectedCncResponseDto = new ClickAndCollectNotificationResponseDto();
        expectedCncResponseDto.setSuccess(false);

        when(clickAndCollectNotificationFacade.notifyCncReturnedToFloor(ORDER_NUMBER, STORE_NUMBER))
                .thenReturn(expectedCncResponseDto);
        final ClickAndCollectNotificationResponseDto actualCncResponseDto = controller
                .notifyCncReturnedToFloor(cncRequestDto);
        Mockito.verify(clickAndCollectNotificationFacade).notifyCncReturnedToFloor(ORDER_NUMBER, STORE_NUMBER);
        Assert.assertFalse(actualCncResponseDto.isSuccess());
    }
}

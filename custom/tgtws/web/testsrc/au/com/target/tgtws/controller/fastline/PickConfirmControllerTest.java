/**
 * 
 */
package au.com.target.tgtws.controller.fastline;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.PickConfirmOrder;
import au.com.target.tgtwsfacades.pickconfirm.PickConfirmIntegrationFacade;


/**
 * @author Rahul
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class PickConfirmControllerTest {

    @InjectMocks
    private final PickConfirmController pickConfirmController = new PickConfirmController();

    @Mock
    private PickConfirmIntegrationFacade pickConfirmIntegrationFacade;

    private final PickConfirmOrder dto = new PickConfirmOrder();

    private IntegrationResponseDto responseDto;

    @Before
    public void setUp() throws Exception {
        dto.setOrderNumber("12345678");
    }

    @Test
    public void testUpdatePickConfirmNullCode() {
        responseDto = pickConfirmController.updatePickConfirm(null, dto);

        Assert.assertFalse(responseDto.isSuccessStatus());
        Assert.assertTrue(responseDto.getMessages().size() > 0);
        Assert.assertEquals(PickConfirmController.PICK_CONF_FAILURE_MESSAGE, responseDto.getMessages().get(0));
    }

    @Test
    public void testUpdatePickConfirmCodeNotMatchingWithDto() {
        responseDto = pickConfirmController.updatePickConfirm("87654321", dto);

        Assert.assertFalse(responseDto.isSuccessStatus());
        Assert.assertTrue(responseDto.getMessages().size() > 0);
        Assert.assertEquals(PickConfirmController.PICK_CONF_FAILURE_MESSAGE, responseDto.getMessages().get(0));
    }

    @Test
    public void testUpdatePickConfirmFailure() {
        responseDto = new IntegrationResponseDto();
        responseDto.setSuccessStatus(false);

        Mockito.when(pickConfirmIntegrationFacade.handlePickConfirm(dto)).thenReturn(responseDto);
        final IntegrationResponseDto result = pickConfirmController.updatePickConfirm("12345678", dto);

        Assert.assertEquals(Boolean.valueOf(responseDto.isSuccessStatus()), Boolean.valueOf(result.isSuccessStatus()));
    }

    @Test
    public void testUpdatePickConfirmSuccess() {
        responseDto = new IntegrationResponseDto();
        responseDto.setSuccessStatus(true);

        Mockito.when(pickConfirmIntegrationFacade.handlePickConfirm(dto)).thenReturn(responseDto);
        final IntegrationResponseDto result = pickConfirmController.updatePickConfirm("12345678", dto);

        Assert.assertEquals(Boolean.valueOf(responseDto.isSuccessStatus()), Boolean.valueOf(result.isSuccessStatus()));
    }
}

/**
 * 
 */
package au.com.target.tgtws.controller.fluent;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import au.com.target.tgtcore.order.FluentErrorException;
import au.com.target.tgtcore.order.FluentOrderException;
import au.com.target.tgtcore.order.FluentOrderService;
import au.com.target.tgtfluent.data.Order;


/**
 * @author fkhan4
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FluentOrderUpdateControllerTest {

    @Mock
    private FluentOrderService fluentOrderService;

    @InjectMocks
    private final FluentOrderUpdateController fluentOrderUpdateController = new FluentOrderUpdateController();

    @Test
    public void testUpdateOrder()
            throws FluentOrderException, FluentErrorException {
        final Order order = mock(Order.class);

        final ResponseEntity responseEntity = fluentOrderUpdateController.updateOrder("event",
                order);
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(fluentOrderService).updateOrder(order);
    }

    @Test
    public void testUpdateOrderBadRequest()
            throws FluentOrderException, FluentErrorException {
        final Order order = mock(Order.class);
        willReturn("2048").given(order).getOrderId();
        willThrow(new FluentOrderException()).given(fluentOrderService).updateOrder(order);

        final ResponseEntity responseEntity = fluentOrderUpdateController.updateOrder("event",
                order);
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void testUpdateOrderFailedDependency()
            throws FluentOrderException, FluentErrorException {
        final Order order = mock(Order.class);
        willReturn("2048").given(order).getOrderId();
        willThrow(new FluentErrorException()).given(fluentOrderService).updateOrder(order);

        final ResponseEntity responseEntity = fluentOrderUpdateController.updateOrder("event",
                order);
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.FAILED_DEPENDENCY);
    }
}

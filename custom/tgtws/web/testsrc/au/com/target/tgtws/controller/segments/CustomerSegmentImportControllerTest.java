/**
 * 
 */
package au.com.target.tgtws.controller.segments;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Collections;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentImportResponseDto;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoDto;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoRequestDto;
import au.com.target.tgtwsfacades.segments.CustomerSegmentImportFacade;


/**
 * @author mjanarth
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomerSegmentImportControllerTest {

    @Mock
    private CustomerSegmentImportFacade customerSegmentImportFacade;
    @InjectMocks
    private final CustomerSegmentImportController controller = new CustomerSegmentImportController();

    @Test
    public void testImportSegmentsWithNullReq() {
        final CustomerSegmentImportResponseDto response = controller.importCustomerSegments(null);
        Assert.assertEquals(response.isSuccess(), false);
        Mockito.verifyZeroInteractions(customerSegmentImportFacade);
    }


    @Test
    public void testImportSegmentsWithValidReq() {
        final CustomerSegmentInfoRequestDto req = new CustomerSegmentInfoRequestDto();
        final CustomerSegmentInfoDto info = new CustomerSegmentInfoDto();
        info.setSubscriptionId("1490");
        req.setCustomerSegments(Collections.singletonList(info));
        controller.importCustomerSegments(req);
        Mockito.verify(customerSegmentImportFacade).importCustomerSegments(req);
    }

}

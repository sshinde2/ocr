/**
 * 
 */
package au.com.target.tgtws.controller.warehouse;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtwsfacades.consignment.ConsignmentIntegrationFacade;
import au.com.target.tgtwsfacades.integration.dto.Consignment;
import au.com.target.tgtwsfacades.integration.dto.Consignments;
import au.com.target.tgtwsfacades.integration.dto.IntegrationConsignmentDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * @author gsing236
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConsignmentControllerTest {

    @InjectMocks
    private final ConsignmentController controller = new ConsignmentController();

    @Mock
    private ConsignmentIntegrationFacade consignmentIntegrationFacade;

    @Test
    public void testShipConsignmentsWithInputEmptyConsignments() {
        final List<IntegrationResponseDto> response = controller.shipConsignments(new IntegrationConsignmentDto());
        assertThat(response).isNotNull();
        assertThat(response).hasSize(1);
        assertThat(response).onProperty("messages").hasSize(1)
                .contains(Arrays.asList("CONSIGNMENT-SHIP - Invalid inputs- No consignments present"));
    }

    @Test
    public void testShipConsignmentsSuccess() {
        final IntegrationConsignmentDto dto = new IntegrationConsignmentDto();
        final Consignments consignments = new Consignments();
        consignments.setConsignments(Collections.singletonList(new Consignment()));
        dto.setConsignments(consignments);

        final IntegrationResponseDto responseDto = new IntegrationResponseDto();
        responseDto.setSuccessStatus(true);
        given(consignmentIntegrationFacade.shipConsignments(dto.getConsignments()))
                .willReturn(Collections.singletonList(responseDto));
        final List<IntegrationResponseDto> response = controller.shipConsignments(dto);
        assertThat(response).isNotNull().hasSize(1);
        assertThat(response).onProperty("successStatus").containsExactly(Boolean.TRUE);
    }
}

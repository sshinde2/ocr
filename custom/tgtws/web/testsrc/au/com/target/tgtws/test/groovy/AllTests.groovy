package au.com.target.tgtws.test.groovy
import junit.framework.TestSuite


class AllTests extends TestSuite {
    // Since Eclipse launches tests relative to the project root,
    // declare the relative path to the test scripts for convenience
    private static final String TEST_ROOT = "web/testsrc/de/hybris/platform/tgtws/test/groovy/";

    public static TestSuite suite() throws Exception {
        TestSuite suite = new TestSuite();
        GroovyTestSuite gsuite = new GroovyTestSuite();
        suite.addTestSuite(gsuite.compile(TEST_ROOT + "CustomerTests.groovy"));
        //etc.

        return suite;
    }

}

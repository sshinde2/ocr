/**
 * 
 */
package au.com.target.tgtwsfacades.activation;

import au.com.target.tgtwsfacades.integration.dto.IntegrationActivationDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;



/**
 * @author mmaki
 * 
 */
public interface ActivationIntegrationFacade {

    IntegrationResponseDto updateApprovalStatus(final IntegrationActivationDto dto);

}

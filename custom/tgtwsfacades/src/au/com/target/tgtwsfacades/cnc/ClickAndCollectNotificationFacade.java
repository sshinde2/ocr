/**
 * 
 */
package au.com.target.tgtwsfacades.cnc;

import au.com.target.tgtwsfacades.integration.dto.ClickAndCollectNotificationResponseDto;


/**
 * @author knemalik
 * 
 */
public interface ClickAndCollectNotificationFacade {

    /**
     * Method to send ready to pick up cnc order notification using letter-type.
     * 
     * @param orderId
     * @param storeNumber
     * @param letterType
     * @return boolean, true - if successfully sent.
     */
    boolean triggerCNCNotification(String orderId, String storeNumber, String letterType);

    /**
     * Method to notify cnc order is picked up.
     * 
     * @param orderNumber
     * @return ClickAndCollectNotificationResponseDto - boolean, true - if successfully updated.
     */
    ClickAndCollectNotificationResponseDto notifyCncPickedUp(String orderNumber, String storeNumber);

    /**
     * Method to notify cnc order is un-packed and returned to floor
     * 
     * @param orderNumber
     * @param storeNumber
     * @return ClickAndCollectNotificationResponseDto - boolean, true - if successfully updated.
     */
    ClickAndCollectNotificationResponseDto notifyCncReturnedToFloor(String orderNumber, String storeNumber);

}

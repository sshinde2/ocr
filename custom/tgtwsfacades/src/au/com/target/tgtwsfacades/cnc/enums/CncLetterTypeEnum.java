/**
 * 
 */
package au.com.target.tgtwsfacades.cnc.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


/**
 * @author bhuang3
 * 
 */
public enum CncLetterTypeEnum {

    INSTORE("14", "instore"), OFFSITE("13", "offsite");

    private static final Map<String, CncLetterTypeEnum> CNC_LETTER_TYPE_LOOKUP_MAP = new HashMap<String, CncLetterTypeEnum>();

    private String letterTypeNumber;
    private String letterType;

    static {
        for (final CncLetterTypeEnum cncLetterType : EnumSet.allOf(CncLetterTypeEnum.class)) {
            CNC_LETTER_TYPE_LOOKUP_MAP.put(cncLetterType.getLetterTypeNumber(), cncLetterType);
        }
    }

    private CncLetterTypeEnum(final String letterTypeNumber, final String letterType) {
        this.letterTypeNumber = letterTypeNumber;
        this.letterType = letterType;
    }

    public static CncLetterTypeEnum getByLetterType(final String letterTypeNumber) {
        return CNC_LETTER_TYPE_LOOKUP_MAP.get(letterTypeNumber);
    }

    /**
     * @return the letterType
     */
    public String getLetterType() {
        return letterType;
    }

    /**
     * @return the letterTypeNumber
     */
    public String getLetterTypeNumber() {
        return letterTypeNumber;
    }

}

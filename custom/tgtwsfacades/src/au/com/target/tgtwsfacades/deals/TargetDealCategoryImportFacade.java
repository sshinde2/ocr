package au.com.target.tgtwsfacades.deals;

import au.com.target.tgtwsfacades.deals.dto.TargetDealCategoryDTO;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * Facade interface to create, update and delete deal category
 */

public interface TargetDealCategoryImportFacade {

    /**
     * Create or update a deal category based on the information provided, including products and allowed principals.
     * 
     * @param categoryDTO
     *            The payload containing the category information
     * @return the response containing a status and messages (if any)
     */

    IntegrationResponseDto persistTargetDealCategory(TargetDealCategoryDTO categoryDTO);

}

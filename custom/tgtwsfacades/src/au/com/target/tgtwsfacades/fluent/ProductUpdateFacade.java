/**
 * 
 */
package au.com.target.tgtwsfacades.fluent;

/**
 * @author mgazal
 *
 */
public interface ProductUpdateFacade {
    /**
     * Finds products for which onlineDate needs to be set and then sets it
     */
    void setOnlineDateForApplicableProducts();
}

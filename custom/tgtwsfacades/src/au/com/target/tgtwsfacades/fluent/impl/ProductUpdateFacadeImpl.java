/**
 * 
 */
package au.com.target.tgtwsfacades.fluent.impl;

import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtwsfacades.fluent.ProductUpdateFacade;


/**
 * @author mgazal
 *
 */
public class ProductUpdateFacadeImpl implements ProductUpdateFacade {

    @Autowired
    private TargetProductService targetProductService;

    @Override
    public void setOnlineDateForApplicableProducts() {
        targetProductService.setOnlineDateForApplicableProducts();
    }
}

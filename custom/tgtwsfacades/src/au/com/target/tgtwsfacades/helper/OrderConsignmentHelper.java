/**
 * 
 */
package au.com.target.tgtwsfacades.helper;

import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * Helper class to have methods to deal with order and its consignment
 * 
 *
 */
public class OrderConsignmentHelper {

    private TargetConsignmentService targetConsignmentService;

    /**
     * Retrieve the consignment based on consignmentId or orderId
     * 
     * @param code
     * @return TargetConsignmentModel
     */
    public TargetConsignmentModel getConsignmentBasedOrderOrConsignmentCode(final String code) {
        TargetConsignmentModel consignment = null;
        try {
            consignment = targetConsignmentService
                    .getConsignmentForCode(code);
        }
        catch (final NotFoundException e) {
            consignment = targetConsignmentService
                    .findConsignmentForDefaultWarehouse(code);
        }
        return consignment;
    }

    /**
     * @param targetConsignmentService
     *            the targetConsignmentService to set
     */
    @Required
    public void setTargetConsignmentService(final TargetConsignmentService targetConsignmentService) {
        this.targetConsignmentService = targetConsignmentService;
    }

}

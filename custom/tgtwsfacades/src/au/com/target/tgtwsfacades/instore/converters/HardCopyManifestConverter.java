/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.platform.core.model.user.AddressModel;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwsfacades.instore.dto.auspost.Address;
import au.com.target.tgtwsfacades.instore.dto.auspost.ConsignmentSummary;
import au.com.target.tgtwsfacades.instore.dto.auspost.ManifestHardCopy;


/**
 * @author smishra1
 *
 */
public class HardCopyManifestConverter {
    private static final Logger LOG = Logger.getLogger(HardCopyManifestConverter.class);
    private HardCopyManifestConsignmentConverter consignmentConverter;

    /**
     * 
     * @param tpos
     *            the tpos object
     * @param manifestModel
     *            the manifest model
     * @return ManifestHardCopy the manifest hard copy data
     */
    public ManifestHardCopy convert(final TargetPointOfServiceModel tpos, final TargetManifestModel manifestModel) {
        Assert.notNull(tpos, "tpos cannot be null");
        Assert.notNull(manifestModel, "Manifest Model cannot be null");
        final StoreFulfilmentCapabilitiesModel storeCapability = tpos.getFulfilmentCapability();
        if (null == storeCapability) {
            LOG.warn("No store capability found for the store : " + tpos.getStoreNumber());
            return null;
        }
        double totalWeight = 0;
        int totalArticles = 0;
        final ManifestHardCopy manifestHardCopy = new ManifestHardCopy();
        final List<ConsignmentSummary> consignmentSummaries = new ArrayList<>();
        manifestHardCopy.setMerchantLocationId(storeCapability.getMerchantLocationId());
        manifestHardCopy.setStoreAddress(getStoreAddress(tpos));
        manifestHardCopy
                .setManifestCreatedDate(TargetDateUtil.getDateFormattedAsString(manifestModel.getDate()));
        manifestHardCopy.setMerchantId(storeCapability.getMerchantId());
        manifestHardCopy.setManifestNumber(manifestModel.getCode());
        manifestHardCopy.setAusPostAccountNumber(storeCapability.getChargeAccountNumber().toString());
        manifestHardCopy.setLodgementFacility(storeCapability.getLodgementFacility());
        final Set<TargetConsignmentModel> consignmentList = manifestModel.getConsignments();
        final DecimalFormat roundUp = new DecimalFormat("#.###");
        if (!CollectionUtils.isEmpty(consignmentList)) {
            for (final TargetConsignmentModel consignment : consignmentList) {
                final ConsignmentSummary consignmentSummary = consignmentConverter.convert(consignment);
                consignmentSummary.setChargeCode(storeCapability.getChargeCode());
                consignmentSummaries.add(consignmentSummary);
                totalWeight += consignmentSummary.getTotalWeight().doubleValue();
                totalArticles += consignmentSummary.getTotalArticles().intValue();
            }
            Collections.sort(consignmentSummaries,
                    new Comparator<ConsignmentSummary>() {
                        @Override
                        public int compare(final ConsignmentSummary conSummary1, final ConsignmentSummary conSummary2) {
                            return new CompareToBuilder()
                                    .append(conSummary1.getChargeZone().getCode(),
                                            conSummary2.getChargeZone().getCode())
                                    .append(conSummary1.getConsignmentId(), conSummary2.getConsignmentId())
                                    .toComparison();
                        }
                    });
        }
        else {
            LOG.warn("No Consignments found for : " + manifestModel.getCode());
        }
        manifestHardCopy.setConsignmentSummaries(consignmentSummaries);
        manifestHardCopy.setTotalArticles(Integer.valueOf(totalArticles));
        manifestHardCopy.setTotalWeight(Double.valueOf(roundUp.format(totalWeight)));
        return manifestHardCopy;
    }

    protected Address getStoreAddress(final TargetPointOfServiceModel tpos) {
        final AddressModel address = tpos.getAddress();
        if (null == address) {
            LOG.warn("Store address information not found for the store : " + tpos.getStoreNumber());
            return null;
        }
        final Address storeAddress = new Address();
        storeAddress.setAddressLine1(address.getLine1());
        storeAddress.setAddressLine2(address.getLine2());
        storeAddress.setSuburb(address.getTown());
        storeAddress.setState(address.getDistrict());
        storeAddress.setPostcode(address.getPostalcode());
        return storeAddress;
    }

    @Required
    public void setHardCopyManifestConsignmentConverter(
            final HardCopyManifestConsignmentConverter manifestConsignmentConverter) {
        this.consignmentConverter = manifestConsignmentConverter;
    }

}

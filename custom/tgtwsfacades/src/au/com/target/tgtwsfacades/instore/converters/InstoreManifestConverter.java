/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.manifests.Manifest;


/**
 * Converter to generate DTO for portal REST API from ManifestModel
 * 
 * @author Vivek
 *
 */
public class InstoreManifestConverter implements Converter<TargetManifestModel, Manifest> {

    private InstoreConsignmentConverter instoreConsignmentConverter;

    @Override
    public Manifest convert(final TargetManifestModel source, final Manifest prototype)
            throws ConversionException {
        return convert(source);
    }

    @Override
    public Manifest convert(final TargetManifestModel source) throws ConversionException {

        Assert.notNull(source, "manifest cannot be null");

        final Manifest manifest = new Manifest();

        manifest.setCode(source.getCode());
        manifest.setDate(TargetDateUtil.getDateFormattedAsString(source.getDate()));
        manifest.setParcels(source.getParcels());
        manifest.setSent(source.isSent());

        final Set<TargetConsignmentModel> consignmentModels = source.getConsignments();
        if (CollectionUtils.isNotEmpty(consignmentModels)) {
            final List<Consignment> consignments = getConsignmentDtosFromModels(consignmentModels);
            manifest.setConsignments(consignments);
        }

        return manifest;
    }

    /**
     * @param consignmentModels
     * @return List<Consignment>
     */
    private List<Consignment> getConsignmentDtosFromModels(final Set<TargetConsignmentModel> consignmentModels) {
        final List<Consignment> consignments = new ArrayList<>();
        for (final TargetConsignmentModel consignmentModel : consignmentModels) {
            consignments.add(instoreConsignmentConverter.convert(consignmentModel));
        }
        return consignments;
    }

    @Required
    public void setInstoreConsignmentConverter(final InstoreConsignmentConverter instoreConsignmentConverter) {
        this.instoreConsignmentConverter = instoreConsignmentConverter;
    }


}

/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgtwsfacades.instore.dto.consignments.ProductStockLevelInstore;



/**
 * @author Vivek
 *
 */
public class ProductStockLevelConverter {

    private static final Logger LOG = Logger.getLogger(ProductStockLevelConverter.class);

    private enum StockLevel {
        NO_STOCK, LOW_STOCK, MEDIUM_STOCK, HIGH_STOCK
    }

    private int highStock;
    private int mediumStock;

    public List<ProductStockLevelInstore> convert(final StockUpdateResponseDto stockUpdateResponse,
            final String storeNumber) {

        Assert.notNull(stockUpdateResponse, "StockUpdateResponseDto cannot be null");
        final List<StockUpdateStoreResponseDto> storeResponses = stockUpdateResponse.getStockUpdateStoreResponseDtos();

        //Response object is empty.
        if (CollectionUtils.isEmpty(storeResponses)) {
            LOG.error("No stock responses found for store=" + storeNumber);
            return null;
        }
        List<StockUpdateProductResponseDto> productStoreStocks = null;

        //get the response object for the store.
        for (final StockUpdateStoreResponseDto storeResponse : storeResponses) {
            final String storeNo = storeResponse.getStoreNumber();
            if (null != storeNumber && storeNo.equals(storeNumber.toString())) {
                productStoreStocks = storeResponse.getStockUpdateProductResponseDtos();
                break;
            }
        }

        if (CollectionUtils.isEmpty(productStoreStocks)) {
            LOG.error("No product responses found for store=" + storeNumber);
            return null;
        }

        final List<ProductStockLevelInstore> productStocks = new ArrayList<>();

        for (final StockUpdateProductResponseDto stockUpdateProductResponseDto : productStoreStocks) {
            final ProductStockLevelInstore productStock = new ProductStockLevelInstore();
            final String stockLevel = stockUpdateProductResponseDto.getSoh();
            productStock.setProductCode(stockUpdateProductResponseDto.getItemcode());
            productStock.setStockOnHand(stockLevel);
            productStock.setStockLevel(evaluateStockLevel(Integer.valueOf(stockLevel).intValue()));
            productStocks.add(productStock);
        }
        return productStocks;
    }

    /**
     * @param stockLevel
     * @return stockStatus
     */
    private String evaluateStockLevel(final int stockLevel) {
        if (stockLevel >= highStock) {
            return StockLevel.HIGH_STOCK.toString();
        }
        if (stockLevel >= mediumStock) {
            return StockLevel.MEDIUM_STOCK.toString();
        }
        if (stockLevel > 0) {
            return StockLevel.LOW_STOCK.toString();
        }
        return StockLevel.NO_STOCK.toString();
    }

    /**
     * @param highStock
     *            the highStock to set
     */
    @Required
    public void setHighStock(final int highStock) {
        this.highStock = highStock;
    }

    /**
     * @param mediumStock
     *            the mediumStock to set
     */
    @Required
    public void setMediumStock(final int mediumStock) {
        this.mediumStock = mediumStock;
    }
}

/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtwsfacades.instore.dto.consignments.ProductStockLevelInstore;



/**
 * This class converts the Stock Lookup in cache response to the product stock level instore converter
 *
 */
public class StockVisResponseToProductStockLevelInstoreConverter {

    private static final Logger LOG = Logger.getLogger(StockVisResponseToProductStockLevelInstoreConverter.class);

    private enum StockLevel {
        NO_STOCK, LOW_STOCK, MEDIUM_STOCK, HIGH_STOCK
    }

    private int highStock;
    private int mediumStock;

    public List<ProductStockLevelInstore> convert(final StockVisibilityItemLookupResponseDto stockVisItemLookupResponse,
            final String storeNumber) {

        Assert.notNull(stockVisItemLookupResponse, "StockVisibilityItemLookupResponseDto cannot be null");
        Assert.notNull(storeNumber, "storeNumber cannot be null");
        final List<StockVisibilityItemResponseDto> stockCheckResponses = stockVisItemLookupResponse.getItems();

        //Response object is empty.
        if (CollectionUtils.isEmpty(stockCheckResponses)) {
            LOG.error("No stock responses found for store=" + storeNumber);
            return Collections.emptyList();
        }
        final List<ProductStockLevelInstore> productStocks = new ArrayList<>();

        //get the response object for the store.
        for (final StockVisibilityItemResponseDto stockCheckResponse : stockCheckResponses) {
            final String storeNo = stockCheckResponse.getStoreNumber();
            if (storeNumber.equals(storeNo)) {
                final ProductStockLevelInstore productStock = new ProductStockLevelInstore();
                final String soh = stockCheckResponse.getSoh();
                productStock.setProductCode(stockCheckResponse.getCode());
                productStock.setStockOnHand(soh);
                productStock.setStockLevel(evaluateStockLevel(soh));
                productStocks.add(productStock);
            }
        }
        return productStocks;
    }

    /**
     * @param soh
     * @return stockStatus
     */
    private String evaluateStockLevel(final String soh) {

        int stockLevel = 0;
        try {
            stockLevel = Integer.parseInt(soh);
        }
        catch (final NumberFormatException ne) {
            return StockLevel.NO_STOCK.toString();
        }
        if (stockLevel >= highStock) {
            return StockLevel.HIGH_STOCK.toString();
        }
        if (stockLevel >= mediumStock) {
            return StockLevel.MEDIUM_STOCK.toString();
        }
        if (stockLevel > 0) {
            return StockLevel.LOW_STOCK.toString();
        }
        return StockLevel.NO_STOCK.toString();
    }

    /**
     * @param highStock
     *            the highStock to set
     */
    @Required
    public void setHighStock(final int highStock) {
        this.highStock = highStock;
    }

    /**
     * @param mediumStock
     *            the mediumStock to set
     */
    @Required
    public void setMediumStock(final int mediumStock) {
        this.mediumStock = mediumStock;
    }
}

/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto;

/**
 * @author kbalasub
 *
 */
public class CustomParcelConfig {

    private Double maxWeight;
    private Double maxSize;

    /**
     * @return the maxWeight
     */
    public Double getMaxWeight() {
        return maxWeight;
    }

    /**
     * @param maxWeight
     *            the maxWeight to set
     */
    public void setMaxWeight(final Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    /**
     * @return the maxSize
     */
    public Double getMaxSize() {
        return maxSize;
    }

    /**
     * @param maxSize
     *            the maxSize to set
     */
    public void setMaxSize(final Double maxSize) {
        this.maxSize = maxSize;
    }

}

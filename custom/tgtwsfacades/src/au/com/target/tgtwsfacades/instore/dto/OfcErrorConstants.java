/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto;

/**
 * @author rsamuel3
 *
 */
public class OfcErrorConstants {
    public static final String ERR_LOGIN = "ERR_LOGIN";
    public static final String ERR_NOT_FOUND = "ERR_NOT_FOUND";
    public static final String ERR_CONSIGNMENT_STATUS_VALIDATION_FAILED = "ERR_CONSIGNMENT_STATUS_VALIDATION_FAILED";
    public static final String ERR_SYSTEM = "ERR_SYSTEM";
    public static final String ERR_INVALID_PARAM = "ERR_INVALID_PARAM";
    public static final String FLUENT_SYSTEM_ERR = "FLUENT_SYSTEM_ERR";

    //constants for Login
    public static final String AUTHENTICATION_FAILED = "Authentication Failed";
    public static final String REQUIRES_LOGIN = "Requires Login";


    private OfcErrorConstants() {
        //not instantiated
    }
}

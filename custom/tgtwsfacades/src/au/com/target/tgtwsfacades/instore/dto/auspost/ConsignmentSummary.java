/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.auspost;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author smishra1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ConsignmentSummary {
    private String consignmentId;
    private String chargeCode;
    private ChargeZone chargeZone;
    private Address deliveryAddress;
    private Integer totalArticles;
    private Double totalWeight;

    /**
     * @return the consignmentId
     */
    public String getConsignmentId() {
        return consignmentId;
    }

    /**
     * @param consignmentId
     *            the consignmentId to set
     */
    public void setConsignmentId(final String consignmentId) {
        this.consignmentId = consignmentId;
    }

    /**
     * @return the chargeCode
     */
    public String getChargeCode() {
        return chargeCode;
    }

    /**
     * @param chargeCode
     *            the chargeCode to set
     */
    public void setChargeCode(final String chargeCode) {
        this.chargeCode = chargeCode;
    }

    /**
     * @return the chargeZone
     */
    public ChargeZone getChargeZone() {
        return chargeZone;
    }

    /**
     * @param chargeZone
     *            the chargeZone to set
     */
    public void setChargeZone(final ChargeZone chargeZone) {
        this.chargeZone = chargeZone;
    }

    /**
     * @return the deliveryAddress
     */
    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * @param deliveryAddress
     *            the deliveryAddress to set
     */
    public void setDeliveryAddress(final Address deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    /**
     * @return the totalArticles
     */
    public Integer getTotalArticles() {
        return totalArticles;
    }

    /**
     * @param totalArticles
     *            the totalArticles to set
     */
    public void setTotalArticles(final Integer totalArticles) {
        this.totalArticles = totalArticles;
    }

    /**
     * @return the totalWeight
     */
    public Double getTotalWeight() {
        return totalWeight;
    }

    /**
     * @param totalWeight
     *            the totalWeight to set
     */
    public void setTotalWeight(final Double totalWeight) {
        this.totalWeight = totalWeight;
    }


}

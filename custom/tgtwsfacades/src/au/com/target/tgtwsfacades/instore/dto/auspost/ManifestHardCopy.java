/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.auspost;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author smishra1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ManifestHardCopy {
    private String merchantLocationId;
    private String merchantId;
    private String ausPostAccountNumber;
    private String lodgementFacility;
    private Address storeAddress;
    private String manifestCreatedDate;
    private String manifestNumber;
    private List<ConsignmentSummary> consignmentSummaries;
    private Integer totalArticles;
    private Double totalWeight;

    /**
     * @return the merchantLocationId
     */
    public String getMerchantLocationId() {
        return merchantLocationId;
    }

    /**
     * @param merchantLocationId
     *            the merchantLocationId to set
     */
    public void setMerchantLocationId(final String merchantLocationId) {
        this.merchantLocationId = merchantLocationId;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId
     *            the merchantId to set
     */
    public void setMerchantId(final String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the ausPostAccountNumber
     */
    public String getAusPostAccountNumber() {
        return ausPostAccountNumber;
    }

    /**
     * @param ausPostAccountNumber
     *            the ausPostAccountNumber to set
     */
    public void setAusPostAccountNumber(final String ausPostAccountNumber) {
        this.ausPostAccountNumber = ausPostAccountNumber;
    }

    /**
     * @return the lodgementFacility
     */
    public String getLodgementFacility() {
        return lodgementFacility;
    }

    /**
     * @param lodgementFacility
     *            the lodgementFacility to set
     */
    public void setLodgementFacility(final String lodgementFacility) {
        this.lodgementFacility = lodgementFacility;
    }

    /**
     * @return the storeAddress
     */
    public Address getStoreAddress() {
        return storeAddress;
    }

    /**
     * @param storeAddress
     *            the storeAddress to set
     */
    public void setStoreAddress(final Address storeAddress) {
        this.storeAddress = storeAddress;
    }

    /**
     * @return the manifestCreatedDate
     */
    public String getManifestCreatedDate() {
        return manifestCreatedDate;
    }

    /**
     * @param manifestCreatedDate
     *            the manifestCreatedDate to set
     */
    public void setManifestCreatedDate(final String manifestCreatedDate) {
        this.manifestCreatedDate = manifestCreatedDate;
    }

    /**
     * @return the manifestNumber
     */
    public String getManifestNumber() {
        return manifestNumber;
    }

    /**
     * @param manifestNumber
     *            the manifestNumber to set
     */
    public void setManifestNumber(final String manifestNumber) {
        this.manifestNumber = manifestNumber;
    }

    /**
     * @return the consignmentSummaries
     */
    public List<ConsignmentSummary> getConsignmentSummaries() {
        return consignmentSummaries;
    }

    /**
     * @param consignmentSummaries
     *            the consignmentSummaries to set
     */
    public void setConsignmentSummaries(final List<ConsignmentSummary> consignmentSummaries) {
        this.consignmentSummaries = consignmentSummaries;
    }

    /**
     * @return the totalArticles
     */
    public Integer getTotalArticles() {
        return totalArticles;
    }

    /**
     * @param totalArticles
     *            the totalArticles to set
     */
    public void setTotalArticles(final Integer totalArticles) {
        this.totalArticles = totalArticles;
    }

    /**
     * @return the totalWeight
     */
    public Double getTotalWeight() {
        return totalWeight;
    }

    /**
     * @param totalWeight
     *            the totalWeight to set
     */
    public void setTotalWeight(final Double totalWeight) {
        this.totalWeight = totalWeight;
    }
}

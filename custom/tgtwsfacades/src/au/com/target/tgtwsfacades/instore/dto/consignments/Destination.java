/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

/**
 * @author Nandini
 *
 */
public class Destination {
    private String city;
    private String state;

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city
     *            the city to set
     */
    public void setCity(final String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state
     *            the state to set
     */
    public void setState(final String state) {
        this.state = state;
    }

}

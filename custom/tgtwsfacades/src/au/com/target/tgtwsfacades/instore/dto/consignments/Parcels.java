/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import java.util.List;


/**
 * @author rsamuel3
 *
 */
public class Parcels {

    private List<Parcel> parcels;

    /**
     * @return the parcels
     */
    public List<Parcel> getParcels() {
        return parcels;
    }

    /**
     * @param parcels
     *            the parcels to set
     */
    public void setParcels(final List<Parcel> parcels) {
        this.parcels = parcels;
    }




}

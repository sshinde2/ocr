/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * @author Nandini
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ProductStockLevelInstore {

    private String productCode;
    private String stockOnHand;
    private String stockLevel;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the stockOnHand
     */
    public String getStockOnHand() {
        return stockOnHand;
    }

    /**
     * @param stockOnHand
     *            the stockOnHand to set
     */
    public void setStockOnHand(final String stockOnHand) {
        this.stockOnHand = stockOnHand;
    }

    /**
     * @return the stockLevel
     */
    public String getStockLevel() {
        return stockLevel;
    }

    /**
     * @param stockLevel
     *            the stockLevel to set
     */
    public void setStockLevel(final String stockLevel) {
        this.stockLevel = stockLevel;
    }


}

/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import java.util.List;

import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtwsfacades.instore.dto.CustomParcelConfig;
import au.com.target.tgtwsfacades.instore.dto.StandardParcel;


/**
 * @author smishra1
 *
 */
public class StandardParcelDetailsData extends BaseResponseData {
    private List<StandardParcel> standardParcels;

    private CustomParcelConfig customParcels;

    /**
     * @return the standardParcels
     */
    public List<StandardParcel> getStandardParcels() {
        return standardParcels;
    }

    /**
     * @param standardParcels
     *            the standardParcels to set
     */
    public void setStandardParcels(final List<StandardParcel> standardParcels) {
        this.standardParcels = standardParcels;
    }

    /**
     * @return the customParcels
     */
    public CustomParcelConfig getCustomParcels() {
        return customParcels;
    }

    /**
     * @param customParcels
     *            the customParcels to set
     */
    public void setCustomParcels(final CustomParcelConfig customParcels) {
        this.customParcels = customParcels;
    }

}

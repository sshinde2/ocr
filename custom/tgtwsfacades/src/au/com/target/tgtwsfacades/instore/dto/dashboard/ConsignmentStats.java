/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.dashboard;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Class to hold the consignment stats to be displayed on Store Dashboard.
 * 
 * @author Rahul
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ConsignmentStats {

    private Integer open;

    private Integer inProgress;

    private Integer picked;

    private Integer completed;

    private Integer rejected;

    private Integer packed;

    /**
     * Gets the open orders.
     *
     * @return the open
     */
    public Integer getOpen() {
        return open;
    }

    /**
     * Sets the open.
     *
     * @param open
     *            the open to set
     */
    public void setOpen(final Integer open) {
        this.open = open;
    }

    /**
     * Gets the in progress orders.
     *
     * @return the inProgress
     */
    public Integer getInProgress() {
        return inProgress;
    }

    /**
     * Sets the inProgress.
     *
     * @param inProgress
     *            the inProgress to set
     */
    public void setInProgress(final Integer inProgress) {
        this.inProgress = inProgress;
    }

    /**
     * @return the picked
     */
    public Integer getPicked() {
        return picked;
    }

    /**
     * @param picked
     *            the picked to set
     */
    public void setPicked(final Integer picked) {
        this.picked = picked;
    }

    /**
     * Gets the completed orders.
     *
     * @return the completed
     */
    public Integer getCompleted() {
        return completed;
    }

    /**
     * Sets the completed.
     *
     * @param completed
     *            the completed to set
     */
    public void setCompleted(final Integer completed) {
        this.completed = completed;
    }

    /**
     * Gets the rejected orders.
     *
     * @return the rejected
     */
    public Integer getRejected() {
        return rejected;
    }

    /**
     * Sets the rejected.
     *
     * @param rejected
     *            the rejected to set
     */
    public void setRejected(final Integer rejected) {
        this.rejected = rejected;
    }

    /**
     * @return the packed
     */
    public Integer getPacked() {
        return packed;
    }

    /**
     * @param packed
     *            the packed to set
     */
    public void setPacked(final Integer packed) {
        this.packed = packed;
    }

}

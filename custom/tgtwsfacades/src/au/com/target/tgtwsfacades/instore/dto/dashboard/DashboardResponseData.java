/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.dashboard;

import au.com.target.tgtfacades.response.data.BaseResponseData;


/**
 * The Class DashboardResponseData.
 * 
 * @author Rahul
 */
public class DashboardResponseData extends BaseResponseData {

    private ConsignmentStats consignmentStats;

    /**
     * Gets the consignment stats.
     *
     * @return the consignmentStats
     */
    public ConsignmentStats getConsignmentStats() {
        return consignmentStats;
    }

    /**
     * Sets the consignment stats.
     *
     * @param consignmentStats
     *            the consignmentStats to set
     */
    public void setConsignmentStats(final ConsignmentStats consignmentStats) {
        this.consignmentStats = consignmentStats;
    }

}

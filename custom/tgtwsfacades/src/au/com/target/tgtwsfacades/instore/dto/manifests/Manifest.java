/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.manifests;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;


/**
 * @author Nandini
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Manifest {

    private String code;
    private Integer parcels;
    private String date;
    private boolean sent;
    private List<Consignment> consignments;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the parcels
     */
    public Integer getParcels() {
        return parcels;
    }

    /**
     * @param parcels
     *            the parcels to set
     */
    public void setParcels(final Integer parcels) {
        this.parcels = parcels;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(final String date) {
        this.date = date;
    }

    /**
     * @return the sent
     */
    public boolean isSent() {
        return sent;
    }

    /**
     * @param sent
     *            the sent to set
     */
    public void setSent(final boolean sent) {
        this.sent = sent;
    }

    /**
     * @return the consignments
     */
    public List<Consignment> getConsignments() {
        return consignments;
    }

    /**
     * @param consignments
     *            the consignments to set
     */
    public void setConsignments(final List<Consignment> consignments) {
        this.consignments = consignments;
    }

}

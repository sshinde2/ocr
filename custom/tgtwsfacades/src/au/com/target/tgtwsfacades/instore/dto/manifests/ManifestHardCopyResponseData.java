/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.manifests;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtwsfacades.instore.dto.auspost.ManifestHardCopy;


/**
 * @author smishra1
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ManifestHardCopyResponseData extends BaseResponseData {
    private ManifestHardCopy manifest;

    /**
     * @return the manifest
     */
    public ManifestHardCopy getManifest() {
        return manifest;
    }

    /**
     * @param manifest
     *            the manifest to set
     */
    public void setManifest(final ManifestHardCopy manifest) {
        this.manifest = manifest;
    }

}

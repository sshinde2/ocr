/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.user;

/**
 * @author sbryan6
 *
 */
public class Store {

    private Integer storeNumber;
    private String storeName;
    private boolean bigAndBulky;


    /**
     * @return the storeNumber
     */
    public Integer getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final Integer storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName
     *            the storeName to set
     */
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the onlyBigAndBulky
     */
    public boolean isBigAndBulky()
    {
        return bigAndBulky;
    }

    /**
     * @param bigAndBulky
     *            the onlyBigAndBulky flag to set
     */
    public void setBigAndBulky(final boolean bigAndBulky)
    {
        this.bigAndBulky = bigAndBulky;
    }
}

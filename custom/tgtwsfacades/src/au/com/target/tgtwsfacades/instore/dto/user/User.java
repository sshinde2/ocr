/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.user;

/**
 * @author rsamuel3
 *
 */
public class User {
    private String username;
    private String firstName;

    private Store store;

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * @param store
     *            the store to set
     */
    public void setStore(final Store store) {
        this.store = store;
    }


}

/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.user;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import au.com.target.tgtfacades.response.data.BaseResponseData;


/**
 * @author rsamuel3
 *
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UserResponseData extends BaseResponseData {
    private User user;

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     *            the user to set
     */
    public void setUser(final User user) {
        this.user = user;
    }

}

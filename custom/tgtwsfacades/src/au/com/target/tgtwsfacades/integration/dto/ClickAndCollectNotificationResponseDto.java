/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;


/**
 * @author pratik
 *
 */
@JsonRootName(value = "cnc-notification-response")
public class ClickAndCollectNotificationResponseDto {

    @JsonProperty
    private boolean success;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

/**
 * @author pthoma20
 *
 */
public class ConsignmentEntry {

    private String itemCode;

    private Integer quantity;

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final Integer quantity) {
        this.quantity = quantity;
    }

}


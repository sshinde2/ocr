/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;


/**
 * @author pthoma20
 *
 */
@JsonRootName(value = "customerSegmentImportResponse")
public class CustomerSegmentImportResponseDto {

    @JsonProperty
    private boolean success;

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success
     *            the success to set
     */
    public void setSuccess(final boolean success) {
        this.success = success;
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;


/**
 * @author pthoma20
 *
 */
@JsonRootName(value = "customerSegmentImportRequest")
public class CustomerSegmentInfoRequestDto {

    @JsonProperty
    private List<CustomerSegmentInfoDto> customerSegments;

    /**
     * @return the customerSegments
     */
    public List<CustomerSegmentInfoDto> getCustomerSegments() {
        return customerSegments;
    }

    /**
     * @param customerSegments
     *            the customerSegments to set
     */
    public void setCustomerSegments(final List<CustomerSegmentInfoDto> customerSegments) {
        this.customerSegments = customerSegments;
    }



}

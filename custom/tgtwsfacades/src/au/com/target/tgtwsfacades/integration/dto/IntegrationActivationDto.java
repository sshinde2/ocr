/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mmaki
 * 
 */
@XmlRootElement(name = "integration-activation")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationActivationDto {

    @XmlElement
    private String productCode;

    @XmlElement
    private String variantCode;

    @XmlElement
    private String approvalStatus;

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the variantCode
     */
    public String getVariantCode() {
        return variantCode;
    }

    /**
     * @param variantCode
     *            the variantCode to set
     */
    public void setVariantCode(final String variantCode) {
        this.variantCode = variantCode;
    }

    /**
     * @return the approvalStatus
     */
    public String getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * @param approvalStatus
     *            the approvalStatus to set
     */
    public void setApprovalStatus(final String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

}

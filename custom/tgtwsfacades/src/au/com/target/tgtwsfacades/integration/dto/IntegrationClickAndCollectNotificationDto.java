/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;


/**
 * @author rmcalave
 * 
 */
@JsonRootName(value = "integration-cnc-notification")
public class IntegrationClickAndCollectNotificationDto {
    @JsonProperty
    private String laybyNumber;

    @JsonProperty
    private String storeNumber;

    @JsonProperty
    private String letterType;

    /**
     * @return the laybyNumber
     */
    public String getLaybyNumber() {
        return laybyNumber;
    }

    /**
     * @param laybyNumber
     *            the laybyNumber to set
     */
    public void setLaybyNumber(final String laybyNumber) {
        this.laybyNumber = laybyNumber;
    }

    /**
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

    /**
     * @return the letterType
     */
    public String getLetterType() {
        return letterType;
    }

    /**
     * @param letterType
     *            the letterType to set
     */
    public void setLetterType(final String letterType) {
        this.letterType = letterType;
    }
}

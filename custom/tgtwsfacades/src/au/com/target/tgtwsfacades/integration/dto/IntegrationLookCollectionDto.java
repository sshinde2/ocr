/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mgazal
 * 
 */
@XmlRootElement(name = "collection")
public class IntegrationLookCollectionDto {

    private String code;

    private String name;

    private String description;

    private List<IntegrationLookDto> looks;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the looks
     */
    public List<IntegrationLookDto> getLooks() {
        return looks;
    }

    /**
     * @param looks
     *            the looks to set
     */
    public void setLooks(final List<IntegrationLookDto> looks) {
        this.looks = looks;
    }

}

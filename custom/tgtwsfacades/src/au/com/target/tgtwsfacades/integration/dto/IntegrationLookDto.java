/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mgazal
 * 
 */
@XmlRootElement(name = "look")
public class IntegrationLookDto {

    private String code;

    private String name;

    private String enabled;

    private String instagramURL;

    private String description;

    private String startDate;

    private String endDate;

    private Set<String> tags;

    private List<IntegrationLookProductDto> products;

    private String images;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the enabled
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * @param enabled
     *            the enabled to set
     */
    public void setEnabled(final String enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the instagramURL
     */
    public String getInstagramURL() {
        return instagramURL;
    }

    /**
     * @param instagramURL
     *            the instagramURL to set
     */
    public void setInstagramURL(final String instagramURL) {
        this.instagramURL = instagramURL;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(final String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(final String endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the tags
     */
    public Set<String> getTags() {
        return tags;
    }

    /**
     * @param tags
     *            the tags to set
     */
    public void setTags(final Set<String> tags) {
        this.tags = tags;
    }

    /**
     * @return the products
     */
    public List<IntegrationLookProductDto> getProducts() {
        return products;
    }

    /**
     * @param products
     *            the products to set
     */
    public void setProducts(final List<IntegrationLookProductDto> products) {
        this.products = products;
    }

    /**
     * @return the images
     */
    public String getImages() {
        return images;
    }

    /**
     * @param images
     *            the images to set
     */
    public void setImages(final String images) {
        this.images = images;
    }
}

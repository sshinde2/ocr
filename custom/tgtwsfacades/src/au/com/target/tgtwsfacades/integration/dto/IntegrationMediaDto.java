/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author rsamuel3
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationMediaDto {
    @XmlElement
    private String mediaFormat;
    @XmlElement
    private String mediaPath;
    @XmlElement
    private String url;

    /**
     * @return the mediaFormat
     */
    public String getMediaFormat() {
        return mediaFormat;
    }

    /**
     * @param mediaFormat
     *            the mediaFormat to set
     */
    public void setMediaFormat(final String mediaFormat) {
        this.mediaFormat = mediaFormat;
    }

    /**
     * @return the mediaPath
     */
    public String getMediaPath() {
        return mediaPath;
    }

    /**
     * @param mediaPath
     *            the mediaPath to set
     */
    public void setMediaPath(final String mediaPath) {
        this.mediaPath = mediaPath;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author rsamuel3
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationMediasDto {
    @XmlElement
    private List<IntegrationMediaDto> media;

    /**
     * @return the media
     */
    public List<IntegrationMediaDto> getMedia() {
        return media;
    }

    /**
     * @param media
     *            the media to set
     */
    public void setMedia(final List<IntegrationMediaDto> media) {
        this.media = media;
    }

    public void addIntegrationMediaDto(final IntegrationMediaDto mediaDto) {
        if (this.media == null) {
            this.media = new ArrayList<>();
        }
        this.media.add(mediaDto);
    }
}

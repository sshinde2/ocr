/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author fkratoch
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationOpeningDayDto {
    @XmlElement
    private Date openingTime;

    @XmlElement
    private Date closingTime;

    @XmlElement
    private Boolean closed;

    /**
     * @return the openingTime
     */
    public Date getOpeningTime() {
        return openingTime;
    }

    /**
     * @param openingTime
     *            the openingTime to set
     */
    public void setOpeningTime(final Date openingTime) {
        this.openingTime = openingTime;
    }

    /**
     * @return the closingTime
     */
    public Date getClosingTime() {
        return closingTime;
    }

    /**
     * @param closingTime
     *            the closingTime to set
     */
    public void setClosingTime(final Date closingTime) {
        this.closingTime = closingTime;
    }

    /**
     * @return the closed
     */
    public Boolean getClosed() {
        return closed;
    }

    /**
     * @param closed
     *            the closed to set
     */
    public void setClosed(final Boolean closed) {
        this.closed = closed;
    }
}

/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author fkratoch
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationOpeningScheduleDto {

    @XmlElement
    private String code;

    @XmlElement
    private String scheduleName;

    @XmlElement
    private String storeName;

    @XmlElement
    private String storeNumber;

    @XmlElement
    private List<IntegrationOpeningDayDto> openingDays;

    /**
     * 
     */
    public IntegrationOpeningScheduleDto() {
        super();
        openingDays = new ArrayList<>();
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the openingDays
     */
    public List<IntegrationOpeningDayDto> getOpeningDays() {
        return openingDays;
    }

    /**
     * @param openingDays
     *            the openingDays to set
     */
    public void setOpeningDays(final List<IntegrationOpeningDayDto> openingDays) {
        this.openingDays = openingDays;
    }

    /**
     * @return the scheduleName
     */
    public String getScheduleName() {
        return scheduleName;
    }

    /**
     * @param scheduleName
     *            the scheduleName to set
     */
    public void setScheduleName(final String scheduleName) {
        this.scheduleName = scheduleName;
    }

    /**
     * 
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * 
     * @param storeName
     *            the storeName to set
     */
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }

    /**
     * 
     * @return the storeNumber
     */
    public String getStoreNumber() {
        return storeNumber;
    }

    /**
     * 
     * @param storeNumber
     *            the storeNumber to set
     */
    public void setStoreNumber(final String storeNumber) {
        this.storeNumber = storeNumber;
    }

}

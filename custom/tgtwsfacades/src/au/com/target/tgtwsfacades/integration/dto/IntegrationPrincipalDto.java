/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "principal")
public class IntegrationPrincipalDto {
    @XmlElement(name = "uid")
    private String uid;

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public void setUid(final String uid) {
        this.uid = uid;
    }
}

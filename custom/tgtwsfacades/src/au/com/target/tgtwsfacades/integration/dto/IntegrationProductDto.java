/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import au.com.target.tgtutility.util.StringTools;


/**
 * @author fkratoch
 * 
 */
@XmlRootElement(name = "integration-product")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationProductDto {

    @XmlElement
    private Boolean isAssortment;

    @XmlElement
    private Boolean isStylegroup;

    @XmlElement
    private Boolean isSizeOnly;

    @XmlElement
    private String productCode;

    @XmlElement
    private String name;

    @XmlElement
    private String approvalStatus;

    @XmlElement
    private Integer department;

    @XmlElement
    private String description;

    @XmlElement
    private String webExtraInfo;

    @XmlElement
    private String webFeatures;

    @XmlElement
    private String materials;

    @XmlElement
    private String careInstructions;

    @XmlElement
    private String primaryCategory;

    @XmlElement
    private List<String> secondaryCategory;

    @XmlElement
    private String brand;

    @XmlElement
    private String license;

    @XmlElement
    private String productType;

    @XmlElement
    private String availableLayby;

    @XmlElement
    private String availableLongtermLayby;

    @XmlElement
    private String availableCnc;

    @XmlElement
    private String availableHomeDelivery;

    @XmlElement
    private String availableExpressDelivery;

    @XmlElement
    private String availableEbayExpressDelivery;

    @XmlElement
    private String availableDigitalDelivery;

    @XmlElement
    private String availableOnEbay;

    @XmlElement
    private String clearance;

    @XmlElement
    private String youtube;

    @XmlElement
    private Double ageFrom;

    @XmlElement
    private Double ageTo;

    @XmlElement
    private String variantCode;

    @XmlElement
    private String colour;

    @XmlElement
    private String swatch;

    @XmlElement
    private String ean;

    @XmlElement
    private Integer maxOrderQty;

    @XmlElement
    private String articleStatus;

    @XmlElement
    private String sizeType;

    @XmlElement
    private String size;

    @XmlElement
    private String bulky;

    @XmlElement
    private Boolean showWasPrice;

    @XmlElement
    private String productOnlineDate;

    @XmlElement
    private String productOfflineDate;

    @XmlElement
    private String showIfOutOfStock;

    @XmlElement
    private Double pkgWeight;

    @XmlElement
    private Double pkgHeight;

    @XmlElement
    private Double pkgLength;

    @XmlElement
    private Double pkgWidth;

    @XmlElement
    private String giftcardBrandId;

    @XmlElement
    private String giftcardProductId;

    @XmlElement
    private Double denominaton;

    @XmlElement
    private String giftcardStyle;

    @XmlElement
    private List<String> genders;

    @XmlElement
    private IntegrationProductMediaDto productMedia;

    @XmlElement
    private final List<IntegrationVariantDto> variants;

    @XmlElement
    private String sizeGroup;

    @XmlElement
    private List<String> warehouses;

    @XmlElement
    private List<String> excludePaymentMethods;

    @XmlElement
    private String showInStoreStock;

    @XmlElement
    private String preview;

    @XmlElement
    private String displayOnly;

    private String assorted;

    @XmlElement
    private String originalCategory;

    @XmlElement
    private String fit;

    @XmlElement
    private String material;

    @XmlElement
    private String trend;

    @XmlElement
    private String giftsFor;

    @XmlElement
    private String platform;

    @XmlElement
    private String genre;

    @XmlElement
    private String season;

    @XmlElement
    private String type;

    @XmlElement
    private String occassion;

    @XmlElement
    private String merchProductStatus;

    @XmlElement
    private String preOrderStartDate;

    @XmlElement
    private String preOrderEndDate;

    @XmlElement
    private Integer preOrderOnlineQuantity;

    @XmlElement
    private String preOrderEmbargoReleaseDate;

    public IntegrationProductDto() {
        super();
        genders = new ArrayList<>();
        variants = new ArrayList<>();
    }

    /**
     * @return originalCategory
     */
    public String getOriginalCategory() {
        return originalCategory;
    }

    /**
     * @param originalCategory
     */
    public void setOriginalCategory(final String originalCategory) {
        this.originalCategory = originalCategory;
    }


    /**
     * @return the isAssortment
     */
    public Boolean getIsAssortment() {
        return isAssortment;
    }

    /**
     * @param isAssortment
     *            the isAssortmentp to set
     */
    public void setIsAssortment(final Boolean isAssortment) {
        this.isAssortment = isAssortment;
    }

    /**
     * @return the isStylegroup
     */
    public Boolean getIsStylegroup() {
        return isStylegroup;
    }


    /**
     * @param isStylegroup
     *            the isStylegroup to set
     */
    public void setIsStylegroup(final Boolean isStylegroup) {
        this.isStylegroup = isStylegroup;
    }


    /**
     * @return the isSizeOnly
     */
    public Boolean getIsSizeOnly() {
        return isSizeOnly;
    }


    /**
     * @param isSizeOnly
     *            the isSizeOnly to set
     */
    public void setIsSizeOnly(final Boolean isSizeOnly) {
        this.isSizeOnly = isSizeOnly;
    }


    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }


    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode != null ? StringTools.trimLeadingZeros(productCode) : null;
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }


    /**
     * @return the approvalStatus
     */
    public String getApprovalStatus() {
        return approvalStatus;
    }


    /**
     * @param approvalStatus
     *            the approvalStatus to set
     */
    public void setApprovalStatus(final String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }


    /**
     * @return the department
     */
    public Integer getDepartment() {
        return department;
    }


    /**
     * @param department
     *            the department to set
     */
    public void setDepartment(final Integer department) {
        this.department = department;
    }


    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }


    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }


    /**
     * @return the webExtraInfo
     */
    public String getWebExtraInfo() {
        return webExtraInfo;
    }


    /**
     * @param webExtraInfo
     *            the webExtraInfo to set
     */
    public void setWebExtraInfo(final String webExtraInfo) {
        this.webExtraInfo = webExtraInfo;
    }


    /**
     * @return the webFeatures
     */
    public String getWebFeatures() {
        return webFeatures;
    }


    /**
     * @param webFeatures
     *            the webFeatures to set
     */
    public void setWebFeatures(final String webFeatures) {
        this.webFeatures = webFeatures;
    }


    /**
     * @return the materials
     */
    public String getMaterials() {
        return materials;
    }


    /**
     * @param materials
     *            the materials to set
     */
    public void setMaterials(final String materials) {
        this.materials = materials;
    }


    /**
     * @return the careInstructions
     */
    public String getCareInstructions() {
        return careInstructions;
    }


    /**
     * @param careInstructions
     *            the careInstructions to set
     */
    public void setCareInstructions(final String careInstructions) {
        this.careInstructions = careInstructions;
    }


    /**
     * @return the primaryCategory
     */
    public String getPrimaryCategory() {
        return primaryCategory;
    }


    /**
     * @param primaryCategory
     *            the primaryCategory to set
     */
    public void setPrimaryCategory(final String primaryCategory) {
        this.primaryCategory = primaryCategory;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }


    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }


    /**
     * @return the license
     */
    public String getLicense() {
        return license;
    }


    /**
     * @param license
     *            the license to set
     */
    public void setLicense(final String license) {
        this.license = license;
    }


    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }


    /**
     * @param productType
     *            the productType to set
     */
    public void setProductType(final String productType) {
        this.productType = productType;
    }

    /**
     * @return the youtube
     */
    public String getYoutube() {
        return youtube;
    }


    /**
     * @param youtube
     *            the youtube to set
     */
    public void setYoutube(final String youtube) {
        this.youtube = youtube;
    }


    /**
     * @return the variantCode
     */
    public String getVariantCode() {
        return variantCode;
    }


    /**
     * @param variantCode
     *            the variantCode to set
     */
    public void setVariantCode(final String variantCode) {
        this.variantCode = variantCode != null ? StringTools.trimLeadingZeros(variantCode) : null;
    }


    /**
     * @return the color
     */
    public String getColour() {
        return colour;
    }


    /**
     * @param colour
     *            the color to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }


    /**
     * @return the swatch
     */
    public String getSwatch() {
        return swatch;
    }


    /**
     * @param swatch
     *            the swatch to set
     */
    public void setSwatch(final String swatch) {
        this.swatch = swatch;
    }


    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }


    /**
     * @param ean
     *            the ean to set
     */
    public void setEan(final String ean) {
        this.ean = ean;
    }


    /**
     * @return the maxOrderQty
     */
    public Integer getMaxOrderQty() {
        return maxOrderQty;
    }


    /**
     * @param maxOrderQty
     *            the maxOrderQty to set
     */
    public void setMaxOrderQty(final Integer maxOrderQty) {
        this.maxOrderQty = maxOrderQty;
    }


    /**
     * @return the articleStatus
     */
    public String getArticleStatus() {
        return articleStatus;
    }


    /**
     * @param articleStatus
     *            the articleStatus to set
     */
    public void setArticleStatus(final String articleStatus) {
        this.articleStatus = articleStatus;
    }


    /**
     * @return the sizeType
     */
    public String getSizeType() {
        return sizeType;
    }


    /**
     * @param sizeType
     *            the sizeType to set
     */
    public void setSizeType(final String sizeType) {
        this.sizeType = sizeType;
    }


    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }


    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }


    /**
     * @return the bulky
     */
    public String getBulky() {
        return bulky;
    }


    /**
     * @param bulky
     *            the bulky to set
     */
    public void setBulky(final String bulky) {
        this.bulky = bulky;
    }


    /**
     * @return the variants
     */
    public List<IntegrationVariantDto> getVariants() {
        return variants;
    }


    /**
     * @return the availableLayby
     */
    public String getAvailableLayby() {
        return availableLayby;
    }


    /**
     * @param availableLayby
     *            the availableLayby to set
     */
    public void setAvailableLayby(final String availableLayby) {
        this.availableLayby = availableLayby;
    }

    /**
     * @return the availableLongtermLayby
     */
    public String getAvailableLongtermLayby() {
        return availableLongtermLayby;
    }


    /**
     * @param availableLongtermLayby
     *            the availableLongtermLayby to set
     */
    public void setAvailableLongtermLayby(final String availableLongtermLayby) {
        this.availableLongtermLayby = availableLongtermLayby;
    }


    /**
     * @return the availableCnc
     */
    public String getAvailableCnc() {
        return availableCnc;
    }


    /**
     * @param availableCnc
     *            the availableCnc to set
     */
    public void setAvailableCnc(final String availableCnc) {
        this.availableCnc = availableCnc;
    }


    /**
     * @return the availableHomeDelivery
     */
    public String getAvailableHomeDelivery() {
        return availableHomeDelivery;
    }


    /**
     * @param availableHomeDelivery
     *            the availableHomeDelivery to set
     */
    public void setAvailableHomeDelivery(final String availableHomeDelivery) {
        this.availableHomeDelivery = availableHomeDelivery;
    }


    /**
     * @return the availableExpressDelivery
     */
    public String getAvailableExpressDelivery() {
        return availableExpressDelivery;
    }


    /**
     * @param availableExpressDelivery
     *            the availableExpressDelivery to set
     */
    public void setAvailableExpressDelivery(final String availableExpressDelivery) {
        this.availableExpressDelivery = availableExpressDelivery;
    }


    /**
     * @return the availableEbayExpressDelivery
     */
    public String getAvailableEbayExpressDelivery() {
        return availableEbayExpressDelivery;
    }



    /**
     * @param availableEbayExpressDelivery
     *            the availableEbayExpressDelivery to set
     */
    public void setAvailableEbayExpressDelivery(final String availableEbayExpressDelivery) {
        this.availableEbayExpressDelivery = availableEbayExpressDelivery;
    }



    /**
     * @return the availableDigitalDelivery
     */
    public String getAvailableDigitalDelivery() {
        return availableDigitalDelivery;
    }



    /**
     * @param availableDigitalDelivery
     *            the availableDigitalDelivery to set
     */
    public void setAvailableDigitalDelivery(final String availableDigitalDelivery) {
        this.availableDigitalDelivery = availableDigitalDelivery;
    }



    /**
     * @return the showWasPrice
     */
    public Boolean getShowWasPrice() {
        return showWasPrice;
    }


    /**
     * @param showWasPrice
     *            the showWasPrice to set
     */
    public void setShowWasPrice(final Boolean showWasPrice) {
        this.showWasPrice = showWasPrice;
    }


    /**
     * @return the ageFrom
     */
    public Double getAgeFrom() {
        return ageFrom;
    }


    /**
     * @param ageFrom
     *            the ageFrom to set
     */
    public void setAgeFrom(final Double ageFrom) {
        this.ageFrom = ageFrom;
    }


    /**
     * @return the ageTo
     */
    public Double getAgeTo() {
        return ageTo;
    }


    /**
     * @param ageTo
     *            the ageTo to set
     */
    public void setAgeTo(final Double ageTo) {
        this.ageTo = ageTo;
    }

    /**
     * @return the genders
     */
    public List<String> getGenders() {
        return genders;
    }

    public void setGenders(final List<String> genders) {
        this.genders = genders;
    }


    /**
     * @return the productMedia
     */
    public IntegrationProductMediaDto getProductMedia() {
        return productMedia;
    }


    /**
     * @param productMediadto
     *            the productMedia to set
     */
    public void setProductMedia(final IntegrationProductMediaDto productMediadto) {
        this.productMedia = productMediadto;
    }

    /**
     * @return the productOnlineDate
     */
    public String getProductOnlineDate() {
        return productOnlineDate;
    }

    /**
     * @param productFromDate
     *            the productOnlineDate to set
     */
    public void setProductOnlineDate(final String productFromDate) {
        this.productOnlineDate = productFromDate;
    }

    /**
     * @return the productOfflineDate
     */
    public String getProductOfflineDate() {
        return productOfflineDate;
    }

    /**
     * @param productToDate
     *            the productOfflineDate to set
     */
    public void setProductOfflineDate(final String productToDate) {
        this.productOfflineDate = productToDate;
    }

    /**
     * @return the showIfOutOfStock
     */
    public String getShowIfOutOfStock() {
        return showIfOutOfStock;
    }

    /**
     * @param showIfOutOfStock
     *            the showIfOutOfStock to set
     */
    public void setShowIfOutOfStock(final String showIfOutOfStock) {
        this.showIfOutOfStock = showIfOutOfStock;
    }



    /**
     * @return the availableOnEbay
     */
    public String getAvailableOnEbay() {
        return availableOnEbay;
    }



    /**
     * @param availableOnEbay
     *            the availableOnEbay to set
     */
    public void setAvailableOnEbay(final String availableOnEbay) {
        this.availableOnEbay = availableOnEbay;
    }

    /**
     * @return the clearance
     */
    public String getClearance() {
        return clearance;
    }

    /**
     * @param clearance
     *            the clearance to set
     */
    public void setClearance(final String clearance) {
        this.clearance = clearance;
    }


    /**
     * @return the pkgWeight
     */
    public Double getPkgWeight() {
        return pkgWeight;
    }



    /**
     * @param pkgWeight
     *            the pkgWeight to set
     */
    public void setPkgWeight(final Double pkgWeight) {
        this.pkgWeight = pkgWeight;
    }



    /**
     * @return the pkgHeight
     */
    public Double getPkgHeight() {
        return pkgHeight;
    }



    /**
     * @param pkgHeight
     *            the pkgHeight to set
     */
    public void setPkgHeight(final Double pkgHeight) {
        this.pkgHeight = pkgHeight;
    }



    /**
     * @return the pkgLength
     */
    public Double getPkgLength() {
        return pkgLength;
    }



    /**
     * @param pkgLength
     *            the pkgLength to set
     */
    public void setPkgLength(final Double pkgLength) {
        this.pkgLength = pkgLength;
    }



    /**
     * @return the pkgWidth
     */
    public Double getPkgWidth() {
        return pkgWidth;
    }



    /**
     * @param pkgWidth
     *            the pkgWidth to set
     */
    public void setPkgWidth(final Double pkgWidth) {
        this.pkgWidth = pkgWidth;
    }

    /**
     * @return giftcardBrandId
     */
    public String getGiftcardBrandId() {
        return giftcardBrandId;
    }

    /**
     * set the giftcardBrandId
     * 
     * @param giftcardBrandId
     */
    public void setGiftcardBrandId(final String giftcardBrandId) {
        this.giftcardBrandId = giftcardBrandId;
    }

    /**
     * @return giftcardProductId
     */
    public String getGiftcardProductId() {
        return giftcardProductId;
    }

    /**
     * set the giftcardProductId
     * 
     * @param giftcardProductId
     */
    public void setGiftcardProductId(final String giftcardProductId) {
        this.giftcardProductId = giftcardProductId;
    }

    /**
     * @return denominaton
     */
    public Double getDenominaton() {
        return denominaton;
    }

    /**
     * set the denominaton
     * 
     * @param denominaton
     */
    public void setDenominaton(final Double denominaton) {
        this.denominaton = denominaton;
    }

    /**
     * @return giftcardStyle
     */
    public String getGiftcardStyle() {
        return giftcardStyle;
    }

    /**
     * set the giftcardStyle
     * 
     * @param giftcardStyle
     */
    public void setGiftcardStyle(final String giftcardStyle) {
        this.giftcardStyle = giftcardStyle;
    }



    /**
     * @return the secondaryCategory
     */
    public List<String> getSecondaryCategory() {
        return secondaryCategory;
    }



    /**
     * @param secondaryCategory
     *            the secondaryCategory to set
     */
    public void setSecondaryCategory(final List<String> secondaryCategory) {
        this.secondaryCategory = secondaryCategory;
    }

    public void addVariant(final IntegrationVariantDto variantDto) {
        this.variants.add(variantDto);
    }



    /**
     * @return the sizeGroup
     */
    public String getSizeGroup() {
        return sizeGroup;
    }



    /**
     * @param sizeGroup
     *            the sizeGroup to set
     */
    public void setSizeGroup(final String sizeGroup) {
        this.sizeGroup = sizeGroup;
    }

    /**
     * @return the warehouses
     */
    public List<String> getWarehouses() {
        return warehouses;
    }

    /**
     * @param warehouses
     *            the warehouses to set
     */
    public void setWarehouses(final List<String> warehouses) {
        this.warehouses = warehouses;
    }

    /**
     * @return the showInStoreStock
     */
    public String getShowInStoreStock() {
        return showInStoreStock;
    }

    /**
     * @param showInStoreStock
     *            the showInStoreStock to set
     */
    public void setShowInStoreStock(final String showInStoreStock) {
        this.showInStoreStock = showInStoreStock;
    }

    /**
     * @return the preview
     */
    public String getPreview() {
        return preview;
    }

    /**
     * @param preview
     *            the preview to set
     */
    public void setPreview(final String preview) {
        this.preview = preview;
    }

    /**
     * @return the displayOnly
     */
    public String getDisplayOnly() {
        return displayOnly;
    }

    /**
     * @param displayOnly
     *            the displayOnly to set
     */
    public void setDisplayOnly(final String displayOnly) {
        this.displayOnly = displayOnly;
    }

    /**
     * @return the assorted
     */
    public String getAssorted() {
        return assorted;
    }

    /**
     * @param assorted
     *            the assorted to set
     */
    public void setAssorted(final String assorted) {
        this.assorted = assorted;
    }

    /**
     * @return the fit
     */
    public String getFit() {
        return fit;
    }

    /**
     * @param fit
     *            the fit to set
     */
    public void setFit(final String fit) {
        this.fit = fit;
    }

    /**
     * @return the material
     */
    public String getMaterial() {
        return material;
    }

    /**
     * @param material
     *            the material to set
     */
    public void setMaterial(final String material) {
        this.material = material;
    }

    /**
     * @return the trend
     */
    public String getTrend() {
        return trend;
    }

    /**
     * @param trend
     *            the trend to set
     */
    public void setTrend(final String trend) {
        this.trend = trend;
    }

    /**
     * @return the giftsFor
     */
    public String getGiftsFor() {
        return giftsFor;
    }

    /**
     * @param giftsFor
     *            the giftsFor to set
     */
    public void setGiftsFor(final String giftsFor) {
        this.giftsFor = giftsFor;
    }

    /**
     * @return the platfom
     */
    public String getPlatform() {
        return platform;
    }

    /**
     * @param platform
     *            the platfom to set
     */
    public void setPlatform(final String platform) {
        this.platform = platform;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @param genre
     *            the genre to set
     */
    public void setGenre(final String genre) {
        this.genre = genre;
    }

    /**
     * @return the season
     */
    public String getSeason() {
        return season;
    }

    /**
     * @param season
     *            the season to set
     */
    public void setSeason(final String season) {
        this.season = season;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final String type) {
        this.type = type;
    }

    /**
     * @return the occassion
     */
    public String getOccassion() {
        return occassion;
    }

    /**
     * @param occassion
     *            the occassion to set
     */
    public void setOccassion(final String occassion) {
        this.occassion = occassion;
    }

    /**
     * @return the merchProductStatus
     */
    public String getMerchProductStatus() {
        return merchProductStatus;
    }

    /**
     * @param merchProductStatus
     *            the merchProductStatus to set
     */
    public void setMerchProductStatus(final String merchProductStatus) {
        this.merchProductStatus = merchProductStatus;
    }

    /**
     * @return the preOrderStartDate
     */
    public String getPreOrderStartDate() {
        return preOrderStartDate;
    }

    /**
     * @param preOrderStartDate
     *            the preOrderStartDate to set
     */
    public void setPreOrderStartDate(final String preOrderStartDate) {
        this.preOrderStartDate = preOrderStartDate;
    }

    /**
     * @return the preOrderEndDate
     */
    public String getPreOrderEndDate() {
        return preOrderEndDate;
    }

    /**
     * @param preOrderEndDate
     *            the preOrderEndDate to set
     */
    public void setPreOrderEndDate(final String preOrderEndDate) {
        this.preOrderEndDate = preOrderEndDate;
    }

    /**
     * @return the preOrderQuantity
     */
    public Integer getPreOrderOnlineQuantity() {
        return preOrderOnlineQuantity;
    }

    /**
     * @param preOrderOnlineQuantity
     *            the preOrderOnlineQuantity to set
     */
    public void setPreOrderOnlineQuantity(final Integer preOrderOnlineQuantity) {
        this.preOrderOnlineQuantity = preOrderOnlineQuantity;
    }

    /**
     * @return the embargoReleaseDate
     */
    public String getPreOrderEmbargoReleaseDate() {
        return preOrderEmbargoReleaseDate;
    }

    /**
     * @param preOrderEmbargoReleaseDate
     *            the embargoReleaseDate to set
     */
    public void setPreOrderEmbargoReleaseDate(final String preOrderEmbargoReleaseDate) {
        this.preOrderEmbargoReleaseDate = preOrderEmbargoReleaseDate;
    }

    /**
     *
     * @return
     */
    public List<String> getExcludePaymentMethods() {
        return excludePaymentMethods;
    }

    /**
     *
     * @param excludePaymentMethods
     */
    public void setExcludePaymentMethods(final List<String> excludePaymentMethods) {
        this.excludePaymentMethods = excludePaymentMethods;
    }

}

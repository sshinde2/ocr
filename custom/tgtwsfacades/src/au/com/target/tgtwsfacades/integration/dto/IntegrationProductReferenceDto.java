/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;


/**
 * @author fkratoch
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationProductReferenceDto implements Serializable {

    @XmlElement
    private String code;

    @XmlElement
    private IntegrationCrossReferenceProduct refProducts;

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the refProducts
     */
    public IntegrationCrossReferenceProduct getRefProducts() {
        return refProducts;
    }

    /**
     * @param refProducts
     *            the refProducts to set
     */
    public void setRefProducts(final IntegrationCrossReferenceProduct refProducts) {
        this.refProducts = refProducts;
    }


}

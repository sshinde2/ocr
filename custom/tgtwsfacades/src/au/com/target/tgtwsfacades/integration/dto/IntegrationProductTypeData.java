/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author fkratoch
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationProductTypeData {


    @XmlElement
    private List<String> type;

    /**
     * @return the type
     */
    public List<String> getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(final List<String> type) {
        this.type = type;
    }

}

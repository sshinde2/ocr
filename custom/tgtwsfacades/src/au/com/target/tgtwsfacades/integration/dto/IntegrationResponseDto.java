/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author fkratoch
 * 
 */
@XmlRootElement(name = "integration-response")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationResponseDto {
    @XmlElement
    private String code;

    @XmlElement
    private boolean successStatus;

    @XmlElement
    private List<String> messages;

    @XmlElement
    private String additionalInfo;

    public IntegrationResponseDto() {
        super();
        messages = new ArrayList<>();
    }

    public IntegrationResponseDto(final String code) {
        super();
        this.code = code;
        messages = new ArrayList<>();
    }

    public IntegrationResponseDto(final String code, final String additionalInfo) {
        this(code);
        this.additionalInfo = additionalInfo;
    }

    /**
     * @return the productCode
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the successStatus
     */
    public boolean isSuccessStatus() {
        return successStatus;
    }

    /**
     * @param successStatus
     *            the successStatus to set
     */
    public void setSuccessStatus(final boolean successStatus) {
        this.successStatus = successStatus;
    }

    /**
     * @return the messages
     */
    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(final List<String> msgs) {
        this.messages = msgs;
    }

    public void addMessage(final String msg) {
        messages.add(msg);
    }

    public void addMessages(final List<String> msgs) {
        messages.addAll(msgs);
    }

    /**
     * @return the additionalInfo
     */
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * @param additionalInfo
     *            the additionalInfo to set
     */
    public void setAdditionalInfo(final String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}

/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rsamuel3
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationSecondaryImagesDto {
    @XmlElement
    private List<String> secondaryImage;

    /**
     * @return the secondaryImage
     */
    public List<String> getSecondaryImage() {
        return secondaryImage;
    }

    /**
     * @param secondaryImage
     *            the secondaryImage to set
     */
    public void setSecondaryImage(final List<String> secondaryImage) {
        this.secondaryImage = secondaryImage;
    }

    public void addSecondaryImage(final String secondaryImageDto) {
        if (this.secondaryImage == null) {
            this.secondaryImage = new ArrayList<>();
        }
        this.secondaryImage.add(secondaryImageDto);
    }
}

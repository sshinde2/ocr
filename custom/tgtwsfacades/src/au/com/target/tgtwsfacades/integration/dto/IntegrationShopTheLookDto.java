/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mgazal
 * 
 */
@XmlRootElement(name = "shopTheLook")
public class IntegrationShopTheLookDto {

    private String code;

    private String name;

    private String description;

    private IntegrationSupercategoryDto supercategory;

    private List<IntegrationLookCollectionDto> collections;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the supercategory
     */
    public IntegrationSupercategoryDto getSupercategory() {
        return supercategory;
    }

    /**
     * @param supercategory
     *            the supercategory to set
     */
    public void setSupercategory(final IntegrationSupercategoryDto supercategory) {
        this.supercategory = supercategory;
    }

    /**
     * @return the collections
     */
    public List<IntegrationLookCollectionDto> getCollections() {
        return collections;
    }

    /**
     * @param collections
     *            the collections to set
     */
    public void setCollections(final List<IntegrationLookCollectionDto> collections) {
        this.collections = collections;
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author mmaki
 * 
 */
@XmlRootElement(name = "integration-stockUpdate")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationStockUpdateDto {

    @XmlElement
    private String transactionDate;

    @XmlElement
    private String transactionNumber;

    @XmlElement
    private String itemCode;

    @XmlElement
    private String warehouse;

    @XmlElement
    private String comment;

    @XmlElement
    private String itemPrimaryBarcode;

    @XmlElement
    private Integer totalQuantityAvailable;

    @XmlElement
    private Integer adjustmentQuantity;

    /**
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate
     *            the transactionDate to set
     */
    public void setTransactionDate(final String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return the transactionNumber
     */
    public String getTransactionNumber() {
        return transactionNumber;
    }

    /**
     * @param transactionNumber
     *            the transactionNumber to set
     */
    public void setTransactionNumber(final String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse
     *            the warehouse to set
     */
    public void setWarehouse(final String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment
     *            the comment to set
     */
    public void setComment(final String comment) {
        this.comment = comment;
    }

    /**
     * @return the itemPrimaryBarcode
     */
    public String getItemPrimaryBarcode() {
        return itemPrimaryBarcode;
    }

    /**
     * @param itemPrimaryBarcode
     *            the itemPrimaryBarcode to set
     */
    public void setItemPrimaryBarcode(final String itemPrimaryBarcode) {
        this.itemPrimaryBarcode = itemPrimaryBarcode;
    }

    /**
     * @return the totalQuantityAvailable
     */
    public Integer getTotalQuantityAvailable() {
        return totalQuantityAvailable;
    }

    /**
     * @param totalQuantityAvailable
     *            the totalQuantityAvailable to set
     */
    public void setTotalQuantityAvailable(final Integer totalQuantityAvailable) {
        this.totalQuantityAvailable = totalQuantityAvailable;
    }

    /**
     * @return the adjustmentQuantity
     */
    public Integer getAdjustmentQuantity() {
        return adjustmentQuantity;
    }

    /**
     * @param adjustmentQuantity
     *            the adjustmentQuantity to set
     */
    public void setAdjustmentQuantity(final Integer adjustmentQuantity) {
        this.adjustmentQuantity = adjustmentQuantity;
    }
}

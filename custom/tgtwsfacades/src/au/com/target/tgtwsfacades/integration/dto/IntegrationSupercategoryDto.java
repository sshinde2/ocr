/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "supercategory")
public class IntegrationSupercategoryDto {

    @XmlElement(name = "code")
    private String code;

    public IntegrationSupercategoryDto() {
    }

    /**
     * @param code
     */
    public IntegrationSupercategoryDto(final String code) {
        super();
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }
}

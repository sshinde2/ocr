/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author rmcalave
 * 
 */
@XmlRootElement(name = "targetproductcategory")
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationTargetProductCategoryDto {
    @XmlElement
    private String code;

    @XmlElement
    private String name;

    @XmlElementWrapper(name = "allowedPrincipals")
    @XmlElement(name = "principal")
    private List<IntegrationPrincipalDto> allowedPrincipals;

    @XmlElementWrapper(name = "supercategories")
    @XmlElement(name = "category")
    private List<IntegrationSupercategoryDto> supercategories;

    /**
     * @return the code
     */
    @XmlAttribute(name = "code")
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the allowedPrincipals
     */
    public List<IntegrationPrincipalDto> getAllowedPrincipals() {
        return allowedPrincipals;
    }

    /**
     * @param allowedPrincipals
     *            the allowedPrincipals to set
     */
    public void setAllowedPrincipals(final List<IntegrationPrincipalDto> allowedPrincipals) {
        this.allowedPrincipals = allowedPrincipals;
    }

    /**
     * @return the supercategories
     */
    public List<IntegrationSupercategoryDto> getSupercategories() {
        return supercategories;
    }

    /**
     * @param supercategories
     *            the supercategories to set
     */
    public void setSupercategories(final List<IntegrationSupercategoryDto> supercategories) {
        this.supercategories = supercategories;
    }
}

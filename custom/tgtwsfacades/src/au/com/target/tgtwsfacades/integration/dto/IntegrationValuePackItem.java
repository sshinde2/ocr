/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;


/**
 * @author mmaki
 * 
 */
public class IntegrationValuePackItem implements Serializable {

    private static final long serialVersionUID = 1L;

    private String dealType;

    private String childSKU;

    private String childSKUDescription;

    private Double price;

    private Integer quantity;

    /**
     * @return the dealType
     */
    public String getDealType() {
        return dealType;
    }

    /**
     * @param dealType
     *            the dealType to set
     */
    public void setDealType(final String dealType) {
        this.dealType = dealType;
    }

    /**
     * @return the childSKU
     */
    public String getChildSKU() {
        return childSKU;
    }

    /**
     * @param childSKU
     *            the childSKU to set
     */
    public void setChildSKU(final String childSKU) {
        this.childSKU = childSKU;
    }

    /**
     * @return the childSKUDescription
     */
    public String getChildSKUDescription() {
        return childSKUDescription;
    }

    /**
     * @param childSKUDescription
     *            the childSKUDescription to set
     */
    public void setChildSKUDescription(final String childSKUDescription) {
        this.childSKUDescription = childSKUDescription;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final Double price) {
        this.price = price;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(final Integer quantity) {
        this.quantity = quantity;
    }

}

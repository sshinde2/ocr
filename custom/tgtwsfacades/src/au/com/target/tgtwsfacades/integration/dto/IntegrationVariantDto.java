/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import au.com.target.tgtutility.util.StringTools;


/**
 * @author fkratoch
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IntegrationVariantDto {

    @XmlElement
    private String baseProduct;

    @XmlElement
    private String variantCode;

    @XmlElement
    private String name;

    @XmlElement
    private String approvalStatus;

    @XmlElement
    private String size;

    @XmlElement
    private String ean;

    @XmlElement
    private String availableOnEbay;

    @XmlElement
    private Double pkgWeight;

    @XmlElement
    private Double pkgHeight;

    @XmlElement
    private Double pkgLength;

    @XmlElement
    private Double pkgWidth;

    @XmlElement
    private String displayOnly;

    @XmlElement
    private String merchProductStatus;

    /**
     * @return the baseProduct
     */
    public String getBaseProduct() {
        return baseProduct;
    }

    /**
     * @param baseProduct
     *            the baseProduct to set
     */
    public void setBaseProduct(final String baseProduct) {
        this.baseProduct = baseProduct != null ? StringTools.trimLeadingZeros(baseProduct) : null;
    }

    /**
     * @return the variantCode
     */
    public String getVariantCode() {
        return variantCode;
    }

    /**
     * @param variantCode
     *            the variantCode to set
     */
    public void setVariantCode(final String variantCode) {
        this.variantCode = variantCode != null ? StringTools.trimLeadingZeros(variantCode) : null;
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the approvalStatus
     */
    public String getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * @param approvalStatus
     *            the approvalStatus to set
     */
    public void setApprovalStatus(final String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }

    /**
     * @param ean
     *            the ean to set
     */
    public void setEan(final String ean) {
        this.ean = ean;
    }

    /**
     * @return the availableOnEbay
     */
    public String getAvailableOnEbay() {
        return availableOnEbay;
    }

    /**
     * @param availableOnEbay
     *            the availableOnEbay to set
     */
    public void setAvailableOnEbay(final String availableOnEbay) {
        this.availableOnEbay = availableOnEbay;
    }

    /**
     * @return the pkgWeight
     */
    public Double getPkgWeight() {
        return pkgWeight;
    }

    /**
     * @param pkgWeight
     *            the pkgWeight to set
     */
    public void setPkgWeight(final Double pkgWeight) {
        this.pkgWeight = pkgWeight;
    }

    /**
     * @return the pkgHeight
     */
    public Double getPkgHeight() {
        return pkgHeight;
    }

    /**
     * @param pkgHeight
     *            the pkgHeight to set
     */
    public void setPkgHeight(final Double pkgHeight) {
        this.pkgHeight = pkgHeight;
    }

    /**
     * @return the pkgLength
     */
    public Double getPkgLength() {
        return pkgLength;
    }

    /**
     * @param pkgLength
     *            the pkgLength to set
     */
    public void setPkgLength(final Double pkgLength) {
        this.pkgLength = pkgLength;
    }

    /**
     * @return the pkgWidth
     */
    public Double getPkgWidth() {
        return pkgWidth;
    }

    /**
     * @param pkgWidth
     *            the pkgWidth to set
     */
    public void setPkgWidth(final Double pkgWidth) {
        this.pkgWidth = pkgWidth;
    }

    /**
     * @return the displayOnly
     */
    public String getDisplayOnly() {
        return displayOnly;
    }

    /**
     * @param displayOnly
     *            the displayOnly to set
     */
    public void setDisplayOnly(final String displayOnly) {
        this.displayOnly = displayOnly;
    }

    /**
     * @return the merchProductStatus
     */
    public String getMerchProductStatus() {
        return merchProductStatus;
    }

    /**
     * @param merchProductStatus
     *            the merchProductStatus to set
     */
    public void setMerchProductStatus(final String merchProductStatus) {
        this.merchProductStatus = merchProductStatus;
    }
}

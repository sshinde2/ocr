/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.ArrayList;
import java.util.List;


/**
 * @author rsamuel3
 * 
 */
public class Orders {
    private List<Order> orders;

    /**
     * @return the orders
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * @param orders
     *            the orders to set
     */
    public void setOrders(final List<Order> orders) {
        this.orders = orders;
    }

    public void addOrder(final String orderNumber) {
        if (this.orders == null) {
            this.orders = new ArrayList<>();
        }
        final Order order = new Order();
        order.setOrderNumber(orderNumber);
        this.orders.add(order);
    }
}

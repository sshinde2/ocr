/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;
import java.util.List;


/**
 * @author mmaki
 * 
 */
public class PickConfirmEntries implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<au.com.target.tgtfulfilment.dto.PickConfirmEntry> pickConfirmEntries;

    /**
     * @return the pickConfirmEntries
     */
    public List<au.com.target.tgtfulfilment.dto.PickConfirmEntry> getPickConfirmEntries() {
        return pickConfirmEntries;
    }

    /**
     * @param pickConfirmEntries
     *            the pickConfirmEntries to set
     */
    public void setPickConfirmEntries(final List<au.com.target.tgtfulfilment.dto.PickConfirmEntry> pickConfirmEntries) {
        this.pickConfirmEntries = pickConfirmEntries;
    }
}

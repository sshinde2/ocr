/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;



/**
 * @author rsamuel3
 *
 */
/**
 * 
 */
public class PickConfirmEntry extends au.com.target.tgtfulfilment.dto.PickConfirmEntry {

    private static final long serialVersionUID = 1L;

    private String itemEan;

    public String getItemEan() {
        return itemEan;
    }

    public void setItemEan(final String itemEan) {
        this.itemEan = itemEan;
    }
}

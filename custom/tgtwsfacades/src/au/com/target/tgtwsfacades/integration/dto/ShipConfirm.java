package au.com.target.tgtwsfacades.integration.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Olivier Lamy
 */
public class ShipConfirm
        implements Serializable
{
    private List<OrderConfirm> orderConfirms;

    public ShipConfirm()
    {
        //no op
    }

    public ShipConfirm(final List<OrderConfirm> orderConfirms)
    {
        this.orderConfirms = orderConfirms;
    }

    public List<OrderConfirm> getOrderConfirms()
    {
        if (orderConfirms == null)
        {
            orderConfirms = new ArrayList<>();
        }
        return orderConfirms;
    }

    public void setOrderConfirms(final List<OrderConfirm> orderConfirms)
    {
        this.orderConfirms = orderConfirms;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("ShipConfirm{");
        sb.append("orderConfirms=").append(orderConfirms);
        sb.append('}');
        return sb.toString();
    }
}

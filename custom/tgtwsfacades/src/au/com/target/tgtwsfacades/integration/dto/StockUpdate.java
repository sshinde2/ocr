package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author Olivier Lamy
 */
@XmlRootElement(name = "stock-update")
@XmlAccessorType(XmlAccessType.FIELD)
public class StockUpdate
{
    private String productCode;

    private int quantity;

    private String warehouseCode;

    public StockUpdate()
    {
        // no op
    }

    public StockUpdate(final String productCode, final int quantity)
    {
        this.productCode = productCode;
        this.quantity = quantity;
    }

    public String getProductCode()
    {
        return productCode;
    }

    public void setProductCode(final String productCode)
    {
        this.productCode = productCode;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public void setQuantity(final int quantity)
    {
        this.quantity = quantity;
    }

    public String getWarehouseCode()
    {
        return warehouseCode;
    }

    public void setWarehouseCode(final String warehouseCode)
    {
        this.warehouseCode = warehouseCode;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("StockUpdate{");
        sb.append("productCode='").append(productCode).append('\'');
        sb.append(", quantity=").append(quantity);
        sb.append(", warehouseCode='").append(warehouseCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

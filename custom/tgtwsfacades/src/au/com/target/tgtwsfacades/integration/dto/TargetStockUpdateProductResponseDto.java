/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author bhuang3
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "principal")
public class TargetStockUpdateProductResponseDto {

    public static final String STATUS_OK = "OK";
    public static final String STATUS_ERROR = "ERROR";
    public static final String STATUS_NOT_FOUND = "NOT_FOUND";

    @XmlElement
    private String statusCode;

    @XmlElement
    private String message;

    @XmlElement
    private String itemCode;

    @XmlElement
    private String itemPrimaryBarcode;

    @XmlElement
    private Integer totalQuantityAvailable;

    @XmlElement
    private Integer adjustmentQuantity;

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode
     *            the statusCode to set
     */
    public void setStatusCode(final String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @param itemCode
     *            the itemCode to set
     */
    public void setItemCode(final String itemCode) {
        this.itemCode = itemCode;
    }

    /**
     * @return the itemPrimaryBarcode
     */
    public String getItemPrimaryBarcode() {
        return itemPrimaryBarcode;
    }

    /**
     * @param itemPrimaryBarcode
     *            the itemPrimaryBarcode to set
     */
    public void setItemPrimaryBarcode(final String itemPrimaryBarcode) {
        this.itemPrimaryBarcode = itemPrimaryBarcode;
    }

    /**
     * @return the totalQuantityAvailable
     */
    public Integer getTotalQuantityAvailable() {
        return totalQuantityAvailable;
    }

    /**
     * @param totalQuantityAvailable
     *            the totalQuantityAvailable to set
     */
    public void setTotalQuantityAvailable(final Integer totalQuantityAvailable) {
        this.totalQuantityAvailable = totalQuantityAvailable;
    }

    /**
     * @return the adjustmentQuantity
     */
    public Integer getAdjustmentQuantity() {
        return adjustmentQuantity;
    }

    /**
     * @param adjustmentQuantity
     *            the adjustmentQuantity to set
     */
    public void setAdjustmentQuantity(final Integer adjustmentQuantity) {
        this.adjustmentQuantity = adjustmentQuantity;
    }


}

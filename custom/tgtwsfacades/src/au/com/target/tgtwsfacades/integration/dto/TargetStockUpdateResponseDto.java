/**
 * 
 */
package au.com.target.tgtwsfacades.integration.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author bhuang3
 *
 */
@XmlRootElement(name = "stockUpdateResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class TargetStockUpdateResponseDto {

    public static final String STATUS_OK = "OK";
    public static final String STATUS_ERROR = "ERROR";
    public static final String STATUS_EXCEED_MAX_BATCH_SIZE = "EXCEED_MAX_BATCH_SIZE";

    @XmlElement
    private String statusCode;

    @XmlElement
    private String message;

    @XmlElement
    private String transactionDate;

    @XmlElement
    private String transactionNumber;

    @XmlElementWrapper(name = "failedItems")
    @XmlElement(name = "failedItem")
    private List<TargetStockUpdateProductResponseDto> failedItems;

    /**
     * @return the statusCode
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode
     *            the statusCode to set
     */
    public void setStatusCode(final String statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * @return the transactionDate
     */
    public String getTransactionDate() {
        return transactionDate;
    }

    /**
     * @param transactionDate
     *            the transactionDate to set
     */
    public void setTransactionDate(final String transactionDate) {
        this.transactionDate = transactionDate;
    }

    /**
     * @return the transactionNumber
     */
    public String getTransactionNumber() {
        return transactionNumber;
    }

    /**
     * @param transactionNumber
     *            the transactionNumber to set
     */
    public void setTransactionNumber(final String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    /**
     * @return the failedItems
     */
    public List<TargetStockUpdateProductResponseDto> getFailedItems() {
        return failedItems;
    }

    /**
     * @param failedItems
     *            the failedItems to set
     */
    public void setFailedItems(final List<TargetStockUpdateProductResponseDto> failedItems) {
        this.failedItems = failedItems;
    }


}

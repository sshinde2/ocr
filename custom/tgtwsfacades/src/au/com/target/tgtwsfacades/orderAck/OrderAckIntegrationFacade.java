/**
 * 
 */
package au.com.target.tgtwsfacades.orderAck;

import java.util.List;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.Orders;



/**
 * @author rsamuel3
 * 
 */
public interface OrderAckIntegrationFacade {

    /**
     * Call the business process to update the ship acknowledgement of the orders in the list
     * 
     * @param orders
     * @return IntegrationResponse for each order indicating whether successful or failure
     */
    List<IntegrationResponseDto> updateOrderAcknowledgements(Orders orders);
}

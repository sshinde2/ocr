/**
 * 
 */
package au.com.target.tgtwsfacades.pickconfirm;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.PickConfirmOrder;



/**
 * @author mmaki
 * 
 */
public interface PickConfirmIntegrationFacade {

    /**
     * Call the business process to update the order in the pick confirm
     * 
     * @param pickConfirmOrder
     * @return IntegrationResponse
     */
    IntegrationResponseDto handlePickConfirm(PickConfirmOrder pickConfirmOrder);
}

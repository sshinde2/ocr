/**
 * 
 */
package au.com.target.tgtwsfacades.pickconfirm.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.dto.PickConfirmEntry;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtutility.constants.TgtutilityConstants.ErrorCode;
import au.com.target.tgtutility.util.SplunkLogFormatter;
import au.com.target.tgtwsfacades.helper.OrderConsignmentHelper;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.PickConfirmOrder;
import au.com.target.tgtwsfacades.pickconfirm.PickConfirmIntegrationFacade;


/**
 * @author mmaki
 * 
 */
public class PickConfirmIntegrationFacadeImpl implements PickConfirmIntegrationFacade {

    private static final Logger LOG = Logger.getLogger(PickConfirmIntegrationFacadeImpl.class);

    private TargetFulfilmentService targetFulfilmentService;

    private OrderConsignmentHelper orderConsignmentHelper;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.pickconfirm.PickConfirmIntegrationFacade#handlePickConfirm(PickConfirmOrder)
     */
    @Override
    public IntegrationResponseDto handlePickConfirm(final PickConfirmOrder pickConfirmOrder) {
        final IntegrationResponseDto response;
        if (pickConfirmOrder == null || pickConfirmOrder.getOrderNumber() == null) {
            response = new IntegrationResponseDto(null);
            response.setSuccessStatus(false);
            response.addMessage("No order number found from PickConfirmOrder");
            LOG.error(SplunkLogFormatter.formatMessage(
                    "Pick confirmation does not have an order number", ErrorCode.ERR_TGTFULLFILLMENT));
        }
        else {
            final String orderConsignmentIdentifier = pickConfirmOrder.getOrderNumber();
            response = new IntegrationResponseDto(orderConsignmentIdentifier);
            try {
                validateTrackingNumber(pickConfirmOrder.getConsignmentNumber());
                final List<PickConfirmEntry> pickConfirmEntries = pickConfirmOrder
                        .getPickConfirmEntries().getPickConfirmEntries();
                final TargetConsignmentModel consignment = orderConsignmentHelper
                        .getConsignmentBasedOrderOrConsignmentCode(orderConsignmentIdentifier);
                if (consignment != null) {
                    targetFulfilmentService.processPickConfForConsignment(consignment.getOrder().getCode(),
                            consignment,
                            pickConfirmOrder.getCarrier(), pickConfirmOrder.getConsignmentNumber(),
                            pickConfirmOrder.getParcelCount(), pickConfirmEntries, LoggingContext.WAREHOUSE);
                    response.setSuccessStatus(true);
                }
                else {
                    response.addMessage("No consignment found from PickConfirmOrder in the warehouse");
                    LOG.error(SplunkLogFormatter.formatMessage(
                            "No consignment for order number : " + orderConsignmentIdentifier
                                    + " in any warehouse for PickConfirmation",
                            ErrorCode.ERR_TGTFULLFILLMENT));
                }
            }
            catch (final Exception e) {
                LOG.error(SplunkLogFormatter.formatOrderMessage("Exception processing pick confirmation",
                        ErrorCode.ERR_TGTFULLFILLMENT, orderConsignmentIdentifier), e);
                response.setSuccessStatus(false);
                response.addMessage("Runtime exception occurred: " + e.getMessage());
            }

        }

        return response;
    }

    private void validateTrackingNumber(final String trackingNumber) throws FulfilmentException {
        if (trackingNumber == null) {
            throw new FulfilmentException("Tracking number is null");
        }
    }

    /**
     * @param targetFulfilmentService
     *            the targetFulfilmentService to set
     */
    @Required
    public void setTargetFulfilmentService(final TargetFulfilmentService targetFulfilmentService) {
        this.targetFulfilmentService = targetFulfilmentService;
    }

    /**
     * @param orderConsignmentHelper
     *            the orderConsignmentHelper to set
     */
    @Required
    public void setOrderConsignmentHelper(final OrderConsignmentHelper orderConsignmentHelper) {
        this.orderConsignmentHelper = orderConsignmentHelper;
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.pinpad;

import au.com.target.tgtshareddto.eftwrapper.dto.EftPaymentDto;
import au.com.target.tgtshareddto.eftwrapper.dto.HybrisResponseDto;


/**
 * @author rsamuel3
 * 
 */
public interface PinPadPaymentFacade {

    /**
     * updates hybris with the payment details
     * 
     * @param paymentDto
     * @return response indicating whether it was a success or failure
     */
    public HybrisResponseDto updatePinPadPayment(EftPaymentDto paymentDto);
}

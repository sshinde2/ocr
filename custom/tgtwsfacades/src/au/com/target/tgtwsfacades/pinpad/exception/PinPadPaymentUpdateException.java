/**
 * 
 */
package au.com.target.tgtwsfacades.pinpad.exception;

/**
 * Represent an unrecoverable exception in pinpad payment update facade
 * 
 */
public class PinPadPaymentUpdateException extends Exception {

    public PinPadPaymentUpdateException(final String msg) {
        super(msg);
    }

    public PinPadPaymentUpdateException(final String msg, final Exception e) {
        super(msg, e);
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.pos;

import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;


/**
 * @author Olivier Lamy
 */
public interface HotCardUpdateFacade {


    /**
     * 
     * @param hotcardNumber
     *            as the name parameter says this is the card number
     * @param toAdd
     *            <code>true</code> for create cards, <code>false</code> to remove the card
     */
    void updateHotCardStatus(final long hotcardNumber, final boolean toAdd) throws ModelSavingException,
            TargetUnknownIdentifierException, TargetAmbiguousIdentifierException;

}

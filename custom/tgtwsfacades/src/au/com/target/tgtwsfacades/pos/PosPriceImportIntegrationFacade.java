/**
 * 
 */
package au.com.target.tgtwsfacades.pos;

import java.util.List;

import au.com.target.tgtsale.integration.dto.IntegrationPosProductItemDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * @author fkratoch
 * 
 */
public interface PosPriceImportIntegrationFacade {
    IntegrationResponseDto importProductPriceFromPos(final List<IntegrationPosProductItemDto> dtos);
}

/**
 * 
 */
package au.com.target.tgtwsfacades.productimport;

import au.com.target.tgtwsfacades.integration.dto.IntegrationAssetImportDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;



/**
 * @author rsamuel3
 * 
 */
public interface AssetImportIntegrationFacade {
    IntegrationResponseDto importTargetAssets(IntegrationAssetImportDto integrationAssetsDto);
}

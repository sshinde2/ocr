/**
 * 
 */
package au.com.target.tgtwsfacades.productimport;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationTargetProductCategoryDto;



/**
 * @author rmcalave
 * 
 */
public interface CategoryImportIntegrationFacade {

    /**
     * Create or update a category based on the information provided, including supercategories and allowed principals.
     * 
     * @param integrationTargetProductCategoryDto
     *            The payload containing the category information
     * @return the response containing a status and messages (if any)
     */
    IntegrationResponseDto persistTargetCategory(
            IntegrationTargetProductCategoryDto integrationTargetProductCategoryDto);

}
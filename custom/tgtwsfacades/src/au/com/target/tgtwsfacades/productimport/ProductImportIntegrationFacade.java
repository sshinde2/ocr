/**
 * 
 */
package au.com.target.tgtwsfacades.productimport;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferenceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductReferencesDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;



/**
 * @author fkratoch
 * 
 */
public interface ProductImportIntegrationFacade {


    IntegrationResponseDto persistTargetProduct(IntegrationProductDto integrationProductDto);

    IntegrationResponseDto generateProductCrossReferences(IntegrationProductReferencesDto integrationReferencesDto);

    IntegrationResponseDto generateProductCrossReference(IntegrationProductReferenceDto integrationReferenceDto);
}

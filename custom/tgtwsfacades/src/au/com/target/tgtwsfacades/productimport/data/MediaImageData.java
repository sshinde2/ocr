/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.data;

import java.io.IOException;
import java.io.InputStream;


/**
 * @author rsamuel3
 * 
 */
public class MediaImageData {


    private InputStream imageData;
    private String filename;
    private String mimetype;

    public MediaImageData(final InputStream inputStream, final String filename, final String mimetype)
            throws IOException {
        setFilename(filename);
        setMimetype(mimetype);
        setImageData(inputStream);
    }

    /**
     * @return the imageData
     */
    public InputStream getImageData() {
        return imageData;
    }

    /**
     * @param imageData
     *            the imageData to set
     */
    public void setImageData(final InputStream imageData) {
        this.imageData = imageData;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename
     *            the filename to set
     */
    public void setFilename(final String filename) {
        this.filename = filename;
    }

    /**
     * @return the mimetype
     */
    public String getMimetype() {
        return mimetype;
    }

    /**
     * @param mimetype
     *            the mimetype to set
     */
    public void setMimetype(final String mimetype) {
        this.mimetype = mimetype;
    }


}

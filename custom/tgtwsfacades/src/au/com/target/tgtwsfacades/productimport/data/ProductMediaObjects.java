/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.data;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author rsamuel3
 * 
 */
public class ProductMediaObjects {
    private String primaryMediaCode;
    private Map<String, MediaModel> primaryImages;
    private List<MediaContainerModel> mediaContainers;
    private List<String> errors;
    private boolean validPrimaryImages;

    public ProductMediaObjects(final String assetId) {
        this.primaryMediaCode = assetId;
    }

    /**
     * @return the primaryImages
     */
    public Map<String, MediaModel> getPrimaryImages() {
        return primaryImages;
    }

    /**
     * @param primaryImages
     *            the primaryImages to set
     */
    public void setPrimaryImages(final Map<String, MediaModel> primaryImages) {
        this.primaryImages = primaryImages;
    }


    /**
     * @param format
     * @param primaryMedia
     */
    public void addPrimaryImages(final String format, final MediaModel primaryMedia) {
        if (this.primaryImages == null) {
            this.primaryImages = new HashMap<String, MediaModel>();
        }
        this.primaryImages.put(format, primaryMedia);
    }

    public String getPrimaryImageKey(final String type) {
        return type + primaryMediaCode;
    }

    /**
     * @return the mediaContainers
     */
    public List<MediaContainerModel> getMediaContainers() {
        return mediaContainers;
    }

    /**
     * @param mediaContainers
     *            the mediaContainers to set
     */
    public void setMediaContainers(final List<MediaContainerModel> mediaContainers) {
        this.mediaContainers = mediaContainers;
    }

    /**
     * @param mediaContainer
     *            the mediaContainers to set
     */
    public void addMediaContainers(final MediaContainerModel mediaContainer) {
        if (this.mediaContainers == null) {
            this.mediaContainers = new ArrayList<>();
        }
        this.mediaContainers.add(mediaContainer);
    }

    /**
     * @return the errors
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * @param errors
     *            the errors to set
     */
    public void setErrors(final List<String> errors) {
        this.errors = errors;
    }

    public void addErrors(final String error) {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }
        this.errors.add(error);
    }

    /**
     * @return the valid
     */
    public boolean hasValidPrimaryImages() {
        return validPrimaryImages;
    }

    /**
     * @param valid
     *            the valid to set
     */
    public void setHasValidPrimaryImages(final boolean valid) {
        this.validPrimaryImages = valid;
    }

    /**
     * @return the assetId
     */
    public String getPrimaryMediaCode() {
        return primaryMediaCode;
    }

    /**
     * @param assetId
     *            the assetId to set
     */
    public void setPrimaryMediaCode(final String assetId) {
        this.primaryMediaCode = assetId;
    }
}

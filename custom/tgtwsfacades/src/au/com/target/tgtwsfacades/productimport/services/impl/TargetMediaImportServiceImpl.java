/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.services.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.media.TargetMediaContainerService;
import au.com.target.tgtwsfacades.productimport.data.ProductMediaObjects;
import au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;




/**
 * @author rsamuel3
 * 
 */
public class TargetMediaImportServiceImpl implements TargetMediaImportService {

    private static final Logger LOG = Logger.getLogger(TargetMediaImportServiceImpl.class);

    private static final String INFO_ASSETIMPORT_OBJECTCREATION = "INFO-ASSETIMPORT-%sCREATION : %s with ID %s created"
            + " for asset %s";

    private static final String INFO_ASSETIMPORT_MEDIAASSETNOTFOUND = "INFO-ASSETIMPORT-MEDIAFOLDERNOTFOUND : Media "
            + "%s with ID %s was not found. Hence creating it in hybris. ";


    @Autowired
    private ModelService modelService;

    @Autowired
    private MediaService mediaService;

    @Autowired
    private TargetMediaContainerService mediaContainerService;

    @Autowired
    private TargetProductImportUtil targetProductImportUtil;

    @Override
    public void removeMedia(final String code) {
        final MediaModel media = getMediaModel(code);
        if (media != null) {
            modelService.remove(media);
        }
    }

    /**
     * @param qualifier
     * @param assetId
     * @return MediaFolder either created if not available
     */
    @Override
    public MediaFolderModel getMediaFolder(final String qualifier, final String assetId) {
        MediaFolderModel folderModel = null;
        try {
            folderModel = mediaService.getFolder(qualifier);
        }
        catch (final UnknownIdentifierException uie) {
            LOG.info(String.format(INFO_ASSETIMPORT_MEDIAASSETNOTFOUND, "folder", qualifier));
            folderModel = null;
        }
        if (folderModel == null) {
            folderModel = modelService.create(MediaFolderModel.class);
            folderModel.setQualifier(qualifier);

            modelService.save(folderModel);
            LOG.info(String.format(INFO_ASSETIMPORT_OBJECTCREATION, "MEDIAFOLDER", "Media Folder", qualifier, assetId));
        }
        return folderModel;
    }

    /**
     * @param qualifier
     * @return MediaContainerModel related to the qualifier as in hybris
     */
    @Override
    public MediaContainerModel getOrCreateMediaContainer(final String qualifier) {
        MediaContainerModel mediaContainer = getMediaContainer(qualifier);

        if (mediaContainer == null) {
            mediaContainer = modelService.create(MediaContainerModel.class);

            mediaContainer.setQualifier(qualifier);
            mediaContainer.setName(qualifier);
            mediaContainer.setCatalogVersion(targetProductImportUtil.getProductCatalogStagedVersion());

            //            modelService.save(mediaContainer);
            LOG.info(String.format(INFO_ASSETIMPORT_OBJECTCREATION, "MEDIACONTAINER", "Media Container", qualifier,
                    qualifier));
        }
        return mediaContainer;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService#getOrCreateMediaContainer(java.lang.String, de.hybris.platform.catalog.model.CatalogVersionModel)
     */
    @Override
    public MediaContainerModel getOrCreateMediaContainer(final String qualifier,
            final CatalogVersionModel catalogVersion) {
        MediaContainerModel mediaContainer = getMediaContainer(qualifier, catalogVersion);

        if (mediaContainer == null) {
            mediaContainer = modelService.create(MediaContainerModel.class);

            mediaContainer.setQualifier(qualifier);
            mediaContainer.setName(qualifier);
            mediaContainer.setCatalogVersion(catalogVersion);

            LOG.info(String.format(INFO_ASSETIMPORT_OBJECTCREATION, "MEDIACONTAINER", "Media Container", qualifier,
                    qualifier));
        }
        return mediaContainer;
    }

    /**
     * @param qualifier
     * @return MediaContainerModel related to the qualifier as in hybris
     */
    @Override
    public MediaContainerModel getMediaContainer(final String qualifier) {
        MediaContainerModel mediaContainer = null;
        try {
            mediaContainer = mediaContainerService.getMediaContainerForQualifier(qualifier);
        }
        catch (final UnknownIdentifierException uie) {
            LOG.info(String.format(INFO_ASSETIMPORT_MEDIAASSETNOTFOUND, "container", qualifier));
            mediaContainer = null;
        }
        return mediaContainer;
    }

    @Override
    public MediaContainerModel getMediaContainer(final String qualifier, final CatalogVersionModel catalogVersion) {
        try {
            return mediaContainerService.getMediaContainerForQualifier(qualifier, catalogVersion);
        }
        catch (final UnknownIdentifierException uie) {
            LOG.info(String.format(INFO_ASSETIMPORT_MEDIAASSETNOTFOUND, "container", qualifier));
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.productimport.services.TargetMediaImportService#getMediaModel(java.lang.String)
     */
    @Override
    public MediaModel getOrCreateMediaModel(final String qualifier) {
        MediaModel mediaModel = getMediaModel(qualifier);

        if (mediaModel == null) {
            mediaModel = modelService.create(MediaModel.class);
            mediaModel.setCode(qualifier);
        }
        return mediaModel;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService#getOrCreateMediaModel(java.lang.String, de.hybris.platform.catalog.model.CatalogVersionModel)
     */
    @Override
    public MediaModel getOrCreateMediaModel(final String qualifier, final CatalogVersionModel catalogVersion) {
        MediaModel mediaModel = getMediaModel(qualifier, catalogVersion);

        if (mediaModel == null) {
            mediaModel = modelService.create(MediaModel.class);
            mediaModel.setCode(qualifier);
            mediaModel.setCatalogVersion(catalogVersion);
        }
        return mediaModel;
    }

    /**
     * @param qualifier
     */
    @Override
    public MediaModel getMediaModel(final String qualifier) {
        MediaModel mediaModel = null;
        try {
            mediaModel = mediaService.getMedia(qualifier);
        }
        catch (final UnknownIdentifierException uie) {
            LOG.info(
                    "ERR-ASSETIMPORT-MEDIAFORMATMISSING : Could not find the mediaformat and an error was returned : ",
                    uie);
            mediaModel = null;
        }
        return mediaModel;
    }

    @Override
    public MediaModel getMediaModel(final String qualifier, final CatalogVersionModel catalogVersion) {
        try {
            return mediaService.getMedia(catalogVersion, qualifier);
        }
        catch (final UnknownIdentifierException uie) {
            LOG.info("INFO-ASSETIMPORT-MEDIANOTFOUND : Media with ID: " + qualifier + " and catalogVersion: "
                    + catalogVersion + " was not found. ");
        }
        return null;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.productimport.services.TargetMediaImportService#getMediaFormatModel(java.lang.String)
     */
    @Override
    public MediaFormatModel getMediaFormatModel(final String qualifier) {
        //setup media format
        MediaFormatModel mediaformat = null;
        if (StringUtils.isNotBlank(qualifier)) {
            try {
                mediaformat = mediaService.getFormat(qualifier);
            }
            catch (final UnknownIdentifierException uie) {
                LOG.error(
                        "ERR-ASSETIMPORT-MEDIAFORMATMISSING : Could not find the mediaformat and an error was returned : ",
                        uie);
                mediaformat = null;
            }
        }
        return mediaformat;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.productimport.services.TargetMediaImportService#getMediaObjects(java.lang.String)
     */
    @Override
    public ProductMediaObjects getMediaObjects(final String productCode, final String primaryQualifier,
            final String swatchImage, final List<String> secondaryQualifier,final  List<String> wideQualifier) {
        final ProductMediaObjects mediaObjects = new ProductMediaObjects(primaryQualifier);
        getPrimaryImages(productCode, mediaObjects, primaryQualifier);

        boolean hasImages = false;
        //gallery also requires the primary qualifier..hence adding the primary qualifier;
        if (StringUtils.isNotBlank(primaryQualifier)) {
            populatePrimaryMediaContainer(mediaObjects, primaryQualifier, swatchImage);
            hasImages = true;
        }

        if (CollectionUtils.isNotEmpty(secondaryQualifier)) {
            hasImages = true;
            for (final String qualifier : secondaryQualifier) {
                populateMediaContainers(mediaObjects, qualifier);
            }
        }

        if (CollectionUtils.isNotEmpty(wideQualifier)) {
            hasImages = true;
            for (final String qualifier : wideQualifier) {
                populateWideMediaContainer(mediaObjects, qualifier);
            }
        }

        if (!hasImages) {
            final String error = String.format(
                    "ERR-PRODUCTMEDIA-NOSECONDARYIMAGES: No secondary images defined for the product with ID %s",
                    productCode);
            LOG.error(error);
            mediaObjects.addErrors(error);
        }
        return mediaObjects;
    }


    /**
     * @param mediaObjects
     * @param qualifier
     */
    private void populateWideMediaContainer(final ProductMediaObjects mediaObjects, final String qualifier) {
        MediaContainerModel container = null;
        try {
            container = getMediaContainer(qualifier);
        }
        catch (final AmbiguousIdentifierException aie) {
            final String error = String
                    .format(
                            "ERR-PRODUCTMEDIA-NOMEDIACONTAINER: More than one media container was found in hybris with the qualifier %s",
                            qualifier);
            LOG.error(error);
            mediaObjects.addErrors(error);
            return;
        }
        if (container == null) {
            final String error = String.format(
                    "ERR-PRODUCTMEDIA-NOMEDIACONTAINER: No media container was found in hybris with the qualifier %s",
                    qualifier);
            LOG.error(error);
            mediaObjects.addErrors(error);
        }
        else {
            mediaObjects.addMediaContainers(container);
        }
    }
    /**
     * @param mediaObjects
     * @param qualifier
     */
    public void populateMediaContainers(final ProductMediaObjects mediaObjects, final String qualifier) {
        MediaContainerModel container = null;
        try {
            container = getMediaContainer(qualifier);
        }
        catch (final AmbiguousIdentifierException aie) {
            final String error = String
                    .format(
                            "ERR-PRODUCTMEDIA-NOMEDIACONTAINER: More than one media container was found in hybris with the qualifier %s",
                            qualifier);
            LOG.error(error);
            mediaObjects.addErrors(error);
            return;
        }
        if (container == null) {
            final String error = String.format(
                    "ERR-PRODUCTMEDIA-NOMEDIACONTAINER: No media container was found in hybris with the qualifier %s",
                    qualifier);
            LOG.error(error);
            mediaObjects.addErrors(error);
        }
        else {
            mediaObjects.addMediaContainers(container);
        }
    }

    /**
     * @param mediaObjects
     * @param qualifier
     */
    public void populatePrimaryMediaContainer(final ProductMediaObjects mediaObjects, final String qualifier,
            final String swatchQualifier) {
        MediaContainerModel container = null;
        try {
            container = getMediaContainer(qualifier);
        }
        catch (final AmbiguousIdentifierException aie) {
            final String error = String
                    .format(
                            "ERR-PRODUCTMEDIA-NOMEDIACONTAINER: More than one media container was found in hybris with the qualifier %s",
                            qualifier);
            LOG.error(error);
            mediaObjects.addErrors(error);
            return;
        }


        if (container == null) {
            final String error = String.format(
                    "ERR-PRODUCTMEDIA-NOMEDIACONTAINER: No media container was found in hybris with the qualifier %s",
                    qualifier);
            LOG.error(error);
            mediaObjects.addErrors(error);
        }
        else {
            if (StringUtils.isNotBlank(swatchQualifier)) {
                final MediaContainerModel swatchContainer = getMediaContainer(swatchQualifier);

                if (swatchContainer != null) {
                    final MediaModel swatchMedia = getSwatchFromContainer(swatchContainer.getMedias());
                    if (swatchMedia != null) {
                        replaceSwatchImage(container, swatchMedia);
                    }
                }
            }
            mediaObjects.addMediaContainers(container);
        }
    }

    /**
     * replaces the swatch image in primary container with swatch provided
     * 
     * @param container
     * @param swatchMedia
     */
    private void replaceSwatchImage(final MediaContainerModel container, final MediaModel swatchMedia) {
        final Collection<MediaModel> mediasInContainer = container.getMedias();
        final MediaModel swatchMediaInContainer = getSwatchFromContainer(mediasInContainer);

        if (swatchMedia != null && swatchMediaInContainer != null) {
            mediaContainerService
                    .removeMediaFromContainer(container, Collections.singletonList(swatchMediaInContainer));
            mediaContainerService.addMediaToContainer(container, Collections.singletonList(swatchMedia));
        }
        else if (swatchMedia != null) {
            mediaContainerService.addMediaToContainer(container, Collections.singletonList(swatchMedia));
        }
    }

    /**
     * Gets the MediaModel which is the swatch images from the container
     * 
     * @param mediasInContainer
     * @return media model which is the swatch image
     */
    public MediaModel getSwatchFromContainer(final Collection<MediaModel> mediasInContainer) {
        return (MediaModel)CollectionUtils.find(mediasInContainer, new Predicate() {

            @Override
            public boolean evaluate(final Object arg0) {
                final MediaModel media = (MediaModel)arg0;
                return media.getCode().startsWith(SWATCH_MEDIA);
            }
        });
    }

    /**
     * @param mediaObjects
     * @param qualifier
     */
    private void getPrimaryImages(final String productCode, final ProductMediaObjects mediaObjects,
            final String qualifier) {
        if (StringUtils.isNotBlank(qualifier)) {
            boolean isValid = true;
            String qualifierToRetrieve = HERO_MEDIA + qualifier;
            isValid = getPrimaryMediaObject(mediaObjects, qualifierToRetrieve);

            qualifierToRetrieve = THUMB_MEDIA + qualifier;
            isValid = getPrimaryMediaObject(mediaObjects, qualifierToRetrieve) && isValid;

            qualifierToRetrieve = LARGE_MEDIA + qualifier;
            getPrimaryMediaObject(mediaObjects, qualifierToRetrieve);

            qualifierToRetrieve = LIST_MEDIA + qualifier;
            getPrimaryMediaObject(mediaObjects, qualifierToRetrieve);

            qualifierToRetrieve = GRID_MEDIA + qualifier;
            getPrimaryMediaObject(mediaObjects, qualifierToRetrieve);

            qualifierToRetrieve = FULL_MEDIA + qualifier;
            getPrimaryMediaObject(mediaObjects, qualifierToRetrieve);

            mediaObjects.setHasValidPrimaryImages(isValid && mediaObjects.getPrimaryImages().size() >= 5);
        }
        else {
            final String error = String.format(
                    "ERR-PRODUCTMEDIA-NOPRIMARYIMAGE : No primary image defined for product with ID %s", productCode);
            LOG.error(error);
            mediaObjects.addErrors(error);
            mediaObjects.setHasValidPrimaryImages(false);
        }
    }


    /**
     * @param mediaObjects
     * @param qualifierToRetrieve
     */
    public boolean getPrimaryMediaObject(final ProductMediaObjects mediaObjects, final String qualifierToRetrieve) {
        boolean success = true;
        MediaModel mediaModel = null;
        try {
            mediaModel = getMediaModel(qualifierToRetrieve);
        }
        catch (final AmbiguousIdentifierException aie) {
            final String error = String
                    .format("ERR-PRODUCTMEDIA-AMBIGOUSMEDIA : The media with the qualifier %s returned ambigous results from hybris",
                            qualifierToRetrieve);
            LOG.error(error);
            mediaObjects.addErrors(error);
            return false;
        }
        if (mediaModel == null) {
            final String error = String.format(
                    "ERR-PRODUCTMEDIA-MEDIANOTFOUND : The media with the qualifier %s not found in hybris",
                    qualifierToRetrieve);
            LOG.error(error);
            mediaObjects.addErrors(error);
            success = false;
        }
        else {
            mediaObjects.addPrimaryImages(qualifierToRetrieve, mediaModel);
        }
        return success;
    }

}

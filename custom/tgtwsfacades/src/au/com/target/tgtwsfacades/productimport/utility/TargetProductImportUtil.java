/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.utility;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;
import au.com.target.tgtwsfacades.productimport.data.MediaImageData;



/**
 * @author rsamuel3
 * 
 */
public class TargetProductImportUtil {
    /**
     * 
     */
    public static final String FORWARD_SLASH = "/";

    public static final String PRODUCT_CATALOG = "targetProductCatalog";

    public static final String CONTENT_CATALOG = "targetContentCatalog";

    public static final String STAGED_VERSION = "Staged";

    public static final String ONLINE_VERSION = "Online";

    protected static final String BASE_FOLDER_IMAGES = "tgtfacades.step.assets.baselocation";

    protected static final String BASE_HYBRIS_MEDIA_FOLDER = "media.read.dir";



    @Autowired
    private CatalogVersionService catalogVersionService;

    @Resource(name = "configurationService")
    private ConfigurationService configurationService;


    /**
     * generic method to create a failure response
     * 
     * @param errmessage
     * @param code
     */
    public IntegrationResponseDto createResponse(final String errmessage, final String code) {
        final IntegrationResponseDto dto = new IntegrationResponseDto(code);
        if (StringUtils.isNotBlank(errmessage)) {
            dto.setSuccessStatus(false);
            dto.addMessage(errmessage);
        }
        else {
            dto.setSuccessStatus(true);
        }
        return dto;
    }

    public IntegrationResponseDto createResponse(final boolean success,
            final List<String> errorMsgs, final String code) {
        final IntegrationResponseDto dto = new IntegrationResponseDto(code);
        dto.setSuccessStatus(success);
        dto.setMessages(errorMsgs);
        return dto;
    }

    public MediaImageData createImageData(final String folder, final String filename, final String mimetype)
            throws IOException {
        return new MediaImageData(getImageData(folder, filename), filename, mimetype);
    }

    /**
     * converts the image from the location BASE_FOLDER_IMAGES+folder+realfilename into a byte array
     * 
     * @param folder
     * @param realFileName
     * @throws IOException
     */
    private InputStream getImageData(final String folder, final String realFileName) throws IOException {
        final String baseFolder = configurationService.getConfiguration().getString(BASE_FOLDER_IMAGES);
        final File file = new File(baseFolder + File.separator + folder + File.separator + realFileName);
        return new FileInputStream(file);
    }

    public String getImageURL(final String file) throws IOException {
        final String baseFolderMedia = configurationService.getConfiguration().getString(BASE_FOLDER_IMAGES);

        return baseFolderMedia + File.separator + file;
    }

    public String getActualImageLocation(final String file) throws IOException {
        final String baseFolder = configurationService.getConfiguration().getString(BASE_HYBRIS_MEDIA_FOLDER);
        final String baseFolderProductMedia = getImageURL(file);
        final int indx = baseFolderProductMedia.indexOf('/', 1);
        final String baseFolderProduct = baseFolderProductMedia.substring(indx + 1);
        return baseFolder + File.separator + baseFolderProduct;
    }

    public CatalogVersionModel getProductCatalogStagedVersion() {
        return catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, STAGED_VERSION);
    }

    public CatalogVersionModel getProductCatalogOnlineVersion() {
        return catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, ONLINE_VERSION);
    }

    public CatalogVersionModel getContentCatalogStagedVersion() {
        return catalogVersionService.getCatalogVersion(CONTENT_CATALOG, STAGED_VERSION);
    }

    public CatalogVersionModel getContentCatalogOnlineVersion() {
        return catalogVersionService.getCatalogVersion(CONTENT_CATALOG, ONLINE_VERSION);
    }

    /**
     * @param code
     * @return String representing the assetId
     */
    public String getAssetId(final String code) {
        final int lastIndexOf = code.lastIndexOf(FORWARD_SLASH);
        String assetId = null;
        if (lastIndexOf >= 0) {
            assetId = code.substring(lastIndexOf + 1);
        }
        return assetId;
    }

    /**
     * checks the bulky flag on the ProductTypeModel
     * 
     * @param typeByCode
     * @return boolean indicating if it is bulky
     */
    public boolean isBulky(final ProductTypeModel typeByCode) {
        if (typeByCode != null) {
            return BooleanUtils.toBoolean(typeByCode.getBulky());
        }
        return false;
    }

    /**
     * 
     * @param stringValue
     * @return when stringValue empty or null or not 'Y' return false else return true
     */
    public Boolean getBooleanValue(final String stringValue) {
        if (StringUtils.isEmpty(stringValue)) {
            return Boolean.FALSE;
        }
        else {
            return stringValue.charAt(0) == 'Y' ? Boolean.TRUE : Boolean.FALSE;
        }
    }

    public boolean isProductPhysicalGiftcard(final IntegrationProductDto dto) {

        final String expectedWarehouseCode = CollectionUtils.isNotEmpty(dto.getWarehouses())
                ? dto.getWarehouses().get(0)
                : null;
        return TargetProductImportConstants.PRODUCT_TYPE_NORMAL.equals(dto.getProductType())
                && TargetProductImportConstants.WAREHOUSE_INCOMM.equals(expectedWarehouseCode);
    }

}

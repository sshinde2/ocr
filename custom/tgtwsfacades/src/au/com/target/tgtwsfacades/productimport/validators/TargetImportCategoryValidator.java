/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.platform.category.CategoryService;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author rsamuel3
 *
 */
public class TargetImportCategoryValidator implements TargetProductImportValidator {

    private CategoryService categoryService;

    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator#validate(au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto)
     */
    @Override
    public List<String> validate(final IntegrationProductDto dto) {
        final String variationCode = dto.getVariantCode();
        // primary category
        final List<String> messages = new ArrayList<>();
        if (StringUtils.isBlank(dto.getPrimaryCategory())) {
            messages.add(MessageFormat
                    .format(
                            "Missing 'PRIMARY CATEGORY' (web storefront classification) for product, aborting product/variant import for: {0}",
                            variationCode));
        }
        else {
            try {
                categoryService.getCategoryForCode(dto.getPrimaryCategory());
            }
            catch (final Exception e) {
                messages.add(MessageFormat.format(
                        "Primary category {1} does not exist in Hybris, aborting product/variant import for: {0}",
                        variationCode, dto.getPrimaryCategory()));
            }
        }

        // secondary category
        if (CollectionUtils.isNotEmpty(dto.getSecondaryCategory())) {
            try {
                for (final String categoryCode : dto.getSecondaryCategory()) {
                    categoryService.getCategoryForCode(categoryCode);
                }
            }
            catch (final Exception e) {
                messages.add(MessageFormat.format(
                        "Secondary category {1} does not exist in Hybris, aborting product/variant import for: {0}",
                        variationCode, dto.getSecondaryCategory()));
            }
        }

        // original category
        if (!StringUtils.isEmpty(dto.getOriginalCategory())) {
            try {
                categoryService.getCategoryForCode(dto.getOriginalCategory());
            }
            catch (final Exception e) {
                messages.add(MessageFormat.format(
                        "Original category {1} does not exist in Hybris, aborting product/variant import for: {0}",
                        variationCode, dto.getOriginalCategory()));
            }
        }
        return messages;
    }

    /**
     * @param categoryService
     *            the categoryService to set
     */
    @Required
    public void setCategoryService(final CategoryService categoryService) {
        this.categoryService = categoryService;
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;


/**
 * @author rsamuel3
 *
 */
public class TargetImportColourVariantValidator implements TargetProductImportValidator {

    private List<TargetProductImportValidator> validators;

    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;


    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator#validate(au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto)
     */
    @Override
    public List<String> validate(final IntegrationProductDto dto) {

        final List<String> messages = new ArrayList<>();

        final String variationCode = dto.getVariantCode();

        final String validateProductNameForVariant = validateProductNameForVariant(dto, variationCode);
        if (StringUtils.isNotBlank(validateProductNameForVariant)) {
            messages.add(validateProductNameForVariant);
        }

        for (final TargetProductImportValidator validator : validators) {
            final List<String> validatedMessages = validator.validate(dto);
            if (CollectionUtils.isNotEmpty(validatedMessages)) {
                messages.addAll(validatedMessages);
            }
        }

        // variant approval status
        if (!CollectionUtils.isEmpty(dto.getVariants())) {
            for (final IntegrationVariantDto var : dto.getVariants()) {
                if (StringUtils.isBlank(var.getApprovalStatus())) {
                    messages.add(MessageFormat.format(
                            "Missing variant 'APPROVAL STATUS' (94267), aborting product/variant import for: {0}",
                            var.getVariantCode()));
                }
            }
        }

        //verify the sizegroup if the feature switch is on
        if (targetFeatureSwitchFacade.isSizeOrderingEnabled()) {
            final String validateSizeGroupForProduct = validProductForSizeGroup(dto);
            if (StringUtils.isNotEmpty(validateSizeGroupForProduct)) {
                messages.add(validateSizeGroupForProduct);
            }
        }

        // description
        if (StringUtils.isBlank(dto.getDescription())) {
            messages.add(MessageFormat.format(
                    "Missing 'PRODUCT DESCRIPTION' (49142), aborting product/variant import for: {0}",
                    variationCode));
        }
        return messages;

    }



    private String validateProductNameForVariant(final IntegrationProductDto dto,
            final String variationCode) {
        // product name
        if (StringUtils.isBlank(dto.getName())) {
            return MessageFormat.format(
                    "Missing 'PRODUCT NAME' (49127), aborting product/variant import for: {0}",
                    variationCode);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Checks if the color variant has sizegroup
     * 
     * @param dto
     * @return String
     */
    protected String validProductForSizeGroup(final IntegrationProductDto dto) {
        final boolean isSizeOnly = dto.getIsSizeOnly() == null ? false : dto.getIsSizeOnly().booleanValue();
        if (isSizeOnly || CollectionUtils.isNotEmpty(dto.getVariants())) {
            if (StringUtils.isEmpty(dto.getSizeGroup())) {
                return MessageFormat.format(
                        "Missing 'SizeGroup', aborting product/variant import for: {0}",
                        dto.getProductCode());
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * @param validators
     *            the validators to set
     */
    @Required
    public void setValidators(final List<TargetProductImportValidator> validators) {
        this.validators = validators;
    }



    /**
     * @param targetFeatureSwitchFacade
     *            the targetFeatureSwitchFacade to set
     */
    @Required
    public void setTargetFeatureSwitchFacade(final TargetFeatureSwitchFacade targetFeatureSwitchFacade) {
        this.targetFeatureSwitchFacade = targetFeatureSwitchFacade;
    }

}

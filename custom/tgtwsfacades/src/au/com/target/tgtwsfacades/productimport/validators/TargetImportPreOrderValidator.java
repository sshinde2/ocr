/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * Validate the pre order attributes of product while importing.
 * 
 * @author gsing236
 *
 */
public class TargetImportPreOrderValidator implements TargetProductImportValidator {

    private int hoursBetEmbargoDateAndEndDate;

    @Override
    public List<String> validate(final IntegrationProductDto product) {
        final String variationCode = product.getVariantCode();
        // on-line, off-line dates 
        final List<String> messages = new ArrayList<>();

        final String preOrderStartDateString = product.getPreOrderStartDate();
        final String preOrderEndDateString = product.getPreOrderEndDate();
        final String embargoReleaseDateString = product.getPreOrderEmbargoReleaseDate();

        Date preOrderStartDate = null;
        Date preOrderEndDate = null;
        Date preOrderEmbargoReleaseDate = null;

        if (StringUtils.isNotBlank(preOrderStartDateString)) {
            try {
                preOrderStartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(preOrderStartDateString);
            }
            catch (final Exception e) {
                messages.add(MessageFormat.format(
                        "Product pre order start date has incorrect format, aborting product/variant import for: {0}",
                        variationCode));
            }
        }

        if (StringUtils.isNotBlank(preOrderEndDateString)) {
            try {
                preOrderEndDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(preOrderEndDateString);

            }
            catch (final Exception e) {
                messages.add(MessageFormat.format(
                        "Product pre order end date has incorrect format, aborting product/variant import for: {0}",
                        variationCode));
            }
        }

        if (StringUtils.isNotBlank(embargoReleaseDateString)) {
            try {
                preOrderEmbargoReleaseDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        .parse(embargoReleaseDateString);

            }
            catch (final Exception e) {
                messages.add(MessageFormat.format(
                        "Product pre order embargo release date has incorrect format, aborting product/variant import for: {0}",
                        variationCode));
            }
        }

        validateEndDateIsAfterStartDate(variationCode, messages, preOrderStartDate, preOrderEndDate);

        validateDiffBetEndDateAndEmbargoDate(variationCode, messages, preOrderEndDate, preOrderEmbargoReleaseDate);

        validatePreOrderOnlineQty(product, messages);

        return messages;
    }

    /**
     * @param variationCode
     * @param messages
     * @param preOrderStartDate
     * @param preOrderEndDate
     */
    private void validateEndDateIsAfterStartDate(final String variationCode, final List<String> messages,
            final Date preOrderStartDate, final Date preOrderEndDate) {
        if (preOrderEndDate != null && preOrderStartDate != null && preOrderEndDate.before(preOrderStartDate)) {
            messages.add(MessageFormat.format(
                    "Product pre order end date is before pre order start date, aborting product/variant import for: {0}",
                    variationCode));
        }
    }

    /**
     * @param product
     * @param messages
     */
    private void validatePreOrderOnlineQty(final IntegrationProductDto product,
            final List<String> messages) {

        final String variationCode = product.getVariantCode();
        final Integer preOrderOnlineQuantity = product.getPreOrderOnlineQuantity();
        if (preOrderOnlineQuantity != null && preOrderOnlineQuantity.intValue() < 0)

        {
            messages.add(MessageFormat.format(
                    "Product pre order online quantity cannot be less than zero, aborting product/variant import for: {0}",
                    variationCode));

        }
    }

    /**
     * @param variationCode
     * @param messages
     * @param preOrderEndDate
     * @param preOrderEmbargoReleaseDate
     */
    private void validateDiffBetEndDateAndEmbargoDate(final String variationCode, final List<String> messages,
            final Date preOrderEndDate, final Date preOrderEmbargoReleaseDate) {
        if (preOrderEndDate != null && preOrderEmbargoReleaseDate != null) {
            final DateTime endDate = new DateTime(preOrderEndDate);
            final DateTime embargoDate = new DateTime(preOrderEmbargoReleaseDate);

            // pre-order end date has to be 48 hours before the embargo end date
            if (embargoDate.minusHours(hoursBetEmbargoDateAndEndDate).isBefore(endDate)) {
                messages.add(MessageFormat.format(
                        "Pre-Order End Date/Time has to finish >{0} hours before ODBMS Embargo Release Date/Time, aborting product/variant import for: {1}",
                        new Integer(hoursBetEmbargoDateAndEndDate), variationCode));
            }
        }
    }

    /**
     * @param hoursBetEmbargoDateAndEndDate
     *            the hoursBetEmbargoDateAndEndDate to set
     */
    @Required
    public void setHoursBetEmbargoDateAndEndDate(final int hoursBetEmbargoDateAndEndDate) {
        this.hoursBetEmbargoDateAndEndDate = hoursBetEmbargoDateAndEndDate;
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;


/**
 * @author mjanarth
 *
 */
public class TargetImportSellableVariantDisplayOnlyValidator implements TargetProductImportValidator {

    private static final String VALIDATION_ERR_MESSAGE = "Display only product cannot be {0}, aborting product/variant import for: {1} ";
    private TargetProductImportUtil targetProductImportUtil;
    private TargetWarehouseService warehouseService;


    /* (non-Javadoc)
     * @see au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator#validate(au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto)
     */
    @Override
    public List<String> validate(final IntegrationProductDto dto) {
        final List<String> messages = new ArrayList<>();
        final Boolean displayOnly;
        //is colorvariant sellable or sizeonly prd
        if (BooleanUtils.isTrue(dto.getIsSizeOnly()) || CollectionUtils.isEmpty(dto.getVariants())) {
            displayOnly = populateDisplayOnly(dto, null);
            if (BooleanUtils.isTrue(displayOnly)) {
                validateDisplayOnlyFlag(dto, messages);
                validateAvailableOnEbay(dto.getAvailableOnEbay(), dto.getVariantCode(), messages);
            }
            return messages;
        }
        //size variant sellable
        return validateSizeVariants(dto);

    }

    /**
     * Validate Product type,Online exclusive and drop ship on the color variant level
     * 
     * @param productDto
     * @return List
     */
    private List<String> validateDisplayOnlyFlag(final IntegrationProductDto productDto, final List<String> messages) {

        if (TargetProductImportConstants.PRODUCT_TYPE_DIGITAL.equals(productDto.getProductType())) {
            messages.add(MessageFormat.format(VALIDATION_ERR_MESSAGE, "digital", productDto.getVariantCode()));
        }
        if (TargetProductImportConstants.ARTICLE_STATUS_ONLINEEXCLUSIVE
                .equalsIgnoreCase(productDto.getArticleStatus())) {
            messages.add(MessageFormat.format(VALIDATION_ERR_MESSAGE, "online exclusive", productDto.getVariantCode()));
        }
        if (isDropShipProduct(productDto)) {
            messages.add(MessageFormat.format(VALIDATION_ERR_MESSAGE, "dropship product", productDto.getVariantCode()));
        }

        return messages;

    }

    /**
     * Check whether the warehouse is a drop ship one
     * 
     * @param productDto
     * @return boolean
     */
    private boolean isDropShipProduct(final IntegrationProductDto productDto) {
        final List<String> warehouses = productDto.getWarehouses();

        if (CollectionUtils.isNotEmpty(warehouses) && null != warehouses.get(0)) {
            final WarehouseModel warehouseModel = warehouseService.getWarehouseForCode(warehouses.get(0));
            if (null != warehouseModel) {
                return warehouseModel.isDropShipWarehouse();
            }
        }
        return false;

    }

    /**
     * If the variants are not empty then the displayonly flag is on the variant level and the available on ebay flag is
     * on the sellable variant level and rest all validations need to be done from the color variant level
     * 
     * @param dto
     * @return List
     */
    private List<String> validateSizeVariants(final IntegrationProductDto dto) {
        final List<String> messages = new ArrayList<>();
        Boolean displayOnly;
        boolean atleastOneVariantDisplayOnly = false;
        if (CollectionUtils.isEmpty(dto.getVariants())) {
            return messages;
        }
        for (final IntegrationVariantDto var : dto.getVariants()) {
            displayOnly = populateDisplayOnly(dto, var);
            if (BooleanUtils.isTrue(displayOnly)) {
                atleastOneVariantDisplayOnly = true;
                validateAvailableOnEbay(populateAvailableOnEbayForSizeVariant(dto, var), var.getVariantCode(),
                        messages);
            }

        }
        if (atleastOneVariantDisplayOnly) {
            validateDisplayOnlyFlag(dto, messages);
        }
        return messages;

    }

    /**
     * 
     * @param availableOnEbay
     * @param productCode
     */
    private void validateAvailableOnEbay(final String availableOnEbay, final String productCode,
            final List<String> messages) {
        if (BooleanUtils.isTrue(targetProductImportUtil.getBooleanValue(availableOnEbay))) {
            messages.add(MessageFormat.format(VALIDATION_ERR_MESSAGE, "available on ebay", productCode));
        }
    }

    /**
     * Populate the displayonly flag from IntegrationProductDto or from IntegrationVariantDto If the variant doesn't
     * have the display flag set,then it should be populated from the color variant
     * 
     * @param productDto
     * @param var
     * @return Boolean
     */
    private Boolean populateDisplayOnly(final IntegrationProductDto productDto, final IntegrationVariantDto var) {
        Boolean displayOnly = null != productDto.getDisplayOnly() ? targetProductImportUtil
                .getBooleanValue(productDto.getDisplayOnly()) : Boolean.FALSE;
        if (null != var && StringUtils.isNotEmpty(var.getDisplayOnly())) {
            displayOnly = targetProductImportUtil
                    .getBooleanValue(var.getDisplayOnly());
        }
        return displayOnly;
    }


    /**
     * Check for available on ebay in the varaint,if the flag is not present in the size variantion level populate it
     * from color variation
     * 
     * @param dto
     * @param var
     * @return String
     */
    private String populateAvailableOnEbayForSizeVariant(final IntegrationProductDto dto,
            final IntegrationVariantDto var) {

        if (null != var && StringUtils.isNotEmpty(var.getAvailableOnEbay())) {
            return var.getAvailableOnEbay();
        }
        return dto.getAvailableOnEbay();
    }

    /**
     * @param targetProductImportUtil
     *            the targetProductImportUtil to set
     */
    @Required
    public void setTargetProductImportUtil(final TargetProductImportUtil targetProductImportUtil) {
        this.targetProductImportUtil = targetProductImportUtil;
    }

    /**
     * @param warehouseService
     *            the warehouseService to set
     */
    @Required
    public void setWarehouseService(final TargetWarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }
}

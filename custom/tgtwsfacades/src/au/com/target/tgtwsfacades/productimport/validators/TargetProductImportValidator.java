/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import java.util.List;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author rsamuel3
 *
 */
public interface TargetProductImportValidator {
    /**
     * validates the product and returns a list of messages if there are any errors
     * 
     * @param product
     * @return list of error messages and empty if all valid
     */
    public List<String> validate(IntegrationProductDto product);
}

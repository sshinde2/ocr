/**
 * 
 */
package au.com.target.tgtwsfacades.segments;

import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentImportResponseDto;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoRequestDto;


/**
 * @author mjanarth
 *
 */
public interface CustomerSegmentImportFacade {

    /**
     * 
     * @param requestDto
     * @return CustomerSegmentImportResponseDto
     */
    CustomerSegmentImportResponseDto importCustomerSegments(final CustomerSegmentInfoRequestDto requestDto);

}

/**
 * 
 */
package au.com.target.tgtwsfacades.segments.impl;

import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.usergroups.model.UserSegmentModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetCustomerModel;
import au.com.target.tgtcore.user.service.TargetUserService;
import au.com.target.tgtmarketing.segments.TargetUserSegmentService;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentImportResponseDto;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoDto;
import au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoRequestDto;
import au.com.target.tgtwsfacades.segments.CustomerSegmentImportFacade;


/**
 * @author mjanarth
 *
 */
public class CustomerSegmentImportFacadeImpl implements CustomerSegmentImportFacade {


    private static final Logger LOG = Logger.getLogger(CustomerSegmentImportFacadeImpl.class);
    private static final String SEGMENT_IMPORT_ERROR = "SEG-IMPORT-ERROR : ";

    private TargetUserSegmentService targetUserSegmentService;
    private TargetUserService targetUserService;
    private ModelService modelService;
    private ConfigurationService configurationService;


    /* (non-Javadoc)
       * @see au.com.target.tgtwsfacades.segments.CustomerSegmentImportFacade#importCustomerSegments(au.com.target.tgtwsfacades.integration.dto.CustomerSegmentInfoRequestDto)
       */
    @Override
    public synchronized CustomerSegmentImportResponseDto importCustomerSegments(
            final CustomerSegmentInfoRequestDto requestDto) {
        //This method has to be synchronized so that multiple requests does not come and modify and the segments as user segment is getting saved in a batch.
        //The chance for this to occur is highly unlikely.. but this is just a saftey net.
        final CustomerSegmentImportResponseDto response = new CustomerSegmentImportResponseDto();
        List<PrincipalGroupModel> existingUserSegments = new ArrayList<>();
        try {

            if (CollectionUtils.isEmpty(requestDto.getCustomerSegments())) {
                LOG.error(SEGMENT_IMPORT_ERROR + "No Customer and Segment information recieved in payload");
                response.setSuccess(false);
                return response;
            }
            final Map<String, UserSegmentModel> userSegmentModelMap = targetUserSegmentService.getAllUserSegmentsMap();
            if (MapUtils.isEmpty(userSegmentModelMap)) {
                LOG.error(SEGMENT_IMPORT_ERROR + "No User Segments Configured in Hybris");
                response.setSuccess(false);
                return response;
            }

            final List<CustomerModel> customerList = new ArrayList<>();
            for (final CustomerSegmentInfoDto customerSegmentInfo : requestDto.getCustomerSegments()) {
                final TargetCustomerModel customerModel = findCustomerBySubscriptionId(customerSegmentInfo
                        .getSubscriptionId());

                UserSegmentModel latestUserSegmentModel = null;

                if (null != customerSegmentInfo.getSegment()) {
                    latestUserSegmentModel = userSegmentModelMap
                            .get(customerSegmentInfo.getSegment().toLowerCase());
                }
                if (customerModel == null || latestUserSegmentModel == null) {
                    LOG.error(SEGMENT_IMPORT_ERROR + "Customer or Segment could not be found for subscriptionId="
                            + customerSegmentInfo
                                    .getSubscriptionId()
                            + " segmentId=" + customerSegmentInfo.getSegment());
                    continue;
                }
                existingUserSegments = targetUserSegmentService
                        .getAllUserSegmentsForUser(customerModel);

                if (CollectionUtils.isEmpty(existingUserSegments)) {
                    //add the user segment even though there is no userSegment.
                    customerList.add(addSegmentsToCustomer(latestUserSegmentModel, customerModel));
                }
                else {
                    for (final PrincipalGroupModel principalGroupModel : existingUserSegments) {
                        if (!(principalGroupModel instanceof UserSegmentModel)) {
                            continue;
                        }
                        else {
                            final UserSegmentModel currentUserSegmentModel = (UserSegmentModel)principalGroupModel;
                            //if current user segment matches the existing user segment then add.
                            if (StringUtils.equals(currentUserSegmentModel.getUid(),
                                    latestUserSegmentModel.getUid())) {
                                continue;
                            }
                            else {
                                //if current user segment is not matching the existing user segment then remove
                                customerList.add(updateCustomerUserSegment(currentUserSegmentModel,
                                        latestUserSegmentModel, customerModel));
                            }
                        }

                    }
                }

                if (customerList.size() >= getCustomerListSize()) {
                    saveAllCustomers(customerList);
                    customerList.clear();
                }
            }
            if (CollectionUtils.isNotEmpty(customerList)) {
                saveAllCustomers(customerList);
            }

            response.setSuccess(true);
            return response;
        }
        catch (final Exception e) {
            LOG.error(SEGMENT_IMPORT_ERROR + "Unexpected error occured during import process ", e);
            response.setSuccess(false);
            return response;
        }
    }

    /**
     * @param customerList
     */
    protected void saveAllCustomers(final List<CustomerModel> customerList) {
        try {
            modelService.saveAll(customerList);
        }
        catch (final Exception e) {
            for (final CustomerModel customerModel : customerList) {
                try {
                    modelService.save(customerModel);
                }
                catch (final Exception exception) {

                    if (customerModel instanceof TargetCustomerModel) {
                        final TargetCustomerModel targetCustomerModel = (TargetCustomerModel)customerModel;
                        LOG.error(SEGMENT_IMPORT_ERROR + "Error saving the customer for the subscriptionKey="
                                + targetCustomerModel.getSubscriberKey(), exception);
                    }
                }

            }
        }
    }

    private final TargetCustomerModel findCustomerBySubscriptionId(final String subscriptionId) {
        try {
            final TargetCustomerModel targetCustomerModel = targetUserService.getUserBySubscriptionId(subscriptionId);
            return targetCustomerModel;
        }
        catch (final TargetAmbiguousIdentifierException | TargetUnknownIdentifierException e) {
            LOG.error("SEGMENT_IMPORT_ERROR - Can't find the customer for the subscriptionKey=" + subscriptionId, e);

        }
        return null;

    }

    protected TargetCustomerModel updateCustomerUserSegment(final UserSegmentModel existingUserSegment,
            final UserSegmentModel newUserSegment,
            final TargetCustomerModel customerModel) {
        boolean isGroupChanged = false;
        final Set<PrincipalGroupModel> existingGroups = customerModel.getGroups();
        final HashSet<PrincipalGroupModel> modifiedGroups = new HashSet<PrincipalGroupModel>(existingGroups);
        if (CollectionUtils.isNotEmpty(modifiedGroups)) {
            for (final PrincipalGroupModel userGrp : modifiedGroups) {
                if ((userGrp instanceof UserSegmentModel)
                        && (StringUtils.equalsIgnoreCase(userGrp.getUid(), existingUserSegment.getUid()))) {
                    modifiedGroups.remove(userGrp);
                    modifiedGroups.add(newUserSegment);
                    isGroupChanged = true;
                    break;
                }
            }
        }
        if (isGroupChanged) {
            customerModel.setGroups(modifiedGroups);
        }
        return customerModel;
    }

    private CustomerModel addSegmentsToCustomer(final UserSegmentModel userSegment,
            final TargetCustomerModel customerModel) {
        final Set<PrincipalGroupModel> existingGroups = customerModel.getGroups();
        final HashSet<PrincipalGroupModel> modifiedGroups = new HashSet<PrincipalGroupModel>(existingGroups);
        modifiedGroups.add(userSegment);
        customerModel.setGroups(modifiedGroups);
        return customerModel;

    }

    /**
     * 
     * @return int
     */
    protected int getCustomerListSize() {
        return configurationService.getConfiguration().getInt("tgtwsfacades.customersegments.customerlist.size", 100);
    }

    /**
     * @param targetUserSegmentService
     *            the targetUserSegmentService to set
     */
    @Required
    public void setTargetUserSegmentService(final TargetUserSegmentService targetUserSegmentService) {
        this.targetUserSegmentService = targetUserSegmentService;
    }

    /**
     * @param targetUserService
     *            the targetUserService to set
     */
    @Required
    public void setTargetUserService(final TargetUserService targetUserService) {
        this.targetUserService = targetUserService;
    }

    /**
     * @param modelService
     *            the modelService to set
     */
    @Required
    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    /**
     * @param configurationService
     *            the configurationService to set
     */
    @Required
    public void setConfigurationService(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

}

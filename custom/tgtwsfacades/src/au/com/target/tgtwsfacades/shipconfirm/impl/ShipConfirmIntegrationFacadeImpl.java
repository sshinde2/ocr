package au.com.target.tgtwsfacades.shipconfirm.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.StringUtils;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtwsfacades.helper.OrderConsignmentHelper;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.OrderConfirm;
import au.com.target.tgtwsfacades.integration.dto.ShipConfirm;
import au.com.target.tgtwsfacades.shipconfirm.ShipConfirmIntegrationFacade;


/**
 * @author Olivier Lamy
 */
public class ShipConfirmIntegrationFacadeImpl
        implements ShipConfirmIntegrationFacade {

    private static final String ERR_SHIPCONF_NOORDERS = "ERR_SHIPCONF_NOORDERS - No order confirm "
            + "found from ShipConfirm instance";

    private static final String ERR_SHIPCONF_EXCEPTION = "ERR-SHIPCONF-HYBRISUPDATE : Exception occured "
            + "processing ship confirm for order {0} with the message : {1}";

    private static final Logger LOG = Logger.getLogger(ShipConfirmIntegrationFacadeImpl.class);

    private TargetFulfilmentService targetFulfilmentService;
    private OrderConsignmentHelper orderConsignmentHelper;

    @Override
    public List<IntegrationResponseDto> handleShipConfirm(final ShipConfirm shipConfirm) {
        final List<IntegrationResponseDto> responses = new ArrayList<>();
        if (shipConfirm == null || shipConfirm.getOrderConfirms() == null || shipConfirm.getOrderConfirms().isEmpty()) {
            final IntegrationResponseDto response = new IntegrationResponseDto(null);
            response.setSuccessStatus(false);
            LOG.error(ERR_SHIPCONF_NOORDERS);
            response.addMessage(ERR_SHIPCONF_NOORDERS);
            responses.add(response);
        }
        else {

            for (final OrderConfirm order : shipConfirm.getOrderConfirms()) {

                final String orderConsignmentIdentifier = order.getOrderNumber();
                final IntegrationResponseDto response = new IntegrationResponseDto(orderConsignmentIdentifier);

                try {

                    if (StringUtils.isEmpty(orderConsignmentIdentifier)) {
                        throw new IllegalStateException("order number is missing in batch");
                    }
                    final TargetConsignmentModel consignment = orderConsignmentHelper
                            .getConsignmentBasedOrderOrConsignmentCode(orderConsignmentIdentifier);
                    targetFulfilmentService.processShipConfForConsignment(consignment.getOrder().getCode(),
                            consignment, order.getDate(), LoggingContext.WAREHOUSE);

                    response.setSuccessStatus(true);
                }
                catch (final Exception e) {
                    final String message = MessageFormat
                            .format(ERR_SHIPCONF_EXCEPTION,
                                    order.getOrderNumber(), e.getMessage());
                    LOG.error(message, e);
                    response.setSuccessStatus(false);
                    response.addMessage(message);
                }
                responses.add(response);
            }

        }

        return responses;
    }

    /**
     * @param targetFulfilmentService
     *            the targetFulfilmentService to set
     */
    @Required
    public void setTargetFulfilmentService(final TargetFulfilmentService targetFulfilmentService) {
        this.targetFulfilmentService = targetFulfilmentService;
    }

    /**
     * @param orderConsignmentHelper
     *            the orderConsignmentHelper to set
     */
    @Required
    public void setOrderConsignmentHelper(final OrderConsignmentHelper orderConsignmentHelper) {
        this.orderConsignmentHelper = orderConsignmentHelper;
    }

}

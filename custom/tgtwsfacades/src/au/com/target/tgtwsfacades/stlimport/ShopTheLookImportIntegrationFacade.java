/**
 * 
 */
package au.com.target.tgtwsfacades.stlimport;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationShopTheLookDto;



/**
 * @author mgazal
 * 
 */
public interface ShopTheLookImportIntegrationFacade {


    /**
     * @param dto
     * @return {@link IntegrationResponseDto}
     */
    IntegrationResponseDto persistTargetShopTheLook(IntegrationShopTheLookDto dto);

}

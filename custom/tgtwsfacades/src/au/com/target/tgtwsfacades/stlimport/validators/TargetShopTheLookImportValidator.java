/**
 * 
 */
package au.com.target.tgtwsfacades.stlimport.validators;

import java.util.List;

import au.com.target.tgtwsfacades.integration.dto.IntegrationShopTheLookDto;


/**
 * @author mgazal
 *
 */
public interface TargetShopTheLookImportValidator {

    /**
     * validates shop the look and returns a list of messages if there are any errors
     * 
     * @param shopTheLook
     * @return list of error messages and empty if all valid
     */
    public List<String> validate(IntegrationShopTheLookDto shopTheLook);
}

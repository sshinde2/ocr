/**
 * 
 */
package au.com.target.tgtwsfacades.stockupdate;

import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateResponseDto;
import au.com.target.tgtwsfacades.integration.dto.StockUpdate;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateResponseDto;



/**
 * @author mmaki
 * 
 */
public interface StockUpdateIntegrationFacade {


    /**
     * 
     * This method is a facade on the top of
     * {@link au.com.target.tgtcore.stock.TargetStockService#updateActualStockLevel}
     * 
     * @param stockUpdateDto
     *            the stock
     * @return the response
     */
    IntegrationStockUpdateResponseDto updateStock(final IntegrationStockUpdateDto stockUpdateDto);

    /**
     * 
     * This method is a facade on the top of {@link au.com.target.tgtcore.stock.TargetStockService#adjustActualAmount}
     * 
     * @param stockUpdateDto
     *            the stock
     * @return the response
     */
    IntegrationStockUpdateResponseDto adjustStock(final IntegrationStockUpdateDto stockUpdateDto);

    /**
     * 
     * This method is a facade on the top of {@link au.com.target.tgtcore.stock.TargetStockService#adjustActualAmount}
     * 
     * <b>If the {@link de.hybris.platform.ordersplitting.model.StockLevelModel} does not exist it will be created</b>
     * 
     * @param stockUpdate
     *            the stock
     * @return the response
     */
    IntegrationResponseDto updateStock(final StockUpdate stockUpdate);

    /**
     * bulk update stock for multiple-product
     * 
     * @param targetStockUpdateDto
     * @return targetStockUpdateResponseDto
     */
    TargetStockUpdateResponseDto updateStockBatch(final TargetStockUpdateDto targetStockUpdateDto);

    /**
     * bulk stock adjustments for multiple product
     * 
     * @param targetStockUpdateDto
     * @return targetStockUpdateResponseDto
     */
    TargetStockUpdateResponseDto adjustStockBatch(final TargetStockUpdateDto targetStockUpdateDto);
}

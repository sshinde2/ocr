/**
 * 
 */
package au.com.target.tgtwsfacades.stockupdate.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationStockUpdateResponseDto;
import au.com.target.tgtwsfacades.integration.dto.StockUpdate;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateProductDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateProductResponseDto;
import au.com.target.tgtwsfacades.integration.dto.TargetStockUpdateResponseDto;
import au.com.target.tgtwsfacades.stockupdate.StockUpdateIntegrationFacade;



/**
 * @author mmaki
 * 
 */
public class StockUpdateIntegrationFacadeImpl implements StockUpdateIntegrationFacade {

    protected static final String ERR_STOCKUPDATE_WAREHOUSE =
            "ERR-WAREHOUSE-SOHWAREHOUSE: Warehouse with code {0} was not found";

    protected static final String ERR_STOCKUPDATE_EAN =
            "ERR-WAREHOUSE-SOHEAN: Product with ean {0} was not found";

    protected static final String ERR_STOCKUPDATE_MULTILPLE_EAN =
            "ERR-WAREHOUSE-SOHEAN: Multiple Products with same Ean={0} are found, product={1}, warehouse={2}, quantity={3}, adjustmentQuantity={4}, comments={5}";

    protected static final String ERR_STOCKUPDATE_PRODUCT =
            "ERR-WAREHOUSE-SOHPRODUCT: Product with code {0} was not found.Product Ean: {1}.";

    protected static final String ERR_STOCKUPDATE_PRODUCT_AVAILABLE =
            "ERR-WAREHOUSE-SOHPRODUCT-AVAILABLE: Product with code {0} was not found. Availability: {1}. Product Ean: {2}.";

    protected static final String ERR_STOCKUPDATE_AVAILABILITY =
            "ERR-WAREHOUSE-SOHAVAILABILITY: TotalQuantityAvailable is null.";

    protected static final String ERR_STOCKUPDATE_EMPTY_PRODUCT =
            "ERR-WAREHOUSE-SOHPRODUCT: Both Product code and product EAN are empty.";

    protected static final String ERR_STOCKUPDATE_OTHER =
            "ERR-WAREHOUSE-SOHOTHER: Other exception occurred: {0}";

    protected static final String ERR_STOCKADJUST_WAREHOUSE =
            "ERR-WAREHOUSE-ADJWAREHOUSE: Warehouse with code {0} was not found";

    protected static final String ERR_STOCKADJUST_EMPTY_PRODUCT =
            "ERR-WAREHOUSE-ADJPRODUCT: Both of Product code and product EAN are empty.";

    protected static final String ERR_STOCKADJUST_PRODUCT =
            "ERR-WAREHOUSE-ADJPRODUCT: Product with code {0} was not found.";

    protected static final String ERR_STOCKADJUST_AVAILABILITY =
            "ERR-WAREHOUSE-ADJAVAILABILITY: AdjustmentQuantity is null.";

    protected static final String ERR_STOCKADJUST_STOCKLEVEL =
            "ERR_FASTLINE-ADJSTOCKLEVEL: StockLevel for product with code {0} and warehouse {1} was not found.";

    protected static final String ERR_STOCKADJUST_CREATESTOCKLEVEL =
            "ERR_FASTLINE-CREATESTOCKLEVEL: cannot create StockLevel for product with code {0} "
                    + " and warehouse {1} was not found.";

    protected static final String INFO_STOCKADJUST_ADJUPDATEOK = "INFO-STOCKONHAND-ADJUPDATEOK: Stock level adjusted"
            + " successfully for product: {0}";

    protected static final String ERR_WAREHOUSE_FINDSTOCKLEVEL = "ERR-STOCKONHAND-FINDSTOCK: No Stock level is found"
            + " for product: {0}";

    protected static final String INFO_STOCKADJUST_TOZERO_ADJUPDATEOK = "INFO-STOCKONHAND-ADJUPDATEOK: "
            + " Negative adjustment value supplied for non-existent StockLevel for product: {0}, setting StockLevel to zero!";

    protected static final String INFO_STOCKADJUSTNEWSTOCKLEVEL_ADJUPDATEOK = "INFO-STOCKONHAND-ADJUPDATEOK: Stock level "
            + "created successfully for product: {0}";

    protected static final String ERR_STOCKADJUST_OTHER = "ERR-WAREHOUSE-ADJOTHER: Other exception occurred: {0}";

    protected static final String INFO_WAREHOUSE_SOHUPDATEOK = "INFO-WAREHOUSE-SOHUPDATEOK: Stock level updated"
            + " successfully for product={0}, ean={1}, warehouse={2}";

    protected static final String INFO_WAREHOUSE_ADJUPDATEOK = "INFO-WAREHOUSE-ADJUPDATEOK: Stock level adjusted"

            + " successfully for product={0}, ean={1}, warehouse={2}, adjustmentQuantity={3}, comment={4}";

    protected static final String INFO_STOCK_UPDATE_BATCH = " transactionNumber={0}, transactionDate={1}, warehouse={2}, comment={3}";

    protected static final String ERR_STOCKUPDATEBATCH_EMPTY_ITEMS =
            "ERR-STOCKUPDATE-BATCHSIZE: Items in the payload are empty." + INFO_STOCK_UPDATE_BATCH;

    protected static final String ERR_STOCKUPDATEBATCH_EXCEED_MAX_BATCH_SIZE =
            "ERR-STOCKUPDATE-BATCHSIZE: The size of items in the payload exceeds the max batch size."
                    + INFO_STOCK_UPDATE_BATCH + ", max batch size in hybris={4}, batch size in the payload={5}";

    protected static final String ERR_STOCKUPDATEBATCH_WITH_ERROR =
            "ERR-STOCKUPDATEBATCH: Stock update batch has been processed with errors,"
                    + INFO_STOCK_UPDATE_BATCH + ", error items amount={4}";

    protected static final String INFO_STOCKUPDATEBATCH_SUCCESS =
            "INFO-STOCKUPDATEBATCH: Stock update batch has been processed successfully,"
                    + INFO_STOCK_UPDATE_BATCH;

    protected static final String ERR_STOCKADJUSTMENTBATCH_WITH_ERROR =
            "ERR-STOCKADJUSTMENTBATCH: Stock adjustment batch has been processed with errors,"
                    + INFO_STOCK_UPDATE_BATCH + ", error items amount={4}";

    protected static final String INFO_STOCKADJUSTMENTBATCH_SUCCESS =
            "INFO-STOCKADJUSTMENTBATCH: Stock adjustment batch has been processed successfully,"
                    + INFO_STOCK_UPDATE_BATCH;

    protected static final String ERROR = "ERROR";

    protected static final String NOT_FOUND = "NOT_FOUND";

    protected static final String OK = "OK";

    private static final Logger LOG = Logger.getLogger(StockUpdateIntegrationFacadeImpl.class);

    @Autowired
    private TargetStockService stockService;

    @Autowired
    private ProductService productService;

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private TargetProductService targetProductService;

    private int maxBatchSize;

    @Override
    public IntegrationStockUpdateResponseDto updateStock(final IntegrationStockUpdateDto dto) {

        final IntegrationStockUpdateResponseDto response = createResponse(dto);

        if (dto.getTotalQuantityAvailable() == null) {
            updateResponse(response, ERR_STOCKUPDATE_AVAILABILITY, ERROR);
            return response;
        }
        final int amount = dto.getTotalQuantityAvailable().intValue();

        WarehouseModel warehouse = null;
        try {
            warehouse = warehouseService.getWarehouseForCode(dto.getWarehouse());
        }
        catch (final UnknownIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKUPDATE_WAREHOUSE, dto.getWarehouse());
            updateResponse(response, errorMsg, ERROR);
            return response;
        }

        ProductModel product = null;
        try {
            final String ean = dto.getItemPrimaryBarcode();

            if (StringUtils.isNotEmpty(dto.getItemCode())) {
                product = productService.getProductForCode(dto.getItemCode());
            }
            else if (StringUtils.isNotEmpty(ean)) {
                product = targetProductService.getProductByEan(ean);
            }
            else {
                final String errorMsg = ERR_STOCKUPDATE_EMPTY_PRODUCT;
                updateResponse(response, errorMsg, ERROR);
                return response;
            }

        }
        catch (final UnknownIdentifierException e) {
            String errorMsg = null;
            if (amount > 0) {
                errorMsg = MessageFormat.format(ERR_STOCKUPDATE_PRODUCT_AVAILABLE, dto.getItemCode(),
                        dto.getTotalQuantityAvailable(), dto.getItemPrimaryBarcode());
            }
            else {
                errorMsg = MessageFormat
                        .format(ERR_STOCKUPDATE_PRODUCT, dto.getItemCode(), dto.getItemPrimaryBarcode());
            }
            updateResponse(response, errorMsg, NOT_FOUND);
            return response;
        }

        catch (final TargetUnknownIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKUPDATE_EAN, dto.getItemPrimaryBarcode());
            updateResponse(response, errorMsg, NOT_FOUND);
            return response;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKUPDATE_MULTILPLE_EAN, dto.getItemPrimaryBarcode(),
                    dto.getItemCode(),
                    dto.getWarehouse(), dto.getTotalQuantityAvailable(), dto.getAdjustmentQuantity(),
                    dto.getComment());
            updateResponse(response, errorMsg, ERROR);
            return response;
        }

        try {
            stockService.updateStockLevel(product, warehouse, amount, dto.getComment());
            setOnlineFromDate(product);
            response.setStatusCode(OK);
            LOG.info(MessageFormat.format(INFO_WAREHOUSE_SOHUPDATEOK, product.getCode(), dto.getItemPrimaryBarcode(),
                    warehouse.getCode()));
            return response;
        }
        catch (final StockLevelNotFoundException e) {
            final String errorMsg = MessageFormat.format(ERR_WAREHOUSE_FINDSTOCKLEVEL, product.getCode());
            updateResponse(response, errorMsg, NOT_FOUND);
            return response;
        }
        catch (final Exception e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKUPDATE_OTHER, e.getMessage());
            updateResponse(response, errorMsg, ERROR);
            return response;
        }
    }

    @Override
    public IntegrationStockUpdateResponseDto adjustStock(final IntegrationStockUpdateDto dto) {

        final IntegrationStockUpdateResponseDto response = createResponse(dto);

        if (dto.getAdjustmentQuantity() == null) {
            updateResponse(response, ERR_STOCKADJUST_AVAILABILITY, ERROR);
            return response;
        }
        final int adjustment = dto.getAdjustmentQuantity().intValue();

        WarehouseModel warehouse = null;
        try {
            warehouse = warehouseService.getWarehouseForCode(dto.getWarehouse());
        }
        catch (final UnknownIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKADJUST_WAREHOUSE, dto.getWarehouse());
            updateResponse(response, errorMsg, ERROR);
            return response;
        }

        ProductModel product = null;
        try {
            if (StringUtils.isNotEmpty(dto.getItemCode())) {
                product = productService.getProductForCode(dto.getItemCode());
            }
            else if (StringUtils.isNotEmpty(dto.getItemPrimaryBarcode())) {
                product = targetProductService.getProductByEan(dto.getItemPrimaryBarcode());
            }
            else {
                final String errorMsg = ERR_STOCKADJUST_EMPTY_PRODUCT;
                updateResponse(response, errorMsg, ERROR);
                return response;
            }

        }
        catch (final UnknownIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKADJUST_PRODUCT, dto.getItemCode());
            updateResponse(response, errorMsg, NOT_FOUND);
            return response;
        }
        catch (final TargetUnknownIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKUPDATE_EAN, dto.getItemPrimaryBarcode());
            updateResponse(response, errorMsg, NOT_FOUND);
            return response;
        }
        catch (final TargetAmbiguousIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKUPDATE_MULTILPLE_EAN, dto.getItemPrimaryBarcode(),
                    dto.getItemCode(),
                    dto.getWarehouse(), dto.getTotalQuantityAvailable(), dto.getAdjustmentQuantity(),
                    dto.getComment());
            updateResponse(response, errorMsg, ERROR);
            return response;
        }

        try {
            stockService.adjustActualAmount(product, warehouse, adjustment, dto.getComment());
            setOnlineFromDate(product);
            response.setStatusCode(OK);
            LOG.info(MessageFormat.format(INFO_WAREHOUSE_ADJUPDATEOK, product.getCode(), dto.getItemPrimaryBarcode(),
                    warehouse.getCode(),
                    dto.getAdjustmentQuantity(), dto.getComment()));
            return response;
        }
        catch (final StockLevelNotFoundException e) {
            final String errorMsg = MessageFormat.format(ERR_WAREHOUSE_FINDSTOCKLEVEL, product.getCode());
            updateResponse(response, errorMsg, NOT_FOUND);
            return response;
        }
        catch (final Exception e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKADJUST_OTHER, e.getMessage());
            updateResponse(response, errorMsg, ERROR);
            return response;
        }
    }

    @Override
    public IntegrationResponseDto updateStock(final StockUpdate stockUpdate)
    {
        final IntegrationResponseDto response = new IntegrationResponseDto(null);

        final int adjustment = stockUpdate.getQuantity();

        WarehouseModel warehouse = null;
        try {
            warehouse = warehouseService.getWarehouseForCode(stockUpdate.getWarehouseCode());
        }
        catch (final UnknownIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKADJUST_WAREHOUSE, stockUpdate.getWarehouseCode());
            response.setMessages(Arrays.asList(errorMsg));
            LOG.error(errorMsg, e);
            return response;
        }

        ProductModel product = null;
        try {
            product = productService.getProductForCode(stockUpdate.getProductCode());
        }
        catch (final UnknownIdentifierException e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKADJUST_PRODUCT, stockUpdate.getProductCode());
            response.setMessages(Arrays.asList(errorMsg));
            LOG.error(errorMsg, e);
            return response;
        }



        try {
            if (adjustment == 0) {
                stockService.updateActualStockLevel(product, warehouse, adjustment, "");
            }
            else {
                stockService.adjustActualAmount(product, warehouse, adjustment, "");
            }
            setOnlineFromDate(product);
            response.setSuccessStatus(true);
            LOG.info(MessageFormat.format(INFO_STOCKADJUST_ADJUPDATEOK, stockUpdate.getProductCode()));
            return response;
        }
        catch (final StockLevelNotFoundException e) {
            return createStockLevel(product, warehouse, response, adjustment);
        }
        catch (final Exception e) {
            final String errorMsg = MessageFormat.format(ERR_STOCKADJUST_OTHER, e.getMessage());
            response.setMessages(Arrays.asList(errorMsg));
            LOG.error(errorMsg, e);
        }

        return response;
    }

    @Override
    public TargetStockUpdateResponseDto updateStockBatch(final TargetStockUpdateDto dto) {
        final TargetStockUpdateResponseDto response = createStockUpdateBatchResponse(dto);
        if (checkBatchSize(dto, response)) {
            final List<TargetStockUpdateProductResponseDto> failedItems = new ArrayList<>();
            for (final TargetStockUpdateProductDto stockUpdateDto : dto.getItems()) {
                final IntegrationStockUpdateDto integrationStockUpdateDto = convertIntegrationStockUpdateDto(dto,
                        stockUpdateDto);
                final IntegrationStockUpdateResponseDto integrationStockUpdateResponseDto = this
                        .updateStock(integrationStockUpdateDto);
                if (IntegrationStockUpdateResponseDto.STATUS_ERROR.equals(integrationStockUpdateResponseDto
                        .getStatusCode())) {
                    failedItems.add(convertTargetStockUpdateProductResponseDto(integrationStockUpdateResponseDto));
                }
            }
            if (CollectionUtils.isNotEmpty(failedItems)) {
                final String errorMessage = MessageFormat.format(ERR_STOCKUPDATEBATCH_WITH_ERROR,
                        dto.getTransactionNumber(), dto.getTransactionDate(), dto.getWarehouse(), dto.getComment(),
                        Integer.valueOf(failedItems.size()));
                LOG.error(errorMessage);
                response.setFailedItems(failedItems);
                updateStatusForStockUpdateBatchResponse(response, TargetStockUpdateResponseDto.STATUS_ERROR,
                        errorMessage);
            }
            else {
                final String infoMessage = MessageFormat.format(INFO_STOCKUPDATEBATCH_SUCCESS,
                        dto.getTransactionNumber(), dto.getTransactionDate(), dto.getWarehouse(), dto.getComment());
                LOG.info(infoMessage);
                updateStatusForStockUpdateBatchResponse(response, TargetStockUpdateResponseDto.STATUS_OK,
                        infoMessage);
            }
        }
        return response;
    }

    @Override
    public TargetStockUpdateResponseDto adjustStockBatch(final TargetStockUpdateDto dto) {
        final TargetStockUpdateResponseDto response = createStockUpdateBatchResponse(dto);
        if (checkBatchSize(dto, response)) {
            final List<TargetStockUpdateProductResponseDto> failedItems = new ArrayList<>();
            for (final TargetStockUpdateProductDto stockUpdateDto : dto.getItems()) {
                final IntegrationStockUpdateDto integrationStockUpdateDto = convertIntegrationStockUpdateDto(dto,
                        stockUpdateDto);
                final IntegrationStockUpdateResponseDto integrationStockUpdateResponseDto = this
                        .adjustStock(integrationStockUpdateDto);
                if (IntegrationStockUpdateResponseDto.STATUS_ERROR.equals(integrationStockUpdateResponseDto
                        .getStatusCode())) {
                    failedItems.add(convertTargetStockUpdateProductResponseDto(integrationStockUpdateResponseDto));
                }
            }
            if (CollectionUtils.isNotEmpty(failedItems)) {
                final String errorMessage = MessageFormat.format(ERR_STOCKADJUSTMENTBATCH_WITH_ERROR,
                        dto.getTransactionNumber(), dto.getTransactionDate(), dto.getWarehouse(), dto.getComment(),
                        Integer.valueOf(failedItems.size()));
                LOG.error(errorMessage);
                response.setFailedItems(failedItems);
                updateStatusForStockUpdateBatchResponse(response, TargetStockUpdateResponseDto.STATUS_ERROR,
                        errorMessage);
            }
            else {
                final String infoMessage = MessageFormat.format(INFO_STOCKADJUSTMENTBATCH_SUCCESS,
                        dto.getTransactionNumber(), dto.getTransactionDate(), dto.getWarehouse(), dto.getComment());
                LOG.info(infoMessage);
                updateStatusForStockUpdateBatchResponse(response, TargetStockUpdateResponseDto.STATUS_OK,
                        infoMessage);
            }
        }
        return response;
    }

    /**
     * create stock update batch response
     * 
     * @param dto
     * @return TargetStockUpdateResponseDto
     */
    private TargetStockUpdateResponseDto createStockUpdateBatchResponse(final TargetStockUpdateDto dto) {
        final TargetStockUpdateResponseDto response = new TargetStockUpdateResponseDto();
        response.setTransactionNumber(dto.getTransactionNumber());
        response.setTransactionDate(dto.getTransactionDate());
        return response;
    }

    /**
     * update status code for stock update batch response
     * 
     * @param response
     * @param status
     * @param message
     */
    private void updateStatusForStockUpdateBatchResponse(
            final TargetStockUpdateResponseDto response, final String status, final String message) {
        response.setStatusCode(status);
        response.setMessage(message);
    }

    /**
     * check the batch size
     * 
     * @param dto
     * @param response
     * @return isValid
     */
    private boolean checkBatchSize(final TargetStockUpdateDto dto, final TargetStockUpdateResponseDto response) {
        boolean isValid = true;
        if (dto.getItems() == null) {
            final String errorMessage = MessageFormat.format(ERR_STOCKUPDATEBATCH_EMPTY_ITEMS,
                    dto.getTransactionNumber(), dto.getTransactionDate(), dto.getWarehouse(), dto.getComment());
            LOG.error(errorMessage);
            updateStatusForStockUpdateBatchResponse(response, TargetStockUpdateResponseDto.STATUS_ERROR, errorMessage);
            isValid = false;
        }
        else if (dto.getItems().size() > this.maxBatchSize) {
            final String errorMessage = MessageFormat.format(ERR_STOCKUPDATEBATCH_EXCEED_MAX_BATCH_SIZE,
                    dto.getTransactionNumber(), dto.getTransactionDate(), dto.getWarehouse(), dto.getComment(),
                    Integer.valueOf(this.maxBatchSize), Integer.valueOf(dto.getItems().size()));
            LOG.error(errorMessage);
            updateStatusForStockUpdateBatchResponse(response,
                    TargetStockUpdateResponseDto.STATUS_EXCEED_MAX_BATCH_SIZE, errorMessage);
            isValid = false;
        }
        return isValid;
    }


    private IntegrationStockUpdateDto convertIntegrationStockUpdateDto(final TargetStockUpdateDto dto,
            final TargetStockUpdateProductDto productDto) {
        final IntegrationStockUpdateDto targetDto = new IntegrationStockUpdateDto();
        targetDto.setTransactionNumber(dto.getTransactionNumber());
        targetDto.setTransactionDate(dto.getTransactionDate());
        targetDto.setWarehouse(dto.getWarehouse());
        targetDto.setComment(dto.getComment());
        targetDto.setItemCode(productDto.getItemCode());
        targetDto.setItemPrimaryBarcode(productDto.getItemPrimaryBarcode());
        targetDto.setTotalQuantityAvailable(productDto.getTotalQuantityAvailable());
        targetDto.setAdjustmentQuantity(productDto.getAdjustmentQuantity());
        return targetDto;
    }

    private TargetStockUpdateProductResponseDto convertTargetStockUpdateProductResponseDto(
            final IntegrationStockUpdateResponseDto dto) {
        final TargetStockUpdateProductResponseDto targetDto = new TargetStockUpdateProductResponseDto();
        targetDto.setItemCode(dto.getItemCode());
        targetDto.setItemPrimaryBarcode(dto.getItemPrimaryBarcode());
        targetDto.setMessage(dto.getMessage());
        targetDto.setStatusCode(dto.getStatusCode());
        targetDto.setTotalQuantityAvailable(dto.getTotalQuantityAvailable());
        targetDto.setAdjustmentQuantity(dto.getAdjustmentQuantity());
        return targetDto;
    }

    /**
     * @param productModel
     *            the Product Model
     * @param warehouseModel
     *            the Warehouse Mode;
     * @param integrationResponseDto
     *            the response
     * @param adjustment
     *            the adjustment
     * @return IntegrationResponseDto the response
     */
    private IntegrationResponseDto createStockLevel(final ProductModel productModel,
            final WarehouseModel warehouseModel,
            final IntegrationResponseDto integrationResponseDto,
            final int adjustment) {
        final StockLevelModel stockLevel = modelService.create(StockLevelModel.class);
        stockLevel.setProduct(productModel);
        stockLevel.setProductCode(productModel.getCode());
        stockLevel.setWarehouse(warehouseModel);
        stockLevel.setAvailable(adjustment);
        try {
            modelService.save(stockLevel);
            integrationResponseDto.setSuccessStatus(true);
            LOG.info(MessageFormat.format(INFO_STOCKADJUSTNEWSTOCKLEVEL_ADJUPDATEOK, productModel.getCode()));
        }
        catch (final ModelSavingException e) {

            LOG.error(e.getMessage(), e);
            integrationResponseDto.setMessages(Arrays.asList(MessageFormat.format(ERR_STOCKADJUST_CREATESTOCKLEVEL,
                    productModel.getCode(),
                    warehouseModel.getCode())));
        }

        setOnlineFromDate(productModel);
        return integrationResponseDto;
    }


    /**
     * @param dto
     *            dto
     * @return IntegrationStockUpdateResponseDto
     */
    private IntegrationStockUpdateResponseDto createResponse(final IntegrationStockUpdateDto dto) {
        final IntegrationStockUpdateResponseDto response = new IntegrationStockUpdateResponseDto();
        response.setItemCode(dto.getItemCode());
        response.setItemPrimaryBarcode(dto.getItemPrimaryBarcode());
        response.setTransactionDate(dto.getTransactionDate());
        response.setTransactionNumber(dto.getTransactionNumber());
        response.setTotalQuantityAvailable(dto.getTotalQuantityAvailable());
        response.setAdjustmentQuantity(dto.getAdjustmentQuantity());
        return response;
    }

    /**
     * @param response
     *            the response
     * @param errorMsg
     *            the error message
     * @param statusCode
     *            the status code
     */
    private void updateResponse(final IntegrationStockUpdateResponseDto response, final String errorMsg,
            final String statusCode) {
        if (ERROR.equalsIgnoreCase(statusCode)) {
            LOG.error(errorMsg);
        }
        else {
            LOG.warn(errorMsg);
        }
        response.setMessage(errorMsg);
        response.setStatusCode(statusCode);
    }

    /**
     * @param product
     */
    private void setOnlineFromDate(final ProductModel product) {
        targetProductService.setOnlineDateIfApplicable(product);
    }

    /**
     * @param maxBatchSize
     *            the maxBatchSize to set
     */
    @Required
    public void setMaxBatchSize(final int maxBatchSize) {
        this.maxBatchSize = maxBatchSize;
    }



}
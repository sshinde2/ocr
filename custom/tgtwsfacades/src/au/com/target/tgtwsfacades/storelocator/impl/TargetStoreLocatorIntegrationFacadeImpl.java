/**
 * 
 */
package au.com.target.tgtwsfacades.storelocator.impl;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.AddressService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.OpeningDayModel;
import de.hybris.platform.storelocator.model.OpeningScheduleModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.TargetOpeningDayModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.product.ProductTypeService;
import au.com.target.tgtcore.storelocator.TargetOpeningDayService;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationAddressDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationOpeningDayDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationOpeningScheduleDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationPointOfServiceDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.storelocator.TargetStoreLocatorIntegrationFacade;


/**
 * @author fkratoch
 * 
 */
public class TargetStoreLocatorIntegrationFacadeImpl implements TargetStoreLocatorIntegrationFacade {

    private static final Logger LOG = Logger.getLogger(TargetStoreLocatorIntegrationFacadeImpl.class);

    @Autowired
    private TargetPointOfServiceService targetPointOfServiceService;

    @Autowired
    private BaseStoreService baseStoreService;

    @Autowired
    private AddressService addressService;

    @Autowired
    private CommonI18NService commonService;

    @Autowired
    private ModelService modelService;

    @Autowired
    private ProductTypeService productTypeService;

    @Autowired
    private TypeService typeService;

    @Resource
    private TargetOpeningDayService targetOpeningDayService;

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.storelocator.StoreLocatorIntegrationFacade#getPointOfService(java.lang.String)
     */
    @Override
    public IntegrationPointOfServiceDto getPointOfService(final Integer storeNumber) {
        final TargetPointOfServiceModel pos = getExistingStore(storeNumber);


        if (pos == null) {
            return null;
        }

        return convertPointOfService(pos);
    }

    private IntegrationPointOfServiceDto convertPointOfService(final TargetPointOfServiceModel pos) {

        final IntegrationPointOfServiceDto dto = new IntegrationPointOfServiceDto();
        dto.setStoreNumber(pos.getStoreNumber());
        dto.setStoreName(pos.getName());
        dto.setDescription(pos.getDescription());
        dto.setLatitude(pos.getLatitude());
        dto.setLongitude(pos.getLongitude());
        dto.setAcceptCNC(pos.getAcceptCNC());
        dto.setAcceptLayBy(pos.getAcceptLayBy());
        dto.setClosed(pos.getClosed());
        dto.setBaseStore(pos.getBaseStore().getUid());
        dto.setType(typeService.getEnumerationValue(PointOfServiceTypeEnum._TYPECODE, pos.getType().getCode())
                .getName());

        final AddressModel posAddress = pos.getAddress();
        if (posAddress != null) {
            final IntegrationAddressDto storeAddress = new IntegrationAddressDto();

            storeAddress.setBuilding(posAddress.getBuilding());
            storeAddress.setStreetname(posAddress.getStreetname());
            storeAddress.setTown(posAddress.getTown());
            storeAddress.setDistrict(posAddress.getDistrict());
            storeAddress.setPostCode(posAddress.getPostalcode());
            if (posAddress.getCountry() != null) {
                storeAddress.setCountry(posAddress.getCountry().getName());
            }
            storeAddress.setPhone(posAddress.getPhone1());

            dto.setAddress(storeAddress);
        }

        return dto;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgtfacades.storelocator.StoreLocatorIntegrationFacade#persistPointOfService
     * (au.com.target.tgtfacades.storelocator.dto.IntegrationPointOfServiceDto)
     */
    @Override
    public IntegrationResponseDto persistPointOfService(final IntegrationPointOfServiceDto posDto) {
        final IntegrationResponseDto response = new IntegrationResponseDto(posDto.getStoreNumber().toString(),
                posDto.getStoreName());

        try {

            final boolean onlySchedule = posDto.isHasOnlySchedule();

            final IntegrationAddressDto addressDto = posDto.getAddress();
            final boolean haveAddress = (addressDto != null);
            AddressModel address = null;

            final IntegrationOpeningScheduleDto scheduleDto = posDto.getOpeningSchedule();
            final boolean haveSchedule = (scheduleDto != null);


            TargetPointOfServiceModel pos = getExistingStore(posDto.getStoreNumber());

            if (!onlySchedule && !haveAddress) {
                response.setSuccessStatus(false);
                response.addMessage("Cannot create/update PointOfService without a valid address!");
                return response;
            }

            // new stores
            if (pos == null) {

                if (onlySchedule) {
                    response.setSuccessStatus(false);
                    response.addMessage("Cannot create an OpeningSchedule for PointOfService that does not exist");
                    return response;
                }

                pos = createNewPointOfService(posDto);
                modelService.save(pos);
            }

            if (!onlySchedule) {
                //# Populate base details  
                populateBaseDetails(pos, posDto);

                //# Attach Address
                address = pos.getAddress();
                if (address == null) {
                    address = addressService.createAddressForOwner(pos);
                    pos.setAddress(address);
                }
                populateAddressDetails(address, addressDto);
                modelService.save(address);
            }

            if (haveSchedule) {
                final OpeningScheduleModel schedule = pos.getOpeningSchedule();
                populateOpeningSchedule(schedule, scheduleDto);
                modelService.save(schedule);
            }

            modelService.save(pos);
            response.setSuccessStatus(true);
        }
        catch (final Exception ex) {
            LOG.error("Unable to create/update point of service", ex);
            response.setSuccessStatus(false);
            final String errMsg = ex.getMessage();

            response.addMessage(errMsg == null ? "Runtime exception occured." : errMsg);
        }

        return response;
    }

    /**
     * 
     * @param storeNumber
     *            storeNumber of an existing store
     * @return TargetPointOfService if it exists, otherwise null
     */
    private TargetPointOfServiceModel getExistingStore(final Integer storeNumber) {
        TargetPointOfServiceModel pos = null;
        try {
            pos = targetPointOfServiceService.getPOSByStoreNumber(storeNumber);
        }
        catch (final Exception ex) {
            LOG.info(ex.getMessage());
        }

        return pos;
    }

    @Override
    public Map<String, List<IntegrationPointOfServiceDto>> getStateAndStoresForCart(final CartModel cartModel) {

        final Map<String, Set<TargetPointOfServiceModel>> storesModel = targetPointOfServiceService
                .getStateAndStoresForCart(cartModel);

        final Map<String, List<IntegrationPointOfServiceDto>> storesDto = new HashMap<String, List<IntegrationPointOfServiceDto>>();

        for (final Entry<String, Set<TargetPointOfServiceModel>> entry : storesModel.entrySet()) {
            final List<IntegrationPointOfServiceDto> setIntegrationPointOfServiceDto = new ArrayList<>();
            for (final TargetPointOfServiceModel targetPointOfServiceModel : entry.getValue()) {
                setIntegrationPointOfServiceDto.add(convertPointOfService(targetPointOfServiceModel));
            }
            storesDto.put(entry.getKey(), setIntegrationPointOfServiceDto);
        }

        return storesDto;
    }

    /**
     * 
     * @param posDto
     *            IntegrationPointOfServiceDto entity
     * @return new instance of TargetPointOfServiceModel that is already persisted
     */
    private TargetPointOfServiceModel createNewPointOfService(final IntegrationPointOfServiceDto posDto) {
        final Integer storeNumber = posDto.getStoreNumber();

        final TargetPointOfServiceModel pos = modelService.create(TargetPointOfServiceModel.class);
        pos.setStoreNumber(storeNumber);
        pos.setName(posDto.getStoreName());
        pos.setType(posDto.getType().equalsIgnoreCase("target")
                ? PointOfServiceTypeEnum.TARGET
                : PointOfServiceTypeEnum.TARGETCOUNTRY);

        //# Attach BaseStore
        if (posDto.getBaseStore() != null) {
            final BaseStoreModel baseStore = baseStoreService.getBaseStoreForUid(posDto.getBaseStore());
            pos.setBaseStore(baseStore);
        }

        //# Attach Normal ProductType
        final Set<ProductTypeModel> productTypes = new HashSet<>();
        productTypes.add(getProductType("Normal"));
        pos.setProductTypes(productTypes);

        //# Create new Opening Schedule
        final OpeningScheduleModel openingSchedule = modelService.create(OpeningScheduleModel.class);
        final String nameByConvention = "opening-schedule-" + storeNumber;
        openingSchedule.setCode(nameByConvention);
        openingSchedule.setName(nameByConvention);
        openingSchedule.setOwner(pos);
        pos.setOpeningSchedule(openingSchedule);

        return pos;
    }

    /**
     * 
     * @param pos
     *            TargetPointOfServiceModel entity
     * @param dto
     *            IntegrationPointOfServiceDto entity
     */
    private void populateBaseDetails(final TargetPointOfServiceModel pos, final IntegrationPointOfServiceDto dto) {
        final Boolean acceptCnc = dto.getAcceptCNC();

        pos.setDescription(dto.getDescription());
        pos.setClosed(dto.getClosed());
        pos.setAcceptCNC(acceptCnc);
        pos.setAcceptLayBy(dto.getAcceptLayBy());
        pos.setLatitude(dto.getLatitude());
        pos.setLongitude(dto.getLongitude());

        final Set<ProductTypeModel> productTypes = new HashSet<>();

        if (acceptCnc != null && acceptCnc.booleanValue()) {
            productTypes.add(getProductType("Normal"));
        }

        if (dto.getProductTypes() != null && dto.getProductTypes().getType() != null) {
            for (final String type : dto.getProductTypes().getType()) {
                productTypes.add(getProductType(type));
            }
        }
        pos.setProductTypes(productTypes);
    }

    /**
     * Populate address details
     * 
     * @param model
     *            AddressModel entity
     * @param dto
     *            IntegrationAddressDto entity
     */
    private void populateAddressDetails(final AddressModel model, final IntegrationAddressDto dto) {
        model.setStreetname(dto.getStreetname());
        model.setBuilding(dto.getBuilding());
        model.setTown(dto.getTown());
        model.setPostalcode(dto.getPostCode());
        model.setDistrict(dto.getDistrict());
        model.setPhone1(dto.getPhone());
        model.setCountry(getCountry());
    }

    /**
     * Return CountryModel entity
     * 
     * @return CountryMOdel entity
     */
    private CountryModel getCountry() {
        //# this should be replaced with another function in the API to lookup countries by name
        //# at this stage we only have Australian stores
        return commonService.getCountry("AU");
    }

    /**
     * 
     * @param schedule
     *            OpeningScheduleModel entity
     * @param scheduleDto
     *            IntegrationOpeningScheduleDto entity
     */
    protected void populateOpeningSchedule(final OpeningScheduleModel schedule,
            final IntegrationOpeningScheduleDto scheduleDto) {

        final List<IntegrationOpeningDayDto> dtoOpeningDays = scheduleDto.getOpeningDays();
        if (CollectionUtils.isNotEmpty(dtoOpeningDays)) {
            populateOpeningDaysAndRemoveUnused(schedule, dtoOpeningDays,
                    scheduleDto.getStoreNumber());
        }
    }

    /**
     * 
     * @param schedule
     *            OpeningScheduleModel entity
     * @param openingDayDtos
     *            List of IntegrationOpeningDayDto entities
     * @param storeNumber
     */
    private void populateOpeningDaysAndRemoveUnused(final OpeningScheduleModel schedule,
            final List<IntegrationOpeningDayDto> openingDayDtos, final String storeNumber) {

        final List<OpeningDayModel> openingDaysToBeRemoved = new ArrayList<>();

        final Collection<OpeningDayModel> existingOpeningDays = schedule.getOpeningDays();
        if (CollectionUtils.isNotEmpty(existingOpeningDays)) {
            for (final OpeningDayModel openingDayModel : existingOpeningDays) {
                openingDaysToBeRemoved.add(openingDayModel);
            }
        }

        for (final IntegrationOpeningDayDto openingDayDto : openingDayDtos) {

            if (openingDayDto.getClosed() == null) {
                throw new IllegalStateException("Missing a required value for store Closed flag.");
            }

            final String targetOpeningDayId = targetOpeningDayService.getTargetOpeningDayId(storeNumber,
                    openingDayDto.getOpeningTime());
            TargetOpeningDayModel openingDay = null;

            try {
                openingDay = targetOpeningDayService.getTargetOpeningDay(schedule, targetOpeningDayId);
            }
            catch (final TargetAmbiguousIdentifierException ex) {
                LOG.error(
                        "Unable to create or update opening day for store: " + storeNumber + ", day: "
                                + openingDayDto.getOpeningTime() + " because too many already exist", ex);
            }

            if (openingDay != null) {
                LOG.info("Found existing opening day for store: " + storeNumber + ", day: "
                        + openingDayDto.getOpeningTime() + ". Hence updating it.");
                openingDaysToBeRemoved.remove(openingDay);
            }
            else {
                LOG.info("Couldn't find existing opening day for store: " + storeNumber + ", day: "
                        + openingDayDto.getOpeningTime() + ". Hence creating new one.");
                openingDay = modelService.create(TargetOpeningDayModel.class);

                openingDay.setTargetOpeningDayId(targetOpeningDayId);
                openingDay.setOwner(schedule);
                openingDay.setOpeningSchedule(schedule);
                openingDay.setOpeningTime(openingDayDto.getOpeningTime());
            }

            // TODO: workaround for CLOSED days in order to satisfy TargetOpeningDayValidateInterceptor
            // add 1 minute to the closing so it is after opening time
            Date closingDate = openingDayDto.getClosingTime();
            final boolean isClosed = openingDayDto.getClosed().booleanValue();
            if (isClosed) {
                final Calendar cal = Calendar.getInstance();
                cal.setTime(closingDate);
                cal.add(Calendar.HOUR_OF_DAY, 1); // adds one hour
                closingDate = cal.getTime();
            }
            openingDay.setOpeningTime(openingDayDto.getOpeningTime());
            openingDay.setClosingTime(closingDate);
            openingDay.setClosed(isClosed);
            modelService.save(openingDay);
        }

        if (CollectionUtils.isNotEmpty(openingDaysToBeRemoved)) {
            modelService.removeAll(openingDaysToBeRemoved);
        }
    }

    /**
     * 
     * @param typeCode
     *            unique code
     * @return ProductTypeModel entity
     */
    private ProductTypeModel getProductType(final String typeCode) {
        return productTypeService.getByCode(typeCode.toLowerCase(Locale.ENGLISH));
    }

}

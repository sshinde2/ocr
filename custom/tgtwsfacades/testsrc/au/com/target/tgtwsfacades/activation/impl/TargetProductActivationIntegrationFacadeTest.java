/**
 * 
 */
package au.com.target.tgtwsfacades.activation.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetPreviousPermanentPriceService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductActivationIntegrationFacadeTest {

    @Mock
    private ProductService productService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetPreviousPermanentPriceService targetPreviousPermanentPriceService;

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private FluentSkuUpsertService fluentSkuUpsertService;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @InjectMocks
    @Spy
    private final TargetProductActivationIntegrationFacade activationIntegrationFacade = new TargetProductActivationIntegrationFacade();

    @Test
    public void testSyncToFluent() {
        final IntegrationResponseDto response = mock(IntegrationResponseDto.class);
        willReturn(Boolean.TRUE).given(response).isSuccessStatus();
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant = mock(TargetSizeVariantProductModel.class);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();
        final List<AbstractTargetVariantProductModel> sellableVariants = new ArrayList<>();
        sellableVariants.add(sizeVariant);
        willReturn(sellableVariants).given(activationIntegrationFacade).getSellableVariants(colourVariant, sizeVariant);

        activationIntegrationFacade.syncToFluent(response, colourVariant, sizeVariant);
        verify(fluentSkuUpsertService).upsertSku(sizeVariant);
        verifyNoMoreInteractions(fluentSkuUpsertService);
    }

    @Test
    public void testSyncToFluentFeatureOff() {
        final IntegrationResponseDto response = mock(IntegrationResponseDto.class);
        willReturn(Boolean.TRUE).given(response).isSuccessStatus();
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant = mock(TargetSizeVariantProductModel.class);
        willReturn(Boolean.FALSE).given(targetFeatureSwitchFacade).isFluentEnabled();

        activationIntegrationFacade.syncToFluent(response, colourVariant, sizeVariant);
        verify(activationIntegrationFacade, never()).getSellableVariants(colourVariant, sizeVariant);
        verify(fluentSkuUpsertService, never()).upsertSku(Mockito.any(AbstractTargetVariantProductModel.class));
    }

    @Test
    public void testSyncToFluentResponseFail() {
        final IntegrationResponseDto response = mock(IntegrationResponseDto.class);
        willReturn(Boolean.FALSE).given(response).isSuccessStatus();
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant = mock(TargetSizeVariantProductModel.class);
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFluentEnabled();

        activationIntegrationFacade.syncToFluent(response, colourVariant, sizeVariant);
        verify(activationIntegrationFacade, never()).getSellableVariants(colourVariant, sizeVariant);
        verify(fluentSkuUpsertService, never()).upsertSku(Mockito.any(AbstractTargetVariantProductModel.class));
    }

    @Test
    public void testGetSellableVariantsWithColourOnly() {
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);

        final List<AbstractTargetVariantProductModel> sellableVariants = activationIntegrationFacade
                .getSellableVariants(colourVariant, null);
        assertThat(sellableVariants).hasSize(1).containsExactly(colourVariant);
    }

    @Test
    public void testGetSellableVariantsWithSizeOnly() {
        final TargetSizeVariantProductModel sizeVariant = mock(TargetSizeVariantProductModel.class);

        final List<AbstractTargetVariantProductModel> sellableVariants = activationIntegrationFacade
                .getSellableVariants(null, sizeVariant);
        assertThat(sellableVariants).hasSize(1).containsExactly(sizeVariant);
    }

    @Test
    public void testGetSellableVariantsWithColourAndSize() {
        final TargetColourVariantProductModel colourVariant = mock(TargetColourVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant1 = mock(TargetSizeVariantProductModel.class);
        final TargetSizeVariantProductModel sizeVariant2 = mock(TargetSizeVariantProductModel.class);
        final List<VariantProductModel> variants = new ArrayList<>();
        variants.add(sizeVariant1);
        variants.add(sizeVariant2);
        given(colourVariant.getVariants()).willReturn(variants);

        final List<AbstractTargetVariantProductModel> sellableVariants = activationIntegrationFacade
                .getSellableVariants(colourVariant, sizeVariant1);
        assertThat(sellableVariants).hasSize(1).containsExactly(sizeVariant1);
    }
}

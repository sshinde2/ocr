package au.com.target.tgtwsfacades.cnc;


import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SalesApplication;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.enums.ProcessState;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtbusproc.TargetBusinessProcessService;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.salesapplication.TargetSalesApplicationConfigService;
import au.com.target.tgtfacades.storelocator.TargetStoreLocatorFacade;
import au.com.target.tgtfacades.storelocator.data.TargetPointOfServiceData;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;
import au.com.target.tgtwsfacades.cnc.impl.ClickAndCollectNotificationFacadeImpl;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ClickAndCollectNotificationFacadeImplTest {

    private static final String ORDER_NUMBER = "01234567";
    private static final String STORE_NUMBER = "1234";

    @InjectMocks
    private final ClickAndCollectNotificationFacadeImpl clickAndCollectNotificationFacadeImpl = new ClickAndCollectNotificationFacadeImpl();

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private TargetStoreLocatorFacade targetStoreLocatorFacade;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetBusinessProcessService targetBusinessProcessService;

    @Mock
    private TargetConsignmentService targetConsignmentService;

    @Mock
    private ModelService modelService;

    @Mock
    private TargetConsignmentModel targetConsignmentModel;

    @Mock
    private TargetSalesApplicationConfigService salesApplicationConfigService;

    @Before
    public void setUp() {
        given(targetOrderService.findOrderModelForOrderId((anyString()))).willReturn(orderModel);
        final TargetConsignmentModel consignment = new TargetConsignmentModel();
        final List<TargetConsignmentModel> consignments = new ArrayList<>();
        consignments.add(consignment);
        given(targetConsignmentService.getActiveConsignmentsForOrder(orderModel)).willReturn(consignments);
    }

    @Test
    public void testTriggerCNCNotificationWithSuccessForPartnerOrder() {

        final String orderId = "00123456";
        final String storeNumber = "7001";
        final String letterType = "13";
        final SalesApplication salesApplicationMock = Mockito.mock(SalesApplication.class);

        final TargetPointOfServiceData targetPointOfServiceData = new TargetPointOfServiceData();
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(storeNumber))).willReturn(
                targetPointOfServiceData);
        final OrderProcessModel orderProcessModel = new OrderProcessModel();
        orderProcessModel.setState(ProcessState.SUCCEEDED);
        given(
                targetBusinessProcessService.startSendClickAndCollectNotificationProcess(any(OrderModel.class),
                        anyObject(), anyBoolean())).willReturn(orderProcessModel);
        given(orderModel.getSalesApplication()).willReturn(salesApplicationMock);
        given(Boolean.valueOf(salesApplicationConfigService.isPartnerChannel(salesApplicationMock)))
                .willReturn(Boolean.TRUE);
        final List<TargetConsignmentModel> consignments = new ArrayList<>();
        consignments.add(targetConsignmentModel);
        given(targetConsignmentService.getActiveDeliverToStoreConsignmentsForOrder(orderModel))
                .willReturn(consignments);
        final boolean result = clickAndCollectNotificationFacadeImpl.triggerCNCNotification(orderId, storeNumber,
                letterType);
        verify(targetConsignmentService).updateReadyForPickupDate(any(TargetConsignmentModel.class));
        assertThat(result).isTrue();

    }

    @Test
    public void testTriggerCNCNotificationWithSuccessForNonPartnerOrder() {

        final String orderId = "00123456";
        final String storeNumber = "7001";
        final String letterType = "13";
        final SalesApplication salesApplicationMock = Mockito.mock(SalesApplication.class);
        given(targetOrderService.findOrderModelForOrderId((Mockito.anyString()))).willReturn(orderModel);
        final TargetPointOfServiceData targetPointOfServiceData = new TargetPointOfServiceData();
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(storeNumber))).willReturn(
                targetPointOfServiceData);
        final OrderProcessModel orderProcessModel = new OrderProcessModel();
        orderProcessModel.setState(ProcessState.SUCCEEDED);
        given(
                targetBusinessProcessService.startSendClickAndCollectNotificationProcess(any(OrderModel.class),
                        anyObject(), anyBoolean())).willReturn(orderProcessModel);
        given(orderModel.getSalesApplication()).willReturn(salesApplicationMock);
        given(Boolean.valueOf(salesApplicationConfigService.isPartnerChannel(salesApplicationMock)))
                .willReturn(Boolean.FALSE);
        final List<TargetConsignmentModel> consignments = new ArrayList<>();
        consignments.add(targetConsignmentModel);
        given(targetConsignmentService.getActiveDeliverToStoreConsignmentsForOrder(orderModel))
                .willReturn(consignments);
        final boolean result = clickAndCollectNotificationFacadeImpl.triggerCNCNotification(orderId, storeNumber,
                letterType);
        verify(targetConsignmentService).updateReadyForPickupDate(any(TargetConsignmentModel.class));
        assertThat(result).isTrue();

    }

    @Test
    public void testTriggerCNCNotificationWithNullOrderModel() {

        final String orderId = "00123456";
        final String storeNumber = "7001";
        final String letterType = "13";
        given(targetOrderService.findOrderModelForOrderId((anyString()))).willReturn(null);
        final TargetPointOfServiceData targetPointOfServiceData = new TargetPointOfServiceData();
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(storeNumber))).willReturn(
                targetPointOfServiceData);
        final OrderProcessModel orderProcessModel = new OrderProcessModel();
        orderProcessModel.setState(ProcessState.SUCCEEDED);
        given(
                targetBusinessProcessService.startSendClickAndCollectNotificationProcess(any(OrderModel.class),
                        anyObject(), anyBoolean())).willReturn(orderProcessModel);
        final boolean result = clickAndCollectNotificationFacadeImpl.triggerCNCNotification(orderId, storeNumber,
                letterType);
        assertThat(result).isFalse();

    }

    @Test
    public void testTriggerCNCNotificationWithNullLetterType() {

        final String orderId = "00123456";
        final String storeNumber = "7001";
        final String letterType = "15";
        final TargetPointOfServiceData targetPointOfServiceData = new TargetPointOfServiceData();
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(storeNumber))).willReturn(
                targetPointOfServiceData);
        final OrderProcessModel orderProcessModel = new OrderProcessModel();
        orderProcessModel.setState(ProcessState.SUCCEEDED);
        given(
                targetBusinessProcessService.startSendClickAndCollectNotificationProcess(any(OrderModel.class),
                        anyObject(), anyBoolean())).willReturn(orderProcessModel);
        final boolean result = clickAndCollectNotificationFacadeImpl.triggerCNCNotification(orderId, storeNumber,
                letterType);

        assertThat(result).isFalse();

    }

    @Test
    public void testTriggerCNCNotificationWithNullPointofServicData() {

        final String orderId = "00123456";
        final String storeNumber = "7001";
        final String letterType = "14";
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(storeNumber))).willReturn(
                null);
        final OrderProcessModel orderProcessModel = new OrderProcessModel();
        orderProcessModel.setState(ProcessState.SUCCEEDED);
        given(
                targetBusinessProcessService.startSendClickAndCollectNotificationProcess(any(OrderModel.class),
                        anyObject(), anyBoolean())).willReturn(orderProcessModel);
        final boolean result = clickAndCollectNotificationFacadeImpl.triggerCNCNotification(orderId, storeNumber,
                letterType);

        assertThat(result).isFalse();

    }

    @Test
    public void testTriggerCNCNotificationWithInvalidStoreNumber() {

        final String orderId = "00123456";
        final String storeNumber = "abcd";
        final String letterType = "14";
        final OrderProcessModel orderProcessModel = new OrderProcessModel();
        orderProcessModel.setState(ProcessState.SUCCEEDED);
        given(
                targetBusinessProcessService.startSendClickAndCollectNotificationProcess(any(OrderModel.class),
                        anyObject(), anyBoolean())).willReturn(orderProcessModel);
        final boolean result = clickAndCollectNotificationFacadeImpl.triggerCNCNotification(orderId, storeNumber,
                letterType);

        assertThat(result).isFalse();

    }

    @Test
    public void testTriggerCNCNotificationWithException() {

        final String orderId = "00123456";
        final String storeNumber = "7001";
        final String letterType = "13";
        final TargetPointOfServiceData targetPointOfServiceData = new TargetPointOfServiceData();
        given(targetStoreLocatorFacade.getPointOfService(Integer.valueOf(storeNumber))).willReturn(
                targetPointOfServiceData);
        final OrderProcessModel orderProcessModel = new OrderProcessModel();
        orderProcessModel.setState(ProcessState.ERROR);
        given(
                targetBusinessProcessService.startSendClickAndCollectNotificationProcess(any(OrderModel.class),
                        anyObject(), anyBoolean())).willThrow(new RuntimeException());
        final boolean result = clickAndCollectNotificationFacadeImpl.triggerCNCNotification(orderId, storeNumber,
                letterType);

        assertThat(result).isFalse();

    }

    @Test
    public void testNotifyCncPickedUpWhenOrderNumberIsInvalid() {
        given(targetOrderService.findOrderModelForOrderId(ORDER_NUMBER)).willReturn(null);
        assertThat(
                clickAndCollectNotificationFacadeImpl.notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER).isSuccess())
                .isFalse();
        Mockito.verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testNotifyCncPickedUpWhenNullOrNoCnCConsignmentFoundForOrder() {
        given(targetOrderService.findOrderModelForOrderId(ORDER_NUMBER)).willReturn(orderModel);
        given(targetConsignmentService.getActiveDeliverToStoreConsignmentForOrder(orderModel))
                .willReturn(null);
        assertThat(
                clickAndCollectNotificationFacadeImpl.notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER).isSuccess())
                .isFalse();
        Mockito.verifyZeroInteractions(targetBusinessProcessService);
    }

    @Test
    public void testNotifyCncPickedUpWhenActiveCnCConsignmentFoundForOrderAndCAIsUpdated() {
        given(targetOrderService.findOrderModelForOrderId(ORDER_NUMBER)).willReturn(orderModel);
        final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
        given(targetConsignmentService.getActiveDeliverToStoreConsignmentForOrder(orderModel))
                .willReturn(consignment);
        given(Boolean.valueOf(salesApplicationConfigService.isPartnerChannel(orderModel.getSalesApplication())))
                .willReturn(Boolean.TRUE);
        //given(consignment.getPickedUpAutoDate()).willReturn(null);
        given(Boolean.valueOf(targetConsignmentService.updatePickedupDate(consignment))).willReturn(Boolean.TRUE);

        assertThat(
                clickAndCollectNotificationFacadeImpl.notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER).isSuccess())
                .isTrue();
        verify(targetBusinessProcessService).startSendClickAndCollectPickedupNotificationProcess(orderModel,
                true);
    }

    @Test
    public void testNotifyCncPickedUpWhenActiveCnCConsignmentFoundForOrderAndCAIsNotUpdated() {
        given(targetOrderService.findOrderModelForOrderId(ORDER_NUMBER)).willReturn(orderModel);
        final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
        given(targetConsignmentService.getActiveDeliverToStoreConsignmentForOrder(orderModel))
                .willReturn(consignment);
        given(Boolean.valueOf(salesApplicationConfigService.isPartnerChannel(orderModel.getSalesApplication())))
                .willReturn(Boolean.TRUE);
        //given(consignment.getPickedUpAutoDate()).willReturn(null);
        given(Boolean.valueOf(targetConsignmentService.updatePickedupDate(consignment))).willReturn(Boolean.FALSE);

        assertThat(
                clickAndCollectNotificationFacadeImpl.notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER).isSuccess())
                .isTrue();
        verify(targetBusinessProcessService).startSendClickAndCollectPickedupNotificationProcess(orderModel,
                false);
    }

    @Test
    public void testNotifyCncPickedUpWhenActiveCnCConsignmentFoundForOrderAndCaAlreadyUpdated() {
        given(targetOrderService.findOrderModelForOrderId(ORDER_NUMBER)).willReturn(orderModel);
        final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
        given(targetConsignmentService.getActiveDeliverToStoreConsignmentForOrder(orderModel))
                .willReturn(consignment);
        given(Boolean.valueOf(salesApplicationConfigService.isPartnerChannel(orderModel.getSalesApplication())))
                .willReturn(Boolean.TRUE);
        given(consignment.getPickedUpAutoDate()).willReturn(new Date());
        given(Boolean.valueOf(targetConsignmentService.updatePickedupDate(consignment))).willReturn(Boolean.FALSE);

        assertThat(
                clickAndCollectNotificationFacadeImpl.notifyCncPickedUp(ORDER_NUMBER, STORE_NUMBER).isSuccess())
                .isTrue();
        verify(targetBusinessProcessService).startSendClickAndCollectPickedupNotificationProcess(orderModel,
                false);
    }

    @Test
    public void testNotifyCncReturnedToFloorWhenOrderNumberIsInvalid() {
        given(targetOrderService.findOrderModelForOrderId(ORDER_NUMBER)).willReturn(null);
        assertThat(
                clickAndCollectNotificationFacadeImpl.notifyCncReturnedToFloor(ORDER_NUMBER, STORE_NUMBER).isSuccess())
                .isFalse();
    }

    @Test
    public void testNotifyCncReturnedToFloorWhenNullOrNoCnCConsignmentFoundForOrder() {
        given(targetOrderService.findOrderModelForOrderId(ORDER_NUMBER)).willReturn(orderModel);
        given(targetConsignmentService.getActiveDeliverToStoreConsignmentForOrder(orderModel))
                .willReturn(null);
        assertThat(
                clickAndCollectNotificationFacadeImpl.notifyCncReturnedToFloor(ORDER_NUMBER, STORE_NUMBER).isSuccess())
                .isFalse();
    }

    @Test
    public void testNotifyCncReturnedToFloorWhenActiveCnCConsignmentFoundForOrder() {
        given(targetOrderService.findOrderModelForOrderId(ORDER_NUMBER)).willReturn(orderModel);
        final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
        given(targetConsignmentService.getActiveDeliverToStoreConsignmentForOrder(orderModel))
                .willReturn(consignment);

        assertThat(
                clickAndCollectNotificationFacadeImpl.notifyCncReturnedToFloor(ORDER_NUMBER, STORE_NUMBER).isSuccess())
                .isTrue();
    }
}

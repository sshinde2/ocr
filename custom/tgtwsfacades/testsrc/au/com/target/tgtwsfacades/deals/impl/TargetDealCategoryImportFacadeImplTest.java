package au.com.target.tgtwsfacades.deals.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.deals.TargetDealService;
import au.com.target.tgtcore.model.MissingDealProductInfoModel;
import au.com.target.tgtcore.model.TargetDealCategoryModel;
import au.com.target.tgtwsfacades.deals.dto.DealProductDTO;
import au.com.target.tgtwsfacades.deals.dto.TargetDealCategoryDTO;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * Unit test for {@link TargetDealCategoryImportFacadeImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetDealCategoryImportFacadeImplTest {

    @InjectMocks
    private final TargetDealCategoryImportFacadeImpl dealCategoryImportFacade = new TargetDealCategoryImportFacadeImpl();

    @Mock
    private ModelService modelService;

    @Mock
    private CategoryService categoryService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private UserService userService;

    @Mock
    private ProductService productService;

    @Mock
    private TargetDealCategoryDTO dealCategoryDTO;

    @Mock
    private TargetDealCategoryModel dealCategoryModel;

    @Mock
    private MissingDealProductInfoModel missingDealProductInfo;

    @Mock
    private TargetDealService targetDealService;

    @Before
    public void setUp() {
        BDDMockito.given(modelService.create(MissingDealProductInfoModel.class)).willReturn(missingDealProductInfo);
    }

    @Test
    public void testPersistExistingNonTargetDealCategory() {
        final CategoryModel mockCategory = Mockito.mock(CategoryModel.class);

        BDDMockito.given(categoryService.getCategoryForCode("dummy")).willReturn(mockCategory);
        BDDMockito.given(dealCategoryDTO.getCode()).willReturn("dummy");

        final IntegrationResponseDto responseDto = dealCategoryImportFacade.persistTargetDealCategory(dealCategoryDTO);

        Mockito.verify(categoryService).getCategoryForCode("dummy");

        Assert.assertFalse(responseDto.isSuccessStatus());
        Assert.assertEquals(1, responseDto.getMessages().size());
        Assert.assertEquals("Category dummy is not of type 'TargetDealCategory'", responseDto.getMessages().get(0));
    }

    @Test
    public void testPersistNewTargetDealCategoryWithoutProducts() {
        BDDMockito.doThrow(new UnknownIdentifierException("Deal category not found")).when(categoryService)
                .getCategoryForCode("dealCat");
        BDDMockito.given(modelService.create(TargetDealCategoryModel.class)).willReturn(dealCategoryModel);
        BDDMockito.given(dealCategoryDTO.getCode()).willReturn("dealCat");

        final IntegrationResponseDto responseDto = dealCategoryImportFacade.persistTargetDealCategory(dealCategoryDTO);

        Mockito.verify(categoryService).getCategoryForCode("dealCat");
        Mockito.verify(modelService).create(TargetDealCategoryModel.class);
        Mockito.verify(dealCategoryModel).setCode("dealCat");
        Mockito.verify(modelService).save(dealCategoryModel);

        Mockito.verifyZeroInteractions(productService);

        Assert.assertTrue(responseDto.isSuccessStatus());
        Assert.assertEquals(1, responseDto.getMessages().size());
        Assert.assertEquals("Import of category dealCat is successful", responseDto.getMessages().get(0));
    }

    @Test
    public void testPersistNewTargetDealCategoryWithExistingProducts() {
        BDDMockito.doThrow(new UnknownIdentifierException("Deal category not found")).when(categoryService)
                .getCategoryForCode("dealCat");
        BDDMockito.given(modelService.create(TargetDealCategoryModel.class)).willReturn(dealCategoryModel);
        BDDMockito.given(dealCategoryDTO.getCode()).willReturn("dealCat");

        final DealProductDTO dealProduct1 = Mockito.mock(DealProductDTO.class);
        final DealProductDTO dealProduct2 = Mockito.mock(DealProductDTO.class);
        final ProductModel dealProductModel1 = Mockito.mock(ProductModel.class);
        final ProductModel dealProductModel2 = Mockito.mock(ProductModel.class);
        BDDMockito.given(dealProduct1.getCode()).willReturn("dealProduct1");
        BDDMockito.given(dealProduct2.getCode()).willReturn("dealProduct2");
        final List<DealProductDTO> productDTOs = new ArrayList<>();
        productDTOs.add(dealProduct1);
        productDTOs.add(dealProduct2);
        BDDMockito.given(dealCategoryDTO.getDealProducts()).willReturn(productDTOs);

        BDDMockito.given(productService.getProductForCode("dealProduct1")).willReturn(dealProductModel1);
        BDDMockito.given(productService.getProductForCode("dealProduct2")).willReturn(dealProductModel2);

        final IntegrationResponseDto responseDto = dealCategoryImportFacade.persistTargetDealCategory(dealCategoryDTO);

        Mockito.verify(categoryService).getCategoryForCode("dealCat");
        Mockito.verify(modelService).create(TargetDealCategoryModel.class);
        Mockito.verify(dealCategoryModel).setCode("dealCat");
        Mockito.verify(modelService).save(dealCategoryModel);

        Mockito.verify(productService).getProductForCode("dealProduct1");
        Mockito.verify(productService).getProductForCode("dealProduct2");

        final ArgumentCaptor<Collection> argumentCaptor1 = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<Collection> argumentCaptor2 = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<Collection> argumentCaptor3 = ArgumentCaptor.forClass(Collection.class);

        Mockito.verify(dealProductModel1).setSupercategories(argumentCaptor1.capture());
        Mockito.verify(dealProductModel2).setSupercategories(argumentCaptor2.capture());

        Mockito.verify(modelService).saveAll(argumentCaptor3.capture());

        Mockito.verify(modelService, Mockito.never()).create(MissingDealProductInfoModel.class);

        Assert.assertTrue(argumentCaptor1.getValue().contains(dealCategoryModel));
        Assert.assertTrue(argumentCaptor2.getValue().contains(dealCategoryModel));
        Assert.assertTrue(argumentCaptor3.getValue().contains(dealProductModel1));
        Assert.assertTrue(argumentCaptor3.getValue().contains(dealProductModel2));

        Assert.assertTrue(responseDto.isSuccessStatus());
        Assert.assertEquals(1, responseDto.getMessages().size());
        Assert.assertEquals("Import of category dealCat is successful", responseDto.getMessages().get(0));
    }

    @Test
    public void testPersistExistingTargetDealCategoryWithExistingProducts() {
        BDDMockito.given(categoryService.getCategoryForCode("dealCat")).willReturn(dealCategoryModel);
        BDDMockito.given(dealCategoryDTO.getCode()).willReturn("dealCat");

        final DealProductDTO dealProduct1 = Mockito.mock(DealProductDTO.class);
        final DealProductDTO dealProduct2 = Mockito.mock(DealProductDTO.class);
        final ProductModel dealProductModel1 = Mockito.mock(ProductModel.class);
        final ProductModel dealProductModel2 = Mockito.mock(ProductModel.class);
        BDDMockito.given(dealProduct1.getCode()).willReturn("dealProduct1");
        BDDMockito.given(dealProduct2.getCode()).willReturn("dealProduct2");
        final List<DealProductDTO> productDTOs = new ArrayList<>();
        productDTOs.add(dealProduct1);
        productDTOs.add(dealProduct2);
        BDDMockito.given(dealCategoryDTO.getDealProducts()).willReturn(productDTOs);

        BDDMockito.given(productService.getProductForCode("dealProduct1")).willReturn(dealProductModel1);
        BDDMockito.given(productService.getProductForCode("dealProduct2")).willReturn(dealProductModel2);

        final IntegrationResponseDto responseDto = dealCategoryImportFacade.persistTargetDealCategory(dealCategoryDTO);

        Mockito.verify(categoryService).getCategoryForCode("dealCat");
        Mockito.verifyZeroInteractions(catalogVersionService);
        Mockito.verifyZeroInteractions(userService);
        Mockito.verify(modelService, Mockito.never()).save(dealCategoryModel);

        Mockito.verify(productService).getProductForCode("dealProduct1");
        Mockito.verify(productService).getProductForCode("dealProduct2");

        final ArgumentCaptor<Collection> argumentCaptor1 = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<Collection> argumentCaptor2 = ArgumentCaptor.forClass(Collection.class);
        final ArgumentCaptor<Collection> argumentCaptor3 = ArgumentCaptor.forClass(Collection.class);

        Mockito.verify(dealProductModel1).setSupercategories(argumentCaptor1.capture());
        Mockito.verify(dealProductModel2).setSupercategories(argumentCaptor2.capture());

        Mockito.verify(modelService).saveAll(argumentCaptor3.capture());

        Mockito.verify(modelService, Mockito.never()).create(MissingDealProductInfoModel.class);

        Assert.assertTrue(argumentCaptor1.getValue().contains(dealCategoryModel));
        Assert.assertTrue(argumentCaptor2.getValue().contains(dealCategoryModel));
        Assert.assertTrue(argumentCaptor3.getValue().contains(dealProductModel1));
        Assert.assertTrue(argumentCaptor3.getValue().contains(dealProductModel2));

        Assert.assertTrue(responseDto.isSuccessStatus());
        Assert.assertEquals(1, responseDto.getMessages().size());
        Assert.assertEquals("Import of category dealCat is successful", responseDto.getMessages().get(0));
    }

    @Test
    public void testPersistNewTargetDealCategoryWithInvalidProducts() {
        BDDMockito.doThrow(new UnknownIdentifierException("Deal category not found")).when(categoryService)
                .getCategoryForCode("dealCat");
        BDDMockito.given(modelService.create(TargetDealCategoryModel.class)).willReturn(dealCategoryModel);
        BDDMockito.given(dealCategoryDTO.getCode()).willReturn("dealCat");

        final DealProductDTO dealProduct1 = Mockito.mock(DealProductDTO.class);
        final DealProductDTO dealProduct2 = Mockito.mock(DealProductDTO.class);
        BDDMockito.given(dealProduct1.getCode()).willReturn("dealProduct1");
        BDDMockito.given(dealProduct2.getCode()).willReturn("dealProduct2");
        final List<DealProductDTO> productDTOs = new ArrayList<>();
        productDTOs.add(dealProduct1);
        productDTOs.add(dealProduct2);
        BDDMockito.given(dealCategoryDTO.getDealProducts()).willReturn(productDTOs);

        BDDMockito.willThrow(new UnknownIdentifierException("Product not found"))
                .given(productService).getProductForCode("dealProduct1");
        BDDMockito.willThrow(new UnknownIdentifierException("Product not found"))
                .given(productService).getProductForCode("dealProduct2");

        final IntegrationResponseDto responseDto = dealCategoryImportFacade.persistTargetDealCategory(dealCategoryDTO);

        Mockito.verify(categoryService).getCategoryForCode("dealCat");
        Mockito.verify(modelService).create(TargetDealCategoryModel.class);
        Mockito.verify(dealCategoryModel).setCode("dealCat");
        Mockito.verify(modelService).save(dealCategoryModel);

        Mockito.verify(productService).getProductForCode("dealProduct1");
        Mockito.verify(productService).getProductForCode("dealProduct2");

        Mockito.verify(modelService, Mockito.times(2)).create(MissingDealProductInfoModel.class);
        Mockito.verify(missingDealProductInfo).setProductCode("dealProduct1");
        Mockito.verify(missingDealProductInfo).setProductCode("dealProduct2");

        Assert.assertTrue(responseDto.isSuccessStatus());
        Assert.assertEquals(3, responseDto.getMessages().size());
        Assert.assertEquals("Import of category dealCat is successful", responseDto.getMessages().get(0));
        Assert.assertEquals(
                "However, the following products could not be assigned to this category as they do not exist",
                responseDto.getMessages().get(1));
        Assert.assertEquals("dealProduct1,dealProduct2", responseDto.getMessages().get(2));
    }

    @Test
    public void testPersistExisitingTargetDealCategoryWithInvalidProducts() {
        BDDMockito.given(categoryService.getCategoryForCode("dealCat")).willReturn(dealCategoryModel);
        BDDMockito.given(dealCategoryDTO.getCode()).willReturn("dealCat");

        final DealProductDTO dealProduct1 = Mockito.mock(DealProductDTO.class);
        final DealProductDTO dealProduct2 = Mockito.mock(DealProductDTO.class);
        BDDMockito.given(dealProduct1.getCode()).willReturn("dealProduct1");
        BDDMockito.given(dealProduct2.getCode()).willReturn("dealProduct2");
        final List<DealProductDTO> productDTOs = new ArrayList<>();
        productDTOs.add(dealProduct1);
        productDTOs.add(dealProduct2);
        BDDMockito.given(dealCategoryDTO.getDealProducts()).willReturn(productDTOs);

        BDDMockito.willThrow(new UnknownIdentifierException("Product not found"))
                .given(productService).getProductForCode("dealProduct1");
        BDDMockito.willThrow(new UnknownIdentifierException("Product not found"))
                .given(productService).getProductForCode("dealProduct2");

        final IntegrationResponseDto responseDto = dealCategoryImportFacade.persistTargetDealCategory(dealCategoryDTO);

        Mockito.verify(categoryService).getCategoryForCode("dealCat");
        Mockito.verifyZeroInteractions(catalogVersionService);
        Mockito.verifyZeroInteractions(userService);
        Mockito.verify(modelService, Mockito.never()).save(dealCategoryModel);

        Mockito.verify(productService).getProductForCode("dealProduct1");
        Mockito.verify(productService).getProductForCode("dealProduct2");

        Assert.assertTrue(responseDto.isSuccessStatus());
        Assert.assertEquals(3, responseDto.getMessages().size());
        Assert.assertEquals("Import of category dealCat is successful", responseDto.getMessages().get(0));
        Assert.assertEquals(
                "However, the following products could not be assigned to this category as they do not exist",
                responseDto.getMessages().get(1));
        Assert.assertEquals("dealProduct1,dealProduct2", responseDto.getMessages().get(2));
    }
}

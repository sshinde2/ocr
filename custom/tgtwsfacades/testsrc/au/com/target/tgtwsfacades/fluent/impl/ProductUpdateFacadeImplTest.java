/**
 * 
 */
package au.com.target.tgtwsfacades.fluent.impl;

import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.product.TargetProductService;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductUpdateFacadeImplTest {

    @Mock
    private TargetProductService targetProductService;

    @InjectMocks
    private final ProductUpdateFacadeImpl productUpdateFacadeImpl = new ProductUpdateFacadeImpl();

    @Test
    public void testSetOnlineDateForApplicableProducts() {
        productUpdateFacadeImpl.setOnlineDateForApplicableProducts();
        verify(targetProductService).setOnlineDateForApplicableProducts();
    }
}

/**
 * 
 */
package au.com.target.tgtwsfacades.helper;

import de.hybris.bootstrap.annotations.UnitTest;

import junit.framework.Assert;

import org.fest.assertions.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.service.TargetConsignmentService;


/**
 * @author siddharam
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderConsignmentHelperTest {

    @InjectMocks
    private final OrderConsignmentHelper orderConsignmentHelper = new OrderConsignmentHelper();
    @Mock
    private TargetConsignmentService targetConsignmentService;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Test
    public void testGetConsignmentBasedOrderOrConsignmentCodeForConsignmentCode() throws NotFoundException {
        final String code = "consCode";
        final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
        Mockito.when(targetConsignmentService.getConsignmentForCode(code)).thenReturn(consignment);
        Assertions.assertThat(orderConsignmentHelper.getConsignmentBasedOrderOrConsignmentCode(code)
                .equals(consignment));
    }

    @Test
    public void testGetConsignmentBasedOrderOrConsignmentCodeForOrderId() throws NotFoundException {
        final String code = "orderCode";
        final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
        Mockito.when(targetConsignmentService.getConsignmentForCode(code)).thenThrow(new NotFoundException("NotFound"));
        Mockito.when(targetConsignmentService.findConsignmentForDefaultWarehouse(code)).thenReturn(consignment);
        Assertions.assertThat(orderConsignmentHelper.getConsignmentBasedOrderOrConsignmentCode(code)
                .equals(consignment));
    }

    @Test
    public void testGetConsignmentBasedOrderOrConsignmentCodeException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException, NotFoundException {
        final String code = "consCode";
        Mockito.when(targetConsignmentService.getConsignmentForCode(code)).thenThrow(
                new NotFoundException("No Records"));
        Mockito.when(targetConsignmentService.findConsignmentForDefaultWarehouse(code)).thenReturn(null);
        Assert.assertEquals(null, orderConsignmentHelper.getConsignmentBasedOrderOrConsignmentCode(code));
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwsfacades.instore.dto.auspost.ChargeZone;
import au.com.target.tgtwsfacades.instore.dto.auspost.ConsignmentSummary;
import au.com.target.tgtwsfacades.instore.dto.auspost.ManifestHardCopy;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class HardCopyManifestConverterTest {

    @Mock
    private TargetConsignmentModel consignmentModel;

    @Mock
    private StoreFulfilmentCapabilitiesModel storeCapability;

    @Mock
    private AddressModel deliveryAddress;

    @Mock
    private TargetPointOfServiceModel tpos;

    @Mock
    private TargetManifestModel manifestModel;

    @Mock
    private HardCopyManifestConsignmentConverter consignmentConverter;

    @InjectMocks
    private final HardCopyManifestConverter converter = new HardCopyManifestConverter();

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    //CHECKSTYLE:ON

    @Before
    public void setup() {
        Mockito.when(tpos.getStoreNumber()).thenReturn(Integer.valueOf(5126));
    }

    @Test
    public void testTposNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("tpos cannot be null");
        converter.convert(null, manifestModel);
    }

    @Test
    public void testManifestNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Manifest Model cannot be null");
        converter.convert(tpos, null);
    }

    @Test
    public void testWhenNoStoreCapabilitiesSet() {
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(null);
        Assert.assertNull(converter.convert(tpos, manifestModel));
    }

    @Test
    public void testAusPostDetailsSet() {
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(storeCapability);
        setStoreAddress();
        setAuspostParameters();
        Mockito.when(manifestModel.getConsignments()).thenReturn(Collections.singleton(consignmentModel));
        setConsignmentSummaries(1);
        Mockito.when(storeCapability.getChargeCode()).thenReturn("chargeCode-007");
        final ManifestHardCopy manifestHardCopy = converter.convert(tpos, manifestModel);
        Assert.assertEquals(manifestHardCopy.getMerchantId(), "2222222");
        Assert.assertEquals(manifestHardCopy.getManifestCreatedDate(),
                TargetDateUtil.getDateFormattedAsString(new Date(999999)));
        Assert.assertEquals(manifestHardCopy.getManifestNumber(), "007");
        Assert.assertEquals(manifestHardCopy.getAusPostAccountNumber(), "3333333");
        Assert.assertEquals(manifestHardCopy.getLodgementFacility(), "Shreeram-007");
        Assert.assertEquals(manifestHardCopy.getMerchantLocationId(), "1111111");
    }

    @Test
    public void testStoreAddress() {
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(storeCapability);
        setStoreAddress();
        setAuspostParameters();
        Mockito.when(manifestModel.getConsignments()).thenReturn(Collections.singleton(consignmentModel));
        setConsignmentSummaries(1);
        Mockito.when(storeCapability.getChargeCode()).thenReturn("chargeCode-007");
        final ManifestHardCopy manifestHardCopy = converter.convert(tpos, manifestModel);
        Assert.assertEquals(manifestHardCopy.getStoreAddress().getAddressLine1(), "Station Road");
        Assert.assertEquals(manifestHardCopy.getStoreAddress().getAddressLine2(), "Lonsdale Street");
        Assert.assertEquals(manifestHardCopy.getStoreAddress().getSuburb(), "Melbourne");
        Assert.assertEquals(manifestHardCopy.getStoreAddress().getState(), "VIC");
        Assert.assertEquals(manifestHardCopy.getStoreAddress().getPostcode(), "3000");

    }

    @Test
    public void testCalulateTotalWeightAndParcels() {
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(storeCapability);
        setStoreAddress();
        setAuspostParameters();
        Mockito.when(storeCapability.getChargeCode()).thenReturn("chargeCode-007");
        createConsignmentsWithSummaries(1, 10, 4);
        ManifestHardCopy manifestHardCopy = converter.convert(tpos, manifestModel);
        Assert.assertEquals(manifestHardCopy.getTotalWeight(), Double.valueOf(47.994));
        Assert.assertEquals(manifestHardCopy.getTotalArticles(), Integer.valueOf(15));
        createConsignmentsWithSummaries(1, 1);
        manifestHardCopy = converter.convert(tpos, manifestModel);
        Assert.assertEquals(manifestHardCopy.getTotalWeight(), Double.valueOf(23.996));
        Assert.assertEquals(manifestHardCopy.getTotalArticles(), Integer.valueOf(2));
    }

    @Test
    public void testSortingByChargeZoneAndConsignmentId() {
        Mockito.when(tpos.getFulfilmentCapability()).thenReturn(storeCapability);
        setStoreAddress();
        setAuspostParameters();
        Mockito.when(storeCapability.getChargeCode()).thenReturn("chargeCode-007");
        createConsignmentsWithSummaries1(6, 2, 4);
        final ManifestHardCopy manifestHardCopy = converter.convert(tpos, manifestModel);
        final List<ConsignmentSummary> consignmentSummaries = manifestHardCopy.getConsignmentSummaries();
        Assert.assertEquals(consignmentSummaries.get(0).getChargeZone().getCode(), "1CZ");
        Assert.assertEquals(consignmentSummaries.get(0).getConsignmentId(), "200001");
        Assert.assertEquals(consignmentSummaries.get(1).getChargeZone().getCode(), "1CZ");
        Assert.assertEquals(consignmentSummaries.get(1).getConsignmentId(), "400001");
        Assert.assertEquals(consignmentSummaries.get(2).getChargeZone().getCode(), "1CZ");
        Assert.assertEquals(consignmentSummaries.get(2).getConsignmentId(), "600001");
        Assert.assertEquals(consignmentSummaries.get(3).getChargeZone().getCode(), "2CZ");
        Assert.assertEquals(consignmentSummaries.get(3).getConsignmentId(), "200001");
        Assert.assertEquals(consignmentSummaries.get(4).getChargeZone().getCode(), "2CZ");
        Assert.assertEquals(consignmentSummaries.get(4).getConsignmentId(), "400001");
        Assert.assertEquals(consignmentSummaries.get(5).getChargeZone().getCode(), "2CZ");
        Assert.assertEquals(consignmentSummaries.get(5).getConsignmentId(), "600001");
    }

    private void createConsignmentsWithSummaries1(final int... numbers) {
        final Set<TargetConsignmentModel> consignments = new HashSet<>();
        int prefix = 0;
        while (++prefix < 3) {
            final ChargeZone chargeZone = Mockito.mock(ChargeZone.class);
            for (final int number : numbers) {
                final ConsignmentSummary consignmentSummary = Mockito.mock(ConsignmentSummary.class);
                Mockito.when(chargeZone.getCode()).thenReturn(Integer.valueOf(prefix).toString() + "CZ");
                Mockito.when(consignmentSummary.getChargeZone()).thenReturn(chargeZone);
                Mockito.when(consignmentSummary.getConsignmentId()).thenReturn(
                        Integer.valueOf(number).toString() + "00001");
                final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
                consignments.add(consignment);
                Mockito.when(consignmentConverter.convert(consignment)).thenReturn(consignmentSummary);
            }
        }
        Mockito.when(manifestModel.getConsignments()).thenReturn(consignments);
    }

    private void createConsignmentsWithSummaries(final int... numbers) {
        final Set<TargetConsignmentModel> consignments = new HashSet<>();

        final ChargeZone chargeZone = Mockito.mock(ChargeZone.class);
        for (final int number : numbers) {
            final ConsignmentSummary consignmentSummary = Mockito.mock(ConsignmentSummary.class);
            Mockito.when(chargeZone.getCode()).thenReturn("CZ");
            Mockito.when(consignmentSummary.getChargeZone()).thenReturn(chargeZone);
            Mockito.when(consignmentSummary.getConsignmentId()).thenReturn("10001101");
            Mockito.when(consignmentSummary.getTotalWeight()).thenReturn(Double.valueOf(number + 10.9979999));
            Mockito.when(consignmentSummary.getTotalArticles()).thenReturn(Integer.valueOf(number));
            final TargetConsignmentModel consignment = Mockito.mock(TargetConsignmentModel.class);
            consignments.add(consignment);
            Mockito.when(consignmentConverter.convert(consignment)).thenReturn(consignmentSummary);
        }
        Mockito.when(manifestModel.getConsignments()).thenReturn(consignments);
    }

    private void setConsignmentSummaries(final int... numberOfConsignments) {
        for (final int consignmentNumber : numberOfConsignments) {
            final ConsignmentSummary consignmentSummary = Mockito.mock(ConsignmentSummary.class);
            Mockito.when(consignmentSummary.getTotalWeight()).thenReturn(Double.valueOf(consignmentNumber + 10));
            Mockito.when(consignmentSummary.getTotalArticles()).thenReturn(Integer.valueOf(consignmentNumber));
            Mockito.when(consignmentConverter.convert(consignmentModel)).thenReturn(consignmentSummary);
        }
    }

    private void setAuspostParameters() {
        Mockito.when(storeCapability.getMerchantLocationId()).thenReturn("1111111");
        setStoreAddress();
        Mockito.when(manifestModel.getDate()).thenReturn(new Date(999999));
        Mockito.when(storeCapability.getMerchantId()).thenReturn("2222222");
        Mockito.when(manifestModel.getCode()).thenReturn("007");
        Mockito.when(storeCapability.getChargeAccountNumber()).thenReturn(Integer.valueOf(3333333));
        Mockito.when(storeCapability.getLodgementFacility()).thenReturn("Shreeram-007");

    }

    private void setStoreAddress() {
        final AddressModel storeAddress = Mockito.mock(AddressModel.class);
        Mockito.when(storeAddress.getLine1()).thenReturn("Station Road");
        Mockito.when(storeAddress.getLine2()).thenReturn("Lonsdale Street");
        Mockito.when(storeAddress.getTown()).thenReturn("Melbourne");
        Mockito.when(storeAddress.getDistrict()).thenReturn("VIC");
        Mockito.when(storeAddress.getPostalcode()).thenReturn("3000");

        Mockito.when(tpos.getAddress()).thenReturn(storeAddress);
    }
}

/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.manifests.Manifest;


/**
 * Test class for InstoreManifestConvert
 * 
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class InstoreManifestConverterTest {

    private static final String MANIFEST_CODE = "1234567";

    @Mock
    private TargetManifestModel manifestModel;

    @Mock
    private InstoreConsignmentConverter instoreConsignmentConvert;

    @InjectMocks
    private final InstoreManifestConverter instoreManifestConverter = new InstoreManifestConverter();

    @Before
    public void setUp() {
        Mockito.when(manifestModel.getCode()).thenReturn(MANIFEST_CODE);
        Mockito.when(manifestModel.getParcels()).thenReturn(Integer.valueOf(6));
    }

    @Test
    public void testConvert() {
        final Date date = Calendar.getInstance().getTime();
        final Set<TargetConsignmentModel> consignmentModels = new HashSet<>();
        final TargetConsignmentModel consignmentModel1 = new TargetConsignmentModel();
        final TargetConsignmentModel consignmentModel2 = new TargetConsignmentModel();
        consignmentModels.add(consignmentModel1);
        consignmentModels.add(consignmentModel2);
        final Consignment consignment1 = new Consignment();
        final Consignment consignment2 = new Consignment();

        Mockito.when(manifestModel.getDate()).thenReturn(date);
        Mockito.when(manifestModel.getConsignments()).thenReturn(consignmentModels);
        Mockito.when(instoreConsignmentConvert.convert(consignmentModel1)).thenReturn(consignment1);
        Mockito.when(instoreConsignmentConvert.convert(consignmentModel2)).thenReturn(consignment2);

        final Manifest manifest = instoreManifestConverter.convert(manifestModel);
        Assert.assertEquals(MANIFEST_CODE, manifest.getCode());
        Assert.assertEquals(6, manifest.getParcels().intValue());
        Assert.assertEquals(2, manifest.getConsignments().size());
        Assert.assertEquals(false, manifest.isSent());
        Assert.assertEquals(TargetDateUtil.getDateFormattedAsString(date), manifest.getDate());
    }

    @Test
    public void testConvertWithoutConsignments() {
        final Manifest manifest = instoreManifestConverter.convert(manifestModel);
        Assert.assertNull(manifest.getConsignments());
    }
}

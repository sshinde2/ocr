/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;

import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgtwsfacades.instore.dto.consignments.ProductStockLevelInstore;


/**
 * @author Vivek
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductStockLevelConverterTest {

    private static final String LOW_STOCK = "LOW_STOCK";
    private static final String MEDIUM_STOCK = "MEDIUM_STOCK";
    private static final String HIGH_STOCK = "HIGH_STOCK";
    private static final String NO_STOCK = "NO_STOCK";

    @InjectMocks
    private final ProductStockLevelConverter converter = new ProductStockLevelConverter();

    private final String storeNumber = "5032";

    @Before
    public void setUp() {
        converter.setHighStock(25);
        converter.setMediumStock(10);
    }

    @Test
    public void testConvertHigh() {

        final String productCode = "11000011";
        final String stockLevel = "25";
        final StockUpdateResponseDto stockDto = createStockUpdateResonseDto(productCode, stockLevel);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        Assert.notEmpty(instoreStockDto);
        Assert.isTrue(instoreStockDto.size() == 1);
        final ProductStockLevelInstore stockLevelResponse = instoreStockDto.get(0);
        Assert.isTrue(stockLevelResponse.getProductCode().equals(productCode));
        Assert.isTrue(stockLevelResponse.getStockOnHand().equals(stockLevel));
        Assert.isTrue(stockLevelResponse.getStockLevel().equals(HIGH_STOCK));
    }

    @Test
    public void testConvertMedium() {

        final String productCode = "11000011";
        final String stockLevel = "10";
        final StockUpdateResponseDto stockDto = createStockUpdateResonseDto(productCode, stockLevel);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        Assert.notEmpty(instoreStockDto);
        Assert.isTrue(instoreStockDto.size() == 1);
        final ProductStockLevelInstore stockLevelResponse = instoreStockDto.get(0);
        Assert.isTrue(stockLevelResponse.getProductCode().equals(productCode));
        Assert.isTrue(stockLevelResponse.getStockOnHand().equals(stockLevel));
        Assert.isTrue(stockLevelResponse.getStockLevel().equals(MEDIUM_STOCK));
    }

    @Test
    public void testConvertLow() {

        final String productCode = "11000011";
        final String stockLevel = "5";
        final StockUpdateResponseDto stockDto = createStockUpdateResonseDto(productCode, stockLevel);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        Assert.notEmpty(instoreStockDto);
        Assert.isTrue(instoreStockDto.size() == 1);
        final ProductStockLevelInstore stockLevelResponse = instoreStockDto.get(0);
        Assert.isTrue(stockLevelResponse.getProductCode().equals(productCode));
        Assert.isTrue(stockLevelResponse.getStockOnHand().equals(stockLevel));
        Assert.isTrue(stockLevelResponse.getStockLevel().equals(LOW_STOCK));
    }

    @Test
    public void testConvertNoStock() {

        final String productCode = "11000011";
        final String stockLevel = "0";
        final StockUpdateResponseDto stockDto = createStockUpdateResonseDto(productCode, stockLevel);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        Assert.notEmpty(instoreStockDto);
        Assert.isTrue(instoreStockDto.size() == 1);
        final ProductStockLevelInstore stockLevelResponse = instoreStockDto.get(0);
        Assert.isTrue(stockLevelResponse.getProductCode().equals(productCode));
        Assert.isTrue(stockLevelResponse.getStockOnHand().equals(stockLevel));
        Assert.isTrue(stockLevelResponse.getStockLevel().equals(NO_STOCK));
    }

    @Test
    public void testConvertMultipleStock() {

        final StockUpdateResponseDto stockDto = new StockUpdateResponseDto();
        final List<StockUpdateStoreResponseDto> stockUpdateDtos = new ArrayList<>();
        final StockUpdateStoreResponseDto stockUpdateDto = new StockUpdateStoreResponseDto();
        stockUpdateDto.setStoreNumber(storeNumber);
        final List<StockUpdateProductResponseDto> productDtos = new ArrayList<>();
        final StockUpdateProductResponseDto productDto1 = new StockUpdateProductResponseDto();
        final StockUpdateProductResponseDto productDto2 = new StockUpdateProductResponseDto();
        productDto1.setItemcode("11000011");
        productDto1.setSoh("15");
        productDto2.setItemcode("11000012");
        productDto2.setSoh("25");
        productDtos.add(productDto1);
        productDtos.add(productDto2);
        stockUpdateDto.setStockUpdateProductResponseDtos(productDtos);
        stockUpdateDtos.add(stockUpdateDto);
        stockDto.setStockUpdateStoreResponseDtos(stockUpdateDtos);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        Assert.notEmpty(instoreStockDto);
        Assert.isTrue(instoreStockDto.size() == 2);

        int productsMatched = 0;
        for (final ProductStockLevelInstore dto : instoreStockDto) {
            if (dto.getProductCode().equals("11000011")) {
                Assert.isTrue(dto.getStockOnHand().equals("15"));
                Assert.isTrue(dto.getStockLevel().equals(MEDIUM_STOCK));
                productsMatched++;
            }
            else {
                Assert.isTrue(dto.getProductCode().equals("11000012"));
                Assert.isTrue(dto.getStockOnHand().equals("25"));
                Assert.isTrue(dto.getStockLevel().equals(HIGH_STOCK));
                productsMatched++;
            }
        }
        Assert.isTrue(productsMatched == 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConvertNullStockUpdateResponse() {

        final StockUpdateResponseDto stockDto = null;

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        Assert.isNull(instoreStockDto);
    }

    private StockUpdateResponseDto createStockUpdateResonseDto(final String itemCode, final String stockLevel) {
        final StockUpdateResponseDto stockDto = new StockUpdateResponseDto();
        final List<StockUpdateStoreResponseDto> stockUpdateDtos = new ArrayList<>();
        final StockUpdateStoreResponseDto stockUpdateDto = new StockUpdateStoreResponseDto();
        stockUpdateDto.setStoreNumber(storeNumber);
        final List<StockUpdateProductResponseDto> productDtos = new ArrayList<>();
        final StockUpdateProductResponseDto productDto1 = new StockUpdateProductResponseDto();
        productDto1.setItemcode(itemCode);
        productDto1.setSoh(stockLevel);
        productDtos.add(productDto1);
        stockUpdateDto.setStockUpdateProductResponseDtos(productDtos);
        stockUpdateDtos.add(stockUpdateDto);
        stockDto.setStockUpdateStoreResponseDtos(stockUpdateDtos);
        return stockDto;
    }
}
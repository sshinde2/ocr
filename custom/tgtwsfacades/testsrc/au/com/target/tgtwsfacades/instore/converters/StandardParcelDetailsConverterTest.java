/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import de.hybris.bootstrap.annotations.UnitTest;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;
import au.com.target.tgtwsfacades.instore.dto.StandardParcel;


/**
 * @author smishra1
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StandardParcelDetailsConverterTest {

    @Mock
    private TargetStandardParcelModel standardParcelModel;

    //CHECKSTYLE:OFF
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    //CHECKSTYLE:ON

    @InjectMocks
    private final StandardParcelDetailsConverter converter = new StandardParcelDetailsConverter();

    @Test
    public void testBasicConversion() {
        Mockito.when(standardParcelModel.getDescription()).thenReturn("Test Box 1");
        Mockito.when(standardParcelModel.getHeight()).thenReturn(Double.valueOf(200));
        Mockito.when(standardParcelModel.getWidth()).thenReturn(Double.valueOf(250));
        Mockito.when(standardParcelModel.getLength()).thenReturn(Double.valueOf(300));

        final StandardParcel standardParcel = converter.convert(standardParcelModel);

        Assert.assertEquals(standardParcel.getDescription(), standardParcelModel.getDescription());
        Assert.assertEquals(standardParcel.getHeight(), standardParcelModel.getHeight());
        Assert.assertEquals(standardParcel.getWidth(), standardParcelModel.getWidth());
        Assert.assertEquals(standardParcel.getLength(), standardParcelModel.getLength());
    }

    @Test
    public void testDescriptionNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Description cannot be null");
        Mockito.when(standardParcelModel.getDescription()).thenReturn(null);
        converter.convert(standardParcelModel);
    }

    @Test
    public void testHeightNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Height cannot be null");
        Mockito.when(standardParcelModel.getDescription()).thenReturn("Test Box 1");
        Mockito.when(standardParcelModel.getHeight()).thenReturn(null);
        converter.convert(standardParcelModel);
    }

    @Test
    public void testWidthNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Width cannot be null");
        Mockito.when(standardParcelModel.getDescription()).thenReturn("Test Box 1");
        Mockito.when(standardParcelModel.getHeight()).thenReturn(Double.valueOf(200));
        Mockito.when(standardParcelModel.getWidth()).thenReturn(null);
        converter.convert(standardParcelModel);
    }

    @Test
    public void testLengthNull() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Length cannot be null");
        Mockito.when(standardParcelModel.getDescription()).thenReturn("Test Box 1");
        Mockito.when(standardParcelModel.getHeight()).thenReturn(Double.valueOf(200));
        Mockito.when(standardParcelModel.getWidth()).thenReturn(Double.valueOf(250));
        Mockito.when(standardParcelModel.getLength()).thenReturn(null);
        converter.convert(standardParcelModel);
    }
}

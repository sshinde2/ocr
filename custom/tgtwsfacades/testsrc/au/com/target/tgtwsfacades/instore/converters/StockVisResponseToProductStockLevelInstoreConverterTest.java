/**
 * 
 */
package au.com.target.tgtwsfacades.instore.converters;

import static org.fest.assertions.Assertions.assertThat;
import static org.fest.assertions.Fail.fail;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemResponseDto;
import au.com.target.tgtwsfacades.instore.dto.consignments.ProductStockLevelInstore;


/**
 * 
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class StockVisResponseToProductStockLevelInstoreConverterTest {

    private static final String LOW_STOCK = "LOW_STOCK";
    private static final String MEDIUM_STOCK = "MEDIUM_STOCK";
    private static final String HIGH_STOCK = "HIGH_STOCK";
    private static final String NO_STOCK = "NO_STOCK";

    @InjectMocks
    private final StockVisResponseToProductStockLevelInstoreConverter converter = new StockVisResponseToProductStockLevelInstoreConverter();

    private final String storeNumber = "5032";

    @Before
    public void setUp() {
        converter.setHighStock(25);
        converter.setMediumStock(10);
    }

    @Test
    public void testConvertHigh() {

        final String productCode = "11000011";
        final String stockLevel = "25";
        final StockVisibilityItemLookupResponseDto stockDto = createSingleItemStockUpdateResonseDto(productCode,
                stockLevel);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        assertThat(instoreStockDto).isNotEmpty();
        assertThat(instoreStockDto).hasSize(1);
        final ProductStockLevelInstore stockLevelResponse = instoreStockDto.get(0);
        assertThat(stockLevelResponse.getProductCode()).isEqualTo(productCode);
        assertThat(stockLevelResponse.getStockOnHand()).isEqualTo(stockLevel);
        assertThat(stockLevelResponse.getStockLevel()).isEqualTo(HIGH_STOCK);
    }

    @Test
    public void testConvertMedium() {

        final String productCode = "11000011";
        final String stockLevel = "10";
        final StockVisibilityItemLookupResponseDto stockDto = createSingleItemStockUpdateResonseDto(productCode,
                stockLevel);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        assertThat(instoreStockDto).isNotEmpty();
        assertThat(instoreStockDto).hasSize(1);
        final ProductStockLevelInstore stockLevelResponse = instoreStockDto.get(0);
        assertThat(stockLevelResponse.getProductCode()).isEqualTo(productCode);
        assertThat(stockLevelResponse.getStockOnHand()).isEqualTo(stockLevel);
        assertThat(stockLevelResponse.getStockLevel()).isEqualTo(MEDIUM_STOCK);
    }

    @Test
    public void testConvertLow() {

        final String productCode = "11000011";
        final String stockLevel = "5";
        final StockVisibilityItemLookupResponseDto stockDto = createSingleItemStockUpdateResonseDto(productCode,
                stockLevel);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        assertThat(instoreStockDto).isNotEmpty();
        assertThat(instoreStockDto).hasSize(1);
        final ProductStockLevelInstore stockLevelResponse = instoreStockDto.get(0);
        assertThat(stockLevelResponse.getProductCode()).isEqualTo(productCode);
        assertThat(stockLevelResponse.getStockOnHand()).isEqualTo(stockLevel);
        assertThat(stockLevelResponse.getStockLevel()).isEqualTo(LOW_STOCK);
    }

    @Test
    public void testConvertNoStock() {

        final String productCode = "11000011";
        final String stockLevel = "0";
        final StockVisibilityItemLookupResponseDto stockDto = createSingleItemStockUpdateResonseDto(productCode,
                stockLevel);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(stockDto, storeNumber);

        assertThat(instoreStockDto).isNotEmpty();
        assertThat(instoreStockDto).hasSize(1);
        final ProductStockLevelInstore stockLevelResponse = instoreStockDto.get(0);
        assertThat(stockLevelResponse.getProductCode()).isEqualTo(productCode);
        assertThat(stockLevelResponse.getStockOnHand()).isEqualTo(stockLevel);
        assertThat(stockLevelResponse.getStockLevel()).isEqualTo(NO_STOCK);
    }

    @Test
    public void testConvertMultipleStock() {
        final StockVisibilityItemLookupResponseDto responseDto = new StockVisibilityItemLookupResponseDto();
        final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDto = new ArrayList<>();
        stockVisibilityItemResponseDto.add(createStockUpdateResonseItemDto("1234", "25", "5032"));
        stockVisibilityItemResponseDto.add(createStockUpdateResonseItemDto("1234", "10", "5001"));
        stockVisibilityItemResponseDto.add(createStockUpdateResonseItemDto("2235", "15", "5032"));
        stockVisibilityItemResponseDto.add(createStockUpdateResonseItemDto("2236", "25", "5032"));
        responseDto.setItems(stockVisibilityItemResponseDto);

        final List<ProductStockLevelInstore> instoreStockDto = converter.convert(responseDto, "5032");


        assertThat(instoreStockDto).isNotEmpty();
        assertThat(instoreStockDto).hasSize(3);

        for (final ProductStockLevelInstore dto : instoreStockDto) {
            switch (dto.getProductCode()) {
                case "1234":
                    assertThat(dto.getStockOnHand()).isEqualTo("25");
                    assertThat(dto.getStockLevel()).isEqualTo(HIGH_STOCK);
                    break;
                case "2235":
                    assertThat(dto.getStockOnHand()).isEqualTo("15");
                    assertThat(dto.getStockLevel()).isEqualTo(MEDIUM_STOCK);
                    break;
                case "2236":
                    assertThat(dto.getStockOnHand()).isEqualTo("25");
                    assertThat(dto.getStockLevel()).isEqualTo(HIGH_STOCK);
                    break;
                default:
                    fail("Unexpected productcode=" + dto.getProductCode());
            }
        }
    }


    private StockVisibilityItemLookupResponseDto createSingleItemStockUpdateResonseDto(final String itemCode,
            final String stockLevel) {
        final StockVisibilityItemLookupResponseDto responseDto = new StockVisibilityItemLookupResponseDto();
        final List<StockVisibilityItemResponseDto> stockVisibilityItemResponseDto = new ArrayList<>();
        final StockVisibilityItemResponseDto itemResponseDto = createStockUpdateResonseItemDto(itemCode,
                stockLevel, storeNumber);
        stockVisibilityItemResponseDto.add(itemResponseDto);
        responseDto.setItems(stockVisibilityItemResponseDto);
        return responseDto;
    }

    private StockVisibilityItemResponseDto createStockUpdateResonseItemDto(final String itemCode,
            final String stockLevel, final String storeNumberForResponse) {
        final StockVisibilityItemResponseDto itemResponseDto = new StockVisibilityItemResponseDto();
        itemResponseDto.setStoreNumber(storeNumberForResponse);
        itemResponseDto.setCode(itemCode);
        itemResponseDto.setSoh(stockLevel);
        return itemResponseDto;
    }
}
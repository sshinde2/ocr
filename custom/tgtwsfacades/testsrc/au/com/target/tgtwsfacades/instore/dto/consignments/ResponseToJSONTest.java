/**
 * 
 */
package au.com.target.tgtwsfacades.instore.dto.consignments;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.fest.util.Collections;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.response.data.Response;


/**
 * @author smudumba
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ResponseToJSONTest {
    private static String property = "java.io.tmpdir";
    private static String tempDir = System.getProperty(property);


    @SuppressWarnings("deprecation")
    @Test
    public void testCompleteResponseToJSON() throws JsonGenerationException, JsonMappingException, IOException

    {

        final Response response = new Response(true);

        // first consignment
        Product product = new Product();
        product.setCode("10");
        product.setColour("black");
        product.setLargeImageUrl("www.test.com");
        product.setThumbImageUrl("www.test.com");
        product.setDepartment("kids");
        product.setPrice(Integer.valueOf("10000"));
        product.setSize("L");
        Customer customer = new Customer();
        customer.setFirstName("Test firs Name");
        customer.setLastName("Test Last Name");
        Consignment consignment = new Consignment();
        consignment.setCode("400");
        consignment.setCustomer(customer);
        consignment.setOrderNumber("1000");
        consignment.setStatus("Completed");
        consignment.setTotalItems(Long.valueOf(20));
        ConsignmentEntry ce = new ConsignmentEntry();
        ce.setProduct(product);
        ce.setPickedQty("123");
        ce.setQuantity("1000");
        List<ConsignmentEntry> entries = new ArrayList<>();
        entries.add(ce);
        consignment.setEntries(entries);
        final List<Consignment> consignments = new ArrayList<>();
        consignments.add(consignment);

        //second consignment

        product = new Product();
        product.setCode("20");
        product.setColour("blue");
        product.setLargeImageUrl("www.test.com");
        product.setThumbImageUrl("www.test.com");
        product.setDepartment("Women");
        product.setPrice(Integer.valueOf("20000"));
        product.setSize("S");
        customer = new Customer();
        customer.setFirstName("Test firs Name2");
        customer.setLastName("Test Last Name2");
        consignment = new Consignment();
        consignment.setCode("500");
        consignment.setCustomer(customer);
        consignment.setOrderNumber("2000");
        consignment.setStatus("Completed");
        consignment.setTotalItems(Long.valueOf(10));
        ce = new ConsignmentEntry();
        ce.setProduct(product);
        ce.setQuantity("2000");
        entries = new ArrayList<>();
        entries.add(ce);
        consignment.setEntries(entries);
        consignments.add(consignment);
        final ConsignmentsResponseData crd = new ConsignmentsResponseData();
        crd.setConsignments(consignments);

        //Add pagination details
        final Pagination pagination = new Pagination();
        pagination.setOffset(Integer.valueOf(1));
        pagination.setPerPage(Integer.valueOf(20));
        pagination.setTotalAvailable(Integer.valueOf(100));

        crd.setPagination(pagination);
        response.setData(crd);


        final ObjectMapper mapper = new ObjectMapper();
        //temp dir for windows is c:\windows\temp

        mapper.defaultPrettyPrintingWriter().writeValue(new File(tempDir + "/responsewithentries.json"), response);

    }

    @SuppressWarnings("deprecation")
    @Test
    public void testConsignmentsWithOutEntriesToJSON() throws JsonGenerationException, JsonMappingException,
            IOException

    {

        final Response response = new Response(true);

        // first consignment      
        Customer customer = new Customer();
        customer.setFirstName("Test firs Name");
        customer.setLastName("Test Last Name");
        Consignment consignment = new Consignment();
        consignment.setCode("400");
        consignment.setCustomer(customer);
        consignment.setOrderNumber("1000");
        consignment.setStatus("Completed");
        consignment.setTotalItems(Long.valueOf(20));


        final List<Consignment> consignments = new ArrayList<>();
        consignments.add(consignment);

        //second consignment 
        customer = new Customer();
        customer.setFirstName("Test firs Name2");
        customer.setLastName("Test Last Name2");
        consignment = new Consignment();
        consignment.setCode("500");
        consignment.setCustomer(customer);
        consignment.setOrderNumber("2000");
        consignment.setStatus("Completed");
        consignment.setTotalItems(Long.valueOf(10));
        consignments.add(consignment);
        final ConsignmentsResponseData crd = new ConsignmentsResponseData();
        crd.setConsignments(consignments);
        response.setData(crd);
        final ObjectMapper mapper = new ObjectMapper();
        mapper.defaultPrettyPrintingWriter().writeValue(new File(tempDir + "/responsewithoutentires.json"), response);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testCompleteResponseToJSONWithoutProducts() throws JsonGenerationException, JsonMappingException,
            IOException

    {
        final Response response = new Response(true);
        final Customer customer = new Customer();
        customer.setFirstName("Test firs Name");
        customer.setLastName("Test Last Name");
        final Consignment consignment = new Consignment();
        consignment.setCode("400");
        consignment.setCustomer(customer);
        consignment.setOrderNumber("1000");
        consignment.setStatus("Completed");
        consignment.setTotalItems(Long.valueOf(20));
        final ConsignmentEntry ce = new ConsignmentEntry();

        ce.setQuantity("2000");
        final List<ConsignmentEntry> entries = new ArrayList<>();
        entries.add(ce);
        consignment.setEntries(entries);
        final List<Consignment> consignments = new ArrayList<>();
        consignments.add(consignment);


        final ConsignmentsResponseData crd = new ConsignmentsResponseData();
        crd.setConsignments(consignments);
        response.setData(crd);

        final ObjectMapper mapper = new ObjectMapper();

        mapper.defaultPrettyPrintingWriter().writeValue(new File(tempDir + "/responsewithoutproducts.json"),
                response);

    }

    @Test
    public void testSerializeParcelDetails() throws JsonGenerationException, JsonMappingException, IOException {

        final Parcel parcel1 = new Parcel();
        parcel1.setHeight(Double.valueOf(12.34d));
        parcel1.setWeight(Double.valueOf(12.45d));
        parcel1.setLength(Double.valueOf(12.56d));
        parcel1.setWidth(Double.valueOf(12.67d));

        final Parcel parcel2 = new Parcel();
        parcel2.setHeight(Double.valueOf(22.34d));
        parcel2.setWeight(Double.valueOf(22.45d));
        parcel2.setLength(Double.valueOf(22.56d));
        parcel2.setWidth(Double.valueOf(22.67d));

        final List<Parcel> parcelList = Collections.list(parcel1, parcel2);

        final ObjectMapper mapper = new ObjectMapper();
        final StringWriter writer = new StringWriter();
        mapper.writeValue(writer, parcelList);
        assertThat(writer.toString())
                .isNotEmpty()
                .isEqualTo(
                        "[{\"height\":12.34,"
                                + "\"weight\":12.45,"
                                + "\"length\":12.56,"
                                + "\"width\":12.67}"
                                + ",{\"height\":22.34,"
                                + "\"weight\":22.45,"
                                + "\"length\":22.56,"
                                + "\"width\":22.67}]");
    }

    @Test
    public void testDeSerializeParcelDetails() throws JsonParseException, JsonMappingException, IOException {
        final String jsonString = "[{\"height\":12.34,"
                + "\"weight\":12.45,"
                + "\"length\":12.56,"
                + "\"width\":12.67}"
                + ",{\"height\":22.34,"
                + "\"weight\":22.45,"
                + "\"length\":22.56,"
                + "\"width\":22.67}]";
        final ObjectMapper mapper = new ObjectMapper();
        final List<Parcel> parcels = mapper.readValue(new StringReader(jsonString), new TypeReference<List<Parcel>>() {
            //nothing to implement
        });
        assertThat(parcels).isNotNull().isNotEmpty().hasSize(2);
        final Parcel parcel1 = parcels.get(0);
        assertThat(parcel1.getHeight()).isNotNull().isEqualTo(Double.valueOf(12.34d));
        assertThat(parcel1.getWeight()).isNotNull().isEqualTo(Double.valueOf(12.45d));
        assertThat(parcel1.getLength()).isNotNull().isEqualTo(Double.valueOf(12.56d));
        assertThat(parcel1.getWidth()).isNotNull().isEqualTo(Double.valueOf(12.67d));
        final Parcel parcel2 = parcels.get(1);
        assertThat(parcel2.getHeight()).isNotNull().isEqualTo(Double.valueOf(22.34d));
        assertThat(parcel2.getWeight()).isNotNull().isEqualTo(Double.valueOf(22.45d));
        assertThat(parcel2.getLength()).isNotNull().isEqualTo(Double.valueOf(22.56d));
        assertThat(parcel2.getWidth()).isNotNull().isEqualTo(Double.valueOf(22.67d));
    }
}

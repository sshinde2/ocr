/**
 * 
 */
package au.com.target.tgtwsfacades.instore.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.ConsignmentStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.config.TargetSharedConfigService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.exception.TargetAmbiguousIdentifierException;
import au.com.target.tgtcore.exception.TargetUnknownIdentifierException;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.StoreEmployeeModel;
import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtcore.model.TargetPointOfServiceModel;
import au.com.target.tgtcore.stockvisibility.dto.response.StockVisibilityItemLookupResponseDto;
import au.com.target.tgtcore.storelocator.pos.TargetPointOfServiceService;
import au.com.target.tgtfacades.response.data.BaseResponseData;
import au.com.target.tgtfacades.response.data.Response;
import au.com.target.tgtfluent.data.Error;
import au.com.target.tgtfluent.exception.FluentClientException;
import au.com.target.tgtfluent.exception.FluentFulfilmentException;
import au.com.target.tgtfluent.service.FluentStockLookupService;
import au.com.target.tgtfulfilment.dispatchlabel.service.DispatchLabelService;
import au.com.target.tgtfulfilment.dto.TargetConsignmentPageResult;
import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.fulfilmentservice.PickConsignmentUpdater;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.integration.dto.LabelLayout;
import au.com.target.tgtfulfilment.integration.dto.LabelResponseDTO;
import au.com.target.tgtfulfilment.integration.dto.ManifestResponseDTO;
import au.com.target.tgtfulfilment.manifest.service.TransmitManifestService;
import au.com.target.tgtfulfilment.model.StoreFulfilmentCapabilitiesModel;
import au.com.target.tgtfulfilment.model.TargetManifestModel;
import au.com.target.tgtfulfilment.model.TargetStandardParcelModel;
import au.com.target.tgtfulfilment.service.TargetManifestService;
import au.com.target.tgtfulfilment.service.TargetStandardParcelService;
import au.com.target.tgtfulfilment.service.TargetStoreConsignmentService;
import au.com.target.tgtsale.stock.dto.response.StockUpdateProductResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateResponseDto;
import au.com.target.tgtsale.stock.dto.response.StockUpdateStoreResponseDto;
import au.com.target.tgtsale.stock.service.TargetStoreStockService;
import au.com.target.tgtutility.util.TargetDateUtil;
import au.com.target.tgtwebcore.exception.TargetIntegrationException;
import au.com.target.tgtwsfacades.constants.TgtwsfacadesConstants;
import au.com.target.tgtwsfacades.instore.converters.InstoreBasicManifestConverter;
import au.com.target.tgtwsfacades.instore.converters.InstoreConsignmentConverter;
import au.com.target.tgtwsfacades.instore.converters.InstoreManifestConverter;
import au.com.target.tgtwsfacades.instore.converters.ProductStockLevelConverter;
import au.com.target.tgtwsfacades.instore.converters.StandardParcelDetailsConverter;
import au.com.target.tgtwsfacades.instore.converters.StockVisResponseToProductStockLevelInstoreConverter;
import au.com.target.tgtwsfacades.instore.dto.CustomParcelConfig;
import au.com.target.tgtwsfacades.instore.dto.OfcErrorConstants;
import au.com.target.tgtwsfacades.instore.dto.StandardParcel;
import au.com.target.tgtwsfacades.instore.dto.consignments.Consignment;
import au.com.target.tgtwsfacades.instore.dto.consignments.ConsignmentEntry;
import au.com.target.tgtwsfacades.instore.dto.consignments.ConsignmentsResponseData;
import au.com.target.tgtwsfacades.instore.dto.consignments.Product;
import au.com.target.tgtwsfacades.instore.dto.consignments.ProductStockLevelInstore;
import au.com.target.tgtwsfacades.instore.dto.consignments.StandardParcelDetailsData;
import au.com.target.tgtwsfacades.instore.dto.dashboard.DashboardResponseData;
import au.com.target.tgtwsfacades.instore.dto.manifests.Manifest;
import au.com.target.tgtwsfacades.instore.dto.manifests.ManifestsResponseData;
import au.com.target.tgtwsfacades.instore.dto.response.data.ProductSohResponseData;
import au.com.target.tgtwsfacades.instore.dto.user.Store;
import au.com.target.tgtwsfacades.instore.dto.user.User;
import au.com.target.tgtwsfacades.instore.dto.user.UserResponseData;


/**
 * Test suite for {@link TargetInStoreIntegrationFacadeImpl}.
 * 
 * @author sbryan6
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetInStoreIntegrationFacadeImplTest {

    /**
     * 
     */
    private static final String INSTORE_REJECT_PICK_NOT_ATTEMPTED = "Pick Not Attempted";
    private static final String MANIFEST_CODE = "0000001";
    private static final String CONSIGNMENT_NUMBER = "a0000000001";
    private static final Integer PARCEL_COUNT = Integer.valueOf(1);
    private static final String STORE_NAME = "Camberwell";
    private static final Integer STORE_NUMBER = Integer.valueOf(7032);
    private static final String USER_NAME = "camberwellUser";
    private static final String USER_DISPLAY_NAME = "Camberwell User";
    private static final int OFFSET = 0;
    private static final int RECS_PER_PAGE = 2;
    private static final int LAST_X_FETCH_CONSINGMENT_DAYS = 60;

    @Mock
    private TargetFulfilmentService targetFulfilmentService;

    @Mock
    private TargetStoreConsignmentService targetStoreConsignmentService;

    @Mock
    private InstoreConsignmentConverter instoreConsignmentConverter;

    // This is the real thing
    private final ConsignmentStatusMapperImpl consignmentStatusMapper = new ConsignmentStatusMapperImpl();

    @InjectMocks
    @Spy
    private final TargetInStoreIntegrationFacadeImpl facade = new TargetInStoreIntegrationFacadeImpl();

    @Mock
    private TargetConsignmentModel consignmentModel;

    @Mock
    private StockUpdateResponseDto stockUpdateResponse;

    @Mock
    private AbstractOrderEntryModel orderEntry;

    @Mock
    private OrderModel orderModel;

    @Mock
    private TargetPointOfServiceModel storeModel;

    @Mock
    private TargetPointOfServiceModel storeBigAndBulkyModel;

    @Mock
    private TargetManifestService targetManifestService;

    @Mock
    private TransmitManifestService transmitManifestService;

    @Mock
    private DispatchLabelService dispatchLabelService;

    @Mock
    private TargetPointOfServiceService targetPointOfServiceService;

    @Mock
    private InstoreBasicManifestConverter instoreBasicManifestConverter;

    @Mock
    private InstoreManifestConverter instoreManifestConverter;

    @Mock
    private ProductStockLevelConverter productStockLevelConverter;

    @Mock
    private TargetStoreStockService targetStoreStockService;

    @Mock
    private StoreEmployeeModel storeUserModel;

    @Mock
    private UserModel userModel;

    @Mock
    private TargetConsignmentPageResult targetConsignmentPageResult;

    @Mock
    private Manifest mockManifest;

    @Mock
    private ManifestResponseDTO transmitResponse;

    @Mock
    private TargetFeatureSwitchService featureSwitchService;

    @Mock
    private PickConsignmentUpdater pickConsignmentUpdater;

    @Mock
    private StoreFulfilmentCapabilitiesModel storeFulFilmentCapabilityModel;

    @Mock
    private StockVisResponseToProductStockLevelInstoreConverter stockVisResponseToProductStockLevelInstoreConverter;

    @Mock
    private FluentStockLookupService fluentStockLookupService;

    @Mock
    private User user;

    @Mock
    private Store bulkyStore;

    @Mock
    private UserResponseData userData;

    @Mock
    private UserService userService;

    @Mock
    private StandardParcel standardParcel;

    @Mock
    private TargetStandardParcelModel targetStandardParcelModel;

    @Mock
    private TargetStandardParcelService targetStandardParcelService;

    @Mock
    private StandardParcelDetailsConverter standardParcelDetailsConverter;

    @Mock
    private TargetSharedConfigService targetSharedConfigService;

    private final Consignment consignmentDto = new Consignment();

    private final List<String> consignmentCodes = Collections.singletonList(CONSIGNMENT_NUMBER);

    private List<AbstractTargetVariantProductModel> varaintModel = null;

    private StockUpdateResponseDto stockDto;

    @Before
    public void setup() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        final List<TargetConsignmentModel> consignments = Collections.singletonList(consignmentModel);
        given(targetStoreConsignmentService.getConsignmentsForStore(STORE_NUMBER)).willReturn(consignments);


        given(targetStoreConsignmentService.getConsignmentsNotManifestedForStore(STORE_NUMBER.intValue()))
                .willReturn(
                        consignments);
        given(instoreConsignmentConverter.convert(consignmentModel)).willReturn(consignmentDto);
        createConsignmentStatsStatusMap();
        facade.setConsignmentStatusMapper(consignmentStatusMapper);

        given(consignmentModel.getCode()).willReturn(CONSIGNMENT_NUMBER);
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        given(targetStoreConsignmentService.getConsignmentsByCodes(consignmentCodes)).willReturn(consignments);
        given(targetStoreConsignmentService.getConsignmentByCode(CONSIGNMENT_NUMBER)).willReturn(
                consignmentModel);

        // Setup user and store details
        given(storeModel.getStoreNumber()).willReturn(STORE_NUMBER);
        given(storeModel.getName()).willReturn(STORE_NAME);
        given(storeBigAndBulkyModel.getStoreNumber()).willReturn(STORE_NUMBER);
        given(storeBigAndBulkyModel.getName()).willReturn(STORE_NAME);
        willReturn(Boolean.TRUE).given(storeBigAndBulkyModel).isBigAndBulky();
        given(userModel.getName()).willReturn(USER_NAME);
        given(userModel.getDisplayName()).willReturn(USER_DISPLAY_NAME);
        given(storeUserModel.getName()).willReturn(USER_NAME);
        given(storeUserModel.getDisplayName()).willReturn(USER_DISPLAY_NAME);
        given(storeUserModel.getStore()).willReturn(storeModel);
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willReturn(storeModel);

        // transmit manifest default success
        given(transmitManifestService.transmitManifest(any(TargetPointOfServiceModel.class),
                any(TargetManifestModel.class))).willReturn(transmitResponse);
        given(Boolean.valueOf(transmitResponse.isSuccess())).willReturn(Boolean.TRUE);

        // Allowed label consignment statuses
        final List<ConsignmentStatus> consignmentStatusAllowedForLabel = new ArrayList<>();
        consignmentStatusAllowedForLabel.add(ConsignmentStatus.PACKED);
        consignmentStatusAllowedForLabel.add(ConsignmentStatus.SHIPPED);
        facade.setConsignmentStatusAllowedForLabel(consignmentStatusAllowedForLabel);

        final List<AbstractOrderEntryModel> oems = new ArrayList<>();
        AbstractOrderEntryModel oem = new AbstractOrderEntryModel();
        varaintModel = new ArrayList<>();

        final AbstractTargetVariantProductModel pm1 = new AbstractTargetVariantProductModel();
        pm1.setCode("11000011");
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm1);
        oems.add(oem);
        oem = new AbstractOrderEntryModel();
        final AbstractTargetVariantProductModel pm2 = new AbstractTargetVariantProductModel();
        pm2.setCode("11000012");
        varaintModel.add(pm1);
        varaintModel.add(pm2);
        oem.setQuantity(Long.valueOf(20L));
        oem.setProduct(pm2);
        oems.add(oem);
        given(orderModel.getEntries()).willReturn(oems);

        given(consignmentModel.getOrder()).willReturn(orderModel);
        stockDto = new StockUpdateResponseDto();
        final List<StockUpdateStoreResponseDto> stockUpdateDtos = new ArrayList<>();
        final StockUpdateStoreResponseDto stockUpdateDto = new StockUpdateStoreResponseDto();
        stockUpdateDto.setStoreNumber(STORE_NUMBER.toString());
        final List<StockUpdateProductResponseDto> productDtos = new ArrayList<>();
        final StockUpdateProductResponseDto productDto1 = new StockUpdateProductResponseDto();
        final StockUpdateProductResponseDto productDto2 = new StockUpdateProductResponseDto();
        productDto1.setItemcode("11000011");
        productDto1.setSoh("15");
        productDto2.setItemcode("11000012");
        productDto2.setSoh("25");
        productDtos.add(productDto1);
        productDtos.add(productDto2);
        stockUpdateDto.setStockUpdateProductResponseDtos(productDtos);
        stockUpdateDtos.add(stockUpdateDto);
        stockDto.setStockUpdateStoreResponseDtos(stockUpdateDtos);

        willReturn(Boolean.FALSE).given(featureSwitchService).isFeatureEnabled("ofc.dashboard.performance");
        willReturn(Boolean.FALSE).given(featureSwitchService)
                .isFeatureEnabled(TgtCoreConstants.FeatureSwitch.USE_CACHE_STOCK_FOR_FULFIMENT);

        given(storeModel.getFulfilmentCapability()).willReturn(storeFulFilmentCapabilityModel);

    }

    @Test
    public void testGetConsignmentsForStore() {

        final List<TargetConsignmentModel> consignments = Collections.singletonList(consignmentModel);
        given(targetStoreConsignmentService.getConsignmentsForStore(STORE_NUMBER, OFFSET, RECS_PER_PAGE,
                LAST_X_FETCH_CONSINGMENT_DAYS, null, null, null, null))
                        .willReturn(targetConsignmentPageResult);
        given(targetConsignmentPageResult.getConsignments()).willReturn(consignments);

        final Response response = facade.getConsignmentsForStore(STORE_NUMBER, OFFSET, RECS_PER_PAGE,
                LAST_X_FETCH_CONSINGMENT_DAYS, null, null, null, null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ConsignmentsResponseData.class);

        final ConsignmentsResponseData data = (ConsignmentsResponseData)response.getData();
        assertThat(data.getConsignments()).isNotNull().hasSize(1);
        assertThat(data.getConsignments().get(0)).isSameAs(consignmentDto);
    }

    @Test
    public void testGetConsignmentsForStoreNoConsignments() {

        given(targetStoreConsignmentService.getConsignmentsForStore(STORE_NUMBER, OFFSET, RECS_PER_PAGE,
                LAST_X_FETCH_CONSINGMENT_DAYS, null, null, null, null))
                        .willReturn(null);
        final Response response = facade.getConsignmentsForStore(STORE_NUMBER, OFFSET, RECS_PER_PAGE,
                LAST_X_FETCH_CONSINGMENT_DAYS, null, null, null, null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ConsignmentsResponseData.class);

        final ConsignmentsResponseData data = (ConsignmentsResponseData)response.getData();
        assertThat(data.getConsignments()).isEmpty();
    }

    @Test
    public void testGetConsignmentsForStoreWithException() {

        given(targetStoreConsignmentService.getConsignmentsForStore(STORE_NUMBER, OFFSET, RECS_PER_PAGE,
                LAST_X_FETCH_CONSINGMENT_DAYS, null, null, null, null))
                        .willThrow(
                                new RuntimeException("TEST"));

        final Response response = facade.getConsignmentsForStore(STORE_NUMBER, OFFSET, RECS_PER_PAGE,
                LAST_X_FETCH_CONSINGMENT_DAYS, null, null, null, null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);

        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);
    }

    @Test
    public void testGetConsignmentsByStatusForTodayWithConsignmentsPerfFeatureSwitchOff() {
        // Today's stats - 2-Open, 1-In Progress, 1-picked, 1-Completed and 1-Rejected
        final List consList = createConsignmentsForDashboard();

        given(targetStoreConsignmentService.getConsignmentsByDayForStore(storeModel, 0))
                .willReturn(consList);

        final Response response = facade.getConsignmentsByStatusForToday(STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(DashboardResponseData.class);

        final DashboardResponseData data = (DashboardResponseData)response.getData();
        assertThat(data.getConsignmentStats().getOpen()).isEqualTo(Integer.valueOf(2));
        assertThat(data.getConsignmentStats().getInProgress()).isEqualTo(Integer.valueOf(1));
        assertThat(data.getConsignmentStats().getPicked()).isEqualTo(Integer.valueOf(1));
        assertThat(data.getConsignmentStats().getCompleted()).isEqualTo(Integer.valueOf(1));
        assertThat(data.getConsignmentStats().getRejected()).isEqualTo(Integer.valueOf(1));
        assertThat(data.getConsignmentStats().getPacked()).isEqualTo(Integer.valueOf(1));
    }


    @Test
    public void testGetConsignmentsByStatusForTodayWithConsignmentsPerfFeatureSwitchOn() {
        given(Boolean.valueOf(featureSwitchService.isFeatureEnabled("ofc.dashboard.performance")))
                .willReturn(Boolean.TRUE);

        given(targetStoreConsignmentService.getConsignmentCountByStatus(storeModel, 0))
                .willReturn(createConsignmentByStatusMap());
        final Response response = facade.getConsignmentsByStatusForToday(STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(DashboardResponseData.class);

        final DashboardResponseData data = (DashboardResponseData)response.getData();
        assertThat(data.getConsignmentStats().getOpen()).isEqualTo(Integer.valueOf(3));
        assertThat(data.getConsignmentStats().getInProgress()).isEqualTo(Integer.valueOf(2));
        assertThat(data.getConsignmentStats().getPicked()).isEqualTo(Integer.valueOf(3));
        assertThat(data.getConsignmentStats().getCompleted()).isEqualTo(Integer.valueOf(3));
        assertThat(data.getConsignmentStats().getRejected()).isEqualTo(Integer.valueOf(5));
        assertThat(data.getConsignmentStats().getPacked()).isEqualTo(Integer.valueOf(4));
    }

    private Map<ConsignmentStatus, Integer> createConsignmentByStatusMap() {
        final Map<ConsignmentStatus, Integer> consignmentStatusMap = new HashMap<>();
        consignmentStatusMap.put(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE, Integer.valueOf(1));
        consignmentStatusMap.put(ConsignmentStatus.SENT_TO_WAREHOUSE, Integer.valueOf(2));
        consignmentStatusMap.put(ConsignmentStatus.PICKED, Integer.valueOf(3));
        consignmentStatusMap.put(ConsignmentStatus.PACKED, Integer.valueOf(4));
        consignmentStatusMap.put(ConsignmentStatus.CANCELLED, Integer.valueOf(5));
        consignmentStatusMap.put(ConsignmentStatus.SHIPPED, Integer.valueOf(3));
        consignmentStatusMap.put(ConsignmentStatus.WAVED, Integer.valueOf(2));
        return consignmentStatusMap;
    }


    @Test
    public void testGetConsignmentsByStatusForYesterdayWithConsignmentsPerfFeatureSwitchOn() {
        given(Boolean.valueOf(featureSwitchService.isFeatureEnabled("ofc.dashboard.performance")))
                .willReturn(Boolean.TRUE);

        given(targetStoreConsignmentService.getConsignmentCountByStatus(storeModel, 1))
                .willReturn(createConsignmentByStatusMap());
        final Response response = facade.getConsignmentsByStatusForYesterday(STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(DashboardResponseData.class);

        final DashboardResponseData data = (DashboardResponseData)response.getData();
        assertThat(data.getConsignmentStats().getOpen()).isNull();
        assertThat(data.getConsignmentStats().getInProgress()).isNull();
        assertThat(data.getConsignmentStats().getPicked()).isNull();
        assertThat(data.getConsignmentStats().getCompleted()).isEqualTo(Integer.valueOf(3));
        assertThat(data.getConsignmentStats().getRejected()).isEqualTo(Integer.valueOf(5));
        assertThat(data.getConsignmentStats().getPacked()).isNull();
    }


    @Test
    public void testGetStandardParcelDetails()
            throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        given(userData.getUser()).willReturn(user);
        given(userService.getCurrentUser()).willReturn(userModel);
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willReturn(null);

        given(targetStandardParcelService
                .getEnabledStandardParcelDetails()).willReturn(Arrays.asList(targetStandardParcelModel));
        given(standardParcelDetailsConverter.convert(targetStandardParcelModel)).willReturn(standardParcel);

        final Response response = facade.getStandardParcelDetails();
        final StandardParcelDetailsData data = (StandardParcelDetailsData)response.getData();
        assertThat(data.getCustomParcels()).isNull();
        assertThat(data.getStandardParcels()).hasSize(1);

        final StandardParcel parcel = data.getStandardParcels().iterator().next();
        assertThat(parcel).isEqualTo(standardParcel);

    }

    @Test
    public void testCustomParcelConfig() throws TargetUnknownIdentifierException, TargetAmbiguousIdentifierException {

        willReturn(userData).given(facade).getLoggedInUser();
        given(userData.getUser()).willReturn(user);
        given(user.getStore()).willReturn(bulkyStore);
        given(bulkyStore.getStoreNumber()).willReturn(STORE_NUMBER);
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willReturn(storeModel);
        willReturn(Boolean.TRUE).given(storeModel).isBigAndBulky();
        given(targetSharedConfigService
                .getDoubleWrapper(TgtCoreConstants.Config.BIG_AND_BULKY_DEFAULT_MAX_SIZE,
                        TargetInStoreIntegrationFacadeImpl.DEFAULT_MAX_SIZE)).willReturn(Double.valueOf(200));
        given(targetSharedConfigService
                .getDoubleWrapper(TgtCoreConstants.Config.BIG_AND_BULKY_DEFAULT_MAX_WEIGHT,
                        TargetInStoreIntegrationFacadeImpl.DEFAULT_MAX_WEIGHT)).willReturn(Double.valueOf(300));

        final Response response = facade.getStandardParcelDetails();
        final StandardParcelDetailsData data = (StandardParcelDetailsData)response.getData();
        assertThat(data.getStandardParcels()).isNull();
        final CustomParcelConfig customParcels = data.getCustomParcels();
        assertThat(customParcels).isNotNull();
        assertThat(customParcels.getMaxSize()).isEqualTo(200);
        assertThat(customParcels.getMaxWeight()).isEqualTo(300);

    }

    private List createConsignmentsForDashboard() {
        final Date currentDate = new Date();
        final TargetConsignmentModel openCons1 = new TargetConsignmentModel();
        openCons1.setStatus(ConsignmentStatus.CONFIRMED_BY_WAREHOUSE);
        openCons1.setCode("1234");
        final TargetConsignmentModel openCons2 = new TargetConsignmentModel();
        openCons2.setStatus(ConsignmentStatus.SENT_TO_WAREHOUSE);
        openCons2.setCode("1235");
        final TargetConsignmentModel wavedCons = new TargetConsignmentModel();
        wavedCons.setStatus(ConsignmentStatus.WAVED);
        wavedCons.setCode("1236");

        final TargetConsignmentModel pickedCons = new TargetConsignmentModel();
        pickedCons.setStatus(ConsignmentStatus.PICKED);
        pickedCons.setCode("1237");

        final TargetConsignmentModel packedCons = new TargetConsignmentModel();
        packedCons.setStatus(ConsignmentStatus.PACKED);
        packedCons.setCode("1238");

        final TargetConsignmentModel shippedCons = new TargetConsignmentModel();
        shippedCons.setStatus(ConsignmentStatus.SHIPPED);
        shippedCons.setShippingDate(currentDate);
        shippedCons.setCode("1239");

        final TargetConsignmentModel cancelledCons = new TargetConsignmentModel();
        cancelledCons.setStatus(ConsignmentStatus.CANCELLED);
        cancelledCons.setCode("1230");

        final List consList = new ArrayList<>();
        consList.add(openCons1);
        consList.add(openCons2);
        consList.add(wavedCons);
        consList.add(pickedCons);
        consList.add(shippedCons);
        consList.add(cancelledCons);
        consList.add(packedCons);
        return consList;
    }

    @Test
    public void testGetConsignmentsByStatusForTodayNoConsignments() {
        given(targetStoreConsignmentService.getConsignmentsForStore(STORE_NUMBER)).willReturn(null);

        final Response response = facade.getConsignmentsByStatusForToday(STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(DashboardResponseData.class);

        final DashboardResponseData data = (DashboardResponseData)response.getData();
        assertThat(data.getConsignmentStats().getOpen()).isEqualTo(Integer.valueOf(0));
        assertThat(data.getConsignmentStats().getInProgress()).isEqualTo(Integer.valueOf(0));
        assertThat(data.getConsignmentStats().getCompleted()).isEqualTo(Integer.valueOf(0));
        assertThat(data.getConsignmentStats().getRejected()).isEqualTo(Integer.valueOf(0));
        assertThat(data.getConsignmentStats().getPicked()).isEqualTo(Integer.valueOf(0));
        assertThat(data.getConsignmentStats().getPacked()).isEqualTo(Integer.valueOf(0));
    }

    @Test
    public void testGetConsignmentsByStatusForTodayWithException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willThrow(
                new TargetUnknownIdentifierException(""));

        final Response response = facade.getConsignmentsByStatusForToday(STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);

        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);
    }

    @Test
    public void testGetConsignmentStatsForYesterdayWithConsignments() {
        // Yesterday's stats - 1-Completed and 1-Rejected
        final List consList = createConsignmentsForDashboard();

        given(targetStoreConsignmentService.getConsignmentsByDayForStore(storeModel, 1))
                .willReturn(consList);

        final Response response = facade.getConsignmentsByStatusForYesterday(STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(DashboardResponseData.class);

        final DashboardResponseData data = (DashboardResponseData)response.getData();
        assertThat(data.getConsignmentStats().getOpen()).isNull();
        assertThat(data.getConsignmentStats().getInProgress()).isNull();
        assertThat(data.getConsignmentStats().getPicked()).isNull();
        assertThat(data.getConsignmentStats().getCompleted()).isEqualTo(Integer.valueOf(1));
        assertThat(data.getConsignmentStats().getRejected()).isEqualTo(Integer.valueOf(1));
    }

    @Test
    public void testGetConsignmentsByStatusForYesterdayNoConsignments() {
        given(targetStoreConsignmentService.getConsignmentsForStore(STORE_NUMBER)).willReturn(null);

        final Response response = facade.getConsignmentsByStatusForYesterday(STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(DashboardResponseData.class);

        final DashboardResponseData data = (DashboardResponseData)response.getData();
        assertThat(data.getConsignmentStats().getOpen()).isNull();
        assertThat(data.getConsignmentStats().getInProgress()).isNull();
        assertThat(data.getConsignmentStats().getCompleted()).isEqualTo(Integer.valueOf(0));
        assertThat(data.getConsignmentStats().getRejected()).isEqualTo(Integer.valueOf(0));
    }

    @Test
    public void testGetConsignmentsByStatusForYesterdayWithException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willThrow(
                new TargetUnknownIdentifierException(""));

        final Response response = facade.getConsignmentsByStatusForYesterday(STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);

        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);
    }

    /**
     * This test will throw {@link NotFoundException} when a consignment is completed in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testCompleteConsignmentForInstoreWhenConsignmentThrowsNotFoundException()
            throws Exception {
        final NotFoundException notFoundException = new NotFoundException(
                "Error getting consignment details for " + CONSIGNMENT_NUMBER);

        willThrow(notFoundException).given(targetFulfilmentService).processCompleteForInstoreFulfilment(
                CONSIGNMENT_NUMBER, PARCEL_COUNT.intValue(),
                null);

        final Response response = facade.completeConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, PARCEL_COUNT,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_NOT_FOUND");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("Error getting consignment details for a0000000001");
    }

    /**
     * This test will throw {@link NumberFormatException} when a consignment is completed in store with invalid parcel
     * count.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testCompleteConsignmentForInstoreWhenParcelCountNullParcelCount()
            throws JsonGenerationException,
            JsonMappingException, IOException {
        final Response response = facade.completeConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, null, null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_INVALID_PARAM");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Parcel count has to be positive");
    }

    /**
     * This test will give and error response when a consignment is completed in store with a zero parcel count.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testCompleteConsignmentForInstoreGivesErrorWhenParcelCountZero()
            throws JsonGenerationException,
            JsonMappingException, IOException {
        final Response response = facade.completeConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER,
                Integer.valueOf(0), null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_INVALID_PARAM");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Parcel count has to be positive");
    }

    /**
     * This test will give and error response when a consignment is completed in store with a negative parcel count.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testCompleteConsignmentForInstoreGivesErrorWhenParcelCountNegative()
            throws JsonGenerationException,
            JsonMappingException, IOException {
        final Response response = facade.completeConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER,
                Integer.valueOf(-2), null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_INVALID_PARAM");
        assertThat(response.getData().getError().getMessage()).isEqualTo("Parcel count has to be positive");
    }

    /**
     * This test will throw {@link RuntimeException} when a consignment is completed in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testCompleteConsignmentForInstoreWhenConsignmentThrowsUnknownExceptions() throws Exception {
        willThrow(new RuntimeException())
                .given(targetFulfilmentService)
                .processCompleteForInstoreFulfilment(CONSIGNMENT_NUMBER, PARCEL_COUNT.intValue(),
                        null);
        final Response response = facade.completeConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, PARCEL_COUNT,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_SYSTEM");
        assertThat(response.getData().getError().getMessage()).isNull();
    }

    /**
     * This test will throw {@link FluentFulfilmentException} when a consignment is completed in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */

    @Test
    public void testCompleteConsignmentForInstoreWhenConsignmentThrowsFluentFulfilmentException() throws Exception {
        final FluentFulfilmentException fluentFulfilmentException = new FluentFulfilmentException(
                "Error re-routing consignment " + CONSIGNMENT_NUMBER);
        willThrow(fluentFulfilmentException).given(targetFulfilmentService)
                .processCompleteForInstoreFulfilment(CONSIGNMENT_NUMBER, PARCEL_COUNT.intValue(),
                        null);
        final Response response = facade.completeConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, PARCEL_COUNT,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("FLUENT_SYSTEM_ERR");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("Error re-routing consignment a0000000001");
    }

    @Test
    public void testCompleteConsignmentForInstoreWhenConsignmentThrowsFluentClientException() throws Exception {
        final Error error = new Error();
        error.setMessage("Error re-routing consignment " + CONSIGNMENT_NUMBER);
        final FluentClientException fluentClientException = new FluentClientException(error);
        willThrow(fluentClientException).given(targetFulfilmentService)
                .processCompleteForInstoreFulfilment(CONSIGNMENT_NUMBER, PARCEL_COUNT.intValue(),
                        null);
        final Response response = facade.completeConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, PARCEL_COUNT,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("FLUENT_SYSTEM_ERR");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("code=null, message=Error re-routing consignment a0000000001");
    }


    /**
     * Happy path for completing a consignment in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testCompleteConsignmentForInstoreWithoutExceptions() throws JsonGenerationException,
            JsonMappingException,
            IOException {
        final Response response = facade.completeConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, PARCEL_COUNT,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
    }

    /**
     * This test will throw {@link NotFoundException} when a consignment is picked in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     * @throws ConsignmentStatusValidationException
     */
    @Test
    public void testPickForPackConsignmentForInstoreWhenConsignmentThrowsKnownExceptions()
            throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException, ConsignmentStatusValidationException {
        final NotFoundException notFoundException = new NotFoundException(
                "Error getting consignment details for " + CONSIGNMENT_NUMBER);
        willThrow(notFoundException).given(targetFulfilmentService)
                .processPickForInstoreFulfilment(CONSIGNMENT_NUMBER);
        final Response response = facade.pickForPackConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_NOT_FOUND");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("Error getting consignment details for a0000000001");
    }

    /**
     * This test will throw {@link RuntimeException} when a consignment is picked in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testPickForPackConsignmentForInstoreWhenConsignmentThrowsUnknownExceptions()
            throws JsonGenerationException,
            JsonMappingException, IOException, NotFoundException, ConsignmentStatusValidationException {
        willThrow(new RuntimeException()).given(targetFulfilmentService)
                .processPickForInstoreFulfilment(CONSIGNMENT_NUMBER);
        final Response response = facade.pickForPackConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_SYSTEM");
        assertThat(response.getData().getError().getMessage()).isNull();
    }

    /**
     * Happy path for picking a consignment in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testPickForPackConsignmentForInstoreWithoutExceptions() throws JsonGenerationException,
            JsonMappingException,
            IOException {
        final Response response = facade.pickForPackConsignmentInstore(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
    }

    /**
     * This test will throw {@link NotFoundException} when a consignment is re-routed.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testRejectConsignmentForInstoreWhenConsignmentThrowsKnownExceptions() throws Exception {
        final NotFoundException notFoundException = new NotFoundException(
                "Error re-routing consignment " + CONSIGNMENT_NUMBER);
        willThrow(notFoundException).given(targetFulfilmentService)
                .processRejectInstoreFulfilment(CONSIGNMENT_NUMBER, null, null);
        final Response response = facade.rejectConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, null,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_NOT_FOUND");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("Error re-routing consignment a0000000001");
    }

    @Test
    public void testRejectConsignmentForInstoreWhenConsignmentThrowsFluentFulfilmentException() throws Exception {
        final FluentFulfilmentException fluentFulfilmentException = new FluentFulfilmentException(
                "Error re-routing consignment " + CONSIGNMENT_NUMBER);
        willThrow(fluentFulfilmentException).given(targetFulfilmentService)
                .processRejectInstoreFulfilment(CONSIGNMENT_NUMBER, null, null);
        final Response response = facade.rejectConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, null,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("FLUENT_SYSTEM_ERR");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("Error re-routing consignment a0000000001");
    }

    @Test
    public void testRejectConsignmentForInstoreWhenConsignmentThrowsFluentClientException() throws Exception {
        final Error error = new Error();
        error.setMessage("Error re-routing consignment " + CONSIGNMENT_NUMBER);
        final FluentClientException fluentClientException = new FluentClientException(error);
        willThrow(fluentClientException).given(targetFulfilmentService)
                .processRejectInstoreFulfilment(CONSIGNMENT_NUMBER, null, null);
        final Response response = facade.rejectConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, null,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("FLUENT_SYSTEM_ERR");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("code=null, message=Error re-routing consignment a0000000001");
    }

    /**
     * This test will throw {@link RuntimeException} when a consignment is re-routed.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testRejectConsignmentForInstoreWhenConsignmentThrowsUnknownExceptions() throws Exception {
        willThrow(new RuntimeException()).given(targetFulfilmentService)
                .processRejectInstoreFulfilment(CONSIGNMENT_NUMBER, null, null);
        final Response response = facade.rejectConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER, null,
                null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_SYSTEM");
        assertThat(response.getData().getError().getMessage()).isNull();
    }

    /**
     * Happy path for re-routing a consignment in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testRejectConsignmentorInstoreWithoutExceptions() throws JsonGenerationException, JsonMappingException,
            IOException {
        final Response response = facade.rejectConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER,
                INSTORE_REJECT_PICK_NOT_ATTEMPTED, null);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
    }

    /**
     * This test will throw {@link NotFoundException} when picking consignment in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testWaveConsignmentForInstoreWhenConsignmentThrowsKnownExceptions() throws Exception {
        final NotFoundException notFoundException = new NotFoundException(
                " Cannot find consignment number " + CONSIGNMENT_NUMBER);
        willThrow(notFoundException).given(targetFulfilmentService)
                .processWavedForInstoreFulfilment(CONSIGNMENT_NUMBER);
        final Response response = facade.waveConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_NOT_FOUND");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo(" Cannot find consignment number a0000000001");
    }

    @Test
    public void testWaveConsignmentForInstoreWhenConsignmentThrowsFluentFulfilmentException() throws Exception {
        final FluentFulfilmentException fluentException = new FluentFulfilmentException(
                "Fluent error for consignment " + CONSIGNMENT_NUMBER);
        willThrow(fluentException).given(targetFulfilmentService)
                .processWavedForInstoreFulfilment(CONSIGNMENT_NUMBER);
        final Response response = facade.waveConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("FLUENT_SYSTEM_ERR");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("Fluent error for consignment a0000000001");
    }


    @Test
    public void testWaveConsignmentForInstoreWhenConsignmentThrowsFluentClientException() throws Exception {
        final Error error = new Error();
        error.setMessage("Error re-routing consignment " + CONSIGNMENT_NUMBER);
        final FluentClientException fluentClientException = new FluentClientException(error);
        willThrow(fluentClientException).given(targetFulfilmentService)
                .processWavedForInstoreFulfilment(CONSIGNMENT_NUMBER);
        final Response response = facade.waveConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("FLUENT_SYSTEM_ERR");
        assertThat(response.getData().getError().getMessage())
                .isEqualTo("code=null, message=Error re-routing consignment a0000000001");
    }

    /**
     * This test will throw {@link RuntimeException} when picking consignment in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     * @throws NotFoundException
     */
    @Test
    public void testWaveConsignmentForInstoreWhenConsignmentThrowsUnknownExceptions() throws Exception {
        willThrow(new RuntimeException()).given(targetFulfilmentService)
                .processWavedForInstoreFulfilment(CONSIGNMENT_NUMBER);
        final Response response = facade.waveConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData().getError()).isNotNull();
        assertThat(response.getData().getError().getCode()).isEqualTo("ERR_SYSTEM");
        assertThat(response.getData().getError().getMessage()).isNull();
    }

    /**
     * Happy path for picking consignment in store.
     * 
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    @Test
    public void testWaveConsignmentForInstoreWithoutExceptions() throws JsonGenerationException, JsonMappingException,
            IOException {
        final Response response = facade.waveConsignmentForInstore(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
    }

    @Test
    public void testGetConsignmentStatusDtos() throws NotFoundException {

        final List<Consignment> cons = facade.getConsignmentStatusDtos(CONSIGNMENT_NUMBER);
        assertThat(cons).isNotNull().hasSize(1);
        final Consignment con = cons.get(0);
        assertThat(con).isNotNull();
        assertThat(con.getCode()).isEqualTo(CONSIGNMENT_NUMBER);
        assertThat(con.getStatus()).isEqualTo(TgtwsfacadesConstants.CONSIGNMENT_STATUS_KEY_OPEN);
        assertThat(con.getRejectState()).isNull();
    }

    @Test(expected = NotFoundException.class)
    public void testGetConsignmentStatusDtosNotFound() throws NotFoundException {

        given(targetStoreConsignmentService.getConsignmentsByCodes(consignmentCodes)).willReturn(
                Collections.EMPTY_LIST);
        facade.getConsignmentStatusDtos(CONSIGNMENT_NUMBER);
    }

    @Test
    public void testGetConsignmentsByCodesWithConsignmnetCodes() throws NotFoundException {
        final List<TargetConsignmentModel> consignments = Collections.singletonList(consignmentModel);
        given(targetStoreConsignmentService.getConsignmentsByCodes(consignmentCodes)).willReturn(consignments);
        final Response response = facade.getConsignmentsByCodes(consignmentCodes, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ConsignmentsResponseData.class);
        final ConsignmentsResponseData data = (ConsignmentsResponseData)response.getData();

        assertThat(data.getConsignments()).isNotNull().hasSize(1);
        assertThat(data.getConsignments().get(0)).isSameAs(consignmentDto);

    }

    @Test
    public void testGetConsignmentsByCodesNoConsignmnetCodes() {
        given(targetStoreConsignmentService.getConsignmentsByCodes(consignmentCodes)).willReturn(null);
        final Response response = facade.getConsignmentsByCodes(consignmentCodes, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ConsignmentsResponseData.class);

        final ConsignmentsResponseData data = (ConsignmentsResponseData)response.getData();

        assertThat(data.getConsignments()).isEmpty();
    }

    @Test(expected = NotFoundException.class)
    public void testGetConsignmentsByCodesNotFound() throws NotFoundException {

        given(targetStoreConsignmentService.getConsignmentsByCodes(consignmentCodes)).willReturn(
                Collections.EMPTY_LIST);
        final Response response = facade.getConsignmentsByCodes(consignmentCodes, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        facade.getConsignmentStatusDtos(CONSIGNMENT_NUMBER);
        final ConsignmentsResponseData data = (ConsignmentsResponseData)response.getData();

        assertThat(data.getConsignments()).isEmpty();
    }

    @Test
    public void testConvertStore() {

        final Store store = facade.convertStore(storeModel);
        verifyStore(store);
    }

    @Test
    public void testConvertStoreBigAndBulky() {

        final Store store = facade.convertStore(storeBigAndBulkyModel);
        verifyStoreBigAndBulky(store);
    }

    @Test
    public void testConvertStoreUser() {

        final User user = facade.convertUser(storeUserModel);
        verifyUserNameDetails(user);
        verifyStore(user.getStore());
    }

    @Test
    public void testConvertNonStoreUser() {

        final User user = facade.convertUser(userModel);
        verifyUserNameDetails(user);

        assertThat(user.getStore()).isNull();
    }

    @Test
    public void testConvertUserWithNullStore() {

        given(storeUserModel.getStore()).willReturn(null);

        final User user = facade.convertUser(storeUserModel);
        verifyUserNameDetails(user);

        assertThat(user.getStore()).isNull();
    }

    @Test
    public void testSortConsignmentEntries() {
        final List<ConsignmentEntry> consignmentEntries = setupConsignmentEntries("Women", "Men", "Kids", "Toys",
                "Babies");
        facade.sortConsignmentEntries(consignmentEntries);
        verifyConsignmentEntry(consignmentEntries, 0, "Babies");
        verifyConsignmentEntry(consignmentEntries, 1, "Kids");
        verifyConsignmentEntry(consignmentEntries, 2, "Men");
        verifyConsignmentEntry(consignmentEntries, 3, "Toys");
        verifyConsignmentEntry(consignmentEntries, 4, "Women");
    }

    @Test
    public void testSortConsignmentEntriesWithNullEntries() {
        final List<ConsignmentEntry> consignmentEntries = setupConsignmentEntries("Women", null, "Kids", "Toys",
                "Babies");
        facade.sortConsignmentEntries(consignmentEntries);
        verifyConsignmentEntry(consignmentEntries, 0, "Babies");
        verifyConsignmentEntry(consignmentEntries, 1, "Kids");
        verifyConsignmentEntry(consignmentEntries, 2, "Toys");
        verifyConsignmentEntry(consignmentEntries, 3, "Women");
        verifyConsignmentEntry(consignmentEntries, 4, null);
    }

    @Test
    public void testSortConsignmentEntriesWithEmptyEntries() {
        final List<ConsignmentEntry> consignmentEntries = setupConsignmentEntries("Women", "", "Kids", "Toys",
                "Babies");
        facade.sortConsignmentEntries(consignmentEntries);
        verifyConsignmentEntry(consignmentEntries, 0, "Babies");
        verifyConsignmentEntry(consignmentEntries, 1, "Kids");
        verifyConsignmentEntry(consignmentEntries, 2, "Toys");
        verifyConsignmentEntry(consignmentEntries, 3, "Women");
        verifyConsignmentEntry(consignmentEntries, 4, "");
    }

    @Test
    public void testSortConsignmentEntriesWithOneNullEntries() {
        final List<ConsignmentEntry> consignmentEntries = setupConsignmentEntries("Women", "Kids", "Toys",
                "Babies");
        final ConsignmentEntry entry = new ConsignmentEntry();
        consignmentEntries.add(entry);
        facade.sortConsignmentEntries(consignmentEntries);
        verifyConsignmentEntry(consignmentEntries, 0, "Babies");
        verifyConsignmentEntry(consignmentEntries, 1, "Kids");
        verifyConsignmentEntry(consignmentEntries, 2, "Toys");
        verifyConsignmentEntry(consignmentEntries, 3, "Women");
        verifyConsignmentEntry(consignmentEntries, 4, "");
    }

    @Test
    public void testSortConsignmentEntriesWithTwoNullEntries() {
        final List<ConsignmentEntry> consignmentEntries = setupConsignmentEntries("Kids", "Toys",
                "Babies");
        final ConsignmentEntry entry = new ConsignmentEntry();
        consignmentEntries.add(entry);
        final ConsignmentEntry entry2 = new ConsignmentEntry();
        consignmentEntries.add(entry2);
        facade.sortConsignmentEntries(consignmentEntries);
        verifyConsignmentEntry(consignmentEntries, 0, "Babies");
        verifyConsignmentEntry(consignmentEntries, 1, "Kids");
        verifyConsignmentEntry(consignmentEntries, 2, "Toys");
        verifyConsignmentEntry(consignmentEntries, 3, "");
        verifyConsignmentEntry(consignmentEntries, 4, "");
    }

    @Test
    public void testSortConsignmentEntriesEmptyList() {
        final List<ConsignmentEntry> consignmentEntries = new ArrayList<>();
        facade.sortConsignmentEntries(consignmentEntries);
        assertThat(consignmentEntries).isEmpty();
    }

    @Test
    public void testSortConsignmentEntriesWithTwoProductsFromSameDepartment() {
        final List<ConsignmentEntry> consignmentEntries = setupConsignmentEntries("Women", "Women", "Kids", "Kids",
                "Toys",
                "Babies");
        facade.sortConsignmentEntries(consignmentEntries);
        verifyConsignmentEntry(consignmentEntries, 0, "Babies");
        verifyConsignmentEntry(consignmentEntries, 1, "Kids");
        verifyConsignmentEntry(consignmentEntries, 2, "Kids");
        verifyConsignmentEntry(consignmentEntries, 3, "Toys");
        verifyConsignmentEntry(consignmentEntries, 4, "Women");
        verifyConsignmentEntry(consignmentEntries, 5, "Women");
    }

    @Test
    public void testGetConsignmentsNotManifestedForStore() {
        final Response response = facade.getConsignmentsNotManifestedForStore(STORE_NUMBER.intValue());
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ManifestsResponseData.class);

        final ManifestsResponseData data = (ManifestsResponseData)response.getData();
        assertThat(data.getManifests()).isNotNull().hasSize(1);
    }

    @Test
    public void testGetConsignmentsNotManifestedForStoreNoConsignments() {

        given(targetStoreConsignmentService.getConsignmentsNotManifestedForStore(STORE_NUMBER.intValue()))
                .willReturn(null);
        final Response response = facade.getConsignmentsNotManifestedForStore(STORE_NUMBER.intValue());
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ManifestsResponseData.class);

        final ManifestsResponseData data = (ManifestsResponseData)response.getData();
        assertThat(data.getManifests()).isNotNull();
    }

    @Test
    public void testGetConsignmentsNotManifestedForStoreWithException() {

        given(targetStoreConsignmentService.getConsignmentsNotManifestedForStore(STORE_NUMBER.intValue()))
                .willThrow(
                        new RuntimeException("TEST"));

        final Response response = facade.getConsignmentsNotManifestedForStore(STORE_NUMBER.intValue());
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);

        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);

        verifyZeroInteractions(targetManifestService);
        verifyZeroInteractions(instoreManifestConverter);
    }

    @Test
    public void testTransmitManifestForStore() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Date currDate = Calendar.getInstance().getTime();
        final TargetManifestModel tempManifestModel = createManifestModel(currDate);
        given(targetManifestService.createManifest(storeModel)).willReturn(tempManifestModel);
        given(instoreBasicManifestConverter.convert(tempManifestModel)).willCallRealMethod();

        final Response response = facade.transmitManifestForStore(STORE_NUMBER.intValue());
        verifyManifestResponse(response, currDate);
    }

    @Test
    public void testTransmitManifestForStoreFailedTransmit() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Date currDate = Calendar.getInstance().getTime();
        final TargetManifestModel tempManifestModel = createManifestModel(currDate);
        given(targetManifestService.createManifest(storeModel)).willReturn(tempManifestModel);
        given(instoreBasicManifestConverter.convert(tempManifestModel)).willCallRealMethod();

        given(Boolean.valueOf(transmitResponse.isSuccess())).willReturn(Boolean.FALSE);

        final Response response = facade.transmitManifestForStore(STORE_NUMBER.intValue());
        verifyManifestResponse(response, currDate);

        // Verify we do not complete the orders
        verify(targetManifestService, never()).completeConsignmentsInManifest(
                any(TargetManifestModel.class), any(Integer.class));
    }

    private void verifyManifestResponse(final Response response, final Date currDate) {

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ManifestsResponseData.class);

        final ManifestsResponseData data = (ManifestsResponseData)response.getData();
        assertThat(data.getManifests().size() == 1);
        final Manifest manifest = data.getManifests().get(0);
        assertThat(MANIFEST_CODE).isEqualTo(manifest.getCode());
        assertThat(TargetDateUtil.getDateFormattedAsString(currDate)).isEqualTo(manifest.getDate());
    }

    @Test
    public void testTransmitManifestForStoreWithWrongStoreNumber() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willThrow(
                new TargetUnknownIdentifierException("Wrong store number"));

        final Response response = facade.transmitManifestForStore(STORE_NUMBER.intValue());
        verifyErrorResponse(response);

        verifyZeroInteractions(targetManifestService);
        verifyZeroInteractions(instoreManifestConverter);
    }

    private void verifyErrorResponse(final Response response) {

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);
        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);
    }

    @Test
    public void testTransmitManifestForStoreWithMutipleStoresForStoreNumber() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willThrow(
                new TargetAmbiguousIdentifierException("Multiple POS for this store number"));

        final Response response = facade.transmitManifestForStore(STORE_NUMBER.intValue());
        verifyErrorResponse(response);

        verifyZeroInteractions(targetManifestService);
        verifyZeroInteractions(instoreManifestConverter);
    }

    @Test
    public void testTransmitManifestForStoreWithException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER))
                .willThrow(
                        new RuntimeException("TEST"));

        final Response response = facade.transmitManifestForStore(STORE_NUMBER.intValue());
        verifyErrorResponse(response);

        verifyZeroInteractions(targetManifestService);
        verifyZeroInteractions(instoreManifestConverter);
    }

    @Test
    public void testGetManifestByCodeNotFound() throws NotFoundException {
        given(targetManifestService.getManifestByCode(MANIFEST_CODE)).willThrow(new NotFoundException(null));

        final Response response = facade.getManifestByCode(MANIFEST_CODE, STORE_NUMBER);

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);

        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_NOT_FOUND);

        verifyZeroInteractions(instoreManifestConverter);
    }


    @Test
    public void testGetManifestByCodeNull() throws NotFoundException {
        given(targetManifestService.getManifestByCode(MANIFEST_CODE)).willReturn(null);
        given(instoreManifestConverter.convert(null)).willThrow(new IllegalArgumentException());

        final Response response = facade.getManifestByCode(MANIFEST_CODE, STORE_NUMBER);

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);

        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);
    }

    @Test
    public void testTransmitManifestForStoreWithExceptionNoConsignments() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetManifestService.createManifest(storeModel))
                .willThrow(
                        new UnsupportedOperationException(
                                "Manifest Cannnot be created as there are no unmanifested consignments for the store."));

        final Response response = facade.transmitManifestForStore(STORE_NUMBER.intValue());

        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);
    }

    @Test
    public void testGetManifestByCode() throws NotFoundException {
        final Date currDate = Calendar.getInstance().getTime();
        final TargetManifestModel tempManifestModel = createManifestModel(currDate);

        given(targetManifestService.getManifestByCode(MANIFEST_CODE)).willReturn(tempManifestModel);
        given(instoreManifestConverter.convert(tempManifestModel)).willCallRealMethod();

        final Response response = facade.getManifestByCode(MANIFEST_CODE, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ManifestsResponseData.class);

        final ManifestsResponseData data = (ManifestsResponseData)response.getData();
        assertThat(data.getManifests().size() == 1);
        final Manifest manifest = data.getManifests().get(0);
        assertThat(MANIFEST_CODE).isEqualTo(manifest.getCode());
        assertThat(TargetDateUtil.getDateFormattedAsString(currDate)).isEqualTo(manifest.getDate());
    }

    @Test
    public void testGetStockLevelsForProductsInConsignment() throws TargetIntegrationException {
        given(targetStoreConsignmentService.getConsignmentByCode(CONSIGNMENT_NUMBER)).willReturn(
                consignmentModel);

        given(targetStoreStockService.getStockForProducts(varaintModel, STORE_NUMBER)).willReturn(
                stockDto);
        given(productStockLevelConverter.convert(stockDto, STORE_NUMBER.toString())).willCallRealMethod();
        final Response response = facade.getStockLevelsForProductsInConsignment(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ProductSohResponseData.class);

        final ProductSohResponseData data = (ProductSohResponseData)response.getData();
        assertThat(data.getProducts()).isNotNull();
        assertThat(data.getProducts()).hasSize(2);
        assertThat(data.getProducts()).onProperty("productCode").contains("11000011", "11000012");
        assertThat(data.getProducts()).onProperty("stockOnHand").contains("15", "25");
        verifyZeroInteractions(fluentStockLookupService);

    }


    @Test
    public void testGetStockLevelsForProductsInConsignmentUsingCache() throws TargetIntegrationException {

        given(Boolean.valueOf(
                featureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.USE_CACHE_STOCK_FOR_FULFIMENT)))
                        .willReturn(Boolean.TRUE);


        given(targetStoreConsignmentService.getConsignmentByCode(CONSIGNMENT_NUMBER)).willReturn(
                consignmentModel);

        final StockVisibilityItemLookupResponseDto stockVisResponseDto = mock(
                StockVisibilityItemLookupResponseDto.class);
        given(targetStoreStockService.lookupStockForItemsInStores(Arrays.asList(String.valueOf(STORE_NUMBER)),
                Arrays.asList("11000011", "11000012"))).willReturn(stockVisResponseDto);

        final List<ProductStockLevelInstore> resultProducts = new ArrayList<>();
        final ProductStockLevelInstore pdtResponse1 = mock(ProductStockLevelInstore.class);
        final ProductStockLevelInstore pdtResponse2 = mock(ProductStockLevelInstore.class);
        resultProducts.add(pdtResponse1);
        resultProducts.add(pdtResponse2);
        given(stockVisResponseToProductStockLevelInstoreConverter.convert(stockVisResponseDto,
                String.valueOf(STORE_NUMBER))).willReturn(resultProducts);

        final Response response = facade.getStockLevelsForProductsInConsignment(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ProductSohResponseData.class);

        final ProductSohResponseData data = (ProductSohResponseData)response.getData();
        assertThat(data.getProducts()).isNotNull();
        assertThat(data.getProducts()).hasSize(2);
        assertThat(data.getProducts()).containsExactly(pdtResponse1, pdtResponse2);
        verifyZeroInteractions(fluentStockLookupService);
    }

    @Test
    public void testGetStockLevelsForProductsInConsignmentUsingFluent()
            throws TargetIntegrationException, FluentClientException {

        willReturn(Boolean.TRUE).given(featureSwitchService).isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT);


        given(targetStoreConsignmentService.getConsignmentByCode(CONSIGNMENT_NUMBER)).willReturn(
                consignmentModel);

        final StockVisibilityItemLookupResponseDto stockVisResponseDto = mock(
                StockVisibilityItemLookupResponseDto.class);
        given(fluentStockLookupService.lookupStock(Arrays.asList("11000011", "11000012"),
                Arrays.asList(String.valueOf(STORE_NUMBER))))
                        .willReturn(stockVisResponseDto);

        final List<ProductStockLevelInstore> resultProducts = new ArrayList<>();
        final ProductStockLevelInstore pdtResponse1 = mock(ProductStockLevelInstore.class);
        final ProductStockLevelInstore pdtResponse2 = mock(ProductStockLevelInstore.class);
        resultProducts.add(pdtResponse1);
        resultProducts.add(pdtResponse2);
        given(stockVisResponseToProductStockLevelInstoreConverter.convert(stockVisResponseDto,
                String.valueOf(STORE_NUMBER))).willReturn(resultProducts);

        final Response response = facade.getStockLevelsForProductsInConsignment(CONSIGNMENT_NUMBER, STORE_NUMBER);

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ProductSohResponseData.class);

        final ProductSohResponseData data = (ProductSohResponseData)response.getData();
        assertThat(data.getProducts()).isNotNull();
        assertThat(data.getProducts()).hasSize(2);
        assertThat(data.getProducts()).containsExactly(pdtResponse1, pdtResponse2);
        verify(fluentStockLookupService).lookupStock(Arrays.asList("11000011", "11000012"),
                Arrays.asList(String.valueOf(STORE_NUMBER)));
        verify(stockVisResponseToProductStockLevelInstoreConverter).convert(stockVisResponseDto,
                String.valueOf(STORE_NUMBER));
        verifyZeroInteractions(targetStoreStockService);
        verifyZeroInteractions(productStockLevelConverter);
    }

    @Test
    public void testGetStockLevelsForProductsInConsignmentWithNoStock() throws TargetIntegrationException {
        given(targetStoreConsignmentService.getConsignmentByCode(CONSIGNMENT_NUMBER)).willReturn(
                consignmentModel);


        given(targetStoreStockService.getStockForProducts(varaintModel, STORE_NUMBER)).willReturn(
                null);
        given(productStockLevelConverter.convert(stockDto, STORE_NUMBER.toString())).willCallRealMethod();
        final Response response = facade.getStockLevelsForProductsInConsignment(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ProductSohResponseData.class);

        final ProductSohResponseData data = (ProductSohResponseData)response.getData();
        assertThat(data.getProducts()).isEmpty();
        verifyZeroInteractions(fluentStockLookupService);

    }

    @Test
    public void testGetManifestHistoryForStore() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        final Date currDate = Calendar.getInstance().getTime();
        final Date currDateMinusTwo = DateUtils.addDays(currDate, -2);

        final TargetManifestModel manifest1 = new TargetManifestModel();
        manifest1.setCode("manifest1");
        manifest1.setDate(currDate);

        final TargetManifestModel manifest2 = new TargetManifestModel();
        manifest2.setCode("manifest2");
        manifest2.setDate(currDateMinusTwo);

        given(targetManifestService.getManifestsHistoryForStore(storeModel)).willReturn(
                Arrays.asList(manifest1, manifest2));
        given(instoreBasicManifestConverter.convert(manifest1)).willCallRealMethod();
        given(instoreBasicManifestConverter.convert(manifest2)).willCallRealMethod();

        final Response response = facade.getManifestHistoryForStore(STORE_NUMBER.intValue());

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getData()).isInstanceOf(ManifestsResponseData.class);

        final ManifestsResponseData data = (ManifestsResponseData)response.getData();
        assertThat(data.getManifests()).isNotNull().hasSize(2);
        assertThat(data.getManifests().get(0).getCode()).isEqualTo("manifest1");
        assertThat(data.getManifests().get(0).getDate()).isEqualTo(
                TargetDateUtil.getDateFormattedAsString(currDate));
        assertThat(data.getManifests().get(1).getCode()).isEqualTo("manifest2");
        assertThat(data.getManifests().get(1).getDate()).isEqualTo(
                TargetDateUtil.getDateFormattedAsString(currDateMinusTwo));
    }

    @Test
    public void testGetManifestHistoryForStoreWithWrongStoreNumber() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willThrow(
                new TargetUnknownIdentifierException("Wrong store number"));

        final Response response = facade.getManifestHistoryForStore(STORE_NUMBER.intValue());

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);
        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);

        verifyZeroInteractions(targetManifestService);
        verifyZeroInteractions(instoreManifestConverter);
    }

    @Test
    public void testGetManifestHistoryForStoreWithMutipleStoresForStoreNumber()
            throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER)).willThrow(
                new TargetAmbiguousIdentifierException("Multiple POS for this store number"));

        final Response response = facade.getManifestHistoryForStore(STORE_NUMBER.intValue());

        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);
        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);

        verifyZeroInteractions(targetManifestService);
        verifyZeroInteractions(instoreManifestConverter);
    }

    @Test
    public void testGetManifestHistoryForStoreWithException() throws TargetUnknownIdentifierException,
            TargetAmbiguousIdentifierException {
        given(targetPointOfServiceService.getPOSByStoreNumber(STORE_NUMBER))
                .willThrow(
                        new RuntimeException("TEST"));

        final Response response = facade.getManifestHistoryForStore(STORE_NUMBER.intValue());
        assertThat(response).isNotNull();
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getData()).isInstanceOf(BaseResponseData.class);
        final BaseResponseData data = response.getData();
        assertThat(data.getError()).isNotNull();
        assertThat(data.getError().getCode()).isEqualTo(OfcErrorConstants.ERR_SYSTEM);

        verifyZeroInteractions(targetManifestService);
        verifyZeroInteractions(instoreManifestConverter);
    }

    @Test
    public void testGetLabelLayoutNull() {

        final LabelLayout layout = facade.getLabelLayout(null);
        assertThat(layout).isNotNull().isEqualTo(LabelLayout.getDefaultLayout());
    }

    @Test
    public void testGetLabelLayoutUnknown() {
        given(storeFulFilmentCapabilityModel.getEParcelPrintLabelLayout()).willReturn("bklargh");
        final LabelLayout layout = facade.getLabelLayout(storeModel);
        assertThat(layout).isNotNull().isEqualTo(LabelLayout.getDefaultLayout());
    }

    @Test
    public void testGetLabelLayoutKnown() {
        given(storeFulFilmentCapabilityModel.getEParcelPrintLabelLayout()).willReturn("a42pp");
        final LabelLayout layout = facade.getLabelLayout(storeModel);
        assertThat(layout).isNotNull().isEqualTo(LabelLayout.a42pp);
    }

    @Test
    public void testGetLabelLayoutKnownAnyCaseAndTrim() {
        given(storeFulFilmentCapabilityModel.getEParcelPrintLabelLayout()).willReturn("A42PP");
        final LabelLayout layout = facade.getLabelLayout(storeModel);
        assertThat(layout).isNotNull().isEqualTo(LabelLayout.a42pp);
    }

    @Test
    public void testGetLabelLayoutWithThermalA6() {
        given(storeFulFilmentCapabilityModel.getEParcelPrintLabelLayout()).willReturn("a61pp");
        final LabelLayout layout = facade.getLabelLayout(storeModel);
        assertThat(layout).isNotNull().isEqualTo(LabelLayout.a61pp);
    }


    @Test
    public void testDoBrandingNull() {

        // Should default to true
        final boolean doBranding = facade.getDoBranding(null);
        assertThat(doBranding).isTrue();
    }

    @Test
    public void testDoBrandingFalse() {

        final boolean doBranding = facade.getDoBranding(Boolean.FALSE);
        assertThat(doBranding).isFalse();
    }

    @Test
    public void testDoBrandingTrue() {

        final boolean doBranding = facade.getDoBranding(Boolean.TRUE);
        assertThat(doBranding).isTrue();
    }

    @Test
    public void testGetValidatedConsignmentForLabelWithConsignmentNull() {

        given(targetStoreConsignmentService.getConsignmentByCode(CONSIGNMENT_NUMBER)).willReturn(
                consignmentModel);
        final TargetConsignmentModel con = facade.getValidatedConsignmentForLabel(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(con).isNull();
    }

    @Test
    public void testGetValidatedConsignmentForLabelWithConsignmentException() {

        given(targetStoreConsignmentService.getConsignmentByCode(CONSIGNMENT_NUMBER)).willThrow(
                new RuntimeException("Exception getting consignment"));
        final TargetConsignmentModel con = facade.getValidatedConsignmentForLabel(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(con).isNull();
    }

    @Test
    public void testGetValidatedConsignmentForLabelWithConsignmentStatusNull() {

        given(consignmentModel.getStatus()).willReturn(null);
        final TargetConsignmentModel con = facade.getValidatedConsignmentForLabel(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(con).isNull();
    }

    @Test
    public void testGetValidatedConsignmentForLabelWithConsignmentStatusInvalid() {

        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PICKED);
        final TargetConsignmentModel con = facade.getValidatedConsignmentForLabel(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(con).isNull();
    }

    @Test
    public void testGetValidatedConsignmentForLabelWithSuccess() {

        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PACKED);
        final TargetConsignmentModel con = facade.getValidatedConsignmentForLabel(CONSIGNMENT_NUMBER, STORE_NUMBER);
        assertThat(con).isNotNull().isEqualTo(consignmentModel);
    }



    @Test
    public void testConsignmentDispatchLabelSuccessResponse() {

        given(
                dispatchLabelService.getDispatchLabel(storeModel, consignmentModel, LabelLayout.getDefaultLayout(),
                        true))
                                .willReturn(createSuccessLabelResponse());

        // Consignment should be in packed status
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PACKED);

        final byte[] result = facade.getConsignmentDispatchLabel(CONSIGNMENT_NUMBER, STORE_NUMBER, null, null);
        assertThat(result).isNotNull();
        assertThat(new String(result)).isEqualTo("abc");
    }

    @Test
    public void testConsignmentDispatchLabelErrorResponse() {

        given(
                dispatchLabelService.getDispatchLabel(storeModel, consignmentModel, LabelLayout.getDefaultLayout(),
                        true))
                                .willReturn(createErrorLabelResponse());

        // Consignment should be in packed status
        given(consignmentModel.getStatus()).willReturn(ConsignmentStatus.PACKED);

        final byte[] result = facade.getConsignmentDispatchLabel(CONSIGNMENT_NUMBER, STORE_NUMBER, null, null);
        assertThat(result).isNull();
    }

    @Test
    public void testUpdateShippedQty() {
        final Consignment consignment = mock(Consignment.class);
        final TargetConsignmentModel mockConsignmentModel = mock(TargetConsignmentModel.class);
        given(consignment.getCode()).willReturn("code");
        given(targetStoreConsignmentService
                .getConsignmentByCode(consignment.getCode())).willReturn(mockConsignmentModel);
        final Response response = facade.updateShippedQty(consignment, STORE_NUMBER);
        assertThat(response.isSuccess()).isTrue();
    }

    @Test
    public void testUpdateShippedQtyWithException() {
        final Consignment consignment = mock(Consignment.class);
        given(consignment.getCode()).willReturn("code");
        given(targetStoreConsignmentService
                .getConsignmentByCode(consignment.getCode())).willThrow(new IllegalArgumentException());
        final Response response = facade.updateShippedQty(consignment, STORE_NUMBER);
        assertThat(response.isSuccess()).isFalse();
    }

    private LabelResponseDTO createSuccessLabelResponse() {

        final LabelResponseDTO response = new LabelResponseDTO();
        response.setSuccess(true);

        final byte[] testData = "abc".getBytes();
        response.setData(testData);
        return response;
    }

    private LabelResponseDTO createErrorLabelResponse() {

        final LabelResponseDTO response = new LabelResponseDTO();
        response.setSuccess(false);
        response.setErrorCode("ERROR");

        return response;
    }


    /**
     * @param consignmentEntries
     * @param position
     *            in the array
     * @param department
     *            expected department
     */
    private void verifyConsignmentEntry(final List<ConsignmentEntry> consignmentEntries, final int position,
            final String department) {
        final ConsignmentEntry consignmentEntry = consignmentEntries.get(position);
        final Product product = consignmentEntry.getProduct();
        if (product != null) {
            assertThat(product.getDepartment()).isEqualTo(department);
        }
        else {
            assertThat(consignmentEntry).isNotNull();
        }
    }

    /**
     * @param departmentNames
     *            String array of department names
     * @return list of COnsignments
     */
    private List<ConsignmentEntry> setupConsignmentEntries(final String... departmentNames) {
        final List<ConsignmentEntry> entries = new ArrayList<>();
        for (final String department : departmentNames) {
            final ConsignmentEntry entry = new ConsignmentEntry();
            final Product p = new Product();
            p.setDepartment(department);
            entry.setProduct(p);
            entries.add(entry);
        }
        return entries;
    }

    private void verifyUserNameDetails(final User user) {

        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo(USER_NAME);
        assertThat(user.getFirstName()).isEqualTo(USER_DISPLAY_NAME);
        assertThat(user.getFirstName()).isEqualTo(USER_DISPLAY_NAME);
    }

    private void verifyStore(final Store store) {

        assertThat(store).isNotNull();
        assertThat(store.getStoreName()).isEqualTo(STORE_NAME);
        assertThat(store.getStoreNumber()).isEqualTo(STORE_NUMBER);
        assertThat(store.isBigAndBulky()).isFalse();
    }

    private void verifyStoreBigAndBulky(final Store store) {

        assertThat(store).isNotNull();
        assertThat(store.getStoreName()).isEqualTo(STORE_NAME);
        assertThat(store.getStoreNumber()).isEqualTo(STORE_NUMBER);
        assertThat(store.isBigAndBulky()).isTrue();
    }

    private void createConsignmentStatsStatusMap() {

        consignmentStatusMapper.setConsignmentStatusMap(ConsignmentStatusMapperTest.createConsignmentStatsStatusMap());
    }

    private TargetManifestModel createManifestModel(final Date date) {
        final TargetManifestModel tempManifestModel = new TargetManifestModel();
        tempManifestModel.setCode(MANIFEST_CODE);
        tempManifestModel.setDate(date);
        return tempManifestModel;
    }


}

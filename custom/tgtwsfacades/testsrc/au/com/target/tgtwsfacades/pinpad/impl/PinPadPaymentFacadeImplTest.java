/**
 * 
 */
package au.com.target.tgtwsfacades.pinpad.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;

import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtpayment.model.PinPadPaymentInfoModel;
import au.com.target.tgtshareddto.eftwrapper.dto.EftPaymentDto;
import au.com.target.tgtshareddto.eftwrapper.dto.HybrisResponseDto;
import au.com.target.tgtshareddto.eftwrapper.dto.PaymentDetailsDto;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
public class PinPadPaymentFacadeImplTest {

    @Mock
    private TargetOrderService targetOrderService;

    @Mock
    private ModelService modelService;

    private PinPadPaymentFacadeImpl pinpadFacade = null;

    @Before
    public void setUp() {
        pinpadFacade = new PinPadPaymentFacadeImpl();
        targetOrderService = Mockito.mock(TargetOrderService.class);
        modelService = Mockito.mock(ModelService.class);
        pinpadFacade.setModelService(modelService);
        pinpadFacade.setTargetOrderService(targetOrderService);
    }

    @Test
    public void testFindOrderAndCartUnknownIdentifierException() {

        // Neither Order nor cart found
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenThrow(
                new UnknownIdentifierException("Order not found"));
        BDDMockito.when(targetOrderService.findCartModelForOrderId(Mockito.anyString())).thenThrow(
                new UnknownIdentifierException("Cart not found"));

        doUpdateWithExpectedError(
                "ERROR Message: [ Error updating PinPad payment: Order or Cart could not be found ], ERROR Code: [ ERR_PINPADPAYMENT ], OrderCode:[ 123456 ]");

        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verify(targetOrderService).findCartModelForOrderId(Mockito.anyString());
        Mockito.verifyZeroInteractions(modelService);
    }

    private void doUpdateWithExpectedError(final String expectedError) {

        final EftPaymentDto eftpaymentdto = buildEftPaymentDto("123456", true);
        final HybrisResponseDto response = pinpadFacade.updatePinPadPayment(eftpaymentdto);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccessStatus()).isFalse();
        Assertions.assertThat(response.isRecoverableError()).isFalse();
        Assertions
                .assertThat(response.getMessages())
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .containsOnly(expectedError);

    }

    @Test
    public void testFindOrderAmbigousIdentifierException() {
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenThrow(
                new AmbiguousIdentifierException("Multiple orders found"));
        BDDMockito.when(targetOrderService.findCartModelForOrderId(Mockito.anyString())).thenThrow(
                new AmbiguousIdentifierException("Multiple carts found"));

        doUpdateWithExpectedError(
                "ERROR Message: [ Error updating PinPad payment: Multiple Orders found ], ERROR Code: [ ERR_PINPADPAYMENT ], OrderCode:[ 123456 ]");

        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(targetOrderService);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testFindCartAmbigousIdentifierException() {
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenThrow(
                new UnknownIdentifierException("Order not found"));
        BDDMockito.when(targetOrderService.findCartModelForOrderId(Mockito.anyString())).thenThrow(
                new AmbiguousIdentifierException("Multiple carts found"));

        doUpdateWithExpectedError(
                "ERROR Message: [ Error updating PinPad payment: Multiple Carts found ], ERROR Code: [ ERR_PINPADPAYMENT ], OrderCode:[ 123456 ]");

        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verify(targetOrderService).findCartModelForOrderId(Mockito.anyString());
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testFindOrderAndCartNull() {
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenReturn(null);
        BDDMockito.when(targetOrderService.findCartModelForOrderId(Mockito.anyString())).thenReturn(null);

        doUpdateWithExpectedError(
                "ERROR Message: [ Error updating PinPad payment: Order or Cart could not be found ], ERROR Code: [ ERR_PINPADPAYMENT ], OrderCode:[ 123456 ]");

        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verify(targetOrderService).findCartModelForOrderId(Mockito.anyString());
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOrderNoPaymentTransactions() {
        final OrderModel order = buildOrder();
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenReturn(order);
        doUpdateWithExpectedError(
                "ERROR Message: [ Error updating PinPad payment: No transactions found ], ERROR Code: [ ERR_PINPADPAYMENT ], OrderCode:[ 123456 ]");
        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(targetOrderService);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOrderPaymentTransactionNonPinpadInfo() {

        final OrderModel order = buildOrder();
        final List<PaymentTransactionModel> transactions = addTransactions(false, TransactionStatus.REVIEW);
        transactions.get(0).setInfo(new PaymentInfoModel());
        order.setPaymentTransactions(transactions);
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenReturn(order);

        doUpdateWithExpectedError(
                "ERROR Message: [ Error updating PinPad payment: The payment info is not of the PinPadPaymentInfo type ], ERROR Code: [ ERR_PINPADPAYMENT ], OrderCode:[ 123456 ]");

        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(targetOrderService);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOrderPaymentTransactionAndPinpadInfo() {

        final OrderModel order = buildOrder();
        order.setPaymentTransactions(addTransactions(true, TransactionStatus.REVIEW));
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenReturn(order);

        doSuccessfulUpdate();

        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verify(modelService).saveAll(Mockito.any(PaymentInfoModel.class),
                Mockito.any(PaymentTransactionEntryModel.class));
        Mockito.verifyNoMoreInteractions(targetOrderService);
        Mockito.verifyNoMoreInteractions(modelService);
    }

    private void doSuccessfulUpdate() {

        final String orderId = "123456";
        final EftPaymentDto eftpaymentdto = buildEftPaymentDto(orderId, true);

        final HybrisResponseDto response = pinpadFacade.updatePinPadPayment(eftpaymentdto);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccessStatus()).isTrue();
        Assertions.assertThat(response.getMessages()).isEmpty();
    }

    @Test
    public void testOrderPaymentTransactionInOrderWithSuccess() {

        final OrderModel order = buildOrder();
        order.setPaymentTransactions(addTransactions(true, TransactionStatus.ACCEPTED));
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenReturn(order);

        doSuccessfulUpdate();

        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(targetOrderService);
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOrderPaymentTransactionInCartWithSuccess() {

        final CartModel cart = buildCart();
        cart.setPaymentTransactions(addTransactions(true, TransactionStatus.ACCEPTED));
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenReturn(null);
        BDDMockito.when(targetOrderService.findCartModelForOrderId(Mockito.anyString())).thenReturn(cart);

        doSuccessfulUpdate();

        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verify(targetOrderService).findCartModelForOrderId(Mockito.anyString());
        Mockito.verifyZeroInteractions(modelService);
    }

    @Test
    public void testOrderPaymentTransactionWithAuthCodeNull() {
        final String orderId = "123456";
        final EftPaymentDto eftpaymentdto = buildEftPaymentDto(orderId, true);
        eftpaymentdto.getPaymentDetails().setAuthCode(null);
        final OrderModel order = buildOrder();
        order.setPaymentTransactions(addTransactions(true, TransactionStatus.ACCEPTED));
        BDDMockito.when(targetOrderService.findOrderModelForOrderId(Mockito.anyString())).thenReturn(order);
        final HybrisResponseDto response = pinpadFacade.updatePinPadPayment(eftpaymentdto);
        Assertions.assertThat(response).isNotNull();
        Assertions.assertThat(response.isSuccessStatus()).isTrue();
        Assertions.assertThat(response.getMessages()).isEmpty();
        Mockito.verify(targetOrderService).findOrderModelForOrderId(Mockito.anyString());
        Mockito.verifyNoMoreInteractions(targetOrderService);
        Mockito.verifyZeroInteractions(modelService);
    }

    /**
     * @param hasPinpadInfo
     * @return List of all paymentTransactionModels
     */
    private List<PaymentTransactionModel> addTransactions(final boolean hasPinpadInfo,
            final TransactionStatus... status) {
        final List<PaymentTransactionModel> transactions = new ArrayList<>();
        for (final TransactionStatus sstatus : status) {
            final PaymentTransactionModel transactionModel = new PaymentTransactionModel();
            final PaymentTransactionEntryModel entry = new PaymentTransactionEntryModel();
            entry.setTransactionStatus(sstatus.name());
            entry.setTime(new Date());
            entry.setType(PaymentTransactionType.CAPTURE);
            if (hasPinpadInfo) {
                final PinPadPaymentInfoModel info = new PinPadPaymentInfoModel();
                transactionModel.setInfo(info);
            }
            entry.setPaymentTransaction(transactionModel);
            transactionModel.setEntries(Collections.singletonList(entry));
            transactions.add(transactionModel);
        }

        return transactions;
    }

    private OrderModel buildOrder() {
        final OrderModel order = new OrderModel();
        order.setCode("123456");
        return order;
    }

    private CartModel buildCart() {
        final CartModel cart = new CartModel();
        cart.setCode("123456");
        return cart;
    }


    /**
     * @param orderId
     * @param success
     * @return EftPaymentDto
     */
    private EftPaymentDto buildEftPaymentDto(final String orderId, final boolean success) {
        final EftPaymentDto dto = new EftPaymentDto();
        dto.setOrderId(orderId);
        final PaymentDetailsDto paymentDetails = new PaymentDetailsDto();
        paymentDetails.setAuthCode("098987345");
        paymentDetails.setJournalRoll("This is the journal Roll");
        paymentDetails.setPan("5424234324234324");
        paymentDetails.setRrn("234");
        paymentDetails.setRespAscii("0");
        paymentDetails.setTransactionSuccess(success);
        paymentDetails.setCardType("5");
        paymentDetails.setCardName("visa");
        paymentDetails.setAccountType("2");
        paymentDetails.setJournal("This is the journal");
        dto.setPaymentDetails(paymentDetails);
        return dto;
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.pos.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.price.TargetPreviousPermanentPriceService;
import au.com.target.tgtcore.product.TargetProductPriceService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtsale.integration.dto.IntegrationPosProductItemDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * @author cbi
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetPosPriceImportIntegrationFacadeTest {

    private static final String NO_CATALOGS = "ERR-PRICEUPDATE-CATMISSING : No catalogs were found";

    @Mock
    private TargetProductPriceService targetProductPriceService;
    @Mock
    private TargetPreviousPermanentPriceService targetPreviousPermanentPriceService;
    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;
    @Mock
    private TargetProductService targetProductService;
    @Mock
    private CatalogVersionService catalogVersionService;
    @Mock
    private FluentSkuUpsertService fluentSkuUpsertService;

    @Captor
    private ArgumentCaptor<List<String>> productCodes;

    @Captor
    private ArgumentCaptor<List<CatalogVersionModel>> catalogVersionModels;

    private CatalogVersionModel mockStagedCatalogVersion;


    @InjectMocks
    @Spy
    private final TargetPosPriceImportIntegrationFacade targetPosPriceImportIntegrationFacade = new TargetPosPriceImportIntegrationFacade();

    @Before
    public void setUp() {
        mockStagedCatalogVersion = mock(CatalogVersionModel.class);
        given(
                catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.OFFLINE_VERSION)).willReturn(mockStagedCatalogVersion);
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.TRUE);
        given(targetProductPriceService.getCatalogs()).willReturn(Arrays.asList(mockStagedCatalogVersion));
    }


    @Test
    public void testImportProductPriceFromPosWhenNoCategory() {
        given(targetProductPriceService.getCatalogs()).willReturn(null);
        final IntegrationPosProductItemDto mockDto = mock(IntegrationPosProductItemDto.class);
        final List<IntegrationPosProductItemDto> dtos = new ArrayList<IntegrationPosProductItemDto>();
        dtos.add(mockDto);
        final IntegrationResponseDto response = targetPosPriceImportIntegrationFacade.importProductPriceFromPos(dtos);
        assertThat(response.getMessages()).isNotEmpty();

        assertThat(response.getMessages().get(0)).isEqualTo(NO_CATALOGS);
        assertThat(response.isSuccessStatus()).isFalse();
    }

    @Test
    public void testBannedProductsForSaleWithFeatureSwitchForFluentOff() {
        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TgtCoreConstants.FeatureSwitch.FLUENT)))
                .willReturn(Boolean.FALSE);
        final IntegrationPosProductItemDto mockDto = mock(IntegrationPosProductItemDto.class);
        willReturn(String.valueOf("PGC1005_skypeblue_25")).given(mockDto).getItemCode();
        willReturn(Integer.valueOf(1)).given(mockDto).getGstRate();
        willReturn(Double.valueOf(20)).given(mockDto).getCurrentSalePrice();
        willReturn(Double.valueOf(20)).given(mockDto).getCurrentSalePrice();
        willReturn(Boolean.TRUE).given(mockDto).isRemoveFromSale();
        willReturn(Boolean.FALSE).given(mockDto).isPromoEvent();

        final TargetSizeVariantProductModel targetSizeVariantProductModel = mock(TargetSizeVariantProductModel.class);
        given(targetProductService.getProductForCode(mockStagedCatalogVersion, "PGC1005_skypeblue_25")).willReturn(
                targetSizeVariantProductModel);

        final List<IntegrationPosProductItemDto> dtos = new ArrayList<IntegrationPosProductItemDto>();
        dtos.add(mockDto);

        targetPosPriceImportIntegrationFacade.importProductPriceFromPos(dtos);
        verify(targetPreviousPermanentPriceService).updatePreviousPermanentPriceEndDate("PGC1005_skypeblue_25");
        verify(targetProductPriceService).removeProductFromSale(productCodes.capture(),
                catalogVersionModels.capture());
        assertThat(productCodes).isNotNull();
        assertThat(productCodes.getValue()).isNotEmpty();
        assertThat(productCodes.getValue().get(0)).isEqualTo("PGC1005_skypeblue_25");
        assertThat(catalogVersionModels).isNotNull();
        assertThat(catalogVersionModels.getValue()).isNotEmpty();
        assertThat(catalogVersionModels.getValue().get(0)).isEqualTo(mockStagedCatalogVersion);
        verify(targetProductPriceService, never()).updateProductPrices(anyList());
        verify(fluentSkuUpsertService, never()).upsertSku(Mockito.any(AbstractTargetVariantProductModel.class));

    }

    @Test
    public void testBannedProductsForSaleWithFeatureSwitchForFluentON() {
        final IntegrationPosProductItemDto mockDto = mock(IntegrationPosProductItemDto.class);
        willReturn(String.valueOf("PGC1005_skypeblue_25")).given(mockDto).getItemCode();
        willReturn(Integer.valueOf(1)).given(mockDto).getGstRate();
        willReturn(Double.valueOf(20)).given(mockDto).getCurrentSalePrice();
        willReturn(Double.valueOf(20)).given(mockDto).getCurrentSalePrice();
        willReturn(Boolean.TRUE).given(mockDto).isRemoveFromSale();
        willReturn(Boolean.FALSE).given(mockDto).isPromoEvent();

        final TargetSizeVariantProductModel targetSizeVariantProductModel = mock(TargetSizeVariantProductModel.class);
        given(targetProductService.getProductForCode(mockStagedCatalogVersion, "PGC1005_skypeblue_25")).willReturn(
                targetSizeVariantProductModel);

        final List<IntegrationPosProductItemDto> dtos = new ArrayList<IntegrationPosProductItemDto>();
        dtos.add(mockDto);

        targetPosPriceImportIntegrationFacade.importProductPriceFromPos(dtos);
        verify(targetPreviousPermanentPriceService).updatePreviousPermanentPriceEndDate("PGC1005_skypeblue_25");
        verify(targetProductPriceService).removeProductFromSale(productCodes.capture(),
                catalogVersionModels.capture());
        assertThat(productCodes).isNotNull();
        assertThat(productCodes.getValue()).isNotEmpty();
        assertThat(productCodes.getValue().get(0)).isEqualTo("PGC1005_skypeblue_25");
        assertThat(catalogVersionModels).isNotNull();
        assertThat(catalogVersionModels.getValue()).isNotEmpty();
        assertThat(catalogVersionModels.getValue().get(0)).isEqualTo(mockStagedCatalogVersion);
        verify(targetProductPriceService, never()).updateProductPrices(anyList());
        verify(fluentSkuUpsertService).upsertSku(targetSizeVariantProductModel);

    }

    @Test
    public void testBannedProductsForSaleWhenErrorOccursWhileRemovingProductsFromSale() {
        final IntegrationPosProductItemDto mockDto = mock(IntegrationPosProductItemDto.class);
        willReturn(String.valueOf("PGC1005_skypeblue_25")).given(mockDto).getItemCode();
        willReturn(Integer.valueOf(1)).given(mockDto).getGstRate();
        willReturn(Double.valueOf(20)).given(mockDto).getCurrentSalePrice();
        willReturn(Double.valueOf(20)).given(mockDto).getCurrentSalePrice();
        willReturn(Boolean.TRUE).given(mockDto).isRemoveFromSale();
        willReturn(Boolean.FALSE).given(mockDto).isPromoEvent();

        final List<String> errorMessageWhileRemovingProductFromSale = Arrays.asList("Error: Removing Pdt From sale");
        given(targetProductPriceService.removeProductFromSale(anyList(), anyList())).willReturn(
                errorMessageWhileRemovingProductFromSale);

        final List<IntegrationPosProductItemDto> dtos = new ArrayList<IntegrationPosProductItemDto>();
        dtos.add(mockDto);


        final IntegrationResponseDto response = targetPosPriceImportIntegrationFacade.importProductPriceFromPos(dtos);
        assertThat(response.getMessages()).isNotEmpty();

        assertThat(response.getMessages().get(0)).isEqualTo("Error: Removing Pdt From sale");
        assertThat(response.isSuccessStatus()).isFalse();


    }
}

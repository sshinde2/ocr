/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.category.TargetCategoryService;
import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.featureswitch.service.TargetFeatureSwitchService;
import au.com.target.tgtcore.model.TargetProductCategoryModel;
import au.com.target.tgtfluent.service.FluentCategoryUpsertService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationPrincipalDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSupercategoryDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationTargetProductCategoryDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;


/**
 * @author rmcalave
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetCategoryImportIntegrationFacadeTest {
    @Mock
    private TargetCategoryService mockTargetCategoryService;

    @Mock
    private ModelService mockModelService;

    @Mock
    private UserService mockUserService;

    @Mock
    private CatalogVersionService mockCatalogVersionService;

    @Mock
    private TargetFeatureSwitchService targetFeatureSwitchService;

    @Mock
    private FluentCategoryUpsertService fluentCategoryUpsertService;

    @InjectMocks
    private final TargetCategoryImportIntegrationFacade targetCategoryImportIntegrationFacade = new TargetCategoryImportIntegrationFacade();

    @Before
    public void setUp() {
        final CatalogVersionModel mockCatalogVersion = mock(CatalogVersionModel.class);
        given(
                mockCatalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                        TgtCoreConstants.Catalog.OFFLINE_VERSION)).willReturn(mockCatalogVersion);

        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TargetProductImportConstants.FEATURE_FLUENT)))
                .willReturn(Boolean.TRUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPersistTargetCategoryWithNullDto() {
        targetCategoryImportIntegrationFacade.persistTargetCategory(null);
    }

    @Test
    public void testPersistTargetCategoryWithNullCategoryCode() {
        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        assertThat(response.isSuccessStatus()).isFalse();

        final List<String> messages = response.getMessages();
        assertThat(messages).hasSize(1);
        assertThat(response.getMessages().get(0))
                .isEqualTo("One of required values is missing [Category Code, Category Name]");

    }

    @Test
    public void testPersistTargetCategoryWithBlankCategoryCode() {
        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn("");

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        assertThat(response.isSuccessStatus()).isFalse();

        final List<String> messages = response.getMessages();
        assertThat(messages).hasSize(1);
        assertThat(response.getMessages().get(0))
                .isEqualTo("One of required values is missing [Category Code, Category Name]");
    }

    @Test
    public void testPersistTargetCategoryWithNullCategoryName() {
        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn("W12345");

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        assertThat(response.isSuccessStatus()).isFalse();

        final List<String> messages = response.getMessages();
        assertThat(messages).hasSize(1);
        assertThat(response.getMessages().get(0))
                .isEqualTo("One of required values is missing [Category Code, Category Name]");
    }

    @Test
    public void testPersistTargetCategoryWithBlankCategoryName() {
        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn("W12345");
        given(mockDto.getName()).willReturn("");

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        assertThat(response.isSuccessStatus()).isFalse();

        final List<String> messages = response.getMessages();
        assertThat(messages).hasSize(1);
        assertThat(response.getMessages().get(0))
                .isEqualTo("One of required values is missing [Category Code, Category Name]");
    }

    @Test
    public void testPersistTargetCategoryWithAmbiguousCategory() {
        final String categoryCode = "W12345";

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn("Department name");

        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willThrow(
                new AmbiguousIdentifierException("Too many categories!"));

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockTargetCategoryService).getCategoryForCode(categoryCode);

        assertThat(response.isSuccessStatus()).isFalse();

        final List<String> messages = response.getMessages();
        assertThat(messages).hasSize(1);
        assertThat(response.getMessages().get(0))
                .isEqualTo("Could not update category. More than one category with code " + categoryCode
                        + " already exists.");
    }

    @Test
    public void testPersistTargetCategoryWithNonExistentCategory() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";
        final String principalUid = "customergroup";
        final String supercategoryCode = "W33333";

        final IntegrationPrincipalDto principal = new IntegrationPrincipalDto();
        principal.setUid(principalUid);
        final List<IntegrationPrincipalDto> allowedPrincipalUids = Collections.singletonList(principal);

        final IntegrationSupercategoryDto supercategory = new IntegrationSupercategoryDto();
        supercategory.setCode(supercategoryCode);
        final List<IntegrationSupercategoryDto> supercategoryCodes = Collections.singletonList(supercategory);

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);
        given(mockDto.getAllowedPrincipals()).willReturn(allowedPrincipalUids);
        given(mockDto.getSupercategories()).willReturn(supercategoryCodes);

        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willThrow(
                new UnknownIdentifierException("No category!"));

        final UserGroupModel mockUserGroup = mock(UserGroupModel.class);
        given(mockUserService.getUserGroupForUID(principalUid)).willReturn(mockUserGroup);

        final TargetProductCategoryModel mockSupercategory = mock(TargetProductCategoryModel.class);
        given(mockTargetCategoryService.getCategoryForCode(supercategoryCode)).willReturn(mockSupercategory);

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getAllowedPrincipals()).willReturn(null);
        given(mockCategory.getSupercategories()).willReturn(null);
        given(mockModelService.create(TargetProductCategoryModel.class)).willReturn(mockCategory);

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockCategory).setCode(categoryCode);
        verify(mockCategory).setName(categoryName);

        final ArgumentCaptor<List> allowedPrincipalsCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setAllowedPrincipals(allowedPrincipalsCaptor.capture());

        final List allowedPrincipals = allowedPrincipalsCaptor.getValue();
        assertThat(allowedPrincipals).hasSize(1);
        assertThat(allowedPrincipals).contains(mockUserGroup);

        final ArgumentCaptor<List> supercategoriesCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setSupercategories(supercategoriesCaptor.capture());

        final List supercategories = supercategoriesCaptor.getValue();
        assertThat(supercategories).hasSize(1);
        assertThat(supercategories).contains(mockSupercategory);

        verify(mockModelService).save(mockCategory);
        verify(fluentCategoryUpsertService).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isTrue();
        assertThat(response.getMessages()).isEmpty();
    }

    @Test
    public void testPersistTargetCategoryWithNonExistentCategoryNoPrincipalsNoSupercategories() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);

        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willThrow(
                new UnknownIdentifierException("No category!"));

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getAllowedPrincipals()).willReturn(null);
        given(mockCategory.getSupercategories()).willReturn(null);
        given(mockModelService.create(TargetProductCategoryModel.class)).willReturn(mockCategory);

        final TargetProductCategoryModel mockAllProductsCategory = mock(TargetProductCategoryModel.class);
        given(mockTargetCategoryService.getCategoryForCode("AP01")).willReturn(mockAllProductsCategory);

        final ArgumentCaptor<List> supercategoryCaptor = ArgumentCaptor.forClass(List.class);

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockCategory).setCode(categoryCode);
        verify(mockCategory).setName(categoryName);
        verify(mockCategory, never()).setAllowedPrincipals(anyList());
        verify(mockCategory).setSupercategories(supercategoryCaptor.capture());

        verify(mockModelService).save(mockCategory);
        verify(fluentCategoryUpsertService).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isTrue();
        assertThat(response.getMessages()).isEmpty();

        final List<TargetProductCategoryModel> categories = supercategoryCaptor.getValue();
        assertThat(categories).hasSize(1);
        assertThat(categories).contains(mockAllProductsCategory);
    }

    @Test
    public void testPersistTargetCategoryWithExistingCategory() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";
        final String principalUid = "customergroup";
        final String supercategoryCode = "W33333";

        final IntegrationPrincipalDto principal = new IntegrationPrincipalDto();
        principal.setUid(principalUid);
        final List<IntegrationPrincipalDto> allowedPrincipalUids = Collections.singletonList(principal);

        final IntegrationSupercategoryDto supercategory = new IntegrationSupercategoryDto();
        supercategory.setCode(supercategoryCode);
        final List<IntegrationSupercategoryDto> supercategoryCodes = Collections.singletonList(supercategory);

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);
        given(mockDto.getAllowedPrincipals()).willReturn(allowedPrincipalUids);
        given(mockDto.getSupercategories()).willReturn(supercategoryCodes);

        final PrincipalModel mockExistingPrincipal = mock(PrincipalModel.class);
        final List<PrincipalModel> existingAllowedPrincipals = Collections.singletonList(mockExistingPrincipal);

        final CategoryModel mockExistingSupercategory = mock(CategoryModel.class);
        final List<CategoryModel> existingSupercategories = Collections.singletonList(mockExistingSupercategory);

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getCode()).willReturn(categoryCode);
        given(mockCategory.getName()).willReturn("Old category name");
        given(mockCategory.getAllowedPrincipals()).willReturn(existingAllowedPrincipals);
        given(mockCategory.getSupercategories()).willReturn(existingSupercategories);
        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willReturn(mockCategory);

        final UserGroupModel mockUserGroup = mock(UserGroupModel.class);
        given(mockUserService.getUserGroupForUID(principalUid)).willReturn(mockUserGroup);

        final TargetProductCategoryModel mockSupercategory = mock(TargetProductCategoryModel.class);
        given(mockTargetCategoryService.getCategoryForCode(supercategoryCode)).willReturn(mockSupercategory);

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockCategory).setName(categoryName);

        final ArgumentCaptor<List> allowedPrincipalsCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setAllowedPrincipals(allowedPrincipalsCaptor.capture());

        final List allowedPrincipals = allowedPrincipalsCaptor.getValue();
        assertThat(allowedPrincipals).hasSize(2);
        assertThat(allowedPrincipals).contains(mockExistingPrincipal);
        assertThat(allowedPrincipals).contains(mockUserGroup);

        final ArgumentCaptor<List> supercategoriesCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setSupercategories(supercategoriesCaptor.capture());

        final List supercategories = supercategoriesCaptor.getValue();
        assertThat(supercategories).hasSize(1);
        assertThat(supercategories).contains(mockSupercategory);

        verify(mockModelService).save(mockCategory);
        verify(fluentCategoryUpsertService).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isTrue();
        assertThat(response.getMessages()).isEmpty();
    }

    @Test
    public void testPersistTargetCategoryWithExistingCategoryWithExistingCategoryAndPrincipal() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";
        final String principalUid = "customergroup";
        final String supercategoryCode = "W33333";

        final IntegrationPrincipalDto principal = new IntegrationPrincipalDto();
        principal.setUid(principalUid);
        final List<IntegrationPrincipalDto> allowedPrincipalUids = Collections.singletonList(principal);

        final IntegrationSupercategoryDto supercategory = new IntegrationSupercategoryDto();
        supercategory.setCode(supercategoryCode);
        final List<IntegrationSupercategoryDto> supercategoryCodes = Collections.singletonList(supercategory);

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);
        given(mockDto.getAllowedPrincipals()).willReturn(allowedPrincipalUids);
        given(mockDto.getSupercategories()).willReturn(supercategoryCodes);

        final UserGroupModel mockExistingPrincipal = mock(UserGroupModel.class);
        final List existingAllowedPrincipals = Collections.singletonList(mockExistingPrincipal);

        final CategoryModel mockExistingSupercategory = mock(CategoryModel.class);
        final List<CategoryModel> existingSupercategories = Collections.singletonList(mockExistingSupercategory);

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getCode()).willReturn(categoryCode);
        given(mockCategory.getName()).willReturn("Old category name");
        given(mockCategory.getAllowedPrincipals()).willReturn(existingAllowedPrincipals);
        given(mockCategory.getSupercategories()).willReturn(existingSupercategories);
        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willReturn(mockCategory);

        given(mockUserService.getUserGroupForUID(principalUid)).willReturn(mockExistingPrincipal);
        given(mockTargetCategoryService.getCategoryForCode(supercategoryCode)).willReturn(mockExistingSupercategory);

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockCategory).setName(categoryName);

        final ArgumentCaptor<List> allowedPrincipalsCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setAllowedPrincipals(allowedPrincipalsCaptor.capture());

        final List allowedPrincipals = allowedPrincipalsCaptor.getValue();
        assertThat(allowedPrincipals).hasSize(1);
        assertThat(allowedPrincipals).contains(mockExistingPrincipal);

        final ArgumentCaptor<List> supercategoriesCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setSupercategories(supercategoriesCaptor.capture());

        final List supercategories = supercategoriesCaptor.getValue();
        assertThat(supercategories).hasSize(1);
        assertThat(supercategories).contains(mockExistingSupercategory);

        verify(mockModelService).save(mockCategory);
        verify(fluentCategoryUpsertService).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isTrue();
        assertThat(response.getMessages()).isEmpty();
    }

    @Test
    public void testPersistTargetCategoryWithExistingCategoryWithUnknownSupercategory() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";
        final String principalUid = "customergroup";
        final String supercategoryCode = "W33333";

        final IntegrationPrincipalDto principal = new IntegrationPrincipalDto();
        principal.setUid(principalUid);
        final List<IntegrationPrincipalDto> allowedPrincipalUids = Collections.singletonList(principal);

        final IntegrationSupercategoryDto supercategory = new IntegrationSupercategoryDto();
        supercategory.setCode(supercategoryCode);
        final List<IntegrationSupercategoryDto> supercategoryCodes = Collections.singletonList(supercategory);

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);
        given(mockDto.getAllowedPrincipals()).willReturn(allowedPrincipalUids);
        given(mockDto.getSupercategories()).willReturn(supercategoryCodes);

        final PrincipalModel mockExistingPrincipal = mock(PrincipalModel.class);
        final List<PrincipalModel> existingAllowedPrincipals = Collections.singletonList(mockExistingPrincipal);

        final CategoryModel mockExistingSupercategory = mock(CategoryModel.class);
        final List<CategoryModel> existingSupercategories = Collections.singletonList(mockExistingSupercategory);

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getCode()).willReturn(categoryCode);
        given(mockCategory.getName()).willReturn("Old category name");
        given(mockCategory.getAllowedPrincipals()).willReturn(existingAllowedPrincipals);
        given(mockCategory.getSupercategories()).willReturn(existingSupercategories);
        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willReturn(mockCategory);

        final UserGroupModel mockUserGroup = mock(UserGroupModel.class);
        given(mockUserService.getUserGroupForUID(principalUid)).willReturn(mockUserGroup);

        given(mockTargetCategoryService.getCategoryForCode(supercategoryCode)).willThrow(
                new UnknownIdentifierException("No category!"));

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockCategory).setName(categoryName);

        final ArgumentCaptor<List> allowedPrincipalsCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setAllowedPrincipals(allowedPrincipalsCaptor.capture());

        final List allowedPrincipals = allowedPrincipalsCaptor.getValue();
        assertThat(allowedPrincipals).hasSize(2);
        assertThat(allowedPrincipals).contains(mockExistingPrincipal);
        assertThat(allowedPrincipals).contains(mockUserGroup);

        verify(mockCategory, never()).setSupercategories(anyList());

        verify(mockModelService, never()).save(mockCategory);
        verify(fluentCategoryUpsertService, never()).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isFalse();
        assertThat(response.getMessages()).hasSize(1);
        assertThat(response.getMessages().get(0))
                .isEqualTo("Category " + supercategoryCode + " defined as a supercategory of " + categoryCode
                        + " does not exist.");
    }

    @Test
    public void testPersistTargetCategoryWithExistingCategoryWithUnknownPrincipal() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";
        final String principalUid = "customergroup";
        final String supercategoryCode = "W33333";

        final IntegrationPrincipalDto principal = new IntegrationPrincipalDto();
        principal.setUid(principalUid);
        final List<IntegrationPrincipalDto> allowedPrincipalUids = Collections.singletonList(principal);

        final IntegrationSupercategoryDto supercategory = new IntegrationSupercategoryDto();
        supercategory.setCode(supercategoryCode);
        final List<IntegrationSupercategoryDto> supercategoryCodes = Collections.singletonList(supercategory);

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);
        given(mockDto.getAllowedPrincipals()).willReturn(allowedPrincipalUids);
        given(mockDto.getSupercategories()).willReturn(supercategoryCodes);

        final PrincipalModel mockExistingPrincipal = mock(PrincipalModel.class);
        final List<PrincipalModel> existingAllowedPrincipals = Collections.singletonList(mockExistingPrincipal);

        final CategoryModel mockExistingSupercategory = mock(CategoryModel.class);
        final List<CategoryModel> existingSupercategories = Collections.singletonList(mockExistingSupercategory);

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getCode()).willReturn(categoryCode);
        given(mockCategory.getName()).willReturn("Old category name");
        given(mockCategory.getAllowedPrincipals()).willReturn(existingAllowedPrincipals);
        given(mockCategory.getSupercategories()).willReturn(existingSupercategories);
        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willReturn(mockCategory);

        final TargetProductCategoryModel mockSupercategory = mock(TargetProductCategoryModel.class);
        given(mockTargetCategoryService.getCategoryForCode(supercategoryCode)).willReturn(mockSupercategory);

        given(mockUserService.getUserGroupForUID(principalUid)).willThrow(
                new UnknownIdentifierException("No principal!"));

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockCategory).setName(categoryName);

        final ArgumentCaptor<List> allowedPrincipalsCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setAllowedPrincipals(allowedPrincipalsCaptor.capture());

        final List allowedPrincipals = allowedPrincipalsCaptor.getValue();
        assertThat(allowedPrincipals).hasSize(1);
        assertThat(allowedPrincipals).contains(mockExistingPrincipal);

        final ArgumentCaptor<List> supercategoriesCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setSupercategories(supercategoriesCaptor.capture());

        final List supercategories = supercategoriesCaptor.getValue();
        assertThat(supercategories).hasSize(1);
        assertThat(supercategories).contains(mockSupercategory);

        verify(mockModelService).save(mockCategory);
        verify(fluentCategoryUpsertService).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isTrue();
        assertThat(response.getMessages()).isEmpty();
    }

    @Test
    public void testPersistTargetCategoryWithSaveException() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);

        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willThrow(
                new UnknownIdentifierException("No category!"));

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getAllowedPrincipals()).willReturn(null);
        given(mockCategory.getSupercategories()).willReturn(null);
        given(mockModelService.create(TargetProductCategoryModel.class)).willReturn(mockCategory);

        final TargetProductCategoryModel mockAllProductsCategory = mock(TargetProductCategoryModel.class);
        given(mockTargetCategoryService.getCategoryForCode("AP01")).willReturn(mockAllProductsCategory);

        Mockito.doThrow(new ModelSavingException("Could not save model!")).when(mockModelService).save(mockCategory);

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        final ArgumentCaptor<List> supercategoryCaptor = ArgumentCaptor.forClass(List.class);

        verify(mockCategory).setCode(categoryCode);
        verify(mockCategory).setName(categoryName);
        verify(mockCategory, never()).setAllowedPrincipals(anyList());
        verify(mockCategory).setSupercategories(supercategoryCaptor.capture());

        final List<CategoryModel> supercategories = supercategoryCaptor.getValue();
        assertThat(supercategories).hasSize(1);
        assertThat(supercategories).contains(mockAllProductsCategory);

        verify(mockModelService).save(mockCategory);
        verify(fluentCategoryUpsertService, never()).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isFalse();
        assertThat(response.getMessages()).hasSize(1);
        assertThat(response.getMessages().get(0)).isEqualTo("Unable to save changes to category " + categoryCode);
    }

    @Test
    public void testPersistTargetCategoryWithExistingCategoryAndExistingSupercategoriesNoNewSupercategories() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";
        final String principalUid = "customergroup";

        final IntegrationPrincipalDto principal = new IntegrationPrincipalDto();
        principal.setUid(principalUid);
        final List<IntegrationPrincipalDto> allowedPrincipalUids = Collections.singletonList(principal);

        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);
        given(mockDto.getAllowedPrincipals()).willReturn(allowedPrincipalUids);
        given(mockDto.getSupercategories()).willReturn(null);

        final PrincipalModel mockExistingPrincipal = mock(PrincipalModel.class);
        final List<PrincipalModel> existingAllowedPrincipals = Collections.singletonList(mockExistingPrincipal);

        final TargetProductCategoryModel mockAllProductsCategory = mock(TargetProductCategoryModel.class);
        final TargetProductCategoryModel mockTMDStdCategory = mock(TargetProductCategoryModel.class);
        final ClassificationClassModel mockClassification = mock(ClassificationClassModel.class);

        final List<CategoryModel> existingSupercategories = new ArrayList<>();
        existingSupercategories.add(mockAllProductsCategory);
        existingSupercategories.add(mockTMDStdCategory);
        existingSupercategories.add(mockClassification);

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getCode()).willReturn(categoryCode);
        given(mockCategory.getName()).willReturn("Old category name");
        given(mockCategory.getAllowedPrincipals()).willReturn(existingAllowedPrincipals);
        given(mockCategory.getSupercategories()).willReturn(existingSupercategories);
        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willReturn(mockCategory);

        final UserGroupModel mockUserGroup = mock(UserGroupModel.class);
        given(mockUserService.getUserGroupForUID(principalUid)).willReturn(mockUserGroup);


        given(mockTargetCategoryService.getCategoryForCode("AP01")).willReturn(mockAllProductsCategory);
        given(mockTargetCategoryService.getCategoryForCode("TMDStd")).willReturn(mockTMDStdCategory);

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockCategory).setName(categoryName);

        final ArgumentCaptor<List> allowedPrincipalsCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setAllowedPrincipals(allowedPrincipalsCaptor.capture());

        final List allowedPrincipals = allowedPrincipalsCaptor.getValue();
        assertThat(allowedPrincipals).hasSize(2);
        assertThat(allowedPrincipals).contains(mockExistingPrincipal);
        assertThat(allowedPrincipals).contains(mockUserGroup);

        final ArgumentCaptor<List> supercategoriesCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockCategory).setSupercategories(supercategoriesCaptor.capture());

        final List supercategories = supercategoriesCaptor.getValue();
        assertThat(supercategories).hasSize(2);
        assertThat(supercategories).contains(mockAllProductsCategory);
        assertThat(supercategories).contains(mockClassification);

        verify(mockModelService).save(mockCategory);
        verify(fluentCategoryUpsertService).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isTrue();
        assertThat(response.getMessages()).isEmpty();
    }


    @Test
    public void testPersistTargetCategoryWithFluentFeedFeatureSwitchOff() {
        final String categoryCode = "W12345";
        final String categoryName = "Department name";
        final String principalUid = "customergroup";
        final String supercategoryCode = "W33333";

        final IntegrationPrincipalDto principal = new IntegrationPrincipalDto();
        principal.setUid(principalUid);
        final List<IntegrationPrincipalDto> allowedPrincipalUids = Collections.singletonList(principal);

        final IntegrationSupercategoryDto supercategory = new IntegrationSupercategoryDto();
        supercategory.setCode(supercategoryCode);
        final List<IntegrationSupercategoryDto> supercategoryCodes = Collections.singletonList(supercategory);

        given(Boolean.valueOf(targetFeatureSwitchService.isFeatureEnabled(TargetProductImportConstants.FEATURE_FLUENT)))
                .willReturn(Boolean.FALSE);
        final IntegrationTargetProductCategoryDto mockDto = mock(IntegrationTargetProductCategoryDto.class);
        given(mockDto.getCode()).willReturn(categoryCode);
        given(mockDto.getName()).willReturn(categoryName);
        given(mockDto.getAllowedPrincipals()).willReturn(allowedPrincipalUids);
        given(mockDto.getSupercategories()).willReturn(supercategoryCodes);

        final PrincipalModel mockExistingPrincipal = mock(PrincipalModel.class);
        final List<PrincipalModel> existingAllowedPrincipals = Collections.singletonList(mockExistingPrincipal);

        final CategoryModel mockExistingSupercategory = mock(CategoryModel.class);
        final List<CategoryModel> existingSupercategories = Collections.singletonList(mockExistingSupercategory);

        final TargetProductCategoryModel mockCategory = mock(TargetProductCategoryModel.class);
        given(mockCategory.getCode()).willReturn(categoryCode);
        given(mockCategory.getName()).willReturn("Old category name");
        given(mockCategory.getAllowedPrincipals()).willReturn(existingAllowedPrincipals);
        given(mockCategory.getSupercategories()).willReturn(existingSupercategories);
        given(mockTargetCategoryService.getCategoryForCode(categoryCode)).willReturn(mockCategory);

        final UserGroupModel mockUserGroup = mock(UserGroupModel.class);
        given(mockUserService.getUserGroupForUID(principalUid)).willReturn(mockUserGroup);

        final TargetProductCategoryModel mockSupercategory = mock(TargetProductCategoryModel.class);
        given(mockTargetCategoryService.getCategoryForCode(supercategoryCode)).willReturn(mockSupercategory);

        final IntegrationResponseDto response = targetCategoryImportIntegrationFacade.persistTargetCategory(mockDto);

        verify(mockCategory).setName(categoryName);
        verify(mockModelService).save(mockCategory);
        verify(fluentCategoryUpsertService, never()).upsertCategory(mockCategory);

        assertThat(response.isSuccessStatus()).isTrue();
        assertThat(response.getMessages()).isEmpty();
    }
}

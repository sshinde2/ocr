/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.enums.InStockStatus;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationSystemVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.LocalizedFeature;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.VariantsService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.exception.StockLevelNotFoundException;
import de.hybris.platform.variants.model.VariantTypeModel;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Lists;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.delivery.TargetDeliveryService;
import au.com.target.tgtcore.model.AbstractTargetVariantProductModel;
import au.com.target.tgtcore.model.BrandModel;
import au.com.target.tgtcore.model.ColourModel;
import au.com.target.tgtcore.model.GiftCardModel;
import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.model.TargetColourVariantProductModel;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.model.TargetProductDimensionsModel;
import au.com.target.tgtcore.model.TargetProductModel;
import au.com.target.tgtcore.model.TargetProductSizeModel;
import au.com.target.tgtcore.model.TargetSizeGroupModel;
import au.com.target.tgtcore.model.TargetSizeVariantProductModel;
import au.com.target.tgtcore.order.TargetOrderService;
import au.com.target.tgtcore.product.BrandService;
import au.com.target.tgtcore.product.ColourService;
import au.com.target.tgtcore.product.ProductTypeService;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtcore.product.TargetProductDimensionsService;
import au.com.target.tgtcore.product.TargetProductSearchService;
import au.com.target.tgtcore.product.TargetProductService;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtcore.sizegroup.service.TargetSizeOrderingService;
import au.com.target.tgtcore.stock.TargetStockService;
import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtfluent.service.FluentProductUpsertService;
import au.com.target.tgtfluent.service.FluentSkuUpsertService;
import au.com.target.tgtfulfilment.warehouseservices.TargetFulfilmentWarehouseService;
import au.com.target.tgtwebcore.purchaseoption.TargetPurchaseOptionHelper;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductMediaDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSecondaryImagesDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationWideImagesDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;
import au.com.target.tgtwsfacades.productimport.data.ProductMediaObjects;
import au.com.target.tgtwsfacades.productimport.services.TargetMediaImportService;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;
import au.com.target.tgtwsfacades.productimport.validators.TargetProductImportValidator;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetProductImportIntegrationFacadeUnitTest {

    private static final String PRODUCT_CODE = "1234";
    private static final String VARIANT_CODE = "4321";
    private static final String NO_COLOUR = "No Colour";
    private static final String PRODUCT_CATALOG = "targetProductCatalog";
    private static final String CATALOG_VERSION = "Staged";
    private static final String TARGET_SIZE_VARIANT_PRODUCT_TYPE = "TargetSizeVariantProduct";
    private static final String HOME_DELIVERY_CODE = "home-delivery";
    private static final String CLICK_N_COLLECT_CODE = "click-n-collect";
    private static final String EXPRESS_DELIVERY_CODE = "express-delivery";
    private static final String EBAY_EXPRESS_DELIVERY_CODE = "ebay-express-delivery";
    private static final String EBAY_HOME_DELIVERY_CODE = "eBay-delivery";
    private static final String EBAY_CNC_DELIVERY_CODE = "ebay-click-and-collect";

    private static final String INFO_DIMENSION_INVALID_EVENT = "INFO-DIMENSION-INVALID-EVENT: For product with productCode={0}, baseProductCode={1}, reason=WeightOrDimensionIsNullOrNegative";

    private static final String INFO_DIGITIAL_PRODUCT_INVALID_EVENT = "INFO_DIGITIAL_PRODUCT_INVALID_EVENT: for product with productCode={0}, baseProduct={1}, reason=InvalidDimensionOrDigitalDeliveryNotAvailble";

    private static final String INFO_ERR_SV_CREATE_UPDATE = "ERR_SV_CREATE_UPDATE: The size variant product SVProductCode={0},CVProductCode={1} can't be created/updated because its color variant has price";

    @Mock
    private ProductService productService;

    @Mock
    private SizeTypeModel sizetype;

    @Mock
    private ModelService modelService;

    @Mock
    private CategoryService categoryService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private BrandService brandService;

    @Mock
    private ColourService colourService;

    @Mock
    private ClassificationService classificationService;

    @Mock
    private VariantsService variantService;

    @Mock
    private TargetMediaImportService targetMediaImportService;

    @Mock
    private TargetDeliveryService deliveryService;

    @Mock
    private TargetPurchaseOptionHelper targetPurchaseOptionHelper;

    @InjectMocks
    @Spy
    private final TargetProductImportIntegrationFacade productImportIntegrationFacade = new TargetProductImportIntegrationFacade();

    @Mock
    private TargetProductModel productModel;

    @Mock
    private TargetColourVariantProductModel colourVariantModel;

    @Mock
    private MediaModel mediaModel;

    @Mock
    private MediaContainerModel mediaContainerModel;

    @Mock
    private CategoryModel categoryModel;

    @Mock
    private TargetMerchDepartmentModel merchDepartmentModel;

    @Mock
    private ColourModel colourModel;

    @Mock
    private VariantTypeModel variantTypeModel;

    @Mock
    private CatalogVersionModel catalogVersionModel;

    @Mock
    private TargetProductSearchService productSearchService;

    @Mock
    private ClassificationSystemService classificationSystemService;

    @Mock
    private ProductTypeService productTypeService;

    @Mock
    private TargetWarehouseService warehouseService;

    @Mock
    private WarehouseModel warehouseModel;

    @Mock
    private List<WarehouseModel> warehouseModels;

    @Mock
    private TargetStockService stockService;

    @Mock
    private ClassAttributeAssignmentModel assignmentModel;

    @Mock
    private ClassificationSystemVersionModel classificationSystemVersionModel;

    @Mock
    private ClassificationAttributeModel classificationAttributeModel;

    @Mock
    private ClassificationAttributeValueModel classificationAttributeValueModel;

    private Locale locale;

    private IntegrationProductDto productDto = null;

    private final Integer department = Integer.valueOf(1);

    @Mock
    private DeliveryModeModel homeDeliveryMode;

    @Mock
    private DeliveryModeModel cncDeliveryMode;

    @Mock
    private DeliveryModeModel expressDeliveryMode;

    @Mock
    private DeliveryModeModel ebayExpressDeliveryMode;

    @Mock
    private DeliveryModeModel ebayHomdeDeliveryMode;

    @Mock
    private DeliveryModeModel ebayCNCDeliveryMode;

    @Captor
    private ArgumentCaptor<Set<DeliveryModeModel>> deliveryModesCaptor;

    @Captor
    private ArgumentCaptor<ProductTypeModel> productTypeCaptor;

    @Mock
    private TargetProductDepartmentService targetProductDepartmentService;

    @Captor
    private ArgumentCaptor<TargetProductModel> targetProductModelCaptor;

    @Captor
    private ArgumentCaptor<TargetMerchDepartmentModel> targetMerchDepartmentModelCaptor;

    @Mock
    private TargetSizeTypeService sizeTypeService;

    @Mock
    private TargetProductDimensionsService targetProductDimensionsService;

    @Mock
    private TargetProductDimensionsModel targetProductPkgDimModel;

    @Mock
    private TargetProductService targetProductService;

    @Mock
    private TargetFulfilmentWarehouseService targetFulfilmentWarehouseService;

    @Mock
    private TargetProductImportUtil targetProductImportUtil;

    @Mock
    private ProductTypeModel productTypeModel;

    @Mock
    private GiftCardModel giftCardModel;

    @Mock
    private TargetFeatureSwitchFacade targetFeatureSwitchFacade;

    @Mock
    private TargetSizeOrderingService targetSizeOrderService;

    @Mock
    private TargetSizeGroupModel sizeGroup;

    @Mock
    private TargetSizeVariantProductModel sizeVariantProductModel;

    @Mock
    private IntegrationProductDto integrationProductDto;

    @Mock
    private TargetProductSizeModel productSize;

    @Mock
    private WarehouseModel fastlineWareHouse;

    @Mock
    private WarehouseModel consolidatedWarehouse;

    @Mock
    private WarehouseModel stockReservationWarehouse;

    @Mock
    private StockLevelModel consolidatedStockLevel;

    @Mock
    private StockLevelModel stockReservationWarehouseStockLevel;


    @Mock
    private FluentProductUpsertService fluentProductUpsertService;

    @Mock
    private FluentSkuUpsertService fluentSkuUpsertService;

    @Mock
    private TargetOrderService targetOrderService;

    @Before
    public void setUp() {
        productImportIntegrationFacade.setHomeDeliveryCode(HOME_DELIVERY_CODE);
        productImportIntegrationFacade.setClickAndCollectCode(CLICK_N_COLLECT_CODE);
        productImportIntegrationFacade.setExpressDeliveryCode(EXPRESS_DELIVERY_CODE);
        productImportIntegrationFacade.setEbayExpressDeliveryCode(EBAY_EXPRESS_DELIVERY_CODE);
        productImportIntegrationFacade.setEbayHomeDeliveryCode(EBAY_HOME_DELIVERY_CODE);
        productImportIntegrationFacade.setEbayClickAndCollectDeliveryCode(EBAY_CNC_DELIVERY_CODE);

        productImportIntegrationFacade.setValidators(new ArrayList<TargetProductImportValidator>());

        productDto = setupBasicIntegrationProductDto();
        locale = new Locale("en");

        merchDepartmentModel.setCode(department.toString());
        given(targetProductImportUtil.getBooleanValue("Y")).willReturn(Boolean.TRUE);

        given(targetProductImportUtil.getBooleanValue("N")).willReturn(Boolean.FALSE);
        given(catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, CATALOG_VERSION)).willReturn(
                catalogVersionModel);
        given(categoryService.getCategoryForCode(department.toString())).willReturn(merchDepartmentModel);
        given(targetProductDepartmentService.getDepartmentCategoryModel(catalogVersionModel, department))
                .willReturn(merchDepartmentModel);

        given(deliveryService.getDeliveryModeForCode(HOME_DELIVERY_CODE)).willReturn(homeDeliveryMode);
        given(deliveryService.getDeliveryModeForCode(CLICK_N_COLLECT_CODE)).willReturn(cncDeliveryMode);
        given(deliveryService.getDeliveryModeForCode(EXPRESS_DELIVERY_CODE)).willReturn(expressDeliveryMode);
        given(deliveryService.getDeliveryModeForCode(EBAY_EXPRESS_DELIVERY_CODE)).willReturn(
                ebayExpressDeliveryMode);
        given(deliveryService.getDeliveryModeForCode(EBAY_HOME_DELIVERY_CODE)).willReturn(
                ebayHomdeDeliveryMode);

        given(deliveryService.getDeliveryModeForCode(EBAY_CNC_DELIVERY_CODE)).willReturn(
                ebayCNCDeliveryMode);

        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(fastlineWareHouse);
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(Collections.EMPTY_LIST));
        given(consolidatedStockLevel.getWarehouse()).willReturn(warehouseModel);
        given(warehouseModel.getCode()).willReturn("warehouse");
    }

    @Test
    public void testpersistTargetProductNullMedia() {
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);
        assertThat(messages.get(0)).isEqualTo(String.format(
                "ERR-PRODUCTIMPORT-MEDIAMISSING : There was no media found on hybris for product with ID %s",
                VARIANT_CODE));

    }

    @Test
    public void testpersistTargetProductAssignHD() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);

        productDto.setAvailableHomeDelivery("Y");
        productDto.setAvailableCnc("N");
        given(merchDepartmentModel.getExpressDeliveryEligible()).willReturn(Boolean.FALSE);

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);

        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(1);
        assertThat(deliveryModesCaptor.getValue()).contains(homeDeliveryMode);
    }

    @Test
    public void testpersistTargetProductAssignHDAndCNC() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);

        productDto.setAvailableHomeDelivery("Y");
        productDto.setAvailableCnc("Y");

        given(merchDepartmentModel.getExpressDeliveryEligible()).willReturn(Boolean.FALSE);

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(2);
        assertThat(deliveryModesCaptor.getValue()).contains(homeDeliveryMode, cncDeliveryMode);
    }


    @Test
    public void testpersistTargetProductAssignebayExpressDeliveryExpressAvailableOnEbay() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        productDto.setAvailableOnEbay("Y");
        productDto.setAvailableEbayExpressDelivery("Y");
        productDto.setAvailableHomeDelivery("N");
        productDto.setAvailableCnc("N");

        given(merchDepartmentModel.getExpressDeliveryEligible()).willReturn(Boolean.FALSE);

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(2);
        assertThat(deliveryModesCaptor.getValue()).contains(ebayExpressDeliveryMode);

    }


    @Test
    public void testpersistTargetProductAssignebayHomeDeliveryExpressAvailableOnEbay() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        productDto.setAvailableOnEbay("Y");
        productDto.setAvailableEbayExpressDelivery("N");
        productDto.setAvailableHomeDelivery("N");
        productDto.setAvailableCnc("N");

        given(merchDepartmentModel.getExpressDeliveryEligible()).willReturn(Boolean.FALSE);

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(1);
        assertThat(deliveryModesCaptor.getValue()).contains(ebayHomdeDeliveryMode);

    }

    @Test
    public void testpersistTargetProductAssignebayExpressDeliveryExpressNotAvailableOnEbay() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        productDto.setAvailableOnEbay("N");
        productDto.setAvailableEbayExpressDelivery("N");
        productDto.setAvailableHomeDelivery("Y");
        productDto.setAvailableCnc("N");

        given(merchDepartmentModel.getExpressDeliveryEligible()).willReturn(Boolean.FALSE);

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(1);
        assertThat(deliveryModesCaptor.getValue()).contains(homeDeliveryMode);
        assertThat(deliveryModesCaptor.getValue()).excludes(ebayHomdeDeliveryMode, ebayExpressDeliveryMode);
    }

    @Test
    public void testpersistTargetProductAssignHomeDeliveryExpressNotAvailableOnEbay() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        productDto.setAvailableOnEbay("N");
        productDto.setAvailableEbayExpressDelivery("N");
        productDto.setAvailableHomeDelivery("Y");
        productDto.setAvailableCnc("N");

        given(merchDepartmentModel.getExpressDeliveryEligible()).willReturn(Boolean.FALSE);

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(1);
        assertThat(deliveryModesCaptor.getValue()).contains(homeDeliveryMode);
        assertThat(deliveryModesCaptor.getValue())
                .excludes(ebayHomdeDeliveryMode, ebayExpressDeliveryMode);
    }


    @Test
    public void testpersistTargetProductAssignExpressDeliveryExpressFeatureOff() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        doReturn(Boolean.FALSE).when(targetFeatureSwitchFacade).isProductImportExpressByFlagEnabled();

        productDto.setAvailableHomeDelivery("Y");
        productDto.setAvailableCnc("Y");

        given(merchDepartmentModel.getExpressDeliveryEligible()).willReturn(Boolean.TRUE);

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(3);
        assertThat(deliveryModesCaptor.getValue()).contains(homeDeliveryMode, cncDeliveryMode, expressDeliveryMode);
    }

    @Test
    public void testpersistTargetProductAssignExpressDeliveryExpressFeatureOn() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        doReturn(Boolean.TRUE).when(targetFeatureSwitchFacade).isProductImportExpressByFlagEnabled();

        productDto.setAvailableExpressDelivery("Y");
        productDto.setAvailableHomeDelivery("Y");
        productDto.setAvailableCnc("Y");

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(3);
        assertThat(deliveryModesCaptor.getValue()).contains(homeDeliveryMode, cncDeliveryMode, expressDeliveryMode);
    }

    @Test
    public void testpersistTargetProductWithValidMedia() {
        final String primaryImage = "A12345";
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        productMediaDto.setPrimaryImage(primaryImage);
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(primaryImage);
        productMediaObjects.setHasValidPrimaryImages(true);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.GRID_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.HERO_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LARGE_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LIST_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.THUMB_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.FULL_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addMediaContainers(mediaContainerModel);
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(null);
        given(
                variantService
                        .getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                                .willReturn(variantTypeModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(warehouseService.getDefWarehouse()).willReturn(getWarehouses());
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(Collections.EMPTY_LIST));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);

        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);

        verify(modelService).save(productModel);
        verify(modelService, times(3)).save(colourVariantModel);
        verify(modelService).create(TargetColourVariantProductModel.class);
        verify(variantService).getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE);
        verify(productService).getProductForCode(Mockito.anyString());
        verify(categoryService, times(3)).getCategoryForCode(Mockito.anyString());
        verifyNoMoreInteractions(modelService);
        verifyNoMoreInteractions(productService);
        verifyNoMoreInteractions(categoryService);
        verifyNoMoreInteractions(variantService);
    }

    @Test
    public void testpersistTargetProductWithNoValidPrimaryMedia() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        productMediaDto.setPrimaryImage("A12345");
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(PRODUCT_CODE);
        productMediaObjects.setHasValidPrimaryImages(false);
        productMediaObjects.addErrors("There is some error");
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.FALSE);

        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);

        verifyNoMoreInteractions(modelService);
        verifyNoMoreInteractions(productService);
        verifyNoMoreInteractions(categoryService);
    }

    @Test
    public void testpersistTargetProductWithValidSecondaryMediaButOneError() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        productMediaDto.setPrimaryImage("A12345");
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(PRODUCT_CODE);
        productMediaObjects.setHasValidPrimaryImages(true);
        final String error = "There is some error";
        productMediaObjects.addErrors(error);
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(null);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(
                variantService
                        .getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                                .willReturn(variantTypeModel);
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(Collections.EMPTY_LIST));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);

        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.FALSE);

        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(2);

        verify(modelService).save(productModel);
        verify(modelService, times(3)).save(colourVariantModel);
        verify(modelService).create(TargetColourVariantProductModel.class);
        verify(variantService).getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE);
        verify(productService).getProductForCode(Mockito.anyString());
        verify(categoryService, times(3)).getCategoryForCode(Mockito.anyString());
        verifyNoMoreInteractions(modelService);
        verifyNoMoreInteractions(productService);
        verifyNoMoreInteractions(categoryService);
        verifyNoMoreInteractions(variantService);
    }

    @Test
    public void testpersistTargetProductWithValidSecondaryMediaButTwoError() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        productMediaDto.setPrimaryImage("A12345");
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(PRODUCT_CODE);
        productMediaObjects.setHasValidPrimaryImages(true);
        productMediaObjects.addErrors("There is some error");
        productMediaObjects.addErrors("There is some other error");
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.FALSE);
        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(2);

        verifyNoMoreInteractions(modelService, productService);
        verifyZeroInteractions(categoryService);
    }

    @Test
    public void testpersistTargetProductWithLicenseAgeNGender() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        final String primaryImage = "A12345";
        productMediaDto.setPrimaryImage(primaryImage);
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        productDto.setAgeFrom(Double.valueOf(3d));
        productDto.setAgeTo(Double.valueOf(5d));
        productDto.setGenders(Lists.asList("girl", new String[] { "boy" }));
        productDto.setLicense("license");
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(primaryImage);
        productMediaObjects.setHasValidPrimaryImages(true);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.GRID_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.HERO_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LARGE_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LIST_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.THUMB_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.FULL_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addMediaContainers(mediaContainerModel);
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(null);
        given(
                variantService
                        .getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                                .willReturn(variantTypeModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(warehouseService.getDefWarehouse()).willReturn(getWarehouses());

        final List<Feature> listOfFeatures = buildFeatures("age", "gender", "license");
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(listOfFeatures));
        given(assignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getCode())
                .willReturn(TargetProductImportConstants.FEATURE_AGE_RANGE)
                .willReturn(TargetProductImportConstants.FEATURE_GENDER)
                .willReturn(TargetProductImportConstants.FEATURE_LICENSE);
        given(classificationAttributeModel.getSystemVersion()).willReturn(classificationSystemVersionModel);
        given(
                classificationSystemService.getAttributeValueForCode(
                        any(ClassificationSystemVersionModel.class), Mockito.anyString()))
                                .willReturn(
                                        classificationAttributeValueModel);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);

        verify(modelService).save(productModel);
        verify(modelService, times(3)).save(colourVariantModel);
        verify(modelService).create(TargetColourVariantProductModel.class);
        verify(variantService).getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE);
        verify(productService).getProductForCode(Mockito.anyString());
        verify(categoryService, times(3)).getCategoryForCode(Mockito.anyString());
        verify(classificationService).getFeatures(productModel);
        verify(classificationAttributeModel, times(3)).getCode();
        verify(classificationAttributeModel).getSystemVersion();
        verify(classificationSystemService, times(2)).getAttributeValueForCode(
                any(ClassificationSystemVersionModel.class), Mockito.anyString());
        verify(classificationService).replaceFeatures(any(ProductModel.class),
                any(FeatureList.class));
        verifyNoMoreInteractions(modelService);
        verifyNoMoreInteractions(productService);
        verifyNoMoreInteractions(categoryService);
        verifyNoMoreInteractions(variantService);
        verifyNoMoreInteractions(classificationService);
        verifyNoMoreInteractions(classificationAttributeModel);
        verifyNoMoreInteractions(classificationSystemService);
    }

    @Test
    public void testpersistTargetProductWithValueForGenderInDTObutFeatureListHasLicenseNAge() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        final String primaryImage = "A12345";
        productMediaDto.setPrimaryImage(primaryImage);
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        productDto.setAgeFrom(Double.valueOf(3d));
        productDto.setAgeTo(Double.valueOf(5d));
        productDto.setGenders(Lists.asList("girl", new String[] { "boy" }));
        productDto.setLicense("license");
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(primaryImage);
        productMediaObjects.setHasValidPrimaryImages(true);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.GRID_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.HERO_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LARGE_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LIST_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.THUMB_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.FULL_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addMediaContainers(mediaContainerModel);
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(null);
        given(
                variantService
                        .getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                                .willReturn(variantTypeModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(warehouseService.getDefWarehouse()).willReturn(getWarehouses());

        final List<Feature> listOfFeatures = buildFeatures("age", null, "license");
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(listOfFeatures));
        given(assignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getCode())
                .willReturn(TargetProductImportConstants.FEATURE_AGE_RANGE)
                .willReturn(TargetProductImportConstants.FEATURE_LICENSE);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);

        verify(modelService).save(productModel);
        verify(modelService, times(3)).save(colourVariantModel);
        verify(modelService).create(TargetColourVariantProductModel.class);
        verify(variantService).getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE);
        verify(productService).getProductForCode(Mockito.anyString());
        verify(categoryService, times(3)).getCategoryForCode(Mockito.anyString());
        verify(classificationService).getFeatures(productModel);
        verify(classificationAttributeModel, times(2)).getCode();
        verify(classificationService).replaceFeatures(any(ProductModel.class),
                any(FeatureList.class));
        verifyNoMoreInteractions(modelService);
        verifyNoMoreInteractions(productService);
        verifyNoMoreInteractions(categoryService);
        verifyNoMoreInteractions(variantService);
        verifyNoMoreInteractions(classificationService);
        verifyNoMoreInteractions(classificationAttributeModel);
        verifyNoMoreInteractions(classificationSystemService);
    }

    @Test
    public void testpersistTargetProductAssignDepartment() {
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);

        productImportIntegrationFacade.setAssociateMedia(false);
        productDto.setAvailableHomeDelivery("Y");
        productDto.setAvailableCnc("Y");
        productDto.setDepartment(new Integer(1));

        given(merchDepartmentModel.getCode()).willReturn("1");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(targetProductDepartmentService).processMerchDeparment(targetProductModelCaptor.capture(),
                targetMerchDepartmentModelCaptor.capture());

        final TargetMerchDepartmentModel tgtMerchDepartmentModel = targetMerchDepartmentModelCaptor.getValue();
        assertThat(tgtMerchDepartmentModel.getCode()).isEqualTo(merchDepartmentModel.getCode());

    }

    @Test
    public void testpersistTargetProductNullSizeType() {
        given(sizeTypeService.findSizeTypeForCode(PRODUCT_CODE)).willReturn(null);
        assertThat(sizeTypeService.findSizeTypeForCode(PRODUCT_CODE)).isNull();
        productImportIntegrationFacade.setAssociateMedia(false);
        productDto.setIsSizeOnly(Boolean.TRUE);
        productDto.setSize("size");
        productDto.setSizeType(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);
    }

    @Test
    public void testpersistTargetProductValidSizeType() {
        productImportIntegrationFacade.setAssociateMedia(true);
        productDto.setIsSizeOnly(Boolean.TRUE);
        productDto.setSize("size");
        productDto.setSizeType("4567");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);
    }

    @Test
    public void testPersistTargetProductDataSizeTypeNullSuccess() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        final String primaryImage = "A12345";
        productMediaDto.setPrimaryImage(primaryImage);
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        productDto.setAgeFrom(Double.valueOf(3d));
        productDto.setAgeTo(Double.valueOf(5d));
        productDto.setGenders(Lists.asList("girl", new String[] { "boy" }));
        productDto.setLicense("license");
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(primaryImage);
        productMediaObjects.setHasValidPrimaryImages(true);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.GRID_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.HERO_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LARGE_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LIST_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.THUMB_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.FULL_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addMediaContainers(mediaContainerModel);
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(colourVariantModel);
        given(
                variantService
                        .getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                                .willReturn(variantTypeModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(warehouseService.getDefWarehouse()).willReturn(getWarehouses());
        productDto.setSizeType("Valid");
        final List<Feature> listOfFeatures = buildFeatures("age", null, "license");
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(listOfFeatures));
        given(assignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getCode())
                .willReturn(TargetProductImportConstants.FEATURE_AGE_RANGE)
                .willReturn(TargetProductImportConstants.FEATURE_LICENSE);
        given(sizeTypeService.findSizeTypeForCode("Valid")).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void testPersistTargetProductDataSizeTypeValidSuccess() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        final String primaryImage = "A12345";
        productMediaDto.setPrimaryImage(primaryImage);
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        productDto.setAgeFrom(Double.valueOf(3d));
        productDto.setAgeTo(Double.valueOf(5d));
        productDto.setGenders(Lists.asList("girl", new String[] { "boy" }));
        productDto.setLicense("license");
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(primaryImage);
        productMediaObjects.setHasValidPrimaryImages(true);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.GRID_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.HERO_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LARGE_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LIST_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.THUMB_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.FULL_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addMediaContainers(mediaContainerModel);
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(colourVariantModel);
        given(
                variantService
                        .getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                                .willReturn(variantTypeModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(warehouseService.getDefWarehouse()).willReturn(getWarehouses());
        productDto.setSizeType("Valid");
        final List<Feature> listOfFeatures = buildFeatures("age", null, "license");
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(listOfFeatures));
        given(assignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getCode())
                .willReturn(TargetProductImportConstants.FEATURE_AGE_RANGE)
                .willReturn(TargetProductImportConstants.FEATURE_LICENSE);
        given(sizeTypeService.findSizeTypeForCode("Valid")).willReturn(sizetype);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(targetProductService).setOnlineDateIfApplicable(colourVariantModel);
    }

    @Test
    public void testPersistTargetProductDataForDigitalDelivery() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        final String primaryImage = "A12345";
        productMediaDto.setPrimaryImage(primaryImage);
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        productDto.setAvailableDigitalDelivery("Y");
        productDto.setProductType("digital");
        productDto.setGiftcardBrandId("test-gc-brand");
        productDto.setDenominaton(Double.valueOf(10d));

        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(primaryImage);
        productMediaObjects.setHasValidPrimaryImages(true);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.GRID_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addMediaContainers(mediaContainerModel);
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages()
                        .getSecondaryImage(),
                null))
                        .willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(null);
        given(variantService.getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                .willReturn(variantTypeModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(warehouseService.getDefWarehouse()).willReturn(getWarehouses());
        given(targetFulfilmentWarehouseService.getGiftcardWarehouse()).willReturn(warehouseModel);
        productDto.setSizeType("Valid");
        final List<Feature> listOfFeatures = buildFeatures("age", null, "license");
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(listOfFeatures));
        given(assignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getCode())
                .willReturn(TargetProductImportConstants.FEATURE_AGE_RANGE)
                .willReturn(TargetProductImportConstants.FEATURE_LICENSE);
        given(sizeTypeService.findSizeTypeForCode("Valid")).willReturn(sizetype);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(targetProductService).setOnlineDateIfApplicable(colourVariantModel);
        verify(targetFulfilmentWarehouseService).getGiftcardWarehouse();
    }

    @Test
    public void testpersistTargetProductNoDimensions() {

        defaultMockSetup();

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
    }

    @Test
    public void testpersistTargetProductAllDimensions() {

        defaultMockSetup();

        productDto.setPkgHeight(Double.valueOf(10));
        productDto.setPkgWidth(Double.valueOf(11));
        productDto.setPkgLength(Double.valueOf(12));
        productDto.setPkgWeight(Double.valueOf(13));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);

        verify(targetProductPkgDimModel).setHeight(Double.valueOf(10));
        verify(targetProductPkgDimModel).setWidth(Double.valueOf(11));
        verify(targetProductPkgDimModel).setLength(Double.valueOf(12));
        verify(targetProductPkgDimModel).setWeight(Double.valueOf(13));
    }

    @Test
    public void testpersistTargetProductSaveDimensionsException() {

        defaultMockSetup();

        final String message = "TestModelSavingException";
        doThrow(new ModelSavingException(message))
                .when(modelService).save(targetProductPkgDimModel);

        productDto.setPkgHeight(Double.valueOf(10));
        productDto.setPkgWidth(Double.valueOf(11));
        productDto.setPkgLength(Double.valueOf(12));
        productDto.setPkgWeight(Double.valueOf(13));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.FALSE);

        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);
        final String expectedMessage = MessageFormat.format(
                TargetProductImportConstants.LogMessages.ERR_PRODUCTIMPORT_EXCEPTION, VARIANT_CODE)
                + " " + message;
        assertThat(messages.get(0)).isEqualTo(expectedMessage);

    }

    @Test
    public void testGetMessageforInvalidEan() {
        assertThat(productImportIntegrationFacade.getMessageforInvalidEan("123", "abc", "987"))
                .isEqualTo(
                        "EAN validation failed for EAN: 987 for product with code: 123, name: abc hence EAN has been set to empty.");

    }

    /**
     * @param age
     * @param gender
     * @param license
     * @return list of features that are not blank in the arguments
     */
    private List<Feature> buildFeatures(final String age, final String gender, final String license) {
        final List<Feature> features = new ArrayList<>();
        if (StringUtils.isNotBlank(age)) {
            final Feature feature = new LocalizedFeature(assignmentModel, null, locale);
            features.add(feature);
        }
        if (StringUtils.isNotBlank(gender)) {
            final Feature feature = new LocalizedFeature(assignmentModel, null, locale);
            features.add(feature);
        }
        if (StringUtils.isNotBlank(license)) {
            final Feature feature = new LocalizedFeature(assignmentModel, null, locale);
            features.add(feature);
        }
        return features;
    }

    /**
     * @return IntegrationProductDto with basic info
     */
    private IntegrationProductDto setupBasicIntegrationProductDto() {
        final IntegrationProductDto dto = new IntegrationProductDto();
        dto.setIsAssortment(Boolean.FALSE);
        dto.setIsStylegroup(Boolean.TRUE);
        dto.setProductCode(PRODUCT_CODE);
        dto.setName("BedSheets");
        dto.setProductType("N");
        dto.setPrimaryCategory("primCategory");
        dto.setSecondaryCategory(Collections.singletonList("secCategory"));
        dto.setBrand("brand");
        dto.setApprovalStatus("Y");
        dto.setVariantCode(VARIANT_CODE);

        dto.setDepartment(department);

        // description
        dto.setDescription("blahblahblah");

        // bulky product / correct product type
        dto.setBulky("Bulky1");

        // available for layby
        dto.setAvailableLayby("Y");

        dto.setAvailableLongtermLayby("Y");

        // available for CNC
        dto.setAvailableCnc("Y");

        // available for MHD
        dto.setAvailableHomeDelivery("Y");

        // sizeType
        dto.setIsSizeOnly(Boolean.FALSE);

        return dto;
    }

    @Test
    public void testProductImportColorVariantSellableDisplayOnly() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        dto.setDisplayOnly("Y");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(colourVariantModel).setDisplayOnly(Boolean.TRUE);
        verify(stockService, never()).getAllStockLevels(colourVariantModel);
        verify(stockService, never())
                .updateActualStockLevel(colourVariantModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    @Test
    public void testProductImportColorVariantSellableNotDisplayOnly() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        dto.setDisplayOnly("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(colourVariantModel).setDisplayOnly(Boolean.FALSE);
        verify(stockService).getAllStockLevels(colourVariantModel);
        verify(stockService)
                .updateActualStockLevel(colourVariantModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    @Test
    public void testProductImportColorVariantSellableDisplayOnlyEmpty() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(colourVariantModel).setDisplayOnly(Boolean.FALSE);
        verify(warehouseService, times(2)).getDefaultOnlineWarehouse();
        verify(stockService).getAllStockLevels(colourVariantModel);
        verify(stockService)
                .updateActualStockLevel(colourVariantModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    @Test
    public void testProductImportColorVariantSellableExistingPrdNonDisplayOnly() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        given(productSearchService.getColourVariantByBaseProductAndSwatch(dto.getProductCode(), null))
                .willReturn(colourVariantModel);
        defaultMockSetup();
        given(productService.getProductForCode(dto.getVariantCode())).willReturn(colourVariantModel);
        given(stockService.getAllStockLevels(colourVariantModel)).willReturn(null);
        dto.setDisplayOnly("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(colourVariantModel).setDisplayOnly(Boolean.FALSE);
        verify(warehouseService, times(2)).getDefaultOnlineWarehouse();
        verify(stockService).getAllStockLevels(colourVariantModel);
        verify(stockService)
                .updateActualStockLevel(colourVariantModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    /**
     * Existing color variant with stock level When the import runs shouldn't create stocklevel
     */
    @Test
    public void testProductImportColorVariantSellableExistingPrdNonDisplayOnlyWithStock() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        given(productSearchService.getColourVariantByBaseProductAndSwatch(dto.getProductCode(), null))
                .willReturn(colourVariantModel);
        final List<StockLevelModel> stockLevels = new ArrayList<StockLevelModel>();
        final StockLevelModel stocklevel = mock(StockLevelModel.class);
        stockLevels.add(stocklevel);
        given(stocklevel.getWarehouse()).willReturn(warehouseModel);
        given(stockService.getAllStockLevels(colourVariantModel)).willReturn(stockLevels);
        dto.setDisplayOnly("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(colourVariantModel).setDisplayOnly(Boolean.FALSE);
        verify(warehouseService, times(2)).getDefaultOnlineWarehouse();
        verify(stockService, times(2)).getAllStockLevels(colourVariantModel);
        verify(stockService, never())
                .updateActualStockLevel(colourVariantModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    /**
     * display only prd,newvariant doesn't create stock level
     */
    @Test
    public void testProductImportSizeVariantSellableDisplayOnly() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("Y");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.TRUE);
        verify(stockService, never()).getAllStockLevels(sizeVariantProductModel);
        verify(stockService, never())
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    /**
     * display only prd,newvariant doesn't create stock level
     */
    @Test
    public void testProductImportSizeVariantSellableDisplayOnlyNewPrd() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("Y");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.TRUE);
        verify(stockService, never()).getAllStockLevels(sizeVariantProductModel);
        verify(stockService, never())
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
        verify(stockService, never())
                .updateActualStockLevel(sizeVariantProductModel, consolidatedWarehouse, 0, StringUtils.EMPTY);
        verify(modelService, never()).create(StockLevelModel.class);
    }


    /**
     * display only prd,existing prd ,stocklevel consolidatewarehouse
     */
    @Test
    public void testProductImportsizeVariantSellableExistingPrd() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("Y");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(sizeVariantProductModel);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE)).willReturn(consolidatedWarehouse);
        given(stockService.checkAndGetStockLevel(sizeVariantProductModel,
                consolidatedWarehouse)).willReturn(consolidatedStockLevel);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.TRUE);
        verify(stockService, never()).getAllStockLevels(sizeVariantProductModel);
        verify(warehouseService)
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        verify(consolidatedStockLevel).setInStockStatus(InStockStatus.FORCEOUTOFSTOCK);
        verify(stockService).checkAndGetStockLevel(sizeVariantProductModel,
                consolidatedWarehouse);
    }

    /**
     * display only prd,existing prd ,no stocklevel
     */
    @Test
    public void testProductImportsizeVariantSellableExistingPrdDisplayOnlyNoStock() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("Y");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(sizeVariantProductModel);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE)).willReturn(consolidatedWarehouse);
        given(stockService.checkAndGetStockLevel(sizeVariantProductModel,
                consolidatedWarehouse)).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.TRUE);
        verify(stockService, never()).getAllStockLevels(sizeVariantProductModel);
        verify(warehouseService)
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        verify(stockService, never())
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
        verify(stockService, never())
                .updateActualStockLevel(sizeVariantProductModel, consolidatedWarehouse, 0, StringUtils.EMPTY);
        verify(modelService, never()).create(StockLevelModel.class);
    }

    /**
     * Not a display only prd,newvariant creates stock level
     */
    @Test
    public void testProductImportSizeVariantSellableDisplayOnlyFalse() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("N");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(warehouseService, times(2)).getDefaultOnlineWarehouse();
        verify(stockService).getAllStockLevels(sizeVariantProductModel);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    /**
     * Not a display only prd(displayonly set to empty,newvariant creates stock level
     */
    @Test
    public void testProductImportSizeVariantSellableDisplayOnlyEmpty() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(warehouseService, times(2)).getDefaultOnlineWarehouse();
        verify(stockService).getAllStockLevels(sizeVariantProductModel);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    /**
     * Not a display only prd(displayonly set to empty,existing prd having stock level,so shoudn't create stock
     */
    @Test
    public void testProductImportsizeVariantSellableExistingPrdNonDisplayOnlyWithStock() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("");
        dto.addVariant(variantDto);
        final List<StockLevelModel> stockLevels = new ArrayList<StockLevelModel>();
        final StockLevelModel stocklevel = mock(StockLevelModel.class);
        stockLevels.add(stocklevel);
        given(stocklevel.getWarehouse()).willReturn(warehouseModel);
        given(stockService.getAllStockLevels(sizeVariantProductModel)).willReturn(stockLevels);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(sizeVariantProductModel);
        given(sizeVariantProductModel.getStockLevels()).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(warehouseService, times(2)).getDefaultOnlineWarehouse();
        verify(stockService, times(2)).getAllStockLevels(sizeVariantProductModel);
        verify(stockService, never())
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }



    /**
     * non display only flag,existing prd displayonly ,stocklevel consolidatewarehouse set back to not specified
     */
    @Test
    public void testProductImportsizeVariantSellableExistingDisplayOnlyPrd() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("N");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(sizeVariantProductModel);
        given(sizeVariantProductModel.getDisplayOnly()).willReturn(Boolean.TRUE);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE)).willReturn(consolidatedWarehouse);
        given(stockService.checkAndGetStockLevel(sizeVariantProductModel,
                consolidatedWarehouse)).willReturn(consolidatedStockLevel);
        given(stockService.getAllStockLevels(sizeVariantProductModel)).willReturn(
                Collections.singletonList(consolidatedStockLevel));
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(stockService, times(2)).getAllStockLevels(sizeVariantProductModel);
        verify(warehouseService)
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        verify(consolidatedStockLevel).setInStockStatus(InStockStatus.NOTSPECIFIED);
        verify(modelService).save(consolidatedStockLevel);
        verify(modelService).refresh(consolidatedStockLevel);
        verify(stockService).checkAndGetStockLevel(sizeVariantProductModel,
                consolidatedWarehouse);
    }



    /**
     * non display only flag,existing prd displayonly ,stocklevel doesn't exist
     */
    @Test
    public void testProductImportsizeVariantSellableExistingDisplayOnlyPrdNoCWStockLevel() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("N");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(sizeVariantProductModel);
        given(sizeVariantProductModel.getDisplayOnly()).willReturn(Boolean.TRUE);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE)).willReturn(consolidatedWarehouse);
        given(stockService.checkAndGetStockLevel(sizeVariantProductModel,
                consolidatedWarehouse)).willReturn(null);
        given(stockService.getAllStockLevels(sizeVariantProductModel)).willReturn(
                Collections.singletonList(stockReservationWarehouseStockLevel));
        given(stockReservationWarehouseStockLevel.getWarehouse()).willReturn(fastlineWareHouse);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(stockService, times(2)).getAllStockLevels(sizeVariantProductModel);
        verify(warehouseService)
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        verifyZeroInteractions(consolidatedStockLevel);
    }


    @Test
    public void testProductImportSizeVariantSellableDisplayOnlyFalseFalconFeatureSwitchOn() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFalconEnabled();
        given(modelService.create(StockLevelModel.class)).willReturn(stockReservationWarehouseStockLevel);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE)).willReturn(consolidatedWarehouse);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE)).willReturn(
                        stockReservationWarehouse);
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("N");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(stockService).getAllStockLevels(sizeVariantProductModel);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, consolidatedWarehouse, 0, StringUtils.EMPTY);
        verify(warehouseService, times(2)).getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        verify(warehouseService).getWarehouseForCode(TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE);
        verify(modelService).create(StockLevelModel.class);
        verify(stockReservationWarehouseStockLevel).setInStockStatus(InStockStatus.NOTSPECIFIED);
        verify(stockReservationWarehouseStockLevel).setWarehouse(stockReservationWarehouse);
    }

    @Test
    public void testProductImportSizeVariantSellableDisplayOnlyEmptyFalconFeatureSwitchOn() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade).isFalconEnabled();
        given(modelService.create(StockLevelModel.class)).willReturn(stockReservationWarehouseStockLevel);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE)).willReturn(consolidatedWarehouse);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE)).willReturn(
                        stockReservationWarehouse);
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(stockService).getAllStockLevels(sizeVariantProductModel);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, consolidatedWarehouse, 0, StringUtils.EMPTY);
        verify(warehouseService, times(2)).getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        verify(warehouseService).getWarehouseForCode(TgtCoreConstants.STOCK_RESERVATION_WAREHOUSE);
        verify(modelService).create(StockLevelModel.class);
        verify(stockReservationWarehouseStockLevel).setInStockStatus(InStockStatus.NOTSPECIFIED);
        verify(stockReservationWarehouseStockLevel).setWarehouse(stockReservationWarehouse);
    }

    /**
     * non display only flag,existing prd displayonly ,stocklevel doesn't exist
     */
    @Test
    public void testProductImportsizeVariantSellableExistingDisplayOnlyStockLevelException() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("N");
        dto.addVariant(variantDto);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(sizeVariantProductModel);
        given(sizeVariantProductModel.getDisplayOnly()).willReturn(Boolean.TRUE);
        given(warehouseService
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE)).willReturn(consolidatedWarehouse);
        given(stockService.checkAndGetStockLevel(sizeVariantProductModel,
                consolidatedWarehouse)).willThrow(new StockLevelNotFoundException("test"));
        given(stockService.getAllStockLevels(sizeVariantProductModel)).willReturn(
                Collections.singletonList(stockReservationWarehouseStockLevel));
        given(stockReservationWarehouseStockLevel.getWarehouse()).willReturn(fastlineWareHouse);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(stockService, times(2)).getAllStockLevels(sizeVariantProductModel);
        verify(warehouseService)
                .getWarehouseForCode(TgtCoreConstants.CONSOLIDATED_STORE_WAREHOUSE);
        verifyZeroInteractions(consolidatedStockLevel);
    }

    /**
     * Not a display only prd(displayonly set to empty,existing prd doesn't have stock level,so should create stock
     */
    @Test
    public void testProductImportSizeVariantSellableDisplayOnlySetAtCVLevel() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        dto.addVariant(variantDto);
        dto.setDisplayOnly("Y");
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.TRUE);
        verify(warehouseService, never()).getDefaultOnlineWarehouse();
    }

    @Test
    public void testProductImportSizeVariantSellableNotDisplayOnlySetAtSVLevel() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("Y");
        dto.addVariant(variantDto);
        dto.setDisplayOnly("N");
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.TRUE);
        verify(warehouseService, never()).getDefaultOnlineWarehouse();
    }

    @Test
    public void testProductImportsizeVariantSellableExistingPrdNonDisplayOnly() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("");
        dto.addVariant(variantDto);
        given(stockService.getAllStockLevels(sizeVariantProductModel)).willReturn(null);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(sizeVariantProductModel);
        given(sizeVariantProductModel.getStockLevels()).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(warehouseService, times(2)).getDefaultOnlineWarehouse();
        verify(stockService).getAllStockLevels(sizeVariantProductModel);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    /**
     * display only prd,existing prd doesn't have stock level,should not create stock
     */
    @Test
    public void testProductImportsizeVariantSellableExistingPrdDisplayOnly() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(dto);
        variantDto.setDisplayOnly("Y");
        dto.addVariant(variantDto);
        given(stockService.getAllStockLevels(sizeVariantProductModel)).willReturn(null);
        given(productService.getProductForCode(variantDto.getVariantCode())).willReturn(sizeVariantProductModel);
        given(sizeVariantProductModel.getStockLevels()).willReturn(null);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.TRUE);
        verify(stockService, never()).getAllStockLevels(sizeVariantProductModel);
        verify(stockService, never())
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    /**
     * Size only prd non display only
     */
    @Test
    public void testProductImportIsSizeOnlyPrdNonDisplayOnly() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setDisplayOnly("N");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(stockService).getAllStockLevels(sizeVariantProductModel);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }


    /**
     * Size only,display only pd,stock will get created
     */
    @Test
    public void testProductImportIsSizeOnlyPrdDisplayOnly() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setDisplayOnly("Y");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.TRUE);
        verify(stockService, never()).getAllStockLevels(sizeVariantProductModel);
        verify(stockService, never())
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    @Test
    public void testProductImportIsSizeOnlyPrdDisplayOnlyEmpty() {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        dto.setIsSizeOnly(Boolean.TRUE);
        dto.setDisplayOnly("");
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(dto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(sizeVariantProductModel).setDisplayOnly(Boolean.FALSE);
        verify(warehouseService, times(2)).getDefaultOnlineWarehouse();
        verify(stockService).getAllStockLevels(sizeVariantProductModel);
        verify(stockService)
                .updateActualStockLevel(sizeVariantProductModel, fastlineWareHouse, 0, StringUtils.EMPTY);
    }

    private List<WarehouseModel> getWarehouses() {
        final List<WarehouseModel> warehouses = new ArrayList<>();
        warehouses.add(warehouseModel);
        return warehouses;
    }

    private void defaultMockSetup() {

        productImportIntegrationFacade.setAssociateMedia(false);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(variantService.getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                .willReturn(variantTypeModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(warehouseService.getDefWarehouse()).willReturn(getWarehouses());
        given(modelService.create(TargetSizeVariantProductModel.class)).willReturn(sizeVariantProductModel);
        given(sizeVariantProductModel.getCode()).willReturn("43210");
        given(modelService.create(TargetProductDimensionsModel.class)).willReturn(targetProductPkgDimModel);

        productDto.setApprovalStatus("active");
    }

    @Test
    public void testPersistTargetProductOnlineOfflineDates() {

        defaultMockSetup();

        productDto.setProductOnlineDate("2015-08-16 00:00:00");
        productDto.setProductOfflineDate("2015-09-26 00:00:00");

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);

        verify(colourVariantModel).setOnlineDate(any(Date.class));
        verify(colourVariantModel).setOfflineDate(any(Date.class));
    }

    @Test
    public void testPersistTargetProductOnlineOfflineDatesNull() {

        defaultMockSetup();

        productDto.setProductOnlineDate(null);
        productDto.setProductOfflineDate(null);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);

        verify(colourVariantModel, never()).setOnlineDate(any(Date.class));
        verify(colourVariantModel, never()).setOfflineDate(any(Date.class));
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenWeightIsInvalid() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(null, Double.valueOf(10.0),
                Double.valueOf(10.0), Double.valueOf(10.0), VARIANT_CODE, PRODUCT_CODE, "Y");
        verify(productImportIntegrationFacade).logInvalidDimension(INFO_DIMENSION_INVALID_EVENT,
                VARIANT_CODE,
                PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenLengthIsInvalid() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(Double.valueOf(10.0), null,
                Double.valueOf(10.0), Double.valueOf(10.0), VARIANT_CODE, PRODUCT_CODE, "Y");
        verify(productImportIntegrationFacade).logInvalidDimension(INFO_DIMENSION_INVALID_EVENT,
                VARIANT_CODE,
                PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenWidthIsInvalid() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(Double.valueOf(10.0), Double.valueOf(10.0),
                Double.valueOf(10.0), Double.valueOf(-10.0), VARIANT_CODE, PRODUCT_CODE, "Y");
        verify(productImportIntegrationFacade).logInvalidDimension(INFO_DIMENSION_INVALID_EVENT,
                VARIANT_CODE,
                PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenHeightIsInvalid() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(Double.valueOf(10.0), Double.valueOf(10.0),
                Double.valueOf(10.0), null, VARIANT_CODE, PRODUCT_CODE, "Y");
        verify(productImportIntegrationFacade).logInvalidDimension(INFO_DIMENSION_INVALID_EVENT,
                VARIANT_CODE,
                PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenAllDimensionsAreValid() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(Double.valueOf(10.0), Double.valueOf(10.0),
                Double.valueOf(10.0), Double.valueOf(10.0), VARIANT_CODE, PRODUCT_CODE, "Y");
        verify(productImportIntegrationFacade, never())
                .logInvalidDimension(INFO_DIMENSION_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenAllDimensionsAreZero() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(Double.valueOf(0.0), Double.valueOf(0.0),
                Double.valueOf(0.0), Double.valueOf(0.0), VARIANT_CODE, PRODUCT_CODE, "Y");
        verify(productImportIntegrationFacade, never())
                .logInvalidDimension(INFO_DIMENSION_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
        verify(productImportIntegrationFacade, never())
                .logInvalidDimension(INFO_DIGITIAL_PRODUCT_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenAllDimensionsAreNull() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(null, null, null, null, VARIANT_CODE,
                PRODUCT_CODE, "Y");
        verify(productImportIntegrationFacade).logInvalidDimension(INFO_DIMENSION_INVALID_EVENT,
                VARIANT_CODE,
                PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenAllDimensionsAreZeroAndWithOutDigitalOutDelivery() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(Double.valueOf(0.0), Double.valueOf(0.0),
                Double.valueOf(0.0), Double.valueOf(0.0), VARIANT_CODE, PRODUCT_CODE, "N");
        verify(productImportIntegrationFacade, never())
                .logInvalidDimension(INFO_DIMENSION_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
        verify(productImportIntegrationFacade)
                .logInvalidDimension(INFO_DIGITIAL_PRODUCT_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenAnyOneDimensionsIsZeroAndWithDigitalDelivery() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(Double.valueOf(10.0), Double.valueOf(10.0),
                Double.valueOf(10.0), Double.valueOf(0.0), VARIANT_CODE, PRODUCT_CODE, "Y");
        verify(productImportIntegrationFacade, never())
                .logInvalidDimension(INFO_DIMENSION_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
        verify(productImportIntegrationFacade, never())
                .logInvalidDimension(INFO_DIGITIAL_PRODUCT_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
    }

    @Test
    public void testCheckIfDimensionsAreInvalidAndLogWhenAnyOneDimensionsIsZeroAndWithOutDigitalDelivery() {
        productImportIntegrationFacade.checkIfDimensionsAreInvalidAndLog(Double.valueOf(10.0), Double.valueOf(10.0),
                Double.valueOf(10.0), Double.valueOf(0.0), VARIANT_CODE, PRODUCT_CODE, "N");
        verify(productImportIntegrationFacade, never())
                .logInvalidDimension(INFO_DIMENSION_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
        verify(productImportIntegrationFacade)
                .logInvalidDimension(INFO_DIGITIAL_PRODUCT_INVALID_EVENT, VARIANT_CODE, PRODUCT_CODE);
    }

    @Test
    public void testLogIfColorVariantHasPrice() {

        productImportIntegrationFacade.setAssociateMedia(false);
        productDto.setIsSizeOnly(Boolean.TRUE);
        productDto.setSize("40");
        productDto.setSizeType("M");
        final Collection<PriceRowModel> prices = new LinkedList<PriceRowModel>();
        final PriceRowModel price = new PriceRowModel();
        prices.add(price);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(colourVariantModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourVariantModel.getEurope1Prices()).willReturn(prices);
        given(
                variantService
                        .getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                                .willReturn(variantTypeModel);
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(Collections.EMPTY_LIST));

        productImportIntegrationFacade.persistTargetProduct(productDto);
        verify(productImportIntegrationFacade)
                .logInvalidPrices(INFO_ERR_SV_CREATE_UPDATE, productDto.getVariantCode(), VARIANT_CODE);
    }

    @Test
    public void testLogIfColorVariantHasNoPrice() {
        productImportIntegrationFacade.setAssociateMedia(false);
        productDto.setIsSizeOnly(Boolean.TRUE);
        productDto.setSize("40");
        productDto.setSizeType("M");
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(colourVariantModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(
                variantService
                        .getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                                .willReturn(variantTypeModel);
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(Collections.EMPTY_LIST));

        productImportIntegrationFacade.persistTargetProduct(productDto);
        verify(productImportIntegrationFacade, never())
                .logInvalidPrices(INFO_ERR_SV_CREATE_UPDATE, productDto.getVariantCode(), VARIANT_CODE);
    }


    @Test
    public void testEbayCNCPopulation() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        productDto.setAvailableOnEbay("Y");
        productDto.setAvailableCnc("Y");


        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(4);
        assertThat(deliveryModesCaptor.getValue()).contains(cncDeliveryMode);
        assertThat(deliveryModesCaptor.getValue()).contains(ebayCNCDeliveryMode);
    }

    @Test
    public void testEbayCNCPopulationNotAvailableOnEbay() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        productDto.setAvailableOnEbay("N");
        productDto.setAvailableCnc("Y");

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(2);
        assertThat(deliveryModesCaptor.getValue()).contains(cncDeliveryMode);
    }

    @Test
    public void testEbayCNCPopulationAvailableOnEbayNotCNC() {

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        productDto.setAvailableOnEbay("Y");
        productDto.setAvailableCnc("N");
        productDto.setAvailableHomeDelivery("Y");

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue().size()).isEqualTo(2);
        assertThat(deliveryModesCaptor.getValue()).excludes(cncDeliveryMode);
    }

    @Test
    public void testAssociateSizeGroupWithSizeGroup() throws InstantiationException {
        final TargetColourVariantProductModel colorVariant = mock(TargetColourVariantProductModel.class);
        given(integrationProductDto.getSizeGroup()).willReturn("womens");
        given(
                targetSizeOrderService.getTargetSizeGroupByName("womens", integrationProductDto.getProductCode(), true))
                        .willReturn(sizeGroup);
        productImportIntegrationFacade.associateSizeGroup(colorVariant, integrationProductDto);
        verify(colorVariant).setSizeGroup(sizeGroup);
    }

    @Test(expected = InstantiationException.class)
    public void testAssociateSizeGroupWithNoSizeGroup() throws InstantiationException {
        final TargetColourVariantProductModel colorVariant = mock(TargetColourVariantProductModel.class);
        given(integrationProductDto.getSizeGroup()).willReturn("womens");
        given(
                targetSizeOrderService.getTargetSizeGroupByName("womens", integrationProductDto.getProductCode(), true))
                        .willReturn(null);
        productImportIntegrationFacade.associateSizeGroup(colorVariant, integrationProductDto);
    }

    @Test
    public void testAssociateProductSizeWithExistingPrdSize() throws InstantiationException {
        final TargetSizeVariantProductModel sizeVariant = mock(TargetSizeVariantProductModel.class);
        given(sizeVariant.getSize()).willReturn("xs");
        given(targetSizeOrderService.getTargetProductSize("xs", sizeGroup, true, sizeVariant.getCode()))
                .willReturn(productSize);
        productImportIntegrationFacade.associateProductSize(sizeVariant, sizeGroup, "xs");
        verify(sizeVariant).setProductSize(productSize);
    }

    @Test(expected = InstantiationException.class)
    public void testAssociateProductSizeWithNullPrdSize() throws InstantiationException {
        final TargetSizeVariantProductModel sizeVariant = mock(TargetSizeVariantProductModel.class);
        given(targetSizeOrderService.getTargetProductSize("xs", sizeGroup, true, sizeVariant.getCode()))
                .willReturn(null);
        productImportIntegrationFacade.associateProductSize(sizeVariant, sizeGroup, "xs");
    }

    @Test
    public void testCheckStockLevelIsAssignedToCorrectWarehouseWhenTheyAreDifferent() {
        final AbstractTargetVariantProductModel variant = mock(AbstractTargetVariantProductModel.class);
        given(variant.getCode()).willReturn("MOCK_PROD");

        mockWarehouseInfo(variant, "FastlineWarehouse", "CnpWarehouse", "DTO_PRODCODE");

        productImportIntegrationFacade.checkStockLevelIsAssignedToCorrectWarehouse(variant, productDto);

        verify(productImportIntegrationFacade).logWarehouseMismatch("MOCK_PROD", "DTO_PRODCODE",
                "FastlineWarehouse", "CnpWarehouse");
    }

    @Test
    public void testCheckStockLevelIsAssignedToCorrectWarehouseWhenTheyAreSame() {
        final AbstractTargetVariantProductModel variant = mock(AbstractTargetVariantProductModel.class);
        given(variant.getCode()).willReturn("MOCK_PROD");

        mockWarehouseInfo(variant, "CnpWarehouse", "CnpWarehouse", "DTO_PRODCODE");

        productImportIntegrationFacade.checkStockLevelIsAssignedToCorrectWarehouse(variant, productDto);

        verify(productImportIntegrationFacade, never()).logWarehouseMismatch("MOCK_PROD",
                "DTO_PRODCODE", "FastlineWarehouse", "CnpWarehouse");
    }

    @Test
    public void testCheckStockLevelIsAssignedToCorrectWarehouseWhenNoData() {
        final AbstractTargetVariantProductModel variant = mock(AbstractTargetVariantProductModel.class);
        given(variant.getCode()).willReturn("MOCK_PROD");

        mockWarehouseInfo(variant, "CnpWarehouse", null, "DTO_PRODCODE");

        final WarehouseModel defaultWarehouse = mock(WarehouseModel.class);
        given(warehouseService.getDefaultOnlineWarehouse()).willReturn(defaultWarehouse);
        given(defaultWarehouse.getCode()).willReturn("FastlineWarehouse");

        productImportIntegrationFacade.checkStockLevelIsAssignedToCorrectWarehouse(variant, productDto);

        verify(productImportIntegrationFacade).logWarehouseMismatch("MOCK_PROD",
                "DTO_PRODCODE", "CnpWarehouse", "FastlineWarehouse");
    }

    @Test
    public void testExcludeForAfterpayForSpecificProductTypeAndBrandForAppleProduct() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        dto.setName("Iphone");
        dto.setBrand("Apple");
        defaultMockSetup();

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        final BrandModel brandModel = mock(BrandModel.class);
        given(colourVariantModel.getBrand()).willReturn(brandModel);
        given(brandModel.getCode()).willReturn("apple");

        given(productModel.getProductType()).willReturn(productTypeModel);
        given(productTypeModel.getDigital()).willReturn(Boolean.FALSE);

        productImportIntegrationFacade.persistTargetProduct(dto);
        verify(colourVariantModel).setExcludeForAfterpay(true);


    }

    @Test
    public void testExcludeForAfterpayForSpecificProductTypeAndBrandForDigitalAppleProduct() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        dto.setName("GiftCard");
        dto.setProductType("digital");
        dto.setBrand("Apple");
        defaultMockSetup();

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        final BrandModel brandModel = mock(BrandModel.class);
        given(colourVariantModel.getBrand()).willReturn(brandModel);
        given(brandModel.getCode()).willReturn("apple");

        productImportIntegrationFacade.persistTargetProduct(dto);
        verify(colourVariantModel).setExcludeForAfterpay(true);


    }


    @Test
    public void testExcludeForAfterpayForSpecificProductTypeAndBrandForNotDigitalNorAppleProduct() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        dto.setProductType("normal");
        defaultMockSetup();

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        final BrandModel brandModel = mock(BrandModel.class);
        given(colourVariantModel.getBrand()).willReturn(brandModel);
        given(brandModel.getCode()).willReturn("target");

        productImportIntegrationFacade.persistTargetProduct(dto);
        verify(colourVariantModel, never()).setExcludeForAfterpay(true);


    }

    @Test
    public void testExcludeForAfterpayForSpecificProductTypeForisDigitalNull() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        dto.setProductType(null);
        defaultMockSetup();

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        final BrandModel brandModel = mock(BrandModel.class);
        given(colourVariantModel.getBrand()).willReturn(brandModel);
        given(brandModel.getCode()).willReturn("target");

        productImportIntegrationFacade.persistTargetProduct(dto);
        verify(colourVariantModel, never()).setExcludeForAfterpay(true);


    }

    @Test
    public void testExcludeForAfterpayForSpecificProductTypeAndBrandForDigitalProduct() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        dto.setProductType("digital");
        defaultMockSetup();

        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        final BrandModel brandModel = mock(BrandModel.class);
        given(colourVariantModel.getBrand()).willReturn(brandModel);
        given(brandModel.getCode()).willReturn("target");

        productImportIntegrationFacade.persistTargetProduct(dto);
        verify(colourVariantModel).setExcludeForAfterpay(true);


    }

    @Test
    public void testPersistTargetProductDataSuccessWithFluentFeed() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFluentEnabled();
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        final String primaryImage = "A12345";
        productMediaDto.setPrimaryImage(primaryImage);
        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);
        productDto.setProductMedia(productMediaDto);
        productDto.setAgeFrom(Double.valueOf(3d));
        productDto.setAgeTo(Double.valueOf(5d));
        productDto.setGenders(Lists.asList("girl", new String[] { "boy" }));
        productDto.setLicense("license");
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(primaryImage);
        productMediaObjects.setHasValidPrimaryImages(true);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.GRID_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.HERO_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LARGE_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.LIST_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.THUMB_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addPrimaryImages(TargetMediaImportService.FULL_MEDIA + primaryImage, mediaModel);
        productMediaObjects.addMediaContainers(mediaContainerModel);
        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages().getSecondaryImage(), null)).willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(colourVariantModel);
        given(variantService.getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE)).willReturn(variantTypeModel);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(warehouseService.getDefWarehouse()).willReturn(getWarehouses());
        productDto.setSizeType("Valid");
        final List<Feature> listOfFeatures = buildFeatures("age", null, "license");
        given(classificationService.getFeatures(productModel)).willReturn(new FeatureList(listOfFeatures));
        given(assignmentModel.getClassificationAttribute()).willReturn(classificationAttributeModel);
        given(classificationAttributeModel.getCode()).willReturn(TargetProductImportConstants.FEATURE_AGE_RANGE)
                .willReturn(TargetProductImportConstants.FEATURE_LICENSE);
        given(sizeTypeService.findSizeTypeForCode("Valid")).willReturn(sizetype);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(targetProductService).setOnlineDateIfApplicable(colourVariantModel);
        verify(fluentProductUpsertService).upsertProduct(productModel);
        verify(fluentSkuUpsertService).upsertSku(colourVariantModel);
    }

    @Test
    public void testPersistTargetProductDataSuccessWithFluentFeedForSizeVariants() {
        willReturn(Boolean.TRUE).given(targetFeatureSwitchFacade)
                .isFluentEnabled();
        defaultMockSetup();
        final IntegrationVariantDto variantDto = createVariantDto(productDto);
        productDto.addVariant(variantDto);

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);
        verify(fluentProductUpsertService).upsertProduct(productModel);
        verify(fluentSkuUpsertService).upsertSku(sizeVariantProductModel);
    }

    @Test
    public void testExcludeForAfterpayForPhysicalGiftcard() {

        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        dto.setName("Iphone");
        dto.setBrand("NonApple");
        dto.setProductType("normal");
        defaultMockSetup();

        willReturn(Boolean.TRUE).given(targetProductImportUtil).isProductPhysicalGiftcard(dto);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        final BrandModel brandModel = mock(BrandModel.class);
        given(colourVariantModel.getBrand()).willReturn(brandModel);
        given(brandModel.getCode()).willReturn("nonApple");

        given(productModel.getProductType()).willReturn(productTypeModel);
        given(productTypeModel.getDigital()).willReturn(Boolean.FALSE);

        final AbstractTargetVariantProductModel variant = mock(AbstractTargetVariantProductModel.class);
        given(variant.getCode()).willReturn("MOCK_PROD");
        mockWarehouseInfo(variant, "IncommWarehouse", "IncommWarehouse", "DTO_PRODCODE");
        dto.setWarehouses(productDto.getWarehouses());

        productImportIntegrationFacade.persistTargetProduct(dto);
        verify(colourVariantModel).setExcludeForAfterpay(true);


    }

    @Test
    public void testPersistTargetProductPhysicalGiftcard() {

        productDto.setProductType("normal");
        defaultMockSetup();

        willReturn(Boolean.TRUE).given(targetProductImportUtil).isProductPhysicalGiftcard(productDto);
        final AbstractTargetVariantProductModel variant = mock(AbstractTargetVariantProductModel.class);
        given(variant.getCode()).willReturn("MOCK_PROD");
        mockWarehouseInfo(variant, "IncommWarehouse", "IncommWarehouse", PRODUCT_CODE);

        given(modelService.create(TargetProductModel.class)).willReturn(productModel);
        given(targetFulfilmentWarehouseService.getGiftcardWarehouse()).willReturn(warehouseModel);

        mockGiftCardProductTypeModel();

        productImportIntegrationFacade.setAssociateMedia(false);
        productImportIntegrationFacade.setDigitalProductStock(100);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();


        verify(productModel).setProductType(productTypeCaptor.capture());
        assertThat(productTypeCaptor.getValue().getCode()).isEqualTo("giftCard");
        assertThat(productTypeCaptor.getValue().getBulky()).isFalse();
        assertThat(productTypeCaptor.getValue().getDigital()).isFalse();
        verify(stockService).getAllStockLevels(colourVariantModel);
        verify(stockService).updateActualStockLevel(colourVariantModel, warehouseModel, 100, StringUtils.EMPTY);

    }

    @Test
    public void testDeliveryModesForPhysicalGiftcard() {

        //given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        productDto.setProductType("normal");
        defaultMockSetup();

        willReturn(Boolean.TRUE).given(targetProductImportUtil).isProductPhysicalGiftcard(productDto);
        final AbstractTargetVariantProductModel variant = mock(AbstractTargetVariantProductModel.class);
        given(variant.getCode()).willReturn("MOCK_PROD");
        mockWarehouseInfo(variant, "IncommWarehouse", "IncommWarehouse", PRODUCT_CODE);

        given(modelService.create(TargetProductModel.class)).willReturn(productModel);

        mockGiftCardProductTypeModel();

        productImportIntegrationFacade.setAssociateMedia(false);
        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);
        assertThat(response).isNotNull();

        verify(productModel).setDeliveryModes(deliveryModesCaptor.capture());
        assertThat(deliveryModesCaptor.getValue()).hasSize(1);
        assertThat(deliveryModesCaptor.getValue()).contains(homeDeliveryMode);

    }

    @Test
    public void testPopulateMerchProductStatus() {
        productDto.setMerchProductStatus("Quit");
        final IntegrationVariantDto variantDto = createVariantDto(productDto);
        variantDto.setMerchProductStatus("Active");
        productDto.addVariant(variantDto);

        productImportIntegrationFacade.populateMerchProductStatus(productDto, variantDto, sizeVariantProductModel);
        verify(sizeVariantProductModel).setMerchProductStatus("Active");
    }

    @Test
    public void testPopulateMerchProductStatusWithoutVariant() {
        productDto.setMerchProductStatus("Quit");

        productImportIntegrationFacade.populateMerchProductStatus(productDto, null, sizeVariantProductModel);
        verify(sizeVariantProductModel).setMerchProductStatus("Quit");
    }

    @Test
    public void testPopulatePreOrderAttributes() throws ParseException {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        dto.setPreOrderEmbargoReleaseDate("2018-02-02 09:30:00");
        dto.setPreOrderStartDate("2018-02-02 09:30:00");
        dto.setPreOrderEndDate("2018-02-02 09:30:00");
        dto.setPreOrderOnlineQuantity(Integer.valueOf(20));

        final StockLevelModel stocklevel = mock(StockLevelModel.class);
        given(stockService.checkAndGetStockLevel(colourVariantModel, warehouseService.getDefaultOnlineWarehouse()))
                .willReturn(stocklevel);
        willDoNothing().given(productImportIntegrationFacade).setNormalSaleStartDateTimeOnOrder(anyString(),
                any(Date.class));

        productImportIntegrationFacade.persistTargetProduct(dto);

        verify(stockService).checkAndGetStockLevel(colourVariantModel,
                warehouseService.getDefaultOnlineWarehouse());

        verify(colourVariantModel).setNormalSaleStartDateTime(
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dto.getPreOrderEndDate()));
        verify(colourVariantModel).setPreOrderStartDateTime(
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dto.getPreOrderStartDate()));
        verify(colourVariantModel).setPreOrderEndDateTime(
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dto.getPreOrderEndDate()));
        verify(colourVariantModel).setMaxPreOrderQuantity(Integer.valueOf(20));
        verify(modelService).save(stocklevel);
        verify(productImportIntegrationFacade).setNormalSaleStartDateTimeOnOrder(colourVariantModel.getCode(),
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dto.getPreOrderEndDate()));
    }

    @Test
    public void testPopulatePreOrderAttributesWithoutStartAndEndDate() throws ParseException {
        final IntegrationProductDto dto = setupBasicIntegrationProductDto();
        defaultMockSetup();
        dto.setPreOrderEmbargoReleaseDate("2018-02-02 09:30:00");
        dto.setPreOrderOnlineQuantity(Integer.valueOf(20));

        final StockLevelModel stocklevel = mock(StockLevelModel.class);
        given(stockService.checkAndGetStockLevel(colourVariantModel, warehouseService.getDefaultOnlineWarehouse()))
                .willReturn(stocklevel);

        productImportIntegrationFacade.persistTargetProduct(dto);

        verify(stockService).checkAndGetStockLevel(colourVariantModel,
                warehouseService.getDefaultOnlineWarehouse());

        verify(colourVariantModel).setNormalSaleStartDateTime(
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dto.getPreOrderEmbargoReleaseDate()));
        verify(colourVariantModel, never()).setMaxPreOrderQuantity(Integer.valueOf(20));
        verify(stocklevel, never()).setMaxPreOrder(20);


    }

    @Test
    public void testPersistTargetProductWithWideMedia() {
        final IntegrationProductMediaDto productMediaDto = new IntegrationProductMediaDto();
        productMediaDto.setPrimaryImage("A123456");

        final IntegrationSecondaryImagesDto secImagesDto = new IntegrationSecondaryImagesDto();
        secImagesDto.addSecondaryImage("A568789");
        secImagesDto.addSecondaryImage("A568780");
        productMediaDto.setSecondaryImages(secImagesDto);

        productDto.setProductMedia(productMediaDto);

        final IntegrationWideImagesDto wideImagesDto = new IntegrationWideImagesDto();
        wideImagesDto.addWideImage("A568790");
        wideImagesDto.addWideImage("A568791");
        productMediaDto.setWideImages(wideImagesDto);

        productDto.setProductMedia(productMediaDto);
        final ProductMediaObjects productMediaObjects = new ProductMediaObjects(PRODUCT_CODE);
        productMediaObjects.setHasValidPrimaryImages(true);
        assertThat(productMediaDto.getSecondaryImages()).isNotNull();
        assertThat(productMediaDto.getWideImages()).isNotNull();

        given(targetMediaImportService.getMediaObjects(PRODUCT_CODE, productMediaDto.getPrimaryImage(), null,
                productMediaDto.getSecondaryImages().getSecondaryImage(), productMediaDto.getWideImages()
                        .getWideImage())).willReturn(productMediaObjects);
        given(productService.getProductForCode(PRODUCT_CODE)).willReturn(productModel);
        given(categoryService.getCategoryForCode(productDto.getPrimaryCategory())).willReturn(categoryModel);
        given(categoryService.getCategoryForCode(productDto.getSecondaryCategory().get(0))).willReturn(
                categoryModel);
        given(productService.getProductForCode(VARIANT_CODE)).willReturn(null);
        given(modelService.create(TargetColourVariantProductModel.class)).willReturn(colourVariantModel);
        given(colourService.getColourForFuzzyName(NO_COLOUR, true, false)).willReturn(colourModel);
        given(colourVariantModel.getCode()).willReturn(VARIANT_CODE);
        given(variantService.getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE))
                .willReturn(variantTypeModel);
        given(classificationService.getFeatures(productModel)).willReturn(
                new FeatureList(Collections.EMPTY_LIST));

        final IntegrationResponseDto response = productImportIntegrationFacade.persistTargetProduct(productDto);

        assertThat(response).isNotNull();
        assertThat(response.isSuccessStatus()).isEqualTo(Boolean.TRUE);

        final List<String> messages = response.getMessages();
        assertThat(messages).isNotNull();
        assertThat(messages.size()).isEqualTo(1);

        verify(modelService).save(productModel);
        verify(modelService, times(3)).save(colourVariantModel);
        verify(modelService).create(TargetColourVariantProductModel.class);
        verify(variantService).getVariantTypeForCode(TARGET_SIZE_VARIANT_PRODUCT_TYPE);
        verify(productService).getProductForCode(Mockito.anyString());
        verify(categoryService, times(3)).getCategoryForCode(Mockito.anyString());
        verifyNoMoreInteractions(modelService);
        verifyNoMoreInteractions(productService);
        verifyNoMoreInteractions(categoryService);
        verifyNoMoreInteractions(variantService);
    }

    @Test
    public void testSetNormalSaleStartDateTimeOnOrder() {
        final Date saleStartDateTime = mock(Date.class);
        final OrderModel order1 = mock(OrderModel.class);
        final OrderModel order2 = mock(OrderModel.class);
        willReturn(Arrays.asList(order1, order2)).given(targetOrderService)
                .findPendingPreOrdersForProduct("productCode");

        productImportIntegrationFacade.setNormalSaleStartDateTimeOnOrder("productCode", saleStartDateTime);
        verify(order1).setNormalSaleStartDateTime(saleStartDateTime);
        verify(order2).setNormalSaleStartDateTime(saleStartDateTime);
        verify(modelService).saveAll(Arrays.asList(order1, order2));
    }

    @Test
    public void testSetNormalSaleStartDateTimeOnNoOrders() {
        final Date saleStartDateTime = mock(Date.class);
        willReturn(Collections.emptyList()).given(targetOrderService)
                .findPendingPreOrdersForProduct("productCode");

        productImportIntegrationFacade.setNormalSaleStartDateTimeOnOrder("productCode", saleStartDateTime);
        verify(modelService, never()).saveAll(anyList());
    }

    private void mockGiftCardProductTypeModel() {
        given(productTypeService.getByCode("giftCard")).willReturn(productTypeModel);
        given(productTypeModel.getCode()).willReturn("giftCard");
        given(productTypeModel.getBulky()).willReturn(Boolean.FALSE);
        given(productTypeModel.getDigital()).willReturn(Boolean.FALSE);
    }


    private IntegrationVariantDto createVariantDto(final IntegrationProductDto dto) {
        final IntegrationVariantDto variant = new IntegrationVariantDto();
        variant.setBaseProduct(dto.getProductCode());
        variant.setName("test");
        variant.setSize("XS");
        variant.setVariantCode("43210");
        variant.setApprovalStatus("Y");
        return variant;
    }

    private void mockWarehouseInfo(final AbstractTargetVariantProductModel variant, final String actualWarehouse,
            final String expectedWarehouse, final String productCode) {
        final List<StockLevelModel> stockLevels = new ArrayList<StockLevelModel>();
        final StockLevelModel stocklevel = mock(StockLevelModel.class);
        given(stocklevel.getWarehouse()).willReturn(warehouseModel);
        given(warehouseModel.getCode()).willReturn(actualWarehouse);
        stockLevels.add(stocklevel);
        List<String> warehouses = null;

        if (expectedWarehouse != null) {
            warehouses = Collections.singletonList(expectedWarehouse);
        }

        given(stockService.getAllStockLevels(variant)).willReturn(stockLevels);
        productDto.setProductCode(productCode);
        productDto.setWarehouses(warehouses);
    }
}

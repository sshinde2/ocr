/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.services.impl;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFolderModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Before;
import org.junit.Ignore;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;


/**
 * @author rsamuel3
 * 
 */
@Ignore
public class TargetMediaImportServiceImplTest {

    @Mock
    private ModelService modelService;
    @Mock
    private MediaService mediaService;
    @Mock
    private MediaContainerService mediaContainerService;
    @Mock
    private TargetProductImportUtil targetProductImportUtil;

    @InjectMocks
    private final TargetMediaImportServiceImpl mediaImportService = new TargetMediaImportServiceImpl();

    @Mock
    private MediaModel mediaModel;

    @Mock
    private MediaFormatModel mediaFormatModel;


    @Mock
    private MediaFolderModel mediaFolderModel;

    @Mock
    private MediaContainerModel mediaContainerModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    //    @Test
    //    public void testPersistMediaMediaFolderNull() throws IOException {
    //        final String assetId = "1234";
    //        final String mediafolder = "folder";
    //        final String filename = "filename";
    //        final String mimetype = "mime";
    //        final MediaImageData imageData = new MediaImageData(new ByteArrayInputStream(new byte[0]), filename, mimetype);
    //        Mockito.when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(assetId);
    //        Mockito.when(mediaModel.getFolder()).thenReturn(null);
    //        Mockito.when(mediaService.getFolder(mediafolder)).thenReturn(mediaFolderModel);
    //        Mockito.when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerModel.getQualifier()).thenReturn(assetId);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(assetId)).thenReturn(mediaContainerModel);
    //
    //        mediaImportService.persistMedia(mediaModel, assetId, mediafolder, imageData);
    //
    //        Mockito.verify(mediaService).setStreamForMedia(mediaModel, imageData.getImageData(), imageData.getFilename(),
    //                imageData.getMimetype());
    //        Mockito.verify(mediaService).getFolder(mediafolder);
    //        Mockito.verify(modelService, Mockito.times(3)).save(mediaModel);
    //
    //        Mockito.verify(mediaContainerService).addMediaToContainer(mediaContainerModel,
    //                Collections.singletonList(mediaModel));
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(assetId);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testPersistMediaMediaFolderNullServiceReturnsNull() throws IOException {
    //        final String assetId = "1234";
    //        final String mediafolder = "folder";
    //        final String filename = "filename";
    //        final String mimetype = "mime";
    //        final MediaImageData imageData = new MediaImageData(new ByteArrayInputStream(new byte[0]), filename, mimetype);
    //        Mockito.when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(assetId);
    //        Mockito.when(mediaModel.getFolder()).thenReturn(null);
    //        Mockito.when(mediaService.getFolder(mediafolder)).thenReturn(null);
    //        Mockito.when(modelService.create(MediaFolderModel.class)).thenReturn(mediaFolderModel);
    //        Mockito.when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerModel.getQualifier()).thenReturn(assetId);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(assetId)).thenReturn(mediaContainerModel);
    //
    //        mediaImportService.persistMedia(mediaModel, assetId, mediafolder, imageData);
    //
    //        Mockito.verify(mediaService).setStreamForMedia(mediaModel, imageData.getImageData(), imageData.getFilename(),
    //                imageData.getMimetype());
    //        Mockito.verify(mediaService).getFolder(mediafolder);
    //        Mockito.verify(modelService, Mockito.times(3)).save(mediaModel);
    //        Mockito.verify(modelService).save(mediaFolderModel);
    //        Mockito.verify(modelService).create(MediaFolderModel.class);
    //
    //        Mockito.verify(mediaContainerService).addMediaToContainer(mediaContainerModel,
    //                Collections.singletonList(mediaModel));
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(assetId);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testPersistMediaMediaFolderUnIdentifierException() throws IOException {
    //        final String assetId = "1234";
    //        final String mediafolder = "folder";
    //        final String filename = "filename";
    //        final String mimetype = "mime";
    //        final MediaImageData imageData = new MediaImageData(new ByteArrayInputStream(new byte[0]), filename, mimetype);
    //        Mockito.when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(assetId);
    //        Mockito.when(mediaModel.getFolder()).thenReturn(null);
    //        Mockito.when(mediaService.getFolder(mediafolder)).thenThrow(new UnknownIdentifierException("blah"));
    //        Mockito.when(modelService.create(MediaFolderModel.class)).thenReturn(mediaFolderModel);
    //        Mockito.when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerModel.getQualifier()).thenReturn(assetId);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(assetId)).thenReturn(mediaContainerModel);
    //
    //        Mockito.when(mediaModel.getFolder()).thenReturn(null);
    //        Mockito.when(mediaFolderModel.getQualifier()).thenReturn(assetId);
    //        Mockito.when(mediaService.getFolder(assetId)).thenReturn(mediaFolderModel);
    //
    //        mediaImportService.persistMedia(mediaModel, assetId, mediafolder, imageData);
    //
    //        Mockito.verify(mediaService).setStreamForMedia(mediaModel, imageData.getImageData(), imageData.getFilename(),
    //                imageData.getMimetype());
    //        Mockito.verify(mediaService).getFolder(mediafolder);
    //
    //        Mockito.verify(modelService, Mockito.times(3)).save(mediaModel);
    //        Mockito.verify(modelService).save(mediaFolderModel);
    //        Mockito.verify(modelService).create(MediaFolderModel.class);
    //
    //        Mockito.verify(mediaContainerService).addMediaToContainer(mediaContainerModel,
    //                Collections.singletonList(mediaModel));
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(assetId);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testPersistMediaMediaFolderNotNull() throws IOException {
    //        final String assetId = "1234";
    //        final String mediafolder = "folder";
    //        final String filename = "filename";
    //        final String mimetype = "mime";
    //        final MediaImageData imageData = new MediaImageData(new ByteArrayInputStream(new byte[0]), filename, mimetype);
    //        Mockito.when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(assetId);
    //        Mockito.when(mediaModel.getFolder()).thenReturn(mediaFolderModel);
    //        Mockito.when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerModel.getQualifier()).thenReturn(assetId);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(assetId)).thenReturn(mediaContainerModel);
    //
    //
    //        mediaImportService.persistMedia(mediaModel, assetId, mediafolder, imageData);
    //
    //        Mockito.verify(mediaService).setStreamForMedia(mediaModel, imageData.getImageData(), imageData.getFilename(),
    //                imageData.getMimetype());
    //
    //        Mockito.verify(modelService, Mockito.times(3)).save(mediaModel);
    //
    //        Mockito.verify(mediaContainerService).addMediaToContainer(mediaContainerModel,
    //                Collections.singletonList(mediaModel));
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(assetId);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testPersistMediaMediaContainerNull() throws IOException {
    //        final String assetId = "1234";
    //        final String mediafolder = "folder";
    //        final String filename = "filename";
    //        final String mimetype = "mime";
    //        final MediaImageData imageData = new MediaImageData(new ByteArrayInputStream(new byte[0]), filename, mimetype);
    //        Mockito.when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(assetId);
    //        Mockito.when(mediaModel.getFolder()).thenReturn(mediaFolderModel);
    //        Mockito.when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerModel.getQualifier()).thenReturn(assetId);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(assetId)).thenReturn(null);
    //        Mockito.when(modelService.create(MediaContainerModel.class)).thenReturn(mediaContainerModel);
    //
    //
    //        mediaImportService.persistMedia(mediaModel, assetId, mediafolder, imageData);
    //
    //        Mockito.verify(mediaService).setStreamForMedia(mediaModel, imageData.getImageData(), imageData.getFilename(),
    //                imageData.getMimetype());
    //
    //        Mockito.verify(modelService, Mockito.times(3)).save(mediaModel);
    //        Mockito.verify(modelService).save(mediaContainerModel);
    //        Mockito.verify(modelService).create(MediaContainerModel.class);
    //
    //        Mockito.verify(mediaContainerService).addMediaToContainer(mediaContainerModel,
    //                Collections.singletonList(mediaModel));
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(assetId);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testPersistMediaMediaContainerUnIdentifiedException() throws IOException {
    //        final String assetId = "1234";
    //        final String mediafolder = "folder";
    //        final String filename = "filename";
    //        final String mimetype = "mime";
    //        final MediaImageData imageData = new MediaImageData(new ByteArrayInputStream(new byte[0]), filename, mimetype);
    //        Mockito.when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(assetId);
    //        Mockito.when(mediaModel.getFolder()).thenReturn(mediaFolderModel);
    //        Mockito.when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerModel.getQualifier()).thenReturn(assetId);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(assetId)).thenThrow(
    //                new UnknownIdentifierException("blah"));
    //        Mockito.when(modelService.create(MediaContainerModel.class)).thenReturn(mediaContainerModel);
    //
    //
    //        mediaImportService.persistMedia(mediaModel, assetId, mediafolder, imageData);
    //
    //        Mockito.verify(mediaService).setStreamForMedia(mediaModel, imageData.getImageData(), imageData.getFilename(),
    //                imageData.getMimetype());
    //
    //        Mockito.verify(modelService, Mockito.times(3)).save(mediaModel);
    //        Mockito.verify(modelService).save(mediaContainerModel);
    //        Mockito.verify(modelService).create(MediaContainerModel.class);
    //
    //        Mockito.verify(mediaContainerService).addMediaToContainer(mediaContainerModel,
    //                Collections.singletonList(mediaModel));
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(assetId);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //
    //    @Test
    //    public void testPersistMediaHappyPath() throws IOException {
    //        final String assetId = "1234";
    //        final String mediafolder = "folder";
    //        final String filename = "filename";
    //        final String mimetype = "mime";
    //        final MediaImageData imageData = new MediaImageData(new ByteArrayInputStream(new byte[0]), filename, mimetype);
    //        Mockito.when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(assetId);
    //        Mockito.when(mediaModel.getFolder()).thenReturn(mediaFolderModel);
    //        Mockito.when(mediaModel.getMediaContainer()).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerModel.getQualifier()).thenReturn(assetId);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(assetId)).thenReturn(mediaContainerModel);
    //
    //
    //        mediaImportService.persistMedia(mediaModel, assetId, mediafolder, imageData);
    //
    //        Mockito.verify(mediaService).setStreamForMedia(mediaModel, imageData.getImageData(), imageData.getFilename(),
    //                imageData.getMimetype());
    //
    //        Mockito.verify(modelService, Mockito.times(3)).save(mediaModel);
    //
    //        Mockito.verify(mediaContainerService).addMediaToContainer(mediaContainerModel,
    //                Collections.singletonList(mediaModel));
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(assetId);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testPersistMediaHappyPathVideo() throws IOException {
    //        final String assetId = "1234";
    //        Mockito.when(targetProductImportUtil.getAssetId(mediaModel.getCode())).thenReturn(assetId);
    //        Mockito.when(mediaModel.getFolder()).thenReturn(null);
    //        Mockito.when(mediaModel.getMediaContainer()).thenReturn(null);
    //
    //
    //        mediaImportService.persistMedia(mediaModel, null, null, null);
    //
    //        Mockito.verify(modelService).save(mediaModel);
    //
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testRemoveMediaDoesNotExist() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getMedia(code)).thenReturn(null);
    //
    //        mediaImportService.removeMedia(code);
    //
    //        Mockito.verify(mediaService).getMedia(code);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testRemoveMediaUnIdentifiedException() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getMedia(code)).thenThrow(new UnknownIdentifierException("blah"));
    //
    //        mediaImportService.removeMedia(code);
    //
    //        Mockito.verify(mediaService).getMedia(code);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testRemoveMediaHappyPath() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getMedia(code)).thenReturn(mediaModel);
    //
    //        mediaImportService.removeMedia(code);
    //
    //        Mockito.verify(mediaService).getMedia(code);
    //        Mockito.verify(modelService).remove(mediaModel);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testgetOrCreateMediaModelDoesNotExist() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getMedia(code)).thenReturn(null);
    //        Mockito.when(modelService.create(MediaModel.class)).thenReturn(mediaModel);
    //
    //        final MediaModel actual = mediaImportService.getOrCreateMediaModel(code);
    //
    //        Assert.assertEquals(mediaModel, actual);
    //
    //        Mockito.verify(mediaService).getMedia(code);
    //        Mockito.verify(modelService).create(MediaModel.class);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testgetOrCreateMediaModelUnIdentifierException() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getMedia(code)).thenThrow(new UnknownIdentifierException("blah"));
    //        Mockito.when(modelService.create(MediaModel.class)).thenReturn(mediaModel);
    //
    //        final MediaModel actual = mediaImportService.getOrCreateMediaModel(code);
    //
    //        Assert.assertEquals(mediaModel, actual);
    //
    //        Mockito.verify(mediaService).getMedia(code);
    //        Mockito.verify(modelService).create(MediaModel.class);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testgetOrCreateMediaModelValid() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getMedia(code)).thenReturn(mediaModel);
    //
    //        final MediaModel actual = mediaImportService.getOrCreateMediaModel(code);
    //
    //        Assert.assertEquals(mediaModel, actual);
    //
    //        Mockito.verify(mediaService).getMedia(code);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testgetMediaFormatModelCodeNull() {
    //        final MediaFormatModel formatModel = mediaImportService.getMediaFormatModel(null);
    //        Assert.assertNull(formatModel);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testgetMediaFormatModelCodeDoesNotExist() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getFormat(code)).thenReturn(null);
    //        final MediaFormatModel formatModel = mediaImportService.getMediaFormatModel(code);
    //        Assert.assertNull(formatModel);
    //
    //        Mockito.verify(mediaService).getFormat(code);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testgetMediaFormatModelCodeUnIdentifiedException() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getFormat(code)).thenThrow(new UnknownIdentifierException("blah"));
    //        final MediaFormatModel formatModel = mediaImportService.getMediaFormatModel(code);
    //        Assert.assertNull(formatModel);
    //
    //        Mockito.verify(mediaService).getFormat(code);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testgetMediaFormatModelValidPath() {
    //        final String code = "1234";
    //        Mockito.when(mediaService.getFormat(code)).thenReturn(mediaFormatModel);
    //        final MediaFormatModel formatModel = mediaImportService.getMediaFormatModel(code);
    //        Assert.assertNotNull(formatModel);
    //        Assert.assertEquals(mediaFormatModel, formatModel);
    //
    //        Mockito.verify(mediaService).getFormat(code);
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(modelService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsAllNulls() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(null);
    //        final String qualifierThumb = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierThumb)).thenReturn(null);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(null);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(null);
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(null);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenReturn(null);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(null);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(null);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(null);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                secondaryCodes);
    //        Assert.assertFalse(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(9, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(4)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsOnlyHeroMissing() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(null);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(mediaModel);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(mediaModel);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(mediaModel);
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                secondaryCodes);
    //        Assert.assertFalse(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(1, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(4)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsOnlyThumbMissing() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(mediaModel);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(null);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(mediaModel);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(mediaModel);
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                secondaryCodes);
    //        Assert.assertFalse(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(1, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(4)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsOnlyListMissing() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(mediaModel);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(mediaModel);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(mediaModel);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(null);
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                secondaryCodes);
    //        Assert.assertTrue(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(1, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(4)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsOnlyListNLargetMissing() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(mediaModel);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(mediaModel);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(null);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(null);
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                secondaryCodes);
    //        Assert.assertFalse(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(2, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(4)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsSecondaryImagesEmptyPrimImageAvailable() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(mediaModel);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(mediaModel);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(mediaModel);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(mediaModel);
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                new ArrayList<String>());
    //        Assert.assertTrue(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertNull(mediaObjects.getErrors());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsPrimImageNull() {
    //        final String productCode = "0987";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, null, null,
    //                secondaryCodes);
    //        Assert.assertFalse(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(1, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaContainerService, Mockito.times(3)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsPrimImageNSecImageNull() {
    //        final String productCode = "0987";
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, null, null, null);
    //        Assert.assertFalse(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(2, mediaObjects.getErrors().size());
    //
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //
    //    @Test
    //    public void testgetMediaObjectsAmbigousMediaModel() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(mediaModel);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(mediaModel);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(mediaModel);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenThrow(new AmbiguousIdentifierException("blah"));
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                secondaryCodes);
    //        Assert.assertTrue(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(1, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(4)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsAmbigousMediaContainer() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(mediaModel);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(mediaModel);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(mediaModel);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(mediaModel);
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenThrow(
    //                new AmbiguousIdentifierException("blah"));
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                secondaryCodes);
    //        Assert.assertTrue(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(1, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(4)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsAmbigousMediaContainerWithSwatchImage() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String swatchCode = "5678";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(mediaModel);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(mediaModel);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(mediaModel);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(mediaModel);
    //
    //        final String qualifierSwatch = TargetMediaImportService.SWATCH_MEDIA + swatchCode;
    //        Mockito.when(mediaService.getMedia(qualifierSwatch)).thenReturn(mediaModel);
    //
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenThrow(
    //                new AmbiguousIdentifierException("blah"));
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(swatchCode)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, swatchCode,
    //                secondaryCodes);
    //        Assert.assertTrue(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertEquals(1, mediaObjects.getErrors().size());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(5)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testgetMediaObjectsValid() {
    //        final String productCode = "0987";
    //        final String code = "1234";
    //        final String secondaryCode1 = "2345";
    //        final String secondaryCode2 = "3456";
    //        final String secondaryCode3 = "4567";
    //        final List<String> secondaryCodes = new ArrayList<>();
    //        secondaryCodes.add(secondaryCode1);
    //        secondaryCodes.add(secondaryCode2);
    //        secondaryCodes.add(secondaryCode3);
    //        final String qualifierHero = TargetMediaImportService.HERO_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierHero)).thenReturn(mediaModel);
    //        final String qualifier = TargetMediaImportService.THUMB_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifier)).thenReturn(mediaModel);
    //        final String qualifierLarge = TargetMediaImportService.LARGE_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierLarge)).thenReturn(mediaModel);
    //        final String qualifierList = TargetMediaImportService.LIST_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierList)).thenReturn(mediaModel);
    //        final String qualifierGrid = TargetMediaImportService.GRID_MEDIA + code;
    //        Mockito.when(mediaService.getMedia(qualifierGrid)).thenReturn(mediaModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode1)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode2)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(secondaryCode3)).thenReturn(
    //                mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(code)).thenReturn(
    //                mediaContainerModel);
    //
    //        final ProductMediaObjects mediaObjects = mediaImportService.getMediaObjects(productCode, code, null,
    //                secondaryCodes);
    //        Assert.assertTrue(mediaObjects.hasValidPrimaryImages());
    //        Assert.assertNull(mediaObjects.getErrors());
    //
    //        Mockito.verify(mediaService, Mockito.times(5)).getMedia(Mockito.anyString());
    //        Mockito.verify(mediaContainerService, Mockito.times(4)).getMediaContainerForQualifier(Mockito.anyString());
    //        Mockito.verifyNoMoreInteractions(mediaService);
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //    }
    //
    //    @Test
    //    public void testpopulatePrimaryMediaContainer() {
    //        final String qualifier = "1234";
    //        final String swatch = "3456";
    //
    //        final ProductMediaObjects mediaObjects = new ProductMediaObjects("A1234567");
    //
    //        final MediaContainerModel swatchContainer = Mockito.mock(MediaContainerModel.class);
    //
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(qualifier)).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(swatch)).thenReturn(swatchContainer);
    //        final List<MediaModel> mediasInContainer = createMediaCollection("A123456", true);
    //        Mockito.when(mediaContainerModel.getMedias()).thenReturn(mediasInContainer);
    //        final List<MediaModel> swatchInContainer = createMediaCollection("A345678", true);
    //        Mockito.when(swatchContainer.getMedias()).thenReturn(swatchInContainer);
    //
    //        mediaImportService.populatePrimaryMediaContainer(mediaObjects, qualifier, swatch);
    //
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(qualifier);
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(swatch);
    //        Mockito.verify(mediaContainerService).removeMediaFromContainer(Mockito.any(MediaContainerModel.class),
    //                Mockito.anyList());
    //        Mockito.verify(mediaContainerService).addMediaToContainer(Mockito.any(MediaContainerModel.class),
    //                Mockito.anyList());
    //
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //
    //    }
    //
    //    @Test
    //    public void testpopulatePrimaryMediaContainerNoSwatch() {
    //        final String qualifier = "1234";
    //
    //        final ProductMediaObjects mediaObjects = new ProductMediaObjects("A1234567");
    //
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(qualifier)).thenReturn(mediaContainerModel);
    //        final List<MediaModel> mediasInContainer = createMediaCollection("A123456", true);
    //        Mockito.when(mediaContainerModel.getMedias()).thenReturn(mediasInContainer);
    //
    //        mediaImportService.populatePrimaryMediaContainer(mediaObjects, qualifier, null);
    //
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(qualifier);
    //
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //
    //    }
    //
    //
    //
    //    @Test
    //    public void testpopulatePrimaryMediaContainerContainerHasNoSwatch() {
    //        final String qualifier = "1234";
    //        final String swatch = "3456";
    //
    //        final ProductMediaObjects mediaObjects = new ProductMediaObjects("A1234567");
    //
    //        final MediaContainerModel swatchContainer = Mockito.mock(MediaContainerModel.class);
    //
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(qualifier)).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(swatch)).thenReturn(swatchContainer);
    //        final List<MediaModel> mediasInContainer = createMediaCollection("A123456", false);
    //        Mockito.when(mediaContainerModel.getMedias()).thenReturn(mediasInContainer);
    //        final List<MediaModel> swatchInContainer = createMediaCollection("A345678", true);
    //        Mockito.when(swatchContainer.getMedias()).thenReturn(swatchInContainer);
    //
    //        mediaImportService.populatePrimaryMediaContainer(mediaObjects, qualifier, swatch);
    //
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(qualifier);
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(swatch);
    //        Mockito.verify(mediaContainerService).addMediaToContainer(Mockito.any(MediaContainerModel.class),
    //                Mockito.anyList());
    //
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //
    //    }
    //
    //    @Test
    //    public void testpopulatePrimaryMediaContainerNoSwatchMediaInSwatchContainer() {
    //        final String qualifier = "1234";
    //        final String swatch = "3456";
    //
    //        final ProductMediaObjects mediaObjects = new ProductMediaObjects("A1234567");
    //
    //        final MediaContainerModel swatchContainer = Mockito.mock(MediaContainerModel.class);
    //
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(qualifier)).thenReturn(mediaContainerModel);
    //        Mockito.when(mediaContainerService.getMediaContainerForQualifier(swatch)).thenReturn(swatchContainer);
    //        final List<MediaModel> mediasInContainer = createMediaCollection("A123456", true);
    //        Mockito.when(mediaContainerModel.getMedias()).thenReturn(mediasInContainer);
    //        final List<MediaModel> swatchInContainer = createMediaCollection("A345678", false);
    //        Mockito.when(swatchContainer.getMedias()).thenReturn(swatchInContainer);
    //
    //        mediaImportService.populatePrimaryMediaContainer(mediaObjects, qualifier, swatch);
    //
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(qualifier);
    //        Mockito.verify(mediaContainerService).getMediaContainerForQualifier(swatch);
    //
    //        Mockito.verifyNoMoreInteractions(mediaContainerService);
    //
    //    }
    //
    //
    //    /**
    //     * creates a collection of media
    //     * 
    //     * @param containerID
    //     *            id
    //     * @param hasSwatch
    //     *            whether the list should contain a swatch
    //     */
    //    private List<MediaModel> createMediaCollection(final String containerID, final boolean hasSwatch) {
    //        final List<MediaModel> medias = new ArrayList<>();
    //        final MediaModel model = new MediaModel();
    //        model.setCode(TargetMediaImportService.GRID_MEDIA + containerID);
    //        medias.add(model);
    //
    //        final MediaModel model1 = new MediaModel();
    //        model1.setCode(TargetMediaImportService.HERO_MEDIA + containerID);
    //        medias.add(model1);
    //
    //        final MediaModel model2 = new MediaModel();
    //        model2.setCode(TargetMediaImportService.LARGE_MEDIA + containerID);
    //        medias.add(model2);
    //
    //        final MediaModel model3 = new MediaModel();
    //        model3.setCode(TargetMediaImportService.LIST_MEDIA + containerID);
    //        medias.add(model3);
    //
    //        final MediaModel model4 = new MediaModel();
    //        model4.setCode(TargetMediaImportService.THUMB_MEDIA + containerID);
    //        medias.add(model4);
    //
    //        if (hasSwatch) {
    //            final MediaModel model5 = new MediaModel();
    //            model5.setCode(TargetMediaImportService.SWATCH_MEDIA + containerID);
    //            medias.add(model5);
    //        }
    //        return medias;
    //
    //    }
}

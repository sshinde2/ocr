/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.utility;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;



/**
 * @author rsamuel3
 * 
 */
@UnitTest
public class TargetProductImportUtilTest {

    @Mock
    private CatalogVersionService catalogVersionService;

    @Mock
    private CatalogVersionModel catalogVersionModel;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private Configuration configuration;

    @InjectMocks
    private final TargetProductImportUtil util = new TargetProductImportUtil();


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testcreateResponseSingleMessage() {
        final String errmessage = "bad response";
        final String code = "1234";
        final IntegrationResponseDto dto = util.createResponse(errmessage, code);
        Assert.assertNotNull(dto);
        Assert.assertFalse(dto.isSuccessStatus());
        final List<String> messages = dto.getMessages();
        Assert.assertTrue(CollectionUtils.isNotEmpty(messages));
        Assert.assertEquals(1, messages.size());
        Assert.assertEquals(errmessage, messages.get(0));
    }

    @Test
    public void testcreateResponseEmptyMessage() {
        final String code = "1234";
        final IntegrationResponseDto dto = util.createResponse(StringUtils.EMPTY, code);
        Assert.assertNotNull(dto);
        Assert.assertTrue(dto.isSuccessStatus());
        final List<String> messages = dto.getMessages();
        Assert.assertTrue(CollectionUtils.isEmpty(messages));
    }

    @Test
    public void testcreateResponsesSingleMessage() {
        final List<String> errMessages = new ArrayList<>();
        final String errmessage = "bad response";
        errMessages.add(errmessage);
        final String code = "1234";
        final IntegrationResponseDto dto = util.createResponse(false, errMessages, code);
        Assert.assertNotNull(dto);
        Assert.assertFalse(dto.isSuccessStatus());
        final List<String> messages = dto.getMessages();
        Assert.assertTrue(CollectionUtils.isNotEmpty(messages));
        Assert.assertEquals(1, messages.size());
        Assert.assertEquals(errmessage, messages.get(0));
    }

    @Test
    public void testcreateResponsesNullMessages() {
        final List<String> errMessages = null;
        final String code = "1234";
        final IntegrationResponseDto dto = util.createResponse(true, errMessages, code);
        Assert.assertNotNull(dto);
        Assert.assertTrue(dto.isSuccessStatus());
        final List<String> messages = dto.getMessages();
        Assert.assertTrue(CollectionUtils.isEmpty(messages));
    }

    @Test(expected = IOException.class)
    public void testcreateImageDataFileNotFoundException() throws IOException {
        final String folder = "test";
        final String fileName = "test";
        final String mimetype = "test";
        Mockito.when(configurationService.getConfiguration()).thenReturn(configuration);
        Mockito.when(configuration.getString(TargetProductImportUtil.BASE_FOLDER_IMAGES)).thenReturn("test");
        util.createImageData(folder, fileName, mimetype);
    }

    @Test
    public void getAssetIdtest() {
        final String code = "test";
        final String actualCode = util.getAssetId(code);
        Assert.assertNull(actualCode);
    }

    @Test
    public void getAssetIdValidtest() {
        final String code = "test/test1";
        final String actualCode = util.getAssetId(code);
        Assert.assertNotNull(actualCode);
        Assert.assertEquals("test1", actualCode);
    }

    @Test
    public void getProductCatalogStagedVersionNull() {
        Mockito.when(
                catalogVersionService.getCatalogVersion(TargetProductImportUtil.PRODUCT_CATALOG,
                        TargetProductImportUtil.STAGED_VERSION))
                .thenReturn(null);
        final CatalogVersionModel versionModel = util.getProductCatalogStagedVersion();
        Assert.assertNull(versionModel);
    }

    @Test
    public void getProductCatalogStagedVersionNotNull() {
        Mockito.when(
                catalogVersionService.getCatalogVersion(TargetProductImportUtil.PRODUCT_CATALOG,
                        TargetProductImportUtil.STAGED_VERSION))
                .thenReturn(catalogVersionModel);
        final CatalogVersionModel versionModel = util.getProductCatalogStagedVersion();
        Assert.assertNotNull(versionModel);
        Assert.assertEquals(catalogVersionModel, versionModel);
    }

    @Test
    public void getContentCatalogStagedVersionNull() {
        Mockito.when(
                catalogVersionService.getCatalogVersion(TargetProductImportUtil.CONTENT_CATALOG,
                        TargetProductImportUtil.STAGED_VERSION))
                .thenReturn(null);
        final CatalogVersionModel versionModel = util.getContentCatalogStagedVersion();
        Assert.assertNull(versionModel);
    }

    @Test
    public void getContentCatalogStagedVersionNotNull() {
        Mockito.when(
                catalogVersionService.getCatalogVersion(TargetProductImportUtil.CONTENT_CATALOG,
                        TargetProductImportUtil.STAGED_VERSION))
                .thenReturn(catalogVersionModel);
        final CatalogVersionModel versionModel = util.getContentCatalogStagedVersion();
        Assert.assertNotNull(versionModel);
        Assert.assertEquals(catalogVersionModel, versionModel);
    }

    @Test
    public void testPhysicalGiftcardTrue() {
        final IntegrationProductDto dto = new IntegrationProductDto();
        dto.setProductType(TargetProductImportConstants.PRODUCT_TYPE_NORMAL);
        final List<String> warehouses = new ArrayList<>();
        warehouses.add(TargetProductImportConstants.WAREHOUSE_INCOMM);
        dto.setWarehouses(warehouses);
        final boolean result = util.isProductPhysicalGiftcard(dto);
        assertThat(result).isTrue();
    }

    @Test
    public void testPhysicalGiftcardWhenNoWarehousePresent() {
        final IntegrationProductDto dto = new IntegrationProductDto();
        dto.setProductType(TargetProductImportConstants.PRODUCT_TYPE_NORMAL);
        final boolean result = util.isProductPhysicalGiftcard(dto);
        assertThat(result).isFalse();
    }

    @Test
    public void testPhysicalGiftcardWhenWarehouseIncommButProductTypeNotNormal() {
        final IntegrationProductDto dto = new IntegrationProductDto();
        dto.setProductType("hmd");
        final List<String> warehouses = new ArrayList<>();
        warehouses.add(TargetProductImportConstants.WAREHOUSE_INCOMM);
        dto.setWarehouses(warehouses);
        final boolean result = util.isProductPhysicalGiftcard(dto);
        assertThat(result).isFalse();
    }

    @Test
    public void testPhysicalGiftcardWhenWarehouseNotIncommButProductTypeNormal() {
        final IntegrationProductDto dto = new IntegrationProductDto();
        dto.setProductType(TargetProductImportConstants.PRODUCT_TYPE_NORMAL);
        final List<String> warehouses = new ArrayList<>();
        warehouses.add("CnpWarehouse");
        dto.setWarehouses(warehouses);
        final boolean result = util.isProductPhysicalGiftcard(dto);
        assertThat(result).isFalse();
    }


}

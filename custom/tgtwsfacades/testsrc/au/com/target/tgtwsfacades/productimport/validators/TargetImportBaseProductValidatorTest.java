/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.willReturn;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.ProductTypeModel;
import au.com.target.tgtcore.product.ProductTypeService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.productimport.TargetProductImportConstants;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetImportBaseProductValidatorTest {

    @Mock
    private ProductTypeService productTypeService;

    @Mock
    private TargetProductImportUtil targetProductImportUtil;

    @InjectMocks
    private final TargetImportBaseProductValidator validator = new TargetImportBaseProductValidator();

    @Mock
    private ProductTypeModel productTypeModel;

    private IntegrationProductDto productDto;

    @Before
    public void setup() {
        buildBaseProductDetails();
        BDDMockito.when(productTypeService.getByCode(Mockito.anyString())).thenReturn(productTypeModel);
        BDDMockito.when(Boolean.valueOf(targetProductImportUtil.isBulky(productTypeModel))).thenReturn(Boolean.FALSE);
        BDDMockito.when(targetProductImportUtil.getBooleanValue("Y")).thenReturn(Boolean.TRUE);
        BDDMockito.when(targetProductImportUtil.getBooleanValue("N")).thenReturn(Boolean.FALSE);
    }

    private void buildBaseProductDetails() {
        productDto = new IntegrationProductDto();
        productDto.setVariantCode("567890");

        // product code
        productDto.setProductCode("P12345");

        // brand
        productDto.setBrand("target");

        productDto.setApprovalStatus("approved");

        // available for layby
        productDto.setAvailableLayby("Y");

        // available for long-term layby
        productDto.setAvailableLongtermLayby("Y");

        // product Type
        productDto.setProductType(TargetProductImportConstants.PRODUCT_TYPE_NORMAL);

        // bulky product / correct product type
        productDto.setProductType("N");

        productDto.setAvailableCnc("Y");


        // available for MHD
        productDto.setAvailableHomeDelivery("Y");

        // if available for ED, must be normal product
        productDto.setAvailableExpressDelivery("Y");

        productDto.setGiftcardBrandId("test");

        productDto.setDenominaton(Double.valueOf(0d));

    }

    @Test
    public void testInvalidFields() {
        productDto = new IntegrationProductDto();
        productDto.setVariantCode("567890");
        productDto.setName("Test product");
        final List<String> errorMessages = validator.validate(productDto);

        Assertions
                .assertThat(errorMessages)
                .isNotEmpty()
                .isNotNull()
                .hasSize(6)
                .contains(
                        "Missing PRODUCT CODE (236245), aborting product/variant import for: Test product",
                        "Missing product/variant BRAND (49135), aborting product/variant import for: 567890",
                        "Missing product/variant APPROVAL STATUS (94267), aborting product/variant import for: 567890",
                        "Missing AVAILABLE FOR LAYBY flag (138956 - Toysale 7), aborting product/variant import for: 567890",
                        "Missing AVAILABLE FOR LONGTERM LAYBY flag (Attr_311962), aborting product/variant import for: 567890",
                        "Missing AVAILABLE FOR HOME DELIVERY flag (279381), aborting product/variant import for: 567890");

    }

    @Test
    public void testErrorForDigitalProducts() {
        productDto.setProductType(TargetProductImportConstants.PRODUCT_TYPE_DIGITAL);
        productDto.setGiftcardBrandId(null);
        productDto.setDenominaton(null);
        final List<String> errorMessages = validator.validate(productDto);
        Assertions
                .assertThat(errorMessages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .contains(
                        "Missing Brand ID for gift card product, aborting product/variant import for: 567890",
                        "Missing Denomination for gift card product, aborting product / variant import for: 567890");
    }

    @Test
    public void testExpressDeliveryBulky() {
        productDto.setBulky("Y");
        productDto.setAvailableExpressDelivery("Y");
        BDDMockito.when(Boolean.valueOf(targetProductImportUtil.isBulky(productTypeModel))).thenReturn(Boolean.TRUE);
        final List<String> errorMessages = validator.validate(productDto);
        Assertions
                .assertThat(errorMessages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .contains(
                        "It is a bulky product with express-delivery enabled, aborting product/variant import for: 567890");
    }

    @Test
    public void testValidFields() {
        final List<String> errorMessages = validator.validate(productDto);
        Assertions.assertThat(errorMessages).isNullOrEmpty();
    }

    @Test
    public void testErrorForPhysicalGiftcardProducts() {

        willReturn(Boolean.TRUE).given(targetProductImportUtil).isProductPhysicalGiftcard(productDto);
        productDto.setProductType(TargetProductImportConstants.PRODUCT_TYPE_NORMAL);
        final ArrayList<String> warehouses = new ArrayList<>();
        warehouses.add(TargetProductImportConstants.WAREHOUSE_INCOMM);
        productDto.setWarehouses(warehouses);
        productDto.setGiftcardBrandId(null);
        productDto.setDenominaton(null);
        final List<String> errorMessages = validator.validate(productDto);
        assertThat(errorMessages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .contains(
                        "Missing Brand ID for gift card product, aborting product/variant import for: 567890",
                        "Missing Denomination for gift card product, aborting product / variant import for: 567890");
    }
}

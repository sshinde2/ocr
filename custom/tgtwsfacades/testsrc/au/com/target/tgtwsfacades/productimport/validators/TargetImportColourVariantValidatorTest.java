/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfacades.featureswitch.TargetFeatureSwitchFacade;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetImportColourVariantValidatorTest {

    private IntegrationProductDto productDto;

    @Mock
    private TargetFeatureSwitchFacade featureSwitchFacade;

    @InjectMocks
    private final TargetImportColourVariantValidator validator = new TargetImportColourVariantValidator();


    @Before
    public void setup() {
        Mockito.when(Boolean.valueOf(featureSwitchFacade.isSizeOrderingEnabled())).thenReturn(Boolean.FALSE);
        productDto = new IntegrationProductDto();
        productDto.setVariantCode("567890");
        validator.setValidators(new ArrayList<TargetProductImportValidator>());
    }

    @Test
    public void testInvalidMessages() {
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .contains("Missing PRODUCT NAME (49127), aborting product/variant import for: 567890",
                        "Missing PRODUCT DESCRIPTION (49142), aborting product/variant import for: 567890");
    }

    @Test
    public void testVariantError() {
        productDto.setName("Test name");
        productDto.setDescription("test description");
        final IntegrationVariantDto variantDto = new IntegrationVariantDto();
        variantDto.setVariantCode("456789");
        productDto.addVariant(variantDto);
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .contains("Missing variant APPROVAL STATUS (94267), aborting product/variant import for: 456789");
    }

    @Test
    public void testValid() {
        Mockito.when(Boolean.valueOf(featureSwitchFacade.isSizeOrderingEnabled())).thenReturn(Boolean.TRUE);
        productDto.setName("Test name");
        productDto.setDescription("test description");
        productDto.setSizeGroup("testGroup");
        final IntegrationVariantDto variantDto = new IntegrationVariantDto();
        variantDto.setApprovalStatus("approved");
        productDto.addVariant(variantDto);
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNullOrEmpty();
    }

    @Test
    public void testValidWithSizeFeatureOff() {
        Mockito.when(Boolean.valueOf(featureSwitchFacade.isSizeOrderingEnabled())).thenReturn(Boolean.FALSE);
        productDto.setName("Test name");
        productDto.setDescription("test description");
        productDto.setSizeGroup("");
        final IntegrationVariantDto variantDto = new IntegrationVariantDto();
        variantDto.setApprovalStatus("approved");
        productDto.addVariant(variantDto);
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNullOrEmpty();
    }

    @Test
    public void testValidateSizeGroupEmpty() {
        setProductDetails(productDto);
        final IntegrationVariantDto variantDto = new IntegrationVariantDto();
        variantDto.setApprovalStatus("approved");
        productDto.addVariant(variantDto);
        productDto.setSizeGroup("");
        final String message = validator.validProductForSizeGroup(productDto);
        Assertions
                .assertThat(message)
                .isNotNull()
                .contains("Missing SizeGroup, aborting product/variant import for: 1234");
    }

    @Test
    public void testValidateSizeGroup() {
        setProductDetails(productDto);
        final IntegrationVariantDto variantDto = new IntegrationVariantDto();
        variantDto.setApprovalStatus("approved");
        productDto.addVariant(variantDto);
        productDto.setSizeGroup("testGroup");
        final String message = validator.validProductForSizeGroup(productDto);
        Assertions
                .assertThat(message)
                .isEmpty();
    }

    @Test
    public void testValidateSizeGroupWithoutVariants() {
        setProductDetails(productDto);
        productDto.setSizeGroup("");
        final String message = validator.validProductForSizeGroup(productDto);
        Assertions
                .assertThat(message)
                .isEmpty();
    }

    @Test
    public void testValidatewithoutSizeGroup() {
        setProductDetails(productDto);
        final IntegrationVariantDto variantDto = new IntegrationVariantDto();
        variantDto.setApprovalStatus("approved");
        productDto.addVariant(variantDto);
        final String message = validator.validProductForSizeGroup(productDto);
        Assertions
                .assertThat(message)
                .isNotNull()
                .contains("Missing SizeGroup, aborting product/variant import for: 1234");
    }

    private void setProductDetails(final IntegrationProductDto dto) {
        dto.setName("Test name");
        dto.setDescription("test description");
        dto.setProductCode("1234");
    }
}

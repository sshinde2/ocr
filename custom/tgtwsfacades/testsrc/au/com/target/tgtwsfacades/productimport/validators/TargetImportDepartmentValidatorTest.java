/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.constants.TgtCoreConstants;
import au.com.target.tgtcore.model.TargetMerchDepartmentModel;
import au.com.target.tgtcore.product.TargetProductDepartmentService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetImportDepartmentValidatorTest {

    @Mock
    private TargetProductDepartmentService targetProductDepartmentService;

    @Mock
    private CatalogVersionService catalogVersionService;

    @InjectMocks
    private final TargetImportDepartmentValidator validator = new TargetImportDepartmentValidator();

    @Mock
    private TargetMerchDepartmentModel merchDept;

    private IntegrationProductDto productDto;

    @Mock
    private CatalogVersionModel catalogVersion;

    @Before
    public void setup() {
        productDto = new IntegrationProductDto();
        productDto.setVariantCode("567890");
    }

    @Test
    public void testDepartmentNull() {
        final List<String> messages = validator.validate(productDto);
        Assertions.assertThat(messages).isNotNull().isNotEmpty().hasSize(1)
                .contains("Missing DEPARTMENT NUMBER (Attr_308459), aborting product/variant import for: 567890");
    }

    @Test
    public void testDepartmentMissing() {
        final Integer dept = Integer.valueOf(777);
        productDto.setDepartment(dept);
        BDDMockito.when(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).thenReturn(catalogVersion);
        BDDMockito.when(targetProductDepartmentService
                .getDepartmentCategoryModel(catalogVersion, dept)).thenReturn(null);
        final List<String> messages = validator.validate(productDto);
        Assertions.assertThat(messages).isNotNull().isNotEmpty().hasSize(1)
                .contains("Department 777 does not exists in hybris, aborting product/variant import for 567890");
    }

    @Test
    public void testValid() {
        final Integer dept = Integer.valueOf(777);
        productDto.setDepartment(dept);
        BDDMockito.when(catalogVersionService.getCatalogVersion(TgtCoreConstants.Catalog.PRODUCTS,
                TgtCoreConstants.Catalog.OFFLINE_VERSION)).thenReturn(catalogVersion);
        BDDMockito.when(targetProductDepartmentService
                .getDepartmentCategoryModel(catalogVersion, dept)).thenReturn(merchDept);
        final List<String> messages = validator.validate(productDto);
        Assertions.assertThat(messages).isNullOrEmpty();
    }
}

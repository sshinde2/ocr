/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.warehouse.service.TargetWarehouseService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;
import au.com.target.tgtwsfacades.productimport.utility.TargetProductImportUtil;


/**
 * @author mjanarth
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetImportSellableVariantDisplayOnlyValidatorTest {

    private static final String VALIDATION_ERR_MESSAGE_ONLINEEXCLUSIVE = "Display only product cannot be online exclusive, aborting product/variant import for: {0} ";
    private static final String VALIDATION_ERR_MESSAGE_DIGITAL = "Display only product cannot be digital, aborting product/variant import for: {0} ";
    private static final String VALIDATION_ERR_MESSAGE_DROPSHIP = "Display only product cannot be dropship product, aborting product/variant import for: {0} ";
    private static final String VALIDATION_ERR_MESSAGE_EBAY = "Display only product cannot be available on ebay, aborting product/variant import for: {0} ";
    @Mock
    private TargetWarehouseService warehouseService;

    @Mock
    private WarehouseModel warehouse;

    @Mock
    private WarehouseModel fastlineWarehouse;

    private final TargetProductImportUtil targetProductImportUtil = new TargetProductImportUtil();

    @InjectMocks
    private final TargetImportSellableVariantDisplayOnlyValidator validator = new TargetImportSellableVariantDisplayOnlyValidator();

    private final IntegrationProductDto productDto = new IntegrationProductDto();
    private final String sizeVariantCode = "1234";

    @Before
    public void setup() {
        productDto.setVariantCode("5678");
        when(warehouseService.getWarehouseForCode("cnpwarehouse")).thenReturn(warehouse);
        when(warehouseService.getWarehouseForCode("fastline")).thenReturn(fastlineWarehouse);
        doReturn(Boolean.FALSE).when(fastlineWarehouse).isDropShipWarehouse();
        doReturn(Boolean.TRUE).when(warehouse).isDropShipWarehouse();
        validator.setTargetProductImportUtil(targetProductImportUtil);
    }

    @Test
    public void testValidateDisplayOnlyCVOnlineExAndDropShip() {
        productDto.setDisplayOnly("Y");
        productDto.setArticleStatus("onlineexclusive");
        productDto.setWarehouses(Collections.singletonList("cnpwarehouse"));
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .contains(MessageFormat.format(VALIDATION_ERR_MESSAGE_DROPSHIP, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_ONLINEEXCLUSIVE, productDto.getVariantCode()));
    }

    @Test
    public void testValidateDisplayOnlyCVDigitalAndAvailableOnEbay() {
        productDto.setDisplayOnly("Y");
        productDto.setAvailableOnEbay("Y");
        productDto.setProductType("digital");
        productDto.setWarehouses(Collections.singletonList(""));
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .contains(MessageFormat.format(VALIDATION_ERR_MESSAGE_EBAY, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_DIGITAL, productDto.getVariantCode()));
    }

    @Test
    public void testValidateNotDisplayOnlyCVDigitalAndAvailableOnEbay() {
        productDto.setDisplayOnly("N");
        productDto.setAvailableOnEbay("Y");
        productDto.setProductType("digital");
        productDto.setWarehouses(Collections.singletonList(""));
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void testValidateDisplayOnlyCVAllValid() {
        productDto.setDisplayOnly("Y");
        productDto.setAvailableOnEbay("N");
        productDto.setProductType("bulky");
        productDto.setWarehouses(Collections.singletonList(""));
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void testValidateDisplayOnlySizeOnlyOnlineExAndDropShip() {
        productDto.setDisplayOnly("Y");
        productDto.setIsSizeOnly(Boolean.TRUE);
        productDto.setArticleStatus("onlineexclusive");
        productDto.setWarehouses(Collections.singletonList("cnpwarehouse"));
        productDto.setProductType("digital");
        productDto.setAvailableOnEbay("Y");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(4)
                .contains(MessageFormat.format(VALIDATION_ERR_MESSAGE_DROPSHIP, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_ONLINEEXCLUSIVE, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_DIGITAL, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_EBAY, productDto.getVariantCode()));
    }

    @Test
    public void testValidateDisplayOnlySizeOnlyAllValid() {
        productDto.setDisplayOnly("Y");
        productDto.setIsSizeOnly(Boolean.TRUE);
        productDto.setAvailableOnEbay("N");
        productDto.setWarehouses(Collections.singletonList("fastline"));
        productDto.setProductType("bulky");
        productDto.setWarehouses(Collections.singletonList(""));
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void testValidateDisplayOnlySVNotvalid() {
        productDto.setIsSizeOnly(Boolean.FALSE);
        productDto.setWarehouses(Collections.singletonList("cnpwarehouse"));
        productDto.setProductType("digital");
        productDto.setArticleStatus("onlineexclusive");
        addSizeVariant(productDto, "Y", "Y");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(4)
                .contains(MessageFormat.format(VALIDATION_ERR_MESSAGE_DROPSHIP, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_ONLINEEXCLUSIVE, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_DIGITAL, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_EBAY, sizeVariantCode));
    }


    @Test
    public void testValidateDisplayOnlyOnColorVariantSVNotvalid() {
        productDto.setIsSizeOnly(Boolean.FALSE);
        productDto.setWarehouses(Collections.singletonList("cnpwarehouse"));
        productDto.setProductType("digital");
        productDto.setArticleStatus("onlineexclusive");
        productDto.setDisplayOnly("Y");
        addSizeVariant(productDto, "Y", "");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(4)
                .contains(MessageFormat.format(VALIDATION_ERR_MESSAGE_DROPSHIP, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_ONLINEEXCLUSIVE, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_DIGITAL, productDto.getVariantCode()),
                        MessageFormat.format(VALIDATION_ERR_MESSAGE_EBAY, sizeVariantCode));
    }


    @Test
    public void testValidateNotDisplayOnlyOnColorVariantSVvalid() {
        productDto.setIsSizeOnly(Boolean.FALSE);
        productDto.setWarehouses(Collections.singletonList("cnpwarehouse"));
        productDto.setProductType("digital");
        productDto.setArticleStatus("onlineexclusive");
        productDto.setDisplayOnly("N");
        addSizeVariant(productDto, "Y", "");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void testValidateDisplayOnlyOnBothColorVariantSVvalid() {
        productDto.setIsSizeOnly(Boolean.FALSE);
        productDto.setWarehouses(Collections.singletonList("cnpwarehouse"));
        productDto.setProductType("digital");
        productDto.setArticleStatus("onlineexclusive");
        productDto.setDisplayOnly("Y");
        addSizeVariant(productDto, "Y", "N");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void testValidateDisplayOnlySVvalid() {
        productDto.setIsSizeOnly(Boolean.FALSE);
        productDto.setWarehouses(Collections.singletonList("fastline"));
        productDto.setProductType("bulky");
        addSizeVariant(productDto, "N", "Y");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isEmpty();
    }

    @Test
    public void testValidateDisplayOnlyEbayOnColorVariantSVInvalid() {
        productDto.setIsSizeOnly(Boolean.FALSE);
        productDto.setAvailableOnEbay("Y");
        addSizeVariant(productDto, "", "Y");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .contains(MessageFormat.format(VALIDATION_ERR_MESSAGE_EBAY, sizeVariantCode));

    }

    @Test
    public void testValidateDisplayOnlyEbayOnSVInvalid() {
        productDto.setIsSizeOnly(Boolean.FALSE);
        addSizeVariant(productDto, "Y", "Y");
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .contains(MessageFormat.format(VALIDATION_ERR_MESSAGE_EBAY, sizeVariantCode));

    }

    private void addSizeVariant(final IntegrationProductDto dto, final String isAvailableOnEbay,
            final String displayOnly) {
        final IntegrationVariantDto variantDto = new IntegrationVariantDto();
        variantDto.setSize("XS");
        variantDto.setName("Peppa Pig Shorts");
        variantDto.setVariantCode(sizeVariantCode);
        variantDto.setAvailableOnEbay(isAvailableOnEbay);
        variantDto.setDisplayOnly(displayOnly);
        dto.addVariant(variantDto);

    }
}

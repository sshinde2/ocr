/**
 * 
 */
package au.com.target.tgtwsfacades.productimport.validators;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.List;

import org.fest.assertions.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.SizeTypeModel;
import au.com.target.tgtcore.product.TargetSizeTypeService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationVariantDto;


/**
 * @author rsamuel3
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetImportSizeVariantValidatorTest {

    @Mock
    private TargetSizeTypeService sizeTypeService;

    @InjectMocks
    private final TargetImportSizeVariantValidator validator = new TargetImportSizeVariantValidator();

    private IntegrationProductDto productDto;

    @Mock
    private SizeTypeModel sizeTypeModel;

    @Before
    public void setup() {
        productDto = new IntegrationProductDto();
        productDto.setVariantCode("567890");
    }

    @Test
    public void testValidMessages() {
        final List<String> messages = validator.validate(productDto);
        Assertions.assertThat(messages).isNullOrEmpty();
    }

    @Test
    public void testSizeOnlyButNoSize() {
        productDto.setIsSizeOnly(Boolean.TRUE);
        final List<String> messages = validator.validate(productDto);
        Assertions
                .assertThat(messages)
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .contains("Missing SIZE TYPE definition (49129), aborting product/variant import for: 567890",
                        "Missing SIZE value (49128), aborting product/variant import for: 567890");
    }

    @Test
    public void testNotSizeOnlyWithDefinedSize() {
        productDto.setIsSizeOnly(Boolean.FALSE);
        productDto.setSizeType("babysize");
        BDDMockito.given(sizeTypeService.findSizeTypeForCode("babysize")).willReturn(sizeTypeModel);
        final IntegrationVariantDto variantDto = new IntegrationVariantDto();
        variantDto.setVariantCode("546789");
        productDto.addVariant(variantDto);
        final List<String> messages = validator.validate(productDto);
        Assertions.assertThat(messages).isNotNull().isNotEmpty().hasSize(1)
                .contains("Missing SIZE value (49128) for size variant, aborting product/variant import for: 546789");
    }
}

/**
 * 
 */
package au.com.target.tgtwsfacades.shipconfirm.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.model.TargetConsignmentModel;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtwsfacades.helper.OrderConsignmentHelper;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.OrderConfirm;
import au.com.target.tgtwsfacades.integration.dto.ShipConfirm;


/**
 * @author rsamuel3
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShipConfirmIntegrationFacadeImplTest {

    private static final String ORDER_NUMBER = "12345";

    @Mock
    private TargetFulfilmentService targetFulfilmentService;

    @Mock
    private OrderConsignmentHelper orderConsignmentHelper;

    @InjectMocks
    private final ShipConfirmIntegrationFacadeImpl integrationFacade = new ShipConfirmIntegrationFacadeImpl();

    @Mock
    private TargetConsignmentModel cons;

    @Mock
    private OrderModel order;

    @Mock
    private WarehouseModel warehouse;

    @Before
    public void setUp() {
        Mockito.when(orderConsignmentHelper.getConsignmentBasedOrderOrConsignmentCode(ORDER_NUMBER)).thenReturn(cons);
        Mockito.when(cons.getOrder()).thenReturn(order);
        Mockito.when(order.getCode()).thenReturn(ORDER_NUMBER);
        Mockito.when(cons.getWarehouse()).thenReturn(warehouse);
    }

    @Test
    public void handleShipConfirmNullValue() {
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(null);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertEquals("ERR_SHIPCONF_NOORDERS - No order confirm found from ShipConfirm instance", response
                .getMessages().get(0));
    }

    @Test
    public void handleShipConfirmOrderConfirmNull() {
        final ShipConfirm confirm = new ShipConfirm();
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(confirm);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertEquals("ERR_SHIPCONF_NOORDERS - No order confirm found from ShipConfirm instance", response
                .getMessages().get(0));
    }

    @Test
    public void handleShipConfirmOrderConfirmEmpty() {
        final ShipConfirm confirm = new ShipConfirm();
        confirm.setOrderConfirms(new ArrayList<OrderConfirm>());
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(confirm);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertEquals("ERR_SHIPCONF_NOORDERS - No order confirm found from ShipConfirm instance", response
                .getMessages().get(0));
    }

    @Test
    public void handleShipConfirmOrderConfirmSingleEntry() {
        final ShipConfirm confirm = new ShipConfirm();
        final ArrayList<OrderConfirm> orderConfirms = new ArrayList<>();
        final OrderConfirm confirm1 = new OrderConfirm();
        confirm1.setOrderNumber(ORDER_NUMBER);
        confirm1.setManifestNumber("MANFEST1");
        orderConfirms.add(confirm1);
        confirm.setOrderConfirms(orderConfirms);
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(confirm);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertTrue(response.isSuccessStatus());
        Assert.assertTrue(CollectionUtils.isEmpty(response.getMessages()));
    }

    @Test
    public void handleShipConfirmOrderConfirmSingleEntryNullOrderCode() {
        final ShipConfirm confirm = new ShipConfirm();
        final ArrayList<OrderConfirm> orderConfirms = new ArrayList<>();
        final OrderConfirm confirm1 = new OrderConfirm();
        confirm1.setOrderNumber(null);
        confirm1.setManifestNumber("MANFEST1");
        orderConfirms.add(confirm1);
        confirm.setOrderConfirms(orderConfirms);
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(confirm);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertFalse(response.isSuccessStatus());
    }

    @Test
    public void handleShipConfirmOrderConfirmException() throws Exception {
        final ShipConfirm confirm = new ShipConfirm();
        final ArrayList<OrderConfirm> orderConfirms = new ArrayList<>();
        final OrderConfirm confirm1 = new OrderConfirm();
        confirm1.setOrderNumber(ORDER_NUMBER);
        confirm1.setManifestNumber("MANFEST1");
        orderConfirms.add(confirm1);
        confirm.setOrderConfirms(orderConfirms);
        Mockito.doThrow(new FulfilmentException("test exception"))
                .when(targetFulfilmentService)
                .processShipConfForConsignment(Mockito.eq(ORDER_NUMBER), Mockito.eq(cons), Mockito.any(Date.class),
                        Mockito.any(LoggingContext.class));
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(confirm);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertFalse(CollectionUtils.isEmpty(response.getMessages()));
    }

    /**
     * Verifies that if order validation throws AmbiguousIdentifierException a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test
    public void handleShipConfirmOrderConfirmThrowsAmbiguousIdException() throws Exception {
        final ShipConfirm confirm = new ShipConfirm();
        final ArrayList<OrderConfirm> orderConfirms = new ArrayList<>();
        final OrderConfirm confirm1 = new OrderConfirm();
        confirm1.setOrderNumber(ORDER_NUMBER);
        confirm1.setManifestNumber("MANFEST1");
        orderConfirms.add(confirm1);
        confirm.setOrderConfirms(orderConfirms);
        Mockito.doThrow(new AmbiguousIdentifierException("test ambiguous id exception"))
                .when(targetFulfilmentService)
                .processShipConfForConsignment(Mockito.eq(ORDER_NUMBER), Mockito.eq(cons),
                        Mockito.any(Date.class), Mockito.any(LoggingContext.class));
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(confirm);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertFalse(CollectionUtils.isEmpty(response.getMessages()));
    }

    /**
     * Verifies that if order validation throws UnknownIdentifierException a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test
    public void handleShipConfirmOrderConfirmThrowsUnknownIdException() throws Exception {
        final ShipConfirm confirm = new ShipConfirm();
        final ArrayList<OrderConfirm> orderConfirms = new ArrayList<>();
        final OrderConfirm confirm1 = new OrderConfirm();
        confirm1.setOrderNumber(ORDER_NUMBER);
        confirm1.setManifestNumber("MANFEST1");
        orderConfirms.add(confirm1);
        confirm.setOrderConfirms(orderConfirms);
        Mockito.doThrow(new UnknownIdentifierException("test unknown id exception"))
                .when(targetFulfilmentService)
                .processShipConfForConsignment(Mockito.eq(ORDER_NUMBER), Mockito.eq(cons),
                        Mockito.any(Date.class), Mockito.any(LoggingContext.class));
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(confirm);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertFalse(CollectionUtils.isEmpty(response.getMessages()));
    }

    /**
     * Verifies that if a generic exception occurs a negative response will be returned.
     * 
     * @throws Exception
     */
    @Test(expected = Exception.class)
    public void handleShipConfirmOrderConfirmThrowsGenericException() throws Exception {
        final ShipConfirm confirm = new ShipConfirm();
        final ArrayList<OrderConfirm> orderConfirms = new ArrayList<>();
        final OrderConfirm confirm1 = new OrderConfirm();
        confirm1.setOrderNumber(ORDER_NUMBER);
        confirm1.setManifestNumber("MANFEST1");
        orderConfirms.add(confirm1);
        confirm.setOrderConfirms(orderConfirms);
        Mockito.doThrow(new Exception("test exception"))
                .when(targetFulfilmentService)
                .processShipConfForConsignment(Mockito.eq(ORDER_NUMBER), Mockito.eq(cons),
                        Mockito.any(Date.class), Mockito.any(LoggingContext.class));
        final List<IntegrationResponseDto> responses = integrationFacade.handleShipConfirm(confirm);
        Assert.assertNotNull(responses);
        Assert.assertEquals(1, responses.size());
        final IntegrationResponseDto response = responses.get(0);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertFalse(CollectionUtils.isEmpty(response.getMessages()));
    }
}

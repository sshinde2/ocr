/**
 * 
 */
package au.com.target.tgtwsfacades.stlimport.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.hamcrest.text.StringContains;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import au.com.target.tgtmarketing.shopthelook.LookCollectionService;
import au.com.target.tgtmarketing.shopthelook.LookProductService;
import au.com.target.tgtmarketing.shopthelook.LookService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationShopTheLookDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationSupercategoryDto;


/**
 * @author mgazal
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class TargetShopTheLookImportIntegrationFacadeUnitTest {

    private static final boolean ASSOCIATE_MEDIA = true;

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    @Mock
    private ModelService modelService;

    @Mock
    private CategoryService categoryService;

    @Mock
    private LookCollectionService lookCollectionService;

    @Mock
    private LookService lookService;

    @Mock
    private LookProductService lookProductService;

    @InjectMocks
    @Spy
    private final TargetShopTheLookImportIntegrationFacade collectionImportIntegrationFacade = new TargetShopTheLookImportIntegrationFacade();

    @Before
    public void setup() {
        collectionImportIntegrationFacade.setAssociateMedia(ASSOCIATE_MEDIA);
        collectionImportIntegrationFacade.setDateFormat(DATE_FORMAT);
        Mockito.when(modelService.create(Mockito.any(Class.class))).thenAnswer(new Answer<Object>() {

            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                return ((Class)invocation.getArguments()[0]).newInstance();
            }
        });
    }

    @Test
    public void testMissingCode() {
        final IntegrationShopTheLookDto dto = new IntegrationShopTheLookDto();
        dto.setName("Shop The Look 1");
        dto.setSupercategory(new IntegrationSupercategoryDto("C01"));
        final IntegrationResponseDto response = collectionImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertThat(response.getMessages().get(0), new StringContains("required values is missing"));
    }

    @Test
    public void testMissingName() {
        final IntegrationShopTheLookDto dto = new IntegrationShopTheLookDto();
        dto.setCode("STL001");
        dto.setSupercategory(new IntegrationSupercategoryDto("C01"));
        final IntegrationResponseDto response = collectionImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertThat(response.getMessages().get(0), new StringContains("required values is missing"));
    }

    @Test
    public void testMissingCategory() {
        final IntegrationShopTheLookDto dto = new IntegrationShopTheLookDto();
        dto.setCode("STL001");
        dto.setName("Shop The Look 1");
        final IntegrationResponseDto response = collectionImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertThat(response.getMessages().get(0), new StringContains("required values is missing"));
    }

    @Test
    public void testMissingCategoryCode() {
        final IntegrationShopTheLookDto dto = new IntegrationShopTheLookDto();
        dto.setCode("STL001");
        dto.setName("Shop The Look 1");
        dto.setSupercategory(new IntegrationSupercategoryDto());
        final IntegrationResponseDto response = collectionImportIntegrationFacade.persistTargetShopTheLook(dto);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertThat(response.getMessages().get(0), new StringContains("required values is missing"));
    }

}

/**
 * 
 */
package au.com.target.tgtwsfacades.valuepack.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtcore.enums.DealTypeEnum;
import au.com.target.tgtcore.model.TargetValuePackModel;
import au.com.target.tgtcore.model.TargetValuePackTypeModel;
import au.com.target.tgtcore.valuepack.TargetValuePackService;
import au.com.target.tgtcore.valuepack.TargetValuePackTypeService;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackItem;
import au.com.target.tgtwsfacades.integration.dto.IntegrationValuePackItemList;


/**
 * @author mmaki
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ValuePackIntegrationFacadeImplTest {

    private static final String CHILD_SKU = "12345";
    private static final String LEAD_SKU = "67890";
    private static final String DESCRIPTION = "Description";
    private static final Integer QUANTITY = new Integer(1);
    private static final Double PRICE = new Double(20.5);

    @Mock
    private ModelService modelService;

    @Mock
    private TargetValuePackService valuePackService;

    @Mock
    private TargetValuePackTypeService valuePackTypeService;

    @InjectMocks
    private final ValuePackIntegrationFacadeImpl integrationFacade = new ValuePackIntegrationFacadeImpl();

    @Test
    public void testUpdateValuePackNull() {
        final IntegrationResponseDto response = integrationFacade.updateValuePack(null);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertEquals(ValuePackIntegrationFacadeImpl.ERR_VALUEPACK_NULL_ERROR, response.getMessages().get(0));
    }

    @Test
    public void testUpdateValuePackEmptyList() {
        final IntegrationValuePackDto dto = new IntegrationValuePackDto();
        dto.setLeadSKU(LEAD_SKU);
        dto.setValuePackItems(null);
        final IntegrationResponseDto response = integrationFacade.updateValuePack(dto);
        Assert.assertFalse(response.isSuccessStatus());
        Assert.assertEquals(ValuePackIntegrationFacadeImpl.ERR_VALUEPACK_NO_ITEMS_ERROR, response.getMessages().get(0));
    }

    @Test
    public void testUpdateValuePackNotExisting() {
        final IntegrationValuePackDto dto = new IntegrationValuePackDto();
        dto.setLeadSKU(LEAD_SKU);
        dto.setValuePackItems(new IntegrationValuePackItemList());
        dto.getValuePackItems().setValuePackItems(new ArrayList<IntegrationValuePackItem>());
        final IntegrationValuePackItem item = new IntegrationValuePackItem();
        item.setChildSKU(CHILD_SKU);
        item.setDealType("Value Bundle");
        item.setChildSKUDescription(DESCRIPTION);
        item.setPrice(PRICE);
        item.setQuantity(QUANTITY);
        dto.getValuePackItems().getValuePackItems().add(item);
        when(valuePackService.getByChildSku(CHILD_SKU)).thenReturn(null);
        when(valuePackTypeService.getByLeadSku(LEAD_SKU)).thenReturn(null);
        final TargetValuePackModel vp = new TargetValuePackModel();
        when(modelService.create(TargetValuePackModel.class)).thenReturn(vp);
        final TargetValuePackTypeModel type = new TargetValuePackTypeModel();
        when(modelService.create(TargetValuePackTypeModel.class)).thenReturn(type);

        final IntegrationResponseDto response = integrationFacade.updateValuePack(dto);

        Assert.assertTrue("Status not correct", response.isSuccessStatus());
        Assert.assertEquals("ChildSKU not correct", CHILD_SKU, vp.getChildSKU());
        Assert.assertEquals("Deal type not correct", DealTypeEnum.VALUEBUNDLE, vp.getValuePackDealType());
        Assert.assertEquals("Price not correct", PRICE, vp.getPrice());
        Assert.assertEquals("Quantity not correct", QUANTITY, vp.getQuantity());
        Assert.assertEquals("Description not correct", DESCRIPTION, vp.getChildSKUDescription());

        Assert.assertEquals("LeadSKU not correct", LEAD_SKU, type.getLeadSKU());
        Assert.assertEquals("Associations not correct", vp, type.getValuePackListQualifier().get(0));
        Assert.assertEquals("Associations size not correct", 1, type.getValuePackListQualifier().size());

        verify(modelService, times(1)).save(vp);
        verify(modelService, times(1)).save(type);
        verify(modelService, never()).remove(any(TargetValuePackModel.class));
        verify(modelService, never()).remove(any(TargetValuePackTypeModel.class));

    }

    @Test
    public void testUpdateValuePackUpdateExisting() {
        final IntegrationValuePackDto dto = new IntegrationValuePackDto();
        dto.setLeadSKU(LEAD_SKU);
        final IntegrationValuePackItemList list = new IntegrationValuePackItemList();
        list.setValuePackItems(new ArrayList<IntegrationValuePackItem>());
        final IntegrationValuePackItem item = new IntegrationValuePackItem();
        dto.setValuePackItems(list);
        item.setChildSKU(CHILD_SKU);
        list.getValuePackItems().add(item);
        final TargetValuePackModel oldValuePack = new TargetValuePackModel();
        when(valuePackService.getByChildSku(CHILD_SKU)).thenReturn(oldValuePack);
        final TargetValuePackTypeModel oldType = new TargetValuePackTypeModel();
        when(valuePackTypeService.getByLeadSku(LEAD_SKU)).thenReturn(oldType);
        final TargetValuePackModel vp = new TargetValuePackModel();
        when(modelService.create(TargetValuePackModel.class)).thenReturn(vp);
        final TargetValuePackTypeModel type = new TargetValuePackTypeModel();
        when(modelService.create(TargetValuePackTypeModel.class)).thenReturn(type);

        final IntegrationResponseDto response = integrationFacade.updateValuePack(dto);

        Assert.assertTrue(response.isSuccessStatus());
        verify(modelService, times(1)).removeAll(oldType.getValuePackListQualifier());
        verify(modelService, times(1)).remove(oldType);
        verify(modelService, times(1)).save(isA(TargetValuePackModel.class));
        verify(modelService, times(1)).save(isA(TargetValuePackTypeModel.class));

        Assert.assertEquals("LeadSKU not correct", LEAD_SKU, type.getLeadSKU());
        Assert.assertEquals("Associations not correct", vp, type.getValuePackListQualifier().get(0));
        Assert.assertEquals("Associations size not correct", 1, type.getValuePackListQualifier().size());

    }
}

/**
 * 
 */
package au.com.target.tgtwsfacades.warehouse.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import au.com.target.tgtfulfilment.exceptions.ConsignmentStatusValidationException;
import au.com.target.tgtfulfilment.exceptions.FulfilmentException;
import au.com.target.tgtfulfilment.exceptions.NotFoundException;
import au.com.target.tgtfulfilment.fulfilmentservice.TargetFulfilmentService;
import au.com.target.tgtfulfilment.util.LoggingContext;
import au.com.target.tgtwsfacades.consignment.impl.ConsignmentIntegrationFacadeImpl;
import au.com.target.tgtwsfacades.integration.dto.Consignment;
import au.com.target.tgtwsfacades.integration.dto.Consignments;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;


/**
 * @author pthoma20
 * 
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConsignmentIntegrationFacadeImplTest {


    @Mock
    private TargetFulfilmentService targetFulfilmentService;


    @InjectMocks
    private final ConsignmentIntegrationFacadeImpl consignmentIntegrationFacade = new ConsignmentIntegrationFacadeImpl();

    @Test
    public void testProcessCancelConsignment()
            throws FulfilmentException, NotFoundException, ConsignmentStatusValidationException {
        final Consignments consignments = mock(Consignments.class);
        final Consignment consignmentGood = mock(Consignment.class);
        final Consignment consignmentBad = mock(Consignment.class);
        given(consignments.getConsignments()).willReturn(Arrays.asList(consignmentGood, consignmentBad));
        given(consignmentGood.getConsignmentId()).willReturn("good");
        given(consignmentBad.getConsignmentId()).willReturn("bad");
        doThrow(new FulfilmentException("error")).when(targetFulfilmentService).processCancelForConsignment("bad",
                LoggingContext.WAREHOUSE);

        final List<IntegrationResponseDto> integrationResponses = consignmentIntegrationFacade
                .cancelConsignments(consignments);
        assertThat(integrationResponses).isNotNull();
        assertThat(integrationResponses).hasSize(2);
        assertThat(integrationResponses.get(0).isSuccessStatus()).isTrue();
        assertThat(integrationResponses.get(1).isSuccessStatus()).isFalse();
    }

    @Test
    public void testProcessCompleteConsignment()
            throws FulfilmentException, NotFoundException, ConsignmentStatusValidationException {
        final Date shipDate = new Date();
        final Consignments consignments = mock(Consignments.class);
        final Consignment consignmentGood = mock(Consignment.class);
        final Consignment consignmentBad = mock(Consignment.class);

        given(consignments.getConsignments()).willReturn(Arrays.asList(consignmentGood, consignmentBad));
        given(consignmentGood.getConsignmentId()).willReturn("good");
        given(consignmentGood.getCarrier()).willReturn("carrier");
        given(consignmentGood.getParcelCount()).willReturn(Integer.valueOf(1));
        given(consignmentGood.getShipDate()).willReturn(shipDate);

        given(consignmentBad.getConsignmentId()).willReturn("bad");
        given(consignmentBad.getCarrier()).willReturn("carrier");
        given(consignmentBad.getParcelCount()).willReturn(Integer.valueOf(1));
        given(consignmentBad.getShipDate()).willReturn(shipDate);
        doThrow(new FulfilmentException("error")).when(targetFulfilmentService).processCancelForConsignment("bad",
                LoggingContext.WAREHOUSE);

        final List<IntegrationResponseDto> integrationResponses = consignmentIntegrationFacade
                .cancelConsignments(consignments);
        assertThat(integrationResponses).isNotNull();
        assertThat(integrationResponses).hasSize(2);
        assertThat(integrationResponses.get(0).isSuccessStatus()).isTrue();
        assertThat(integrationResponses.get(1).isSuccessStatus()).isFalse();
    }

    @Test
    public void testProcessCompleteConsignmentNoShipDate() {
        final Consignment consignmentNoShipDate = mock(Consignment.class);
        final Consignments consignments = mock(Consignments.class);
        given(consignments.getConsignments()).willReturn(Arrays.asList(consignmentNoShipDate));
        given(consignmentNoShipDate.getConsignmentId()).willReturn("no-shipdate");
        given(consignmentNoShipDate.getCarrier()).willReturn("carrier");
        given(consignmentNoShipDate.getParcelCount()).willReturn(Integer.valueOf(1));
        final List<IntegrationResponseDto> integrationResponses = consignmentIntegrationFacade
                .completeConsignments(consignments);
        assertThat(integrationResponses).isNotNull();
        assertThat(integrationResponses).hasSize(1);
        assertThat(integrationResponses.get(0).isSuccessStatus()).isFalse();
        assertThat(integrationResponses.get(0).getMessages()).hasSize(1);
        assertThat(integrationResponses.get(0).getMessages().get(0)).isEqualTo(
                "CONSIGNMENT-COMPLETE : When completing consignment=no-shipdate an error occured with the message=Consignment ship date is null");
    }


    @Test
    public void testProcessCompleteConsignmentNoParcel() {
        final Consignment consignmentNoParcelCount = mock(Consignment.class);
        final Consignments consignments = mock(Consignments.class);
        given(consignments.getConsignments()).willReturn(Arrays.asList(consignmentNoParcelCount));
        given(consignmentNoParcelCount.getConsignmentId()).willReturn("no-parcel");
        given(consignmentNoParcelCount.getCarrier()).willReturn("carrier");
        final List<IntegrationResponseDto> integrationResponses = consignmentIntegrationFacade
                .completeConsignments(consignments);
        assertThat(integrationResponses).isNotNull();
        assertThat(integrationResponses).hasSize(1);
        assertThat(integrationResponses.get(0).isSuccessStatus()).isFalse();
        assertThat(integrationResponses.get(0).getMessages()).hasSize(1);
        assertThat(integrationResponses.get(0).getMessages().get(0)).isEqualTo(
                "CONSIGNMENT-COMPLETE : When completing consignment=no-parcel an error occured with the message=Consignment ship date is null");
    }


    @Test
    public void testProcessCompleteConsignmentNoCarrier() {
        final Consignment consignmentNoCarrier = mock(Consignment.class);
        final Consignments consignments = mock(Consignments.class);
        given(consignmentNoCarrier.getConsignmentId()).willReturn("no-carrier");
        given(consignments.getConsignments()).willReturn(Arrays.asList(consignmentNoCarrier));
        final List<IntegrationResponseDto> integrationResponses = consignmentIntegrationFacade
                .completeConsignments(consignments);
        assertThat(integrationResponses).isNotNull();
        assertThat(integrationResponses).hasSize(1);
        assertThat(integrationResponses.get(0).isSuccessStatus()).isFalse();
        assertThat(integrationResponses.get(0).getMessages()).hasSize(1);
        assertThat(integrationResponses.get(0).getMessages().get(0)).isEqualTo(
                "CONSIGNMENT-COMPLETE : When completing consignment=no-carrier an error occured with the message=Consignment carrier is null");
    }

    @Test
    public void testProcessShipConsignment() throws Exception {
        final Consignments consignments = mock(Consignments.class);

        final Consignment consignment1 = mock(Consignment.class);
        given(consignment1.getConsignmentId()).willReturn("badConsignment1");
        given(consignment1.getTrackingNumber()).willReturn("badTrackingId1");

        final Consignment consignment2 = mock(Consignment.class);
        given(consignment2.getConsignmentId()).willReturn("goodConsignment");
        given(consignment2.getTrackingNumber()).willReturn("goodTrackingId");

        final Consignment consignment3 = mock(Consignment.class);
        given(consignment3.getConsignmentId()).willReturn("badConsignment2");
        given(consignment3.getTrackingNumber()).willReturn("badTrackingId2");
        given(consignments.getConsignments()).willReturn(Arrays.asList(consignment1, consignment2, consignment3));

        doThrow(new FulfilmentException("Fulfilment test case error1")).when(targetFulfilmentService)
                .processShipConsignment("badConsignment1", "badTrackingId1", LoggingContext.WAREHOUSE);
        doNothing().when(targetFulfilmentService).processShipConsignment("goodConsignment", "goodTrackingId",
                LoggingContext.WAREHOUSE);
        doThrow(new FulfilmentException("Fulfilment test case error2")).when(targetFulfilmentService)
                .processShipConsignment("badConsignment2", "badTrackingId2", LoggingContext.WAREHOUSE);

        final List<IntegrationResponseDto> response = consignmentIntegrationFacade.shipConsignments(consignments);
        assertThat(response).isNotNull().hasSize(3);
        assertThat(response).onProperty("code").containsSequence("badConsignment1", "goodConsignment",
                "badConsignment2");
        assertThat(response).onProperty("successStatus").containsSequence(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
        assertThat(response).onProperty("messages").containsSequence(Arrays
                .asList("CONSIGNMENT-SHIP : When shipping consignment=badConsignment1 an error occured with the message=Fulfilment test case error1"),
                Collections.EMPTY_LIST, Arrays
                        .asList("CONSIGNMENT-SHIP : When shipping consignment=badConsignment2 an error occured with the message=Fulfilment test case error2"));
    }

}

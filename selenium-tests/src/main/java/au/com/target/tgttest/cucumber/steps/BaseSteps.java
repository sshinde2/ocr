/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import au.com.target.tgttest.hybris.admin.ImpexImporter;
import au.com.target.tgttest.hybris.admin.StockLevelAdjuster;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.base.SeleniumSession;
import au.com.target.tgttest.selenium.datastore.DataStoreLookup;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;
import au.com.target.tgttest.selenium.process.ShoppingProcess;


/**
 * Base for cucumber steps - convenient access to navigation methods etc
 * 
 */
public abstract class BaseSteps {

    protected StockLevelAdjuster getStockLevelAdjuster() {
        return new StockLevelAdjuster(ImpexImporter.getInstance());
    }


    protected ProductPage navigateToProductPage(final Product product) {
        final ProductPage productPage = SeleniumSession.getInstance().navigate(product.getUrl(), ProductPage.class);

        return productPage;
    }

    protected BrowserDriver getBrowserDriver() {

        return SeleniumSession.getInstance().getBrowserDriver();
    }

    protected DataStoreLookup getDataStore() {
        return DataStoreLookup.getInstance();
    }

    protected HomePage navigateHome() {
        return SeleniumSession.getInstance().navigateHome();
    }

    protected <T> T navigate(final String rel, final Class<T> pageClass) {
        return SeleniumSession.getInstance().navigate(rel, pageClass);
    }

    protected void navigate(final String rel)
    {
        SeleniumSession.getInstance().navigate(rel);
    }

    protected void navigate(final TemplatePage page)
    {
        SeleniumSession.getInstance().navigate(page);
    }

    protected ShoppingProcess getShoppingProcess() {
        return ShoppingProcess.getInstance();
    }


}

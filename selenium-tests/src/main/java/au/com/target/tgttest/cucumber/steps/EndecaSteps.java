/**
 *
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;
import au.com.target.tgttest.webservice.EndecaSearchFacade;
import au.com.target.tgttest.webservice.EndecaWebserviceFacade;
import au.com.target.tgttest.webservice.endeca.wsdl.BatchStatusType;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author htan3
 *
 */
public class EndecaSteps {

    /**
     * the interval between each check for script status.
     */
    private static final int INTERVAL_CHECKSCRIPT = 50000;

    private static final String ENDECA_APPID_TARGET = "target";

    private final EndecaWebserviceFacade endecaFacade = EndecaWebserviceFacade.getInstance();

    private final EndecaSearchFacade endecaSearchFacade = EndecaSearchFacade.getInstance();

    @When("^endeca script '(.*)' completes successfully$")
    public void runEndecaScript(final String scriptId) throws InterruptedException {
        //make sure last run is successful before the update.
        checkScriptIsFinished(scriptId);

        //start script by calling the web service.
        if (startScript(scriptId)) {
            //wait until finished with no error.
            checkScriptIsFinished(scriptId);
        }
        else {
            throw new RuntimeException("could not start endeca script: " + scriptId);
        }
    }

    @Then("stockLevelStatus of saleable product '(.*)' is '(.*)' in endeca")
    public void checkStockStatus(final String saleableProductCode, final String expectedStatus) {
        assertThat(
                endecaSearchFacade.checkStockLevelStatus(saleableProductCode)).as("stockLevelStatus").isEqualTo(
                expectedStatus);
    }

    @Given("available stock of saleable product '(.*)' is (\\d+) in endeca")
    public void checkAvilQty(final String saleableProductCode, final int expectedQty) {
        assertThat(endecaSearchFacade.checkAvailQty(saleableProductCode))
                .as("available stock").isEqualTo(
                        expectedQty);
    }

    /**
     * Check the script is finished and no failure message
     *
     * @param scriptId
     * @throws InterruptedException
     */
    private void checkScriptIsFinished(final String scriptId) throws InterruptedException {
        BatchStatusType batchStatus = endecaFacade.checkScriptStatus(ENDECA_APPID_TARGET, scriptId);
        while ("RUNNING".equals(batchStatus.getState().toString())) {
            final Thread currentThread = Thread.currentThread();
            synchronized (currentThread) {
                currentThread.wait(INTERVAL_CHECKSCRIPT);
            }
            batchStatus = endecaFacade.checkScriptStatus(ENDECA_APPID_TARGET, scriptId);
        }
        assertThat(batchStatus.getFailureMessage()).as("Failed Message").isEmpty();
    }

    /**
     * @param scriptId
     */
    private boolean startScript(final String scriptId) {
        return endecaFacade.startScript(ENDECA_APPID_TARGET, scriptId);
    }
}

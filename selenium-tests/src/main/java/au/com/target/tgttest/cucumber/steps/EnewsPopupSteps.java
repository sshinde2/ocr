/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.openqa.selenium.Cookie;

import au.com.target.tgttest.selenium.pageobjects.HomePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author cwijesu1
 * 
 */
public class EnewsPopupSteps extends BaseSteps {

    private static final String LOGED_IN_COOKIE = "targetToken";
    private static final String ENEWS_SIGNEDUP_COOKIE = "enews-subscribed";
    private static final String ENEWS_SURPRESS_COOKIE = "enews-supress";
    private HomePage homepage;

    @When("^popup visit homepage$")
    public void visitHomepage() throws Throwable {
        homepage = navigateHome();
        assertThat(homepage.isCurrentPage()).isTrue();

    }

    @Given("^not already signed up for enews$")
    public void notAlreadySignedUpForEnews() throws Throwable {
        assertThat(getBrowserDriver().checkCookieExists(ENEWS_SIGNEDUP_COOKIE)).isFalse();
    }

    @Given("^no soft login$")
    public void noSoftLogin() throws Throwable {
        assertThat(getBrowserDriver().checkCookieExists(LOGED_IN_COOKIE)).isFalse();
    }

    @When("^sign up for enews via popup$")
    public void signUpForEnewsViaPopup() throws Throwable {
        homepage = navigateHome();
        assertThat(homepage.isEnewsPopupDisplayed()).isTrue();

    }

    @Then("^sign up success is 'true'$")
    public void signUpSuccessIsTrue() throws Throwable {
        assertThat(homepage.isEnewsPopupDisplayed()).isTrue();
        homepage.signupForEnewsFromPopup("test@test.com");
        assertThat(homepage.isSignupSuccess()).isTrue();

    }

    @Then("^remember signed up for enews is '(.*)'$")
    public void rememberSignedUpForEnewsIs(final boolean flag) throws Throwable {
        if (flag) {
            assertThat(homepage.getBrowserDriver().checkCookieExists(ENEWS_SIGNEDUP_COOKIE)).isTrue();
        }
        else
        {
            assertThat(homepage.getBrowserDriver().checkCookieExists(ENEWS_SIGNEDUP_COOKIE)).isFalse();
        }
    }

    @Given("^has soft login$")
    public void hasSoftLogin() throws Throwable {
        final String key = "targetToken";
        final String value = "dGVzdC51c2VyMUB0YXJnZXQuY29tLmF1OjE0MDU5MDUzMjY0MDY6N2FjNWFmZTEyYTcxMTdhZTI0NTViYWU2M2U3MTZjZDY";
        final Date expiry = new Date(2030, 05, 15);


        final Cookie rememberMe = new Cookie.Builder(key, value).domain("localhost").path("/").expiresOn(expiry)
                .build();

        getBrowserDriver().setCookie(rememberMe);
    }

    @Then("^popup form does not appear$")
    public void popupFormDoesNotAppear() throws Throwable {
        assertThat(homepage.isEnewsPopupDisplayed()).isFalse();
    }

    @Given("^already signed up for enews$")
    public void alreadySignedUpForEnews() throws Throwable {
        final String key = "enews-subscribed";
        final String value = "true";
        final Date expiry = new Date(2030, 05, 15);
        final Cookie enewsSignedUp = new Cookie.Builder(key, value).domain("localhost").path("/").expiresOn(expiry)
                .build();
        getBrowserDriver().setCookie(enewsSignedUp);
    }

    @When("^dismiss enews popup$")
    public void dismissEnewsPopup() throws Throwable {
        assertThat(homepage.isEnewsPopupDisplayed()).isTrue();
        homepage.dissmissEnewsSignup();
        Thread.sleep(3000);


    }

    @Then("^last dismissed time is set for (\\d+) days$")
    public void lastDismissedTimeIsSetForDays(final int days) throws Throwable {
        assertThat(homepage.getBrowserDriver().checkCookieExists(ENEWS_SURPRESS_COOKIE)).isTrue();
        final Cookie supressCookie = homepage.getBrowserDriver().getCookie(ENEWS_SURPRESS_COOKIE);
        final Calendar calender = new GregorianCalendar();
        calender.setTime(supressCookie.getExpiry());
        final int expireDay = calender.get(Calendar.DAY_OF_MONTH);
        calender.setTime(new Date());
        calender.add(Calendar.DATE, days);
        final int shouldExpireDat = calender.get(Calendar.DAY_OF_MONTH);
        assertThat(expireDay).isEqualTo(shouldExpireDat);
    }

    @Given("^last dismissed time is set to (\\d+) day ago$")
    public void lastDismissedTimeIsSetToDayAgo(final int days) throws Throwable {
        final String key = ENEWS_SURPRESS_COOKIE;
        final String value = "true";
        final Calendar calender = new GregorianCalendar();
        calender.setTime(new Date());
        calender.add(Calendar.DATE, days);
        final Date expiry = calender.getTime();

        final Cookie enewsSupress = new Cookie.Builder(key, value).domain("localhost").path("/").expiresOn(expiry)
                .build();
        getBrowserDriver().setCookie(enewsSupress);
    }

    @Given("^last dismissed time is set to (\\d+) days ago$")
    public void lastDismissedTimeIsSetToDaysAgo(final int days) throws Throwable {
        homepage = navigateHome();
        final String key = ENEWS_SURPRESS_COOKIE;
        final String value = "true";
        final Calendar calender = new GregorianCalendar();
        calender.setTime(new Date());
        calender.add(Calendar.DATE, -1);
        final Date expiry = calender.getTime();
        final Cookie enewsSupress = new Cookie.Builder(key, value).domain("localhost").path("/").expiresOn(expiry)
                .build();
        homepage.getBrowserDriver().setCookie(enewsSupress);

    }

    @Then("^popup form appears$")
    public void popupFormAppears() throws Throwable {
        homepage = navigateHome();
        assertThat(homepage.isEnewsPopupDisplayed()).isTrue();
    }
}

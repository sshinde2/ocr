/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Collections;
import java.util.List;

import au.com.target.tgttest.selenium.datastore.DataStoreLookup;
import au.com.target.tgttest.selenium.datastore.bean.CreditCard;
import au.com.target.tgttest.selenium.datastore.bean.Customer;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import au.com.target.tgttest.selenium.pageobjects.LoginPage;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;
import au.com.target.tgttest.selenium.pageobjects.checkout.DeliveryMethodPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.PaymentDetailsPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.ThankyouPage;
import au.com.target.tgttest.selenium.pageobjects.myaccount.MyAccountPage;
import au.com.target.tgttest.selenium.process.ShoppingProcess;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author cwijesu1
 * 
 */
public class EnewsSignupFooterSteps extends BaseSteps {


    private boolean foundenew = false;
    private HomePage homepage;
    boolean success = false;


    @When("^visit page 'homepage'$")
    public void visitPageHomepage() throws Throwable {
        final HomePage homepage = navigateHome();
        foundenew = homepage.getEnews().isDisplayed();


    }

    @Then("^enews sign up footer box appears is '(.*)'$")
    public void enewsSignUpFooterBoxAppears(final boolean expected) throws Throwable {

        assertThat(foundenew).as("Found Enews in footer").isEqualTo(expected);
    }

    @When("^visit page 'checkout pages'$")
    public void visitPageCheckoutPages() throws Throwable {
        final ShoppingProcess shop = getShoppingProcess();
        final List<Product> productList = Collections.singletonList(getDataStore().getProductDataStore(
                DataStoreLookup.PRODUCTS_CV_HD)
                .getNextProduct());
        final ShoppingBasketPage basketPage = shop.addProductsIntoShoppingBasket(productList);
        final Customer customer = getDataStore().getStandardRegisteredCustomer();

        final DeliveryMethodPage deliveryMethodPage = shop.checkoutLoginAsRegisteredCustomer(basketPage, customer);
        if (deliveryMethodPage.getEnews().isDisplayed())
        {
            foundenew = true;

        }
        final PaymentDetailsPage paymentDetailsPage = shop.checkoutHomeDeliveryUseFirstAddress(deliveryMethodPage);
        if (paymentDetailsPage.getEnews().isDisplayed())
        {
            foundenew = true;

        }
        final CreditCard card = getDataStore().getStandardCreditCard();
        final ThankyouPage thankyouPage = shop.checkoutPayWithCreditCard(paymentDetailsPage, customer, card);
        if (thankyouPage.getEnews().isDisplayed())
        {
            foundenew = true;

        }


    }

    @When("^visit page 'my account pages'$")
    public void visitPageMyAccountPages() throws Throwable {
        final HomePage homepage = navigateHome();
        final TemplatePage returnedPage = homepage.getHeader().clickMyaccountLink();
        assertThat(returnedPage).isInstanceOf(LoginPage.class);
        final LoginPage loginPage = (LoginPage)returnedPage;
        final Customer customer = getDataStore().getStandardRegisteredCustomer();
        final MyAccountPage myaccountPage = loginPage.logInExistingUserSuccessfully(customer.getEmail(),
                customer.getPassword());
        if (myaccountPage.getEnews().isDisplayed()) {
            foundenew = true;
        }

    }

    @When("^visit homepage$")
    public void visitHomepage() throws Throwable {
        homepage = navigateHome();
        assertThat(homepage.isCurrentPage()).isTrue();

    }

    @When("^activate enews sign in$")
    public void activateEnewsSignIn() throws Throwable {
        final boolean enewsCheck = homepage.getEnews().activateEnews();
        assertThat(enewsCheck).isTrue();

    }

    @Then("^see enews disclaimer text and button$")
    public void seeEnewsDisclaimerTextAndButton() throws Throwable {
        assertThat(homepage.getEnews().getSignupButtonText()).contains("I Agree , Sign Me Up");
        assertThat(homepage.getEnews().getInlineTermsDisplayed()).isTrue();
    }

    @When("^do enews footer signup '(.*)'$")
    public void doEnewsFooterSignup(final String email) throws Throwable {
        homepage.getEnews().enewsSignUp(email);
    }

    @Then("^sign up succsess is '(.*)'$")
    public void signUpSuccsess(final boolean flag) throws Throwable {
        success = flag;
    }

    @Then("^enews validation message '(.*)'$")
    public void enewsValidationMessage(final String msg) throws Throwable {
        if (success)
        {
            assertThat(homepage.getEnews().getThankyouMsg()).contains("Thank you");
        }
        else
        {
            assertThat(homepage.getEnews().getErrorMsg()).contains(
                    msg);
        }
    }
}

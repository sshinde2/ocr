/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import cucumber.api.java.en.Given;


/**
 * Steps relating to home page
 * 
 */
public class HomeSteps extends BaseSteps {

    @Given("^go to homepage$")
    public void homepage() {

        navigateHome();
    }

}

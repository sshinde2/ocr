/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;
import au.com.target.tgttest.selenium.datastore.bean.Customer;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import au.com.target.tgttest.selenium.pageobjects.LoginPage;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;
import au.com.target.tgttest.selenium.pageobjects.myaccount.MyAccountPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author cwijesu1
 * 
 */
public class LoginSteps extends BaseSteps {


    private MyAccountPage myaccountPage;
    private Customer customer;

    @When("^registered user try to login$")
    public void registedUserTryToLogin() throws Throwable {
        final HomePage homepage = new HomePage(getBrowserDriver());
        assertThat(homepage.getHeader().hasTargetLogo()).isTrue();
        final TemplatePage hpTemplatePage = homepage.getHeader().clickMyaccountLink();
        assertThat(hpTemplatePage).isInstanceOf(LoginPage.class);
        final LoginPage loginpage = (LoginPage)hpTemplatePage;
        assertThat(loginpage.isDisplayed()).isTrue();
        customer = getDataStore().getStandardRegisteredCustomer();
        myaccountPage = loginpage.logInExistingUserSuccessfully(customer.getEmail(), customer.getPassword());
    }

    @Then("^login should complete successfully without errors$")
    public void loginShouldCompleteSuccessfullyWithoutErrors() throws Throwable {
        if (customer.getFirstname().length() < 15)
        {
            assertThat(myaccountPage.getHeader().getWelcomeMsg()).contains(customer.getFirstname());
        }
        else
        {
            assertThat(myaccountPage.getHeader().getWelcomeMsg()).contains("Your");
        }
    }

}

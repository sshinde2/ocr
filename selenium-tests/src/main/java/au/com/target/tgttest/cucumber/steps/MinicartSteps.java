/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import au.com.target.tgttest.selenium.base.SeleniumSession;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.common.MiniCartEntry;
import au.com.target.tgttest.selenium.pageobjects.common.MinicartPopup;
import au.com.target.tgttest.selenium.process.TestSession;
import au.com.target.tgttest.selenium.verifier.MinicartVerifier;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps relating to minicart
 * 
 */
public class MinicartSteps extends BaseSteps {

    private MinicartPopup minicart;

    @When("^activate minicart$")
    public void activateMinicart() {

        minicart = SeleniumSession.getInstance().getHomePage().getHeader().activateMinicartPopup();
    }

    @When("^minicart continue shopping$")
    public void minicartContinueShopping() {

        getMinicart().continueShopping();
        assertThat(minicart.isDisplayed()).as("minicart displayed").isFalse();
    }

    @When("^minicart view basket$")
    public void minicartViewBasket() {

        final ShoppingBasketPage basketPage = getMinicart().viewBasket();
        assertThat(basketPage.isCurrentPage()).as("expect basket page").isTrue();
    }

    @Then("^minicart is displayed$")
    public void verifyMinicartDisplayed() {

        assertThat(getMinicart().isDisplayed()).as("Minicart popup displayed").isTrue();
    }

    @Then("^minicart is displayed with title '(.*)'$")
    public void verifyMinicartDisplayedWithTitle(final String title) {

        verifyMinicartDisplayed();
        assertThat(getMinicart().getPopupHeading()).isEqualTo(title);
    }

    @Then("^minicart is empty with text '(.*)'$")
    public void verifyMinicartEmpty(final String message) {

        assertThat(getMinicart().getCartEmptyText()).isEqualTo(message);
    }

    @Then("^minicart total is '(.*)'$")
    public void verifyMinicartTotal(final String total) {

        assertThat(getMinicart().getPriceText()).isEqualTo(total);
    }

    @Then("^minicart num items is (\\d+)$")
    public void verifyMinicartNumItems(final int num) {

        assertThat(getMinicart().getQuantity()).isEqualTo(num);
    }

    @Then("^minicart total is the product price$")
    public void verifyMinicartTotalIsProductPrice() {

        final String expectedPrice = TestSession.getInstance().getProductPriceFormatted();
        verifyMinicartTotal(expectedPrice);
    }

    @Then("^minicart total is the total product price$")
    public void verifyMinicartTotalIsTotalProductPrice() {

        final String expectedPrice = TestSession.getInstance().getTotalProductPriceFormatted();
        verifyMinicartTotal(expectedPrice);
    }

    @Then("^minicart has the product$")
    public void verifyMinicartHasProduct() {

        final List<MiniCartEntry> entries = getMinicart().getCartEntries();
        assertThat(entries.size()).as("minicart entries size").isEqualTo(1);

        final MiniCartEntry entry = entries.get(0);
        MinicartVerifier.verifyCartEntry(entry, TestSession.getInstance().getProduct(), 1);
    }

    @Then("^minicart has all the products$")
    public void verifyMinicartHasAllProducts() {

        final List<MiniCartEntry> entries = getMinicart().getCartEntries();

        assertThat(entries.size()).isEqualTo(TestSession.getInstance().getProductList().size());

        for (final Product product : TestSession.getInstance().getProductList()) {
            MinicartVerifier.verifyCartEntry(MinicartVerifier.findCartEntry(product.getCode(), entries), product, 1);
        }

    }

    @Then("^minicart shows shopping link$")
    public void verifyMinicartShowsShoppingLink() {

        assertThat(getMinicart().isContinueShoppingLinkDisplayed()).isTrue();
    }

    @Then("^minicart shows checkout link$")
    public void verifyMinicartShowsCheckoutLink() {

        assertThat(getMinicart().isCheckoutButtonDisplayed()).isTrue();
    }


    private MinicartPopup getMinicart() {

        if (minicart == null) {
            minicart = new MinicartPopup(getBrowserDriver());
        }
        return minicart;
    }


}

/**
 *
 */
package au.com.target.tgttest.cucumber.steps;

import au.com.target.tgttest.selenium.datastore.DataStoreLookup;
import au.com.target.tgttest.selenium.datastore.store.ProductDataStore;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;
import au.com.target.tgttest.selenium.process.TestSession;
import au.com.target.tgttest.selenium.verifier.PDPVerifier;
import au.com.target.tgttest.webservice.CustomWebserviceFacade;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;


/**
 * Steps regarding products.<br/>
 * Assumes product or productList is set appropriately in TestSession.
 *
 */
public class ProductSteps extends BaseSteps {

    private ProductPage productPage;


    @Given("^go to product page$")
    public void goProductPage() {

        productPage = navigateToProductPage(TestSession.getInstance().getProduct());
    }

    @Given("^given (\\d+) products$")
    public void loadColourVariantProducts(final int num) {

        final ProductDataStore buyNowProductStore = DataStoreLookup.getInstance().getProductDataStore(
                DataStoreLookup.PRODUCTS_CV_HD);

        TestSession.getInstance().setProductList(buyNowProductStore.getProductsSortedByName(num));

        // By default reset the stock
        TestSession.getInstance().resetStockForProducts();
    }

    @Given("^given single product in stock$")
    public void loadAnyProductInStock() {

        // For now load colour variant
        loadColourVariantProduct();
        TestSession.getInstance().resetStockForSingleProduct();
    }

    @Given("^given single product out of stock$")
    public void loadAnyProductOutOfStock() {

        // For now load colour variant
        loadColourVariantProduct();
        TestSession.getInstance().zeroStockForSingleProduct();
    }

    @Given("^given single colour variant in stock$")
    public void loadColourVariantProductInStock() {

        loadColourVariantProduct();
        TestSession.getInstance().resetStockForSingleProduct();
    }

    @Given("^given single colour variant out of stock$")
    public void loadColourVariantProductOutOfStock() {

        loadColourVariantProduct();
        TestSession.getInstance().zeroStockForSingleProduct();
    }

    @Given("^given single size variant in stock$")
    public void loadSizeVariantProductInStock() {

        loadSizeVariantProduct();
        TestSession.getInstance().resetStockForSingleProduct();
    }

    @Given("^available stock of saleable product '(.*)' is (\\d+)$")
    public void resetStockForProduct(final String productCode, final int totalQuantityAvailable) {
        CustomWebserviceFacade.resetStock(productCode, totalQuantityAvailable);
    }

    private void loadColourVariantProduct() {

        final ProductDataStore buyNowProductStore = DataStoreLookup.getInstance().getColourVariantsDataStore();
        TestSession.getInstance().setProduct(buyNowProductStore.getNextProduct());
    }

    private void loadSizeVariantProduct() {

        final ProductDataStore buyNowProductStore = DataStoreLookup.getInstance().getSizeVariantsDataStore();
        TestSession.getInstance().setProduct(buyNowProductStore.getNextProduct());
    }

    @Then("^verify product page for in stock$")
    public void verifyProductForInStock() {

        verifyProduct(true);
    }

    @Then("^verify product page for out of stock$")
    public void verifyProductForOutOfStock() {

        verifyProduct(false);
    }

    private void verifyProduct(final boolean inStock) {

        PDPVerifier.verifyProductDetailsPage(getProductPage(),
                TestSession.getInstance().getProduct(), inStock);
    }


    private ProductPage getProductPage() {
        if (productPage == null) {
            productPage = new ProductPage(getBrowserDriver());
        }
        return productPage;
    }

}

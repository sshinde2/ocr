/**
 * 
 */
package au.com.target.tgttest.cucumber.steps;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Collection;

import au.com.target.tgttest.selenium.data.SearchFilterType;
import au.com.target.tgttest.selenium.pageobjects.SearchResultsPage;
import au.com.target.tgttest.selenium.pageobjects.common.GlobalHeader;
import au.com.target.tgttest.selenium.verifier.SearchSortVerifier;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps relating to search
 * 
 */
public class SearchSteps extends BaseSteps {

    private GlobalHeader header;

    private SearchResultsPage searchResultsPage;

    private String searchTerm;

    private SearchFilterType searchFilterType;


    @When("^enter search stem '(.*)'$")
    public void enterSearchStem(final String term) throws Throwable {

        this.searchTerm = term;

        getHeader().typeInSearchBox(term);
    }

    @When("^search for '(.*)'$")
    public void searchFor(final String term) throws Throwable {

        this.searchTerm = term;

        searchResultsPage = getHeader().searchFor(term);
    }

    @When("^sort results '(.*)'$")
    public void sortResults(final String sort) throws Throwable {

        searchFilterType = SearchFilterType.valueOf(sort);
        getSearchResultsPage().filterResults(searchFilterType);
    }

    @Then("^autocomplete list terms start with stem$")
    public void autocompleteListTermsStartWith() throws Throwable {

        for (final String entry : getAutocompleteEntries())
        {
            assertThat(entry).startsWith(searchTerm);
        }
    }

    @Then("^autocomplete list contains '(.*)'$")
    public void autocompleteListContains(final String word) throws Throwable {

        boolean foundBaby = false;
        for (final String entry : getAutocompleteEntries())
        {
            if (entry.equals(word)) {
                foundBaby = true;
            }
        }
        assertThat(foundBaby).as("autocomplete entries contains " + word).isTrue();
    }

    private Collection<String> getAutocompleteEntries() {

        final Collection<String> autocompleteEntries = getHeader().getAutocompleteEntries();
        assertThat(autocompleteEntries).as("autocomplete list").isNotEmpty();
        return autocompleteEntries;
    }

    @Then("^number search results on page is (\\d+)$")
    public void numberSearchResultsOnPage(final int num) throws Throwable {

        assertThat(getSearchResultsPage().getProductList().size()).as("num results on page").isEqualTo(num);
    }

    @Then("^search results message says found (\\d+)$")
    public void search_results_message_has_bike_and_found(final int num) throws Throwable {

        assertThat(getSearchResultsPage().getMainText()).as("results text").contains(getSearchResultsMessage(num));
    }

    private String getSearchResultsMessage(final int num) {
        return "Your search for \"" + searchTerm + "\" found " + num + " product results";
    }

    @Then("^verify results are sorted$")
    public void verifyResultsAreSorted() throws Throwable {

        SearchSortVerifier.checkSortOrder(searchFilterType, getSearchResultsPage().getProductList());
    }


    private GlobalHeader getHeader() {
        if (header == null) {
            header = new GlobalHeader(getBrowserDriver());
        }
        return header;
    }

    private SearchResultsPage getSearchResultsPage() {
        if (searchResultsPage == null) {
            searchResultsPage = new SearchResultsPage(getBrowserDriver());
        }
        return searchResultsPage;
    }
}

/**
 * 
 */
package au.com.target.tgttest.cucumber.steps.bean;

/**
 * @author cwijesu1
 * 
 */
public class SignupInfo {

    private final String title;
    private final String first;
    private final String email;

    /**
     * @param title
     * @param first
     * @param email
     */
    public SignupInfo(final String title, final String first, final String email) {
        super();
        this.title = title;
        this.first = first;
        this.email = email;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title.replace("'", "");
    }

    /**
     * @return the first
     */
    public String getFirst() {
        return first.replace("'", "");
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email.replace("'", "");
    }










}

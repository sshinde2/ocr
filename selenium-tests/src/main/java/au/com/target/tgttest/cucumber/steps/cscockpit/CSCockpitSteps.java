/**
 * 
 */
package au.com.target.tgttest.cucumber.steps.cscockpit;

import static org.fest.assertions.Assertions.assertThat;
import au.com.target.tgttest.selenium.cscockpit.pageobjects.CSHomePage;
import au.com.target.tgttest.selenium.cscockpit.pageobjects.CSOrderPage;
import au.com.target.tgttest.selenium.cscockpit.pageobjects.CSOrderPage.OrderManagementFunction;
import au.com.target.tgttest.selenium.cscockpit.process.LoginProcess;
import au.com.target.tgttest.webservice.TestWebServiceFacade;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for CS Cockpit
 * 
 * @author sbryan6
 * 
 */
public class CSCockpitSteps {

    private String orderNum;

    private CSOrderPage orderPage;

    @Given("^login to cscockpit$")
    public void loginCSCockpit() throws Throwable {

        LoginProcess.getInstance().loginCSAgent();
    }

    @Given("^any order is placed$")
    public void placeAnyOrder() throws Throwable {

        TestWebServiceFacade.testInitialisation();
        orderNum = TestWebServiceFacade.createAnyOrder();
        assertThat(orderNum).as("new order number").isNotEmpty();
    }

    @When("^find order in cscockpit$")
    public void findOrder() throws Throwable {

        final CSHomePage homePage = LoginProcess.getInstance().loginCSAgent();
        orderPage = homePage.findOrder(orderNum);
        assertThat(orderPage.isCurrentPage()).as("Current order page").isTrue();
    }

    @Then("^function '(.*)' is available$")
    public void functionIsAvailable(final String functionName) throws Throwable {

        assertThat(orderPage.isOrderManagementFunctionAvailable(OrderManagementFunction.valueForLabel(functionName)))
                .as("order page function " + functionName).isTrue();
    }

    @Then("^function '(.*)' is not available$")
    public void functionIsNotAvailable(final String functionName) throws Throwable {

        assertThat(orderPage.isOrderManagementFunctionAvailable(OrderManagementFunction.valueForLabel(functionName)))
                .as("order page function " + functionName).isFalse();
    }


}

/**
 * 
 */
package au.com.target.tgttest.cucumber.steps.kiosk;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgttest.cucumber.steps.kiosk.bean.PosPriceCheckInfo;
import au.com.target.tgttest.selenium.base.SeleniumSession;
import au.com.target.tgttest.selenium.pageobjects.kiosk.PosPriceCheckPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for Pos Price Check feature
 * 
 */
public class PosPriceCheckSteps {

    private PosPriceCheckPage page;

    @When("^get pos product with barcode (.*)$")
    public void getPosProductWithBarcode(final String barcode) throws Throwable {

        SeleniumSession.getInstance().navigate(PosPriceCheckPage.getUrl("7001", barcode));

        page = new PosPriceCheckPage(SeleniumSession.getInstance().getBrowserDriver());
    }


    @Then("^pos product information is:$")
    public void verifyPosProductInformation(final List<PosPriceCheckInfo> infoList) throws Throwable {

        final PosPriceCheckInfo info = infoList.get(0);

        assertThat(page.getItemCode()).as("item code").contains(info.getCode());
        assertThat(page.getTitle()).as("item title").isEqualTo(info.getName());
        assertThat(page.getPrice()).as("item price").isEqualTo(info.getPrice());

        if (StringUtils.isNotEmpty(info.getImageAlt())) {
            assertThat(page.getImageAltText()).as("item image").isEqualTo(info.getImageAlt());
        }

        if (StringUtils.isNotEmpty(info.getWasPrice())) {
            assertThat(page.getWasPrice()).as("item price").contains(info.getWasPrice());
        }
    }

    @Then("^error is '(.*)'$")
    public void errorCode(final String expected) throws Throwable {

        assertThat(page.getErrorMessage()).as("error message").isEqualTo(expected);
    }


}

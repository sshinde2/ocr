/**
 * 
 */
package au.com.target.tgttest.cucumber.steps.webservice;

import static org.fest.assertions.Assertions.assertThat;

import de.hybris.platform.category.dto.CategoryDTO;
import de.hybris.platform.core.dto.order.delivery.DeliveryModeDTO;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import au.com.target.tgtcore.dto.TargetProductDTO;
import au.com.target.tgttest.cucumber.steps.webservice.bean.ImportProductDetails;
import au.com.target.tgttest.webservice.CustomWebserviceFacade;
import au.com.target.tgttest.webservice.HybrisWebserviceFacade;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * Steps for ProductImport via web service feature
 * 
 */
public class ProductImportSteps {

    private String productCodeToUpdate;
    private Object newProductDetails;
    private TargetProductDTO productResponse;


    @Given("^product code '(.*)'$")
    public void givenProductCodeToUpdate(final String arg1) {

        productCodeToUpdate = arg1;
    }

    @Given("^new product details with xml:$")
    public void givenNewProductDetailsAsXML(final String arg1) {

        newProductDetails = arg1;
    }

    @Given("^new product details with dto:$")
    public void givenNewProductDetailsAsDTO(final List<ImportProductDetails> detailsList) {

        final ImportProductDetails details = detailsList.get(0);

        newProductDetails = CustomWebserviceFacade.createProductDto(details);
    }

    @When("^run product import via ws$")
    public void runProductImportViaWS() {

        CustomWebserviceFacade.updateProduct(productCodeToUpdate, newProductDetails);
    }

    @When("^retrieve product via ws '(.*)'$")
    public void retrieveProductViaWS(final String code) {

        productResponse = HybrisWebserviceFacade.getStagedTargetProduct(code);
    }

    @Then("^product description is '(.*)'$")
    public void verifyProductResponse(final String description) {

        assertThat(productResponse.getDescription()).as("Response description").isEqualTo(description);
    }

    @Then("^assigned delivery modes are '(.*)'$")
    public void verifyAssignedDeliveryModesOnProduct(final String deliveryModes) {
        if (StringUtils.isBlank(deliveryModes)) {
            assertThat(productResponse.getDeliveryModes()).as("productResponse.deliveryModes")
                    .isEmpty();
        }
        else {
            final String[] deliveryModesAssigned = deliveryModes.split(",");
            assertThat(productResponse.getDeliveryModes()).as("productResponse.deliveryModes")
                    .hasSize(deliveryModesAssigned.length);

            for (final String deliveryMode : deliveryModesAssigned) {
                assertThat(containsModeWithCode(productResponse.getDeliveryModes(), deliveryMode))
                        .as("productResponse.deliveryModes contains " + deliveryMode).isTrue();
            }
        }
    }

    @Then("^assigned departments are '(\\d+)'$")
    public void verifyAssignedDepartments(final String departmentNumber) {
        assertThat(containsCatWithCode(productResponse.getSupercategories(), departmentNumber))
                .as("productResponse.getSupercategories contains " + departmentNumber).isTrue();
        assertThat(productResponse.getMerchDepartment().getCode())
                .as("productResponse.merchDepartment").isEqualTo(departmentNumber);
    }

    private boolean containsModeWithCode(final Set<DeliveryModeDTO> modes, final String code) {

        boolean bFound = false;
        for (final DeliveryModeDTO mode : modes) {
            if (mode != null && mode.getCode().equals(code)) {
                bFound = true;
                break;
            }
        }
        return bFound;
    }

    private boolean containsCatWithCode(final Collection<CategoryDTO> cats, final String code) {

        boolean bFound = false;
        for (final CategoryDTO cat : cats) {
            if (cat != null && cat.getCode().equals(code)) {
                bFound = true;
                break;
            }
        }
        return bFound;
    }


}

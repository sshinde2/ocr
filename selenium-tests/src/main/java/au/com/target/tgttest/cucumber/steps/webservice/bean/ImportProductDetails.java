/**
 * 
 */
package au.com.target.tgttest.cucumber.steps.webservice.bean;

/**
 * Represent product details for product import
 * 
 */
public class ImportProductDetails {

    private final String productCode;
    private final String variantCode;
    private final String name;
    private final Integer department;
    private final String primaryCategory;
    private final String description;
    private final String brand;

    /**
     * @param productCode
     * @param variantCode
     * @param name
     * @param department
     * @param primaryCategory
     * @param description
     */
    public ImportProductDetails(final String productCode, final String variantCode, final String name,
            final Integer department,
            final String primaryCategory, final String description, final String brand) {
        super();
        this.productCode = productCode;
        this.variantCode = variantCode;
        this.name = name;
        this.department = department;
        this.primaryCategory = primaryCategory;
        this.description = description;
        this.brand = brand;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getVariantCode() {
        return variantCode;
    }

    public String getName() {
        return name;
    }

    public Integer getDepartment() {
        return department;
    }

    public String getPrimaryCategory() {
        return primaryCategory;
    }

    public String getDescription() {
        return description;
    }

    public String getBrand() {
        return brand;
    }

}

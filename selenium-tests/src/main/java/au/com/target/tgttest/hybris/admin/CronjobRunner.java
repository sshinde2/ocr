/**
 * 
 */
package au.com.target.tgttest.hybris.admin;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;


/**
 * Run cron jobs
 * 
 */
public class CronjobRunner {

    /*the impex import script for SOLR synonym updater CronJob...*/
    private static final String IMPEX_IMPORT_FILE_RESOURCE_SYNONYM_CRONJOB = "/impex/cronjobs/solr-synonym-cronjob.impex";

    /**
     * the hybris cron job starter. starts cron jobs over the rest webservice api.
     */
    private HybrisCronjobResourceHandler cronJobStarter;


    /**
     * runs the solr index updater cron job via webservice and in synchronous mode...
     */
    public void runSolrIndexUpdate() {
        getCronJobStarter().startCronjobCommand(HybrisCronjobResourceHandler.CRONJOB_ID_UPDATE_TARGET_INDEX, true);
    }


    /**
     * runs the solr synonym update cron job in synchronous mode... this method was added, as the normal solr index
     * updater could not pick up the synonym-changes, as this synonym updates need a special type of cron job.
     */
    public void runSolrSynonymsUpdate() {

        //insert-update assures that the cronjob for synonym updater is in hmc.
        ImpexImporter.getInstance().importImpexFromResource(IMPEX_IMPORT_FILE_RESOURCE_SYNONYM_CRONJOB);
        getCronJobStarter().startCronjobCommand(HybrisCronjobResourceHandler.CRONJOB_ID_UPDATE_SOLR_SYNONYMS, true);

    }

    /**
     * returns an instance of the Cron job starter, which triggers snchronous rest es calls to fire up index updates.
     * 
     * @return HybrisCronjobRescourceHandler
     */
    public HybrisCronjobResourceHandler getCronJobStarter() {

        if (cronJobStarter == null) {
            final TestConfiguration config = TestConfiguration.getInstance();

            cronJobStarter = new HybrisCronjobResourceHandler(config.getAdminHost(), Integer.parseInt(config
                    .getAdminPort()),
                    config.getWSUsername(), config.getWSPassword());
        }

        return cronJobStarter;
    }

}

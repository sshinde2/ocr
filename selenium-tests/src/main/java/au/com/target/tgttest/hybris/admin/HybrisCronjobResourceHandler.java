package au.com.target.tgttest.hybris.admin;

import de.hybris.platform.cronjob.dto.CronJobDTO;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;


/**
 * A handler class for the CronJob Rest API for Hybris. This class is used to trigger certain Hybris Cron Jobs over a
 * Jersey client.
 * 
 * TODO i will need to make this class more generic, so we are able to use this WS triggers for all kind of hybris
 * system administration.
 * 
 * @author maesi
 * 
 */
public class HybrisCronjobResourceHandler {


    public static final String CRONJOB_ID_UPDATE_TARGET_INDEX = "update-targetIndex-cronJob";
    public static final String CRONJOB_ID_UPDATE_SOLR_SYNONYMS = "target-update-solr-search-synonyms";

    private static final String HTTP_PROTOCOL_PREFIX = "http://";

    private static final String WS_VERSION = "ws410";
    private static final String WS_CRONJOBS = "cronjobs";
    private static final String WS_REST = "rest";

    private static final String QUERY_PARAM_CMD = "cmd";
    private static final String QUERY_VALUE_START_CRON_JOB_COMMAND = "StartCronJobCommand";
    private static final String QUERY_PARAM_SYNCHRONOUS = "synchronous";

    private final WebResource webResource;
    private final Client jerseyClient;


    /**
     * constructor
     * 
     * creates a new HybrisCronJobResourceHandler, which handles cronjobs over a rest webservice resource.
     * 
     * @param hostAddress
     * @param port
     */
    public HybrisCronjobResourceHandler(final String hostAddress, final int port, final String username,
            final String password) {
        final ClientConfig config = new DefaultClientConfig();

        jerseyClient = Client.create(config);
        jerseyClient.addFilter(new LoggingFilter());
        jerseyClient.addFilter(new HTTPBasicAuthFilter(username, password));

        String host;
        if (hostAddress.startsWith(HTTP_PROTOCOL_PREFIX)) {
            host = hostAddress;
        }
        else {
            host = HTTP_PROTOCOL_PREFIX + hostAddress;
        }

        webResource = jerseyClient.resource(UriBuilder.fromUri(host)
                .port(port).path(WS_VERSION)
                .path(WS_REST).build());
    }


    /**
     * Resolves the path URL for a certain rest web service resource category 'cronjobs'.
     * 
     * @param cronjob
     *            a certain cronjob resource.
     * @return the sub HTTP url for the path.
     */
    private String urlForCronjobs(final String cronjob) {
        return new StringBuilder(WS_CRONJOBS).append("/").append(cronjob)
                .toString();
    }


    /**
     * checks whether the cron job API is set up correctly or not.
     * 
     * @return Boolean true if the cron job rest API is accessible, false if not.
     */
    public boolean isCronJobViaRestWebserviceAllowed() {
        final ClientResponse result = webResource.path("mediaprocesscronjobs")
                .accept(MediaType.APPLICATION_XML)
                .get(ClientResponse.class);

        return (result.getStatus() == ClientResponse.Status.OK.getStatusCode());
    }


    /**
     * starts a certain cron job and returns true/false whether the status is OK (status code 200) or not.
     * 
     * @param cronJob
     *            cron job identifier.
     * @param synchronous
     *            boolean true/false whether the cronjob should be run in synchronous mode or not.
     * @return true if the status code for the REST ws call returns 200 (OK), false if there was an error.
     */
    public boolean startCronjobCommand(final String cronJob, final boolean synchronous) {
        final CronJobDTO cronJobDTO = new CronJobDTO();

        final ClientResponse result = webResource.path(urlForCronjobs(cronJob))
                .queryParam(QUERY_PARAM_CMD, QUERY_VALUE_START_CRON_JOB_COMMAND)
                .queryParam(QUERY_PARAM_SYNCHRONOUS, synchronous ? Boolean.TRUE.toString() : Boolean.FALSE.toString())
                .accept(MediaType.APPLICATION_XML)
                .entity(cronJobDTO)
                .put(ClientResponse.class);

        return result.getStatus() == ClientResponse.Status.OK.getStatusCode();
    }
}

package au.com.target.tgttest.hybris.admin;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;


/**
 * A helper class which can be used to import impex files during test execution using the Hybris Admin Console.
 * 
 * @author sswor
 */
public final class ImpexImporter {

    private static final String IMPEX_FOLDER_PATH = "/impex/";

    protected static final Log LOG = LogFactory.getLog(ImpexImporter.class);

    private static ImpexImporter theImpexImporter;


    /**
     * The Hybris admin session.
     */
    private final HybrisAdminSession session;

    private final TestConfiguration config;

    /**
     * Creates a new ImpexImporter.
     * 
     */
    private ImpexImporter()
    {
        this.config = TestConfiguration.getInstance();
        session = new HybrisAdminSession(config.getAdminHost(), config.getAdminPort(),
                config.getAdminUsername(), config.getAdminPassword());

    }

    /**
     * Return the ImpexImporter instance
     * 
     * @return ImpexImporter
     */
    public static ImpexImporter getInstance() {
        if (theImpexImporter == null) {
            theImpexImporter = new ImpexImporter();
        }
        return theImpexImporter;
    }


    /**
     * Imports raw impex commands.
     * 
     * @param impexSource
     *            the impex commands
     */
    public void importImpex(final String impexSource)
    {
        final Map<String, String> postData = new HashMap<String, String>();
        postData.put("_legacyMode", "off");
        postData.put("legacyMode", "false");
        postData.put("encoding", "UTF-8");
        postData.put("maxThreads", "1");
        postData.put("validationEnum", "IMPORT_STRICT");
        postData.put("scriptContent", impexSource);

        try {
            session.login();
            session.post("/admin/console/impex/import", postData, HybrisAdminSession.voidHandler());
        }
        catch (final IOException e) {
            throw new RuntimeException("Could not import impex: " + impexSource, e);
        }
    }

    /**
     * Safely closes a resource, trapping any exceptions which might be thrown during resource release.
     * 
     * @param c
     *            the resource to close
     */
    // Once we move to Java 7 we can use the try-with-resources construct to eliminate the need for this.
    private static void safelyClose(final Closeable c) {
        if (c != null) {
            try {
                c.close();
            }
            //CHECKSTYLE:OFF we want to swaller this exception
            catch (final Exception ex) {
                LOG.info("Exception closing resource ", ex);
            }
            //CHECKSTYLE:ON
        }
    }

    /**
     * Imports an impex from a resource on the class path.
     * 
     * @param resourcePath
     *            the path to the resource
     */
    public void importImpexFromResource(final String resourcePath)
    {
        InputStream stream = null;
        InputStreamReader isr = null;
        BufferedReader reader = null;
        try
        {
            stream = getClass().getResourceAsStream(resourcePath);
            isr = new InputStreamReader(stream);
            reader = new BufferedReader(isr);
            final String newline = System.getProperty("line.separator", "\n");
            final StringBuilder sb = new StringBuilder();
            while (reader.ready())
            {
                sb.append(reader.readLine()).append(newline);
            }
            importImpex(sb.toString());
        }
        catch (final IOException e) {
            throw new RuntimeException("Unable to import impex file: " + resourcePath, e);
        }
        finally
        {
            safelyClose(reader);
            safelyClose(isr);
            safelyClose(stream);
        }
    }


    /**
     * Import impex from impex folder
     * 
     * @param name
     */
    public void importImpexFromImpexFolder(final String name) {
        importImpexFromResource(IMPEX_FOLDER_PATH + name);
    }


}

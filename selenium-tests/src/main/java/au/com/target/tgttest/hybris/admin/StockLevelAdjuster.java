/**
 * 
 */
package au.com.target.tgttest.hybris.admin;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import au.com.target.tgttest.selenium.data.ProductStockLevelEntry;
import au.com.target.tgttest.selenium.data.WarehouseType;
import au.com.target.tgttest.selenium.datastore.bean.Product;


/**
 * Adjust stock levels of given products
 * 
 */
public class StockLevelAdjuster {

    private static final String HEADER = "INSERT_UPDATE StockLevel;available;warehouse(code)[unique=true];productCode[unique=true];reserved";
    private static final char EOL = '\n';
    private static final char SEP = ';';

    private static final int DEFAULT_STOCK = 100;

    private final ImpexImporter importer;


    /**
     * Constructor
     * 
     * @param importer
     */
    public StockLevelAdjuster(final ImpexImporter importer) {
        this.importer = importer;
    }

    /**
     * Adjust stock as indicated by entries
     * 
     * @param entries
     */
    public void adjustStock(final List<ProductStockLevelEntry> entries) {

        final String impexSource = buildImpexSource(entries);
        importer.importImpex(impexSource);
    }

    /**
     * Reset stock for given products
     * 
     * @param products
     */
    public void resetStock(final List<Product> products) {

        adjustStock(getDefaultStockEntries(products));
    }


    /**
     * Reset stock for given product code
     * 
     * @param code
     */
    public void resetStock(final String code) {

        adjustStock(Collections.singletonList(getDefaultStockEntry(code)));
    }

    /**
     * Reset stock for given product code
     * 
     * @param code
     */
    public void zeroStock(final String code) {

        adjustStock(Collections.singletonList(getZeroStockEntry(code)));
    }


    private String buildImpexSource(final List<ProductStockLevelEntry> entries) {

        final StringBuilder buffer = new StringBuilder();
        buffer.append(HEADER).append(EOL);

        for (final ProductStockLevelEntry entry : entries) {

            buffer.append(buildImpexDataLine(entry)).append(EOL);
        }

        return buffer.toString();
    }

    private StringBuilder buildImpexDataLine(final ProductStockLevelEntry entry) {

        final StringBuilder buffer = new StringBuilder();
        buffer.append(SEP).append(entry.getAvailable())
                .append(SEP).append(entry.getWarehouse().getCode())
                .append(SEP).append(entry.getProductCode())
                .append(SEP).append(entry.getReserved());

        return buffer;
    }

    private ProductStockLevelEntry getDefaultStockEntry(final String code) {

        final ProductStockLevelEntry entry = getBaseStockEntry(code);
        entry.setAvailable(DEFAULT_STOCK);
        return entry;
    }

    private ProductStockLevelEntry getZeroStockEntry(final String code) {

        final ProductStockLevelEntry entry = getBaseStockEntry(code);
        entry.setAvailable(0);
        return entry;
    }

    private ProductStockLevelEntry getBaseStockEntry(final String code) {

        final ProductStockLevelEntry entry = new ProductStockLevelEntry();
        entry.setProductCode(code);
        entry.setWarehouse(WarehouseType.FASTLINE);
        return entry;
    }




    private List<ProductStockLevelEntry> getDefaultStockEntries(final List<Product> products) {

        final List<ProductStockLevelEntry> stockEntries = new LinkedList<ProductStockLevelEntry>();

        if (products != null) {
            for (final Product product : products) {
                stockEntries.add(getDefaultStockEntry(product.getCode()));
            }
        }

        return stockEntries;
    }




}

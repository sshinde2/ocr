/**
 * 
 */
package au.com.target.tgttest.selenium.base;

import static org.fest.assertions.Assertions.assertThat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.seleniumhq.selenium.fluent.FluentWebDriver;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.pageobjects.common.SelectUtil;


/**
 * Base for Page Objects
 * 
 */
public abstract class BasePage {

    protected static final Logger LOG = LogManager.getLogger(BasePage.class);

    private final BrowserDriver browserDriver;


    public BasePage(final BrowserDriver driver) {
        this.browserDriver = driver;

        // Verify there are no page load errors
        assertThat(driver.hasPageLoadErrors()).as("Page load errors").isFalse();
    }

    public BrowserDriver getBrowserDriver() {
        return browserDriver;
    }

    public FluentWebDriver fwd() {
        return browserDriver.getFluentWebDriver();
    }

    public SelectUtil getComponentUtil() {
        return new SelectUtil(browserDriver);
    }


    protected boolean isDisplayed(final FluentWebElement element) {
        return element.isDisplayed().value().booleanValue();

    }

    protected boolean isEnabled(final FluentWebElement element) {
        return element.isEnabled().value().booleanValue();

    }

    protected boolean isSelected(final FluentWebElement element) {
        return element.isSelected().value().booleanValue();

    }

    protected String getInputText(final FluentWebElement element) {
        return element.getAttribute("value").toString();

    }

    protected String getText(final FluentWebElement element) {
        String text = element.getText().toString();
        if (text != null) {
            text = text.trim();
        }
        return text;
    }
}

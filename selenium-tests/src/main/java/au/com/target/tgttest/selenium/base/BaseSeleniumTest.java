/**
 * 
 */
package au.com.target.tgttest.selenium.base;

import au.com.target.tgttest.hybris.admin.ImpexImporter;
import au.com.target.tgttest.hybris.admin.StockLevelAdjuster;
import au.com.target.tgttest.selenium.datastore.DataStoreLookup;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;
import au.com.target.tgttest.selenium.process.ShoppingProcess;


/**
 * Base Test - add data store functionality
 * 
 */
public abstract class BaseSeleniumTest extends BaseTestNGSeleniumTest {

    public DataStoreLookup getDataStore() {
        return DataStoreLookup.getInstance();
    }

    protected ImpexImporter getImpexImporter() {
        return ImpexImporter.getInstance();
    }

    protected ShoppingProcess getShoppingProcess() {
        return ShoppingProcess.getInstance();
    }

    protected StockLevelAdjuster getStockLevelAdjuster() {
        return new StockLevelAdjuster(getImpexImporter());
    }

    protected ProductPage navigateToProductPage(final Product product) {
        final ProductPage productPage = SeleniumSession.getInstance().navigate(product.getUrl(), ProductPage.class);

        return productPage;
    }

    protected BrowserDriver getBrowserDriver() {

        return SeleniumSession.getInstance().getBrowserDriver();
    }

    protected HomePage navigateHome() {
        return SeleniumSession.getInstance().navigateHome();
    }

    protected <T> T navigate(final String rel, final Class<T> pageClass) {
        return SeleniumSession.getInstance().navigate(rel, pageClass);
    }

    public void navigate(final String rel)
    {
        SeleniumSession.getInstance().navigate(rel);
    }


}

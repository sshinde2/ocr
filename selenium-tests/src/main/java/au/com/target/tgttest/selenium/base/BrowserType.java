/**
 *
 */
package au.com.target.tgttest.selenium.base;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;

import com.opera.core.systems.OperaDriver;


/**
 *
 */
public enum BrowserType {
    /** */
    firefox(DesiredCapabilities.firefox(), FirefoxDriver.class, true),
    /** */
    ie(DesiredCapabilities.internetExplorer(), InternetExplorerDriver.class, true),
    /** */
    opera(DesiredCapabilities.opera(), OperaDriver.class, true),
    /** */
    chrome(DesiredCapabilities.chrome(), ChromeDriver.class, true),
    /** */
    safari(DesiredCapabilities.safari(), SafariDriver.class, true),
    /** */
    android(DesiredCapabilities.android(), null, false),
    /** */
    phantomjs(DesiredCapabilities.phantomjs(), PhantomJSDriver.class, false),
    /** */
    htmlunit(DesiredCapabilities.htmlUnitWithJs(), HtmlUnitDriver.class, false);

    private final Logger logger = LogManager.getLogger(getClass());

    private DesiredCapabilities cap;
    private Class<? extends WebDriver> webDriverClass;

    private String version;
    private String platform;
    private String deviceOrientation;
    private String deviceType;
    private final boolean windowHandlingSupported;

    /**
     * Creates BrowserType
     *
     * @param cap
     *            desired capabilities.
     * @param webDriverClass
     *            web driver type class.
     */
    private BrowserType(final DesiredCapabilities cap, final Class<? extends WebDriver> webDriverClass,
            final boolean windowHandlingSupported)
    {
        this.cap = cap;
        this.webDriverClass = webDriverClass;
        this.windowHandlingSupported = windowHandlingSupported;
    }

    /**
     * Instantiates a WebDriver based on the browser type
     *
     * @return WebDriver
     */
    public WebDriver createLocalWebDriver()
    {

        try {

            // Start the browser driver
            if (this == phantomjs)
            {
                cap.setJavascriptEnabled(true);
                cap.setCapability("takesScreenshot", false);
                //Currently running on hardcoded path this needs to be set to

                cap.setCapability(
                        PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                        TestConfiguration.getInstance().getSettings().getPhantomjsDriverPath()
                        );
                return new PhantomJSDriver(cap);
            }
            if (this == firefox) {

                return new FirefoxDriver(cap);
            }
            if (this == chrome) {

                final String chromeDriverPath = TestConfiguration.getInstance().getSettings().getChromeDriverPath();
                if (StringUtils.isNotEmpty(chromeDriverPath)) {
                    System.setProperty("webdriver.chrome.driver", chromeDriverPath);
                }

                final ChromeOptions options = new ChromeOptions();
                // add chrome options here
                cap.setCapability(ChromeOptions.CAPABILITY, options);

                return new ChromeDriver(cap);
            }

            if (webDriverClass != null) {
                return webDriverClass.newInstance();
            }
        }
        catch (final Exception ex) {
            throw new RuntimeException(ex);
        }

        return null;
    }

    /**
     * Generate the DesiredCapabilities for a RemoteDriver
     *
     * @return desiredCapabilities
     */
    public DesiredCapabilities getRemoteCapabilities()
    {
        // Get browser version and platform if specified
        if (!StringUtils.isEmpty(version)) {
            cap.setCapability("version", version);
            logger.info("Setting Sauce Labs browser version " + name() + version);
        }

        if (!StringUtils.isEmpty(platform)) {
            cap.setCapability("platform", platform);
            logger.info("Setting Sauce Labs platform " + platform);
        }

        if (!StringUtils.isEmpty(deviceOrientation))
        {
            cap.setCapability("deviceOrientation", deviceOrientation);
            logger.info("Setting Sauce Labs device orientation " + deviceOrientation);
        }

        if (!StringUtils.isEmpty(deviceType))
        {
            cap.setCapability("deviceType", deviceType);
            logger.info("Setting Sauce Labs device type " + deviceType);
        }

        return cap;
    }


    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(final String version) {
        this.version = version;
    }

    /**
     * @return the platform
     */
    public String getPlatform() {
        return platform;
    }

    /**
     * @param platform
     *            the platform to set
     */
    public void setPlatform(final String platform) {
        this.platform = platform;
    }

    /**
     * @return the deviceOrientation
     */
    public String getDeviceOrientation() {
        return deviceOrientation;
    }

    /**
     * @param deviceOrientation
     *            the deviceOrientation to set
     */
    public void setDeviceOrientation(final String deviceOrientation) {
        this.deviceOrientation = deviceOrientation;
    }

    /**
     * @return the deviceType
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * @param deviceType
     *            the deviceType to set
     */
    public void setDeviceType(final String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * @return the windowHandlingSupported
     */
    public boolean isWindowHandlingSupported() {
        return windowHandlingSupported;
    }

}
/**
 * 
 */
package au.com.target.tgttest.selenium.base;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;

import com.google.common.base.Function;


/**
 * TODO: possibly remove this, currently usused
 * 
 * @deprecated
 * 
 */
@Deprecated
public final class PageUtil {

    private static final Logger LOG = LogManager.getLogger(PageUtil.class);
    private static final int LOW_TIMEOUT_MILLISECONDS = 100;

    /**
     * NOTE: there is no way in selenium webdriver to read the http code
     * (http://code.google.com/p/selenium/issues/detail?id=141) So we'll need to check for text instead
     * 
     * TODO set up to read from tgtstorefront/project.properties
     **/
    private static final String ERROR_404_PAGE_NOT_FOUND = "404 Page Not Found";
    private static final String ERROR_500 = "HTTP Status 500";

    protected PageUtil()
    {
        // empty
    }


    public static boolean elementExists(final WebElement element) {
        try {
            element.getTagName();
            return true;
        }
        catch (final Exception e) {
            return false;
        }
    }

    /**
     * Return true if the current page url ends with given string
     */
    public static boolean checkPageUrlEndsWith(final WebDriver driver, final String relativeUrl)
    {
        boolean currentPage = true;
        final String currentUrl = driver.getCurrentUrl();

        if (!currentUrl.endsWith(relativeUrl)) {
            currentPage = false;
            LOG.warn("Current URL was expected to end with " + relativeUrl + ",  but was " + currentUrl);
        }
        return currentPage;
    }

    /**
     * Return true if the current page url contains given string
     */
    public static boolean checkPageUrlContains(final WebDriver driver, final String match)
    {
        boolean currentPage = true;
        final String currentUrl = driver.getCurrentUrl();

        if (!currentUrl.contains(match)) {
            currentPage = false;
            LOG.warn("Current URL was expected to contain " + match + ",  but was " + currentUrl);
        }
        return currentPage;
    }


    /**
     * checks if a link to an external resource is valid or not.
     * 
     * @param element
     *            the web element to check.
     * @param hRefURLMatcher
     *            String a matcher to check the href/external link url is contained in the href element.
     * @return boolean true if the external link URL is valid, false if not.
     */
    public static boolean checkLinkURLValid(final WebElement element, final String hRefURLMatcher) {
        try {
            final String hRefLink = element.getAttribute("href");

            if (hRefURLMatcher != null && !hRefURLMatcher.isEmpty()) {
                return (hRefLink != null && hRefLink.contains(hRefURLMatcher));
            }
            return (hRefLink != null && !hRefLink.isEmpty());
        }
        catch (final Exception e) {
            return false;
        }
    }

    /**
     * Fires a mouseOver event via jQuery.
     * 
     * @param driver
     *            the driver
     * @param jQuerySelector
     *            the jQuery selector indicating the item over which the mouse will be moved
     */
    public static void jQueryMouseover(final WebDriver driver, final String jQuerySelector)
    {
        if (driver instanceof JavascriptExecutor)
        {
            final JavascriptExecutor js = (JavascriptExecutor)driver;
            final String script = MessageFormat.format("jQuery(\"{0}\").mouseover();", jQuerySelector);
            js.executeScript(script);
        }
    }

    /**
     * Waits until there are no more page animations.
     * 
     * @param driver
     *            the driver
     */
    public static void waitForAnimationsToFinish(final WebDriver driver)
    {
        final int maxSeconds = 3;
        final WebDriverWait wait = new WebDriverWait(driver, maxSeconds);
        final Function<WebDriver, Boolean> animationsHaveStopped = new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(final WebDriver driver)
            {
                /*
                 * Assume the animations are done if we cannot actually test for them.
                 */
                Boolean results = Boolean.TRUE;

                if (driver instanceof JavascriptExecutor)
                {
                    /*
                     * Execute JavaScript to check the page for animated elements.
                     */
                    final JavascriptExecutor js = (JavascriptExecutor)driver;
                    final Object animated = js.executeScript("return jQuery(':animated').length==0");
                    if (animated != null && animated instanceof Boolean)
                    {
                        results = (Boolean)animated;
                    }
                }
                return results;
            }
        };
        wait.ignoring(WebDriverException.class).until(animationsHaveStopped);
    }

    /**
     * Waits until an animation has started on the page.
     * 
     * @param driver
     *            the driver
     */
    public static void waitForAnimationsToBegin(final WebDriver driver)
    {
        final int maxSeconds = 3;
        final WebDriverWait wait = new WebDriverWait(driver, maxSeconds);
        final Function<WebDriver, Boolean> animationsHaveStarted = new Function<WebDriver, Boolean>() {
            @Override
            public Boolean apply(final WebDriver driver)
            {
                /*
                 * Assume the animations have started if we cannot test for them.
                 */
                Boolean results = Boolean.TRUE;

                if (driver instanceof JavascriptExecutor)
                {
                    /*
                     * Execute JavaScript to check the page for animated elements.
                     */
                    final JavascriptExecutor js = (JavascriptExecutor)driver;
                    final Object animated = js.executeScript("return jQuery(':animated').length!=0");
                    if (animated != null && animated instanceof Boolean)
                    {
                        results = (Boolean)animated;
                    }
                }
                return results;
            }
        };

        try
        {
            wait.ignoring(WebDriverException.class, TimeoutException.class).until(animationsHaveStarted);
        }
        /*
         * CHECKSTYLE:OFF There's an unavoidable race condition here, so we don't want to fail our tests if we don't see
         * animations starting (they may have already started and finished by the time our javascript code is executed).
         */
        catch (final TimeoutException ex)
        {
            //Trap.
        }
        //CHECKSTYLE:ON
    }

    /**
     * Determines if an element is within the bounds of another.
     * 
     * @param container
     *            the "outer" element
     * @param candidate
     *            the element which should be checked
     * @return {@code true} if the candidate is located within the bounds of the container
     */
    public static boolean isWithinBounds(final WebElement container, final WebElement candidate)
    {
        final int containerLeft = container.getLocation().getX();
        final int containerRight = containerLeft + container.getSize().getWidth();
        final int candidateLeft = candidate.getLocation().getX();
        final int containerTop = container.getLocation().getY();
        final int containerBottom = containerTop + container.getSize().getHeight();
        final int candidateTop = candidate.getLocation().getY();
        return candidateLeft >= containerLeft && candidateLeft <= containerRight && candidateTop >= containerTop
                && candidateTop <= containerBottom;
    }

    /**
     * Comparable Selenium Color getter by String
     * 
     * Returns a comparable color code by a given string, so we are able to compare colors from CSS properties (f.e. RGB
     * colors with alpha values such as 'rgb(a[102, 98, 45, 1])') and run tests with it.
     * 
     * @param colorCode
     *            The color code as a String.
     * @return Color A Selenium Color class.
     */
    public static Color getComparableColorCode(final String colorCode) {
        return Color.fromString(colorCode);
    }

    /**
     * Waits for the current page to render completely.
     * 
     * @param driver
     */
    public static void waitForPageToLoad(final WebDriver driver) {
        final ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    @SuppressWarnings("boxing")
                    @Override
                    public Boolean apply(final WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals(
                                "complete");
                    }
                };

        final Wait<WebDriver> wait = new WebDriverWait(driver, 30);
        try {
            wait.until(expectation);
        }
        catch (final Throwable error) {
            LOG.warn("Timeout waiting for Page Load Request to complete.");
        }
    }

    /**
     * Waits for an element to complete rendering.
     * 
     * @param driver
     */
    public static void waitForElementToRender(final WebDriver driver, final WebElement element) {
        final Wait<WebDriver> wait = new WebDriverWait(driver, 30);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        catch (final Throwable error) {
            LOG.warn("Timeout waiting for web element to render.");
        }
    }



    /**
     * returns a Selenium Select component by WebElement.
     * 
     * Attention: This method is not very helpful in case of Target, as there is used quite a bit of javascript to
     * operate a select. In case a standard select component is used wrapped within a single dom-element, this is the
     * way to use!
     * 
     * @param element
     *            a WebElement which should represent a Select
     * @return a Selenium Select.
     */
    public static Select getSelect(final WebElement element) {
        try {
            return new Select(element);
        }
        catch (final Exception e) {
            final String error = "Not able to instantiate a Select component for a WebElement with Tag name'"
                    + element.getTagName() + "'.";
            LOG.error(error, e);
            return null;
        }
    }




    /**
     * checks the page for loading errors.
     * 
     * @return boolean true/false whether the page had loading errors or not.
     */
    public static boolean hasPageLoadErrors(final WebDriver driver) {
        // NOTE: there is no way in selenium webdriver to read the http code (http://code.google.com/p/selenium/issues/detail?id=141)
        // So we'll need to check for text instead.
        // TODO: investigate if this can be improved.
        final String source = driver.getPageSource();

        if (source.contains(ERROR_404_PAGE_NOT_FOUND))
        {
            LOG.warn(ERROR_404_PAGE_NOT_FOUND + " message was found on the page.");
            return true;
        }

        if (source.contains(ERROR_500))
        {
            LOG.warn(ERROR_500 + " message was found on the page.");
            return true;
        }

        return false;
    }


    /**
     * removes a certain css attribute from a web-element in the DOM.
     * 
     * @param webElementId
     *            the Id for a certain WebElement.
     * @param cssAttribute
     *            the attribute which needs to be gone.
     * @param driver
     *            the WebDriver instance.
     */
    public static void removeCSSAttributeFromDOM(final String webElementId, final String cssAttribute,
            final WebDriver driver) {
        final JavascriptExecutor js = (JavascriptExecutor)driver;
        try {
            js.executeScript("document.getElementById('" + webElementId + "').removeAttr('" + cssAttribute + "');");
        }
        catch (final Exception e) {
            LOG.error("Unable to remove the Attribute '" + cssAttribute + "' from the DOM.");
        }
    }

    /**
     * sets the driver timeout to the values defined in the configuration.
     * 
     * @param config
     *            a SelBaseTestConfiguration.
     * @param driver
     *            the WebDriver instance.
     */
    public static void setDriverTimeout(final TestConfiguration config, final WebDriver driver)
    {
        driver.manage().timeouts().implicitlyWait(config.getWaitTimeout(), TimeUnit.SECONDS);
    }

    /**
     * sets the selenium webDriver timeout to a minimal amount.
     * 
     * @param driver
     *            the WebDriver instance.
     */
    public static void setLowDriverTimeout(final WebDriver driver)
    {
        setDriverTimeout(LOW_TIMEOUT_MILLISECONDS, driver);
    }

    /**
     * sets the selenium webDriver timeout to a different amount.
     * 
     * @param int wait timeout amount in milliseconds.
     * @param driver
     *            the WebDriver instance.
     */
    public static void setDriverTimeout(final int timeoutInMilliseconds, final WebDriver driver)
    {
        driver.manage().timeouts().implicitlyWait(timeoutInMilliseconds, TimeUnit.MILLISECONDS);
    }


}

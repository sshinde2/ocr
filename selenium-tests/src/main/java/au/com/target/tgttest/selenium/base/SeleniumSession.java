/**
 * 
 */
package au.com.target.tgttest.selenium.base;

import au.com.target.tgttest.selenium.base.config.BrowserOptions;
import au.com.target.tgttest.selenium.base.config.TestConfiguration;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;


/**
 * Represents a selenium session
 * 
 */
public class SeleniumSession {

    private static SeleniumSession theSession;

    private BrowserDriver browserDriver;

    // Nice name for the 'session'
    private final String sessionName;

    // Overall session success
    private boolean sessionSuccess = true;

    private HomePage homePage;

    private SeleniumSession() {
        sessionName = "default";
    }

    private SeleniumSession(final BrowserOptions options, final String name) {

        sessionName = name;

        try {
            // Start the configured driver
            browserDriver = new BrowserDriver();
            browserDriver.setUpDriver(options, sessionName);
        }
        catch (final Exception e) {
            throw new RuntimeException("Could not start BrowserDriver", e);
        }

    }

    public static boolean isInitialised() {
        return (theSession != null);
    }

    /**
     * Initialise the selenium test session
     * 
     * @param options
     * @param sessionName
     */
    public static void initialise(final BrowserOptions options, final String sessionName) {

        if (theSession == null) {
            theSession = new SeleniumSession(options, sessionName);
        }

    }

    /**
     * Return the singleton session
     * 
     * @return the session
     */
    public static SeleniumSession getInstance() {

        if (theSession == null) {
            throw new RuntimeException("SeleniumSession has not been initialised");
        }

        return theSession;
    }

    /**
     * Shutdown the test session
     */
    public void shutdownDriverSession() {

        final String jobId = browserDriver.shutdownDriver();
        notifySessionResult(jobId);

        theSession = null;
    }

    /**
     * Mark the session as failed
     */
    public void failSession() {
        sessionSuccess = false;
    }

    public BrowserDriver getBrowserDriver() {

        return browserDriver;
    }

    private TestConfiguration getTestConfiguration() {
        return TestConfiguration.getInstance();
    }

    /**
     * Notify listeners (eg Sauce) of the session result, given job Id
     * 
     * @param jobId
     */
    private void notifySessionResult(final String jobId) {
        getTestConfiguration().notifySessionResult(jobId, sessionName, sessionSuccess);
    }


    /**
     * Navigates to Home and instantiates a page object for it.
     * 
     * @return a page object instance
     */
    public HomePage navigateHome() {
        navigate(getBaseUrl());
        homePage = new HomePage(browserDriver);
        return homePage;
    }

    /**
     * Return the relative base url for the test
     * 
     * @return relative url
     */
    private String getBaseUrl() {

        // By default the base url is the home page
        return "";
    }

    /**
     * Navigate to the given relative URL
     * 
     * @param rel
     *            the relative URL to navigate to.
     */
    public void navigate(final String rel)
    {
        browserDriver.navigate(constructUrl(rel));
    }


    /**
     * Navigate to the url implied by type of the given TemplatePage
     * 
     * @param page
     */
    public void navigate(final TemplatePage page)
    {
        browserDriver.navigate(constructUrl(page.getPageRelativeURL()));
    }

    /**
     * Navigates to a specific relative URL path.
     * 
     * @param <T>
     *            the page object type
     * @param rel
     *            the relative URL to navigate to.
     * @param pageClass
     *            the page object class
     * @return a page object instance
     */
    public <T> T navigate(final String rel, final Class<T> pageClass) {
        browserDriver.navigate(constructUrl(rel));
        T ret;
        try {
            ret = pageClass.getConstructor(BrowserDriver.class).newInstance(browserDriver);
        }
        catch (final Exception e) {
            throw new RuntimeException("BaseSeleniumTest.navigate - could not create page object", e);
        }
        return ret;
    }

    /**
     * Navigate to the given product page
     * 
     * @param product
     * @return ProductPage
     */
    public ProductPage navigateToProductPage(final Product product) {
        return navigate(product.getUrl(), ProductPage.class);
    }


    private String constructUrl(final String rel) {

        return getTestConfiguration().constructUrl(rel);
    }

    /**
     * Clear session cookie
     */
    public void clearSessionData() {
        browserDriver.clearSessionData();
    }

    public HomePage getHomePage() {
        return homePage;
    }

}

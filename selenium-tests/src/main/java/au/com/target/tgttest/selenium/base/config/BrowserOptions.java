/**
 * 
 */
package au.com.target.tgttest.selenium.base.config;

/**
 * Represent browser/platform options
 * 
 */
public class BrowserOptions {

    private final String browser;
    private String platform;
    private String version;
    private String deviceOrientation;
    private String deviceType;


    /**
     * @param browser
     */
    public BrowserOptions(final String browser) {
        super();
        this.browser = browser;
    }


    public String getPlatform() {
        return platform;
    }

    public void setPlatform(final String platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getDeviceOrientation() {
        return deviceOrientation;
    }

    public void setDeviceOrientation(final String deviceOrientation) {
        this.deviceOrientation = deviceOrientation;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(final String deviceType) {
        this.deviceType = deviceType;
    }

    public String getBrowser() {
        return browser;
    }



}

package au.com.target.tgttest.selenium.base.config;

import org.openqa.selenium.WebDriver;

import au.com.target.tgttest.selenium.base.BrowserType;


public class LocalTestConfiguration extends TestConfiguration
{

    protected LocalTestConfiguration(final Settings settings)
    {
        super(settings);
    }

    @Override
    public WebDriver getDriver(final BrowserType browser, final String name)
    {
        final WebDriver driver = browser.createLocalWebDriver();
        return driver;
    }
}

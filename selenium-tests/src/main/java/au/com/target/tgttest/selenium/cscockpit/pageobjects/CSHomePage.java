/**
 * 
 */
package au.com.target.tgttest.selenium.cscockpit.pageobjects;

import static org.seleniumhq.selenium.fluent.Period.secs;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.base.SeleniumSession;


/**
 * Represent the cscockpit home page
 * 
 */
public class CSHomePage extends BasePage {

    private final By menuSpan = By.cssSelector("div.menulabel span.z-label");
    private final By findOrderLink = By.linkText("Find Order");

    // Order Search - note we have to click in the td's rather than on the buttons themselves
    private final By orderSearchInput = By.cssSelector("div.csSearchPane input");
    private final By orderSearchButtonTd = By.cssSelector("div.csSearchPane td.z-button-cm");
    private final By orderSearchSelectButtonTd = By.cssSelector("div.csSearchResults td.z-button-cm");

    /**
     * Constructor
     * 
     * @param driver
     */
    public CSHomePage(final BrowserDriver driver) {
        super(driver);
    }

    /**
     * Verify this is the current page - wait for a while for page to render
     * 
     * @return true if it is
     */
    public boolean isCurrentPage() {

        // Check for Menu span
        final FluentWebElement elt = fwd().within(secs(20)).span(menuSpan);
        if (elt.isDisplayed().value().booleanValue()) {
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * Find Order
     * 
     * @param orderNum
     */
    public CSOrderPage findOrder(final String orderNum) {

        fwd().link(findOrderLink).click();
        fwd().within(secs(5)).input(orderSearchInput).sendKeys(orderNum);
        fwd().td(orderSearchButtonTd).click();
        fwd().within(secs(5)).td(orderSearchSelectButtonTd).click();

        final CSOrderPage orderPage = new CSOrderPage(SeleniumSession.getInstance().getBrowserDriver());
        return orderPage;
    }


}

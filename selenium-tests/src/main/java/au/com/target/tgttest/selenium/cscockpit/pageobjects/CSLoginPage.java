/**
 * 
 */
package au.com.target.tgttest.selenium.cscockpit.pageobjects;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * @author sbryan6
 * 
 */
public class CSLoginPage extends BasePage {

    private final By loginNameInput = By.name("j_username");
    private final By loginPasswordInput = By.name("j_password");
    private final By loginTd = By.cssSelector("td.z-button-cr");

    /**
     * @param driver
     */
    public CSLoginPage(final BrowserDriver driver) {
        super(driver);
    }

    public void loginUser(final String email, final String password) {
        fwd().input(loginNameInput).clearField().sendKeys(email);
        fwd().input(loginPasswordInput).clearField().sendKeys(password);
        fwd().td(loginTd).click();
    }

}

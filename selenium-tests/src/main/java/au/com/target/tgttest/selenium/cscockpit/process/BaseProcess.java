package au.com.target.tgttest.selenium.cscockpit.process;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.base.config.TestConfiguration;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;


/**
 * Base class for process test helpers for cscockpit.
 * 
 */
public class BaseProcess {

    protected static final Logger LOG = LogManager.getLogger(BaseProcess.class);

    private final BrowserDriver driver;

    /**
     * constructor.
     */
    public BaseProcess(final BrowserDriver driver) {
        this.driver = driver;
    }

    protected BrowserDriver getBrowserDriver() {
        return driver;
    }

    /**
     * returns the Selenium test configuration settings.
     * 
     * @return TestConfiguration
     */
    protected TestConfiguration getConfiguration() {
        return TestConfiguration.getInstance();
    }


    /**
     * Navigate to given relative URL
     * 
     * @param rel
     */
    protected void navigate(final String rel)
    {
        final String url = getConfiguration().getCSCockpitUrl() + "/" + rel;
        getBrowserDriver().navigate(url);
    }


    /**
     * Navigate to the url for the given template page
     * 
     * @param page
     */
    protected void navigate(final TemplatePage page)
    {
        navigate(page.getPageRelativeURL());
    }

    /**
     * Navigate to the url for the given type of template page, and return an instance of the page object
     * 
     * @param pageClass
     * @return page object
     */
    public <T extends TemplatePage> T navigate(final Class<T> pageClass) {

        T page;
        try {
            page = pageClass.getConstructor(BrowserDriver.class).newInstance(getBrowserDriver());
        }
        catch (final Exception e) {
            throw new RuntimeException("BaseSeleniumTest.navigate - could not create page object", e);
        }

        navigate(page);

        return page;
    }

}
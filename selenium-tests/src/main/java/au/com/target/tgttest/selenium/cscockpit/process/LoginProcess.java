/**
 * 
 */
package au.com.target.tgttest.selenium.cscockpit.process;

import static org.fest.assertions.Assertions.assertThat;
import au.com.target.tgttest.selenium.base.SeleniumSession;
import au.com.target.tgttest.selenium.cscockpit.pageobjects.CSHomePage;
import au.com.target.tgttest.selenium.cscockpit.pageobjects.CSLoginPage;


/**
 * CSCockpit login process
 * 
 * @author sbryan6
 * 
 */
public final class LoginProcess extends BaseProcess {

    private static LoginProcess theLoginProcess;


    /**
     * Constructor
     * 
     */
    private LoginProcess() {
        super(SeleniumSession.getInstance().getBrowserDriver());
    }

    public static LoginProcess getInstance() {

        if (theLoginProcess == null) {
            theLoginProcess = new LoginProcess();
        }

        return theLoginProcess;
    }

    /**
     * Login to CS Cockpit using standard csagent
     */
    public CSHomePage loginCSAgent() {

        navigate("index.zul");
        final CSLoginPage loginPage = new CSLoginPage(getBrowserDriver());
        loginPage.loginUser(getConfiguration().getCSCockpitLogin(), getConfiguration().getCSCockpitPassword());

        // Wait for and verify homepage
        final CSHomePage homePage = new CSHomePage(SeleniumSession.getInstance().getBrowserDriver());
        assertThat(homePage.isCurrentPage()).as("homepage is current").isTrue();

        return homePage;
    }
}

/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Product size variant enum for bedding.
 */
public enum BeddingSizeVariantType {

    SINGLE("SB", "Single Bed"),
    DOUBLE("DB", "Double Bed"),
    QUEEN("QB", "Queen Bed"),
    KING("KB", "King Bed"),
    KING_SINGLE("KSB", "King Single Bed");

    /**
     * The URL for the parent item.
     */
    public static final String PARENT_PRODUCT_URL = "p/bed-sheets-pillowcases-product/CP3071";

    /**
     * The text which should be displayed on the icon.
     */
    private final String iconText;

    /**
     * The displayed text on the size variant selection.
     */
    private final String name;

    /**
     * Creates a new BeddingType.
     * 
     * @param iconText
     *            the displayed icon text
     * @param name
     *            the displayed text on the size variant selection
     */
    private BeddingSizeVariantType(final String iconText, final String name)
    {
        this.iconText = iconText;
        this.name = name;
    }

    /**
     * @return the iconText
     */
    public String getIconText() {
        return iconText;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }


}

/**
 * 
 */
package au.com.target.tgttest.selenium.data;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.pageobjects.SiteMapPage;



/**
 * Data enum for the home page carousel.
 * 
 * @author sswor
 * 
 */
public enum CarouselType {
    HOT_OPTIONS_CLOTHING("item-1", "/sitemap", "8796165210142.jpg", "Banner 1",
            "Banner 1", SiteMapPage.class),
    KIDS_CLOTHING("item-2", "/sitemap", "8796165177374.jpg", "Banner 2", "Banner 2",
            SiteMapPage.class),
    DEALS_OF_THE_DAY("item-3", "/sitemap", "8796165242910.jpg", "Banner 3",
            "Banner 3", SiteMapPage.class);

    /**
     * The name of the HTML anchor ({@code <a name="...">} for the button which displays the item in the carousel.
     */
    private final String anchorName;
    /**
     * The relative URL of the link the user is taken to when clicking on the carousel item.
     */
    private final String linkRelativeUrl;
    /**
     * The relative URL of the image displayed on the carousel item.
     */
    private final String imageRelativeUrl;
    /**
     * The alt text of the image displayed on the carousel item.
     */
    private final String altText;
    /**
     * The title of the image displayed on the carousel item.
     */
    private final String title;

    /**
     * The page class.
     */
    private final Class<? extends BasePage> pageClass;

    private CarouselType(final String anchorName, final String linkRelativeUrl, final String imageRelativeUrl,
            final String altText,
            final String title,
            final Class<? extends BasePage> pageClass)
    {
        this.anchorName = anchorName;
        this.linkRelativeUrl = linkRelativeUrl;
        this.imageRelativeUrl = imageRelativeUrl;
        this.altText = altText;
        this.title = title;
        this.pageClass = pageClass;
    }



    /**
     * @return the anchorName
     */
    public String getAnchorName() {
        return anchorName;
    }

    /**
     * @return the linkRelativeUrl
     */
    public String getLinkRelativeUrl() {
        return linkRelativeUrl;
    }

    /**
     * @return the imageRelativeUrl
     */
    public String getImageRelativeUrl() {
        return imageRelativeUrl;
    }

    /**
     * @return the altText
     */
    public String getAltText() {
        return altText;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the pageClass
     */
    public Class<? extends BasePage> getPageClass()
    {
        return pageClass;
    }
}

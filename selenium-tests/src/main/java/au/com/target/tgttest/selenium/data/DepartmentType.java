/**
 * 
 */
package au.com.target.tgttest.selenium.data;



/**
 * Represents a content page type.
 */
public enum DepartmentType {
    /** Department type. */
    KIDS("Kids", "W93742"),
    /** Department type. */
    BABY("Baby", "W93741"),
    /** Department type. */
    BODY("Body + Beauty", "W298278"),
    /** Department type. */
    SCHOOL("School", "W93744"),
    /** Department type. */
    TOYS("Toys", "W138532"),
    /** Department type. */
    WOMEN("Women", "W93743");

    private String name;
    private String code;

    private DepartmentType(final String name, final String code)
    {
        this.name = name;
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    public String getCode() {
        return code;
    }

    /**
     * @return the relativeURL
     */
    public String getRelativeURL()
    {
        return "c/" + getName().replaceAll("\\ ", "-").replaceAll("\\+", "%2B") + "/" + getCode();
    }
}

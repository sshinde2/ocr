/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Footer content links specified in the Information Architecture document attached to OCR-218.
 * 
 * @author sswor
 */
public enum FooterContentType {

    WHATS_ON("What's On", "http://www.target.com.au/html/whatson/whatson.htm", true, false),
    SERVICES("Services", "http://www.target.com.au/html/services/services.htm", true, false),
    CATALOGUE("Catalogue", "http://target.dynamiccatalogue.com.au/portal/", true, false),
    COMPANY("Company", "http://www.target.com.au/", true, false),
    HELP("Help", "help", true, true),
    ENEWS_SIGN_UP("eNews", "eNewsSubscription", true, true);

    private final String linkText;
    private final String linkUrl;
    private final boolean rootNode;
    private final boolean internal;


    private FooterContentType(final String linkText, final String linkUrl, final boolean rootNode,
            final boolean internal) {
        this.linkText = linkText;
        this.linkUrl = linkUrl;
        this.rootNode = rootNode;
        this.internal = internal;
    }

    public String getLinkText() {
        return linkText;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    /**
     * Gets the title text.<br />
     * Implementation Note: The IA document does not explicitly state the title text. It is assumed to be the same as
     * the visible text.
     * 
     * @return the title text
     */
    public String getTitleText() {
        return getLinkText();
    }

    /**
     * Gets whether the content page is an internal online store page.
     * 
     * @return {@code true} if the content page is an internal page of the online store, {@code false} if it is
     *         externally hosted.
     */
    public boolean isInternal() {
        return internal;
    }

    public boolean isRootNode() {
        return rootNode;
    }
}

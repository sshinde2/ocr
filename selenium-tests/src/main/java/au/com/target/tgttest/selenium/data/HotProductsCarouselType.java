/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * 
 */
public enum HotProductsCarouselType implements ProductCarouselItem {
    CANDLES("Body + Beauty Candles + Diffusers product", "/_ui/desktop/theme-default/images/missing-product-96x96.png",
            "/p/body-beauty-candles-diffusers-product/P2005", "$10.00", "$XX.XX"),
    WOMEN("Body + Beauty Women product", "/_ui/desktop/theme-default/images/missing-product96x96.png",
            "/p/body-beauty-women-product/CP2006", "$10.00", "$XX.XX"),
    FAUX_LASHES("Body + Beauty Faux Lashes product", "/_ui/desktop/theme-default/images/missing-product96x96.png",
            "/p/body-beauty-faux-lashes-product/CP2012", "$10.00", "$XX.XX"),
    BLUSH("Body + Beauty Blush product", "/_ui/desktop/theme-default/images/missing-product96x96.png",
            "/p/body-beauty-blush-product/CP2021", "$10.00", "$XX.XX");

    private final String titleText;
    private final String imageUrl;
    private final String linkHref;
    private final String currentPrice;
    private final String wasPrice;

    /**
     * Creates an ItemCarouselType.
     * 
     * @param titleText
     *            the title text (not truncated)
     * @param imageUrl
     *            the url of the thumbnail image
     * @param linkHref
     *            the href of the link
     * @param currentPrice
     *            the current price (with currency symbol)
     * @param wasPrice
     *            the old price (with currency symbol)
     */
    private HotProductsCarouselType(final String titleText, final String imageUrl, final String linkHref,
            final String currentPrice, final String wasPrice)
    {
        this.titleText = titleText;
        this.imageUrl = imageUrl;
        this.linkHref = linkHref;
        this.currentPrice = currentPrice;
        this.wasPrice = wasPrice;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getTitleText()
     */
    @Override
    public String getTitleText() {
        return titleText;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getImageUrl()
     */
    @Override
    public String getImageUrl() {
        return imageUrl;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getLinkHref()
     */
    @Override
    public String getLinkHref() {
        return linkHref;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getCurrentPrice()
     */
    @Override
    public String getCurrentPrice() {
        return currentPrice;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#getWasPrice()
     */
    @Override
    public String getWasPrice() {
        return wasPrice;
    }

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.data.ProductCarouselItem#isApproved()
     */
    @Override
    public boolean isApproved() {
        return true;
    }
}

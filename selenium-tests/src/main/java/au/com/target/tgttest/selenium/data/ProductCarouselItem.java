/**
 * 
 */
package au.com.target.tgttest.selenium.data;



/**
 * Data interface for an item in a product carousel.
 */
public interface ProductCarouselItem {

    /**
     * @return the titleText
     */
    String getTitleText();

    /**
     * @return the imageUrl
     */
    String getImageUrl();

    /**
     * @return the linkHref
     */
    String getLinkHref();

    /**
     * @return the currentPrice
     */
    String getCurrentPrice();

    /**
     * @return the wasPrice
     */
    String getWasPrice();

    /**
     * @return the approved
     */
    boolean isApproved();
}
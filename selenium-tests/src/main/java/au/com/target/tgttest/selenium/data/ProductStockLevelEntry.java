/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Represents a product stock level
 * 
 */
public class ProductStockLevelEntry {

    private int available;
    private int reserved;
    private WarehouseType warehouse;
    private String productCode;

    /**
     * @return the available
     */
    public int getAvailable() {
        return available;
    }

    /**
     * @param available
     *            the available to set
     */
    public void setAvailable(final int available) {
        this.available = available;
    }

    /**
     * @return the reserved
     */
    public int getReserved() {
        return reserved;
    }

    /**
     * @param reserved
     *            the reserved to set
     */
    public void setReserved(final int reserved) {
        this.reserved = reserved;
    }

    /**
     * @return the warehouse
     */
    public WarehouseType getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse
     *            the warehouse to set
     */
    public void setWarehouse(final WarehouseType warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode
     *            the productCode to set
     */
    public void setProductCode(final String productCode) {
        this.productCode = productCode;
    }

}

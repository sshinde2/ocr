/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * 
 */
public enum ProductType {

    BODY_GIFTSETS_MEN(CategoryType.BATH_BODY, "Body + Beauty Gift Sets For Men product",
            "p/body-beauty-gift-sets-product/CP2009",
            "/_ui/desktop/theme-default/images/missing-product-620x620.png", PromotionalFlag.HOT_PRODUCT, "gift"),

    BODY_DEODORANT(CategoryType.BATH_BODY, "Body + Beauty Deodorant product",
            "p/body-beauty-deodorant-product/CP2002",
            "/_ui/desktop/theme-default/images/missing-product-620x620.png", PromotionalFlag.HOT_PRODUCT, "deodorant"),

    BODY_CANDLESDIFFUSERS(CategoryType.BATH_BODY, "Body + Beauty Candles + Diffusers product",
            "p/body-beauty-candles-diffusers-product/CP2005",
            "/_ui/desktop/theme-default/images/missing-product-620x620.png", PromotionalFlag.HOT_PRODUCT, "candles"),

    BABY_SHOES_BLACK(CategoryType.BABY_BABYWEAR, "Baby Shoes product - Black", "p/baby-shoes-product/P1000",
            "/medias/sys_master/h29/h07/8796153085982/P1000_BLACK_product.jpg", null, "shoes"),

    BABY_JACKET_RED(CategoryType.BABY_BABYWEAR, "Baby Jacket product - Red", "p/baby-jacket-product/P1060_red",
            "P1060_red_product.jpg", null, "baby jacket"),

    SCHOOL_PENS(CategoryType.SCHOOL_STATIONERY, "Pens product", "p/pens-product/CP6018",
            "/_ui/desktop/theme-default/images/missing-product-620x620.png", PromotionalFlag.CLEARANCE, "pens"),

    SCHOOL_DESKACCESSORIES(CategoryType.SCHOOL_STATIONERY, "Desk Accessories product", "p/pens-product/CP6020",
            "/_ui/desktop/theme-default/images/missing-product-620x620.png", PromotionalFlag.ESSENTIALS,
            "desk accessories product"),

    TOYS_BIKES(CategoryType.TOYS_BIKES, "Bikes product", "p/bikes-product/CP7002",
            "/_ui/desktop/theme-default/images/missing-product-620x620.png", PromotionalFlag.ONLINE_ONLY, "bikes"),

    BABY_BAG_BLACK(CategoryType.BABY_TRAVEL_AND_TRANSPORT, "Baby Bags product - Black",
            "p/baby-bags-product/P1050_black",
            "/_ui/desktop/theme-default/images/missing-product-620x620.png", PromotionalFlag.CLEARANCE, "baby bags");

    private final CategoryType category;
    private final String linkTitle;
    private final String linkHref;
    private final String heroImageUrl;
    private final PromotionalFlag promotionalFlag;
    private final String searchKey;

    private ProductType(final CategoryType category, final String linkTitle, final String linkHref,
            final String heroImageUrl, final PromotionalFlag promotionalFlag, final String searchKey)
    {
        this.category = category;
        this.linkTitle = linkTitle;
        this.linkHref = linkHref;
        this.heroImageUrl = heroImageUrl;
        this.promotionalFlag = promotionalFlag;
        this.searchKey = searchKey;
    }

    /**
     * @return the category
     */
    public CategoryType getCategory() {
        return category;
    }

    /**
     * @return the linkTitle
     */
    public String getLinkTitle() {
        return linkTitle;
    }

    /**
     * @return the linkHref
     */
    public String getLinkHref() {
        return linkHref;
    }

    /**
     * @return the hotItem
     */
    public boolean isHotItem() {
        return PromotionalFlag.HOT_PRODUCT.equals(promotionalFlag);
    }

    /**
     * @return the heroImageUrl
     */
    public String getHeroImageUrl()
    {
        return heroImageUrl;
    }

    /**
     * @return the promotionalFlag
     */
    public PromotionalFlag getPromotionalFlag()
    {
        return promotionalFlag;
    }

    /**
     * @return the searchKey
     */
    public String getSearchKey() {
        return searchKey;
    }
}

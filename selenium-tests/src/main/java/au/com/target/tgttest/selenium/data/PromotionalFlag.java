/*
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * 
 */
public enum PromotionalFlag {

    CLEARANCE("clearance", "/common/images/icons/status/clearance.png"),
    TARGET_EXCLUSIVE("only-at", "/common/images/icons/status/only-at.png"),
    ESSENTIALS("essentials", "/common/images/icons/status/essentials.png"),
    ONLINE_ONLY("online-only", "/common/images/icons/status/online-only.png"),
    HOT_PRODUCT("hot-product", "/common/images/icons/status/hot-icon.png");

    private final String cssClass;
    private final String imageUrl;

    private PromotionalFlag(final String cssClass, final String imageUrl)
    {
        this.cssClass = cssClass;
        this.imageUrl = imageUrl;
    }

    /**
     * @return the cssClass
     */
    public String getCssClass() {
        return cssClass;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }


}

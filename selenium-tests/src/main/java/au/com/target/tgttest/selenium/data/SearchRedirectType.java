/**
 * 
 */
package au.com.target.tgttest.selenium.data;


/**
 * Represents a search redirect data.
 * 
 * @author SBryan5
 * 
 */
public enum SearchRedirectType {
    /** Search Redirect Data. */
    EXTERNAL_GIFT_CARD(SearchRedirectType.EXTERNAL, "Gift card", "onlineshopping.target.com.au/target.php", null),
    /** Search Redirect Data. */
    EXTERNAL_STORE_LOCATOR(SearchRedirectType.EXTERNAL, "store locator",
            "catalogues.target.com.au/storelocator.html", null),
    /** Search Redirect Data. */
    CATEGORY_MEN(SearchRedirectType.CATEGORY, "men", null, "men"),
    /** Search Redirect Data. */
    CATEGORY_TOYS(SearchRedirectType.CATEGORY, "toys", null, "toys");

    public static final String EXTERNAL = "EXTERNAL";
    public static final String CATEGORY = "CATEGORY";

    private String type;
    private String term;
    private String urlmatch;
    private String catName;

    private SearchRedirectType(final String type, final String term, final String urlmatch,
            final String catName)
    {
        this.type = type;
        this.term = term;
        this.urlmatch = urlmatch;
        this.catName = catName;
    }

    public String getTerm() {
        return term;
    }

    public String getUrlmatch() {
        return urlmatch;
    }

    public String getCatName() {
        return catName;
    }

    public String getType() {
        return type;
    }
}

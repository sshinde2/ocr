/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Data enum for subcategory grid items.
 */
public enum SubcategoryGridItemType {
    BABYWEAR("Babywear", "subcategory-thumbnail-placeholder.jpg", "/c/baby/babywear/W93745", "Babywear"),
    ACTIVITY_PLAY("Activity + Play", "activity.png", "/c/baby/activity-play/W106746", "Activity + Play"),
    FURNITURE("Furniture", "furniture.png", "/c/baby/furniture/W109775", "Furniture"),
    MANCHESTER("Manchester", "manchester.png", "/c/baby/manchester/W114707", "Manchester"),
    FEEDING_NURSING("Feeding + Nursing", "feeding.png", "/c/baby/feeding-nursing/W115383", "Feeding + Nursing"),
    BABY_CARE("Baby Care", "baby-care.png", "/c/baby/baby-care/W115397", "Baby Care"),
    SAFETY("Safety", "safety.png", "/c/baby/safety/W118757", "Safety"),
    TRAVEL_TRANSPORT("Travel + Transport", "travel.png", "/c/baby/travel-transport/W150900", "Travel + Transport"),
    GIFTS("Gifts", "gifts.png", "/c/baby/gifts/W153098", "Gifts"),
    CLEARANCE("Clearance", "clearance.png", "/c/baby/clearance/W201480", "Clearance");

    /**
     * The item text.
     */
    private final String text;

    /**
     * The path to the background image.
     */
    private final String backgroundImagePath;

    /**
     * The link href.
     */
    private final String linkHref;

    /**
     * The link title.
     */
    private final String linkTitle;

    /**
     * Creates a new SubcategoryGridItemType.
     * 
     * @param text
     *            the text
     * @param backgroundImagePath
     *            the background image path
     * @param linkHref
     *            the link href
     * @param linkTitle
     *            the link title
     */
    private SubcategoryGridItemType(final String text, final String backgroundImagePath, final String linkHref,
            final String linkTitle)
    {
        this.text = text;
        this.backgroundImagePath = backgroundImagePath;
        this.linkHref = linkHref;
        this.linkTitle = linkTitle;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @return the backgroundImagePath
     */
    public String getBackgroundImagePath() {
        return backgroundImagePath;
    }

    /**
     * @return the linkHref
     */
    public String getLinkHref() {
        return linkHref;
    }

    /**
     * @return the linkTitle
     */
    public String getLinkTitle() {
        return linkTitle;
    }
}

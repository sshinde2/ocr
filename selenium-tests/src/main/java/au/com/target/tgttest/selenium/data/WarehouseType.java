/**
 * 
 */
package au.com.target.tgttest.selenium.data;

/**
 * Represent possible warehouse codes
 * 
 */
public enum WarehouseType {

    FASTLINE("FastlineWarehouse");

    private String code;

    private WarehouseType(final String code) {
        this.code = code;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }


}

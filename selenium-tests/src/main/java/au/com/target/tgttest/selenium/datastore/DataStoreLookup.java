/**
 *
 */
package au.com.target.tgttest.selenium.datastore;

import java.io.File;
import java.net.URL;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;
import au.com.target.tgttest.selenium.datastore.bean.CreditCard;
import au.com.target.tgttest.selenium.datastore.bean.Customer;
import au.com.target.tgttest.selenium.datastore.bean.PaypalCreditCard;
import au.com.target.tgttest.selenium.datastore.store.CSVCreditCardDataStore;
import au.com.target.tgttest.selenium.datastore.store.CSVCustomerDataStore;
import au.com.target.tgttest.selenium.datastore.store.CSVPaypalCreditCardDataStore;
import au.com.target.tgttest.selenium.datastore.store.CSVProductDataStore;
import au.com.target.tgttest.selenium.datastore.store.CreditCardDataStore;
import au.com.target.tgttest.selenium.datastore.store.CustomerDataStore;
import au.com.target.tgttest.selenium.datastore.store.PaypalCreditCardDataStore;
import au.com.target.tgttest.selenium.datastore.store.ProductDataStore;


/**
 * Lookup for accessing data stores for different data types.
 * 
 */
public final class DataStoreLookup {

    public static final String PRODUCTS_CV_HD = "colour-variants-hd-buynow.csv";
    public static final String PRODUCTS_CV_CNC = "colour-variants-cnc-buynow.csv";
    public static final String PRODUCTS_SV_HD = "size-variants-hd-buynow.csv";
    public static final String CUSTOMERS = "customers.csv";
    public static final String CREDITCARDS = "creditcards.csv";
    public static final String PAYPALCREDITCARDS = "paypal-creditcards.csv";

    private static DataStoreLookup theDataStore;

    private String baseDataFilePath;

    private DataStoreLookup() {
        // singleton, use getInstance
    }

    /**
     * Get the DataStoreLookup instance
     * 
     * @return DataStoreLookup
     */
    public static DataStoreLookup getInstance() {

        if (theDataStore == null) {
            theDataStore = new DataStoreLookup();

            theDataStore.setBaseDataFilePath(TestConfiguration.getInstance().getSettings()
                    .getSetting("baseDataFilePath"));
        }

        return theDataStore;
    }

    public void setBaseDataFilePath(final String baseDataFilePath) {
        this.baseDataFilePath = baseDataFilePath;
    }

    private File getFile(final String name) {

        final URL url = this.getClass().getResource(baseDataFilePath + name);
        File file;
        try {
            file = new File(url.toURI());
        }
        catch (final Exception e) {
            throw new RuntimeException("Could not read data file: " + name + " from path " + baseDataFilePath, e);
        }
        return file;
    }

    /**
     * @return the ProductDataStore
     */
    public ProductDataStore getProductDataStore(final String filename) {
        final CSVProductDataStore results = new CSVProductDataStore();
        results.setCsvFile(getFile(filename));
        return results;
    }

    /**
     * @return Colour Variants data store
     */
    public ProductDataStore getColourVariantsDataStore() {

        return getProductDataStore(DataStoreLookup.PRODUCTS_CV_HD);
    }

    /**
     * @return Size Variants data store
     */
    public ProductDataStore getSizeVariantsDataStore() {

        return getProductDataStore(DataStoreLookup.PRODUCTS_SV_HD);
    }


    /**
     * @return the CustomerDataStore
     */
    public CustomerDataStore getCustomerDataStore(final String filename) {
        final CSVCustomerDataStore results = new CSVCustomerDataStore();
        results.setCsvFile(getFile(filename));
        return results;
    }

    /**
     * Get the standard registered customer
     * 
     * @return customer
     */
    public Customer getStandardRegisteredCustomer() {
        return getCustomerDataStore(DataStoreLookup.CUSTOMERS).getStandardRegisteredCustomer();
    }

    private CreditCardDataStore getCreditCardDataStore(final String filename) {
        final CSVCreditCardDataStore results = new CSVCreditCardDataStore();
        results.setCsvFile(getFile(filename));
        return results;
    }

    /**
     * Get the standard credit card
     * 
     * @return customer
     */
    public CreditCard getStandardCreditCard() {
        return getCreditCardDataStore(DataStoreLookup.CREDITCARDS).getCreditCard();
    }

    private PaypalCreditCardDataStore getPaypalCreditCardDataStore(final String filename) {
        final CSVPaypalCreditCardDataStore results = new CSVPaypalCreditCardDataStore();
        results.setCsvFile(getFile(filename));
        return results;
    }

    /**
     * Get the standard Paypal credit card
     * 
     * @return customer
     */
    public PaypalCreditCard getStandardPaypalCreditCard() {
        return getPaypalCreditCardDataStore(DataStoreLookup.PAYPALCREDITCARDS).getPaypalCreditCard();
    }


}
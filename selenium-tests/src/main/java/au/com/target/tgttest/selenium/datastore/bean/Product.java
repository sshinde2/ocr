/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.bean;

import java.math.BigDecimal;

import au.com.target.tgttest.selenium.datastore.store.ProductUrlResolver;


/**
 * Represents a single product
 * 
 */
public class Product {

    private String code;
    private String name;
    private String baseName;
    private String description;
    private BigDecimal price;
    private BigDecimal wasPrice;
    private String brand;

    private String size;

    private String colour;
    private String colourCode;

    private String swatch;
    private String swatchCode;


    /**
     * Construct url for the product using hybris site logic
     */
    public String getUrl() {
        return ProductUrlResolver.getUrl(this);
    }

    public boolean isNoColour() {
        return colourCode.equals("nocolour");
    }

    public boolean hasWasPrice() {
        return wasPrice != null;
    }


    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(final BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand
     *            the brand to set
     */
    public void setBrand(final String brand) {
        this.brand = brand;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size
     *            the size to set
     */
    public void setSize(final String size) {
        this.size = size;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        if (isNoColour()) {
            return null;
        }
        return colour;
    }

    /**
     * @param colour
     *            the colour to set
     */
    public void setColour(final String colour) {
        this.colour = colour;
    }

    /**
     * @return the swatch
     */
    public String getSwatch() {
        return swatch;
    }

    /**
     * @param swatch
     *            the swatch to set
     */
    public void setSwatch(final String swatch) {
        this.swatch = swatch;
    }

    /**
     * @return the baseName
     */
    public String getBaseName() {
        return baseName;
    }

    /**
     * @param baseName
     *            the baseName to set
     */
    public void setBaseName(final String baseName) {
        this.baseName = baseName;
    }

    /**
     * @return the wasPrice
     */
    public BigDecimal getWasPrice() {
        return wasPrice;
    }

    /**
     * @param wasPrice
     *            the wasPrice to set
     */
    public void setWasPrice(final BigDecimal wasPrice) {
        this.wasPrice = wasPrice;
    }

    /**
     * @return the colourCode
     */
    public String getColourCode() {
        return colourCode;
    }

    /**
     * @param colourCode
     *            the colourCode to set
     */
    public void setColourCode(final String colourCode) {
        this.colourCode = colourCode;
    }

    /**
     * @return the swatchCode
     */
    public String getSwatchCode() {
        return swatchCode;
    }

    /**
     * @param swatchCode
     *            the swatchCode to set
     */
    public void setSwatchCode(final String swatchCode) {
        this.swatchCode = swatchCode;
    }


}

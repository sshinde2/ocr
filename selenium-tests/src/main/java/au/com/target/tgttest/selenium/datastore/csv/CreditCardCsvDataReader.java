/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.csv;

import au.com.target.tgttest.selenium.datastore.bean.CreditCard;


/**
 * CreditCard csv reader
 * 
 */
public class CreditCardCsvDataReader extends AbstractCsvDataReader<CreditCard> {

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.datastore.csv.AbstractCsvDataReader#createInstanceFromCsv(java.lang.String[])
     */
    @Override
    protected CreditCard createInstanceFromCsv(final String[] csvData) {
        final CreditCard card = new CreditCard();

        card.setNumber(getValueForHeader("NUMBER", csvData));
        card.setName(getValueForHeader("NAME", csvData));
        card.setExpiryMonth(getValueForHeader("EXPIRY_MONTH", csvData));
        card.setExpiryYear(getValueForHeader("EXPIRY_YEAR", csvData));
        card.setSecurityCode(getValueForHeader("SECURITY_CODE", csvData));

        return card;
    }

}

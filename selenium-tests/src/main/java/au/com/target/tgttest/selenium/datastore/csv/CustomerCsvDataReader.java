/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.csv;

import au.com.target.tgttest.selenium.datastore.bean.Customer;


/**
 * Customer reader
 * 
 */
public class CustomerCsvDataReader extends AbstractCsvDataReader<Customer> {

    /* (non-Javadoc)
     * @see au.com.target.tgttest.selenium.datastore.csv.AbstractCsvDataReader#createInstanceFromCsv(java.lang.String[])
     */
    @Override
    protected Customer createInstanceFromCsv(final String[] csvData) {

        final Customer customer = new Customer();
        customer.setEmail(getValueForHeader("EMAIL", csvData));
        customer.setPassword(getValueForHeader("PASSWORD", csvData));
        customer.setTitle(getValueForHeader("TITLE", csvData));
        customer.setFirstname(getValueForHeader("FIRSTNAME", csvData));
        customer.setSurname(getValueForHeader("SURNAME", csvData));
        customer.setPhone(getValueForHeader("PHONE", csvData));
        customer.setAddr1(getValueForHeader("ADDR1", csvData));
        customer.setAddr2(getValueForHeader("ADDR2", csvData));
        customer.setCity(getValueForHeader("CITY", csvData));
        customer.setPostcode(getValueForHeader("POSTCODE", csvData));
        customer.setState(getValueForHeader("STATE", csvData));

        return customer;
    }

}

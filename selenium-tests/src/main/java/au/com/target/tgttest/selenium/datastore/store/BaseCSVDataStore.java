/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import static org.fest.assertions.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import au.com.target.tgttest.selenium.datastore.csv.AbstractCsvDataReader;


/**
 * Base for CSV data stores
 * 
 */
public abstract class BaseCSVDataStore<T> {

    private File csvFile;

    private final AbstractCsvDataReader<T> reader;

    private Iterator<T> iterator;

    private Collection<T> objects;

    public BaseCSVDataStore(final AbstractCsvDataReader<T> reader) {
        this.reader = reader;
    }

    /**
     * @param file
     *            the file to set
     */
    public void setCsvFile(final File file) {
        this.csvFile = file;
    }


    /**
     * Reset the iterator
     */
    protected void reset() {
        iterator = null;
    }


    /**
     * Get the next one
     * 
     * @return next thing
     */
    protected T getNext() {
        final T next = getIterator().next();

        // Assume we don't want to overrun the size of the data set
        assertThat(next).as("Hit end of list for data file: " + getFileName()).isNotNull();

        return next;
    }


    private synchronized Iterator<T> getIterator() {

        if (iterator == null) {
            iterator = getObjects().iterator();
        }
        return iterator;
    }

    /**
     * Lazy-loads the base products.
     * 
     * @return the products
     */
    private synchronized Collection<T> getObjects() {
        if (objects == null) {
            objects = loadObjects();
        }
        return objects;
    }


    private Collection<T> loadObjects() {
        try
        {
            return reader.readFromCsvFile(csvFile, true);
        }
        catch (final IOException ex)
        {
            throw new RuntimeException("Exception reading from CSV file: " + getFileName(), ex);
        }
    }

    private String getFileName() {
        return (csvFile.getName() != null ? csvFile.getName() : "null");
    }


}

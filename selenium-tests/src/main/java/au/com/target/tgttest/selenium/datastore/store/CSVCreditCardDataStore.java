/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import au.com.target.tgttest.selenium.datastore.bean.CreditCard;
import au.com.target.tgttest.selenium.datastore.csv.CreditCardCsvDataReader;


/**
 * CreditCard data store
 * 
 */
public class CSVCreditCardDataStore extends BaseCSVDataStore<CreditCard> implements CreditCardDataStore {

    /**
     */
    public CSVCreditCardDataStore() {
        super(new CreditCardCsvDataReader());
    }

    @Override
    public CreditCard getCreditCard() {

        reset();
        return getNext();
    }

}

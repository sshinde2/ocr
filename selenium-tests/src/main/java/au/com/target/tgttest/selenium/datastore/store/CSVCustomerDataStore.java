/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import au.com.target.tgttest.selenium.datastore.bean.Customer;
import au.com.target.tgttest.selenium.datastore.csv.CustomerCsvDataReader;


/**
 * CSV implementation for customer data store
 * 
 */
public class CSVCustomerDataStore extends BaseCSVDataStore<Customer> implements CustomerDataStore {

    /**
     * Constructor
     * 
     */
    public CSVCustomerDataStore() {
        super(new CustomerCsvDataReader());
    }

    @Override
    public Customer getStandardRegisteredCustomer() {

        reset();
        return getNext();
    }

}

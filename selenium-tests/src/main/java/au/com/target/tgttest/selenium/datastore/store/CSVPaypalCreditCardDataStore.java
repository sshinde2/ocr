/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import au.com.target.tgttest.selenium.datastore.bean.PaypalCreditCard;
import au.com.target.tgttest.selenium.datastore.csv.PaypalCreditCardCsvDataReader;


/**
 * PaypalCreditCard data store
 * 
 */
public class CSVPaypalCreditCardDataStore extends BaseCSVDataStore<PaypalCreditCard> implements
        PaypalCreditCardDataStore {

    /**
     */
    public CSVPaypalCreditCardDataStore() {
        super(new PaypalCreditCardCsvDataReader());
    }

    @Override
    public PaypalCreditCard getPaypalCreditCard() {

        reset();
        return getNext();
    }

}

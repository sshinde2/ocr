/**
 *
 */
package au.com.target.tgttest.selenium.datastore.store;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.datastore.csv.ProductCsvDataReader;



/**
 * Implementation of ProductDataStore using CSV file.
 * 
 */
public class CSVProductDataStore extends BaseCSVDataStore<Product> implements ProductDataStore {


    /**
     * Constructor
     * 
     */
    public CSVProductDataStore() {
        super(new ProductCsvDataReader());
    }

    @Override
    public Product getNextProduct() {

        return getNext();
    }

    @Override
    public List<Product> getProductsSortedByName(final int num) {

        reset();

        final List<Product> list = getProducts(num);
        Collections.sort(list, new Comparator<Product>() {

            @Override
            public int compare(final Product p1, final Product p2) {
                return p1.getName().compareTo(p2.getName());
            }
        });
        return list;
    }

    private List<Product> getProducts(final int num) {

        final List<Product> list = new ArrayList<Product>();
        for (int i = 0; i < num; i++) {
            list.add(getNextProduct());
        }
        return list;
    }

}

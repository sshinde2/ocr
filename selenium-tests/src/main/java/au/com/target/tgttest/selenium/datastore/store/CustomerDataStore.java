/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import au.com.target.tgttest.selenium.datastore.bean.Customer;


/**
 * Provider for Customer data
 * 
 */
public interface CustomerDataStore {

    /**
     * Get a standard registered customer
     * 
     * @return Customer
     */
    Customer getStandardRegisteredCustomer();
}

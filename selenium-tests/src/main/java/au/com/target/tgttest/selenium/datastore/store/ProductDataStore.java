/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import java.util.List;

import au.com.target.tgttest.selenium.datastore.bean.Product;


/**
 * Provider for product data
 * 
 */
public interface ProductDataStore {

    Product getNextProduct();

    List<Product> getProductsSortedByName(int num);
}

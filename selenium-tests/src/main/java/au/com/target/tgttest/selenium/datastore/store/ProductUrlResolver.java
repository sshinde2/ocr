/**
 * 
 */
package au.com.target.tgttest.selenium.datastore.store;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import au.com.target.tgttest.selenium.datastore.bean.Product;


/**
 * Provide urls for products
 * 
 */
public final class ProductUrlResolver {

    private static final String PRODUCT_PATTERN = "p/{product-name}/{product-code}";

    private ProductUrlResolver() {
        // Empty
    }

    public static String getUrl(final Product product) {

        String url = PRODUCT_PATTERN.replace("{product-name}", urlSafe(product.getBaseName()));
        url = url.replace("{product-code}", product.getCode());
        return url;
    }


    protected static String urlSafe(final String text)
    {
        if (text == null || text.isEmpty())
        {
            return "";
        }

        String encodedText;
        try
        {
            encodedText = URLEncoder.encode(text, "utf-8");
        }
        catch (final UnsupportedEncodingException encodingException)
        {
            encodedText = text;
        }

        // Cleanup the text
        String cleanedText = encodedText;
        cleanedText = cleanedText.replaceAll("%2F", "/");
        cleanedText = cleanedText.replaceAll("[^%A-Za-z0-9\\-]+", "-");
        return cleanedText;
    }

}

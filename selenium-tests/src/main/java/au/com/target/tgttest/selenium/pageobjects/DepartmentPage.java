/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.data.DepartmentType;
import au.com.target.tgttest.selenium.verifier.DepartmentPageVerifier;


/**
 * Department page
 * 
 */
public class DepartmentPage extends BaseProductListPage {

    private final DepartmentType departmentType;

    /**
     * @param driver
     */
    public DepartmentPage(final BrowserDriver driver, final DepartmentType departmentType) {
        super(driver);
        this.departmentType = departmentType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPageRelativeURL() {
        return departmentType.getCode();
    }

    public DepartmentType getDepartmentType() {
        return departmentType;
    }

    @Override
    public boolean isCurrentPage() {
        DepartmentPageVerifier.verifyDeptLanding(this);
        return true;
    }

}

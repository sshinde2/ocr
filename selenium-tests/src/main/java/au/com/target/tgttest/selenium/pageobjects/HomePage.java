/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import static org.seleniumhq.selenium.fluent.Period.secs;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * Home Page Object
 * 
 */
public class HomePage extends TemplatePage {

    /**
     * @param driver
     */

    private final By homepageUnique = By.cssSelector("div.supplement div.supplement-home-banners");
    private final By homepageEnewsPopup = By.cssSelector("div#cboxLoadedContent div.enews-quick h2");
    private final By emailInput = By.cssSelector("div#cboxContent input.enews-quick-email");
    private final By emailSingupSubmit = By.cssSelector("div#cboxLoadedContent button.button-submit");
    private final By signUpSuccessMsg = By.cssSelector("div#cboxContent div.f-success p.f-suc-msg");
    private final By enewsSignupDissmiss = By.id("cboxClose");
    private final By enewsDissmissValidate = By.cssSelector("div#cboxLoadedContent enews-quick-email");

    public HomePage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public boolean isCurrentPage() {
        if (getText(fwd().div(homepageUnique)).contains("Your Peace of Mind")) {
            return true;
        }
        else {
            return false;
        }

    }

    @Override
    public String getPageRelativeURL() {
        return "";
    }


    public boolean isEnewsPopupDisplayed() {

        FluentWebElement enewsPopup;
        try {
            enewsPopup = fwd().h2(homepageEnewsPopup).within(secs(5));
        }
        catch (final Exception e)
        {
            return false;
        }
        return isDisplayed(enewsPopup.ifInvisibleWaitUpTo(secs(5)));
    }

    public void signupForEnewsFromPopup(final String email)
    {
        fwd().button(emailSingupSubmit).ifInvisibleWaitUpTo(secs(10));
        fwd().input(emailInput).clearField().sendKeys(email);
        fwd().button(emailSingupSubmit).click();
    }

    public boolean isSignupSuccess()
    {
        fwd().p(signUpSuccessMsg).within(secs(5));
        if (getText(fwd().p(signUpSuccessMsg)).contains("Thank you!")) {
            return true;
        }
        else {
            return false;
        }
    }

    public void dissmissEnewsSignup()
    {
        fwd().button(enewsSignupDissmiss).ifInvisibleWaitUpTo(secs(10)).click();
        fwd().hasMissing().input(enewsDissmissValidate);

    }
}

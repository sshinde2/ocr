/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * Search results page
 * 
 */
public class SearchResultsPage extends BaseProductListPage {

    /**
     * @param driver
     */
    public SearchResultsPage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public boolean isCurrentPage() {

        final String message = "Your search for";
        return getMainText().contains(message);
    }

    @Override
    public String getPageRelativeURL() {
        // YTODO Auto-generated method stub
        return null;
    }
}

/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import static org.fest.assertions.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.common.ShoppingBasketItem;


/**
 * Shopping Basket page
 * 
 */
public class ShoppingBasketPage extends TemplatePage {

    private static final String NO_ITEMS_TEXT_FRAGMENT = "no items";

    private final By buyNowTabLink = By.cssSelector("a[href='#tab-buynow']");
    private final By basketItems = By.cssSelector("table.basket-table tbody tr");
    private final By continueButton = By.cssSelector("div.next-action a.forward");
    private final By continueShoppingButton = By.cssSelector("div.next-action a.continue");
    private final By subtotal = By.cssSelector("tr.row-subtotal td.price");
    private final By total = By.cssSelector("tr.row-total td.price");
    private final By emptyShoppingBasketMessage = By.cssSelector("div.main p");


    /**
     * @param driver
     */
    public ShoppingBasketPage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public boolean isCurrentPage() {

        final String headingText = getText(fwd().link(buyNowTabLink));
        return headingText.startsWith("Items in Your Basket");
    }

    @Override
    public String getPageRelativeURL() {
        return "basket";
    }

    public int getItemCount() {

        final String count = getText(fwd().link(buyNowTabLink).span(By.className("size")).span(By.className("num")));
        assertThat(count).as("cant read basket count").isNotNull();
        return Integer.parseInt(count);
    }


    public List<ShoppingBasketItem> getItems()
    {
        final List<FluentWebElement> elements = fwd().trs(basketItems);
        final List<ShoppingBasketItem> results = new ArrayList<ShoppingBasketItem>(elements.size());

        for (final FluentWebElement element : elements)
        {
            results.add(new ShoppingBasketItem(element));
        }

        return results;
    }

    public ShoppingBasketItem findItemByName(final String name)
    {
        ShoppingBasketItem result = null;
        for (final ShoppingBasketItem item : getItems())
        {
            if (name.equalsIgnoreCase(item.getName()))
            {
                result = item;
                break;
            }
        }
        return result;
    }

    public void clickContinueProductsCheckout() {
        fwd().link(continueButton).click();
    }

    public HomePage clickContinueShoppingButton() {
        fwd().link(continueShoppingButton).click();
        final HomePage shoppingBasePage = new HomePage(getBrowserDriver());
        return shoppingBasePage;
    }

    public String getSubtotal() {

        return getText(fwd().td(subtotal));
    }

    public String getTotal() {

        return getText(fwd().td(total));
    }

    public boolean isShoppingBasketEmpty() {
        return getText(fwd().p(emptyShoppingBasketMessage)).contains(NO_ITEMS_TEXT_FRAGMENT);
    }

    /**
     * returns a shoping basket item by its item code.
     * 
     * @param itemCode
     *            the item code.
     * @return ShoppingBasketItem a certain shopping basket item by its code.
     */
    public ShoppingBasketItem getShoppingItemFromBasket(final String itemCode) {
        for (final ShoppingBasketItem item : getItems()) {
            if (item.getItemCode().equals(itemCode)) {
                return item;
            }
        }
        return null;
    }


    /**
     * calculate the total price by iterating trough all Product Items in the Shopping basket.
     * 
     * @return the total of all product items in the shopping basket page as a BigDecimal instance.
     */
    public BigDecimal getCalculatedProductTotalByProductItem() {
        BigDecimal result = BigDecimal.ZERO;
        for (final ShoppingBasketItem itemToCheck : getItems()) {
            result = result.add(itemToCheck.getTotalPrice());
        }

        return result;
    }


}

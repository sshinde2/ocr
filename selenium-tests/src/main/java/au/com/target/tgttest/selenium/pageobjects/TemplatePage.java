/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.common.BreadCrumb;
import au.com.target.tgttest.selenium.pageobjects.common.Enews;
import au.com.target.tgttest.selenium.pageobjects.common.Footer;
import au.com.target.tgttest.selenium.pageobjects.common.GlobalHeader;
import au.com.target.tgttest.selenium.pageobjects.common.MegaMenu;


/**
 * Template page with header and footer etc
 * 
 */
public abstract class TemplatePage extends BasePage {

    private final GlobalHeader header;
    private final Footer footer;
    private final MegaMenu megaMenu;
    private final BreadCrumb breadcrumb;
    private final Enews enews;


    /**
     * @return the enews
     */
    private final By main = By.cssSelector("div.main");
    private final By mainBanner = By.cssSelector("div.main div.banner");

    private final By globleMsg = By.cssSelector("div.global-messages");

    public abstract String getPageRelativeURL();

    public abstract boolean isCurrentPage();



    /**
     * Constructor
     * 
     * @param driver
     */
    public TemplatePage(final BrowserDriver driver) {
        super(driver);
        header = new GlobalHeader(driver);
        footer = new Footer(driver);
        megaMenu = new MegaMenu(driver);
        breadcrumb = new BreadCrumb(driver);
        enews = new Enews(driver);
    }

    public GlobalHeader getHeader() {
        return header;
    }

    public Footer getFooter() {
        return footer;
    }

    public MegaMenu getMegaMenu() {
        return megaMenu;
    }

    public BreadCrumb getBreadCrumb() {
        return breadcrumb;
    }

    public Enews getEnews() {
        return enews;
    }

    public String getMainText() {
        return getText(fwd().div(main));
    }

    public String getMainH2Text() {
        return getText(fwd().div(main).h2());
    }

    public String getMainH1Text() {
        return getText(fwd().div(main).h1());
    }

    public List<String> getGlobalMsgs() {

        final List<String> ret = new ArrayList<String>();

        for (final FluentWebElement elt : fwd().div(globleMsg).h3s()) {
            ret.add(getText(elt));
        }
        return ret;
    }

    public boolean hasMainBanner() {
        return isDisplayed(fwd().div(mainBanner));
    }




}

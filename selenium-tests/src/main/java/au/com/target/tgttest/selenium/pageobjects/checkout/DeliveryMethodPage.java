package au.com.target.tgttest.selenium.pageobjects.checkout;

import static org.seleniumhq.selenium.fluent.Period.secs;

import java.util.List;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * A Selenium page base class for Delivery methods (such is f.e. home delivery or click & collect). This page is the
 * first page during a checkout procedure.
 * 
 * --> CHECKOUT STEP NUMBER: 1
 * 
 * @author maesi
 * 
 */
public class DeliveryMethodPage extends BaseCheckoutStepPage {

    private static final String ID_SELECTOR_PICKUPDETAIL_TITLE_COMBOBOX = "cnc_title_chosen";
    private static final String ID_SELECTOR_PICKUPDETAIL_FIRSTNAME = "cnc-firstName";
    private static final String ID_SELECTOR_PICKUPDETAIL_SURNAME = "cnc-surname";
    private static final String ID_SELECTOR_PICKUPDETAIL_PHONENUMBER = "cnc-mobileNumber";
    private static final String ID_SELECTOR_BUTTON_CONTINUE_SECURELY = "co-submit-form";
    private static final String NAME_SELECTOR_STORE_SELECTOR_LIST = "sSN";
    private static final String CSS_SELECTOR_STORE_COLLECTOR_LIST_ITEMS = "ul.store-list li.available";
    private static final String CSS_SELECTOR_COMMIT_STORE_FROM_SELECTOR_BUTTON = "a.button-norm.collect-from-store";
    private static final String CSS_SELECTOR_HOMEDELIVERY_RADIO_VALUE = "div.co-groupe.del-mode.del-mode-home-delivery";
    private static final String CSS_SELECTOR_RADIO_CHECK_VALUE = "input.radio.del-mode-radio";
    private static final String CSS_SELECTOR_CLICK_AND_COLLECT_RADIO_VALUE = "div.co-groupe.del-mode.del-mode-click-and-collect";
    private static final String CSS_SELECTOR_HOMEDELIVERY_ADDRESS_LINK_WRAPPER = "p.actions";

    private final By radioValueHomeDelivery = By.cssSelector(CSS_SELECTOR_HOMEDELIVERY_RADIO_VALUE);
    private final By homeDeliveryAddressLinkWrapper = By.cssSelector(CSS_SELECTOR_HOMEDELIVERY_ADDRESS_LINK_WRAPPER);
    private final By radioValueClickAndCollect = By.cssSelector(CSS_SELECTOR_CLICK_AND_COLLECT_RADIO_VALUE);
    private final By continueButton = By.id(ID_SELECTOR_BUTTON_CONTINUE_SECURELY);
    private final By pickupStateSelectDropdownDiv = By.cssSelector("div#cnc_state_chosen");


    /**
     * constructor
     * 
     * @param driver
     *            a WebDriver instance.
     */
    public DeliveryMethodPage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public String getPageRelativeURL() {
        return "/checkout/your-address";
    }

    @Override
    public boolean isCurrentPage() {
        return isDisplayed(fwd().div(radioValueHomeDelivery));
    }

    /**
     * selects the home delivery option.
     */
    public void selectHomeDelivery() {

        fwd().div(radioValueHomeDelivery).isDisplayed().shouldBe(Boolean.TRUE);

        final FluentWebElement inputHomeDelivery = fwd().div(radioValueHomeDelivery).input(By
                .cssSelector("input.radio.del-mode-radio"));

        inputHomeDelivery.isDisplayed().shouldBe(Boolean.TRUE);

        LOG.info("Selecting home delivery");
        inputHomeDelivery.click();
    }


    /**
     * determines whether home delivery is selected or not.
     * 
     * @return boolean true/false whether home delivery checkbox is selected or not.
     */
    public boolean isHomeDeliverySelected() {

        fwd().div(radioValueHomeDelivery).isDisplayed().shouldBe(Boolean.TRUE);

        final FluentWebElement radio = fwd().div(radioValueHomeDelivery).input(By
                .cssSelector(CSS_SELECTOR_RADIO_CHECK_VALUE));

        radio.isDisplayed().shouldBe(Boolean.TRUE);

        return radio.isSelected().value().booleanValue();
    }


    /**
     * selects the 'click and collect' option.
     */
    public void selectClickAndCollect() {

        fwd().div(radioValueClickAndCollect).isDisplayed().shouldBe(Boolean.TRUE);

        final FluentWebElement inputClickAndCollect = fwd().div(radioValueClickAndCollect).input(By
                .cssSelector("input.radio.del-mode-radio"));

        inputClickAndCollect.isDisplayed().shouldBe(Boolean.TRUE);

        LOG.info("Selecting click and collect");
        inputClickAndCollect.click();

    }

    /**
     * returns a boolean value whether the click & collect option is selected or not.
     * 
     * @return boolean true/false, depending on the selection state of the 'click and collect checkbox'.
     */
    public boolean isClickAndCollectSelected() {

        fwd().div(radioValueClickAndCollect).isDisplayed().shouldBe(Boolean.TRUE);

        final FluentWebElement radio = fwd().div(radioValueClickAndCollect).input(By
                .cssSelector(CSS_SELECTOR_RADIO_CHECK_VALUE));

        radio.isDisplayed().shouldBe(Boolean.TRUE);

        return radio.isSelected().value().booleanValue();
    }

    /**
     * selects 'click&collect' and selects a certain state from the state selector.
     * 
     * @param displayedValue
     *            a state name as displayed value.
     * @return boolean true/false whether the workflow could be successfully processed or not.
     */
    public boolean selectClickAndCollectStateByText(final String displayedValue) {

        return getComponentUtil().selectOptionByValue(fwd().div(pickupStateSelectDropdownDiv), displayedValue, true);
    }

    private List<FluentWebElement> getAllDisplayedCollectionStores() {
        return fwd().lis(By.cssSelector(CSS_SELECTOR_STORE_COLLECTOR_LIST_ITEMS));
    }

    public boolean hasStores() {
        return !getAllDisplayedCollectionStores().isEmpty()
                && getAllDisplayedCollectionStores().get(0)
                        .ifInvisibleWaitUpTo(secs(5)).isDisplayed().value().booleanValue();

    }

    /**
     * selects a store where the goods will be collected.
     * 
     * @param option
     *            an option representing a store from the store list.
     * @param confirmSelection
     *            boolean true/false whether the confirmation button should be triggered or not.
     */
    private void selectCollectStore(final FluentWebElement option, final boolean confirmSelection) {
        if (option != null) {
            LOG.info("Selecting store for cnc: " + option);
            final FluentWebElement clickElement = option.input(By.name(NAME_SELECTOR_STORE_SELECTOR_LIST));
            clickElement.click();
            if (confirmSelection) {
                final FluentWebElement confirmButton = fwd().link(
                        By.cssSelector(CSS_SELECTOR_COMMIT_STORE_FROM_SELECTOR_BUTTON));

                confirmButton.isDisplayed().shouldBe(Boolean.TRUE);

                confirmButton.click();
            }

        }
    }

    /**
     * selects the first possible value from the store selector for click and collect option.
     * 
     */
    public boolean selectFirstPossibleCollectStore() {

        for (final FluentWebElement storeCollectElement : getAllDisplayedCollectionStores()) {

            storeCollectElement.ifInvisibleWaitUpTo(secs(5)).isDisplayed().shouldBe(Boolean.TRUE);
            selectCollectStore(storeCollectElement, false);
            return true;
        }

        return false;
    }


    /**
     * returns whether the continue button is clickable or not.
     * 
     * @return boolean whether the continue button is clickable or not.
     */
    public boolean isContinueButtonClickable() {
        final FluentWebElement continueElt = fwd().button(continueButton);
        return isDisplayed(continueElt) && isEnabled(continueElt);
    }

    /**
     * triggers the link "use this address" for home delivery and returns the new loaded Payment Detail Page or Lay-by
     * Details Page, depending what kind of checkout process we are running trouhg. In case the link could not be
     * found/determinded or is not shown, an exception will be thrown!
     * 
     * @return PaymentDetailsPage or LayByDetailsPage (depending on the checkout process) if the link can be fond and
     *         clicked, otherwise an Error exception will be thrown!
     */
    public BaseCheckoutStepPage clickHomeDeliveryUseThisAddressLink() {

        LOG.info("Clicking use this address for home delivery link");

        fwd().p(homeDeliveryAddressLinkWrapper).isDisplayed().shouldBe(Boolean.TRUE);


        final List<FluentWebElement> links = fwd().p(homeDeliveryAddressLinkWrapper).links(By.tagName("a"));
        if (!links.isEmpty()) {
            for (final FluentWebElement link : links) {
                if (link.getText().toString().contains("Use this address")) {

                    link.click();

                    return new PaymentDetailsPage(getBrowserDriver());
                }
            }
        }

        LOG.warn("No addresses available for home delivery");
        return this;
    }


    /**
     * method to check if the home delivery panel shows some existing addresses to choose from. true if there are some
     * addresses already persistent, false if there is no active address for home delivery persistent on the currently
     * logged in user.
     * 
     * @return true/false whether the currently logged in user has an existing address ort not.
     */
    public boolean isHomeDeliveryWrapperDisplayingSomeExistingAddresses() {

        if (!fwd().p(homeDeliveryAddressLinkWrapper).ifInvisibleWaitUpTo(secs(5)).isDisplayed().value().booleanValue()) {
            return false;
        }

        final List<FluentWebElement> links = fwd().p(homeDeliveryAddressLinkWrapper).links();
        return (!links.isEmpty());
    }


    /**
     * triggers the continue button and proceeds the checkout process to the next step, which is the home delivery
     * address page or the payment details, in case of click and collect.
     * 
     * @return a CheckoutPaymentDetailsPage, a DeliveryMethodPageHomeDeliveryAddress or a LayByDetailsPage, depending if
     *         the checkout process runs in a LayByCheckout mode or a simpl 'buy now mode' which decides the return page
     *         on the delivery mode (click and collect or home delivery). if somehow something could not be processed,
     *         this method returns null.
     */
    public BaseCheckoutStepPage clickContinueButton() {

        LOG.info("Clicking continue button");
        final boolean isHomeDeliverySelected = isHomeDeliverySelected();

        fwd().button(continueButton).click();

        if (isHomeDeliverySelected) {
            return new DeliveryMethodPageHomeDeliveryAddress(getBrowserDriver());
        }
        else {
            return new PaymentDetailsPage(getBrowserDriver());
        }

    }

    public String getCnCFirstNameValue() {

        fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_FIRSTNAME)).isDisplayed().shouldBe(Boolean.TRUE);
        return getInputText(fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_FIRSTNAME)));
    }

    public String getCnCLastNameValue() {

        fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_SURNAME)).isDisplayed().shouldBe(Boolean.TRUE);
        return getInputText(fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_SURNAME)));
    }

    public String getCnCPhoneNumberValue() {

        fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_PHONENUMBER)).isDisplayed().shouldBe(Boolean.TRUE);
        return getInputText(fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_PHONENUMBER)));
    }


    /**
     * populates the pick up detail with data...
     * 
     * @param preTitle
     *            String for Title, such is Mr, Ms, Dr., etc...
     * @param firstName
     *            String a first name.
     * @param lastName
     *            String a last name.
     * @param phoneNumber
     *            String a phone number.
     */
    public void populateAndCheckPickUpDetails(final String preTitle, final String firstName, final String lastName,
            final String phoneNumber) {

        if (!getComponentUtil().selectOptionByValue(ID_SELECTOR_PICKUPDETAIL_TITLE_COMBOBOX, preTitle)) {
            throw new Error("The Title could not be selected with value '" + preTitle + "'.");
        }

        final FluentWebElement elFirstName = fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_FIRSTNAME));
        final FluentWebElement elLastName = fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_SURNAME));
        final FluentWebElement elPhoneNumber = fwd().input(By.id(ID_SELECTOR_PICKUPDETAIL_PHONENUMBER));

        elFirstName.clearField().sendKeys(firstName);
        elLastName.clearField().sendKeys(lastName);
        elPhoneNumber.clearField().sendKeys(phoneNumber);
    }



}
/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.checkout;

import static org.seleniumhq.selenium.fluent.Period.secs;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.datastore.bean.Customer;


/**
 * A Selenium page base class for Home Delivery method Address data form. This page is a subpage of the first page
 * during a checkout procedure and only appears if home delivery is active.
 * 
 * --> CHECKOUT STEP NUMBER: 1 (sub page - follows when Delivery method continues with home delivery mode.)
 * 
 * @author maesi
 * 
 */
public class DeliveryMethodPageHomeDeliveryAddress extends BaseCheckoutStepPage {

    private static final String ID_DIV_SELECTOR_TITLE_DROPDOWN = "address_title_chosen";
    private static final String ID_DIV_SELECTOR_STATE_DROPDOWN = "address_state_chosen";

    private final By elFirstname = By.cssSelector("address.firstName");
    private final By elSurname = By.cssSelector("address.surname");
    private final By elAddressLine1 = By.cssSelector("address.line1");
    private final By elAddressLine2 = By.cssSelector("address.line2");
    private final By elAddressTownCity = By.cssSelector("address.townCity");
    private final By elAddressPostcode = By.cssSelector("address.postcode");
    private final By elPhoneNumber = By.cssSelector("address.phoneNumber");
    private final By saveAddressButton = By.cssSelector("button.button-fwd");
    private final By cancelButton = By.cssSelector("a.button-norm");


    /**
     * constructor
     * 
     * @param driver
     *            the WebDriver instance.
     */
    public DeliveryMethodPageHomeDeliveryAddress(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public boolean isCurrentPage() {
        return isDisplayed(fwd().button(saveAddressButton));
    }

    @Override
    public String getPageRelativeURL() {
        return "/checkout/your-address/add";
    }

    /**
     * triggers the save address button and proceeds back to the delivery methods page with an active address for home
     * delivery.
     * 
     * as a return we either get a LayByDetailsPage or a PaymentDetailsPage instance, depending on what checkout way we
     * are running on (buy now product or lay-by product).
     * 
     * @return a BaseCheckoutStepPage (LayByDetailsPage or a PaymentDetailsPage) if everything loaded correctly,
     *         otherwise this method returns null.
     */
    public BaseCheckoutStepPage clickSaveAddressButtonAndConfirmAddressDialog() {

        fwd().button(saveAddressButton).click();

        // the address verifier pop up...
        //PageUtil.waitForPageToLoad(getDriver());
        //getDriver().switchTo().activeElement();

        final FluentWebElement overlayWrapper = fwd().form(
                By.cssSelector("form#confirmAddressForm.confirm-address")).within(secs(5));

        final FluentWebElement useThisAddressCheckbox = overlayWrapper.input(By.id("address.supplied")).within(secs(5));

        if (!useThisAddressCheckbox.isSelected().value().booleanValue()) {
            useThisAddressCheckbox.click();
        }

        overlayWrapper.button(By.cssSelector("button.button-fwd")).click();

        return new PaymentDetailsPage(getBrowserDriver());
    }



    /**
     * populates the home delivery tab with data...
     * 
     * 
     */
    public void populateHomeDeliveryDetails(final Customer customer) {

        if (!getComponentUtil().selectOptionByValue(ID_DIV_SELECTOR_TITLE_DROPDOWN, customer.getTitle())) {
            throw new Error("The Title select could not be selected with the value '" + customer.getTitle() + "'.");
        }

        fwd().input(elFirstname).clearField().sendKeys(customer.getFirstname());
        fwd().input(elSurname).clearField().sendKeys(customer.getSurname());
        fwd().input(elAddressLine1).clearField().sendKeys(customer.getAddr1());
        fwd().input(elAddressLine2).clearField().sendKeys(customer.getAddr2());
        fwd().input(elAddressPostcode).clearField().sendKeys(customer.getPostcode());
        fwd().input(elAddressTownCity).clearField().sendKeys(customer.getCity());
        fwd().input(elPhoneNumber).clearField().sendKeys(customer.getPhone());

        if (!getComponentUtil().selectOptionByValue(ID_DIV_SELECTOR_STATE_DROPDOWN, customer.getState())) {
            throw new Error("The State select could not be selected with the value ' " + customer.getState() + "'.");
        }

    }


}

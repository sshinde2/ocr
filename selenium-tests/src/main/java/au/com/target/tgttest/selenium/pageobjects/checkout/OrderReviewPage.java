package au.com.target.tgttest.selenium.pageobjects.checkout;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * A Selenium page base class for Order review page. This page is the third page during a checkout procedure.
 * 
 * @author maesi
 * 
 */
public class OrderReviewPage extends BaseCheckoutStepPage {

    private final By buttonPaySecurelyNow = By.id("co-submit-form");

    private final By linkBackToPaymentDetails = By.cssSelector("a.continue.continue-large");
    private final By linkToChangeDeliveryAddress = By.cssSelector("div.summary p.actions a");
    private final By linkToChangeBillingAddress = By.cssSelector("div.summary-detail p.actions a");

    /**
     * constructor
     * 
     * @param driver
     *            current WebDriver.
     */
    public OrderReviewPage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public boolean isCurrentPage() {
        return isDisplayed(fwd().button(buttonPaySecurelyNow));
    }

    @Override
    public String getPageRelativeURL() {
        return "/checkout/review-your-order";
    }


    /**
     * this method triggers the "pay securely now" button and starts the payment process... if everything works well,
     * the Thank you page will be returned. otherwise we will get an Error on this page.
     * 
     * TODO do check the error message and then return the page accordingly to what we have...
     * 
     * @return ThankyouPage as an instance if everything went well with the payment. otherwise we will have the same
     *         page returned with an error message.
     * 
     */
    public ThankyouPage clickPaySecurelyNowButton() {

        fwd().button(buttonPaySecurelyNow).isDisplayed().shouldBe(Boolean.TRUE);
        fwd().button(buttonPaySecurelyNow).isEnabled().shouldBe(Boolean.TRUE);
        fwd().button(buttonPaySecurelyNow).click();

        return new ThankyouPage(getBrowserDriver());
    }

}
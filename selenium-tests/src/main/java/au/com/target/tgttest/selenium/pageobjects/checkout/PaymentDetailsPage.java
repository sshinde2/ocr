package au.com.target.tgttest.selenium.pageobjects.checkout;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.datastore.bean.CreditCard;


/**
 * A Selenium base page for checkout Payment Details. This Checkout page is currently the second page called during the
 * checkout procedure and hosts beside the payment type selection as well the order summary, a flybuys card number entry
 * field and total amount label.
 * 
 * --> CHECKOUT STEP NUMBER: 2
 * 
 * @author maesi
 * 
 */
public class PaymentDetailsPage extends BaseCheckoutStepPage {


    private static final String ID_DIV_SELECTOR_TITLE_DROPDOWN = "address_title_chosen";
    private static final String ID_DIV_SELECTOR_STATE_DROPDOWN = "address_state_chosen";
    private static final String ID_DIV_SELECTOR_COUNTRY_DROPDOWN = "address_country_chosen";

    private static final String ID_SELECTOR_RADIO_BUTTON_PAYPAL = "card-paypal";
    private static final String ID_SELECTOR_RADIO_BUTTON_CREDIT_CARD = "credit-card";

    private final By conditionsOfUseCheckBox = By.cssSelector("input#termsCheck.f-opt-input");
    private final By goBackButtonLink = By.cssSelector("a.continue.continue-large");
    private final By continueButton = By.id("co-submit-form");

    private final By radioValuePaypal = By.id(ID_SELECTOR_RADIO_BUTTON_PAYPAL);
    private final By radioValueCreditCardPayment = By.id(ID_SELECTOR_RADIO_BUTTON_CREDIT_CARD);

    // vouchers etc
    private final By flybuysCardNumber = By.cssSelector("input#flybuys");
    private final By teamMemberEntry = By.cssSelector("input#payment.teamMemberCode");
    private final By voucher = By.cssSelector("input#payment.voucher");


    // billing details
    private final By elTitleSelect = By.cssSelector(ID_DIV_SELECTOR_TITLE_DROPDOWN);
    private final By elFirstname = By.cssSelector("address.firstName");
    private final By elSurname = By.cssSelector("address.surname");
    private final By elAddressLine1 = By.cssSelector("address.line1");
    private final By elAddressLine2 = By.cssSelector("address.line2");
    private final By elAddressTownCity = By.cssSelector("address.townCity");
    private final By elAddressStateSelect = By.cssSelector(ID_DIV_SELECTOR_STATE_DROPDOWN);
    private final By elAddressPostcode = By.cssSelector("address.postcode");
    private final By elCountrySelect = By.cssSelector(ID_DIV_SELECTOR_COUNTRY_DROPDOWN);


    /**
     * constructor
     * 
     * @param driver
     *            WebDriver
     */
    public PaymentDetailsPage(final BrowserDriver driver) {
        super(driver);
    }

    @Override
    public String getPageRelativeURL() {
        return "/checkout/payment-details";
    }

    @Override
    public boolean isCurrentPage() {
        return isDisplayed(getCreditCardRadioElement());
    }

    private FluentWebElement getPayPalRadioElement() {
        return fwd().input(radioValuePaypal);
    }

    private FluentWebElement getCreditCardRadioElement() {
        return fwd().input(radioValueCreditCardPayment);
    }


    /**
     * activates the pay pal payment radio value.
     */
    public void activatePayPalPayment() {

        final FluentWebElement radio = getPayPalRadioElement();
        radio.isDisplayed().shouldBe(Boolean.TRUE);

        if (!isSelected(radio)) {
            radio.click();
        }
    }

    /**
     * activates the credit card payment radio value and opens its div-tab.
     */
    public void activateCreditCardPayment() {

        final FluentWebElement radio = getCreditCardRadioElement();
        radio.isDisplayed().shouldBe(Boolean.TRUE);

        if (!radio.isSelected().value().booleanValue()) {
            radio.click();
        }
    }

    /**
     * returns true/false whether the paypal is activated or not.
     * 
     * @return boolean whether paypal select is activated or not.
     */
    public boolean isPayPalPaymentActivated() {
        try {
            return isSelected(getPayPalRadioElement());
        }
        catch (final Exception e) {
            return false;
        }
    }

    /**
     * returns true/false whether the credit card payment radio checkbox is activated or not.
     * 
     * @return boolean whether credit card payment is activated or not.
     */
    public boolean isCreditCardPaymentActivated() {
        return isSelected(getCreditCardRadioElement());
    }


    /**
     * this method fills the flybuys entry with certain test related data.
     * 
     * @param flyBuyValue
     */
    public void fillFlybuysEntry(final String flyBuyValue) {
        fwd().input(flybuysCardNumber).clearField().sendKeys(flyBuyValue);
    }

    /**
     * returns whether the continue button is clickable or not.
     * 
     * @return boolean whether the continue button is clickable or not.
     */
    public boolean isContinueButtonClickable() {
        return isEnabled(fwd().button(continueButton));
    }

    /**
     * returns whether the go back button is clickable or not.
     * 
     * @return boolean whether the go back button is clickable or not.
     */
    public boolean isGoBackButtonLinkClickable() {
        return isEnabled(fwd().button(goBackButtonLink));
    }


    /**
     * triggers the continue button and proceeds the checkout process to the next step, which is the order review page.
     * 
     * @return a OrderReviewPage, if everything could be loaded correctly, otherwise this method returns null.
     * 
     * 
     */
    public BasePage clickContinueButton() {
        if (isContinueButtonClickable()) {

            final boolean isPayPal = isPayPalPaymentActivated();

            fwd().button(continueButton).click();

            if (isPayPal) {
                return new PaypalPaymentPage(getBrowserDriver());
            }
            else {
                return new OrderReviewPage(getBrowserDriver());
            }
        }
        return null;
    }


    /**
     * triggers the go back button link and go back one step in the checkout process, which is the delivery method page.
     * 
     * @return a DeliveryMethodPage, if everything could be loaded correctly, otherwise this method returns null.
     */
    public DeliveryMethodPage clickGoBackButtonLink() {
        if (isGoBackButtonLinkClickable()) {
            fwd().button(goBackButtonLink);
            return new DeliveryMethodPage(getBrowserDriver());
        }
        return null;
    }


    /**
     * returns true or false whether the condition of use checkbox is activated or not.
     * 
     * @return boolean true / false depending if condition of terms is selected or not.
     */
    public boolean isConditionOfUseCheckboxActivated() {
        return isSelected(fwd().input(conditionsOfUseCheckBox));
    }

    /**
     * activates/deactivates the condition of use checkbox...
     */
    public void clickConditionOfUseCheckbox() {
        fwd().input(conditionsOfUseCheckBox).click();
    }


    /**
     * populates data to the credit card payment tab.
     * 
     * @param card
     */
    public void populateCreditCardDetails(final CreditCard card) {

        if (!isCreditCardPaymentActivated()) {
            throw new Error(
                    "You must have credit card payment activated before you can populate creditcard data to its components.");
        }

        fwd().input(By.id("payment.cardNumber")).clearField().sendKeys(card.getNumber());
        fwd().input(By.id("payment.nameOnCard")).clearField().sendKeys(card.getName());
        fwd().input(By.id("payment.issueNumber")).clearField().sendKeys(card.getSecurityCode());

        if (!getComponentUtil().selectOptionByValue("payment_expiryMonth_chosen", card.getExpiryMonth())) {
            throw new Error("The expiry month dropdown could not be selected to the option value '"
                    + card.getExpiryMonth()
                    + "'.");
        }

        if (!getComponentUtil().selectOptionByValue("payment_expiryYear_chosen", card.getExpiryYear())) {
            throw new Error("The expiry year dropdown could not be selected to the option value '"
                    + card.getExpiryYear()
                    + "'.");
        }

    }


    /**
     * this method populates all data into the billing address tab.
     * 
     * @param title
     * @param firstName
     * @param lastName
     * @param addressLine1
     * @param addressLine2
     * @param suburb
     * @param state
     * @param postcode
     * @param country
     * 
     *            TODO merge with DeliveryMethodPageHomeDeliveryAddress address one day, as it should be the same
     *            form...
     */
    public void populateBillingDetails(final String title, final String firstName, final String lastName,
            final String addressLine1, final String addressLine2, final String suburb, final String state,
            final String postcode, final String country) {

        // TODO
    }


}
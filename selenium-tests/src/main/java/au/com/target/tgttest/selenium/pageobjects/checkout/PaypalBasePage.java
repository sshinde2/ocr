/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.checkout;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * A base page for Paypal sandbox selenium tests.
 * 
 * @author maesi
 * 
 */
public class PaypalBasePage extends BasePage {

    private final By cancelAndReturnToOriginPageLinkButton = By.cssSelector("input#submit");


    /**
     * constructor
     * 
     * @param driver
     */
    public PaypalBasePage(final BrowserDriver driver) {
        super(driver);
    }

}

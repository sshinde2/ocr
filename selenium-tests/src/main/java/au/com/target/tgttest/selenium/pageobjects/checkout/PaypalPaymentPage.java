/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.checkout;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.datastore.bean.Customer;
import au.com.target.tgttest.selenium.datastore.bean.PaypalCreditCard;


/**
 * A page instance, which tests the paypal checkout via sandbox...
 * 
 * @author maesi
 * 
 */
public class PaypalPaymentPage extends PaypalBasePage {

    private static final String ID_SELECTOR_PANEL_BILLINGMODULE = "billingModule";


    // progress button ID selectors.
    private static final String ID_SELECTOR_ACCOUNT_LOGIN_BUTTON = "submitLogin";
    private static final String ID_SELECTOR_CREDIT_CARD_SUBMIT_BILLING_BUTTON = "submitBilling";

    // paypal account payment ID selectors.
    private static final String ID_SELECTOR_ACCOUNT_EMAIL_ADDRESS = "login_email";
    private static final String ID_SELECTOR_ACCOUNT_PASSWORD = "login_password";

    // credit card guest payment ID selectors.
    private static final String ID_SELECTOR_CREDIT_CARD_COUNTRY_SELECT = "country_code";
    private static final String ID_SELECTOR_CREDIT_CARD_NUMBER = "cc_number";
    private static final String ID_SELECTOR_CREDIT_CARD_EXPIRY_MONTH = "expdate_month";
    private static final String ID_SELECTOR_CREDIT_CARD_EXPIRY_YEAR = "expdate_year";
    private static final String ID_SELECTOR_CREDIT_CARD_SECURITY_CODE = "cvv2_number";
    private static final String ID_SELECTOR_CREDIT_CARD_FIRST_NAME = "first_name";
    private static final String ID_SELECTOR_CREDIT_CARD_LAST_NAME = "last_name";
    private static final String ID_SELECTOR_CREDIT_CARD_ADDRESS1 = "address1";
    private static final String ID_SELECTOR_CREDIT_CARD_ADDRESS2 = "address2";
    private static final String ID_SELECTOR_CREDIT_CARD_SUBURB = "city";
    private static final String ID_SELECTOR_CREDIT_CARD_STATE_SELECT = "state";
    private static final String ID_SELECTOR_CREDIT_CARD_POSTCODE = "zip";
    private static final String ID_SELECTOR_CREDIT_CARD_PHONE_NUMBER = "H_PhoneNumber";
    private static final String ID_SELECTOR_CREDIT_CARD_EMAIL_ADDRESS = "email-address";


    private final By billingContainer = By.id(ID_SELECTOR_PANEL_BILLINGMODULE);
    private final By reviewAndContinueButton = By.id("submitBilling");
    private final By paypalLoginEmail = By.id(ID_SELECTOR_ACCOUNT_EMAIL_ADDRESS); // input
    private final By paypalLoginPassword = By.id(ID_SELECTOR_ACCOUNT_PASSWORD); // input
    private final By loginButton = By.id(ID_SELECTOR_ACCOUNT_LOGIN_BUTTON); // input
    private final By submitPaymentButton = By.id(ID_SELECTOR_CREDIT_CARD_SUBMIT_BILLING_BUTTON); // input

    private final By payWithAccountButton = By.id("loadLogin"); // input
    private final By payWithACardButton = By.id("minipageSubmitBtn"); // input


    /**
     * constructor
     * 
     * @param driver
     */
    public PaypalPaymentPage(final BrowserDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return isDisplayed(getBillingContainer());
    }

    /**
     * returns the billing container div.
     * 
     * @return the WebElement for the billing container.
     */
    private FluentWebElement getBillingContainer() {
        return fwd().div(billingContainer);
    }


    /**
     * the web element for the main link "pay with paypal account".
     * 
     * @return a WebElement instance of the link.
     */
    private FluentWebElement getLinkPayWithPayPalAccount() {
        return fwd().input(payWithAccountButton);
    }

    /**
     * the web element for the main link "pay with a credit card".
     * 
     * @return a WebElement instance of the link.
     */
    private FluentWebElement getLinkPayWithACard() {
        return fwd().input(payWithACardButton);
    }

    /**
     * the web element for the main button "review nd continue", which opens the payment panel over AJAX / JS.
     * 
     * @return a WebElement instance of the button.
     */
    protected FluentWebElement getButtonReviewAndContinue() {
        return fwd().input(reviewAndContinueButton);
    }


    /**
     * returns the account login button. this is wrapped into a exists method, as the ajax initialization might have
     * been called a bit later trough triggering the link.
     * 
     * @return the WebElement for the account login button.
     */
    private FluentWebElement getAccountLoginButton() {
        return fwd().input(loginButton);
    }


    /**
     * returns the submit billing (review and continue) button from the credit card (guest user) payment tab.
     * 
     * @return the WebElement for the credit card payment trigger button.
     */
    private FluentWebElement getCreditCardSubmitBillingButton() {
        return fwd().input(submitPaymentButton);
    }


    /**
     * triggers a link for paying with card and opens the panel with cc entries.
     */
    public void clickPayWithCard() {
        getLinkPayWithACard().click();
    }


    public FluentWebElement getCreditCardCountrySelect() {
        return fwd().select(By.id(ID_SELECTOR_CREDIT_CARD_COUNTRY_SELECT));
    }

    /**
     * determines whether the credit card payment panel is activ or not.
     * 
     * @return true/false whether the credit card payment panel is active or not.
     */
    public boolean isCreditCardPaymentActive() {
        return isDisplayed(getCreditCardCountrySelect());
    }


    /**
     * triggers the link pay with paypal account and opens the panel with the paypal account.
     */
    public void clickPayWithAccount() {
        getLinkPayWithPayPalAccount().click();
    }


    /**
     * clicks on the account login button and returns a new paypal account payment page.
     * 
     * @return PaypalPaymentOverAccountPage if click on login button was successful, otherwise null is returned.
     */
    public PaypalPaymentReviewPage clickAccountLogin() {
        getAccountLoginButton().click();
        return new PaypalPaymentReviewPage(getBrowserDriver());
    }

    /**
     * clicks the pay with credit card billing button and progresses back to the site thankyou page.
     * 
     * @return returns a ThankyouPage.
     */
    public ThankyouPage clickPayWithCreditCard() {
        getCreditCardSubmitBillingButton().click();
        return new ThankyouPage(getBrowserDriver());
    }


    /**
     * this method populates login data into the paypal account payment div. please make sure the tab is open and
     * displayed before you call this method!
     * 
     * @param email
     * @param password
     */
    public void populatePaypalAccountLoginData(final String email, final String password) {

        fwd().input(paypalLoginEmail).sendKeys(email);
        fwd().input(paypalLoginPassword).sendKeys(password);
    }


    /**
     * this method populates data into the credit card entries of the paypal sandbox... please make sure the credit card
     * tab is open and displayed before you call this method!
     * 
     * 
     * @param paypalCard
     */
    public void populateCreditCardData(final PaypalCreditCard paypalCard) {

        if (!isCreditCardPaymentActive()) {
            throw new Error(
                    "You must have credit card payment activated before you can populate creditcard "
                            + "data to its components.");
        }

        if (paypalCard.getNumber() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_NUMBER)).clearField().sendKeys(paypalCard.getNumber());
        }

        if (paypalCard.getExpiryMonth() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_EXPIRY_MONTH)).clearField().sendKeys(paypalCard.getExpiryMonth());
        }

        if (paypalCard.getExpiryYear() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_EXPIRY_YEAR)).clearField().sendKeys(paypalCard.getExpiryYear());
        }

        if (paypalCard.getSecurityCode() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_SECURITY_CODE)).clearField()
                    .sendKeys(paypalCard.getSecurityCode());
        }

    }

    /**
     * method to populate all billing address related data to the paypal sandbox page with activated credit card tab.
     * 
     * @param customer
     */
    public void populateCreditcardBillingAddressData(final Customer customer) {

        if (!isCreditCardPaymentActive()) {
            throw new Error(
                    "You must have credit card payment activated before you can populate creditcard "
                            + "data to its components.");
        }

        if (customer.getAddr1() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_ADDRESS1)).clearField().sendKeys(customer.getAddr1());
        }

        if (customer.getAddr2() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_ADDRESS2)).clearField().sendKeys(customer.getAddr2());
        }

        if (customer.getState() != null) {
            getComponentUtil().selectOptionForNativeSelect(fwd().select(By.id(ID_SELECTOR_CREDIT_CARD_STATE_SELECT)),
                    customer.getState());
        }

        if (customer.getCity() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_SUBURB)).clearField().sendKeys(customer.getCity());
        }

        if (customer.getPostcode() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_POSTCODE)).clearField().sendKeys(customer.getPostcode());
        }
    }


    /**
     * method to populate all personal related data to the paypal sandbox page with activated credit card tab.
     * 
     * @param customer
     */
    public void populateCreditcardPersonalData(final Customer customer) {

        if (!isCreditCardPaymentActive()) {
            throw new Error(
                    "You must have credit card payment activated before you can populate creditcard data"
                            + " to its components.");
        }

        if (customer.getFirstname() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_FIRST_NAME)).clearField().sendKeys(customer.getFirstname());
        }

        if (customer.getSurname() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_LAST_NAME)).clearField().sendKeys(customer.getSurname());
        }

        if (customer.getPhone() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_PHONE_NUMBER)).clearField().sendKeys(customer.getPhone());
        }

        if (customer.getEmail() != null) {
            fwd().input(By.id(ID_SELECTOR_CREDIT_CARD_EMAIL_ADDRESS)).clearField().sendKeys(customer.getEmail());
        }
    }
}

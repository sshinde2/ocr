/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.checkout;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BrowserDriver;



/**
 * A Paypal payment review page, showing the account items or credit card details during a checkout payment process as
 * well as all transaction information before triggering the actual payment transaction as a review page.
 * 
 * the "pay now" button triggers eventually the payment process.
 * 
 * @author maesi
 * 
 */
public class PaypalPaymentReviewPage extends PaypalBasePage {

    // TODO is this a link?
    private final By payNowButton = By.id("continue_abovefold");

    /**
     * @param driver
     */
    public PaypalPaymentReviewPage(final BrowserDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return isDisplayed(fwd().link(payNowButton));
    }

    /**
     * returns whether the pay now button is clickable or not.
     * 
     * @return boolean
     */
    public boolean isPayNowButtonClickable() {
        return isEnabled(fwd().link(payNowButton));
    }

    /**
     * triggers the actual payment over a paypal account, waits until finished and returns a current ThankyouPage. The
     * handling is done over paypal token, getting generated over the server backend and will be attached to the URL.
     * 
     * @return new instance of ThankyouPage
     */
    public ThankyouPage clickPayNowButton() {

        fwd().link(payNowButton).click();
        return new ThankyouPage(getBrowserDriver());
    }

}

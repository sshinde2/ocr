/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * Breadcrumb element
 * 
 */
public class BreadCrumb extends BasePage {

    private final By breadCrumb = By.cssSelector("div.breadcrumb");

    /**
     * @param driver
     */
    public BreadCrumb(final BrowserDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return isDisplayed(fwd().div(breadCrumb));
    }


}

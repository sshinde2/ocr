/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import static org.seleniumhq.selenium.fluent.Period.secs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;


/**
 * @author cwijesu1
 * 
 */
public class Enews extends BasePage {


    protected static final Logger LOG = LogManager.getLogger(Enews.class);
    private final By footerEnews = By.cssSelector("div.enews-quick h3");
    private final By footerEnewsTextBox = By.name("email");
    private final By inlineTermsDisplayed = By.cssSelector("div.signup-controls p.inline-terms");
    private final By signMeUpButton = By.cssSelector("div.signup-controls button");
    private final By errorMsg = By.cssSelector("div.f-element p.f-err-msg");
    private final By successMsg = By.cssSelector("div.f-element p.f-suc-msg");


    /**
     * @param driver
     */
    public Enews(final BrowserDriver driver) {
        super(driver);

    }

    public boolean isDisplayed() {
        final boolean isPresent = fwd().has().h3(footerEnews);
        if (!isPresent) {
            return false;
        }
        return isDisplayed(fwd().h3(footerEnews));
    }

    public boolean activateEnews()
    {

        if (fwd().input(footerEnewsTextBox).isDisplayed().value().booleanValue())
        {
            fwd().input(footerEnewsTextBox).sendKeys("");
            return true;
        }
        else
        {
            return false;
        }

    }

    public void enewsSignUp(final String username)
    {

        fwd().input(footerEnewsTextBox).sendKeys(username);
        fwd().button(signMeUpButton).ifInvisibleWaitUpTo(secs(5)).click();

    }

    public String getThankyouMsg()
    {
        return getText(fwd().p(successMsg).ifInvisibleWaitUpTo(secs(5)));
    }

    public String getErrorMsg()
    {
        return getText(fwd().p(errorMsg).ifInvisibleWaitUpTo(secs(5)));
    }

    public boolean getInlineTermsDisplayed()
    {
        return fwd().p(inlineTermsDisplayed).ifInvisibleWaitUpTo(secs(5)).isDisplayed().value().booleanValue();
    }

    public String getSignupButtonText()
    {
        return getText(fwd().button(signMeUpButton).ifInvisibleWaitUpTo(secs(5)));
    }

}

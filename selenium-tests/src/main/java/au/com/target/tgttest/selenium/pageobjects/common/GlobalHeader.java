/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import static org.fest.assertions.Assertions.assertThat;
import static org.seleniumhq.selenium.fluent.Period.secs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.HomePage;
import au.com.target.tgttest.selenium.pageobjects.LoginPage;
import au.com.target.tgttest.selenium.pageobjects.SearchResultsPage;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;
import au.com.target.tgttest.selenium.pageobjects.myaccount.MyAccountPage;


/**
 * Global Header element
 * 
 */
public class GlobalHeader extends BasePage {

    private final By targetLogo = By.cssSelector("div.header div.site-logo a.logo");

    // Minicart 
    private final By miniCartContent = By.cssSelector("div.cart-content");
    private final By viewBasketLink = By.cssSelector("div.cart-content a.mini-cart-summary");

    // Search
    private final By searchInputField = By.cssSelector("div.mini-search input#search");
    private final By searchButton = By.cssSelector("div.mini-search .button");
    private final By autocompleteEntryHolder = By.cssSelector("ul.ui-autocomplete");

    // Account
    private final By accountSection = By.cssSelector("li.my-account");
    private final By welcomeMsg = By.cssSelector("li.my-account span.welcome");





    /**
     * @param driver
     */
    public GlobalHeader(final BrowserDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return hasTargetLogo();
    }

    public boolean hasTargetLogo() {

        return isDisplayed(fwd().link(targetLogo));
    }

    public HomePage clickTargetLogo() {

        fwd().link(targetLogo).click();
        return new HomePage(getBrowserDriver());
    }

    public MinicartPopup activateMinicartPopup()
    {
        getBrowserDriver().mouseover(miniCartContent);

        return new MinicartPopup(getBrowserDriver());
    }

    public void typeInSearchBox(final String text) {

        fwd().input(searchInputField).sendKeys(text);
    }

    public List<String> getAutocompleteEntries() {

        final List<String> strings = new ArrayList<String>();

        // Wait for the autocomplete to pop up
        final List<FluentWebElement> links = fwd().ul(autocompleteEntryHolder).ifInvisibleWaitUpTo(secs(5))
                .links(By.className("ui-corner-all"));
        for (final FluentWebElement link : links) {
            strings.add(link.getText().toString());
        }

        return strings;
    }

    public SearchResultsPage searchFor(final String searchText) {

        LOG.info("Searching for text: " + searchText);

        fwd().input(searchInputField).clearField().click().sendKeys(searchText);
        fwd().button(searchButton).click();

        return new SearchResultsPage(getBrowserDriver());
    }

    public ShoppingBasketPage clickViewBasketLink() {
        final MinicartPopup minicartPopup = activateMinicartPopup();
        assertThat(minicartPopup.isDisplayed()).isTrue();
        minicartPopup.checkout();
        return new ShoppingBasketPage(getBrowserDriver());
    }

    public TemplatePage clickMyaccountLink() {

        if (!isLoggedIn()) {
            fwd().li(accountSection).link(By.linkText("Sign In")).click();
            return new LoginPage(getBrowserDriver());
        }
        else {
            fwd().li(accountSection).link().click();
            return new MyAccountPage(getBrowserDriver());
        }

    }

    public boolean isLoggedIn()
    {
        if (getText(fwd().li(accountSection).link()).contains("Account")) {
            return true;
        }
        else
        {
            return false;
        }

    }

    public String getWelcomeMsg() {
        return getText(fwd().span(welcomeMsg));
    }




}

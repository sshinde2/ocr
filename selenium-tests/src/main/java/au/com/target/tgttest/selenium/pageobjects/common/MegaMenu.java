/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import static org.seleniumhq.selenium.fluent.Period.secs;

import java.text.MessageFormat;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.data.CategoryType;
import au.com.target.tgttest.selenium.data.DepartmentType;
import au.com.target.tgttest.selenium.pageobjects.CategoryPage;
import au.com.target.tgttest.selenium.pageobjects.DepartmentPage;


/**
 * Megamenu element
 * 
 */
public class MegaMenu extends BasePage {

    private final By megaMenu = By.cssSelector("div#megamenu");

    //    @FindBy(css = "div#megamenu li.has-flyout")
    //    private List<WebElement> departmentFlyouts;

    /**
     * The timeout for web driver waits (in seconds).
     */
    public static final int WAIT_TIMEOUT_SECONDS = 3;

    /**
     * @param driver
     */
    public MegaMenu(final BrowserDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return isDisplayed(fwd().div(megaMenu));
    }

    /*
     * Selectors for mega menu items work like so:
     * 
     * In the div with id "megamenu",
     *   In the 'li' with class 'has-flyout' (which has a direct descendant 'a' whose text matches the first parameter),
     *     Find the descendant 'a' whose text matches the second parameter.
     */

    /**
     * Template for selecting items in the MegaMenu by their department and link href using CSS.
     */
    public static final String CSS_ITEM_SELECTOR_TEMPLATE = "div#megamenu" //look in mega menu
            + " li.has-flyout a[href*=''{0}'']" // correct department menu
            + "+div a[title=''{1}'']"; //item link href


    private FluentWebElement getDepartmentLinkElement(final DepartmentType dept) {
        return fwd().div(megaMenu).link(By.linkText(dept.getName()));
    }

    public DepartmentPage clickOnDepartmentLink(final DepartmentType dept) {
        getDepartmentLinkElement(dept).click();
        return new DepartmentPage(getBrowserDriver(), dept);
    }


    public CategoryPage clickDepartmentSubLink(final DepartmentType dept, final CategoryType cat)
    {
        activateDepartmentPopup(dept);

        LOG.info("Clicking on megamenu department sublink: " + dept + "->" + cat.getName());

        final String selector = MessageFormat.format(CSS_ITEM_SELECTOR_TEMPLATE, dept.getCode(), cat.getName());
        fwd().link(By.cssSelector(selector)).ifInvisibleWaitUpTo(secs(5)).click();

        return new CategoryPage(getBrowserDriver(), cat);
    }

    public void activateDepartmentPopup(final DepartmentType dept)
    {
        LOG.info("Activating megamenu department popup: " + dept);
        deactivateDepartmentPopups();
        getBrowserDriver().mouseover(getDepartmentLinkElement(dept));
    }

    public void deactivateDepartmentPopups()
    {
        LOG.info("Deactivating any open megamenu popups");

        getBrowserDriver().mouseover(By.cssSelector("a.logo"));
    }




}

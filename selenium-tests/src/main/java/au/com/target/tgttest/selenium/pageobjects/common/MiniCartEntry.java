/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;


/**
 * CartEntry element
 * 
 */
public class MiniCartEntry
{
    private static final Pattern ITEM_CODE_PATTERN = Pattern.compile("^ITEM CODE: (.*)$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);
    private static final Pattern QTY_PATTERN = Pattern.compile("^QTY: (\\d*)$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);
    private static final Pattern SIZE_PATTERN = Pattern.compile("^SIZE: (.*)$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);
    private static final Pattern COLOUR_PATTERN = Pattern.compile("^COLOUR: (.*)$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);
    private static final Pattern PRICE_PATTERN = Pattern.compile("^\\$(\\d+\\.\\d{2})$", Pattern.CASE_INSENSITIVE
            | Pattern.MULTILINE);
    private final String imageSrc;
    private final String imageAltText;
    private final BigDecimal price;
    private final String name;
    private final String itemCode;
    private final int quantity;
    private final String size;
    private final String colour;

    public MiniCartEntry(final FluentWebElement minicartEntry)
    {
        //We can determine if the individual pieces of data are present faster with regex than with Selenium.
        final String text = minicartEntry.getText().toString().trim();

        imageSrc = minicartEntry.img(By.cssSelector("div.entry-image img")).getAttribute("src").toString();
        imageAltText = minicartEntry.img(By.cssSelector("div.entry-image img")).getAttribute("alt").toString();
        name = minicartEntry.link(By.cssSelector("div.entry-info p[class*='name'] a")).getText().toString();
        itemCode = extract(ITEM_CODE_PATTERN,
                minicartEntry.p(By.cssSelector("div.entry-info p[class*='spec-code']")).getText().toString());
        final String priceString = extract(PRICE_PATTERN,
                minicartEntry.div(By.cssSelector("div[class*='entry-price']")).getText().toString());

        if (priceString == null)
        {
            throw new IllegalStateException(MessageFormat.format(
                    "Per-item price could not be determined for item {0}", this.name));
        }
        else
        {
            this.price = new BigDecimal(priceString);
        }

        quantity = Integer.parseInt(extract(QTY_PATTERN, minicartEntry
                .p(By.cssSelector("div.entry-info p[class*='spec-quantity']"))
                .getText().toString()));
        size = extract(SIZE_PATTERN, text);
        colour = extract(COLOUR_PATTERN, text);
    }

    private static String extract(final Pattern pattern, final String str)
    {
        String results = null;
        final Matcher matcher = pattern.matcher(str);
        if (matcher.find())
        {
            results = matcher.group(1);
        }
        return results;
    }

    /**
     * @return the imageSrc
     */
    public String getImageSrc() {
        return imageSrc;
    }

    /**
     * @return the imageAltText
     */
    public String getImageAltText() {
        return imageAltText;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the itemCode
     */
    public String getItemCode() {
        return itemCode;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @return the colour
     */
    public String getColour() {
        return colour;
    }
}

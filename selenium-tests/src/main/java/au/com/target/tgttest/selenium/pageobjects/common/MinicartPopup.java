/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import static org.seleniumhq.selenium.fluent.Period.secs;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;
import org.seleniumhq.selenium.fluent.FluentWebElements;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;


/**
 * Minicart element
 * 
 */
public class MinicartPopup extends BasePage {

    private final By element = By.id("mini-cart-popup");

    private final By checkoutButton = By.cssSelector("div.cart-links a.checkout");
    private final By continueLink = By.cssSelector("div.cart-links a.continue");
    private final By heading = By.cssSelector("div#mini-cart-popup h4");
    private final By closeButton = By.cssSelector("div#mini-cart-popup a.close");
    private final By cartEmptyDiv = By.cssSelector("div#mini-cart-popup div[class*='cart-empty']");
    private final By cartEntries = By.cssSelector("div#mini-cart-popup div.cart-entry");
    private final By price = By.cssSelector("div#mini-cart-popup p.subtotal span.price");
    private final By viewBasketLink = By.cssSelector("div#mini-cart-popup a[href='/basket']");
    private final By quantity = By.cssSelector("div#mini-cart-popup span.qty span.num");

    // private final By legend = By.cssSelector("div#mini-cart-popup p.legend");


    /**
     * @param driver
     */
    public MinicartPopup(final BrowserDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return fwd().div(element).ifInvisibleWaitUpTo(secs(10)).isDisplayed().value().booleanValue();
    }

    public String getCartEmptyText() {
        return getText(fwd().div(cartEmptyDiv));
    }

    public String getPriceText() {
        return getText(fwd().span(price));
    }

    public String getPopupHeading() {
        return getText(fwd().h4(heading));
    }

    public int getQuantity() {
        int qty = 0;
        final String sQty = getText(fwd().span(quantity));
        if (sQty != null) {
            try {
                qty = Integer.parseInt(sQty);
            }
            catch (final Exception e) {
                throw new RuntimeException("Could not parse minicart quantity: " + sQty);
            }
        }

        return qty;
    }

    public boolean isCheckoutButtonDisplayed()
    {
        return isDisplayed(fwd().link(checkoutButton));
    }

    public boolean isContinueShoppingLinkDisplayed()
    {
        return isDisplayed(fwd().link(continueLink));
    }

    public List<MiniCartEntry> getCartEntries()
    {
        final List<MiniCartEntry> list = new ArrayList<MiniCartEntry>();

        final FluentWebElements elts = fwd().divs(cartEntries);
        if (elts != null)
        {
            for (final FluentWebElement elt : elts)
            {
                final MiniCartEntry entry = new MiniCartEntry(elt);
                list.add(entry);
            }
        }
        return list;
    }

    public void close() {

        fwd().link(closeButton).click();

        getBrowserDriver().waitForAnimations();
    }

    public void continueShopping() {

        fwd().link(continueLink).click();

        getBrowserDriver().waitForAnimations();
    }

    public ShoppingBasketPage checkout() {

        fwd().link(checkoutButton).click();
        return new ShoppingBasketPage(getBrowserDriver());

    }

    public ShoppingBasketPage viewBasket() {

        fwd().link(viewBasketLink).click();
        return new ShoppingBasketPage(getBrowserDriver());
    }

}

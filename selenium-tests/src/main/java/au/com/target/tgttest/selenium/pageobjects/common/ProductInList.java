/**
 * 
 */
package au.com.target.tgttest.selenium.pageobjects.common;

import java.math.BigDecimal;

import org.openqa.selenium.By;
import org.seleniumhq.selenium.fluent.FluentWebElement;

import au.com.target.tgttest.selenium.base.BasePage;
import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;
import au.com.target.tgttest.selenium.util.PriceUtil;


/**
 * Product in list element
 * 
 */
public class ProductInList extends BasePage {

    private final By productImageLink = By.cssSelector("a");
    private final By productNameLink = By.cssSelector(".detail .summary a");
    private final By productSummary = By.cssSelector(".detail .summary p");
    private final By productBasketLink = By.cssSelector(".detail a");
    private final By productPrice = By.cssSelector(".detail .price-info span.price");
    private final By productWasPrice = By.cssSelector(".detail .price-info span.price");
    private final By promotionalIcon = By.cssSelector("span[class~=promo-status]");

    private final FluentWebElement productElement;

    public ProductInList(final BrowserDriver driver, final FluentWebElement productElement) {
        super(driver);
        this.productElement = productElement;
    }

    public boolean isDisplayed() {
        return isDisplayed(fwd().link(productImageLink));
    }

    public String getProductPriceText()
    {
        return productElement.span(productPrice).getText().toString();
    }

    public BigDecimal getProductPriceValue()
    {
        return PriceUtil.getPriceValue(getProductPriceText());
    }

    public String getProductWasPriceText()
    {
        return productElement.span(productWasPrice).getText().toString();
    }

    public BigDecimal getProductWasPriceValue()
    {
        return PriceUtil.getWasPriceValue(getProductWasPriceText());
    }

    public boolean isOutOfStock() {
        return getProductPriceText().contains("Sold Out");
    }

    public String getProductName()
    {
        return productElement.link(productNameLink).getText().toString();
    }

    public ProductPage clickProductImageLink() {

        LOG.info("Clicking product image link: " + getProductName());
        productElement.link(productImageLink).click();
        return new ProductPage(getBrowserDriver());
    }


}

package au.com.target.tgttest.selenium.process;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.target.tgttest.selenium.base.BrowserDriver;
import au.com.target.tgttest.selenium.base.SeleniumSession;
import au.com.target.tgttest.selenium.base.config.TestConfiguration;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;
import au.com.target.tgttest.selenium.pageobjects.TemplatePage;


/**
 * Base class for process test helpers which are assembled in the class BusinessTaskHelper, which acts as a parent
 * handler and hosts access methods for certain test specific variables such as test configuration and the current
 * selenium web driver.
 * 
 * @author maesi
 * 
 */
public class BaseProcess {

    protected static final Logger LOG = LogManager.getLogger(BaseProcess.class);

    private final BrowserDriver driver;

    /**
     * constructor.
     */
    public BaseProcess(final BrowserDriver driver) {
        this.driver = driver;
    }

    protected BrowserDriver getBrowserDriver() {
        return driver;
    }

    /**
     * returns the Selenium test configuration settings.
     * 
     * @return TestConfiguration
     */
    protected TestConfiguration getConfiguration() {
        return TestConfiguration.getInstance();
    }


    /**
     * Navigate to given relative URL
     * 
     * @param rel
     */
    protected void navigate(final String rel)
    {
        final String url = getConfiguration().constructUrl(rel);
        getBrowserDriver().navigate(url);
    }


    /**
     * Navigate to the url for the given template page
     * 
     * @param page
     */
    protected void navigate(final TemplatePage page)
    {
        navigate(page.getPageRelativeURL());
    }

    /**
     * Navigate to the url for the given type of template page, and return an instance of the page object
     * 
     * @param pageClass
     * @return page object
     */
    public <T extends TemplatePage> T navigate(final Class<T> pageClass) {

        T page;
        try {
            page = pageClass.getConstructor(BrowserDriver.class).newInstance(getBrowserDriver());
        }
        catch (final Exception e) {
            throw new RuntimeException("BaseSeleniumTest.navigate - could not create page object", e);
        }

        navigate(page);

        return page;
    }


    /**
     * @param product
     * @return product page
     */
    protected ProductPage navigateToProductPage(final Product product) {
        final ProductPage productPage = SeleniumSession.getInstance().navigate(product.getUrl(), ProductPage.class);

        return productPage;
    }


    /**
     * Navigate to the given url and return a page object as indicated
     * 
     * @param rel
     * @param pageClass
     * @return page object instance
     */
    public <T> T navigate(final String rel, final Class<T> pageClass) {
        return SeleniumSession.getInstance().navigate(rel, pageClass);
    }

}
package au.com.target.tgttest.selenium.process;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Collections;
import java.util.List;

import au.com.target.tgttest.selenium.base.SeleniumSession;
import au.com.target.tgttest.selenium.datastore.bean.CreditCard;
import au.com.target.tgttest.selenium.datastore.bean.Customer;
import au.com.target.tgttest.selenium.datastore.bean.PaypalCreditCard;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.DeliveryMethodPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.DeliveryMethodPageHomeDeliveryAddress;
import au.com.target.tgttest.selenium.pageobjects.checkout.OrderReviewPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.PaymentDetailsPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.PaypalPaymentPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.ThankyouPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.UserLoginCheckoutPage;


/**
 * a class with a collection of helper methods to handle shopping actions
 * 
 * @author maesi
 * 
 */
public final class ShoppingProcess extends BaseProcess {

    private static final String TEST_GUEST_EMAIL_ADDRESS = "test@tester.com";

    private static ShoppingProcess theShoppingProcess;

    /**
     * constructor
     * 
     */
    private ShoppingProcess() {
        super(SeleniumSession.getInstance().getBrowserDriver());
    }

    public static ShoppingProcess getInstance() {

        if (theShoppingProcess == null) {
            theShoppingProcess = new ShoppingProcess();
        }

        return theShoppingProcess;
    }

    /**
     * Add a single product to basket
     * 
     * @param product
     */
    public void addProductToBasket(final Product product) {

        addProductsToBasket(Collections.singletonList(product));
    }



    /**
     * this method orders a few products with 'buy now' over their relative Product URL Path and returns the final
     * ShoppingBasketPage instance.
     * 
     * @param products
     * @return returns an instance of ShoppingBasketPage.
     */
    public ShoppingBasketPage addProductsIntoShoppingBasket(final List<Product> products) {
        final ProductPage page = addProductsToBasket(products);

        final ShoppingBasketPage basketPage = page.getHeader().clickViewBasketLink();

        // Check basket page has expected number of products
        assertThat(basketPage.isCurrentPage()).as("expect basket page").isTrue();
        assertThat(basketPage.getItemCount()).as("basket count").isEqualTo(products.size());

        return basketPage;
    }


    /**
     * Adds products to the shopping basket.
     * 
     * @param products
     * @return the product page for the last item added to the cart
     */
    public ProductPage addProductsToBasket(final List<Product> products) {
        ProductPage productPage = null;
        for (final Product product : products) {
            productPage = navigateToProductPage(product);

            final String error = "There is no buy button available for the product '"
                    + product.getCode()
                    + "'. Maybe this product is not defined for 'buy now', "
                    + "not existent or it is out of stock?";

            assertThat(productPage.hasBuyNowButton()).as(error).isTrue();

            productPage.clickBuyNowButton();
        }
        return productPage;
    }


    /**
     * this method clears all items from a shopping basket.
     * 
     * @return returns an instance of the current ShoppingBasketPage.
     */
    public ShoppingBasketPage removeAllProductsFromShoppingBasket() {

        final ShoppingBasketPage basketPage = navigate(ShoppingBasketPage.class);

        while (basketPage.getItemCount() > 0) {
            basketPage.getItems().get(0).clickRemoveButton();
        }

        return basketPage;
    }


    /**
     * Given ShoppingBasketPage, checkout login as a registered customer
     * 
     * @param basketPage
     *            the ShoppingBasketPage instance.
     * @param userEmail
     *            String the existing user email.
     * @param password
     *            String the user password.
     * @return DeliveryMethodPage
     */
    public DeliveryMethodPage checkoutLoginAsRegisteredCustomer(
            final ShoppingBasketPage basketPage, final String userEmail, final String password) {

        LOG.info("Logging in customer: " + userEmail);
        basketPage.clickContinueProductsCheckout();

        final UserLoginCheckoutPage checkoutPage = new UserLoginCheckoutPage(getBrowserDriver());
        assertThat(checkoutPage.isCurrentPage()).as("Expect checkout page").isTrue();

        final DeliveryMethodPage deliveryMethodPage = checkoutPage.proceedAsSignedInUser(userEmail, password);
        assertThat(deliveryMethodPage.isCurrentPage()).as("expect delivery method page").isTrue();

        return deliveryMethodPage;
    }


    /**
     * Checkout login the registered customer from the basket page
     * 
     * @param basketPage
     * @param customer
     * @return DeliveryMethodPage
     */
    public DeliveryMethodPage checkoutLoginAsRegisteredCustomer(
            final ShoppingBasketPage basketPage, final Customer customer) {

        return checkoutLoginAsRegisteredCustomer(basketPage, customer.getEmail(), customer.getPassword());
    }


    /**
     * Given ShoppingBasketPage, checkout assuming already logged in
     * 
     * @param basketPage
     * @return DeliveryMethodPage
     */
    public DeliveryMethodPage checkoutAlreadyLoggedIn(final ShoppingBasketPage basketPage) {

        LOG.info("Continuing as signed-in customer");
        basketPage.clickContinueProductsCheckout();
        return new DeliveryMethodPage(getBrowserDriver());
    }


    /**
     * Checkout with guest user
     * 
     * @param basketPage
     * @return DeliveryMethodPage
     */
    public DeliveryMethodPage checkoutGuestUser(final ShoppingBasketPage basketPage) {

        basketPage.clickContinueProductsCheckout();
        final UserLoginCheckoutPage userLoginCheckoutPage = new UserLoginCheckoutPage(getBrowserDriver());
        assertThat(userLoginCheckoutPage.isCurrentPage()).as("expect checkout login page").isTrue();

        final DeliveryMethodPage deliveryMethodPage = userLoginCheckoutPage.proceedAsGuest(TEST_GUEST_EMAIL_ADDRESS);
        assertThat(deliveryMethodPage.isCurrentPage()).as("expect delivery method page").isTrue();

        return deliveryMethodPage;
    }


    /**
     * Given checkout delivery page, select home delivery with existing address
     * 
     * @param deliveryMethodPage
     * @return PaymentDetailsPage
     */
    public PaymentDetailsPage checkoutHomeDeliveryUseFirstAddress(final DeliveryMethodPage deliveryMethodPage) {

        return checkoutHomeDelivery(deliveryMethodPage, null);
    }


    /**
     * Given checkout delivery page, select home delivery with supplied customer address
     * 
     * @param deliveryMethodPage
     * @param customer
     * @return PaymentDetailsPage
     */
    public PaymentDetailsPage checkoutHomeDeliverySupplyAddress(final DeliveryMethodPage deliveryMethodPage,
            final Customer customer) {

        return checkoutHomeDelivery(deliveryMethodPage, customer);
    }


    private PaymentDetailsPage checkoutHomeDelivery(final DeliveryMethodPage deliveryMethodPage, final Customer customer) {

        if (!deliveryMethodPage.isHomeDeliverySelected()) {
            deliveryMethodPage.selectHomeDelivery();
        }

        PaymentDetailsPage paymentDetailPage;

        if (customer == null) {

            // Use existing address
            assertThat(deliveryMethodPage.isHomeDeliveryWrapperDisplayingSomeExistingAddresses())
                    .as("Existing address present").isTrue();

            paymentDetailPage = (PaymentDetailsPage)deliveryMethodPage
                    .clickHomeDeliveryUseThisAddressLink();
        }
        else {

            // Supply address
            final DeliveryMethodPageHomeDeliveryAddress addressPage = (DeliveryMethodPageHomeDeliveryAddress)
                    deliveryMethodPage.clickContinueButton();
            addressPage.populateHomeDeliveryDetails(customer);
            paymentDetailPage = (PaymentDetailsPage)addressPage.clickSaveAddressButtonAndConfirmAddressDialog();
        }

        return paymentDetailPage;
    }


    /**
     * Use credit card payment.
     * 
     * @param paymentDetailPage
     * @param customer
     * @param card
     */
    public ThankyouPage checkoutPayWithCreditCard(final PaymentDetailsPage paymentDetailPage,
            final Customer customer, final CreditCard card) {

        assertThat(paymentDetailPage.isCurrentPage()).as("expect payment detail page").isTrue();

        paymentDetailPage.activateCreditCardPayment();
        paymentDetailPage.populateCreditCardDetails(card);

        final OrderReviewPage orderReviewPage = (OrderReviewPage)paymentDetailPage.clickContinueButton();
        assertThat(orderReviewPage.isCurrentPage()).as("expect order review page").isTrue();

        final ThankyouPage thanksPage = orderReviewPage.clickPaySecurelyNowButton();
        assertThat(thanksPage.isCurrentPage()).as("expect thanks page").isTrue();

        return thanksPage;
    }


    /**
     * Checkout with click and collect
     * 
     * @param deliveryMethodPage
     * @param customer
     * @return PaymentDetailsPage
     */
    public PaymentDetailsPage checkoutClickAndCollect(final DeliveryMethodPage deliveryMethodPage,
            final Customer customer) {

        deliveryMethodPage.selectClickAndCollect();

        final boolean selected = deliveryMethodPage.selectClickAndCollectStateByText(customer.getState());
        assertThat(selected).as("expect to see state " + customer.getState()).isTrue();

        assertThat(deliveryMethodPage.hasStores()).as("expect some stores").isTrue();
        assertThat(deliveryMethodPage.selectFirstPossibleCollectStore()).as("select first store").isTrue();

        deliveryMethodPage.populateAndCheckPickUpDetails(customer.getTitle(),
                customer.getFirstname(), customer.getSurname(), customer.getPhone());

        assertThat(deliveryMethodPage.isContinueButtonClickable()).as("continue button clickable").isTrue();

        final PaymentDetailsPage paymentDetailPage = (PaymentDetailsPage)deliveryMethodPage.clickContinueButton();
        assertThat(paymentDetailPage.isCurrentPage()).as("expect payment detail page").isTrue();

        return paymentDetailPage;
    }


    /**
     * Paypal guest payment checkout process
     * 
     * @param paymentDetailPage
     * @param paypalCard
     * @param customer
     * @return ThankyouPage
     */
    public ThankyouPage checkoutPayWithGuestPaypal(final PaymentDetailsPage paymentDetailPage,
            final PaypalCreditCard paypalCard, final Customer customer) {

        paymentDetailPage.activatePayPalPayment();

        final PaypalPaymentPage paypalSandboxPage = (PaypalPaymentPage)paymentDetailPage.clickContinueButton();
        assertThat(paypalSandboxPage.isDisplayed()).as("expect sandbox page").isTrue();

        paypalSandboxPage.populateCreditCardData(paypalCard);

        paypalSandboxPage.populateCreditcardPersonalData(customer);

        final ThankyouPage thankyouPage = paypalSandboxPage.clickPayWithCreditCard();
        assertThat(thankyouPage.isCurrentPage()).as("expect thankyou page").isTrue();
        return thankyouPage;
    }

}
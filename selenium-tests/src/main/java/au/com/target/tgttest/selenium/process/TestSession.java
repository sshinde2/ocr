/**
 * 
 */
package au.com.target.tgttest.selenium.process;

import java.math.BigDecimal;
import java.util.List;

import au.com.target.tgttest.hybris.admin.ImpexImporter;
import au.com.target.tgttest.hybris.admin.StockLevelAdjuster;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.util.PriceUtil;


/**
 * Represent state for the cucumber test scenario.<br/>
 * Allows things to be used across different steps classes.
 * 
 */
public final class TestSession {

    private static TestSession theSession;

    // List of products, eg to add to cart
    private List<Product> productList;

    // Product being dealt with, eg added to cart or viewed
    private Product theProduct;

    public static TestSession getInstance() {

        if (theSession == null) {
            theSession = new TestSession();
        }

        return theSession;
    }

    public String getProductPriceFormatted() {

        return PriceUtil.getDisplayPrice(getProduct().getPrice());
    }

    public String getTotalProductPriceFormatted() {

        return PriceUtil.getDisplayPrice(getTotalProductPrice());
    }

    private BigDecimal getTotalProductPrice() {

        BigDecimal total = BigDecimal.ZERO;
        for (final Product product : productList) {
            total = total.add(product.getPrice());
        }
        return total;
    }

    /**
     * reset stock for all the loaded products
     */
    public void resetStockForProducts() {

        for (final Product product : getProductList()) {
            getStockLevelAdjuster().resetStock(product.getCode());
        }
    }

    public void resetStockForSingleProduct() {

        getStockLevelAdjuster().resetStock(theProduct.getCode());
    }

    public void zeroStockForSingleProduct() {

        getStockLevelAdjuster().zeroStock(theProduct.getCode());
    }


    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(final List<Product> productList) {
        this.productList = productList;
    }

    public Product getProduct() {
        return theProduct;
    }

    public void setProduct(final Product product) {
        this.theProduct = product;
    }

    private StockLevelAdjuster getStockLevelAdjuster() {
        return new StockLevelAdjuster(ImpexImporter.getInstance());
    }

}

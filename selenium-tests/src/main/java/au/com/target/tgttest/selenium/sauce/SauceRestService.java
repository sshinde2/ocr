package au.com.target.tgttest.selenium.sauce;


// CHECKSTYLE:OFF 
// Since this class is adapted from Sauce sample code, which uses the BASE64Encoder
// (Checkstyle indicates this is an' illegal' package).

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONValue;


// CHECKSTYLE:ON

/**
 * Adapted from Sauce sample code, see: https://github.com/saucelabs/saucerest-java https://saucelabs.com/docs/rest
 * 
 */
public class SauceRestService
{
    private static final Logger LOG = LogManager.getLogger(SauceRestService.class);

    private static final String RESTURL = "http://saucelabs.com/rest/v1/%1$s";
    private static final String JOB_RESULT_FORMAT = RESTURL + "/jobs/%2$s";

    private final String username;
    private final String accessKey;


    public SauceRestService(final String username, final String accessKey)
    {
        this.username = username;
        this.accessKey = accessKey;
    }


    @SuppressWarnings("boxing")
    public void jobPassed(final String name, final String jobId)
    {
        final Map<String, Object> updates = new HashMap<String, Object>();
        updates.put("name", name);
        updates.put("passed", true);
        updateJobInfo(jobId, updates);
    }

    @SuppressWarnings("boxing")
    public void jobFailed(final String name, final String jobId) {
        final Map<String, Object> updates = new HashMap<String, Object>();
        updates.put("name", name);
        updates.put("passed", false);
        updateJobInfo(jobId, updates);
    }


    public void updateJobInfo(final String jobId, final Map<String, Object> updates)
    {
        HttpURLConnection postBack = null;

        try {
            final URL restEndpoint = new URL(String.format(JOB_RESULT_FORMAT, username, jobId));
            postBack = (HttpURLConnection)restEndpoint.openConnection();
            postBack.setDoOutput(true);
            postBack.setRequestMethod("PUT");
            final String auth = encodeAuthentication();
            postBack.setRequestProperty("Authorization", auth);
            final String jsonText = JSONValue.toJSONString(updates);
            postBack.getOutputStream().write(jsonText.getBytes());
        }
        catch (final IOException e) {
            LOG.warn("Error updating Sauce Results", e);
        }

        try {
            if (postBack != null) {
                postBack.getInputStream().close();
            }
        }
        catch (final IOException e) {
            LOG.warn("Error updating Sauce Results, closing stream", e);
        }

    }

    private String encodeAuthentication() {
        String auth = username + ":" + accessKey;

        auth = Base64.encodeBase64(auth.getBytes()).toString();
        auth = "Basic " + auth;
        return auth;
    }
}

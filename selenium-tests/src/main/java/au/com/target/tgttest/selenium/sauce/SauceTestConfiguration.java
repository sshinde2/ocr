package au.com.target.tgttest.selenium.sauce;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import au.com.target.tgttest.selenium.base.BrowserType;
import au.com.target.tgttest.selenium.base.config.Settings;
import au.com.target.tgttest.selenium.base.config.TestConfiguration;


public class SauceTestConfiguration extends TestConfiguration
{
    private static final Logger LOG = LogManager.getLogger(SauceTestConfiguration.class);

    private final String username;
    private final String accessKey;
    private final String sauceTemplateUrl;

    private final SauceRestService sauceRest;

    public SauceTestConfiguration(final Settings settings)
    {
        super(settings);

        this.username = getSauceUsername();
        this.accessKey = getSauceKey();
        this.sauceTemplateUrl = getSauceTemplateUrl();

        sauceRest = new SauceRestService(username, accessKey);
    }

    @Override
    public WebDriver getDriver(final BrowserType browser, final String name)
    {
        final DesiredCapabilities cap = browser.getRemoteCapabilities();

        // Override the default name for sauce if supplied
        if (name != null)
        {
            cap.setCapability("name", name);
        }

        populateSauceSettings(cap);

        final WebDriver driver = new RemoteWebDriver(getSauceUrl(), cap);

        return driver;
    }

    public URL getSauceUrl()
    {
        URL url;

        if (StringUtils.isEmpty(sauceTemplateUrl)) {
            throw new RuntimeException("No Sauce Labs url found from configuration. Unable to proceed.");
        }

        try
        {
            // Plug the username and access key into the template url
            final String fullUrl = sauceTemplateUrl.replaceAll("\\$\\{username\\}", username).replaceAll(
                    "\\$\\{key\\}", accessKey);
            LOG.info("Sauce url: " + fullUrl);

            url = new URL(fullUrl);
        }
        catch (final MalformedURLException e) {
            throw new RuntimeException("Malformed Hub URL", e);
        }

        return url;
    }



    @Override
    public void notifySessionResult(final String jobId, final String name, final boolean result)
    {
        super.notifySessionResult(jobId, name, result);

        if (result)
        {
            sauceRest.jobPassed(name, jobId);
        }
        else
        {
            sauceRest.jobFailed(name, jobId);
        }
    }


    private String getSauceUsername()
    {
        // Passed through as env variable (from Jenkins job)
        String user = System.getenv("SAUCE_USER_NAME");
        if (StringUtils.isEmpty(user))
        {
            user = settings.getSauceUsername();
        }

        LOG.info("Using sauce username=" + user);

        return user;
    }


    private String getSauceKey()
    {
        // Passed through as env variable (from Jenkins job)
        String key = System.getenv("SAUCE_API_KEY");
        if (StringUtils.isEmpty(key))
        {
            key = settings.getSauceAccessKey();
        }

        LOG.info("Using sauce key=" + key);

        return key;
    }

    private String getSauceTemplateUrl()
    {
        // Straight from setting
        final String url = settings.getSauceUrl();

        return url;
    }


    private void populateSauceSettings(final DesiredCapabilities cap)
    {
        cap.setCapability("record-video", getRecordVideo());
        cap.setCapability("build", getBuildNumber());
        cap.setCapability("tags", getTags());
        final String seleniumVersion = getSeleniumVersion();
        if (!StringUtils.isEmpty(seleniumVersion))
        {
            cap.setCapability("selenium-version", seleniumVersion);
        }
        final String tunnelIdentifier = getSauceTunnelIdentifier();
        if (!StringUtils.isEmpty(tunnelIdentifier)) {
            LOG.info("Setting SauceConnect tunnel identifier " + tunnelIdentifier);
            cap.setCapability("tunnel-identifier", tunnelIdentifier);
        }
    }

    private String getSauceTunnelIdentifier() {
        String results = System.getenv("tgttest.selenium.sauce-tunnelId");
        if (StringUtils.isEmpty(results)) {
            results = System.getProperty("tgttest.selenium.sauce-tunnelId", settings.getSauceTunnelIdentifier());
        }
        return results;
    }

    private boolean getRecordVideo()
    {
        boolean bUseVideo = true;

        final String video = System.getenv("tgttest.selenium.recordvideo");
        if (StringUtils.isEmpty(video))
        {
            bUseVideo = settings.getRecordVideo();
        }
        else if (video.equals("false"))
        {
            bUseVideo = false;
        }

        LOG.info("Using record video=" + bUseVideo);

        return bUseVideo;
    }


    private String getBuildNumber()
    {
        // Try environment variables from Jenkins, else set blank
        String build = System.getenv("tgttest.selenium.build");
        if (StringUtils.isEmpty(build))
        {
            build = System.getenv("release");
        }
        if (StringUtils.isEmpty(build))
        {
            build = "";
        }

        LOG.info("Using build=" + build);

        return build;
    }

    private String getTags()
    {
        String tags = "";
        final String testset = System.getenv("tgttest.seleniumtest.testset");
        if (!StringUtils.isEmpty(testset))
        {
            tags = testset;
        }

        LOG.info("Using tags=" + tags);

        return tags;
    }

    /**
     * Gets the setting for running SauceLabs tests against a specific version of Selenium (
     * {@code tgttest.selenium.version}).
     * 
     * @see <a href="https://saucelabs.com/docs/additional-config#selenium-version">Use A Specific Selenium Version</a>
     * @return the Selenium version to use at SauceLabs, or an empty/null result if the default version should be used.
     */
    private String getSeleniumVersion()
    {
        final String results = System.getProperty("tgttest.selenium.version", settings.getSeleniumVersion());
        if (StringUtils.isEmpty(results))
        {
            LOG.info("Using default Selenium version");
        }
        else if (LOG.isInfoEnabled())
        {
            LOG.info(MessageFormat.format("Using Selenium version {0}", results));
        }
        return results;
    }




}

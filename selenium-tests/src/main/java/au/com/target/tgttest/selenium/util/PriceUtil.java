/**
 * 
 */
package au.com.target.tgttest.selenium.util;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;


/**
 * Utils for price formats
 * 
 */
public final class PriceUtil {

    private static final String FREE_VALUE = "FREE";

    private PriceUtil() {
        // util
    }

    public static String getDisplayPrice(final BigDecimal price) {
        return "$" + price.setScale(2).toString();
    }

    public static BigDecimal getPriceValue(final String price) {
        return new BigDecimal(price.replace("$", "")).setScale(2);
    }

    public static BigDecimal getWasPriceValue(final String wasprice) {
        return new BigDecimal(wasprice.replace("Was $", "")).setScale(2);
    }

    /**
     * method to unstripe the $ sign from a currency signed value.
     * 
     * @param signedValue
     *            the currency value/amount.
     * @return the String as a raw value amount. if the value parameter is invalid or empty, an Error Exception will be
     *         thrown!
     */
    public static String getUnsignedValueFromCurrencyValue(final String signedValue) {

        if (!StringUtils.isEmpty(signedValue)) {
            if (signedValue.equals(FREE_VALUE)) {
                return "0";
            }
            if (signedValue.contains("$")) {
                return StringUtils.remove(signedValue, "$").trim();
            }
            else {
                return signedValue.trim();
            }
        }
        throw new Error("Undefined or empty currency value to reformat: '" + signedValue + "'.");
    }



}

/**
 * 
 */
package au.com.target.tgttest.selenium.verifier;

import static org.fest.assertions.Assertions.assertThat;
import au.com.target.tgttest.selenium.data.DepartmentType;
import au.com.target.tgttest.selenium.pageobjects.DepartmentPage;


/**
 * Verify department page
 * 
 */
public final class DepartmentPageVerifier {

    private DepartmentPageVerifier() {
        // util
    }

    public static void verifyDeptLanding(final DepartmentPage landing) {

        assertThat(landing.getFacetCategoryTitle()).as("facet category title").isEqualTo(
                landing.getDepartmentType().getName());

        // Baby landing has content
        if (landing.getDepartmentType().equals(DepartmentType.BABY)) {
            assertThat(landing.hasMainBanner()).as("expect landing page main banner").isTrue();
        }

    }

}

/**
 * 
 */
package au.com.target.tgttest.selenium.verifier;

import static org.fest.assertions.Assertions.assertThat;

import java.text.MessageFormat;
import java.util.List;

import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.common.MiniCartEntry;


/**
 * Util for minicart verification
 * 
 */
public final class MinicartVerifier {

    private MinicartVerifier() {
        // util
    }


    /**
     * Verify the given product against the minicart entry
     * 
     * @param entry
     * @param product
     * @param expectedQuantity
     */
    public static void verifyCartEntry(final MiniCartEntry entry, final Product product,
            final int expectedQuantity)
    {
        final String template = MessageFormat.format("minicart product {0} - wrong ", product.getName());

        assertThat(entry.getName()).as(template + "name").isEqualTo(product.getName());
        assertThat(entry.getItemCode()).as(template + "code").isEqualTo(product.getCode());
        assertThat(entry.getPrice()).as(template + "price").isEqualTo(product.getPrice());
        assertThat(entry.getColour()).as(template + "colour").isEqualTo(product.getColour());
        assertThat(entry.getSize()).as(template + "size").isEqualTo(product.getSize());
        assertThat(entry.getQuantity()).as(template + "quantity").isEqualTo(expectedQuantity);

    }

    /**
     * Find the MiniCartEntry for the product code
     * 
     * @param code
     * @param cartEntries
     * @return MiniCartEntry
     */
    public static MiniCartEntry findCartEntry(final String code, final List<MiniCartEntry> cartEntries) {

        MiniCartEntry found = null;
        for (final MiniCartEntry entry : cartEntries) {
            if (entry.getItemCode() != null && entry.getItemCode().equals(code)) {
                found = entry;
                break;
            }
        }

        assertThat(found).overridingErrorMessage("Could not find cart entry: " + code).isNotNull();

        return found;
    }


}

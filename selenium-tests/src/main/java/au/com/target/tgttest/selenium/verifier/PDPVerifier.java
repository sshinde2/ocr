/**
 * 
 */
package au.com.target.tgttest.selenium.verifier;

import static org.fest.assertions.Assertions.assertThat;

import java.text.MessageFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.ProductPage;


/**
 * Verifier for product details page<br/>
 * TODO: add more verification.
 * 
 */
public final class PDPVerifier {

    private static final Logger LOG = LogManager.getLogger(PDPVerifier.class);

    private PDPVerifier() {

    }

    /**
     * Checks a product details page against product data to ensure the page has been correctly loaded and all required
     * product data are displayed.
     * 
     * @param page
     *            the product details page
     * @param product
     *            the product data
     */
    public static void verifyProductDetailsPage(final ProductPage page, final Product product, final boolean inStock) {
        LOG.info(MessageFormat.format("Verifying product details page for product: \"{0}\" ({1})",
                product.getName(), product.getCode()));

        verifyTitle(product, page.getProductDetailTitleText());

        verifyStock(page, product, inStock);
    }


    private static void verifyTitle(final Product product, final String pageTitle) {

        // Expect title to be the base name, with colour appended if there is one
        String expectedTitle = product.getBaseName();
        if (!product.isNoColour()) {
            expectedTitle += " - " + product.getColour();
        }

        assertThat(pageTitle).as("product page title").isEqualTo(expectedTitle);
    }


    private static void verifyStock(final ProductPage page, final Product product, final boolean inStock) {

        if (inStock) {

            final String color = page.getProductInStockColour();
            assertThat(color).as("in stock colour").isEqualTo("rgba(96, 170, 48, 1)");

            // Has Price
            assertThat(page.getProductPriceValue()).as("product price").isEqualTo(product.getPrice());

            // Check for was price
            if (product.hasWasPrice()) {
                assertThat(page.getProductWasPriceValue()).as("product was price").isEqualTo(product.getWasPrice());
            }

        }
        else
        {
            final String color = page.getProductOutOfStockColour();
            assertThat(color).as("out of stock colour").isEqualTo("rgba(186, 0, 0, 1)");
            assertThat(page.getProductOutOfStockText()).as("out of stock text").contains("Currently unavailable");
        }

    }
}

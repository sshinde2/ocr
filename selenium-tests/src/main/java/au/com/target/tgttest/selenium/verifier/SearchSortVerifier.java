/**
 * 
 */
package au.com.target.tgttest.selenium.verifier;

import static org.fest.assertions.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import au.com.target.tgttest.selenium.data.SearchFilterType;
import au.com.target.tgttest.selenium.pageobjects.common.ProductInList;


/**
 * Search sort verifier util
 * 
 */
public final class SearchSortVerifier {

    private SearchSortVerifier() {
        //util
    }

    public static void checkSortOrder(final SearchFilterType searchFilterType, final List<ProductInList> list) {

        switch (searchFilterType) {
            case name_asc:
                checkOrderAZ(list);
                break;
            case name_desc:
                checkOrderZA(list);
                break;
            case price_asc:
                checkOrderPriceLowHigh(list);
                break;
            case price_desc:
                checkOrderPriceHighLow(list);
                break;

        }

    }

    /**
     * Check names in the list are in asc order alphabetically, ignoring case
     * 
     * @param list
     */
    public static void checkOrderAZ(final List<ProductInList> list)
    {
        final List<ProductInList> sorted = new ArrayList<ProductInList>(list);
        Collections.sort(sorted, new Comparator<ProductInList>() {

            @Override
            public int compare(final ProductInList arg0, final ProductInList arg1) {
                int value = compareOutOfStock(arg0, arg1);
                if (value == 0) {
                    value = arg0.getProductName().compareTo(arg1.getProductName());
                }

                return value;
            }
        });

        for (int i = 0; i < sorted.size(); i++) {
            final String message = "Names out of order A-Z: " + sorted.get(i).getProductName() + ", "
                    + list.get(i).getProductName();
            assertThat(sorted.get(i).getProductName()).overridingErrorMessage(message)
                    .isEqualTo(list.get(i).getProductName());
        }
    }

    /**
     * Check names in the list are in desc order alphabetically, ignoring case
     * 
     * @param list
     */
    public static void checkOrderZA(final List<ProductInList> list)
    {
        final List<ProductInList> sorted = new ArrayList<ProductInList>(list);
        Collections.sort(sorted, new Comparator<ProductInList>() {

            @Override
            public int compare(final ProductInList arg0, final ProductInList arg1) {
                int value = compareOutOfStock(arg0, arg1);
                if (value == 0) {
                    value = arg1.getProductName().compareTo(arg0.getProductName());
                }

                return value;
            }
        });

        for (int i = 0; i < sorted.size(); i++) {
            final String message = "Names out of order Z-A: " + sorted.get(i).getProductName() + ", "
                    + list.get(i).getProductName();
            assertThat(sorted.get(i).getProductName()).overridingErrorMessage(message)
                    .isEqualTo(list.get(i).getProductName());
        }
    }

    /**
     * Check names in the list are in increasing order of price
     * 
     * @param list
     */
    public static void checkOrderPriceLowHigh(final List<ProductInList> list)
    {
        final List<ProductInList> sorted = new ArrayList<ProductInList>(list);
        Collections.sort(sorted, new Comparator<ProductInList>() {

            @Override
            public int compare(final ProductInList arg0, final ProductInList arg1) {
                int value = compareOutOfStock(arg0, arg1);
                if (value == 0 && (!arg0.isOutOfStock() && !arg1.isOutOfStock())) {
                    value = arg0.getProductPriceValue().compareTo(arg1.getProductPriceValue());
                }

                return value;
            }
        });

        for (int i = 0; i < sorted.size(); i++) {
            final String message = "Names out of order price inc";
            assertThat(sorted.get(i).getProductPriceValue()).overridingErrorMessage(message)
                    .isEqualTo(list.get(i).getProductPriceValue());
        }
    }

    /**
     * Check names in the list are in decreasing order of price
     * 
     * @param list
     */
    public static void checkOrderPriceHighLow(final List<ProductInList> list)
    {
        final List<ProductInList> sorted = new ArrayList<ProductInList>(list);
        Collections.sort(sorted, new Comparator<ProductInList>() {

            @Override
            public int compare(final ProductInList arg0, final ProductInList arg1) {
                int value = compareOutOfStock(arg0, arg1);
                if (value == 0 && (!arg0.isOutOfStock() && !arg1.isOutOfStock())) {
                    value = arg1.getProductPriceValue().compareTo(arg0.getProductPriceValue());
                }

                return value;
            }
        });

        for (int i = 0; i < sorted.size(); i++) {
            final String message = "Names out of order price decr";
            assertThat(sorted.get(i).getProductPriceValue()).overridingErrorMessage(message)
                    .isEqualTo(list.get(i).getProductPriceValue());
        }
    }

    private static int compareOutOfStock(final ProductInList arg0, final ProductInList arg1) {
        int value = 0;
        if (arg0.isOutOfStock() && arg1.isOutOfStock()) {
            value = 0;
        }
        else if (arg0.isOutOfStock()) {
            value = 1;
        }
        else if (arg1.isOutOfStock()) {
            value = -1;
        }

        return value;
    }

}

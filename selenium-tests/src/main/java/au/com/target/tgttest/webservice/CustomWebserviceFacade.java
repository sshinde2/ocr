/**
 *
 */
package au.com.target.tgttest.webservice;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.text.StrSubstitutor;

import au.com.target.tgttest.cucumber.steps.webservice.bean.ImportProductDetails;
import au.com.target.tgtwsfacades.integration.dto.IntegrationProductDto;
import au.com.target.tgtwsfacades.integration.dto.IntegrationResponseDto;

import com.sun.jersey.api.client.ClientResponse;


/**
 * Facade for accessing Target custom web services
 *
 */
public class CustomWebserviceFacade {

    public static final String IMPORT_PRODUCT_URI = "productimportapi/productimport/";
    private static final String STOCK_UPDATE_URI = "stockupdateapi/targetstockupdate/";

    private static final String STOCK_UPDATE_XML = "<integration-stockUpdate><itemCode>${code}</itemCode><warehouse>FastlineWarehouse</warehouse>"
            + "<totalQuantityAvailable>${stock}</totalQuantityAvailable></integration-stockUpdate>";

    /**
     * Update product via product import web service
     *
     * @param code
     * @param xml
     *            can be either a dto or raw xml string
     * @return TestIntegrationResponseDto
     */
    public static IntegrationResponseDto updateProduct(final String code, final Object xml) {

        final ClientResponse result = WebserviceUtil.getCustomWebResource()
                .path(IMPORT_PRODUCT_URI + code)
                .accept(MediaType.APPLICATION_XML).type("application/xml").put(ClientResponse.class, xml);
        result.bufferEntity();
        WebserviceUtil.assertOk(result, false);

        final IntegrationResponseDto responseDto = result.getEntity(IntegrationResponseDto.class);
        return responseDto;
    }


    /**
     * Create a IntegrationProductDto from the given details
     *
     * @param details
     * @return IntegrationProductDto
     */
    public static IntegrationProductDto createProductDto(final ImportProductDetails details) {

        final IntegrationProductDto product = new IntegrationProductDto();
        product.setProductCode(details.getProductCode());
        product.setVariantCode(details.getVariantCode());
        product.setName(details.getName());
        product.setDepartment(details.getDepartment());
        product.setBrand(details.getBrand());
        product.setPrimaryCategory(details.getPrimaryCategory());
        product.setDescription(details.getDescription());

        // Defaults
        product.setApprovalStatus("active");
        product.setAvailableLayby("false");
        product.setAvailableLongtermLayby("false");
        product.setAvailableHomeDelivery("true");
        product.setAvailableCnc("true");
        product.setIsStylegroup(Boolean.FALSE);
        product.setIsAssortment(Boolean.FALSE);
        product.setIsSizeOnly(Boolean.FALSE);

        return product;

    }

    /**
     * reset stock for given code.
     * 
     * @param code
     *            identifies the code for the product
     * @param stock
     *            is the available qty for the given product
     */
    public static void resetStock(final String code, final int stock) {
        final Map<String, String> valuesMap = new HashMap<String, String>();
        valuesMap.put("code", code);
        valuesMap.put("stock", "" + stock);
        final StrSubstitutor sub = new StrSubstitutor(valuesMap);
        final String resolvedString = sub.replace(STOCK_UPDATE_XML);
        final ClientResponse result = WebserviceUtil.getCustomWebResource().path(STOCK_UPDATE_URI + code)
                .accept(MediaType.APPLICATION_XML).type("application/xml").put(ClientResponse.class, resolvedString);
        result.bufferEntity();
        WebserviceUtil.assertOk(result, false);
    }

}

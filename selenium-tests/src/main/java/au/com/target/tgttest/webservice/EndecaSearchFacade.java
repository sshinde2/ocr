/**
 *
 */
package au.com.target.tgttest.webservice;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;

import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQuery;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.UrlENEQuery;
import com.endeca.navigation.UrlENEQueryParseException;


/**
 * @author htan3
 *
 */
public class EndecaSearchFacade {
    private final static Logger LOG = LogManager.getLogger(EndecaSearchFacade.class);

    private static final String CP_URL = "N=0&Ntk=colourVariantCode&Ntx=mode+matchall&Ntt=";
    private static final String KEY_STOCK_LEVEL_STATUS = "stockLevelStatus";
    private static final String KEY_AVAIL_QTY = "availQty";
    private static final String KEY_SIZEVARIANTCODE = "sizeVariantCode";
    private static final String STR_ = "_";

    private final static EndecaSearchFacade endecaSearchFacade = new EndecaSearchFacade();

    private final String host;
    private final String port;

    private EndecaSearchFacade() {
        final TestConfiguration TestConfig = TestConfiguration.getInstance();
        host = TestConfig.getAdminHost();
        port = TestConfig.getEndecaEnePort();
    }

    /**
     * Get an instance of EndecaSearchFacade to perform search tasks.
     *
     * @return the private static instance.
     */
    public static EndecaSearchFacade getInstance() {
        return endecaSearchFacade;
    }

    /**
     * get availQty from Endeca for given saleable productCode.
     *
     * @param saleableProductCode
     *            identifies a saleable product.
     * @return int value of availQty if single record found for given colourVariantCode and sizeVariantCode
     */
    public int checkAvailQty(final String saleableProductCode) {
        final ERec erecRecord = getERec(new HttpENEConnection(host, port), CP_URL
                + getColourVariantCode(saleableProductCode),
                getSizeVariantCode(saleableProductCode));
        return Integer.parseInt((String)erecRecord.getProperties().get(KEY_AVAIL_QTY));
    }

    /**
     * get availQty from Endeca for given colourVariantCode and sizeVariantCode
     *
     * @param saleableProductCode
     *            identifies a saleable product.
     * @return string value of stockLevelStatus if single record found for given colourVariantCode and sizeVariantCode
     */
    public String checkStockLevelStatus(final String saleableProductCode) {
        final ERec erecRecord = getERec(new HttpENEConnection(host, port), CP_URL
                + getColourVariantCode(saleableProductCode),
                getSizeVariantCode(saleableProductCode));
        return (String)erecRecord.getProperties().get(KEY_STOCK_LEVEL_STATUS);
    }

    /**
     * Identify single ERec record by given query and sizeVariantCode.
     *
     * @param conn
     * @param query
     * @param sizeVariantCode
     * @return ERec record if a single record can be identified
     * @throws RuntimeException
     *             if no record or multiple records found.
     */
    private ERec getERec(final ENEConnection conn, final String query, final String sizeVariantCode) {
        ERec result = null;
        try {
            final ENEQuery eneQuery = new UrlENEQuery(query, "UTF-8");
            eneQuery.setNavERecsPerAggrERec(2);
            LOG.info("Query Fired- " + eneQuery.toString());
            ENEQueryResults results = null;
            try {
                results = conn.query(eneQuery);
            }
            catch (final ENEQueryException e) {
                LOG.error("Error: " + e.getMessage() + ", Query: " + eneQuery.toString());
            }
            if (null == results) {
                LOG.error("No Data Returned");
            }
            else {
                final List<ERec> resultList = results.getNavigation().getERecs();
                if (StringUtils.isEmpty(sizeVariantCode)) {
                    if (resultList.size() == 1) {
                        result = resultList.get(0);
                    }
                }
                else {
                    for (final ERec erec : resultList) {
                        if (sizeVariantCode.equals(erec.getProperties().get(KEY_SIZEVARIANTCODE))) {
                            result = erec;
                        }
                    }
                }
            }
        }
        catch (final UrlENEQueryParseException e1) {
            LOG.error(e1);
        }
        if (result == null) {
            throw new RuntimeException("Failed to identify single record query:" + query + ", sizeVariantCode="
                    + sizeVariantCode);
        }
        return result;

    }



    /**
     * @param saleableProductCode
     * @return colourVariantCode from given productCode
     */
    private String getColourVariantCode(final String saleableProductCode) {
        if (saleableProductCode.indexOf(STR_) == -1) {
            return saleableProductCode;
        }
        else {
            return saleableProductCode.substring(0, saleableProductCode.lastIndexOf(STR_));
        }
    }

    /**
     * @param saleableProductCode
     * @return sizeVariantCode from given productCode
     */
    private String getSizeVariantCode(final String saleableProductCode) {
        if (saleableProductCode.indexOf(STR_) == -1) {
            return null;
        }
        else {
            return saleableProductCode;
        }
    }

}

/**
 *
 */
package au.com.target.tgttest.webservice;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import au.com.target.tgttest.webservice.endeca.wsdl.BatchStatusType;
import au.com.target.tgttest.webservice.endeca.wsdl.FullyQualifiedScriptIDType;
import au.com.target.tgttest.webservice.endeca.wsdl.ObjectFactory;


/**
 * @author htan3
 *
 */
public class EndecaWebserviceFacade {

    private final static Logger LOG = LogManager.getLogger(EndecaWebserviceFacade.class);

    private final static EndecaWebserviceFacade endecaService = new EndecaWebserviceFacade();

    private final WebServiceTemplate webServiceTemplate;

    /**
     * factory instance to create required objects pass in and out of the endeca webservice.
     */
    private final ObjectFactory objectFactory = new ObjectFactory();


    @SuppressWarnings("resource")
    private EndecaWebserviceFacade() {
        webServiceTemplate = (WebServiceTemplate)(new ClassPathXmlApplicationContext("spring-endeca.xml"))
                .getBean("webServiceTemplate");
    }

    public static EndecaWebserviceFacade getInstance() {
        return endecaService;
    }

    /**
     * Start Endeca script via webservice.
     *
     * @param appId
     *            identifies the app to run the script.
     * @param scriptId
     *            identifies the script to run
     * @return true if the script is started, false if not.
     */
    public boolean startScript(final String appId, final String scriptId) {
        try {
            webServiceTemplate.marshalSendAndReceive(objectFactory.createStartScriptInput(createScriptID(appId,
                    scriptId)));
        }
        catch (final org.springframework.oxm.UnmarshallingFailureException ume) {
            //false exception;
        }
        catch (final SoapFaultClientException e) {
            LOG.info("Endeca start " + scriptId + " Failed:" + e.getFaultStringOrReason());
            return false;
        }
        LOG.info("Endeca " + scriptId + " is started and running.");
        return true;
    }

    /**
     * Check the status of a given script.
     *
     * @param appId
     *            identifies the app to run the script.
     * @param scriptId
     *            identifies the script to run.
     * @return the status of script as BatchStatusType
     */
    public BatchStatusType checkScriptStatus(final String appId, final String scriptId) {
        LOG.info("Checking " + scriptId + "");
        final JAXBElement response = (JAXBElement)webServiceTemplate.marshalSendAndReceive(
                objectFactory.createGetScriptStatusInput(createScriptID(appId, scriptId)));
        final BatchStatusType scriptStatus = (BatchStatusType)response.getValue();

        LOG.info(StringUtils.join(new Object[] { scriptId, scriptStatus.getState(), scriptStatus.getFailureMessage(),
                scriptStatus.getDuration() }, ":"));
        return scriptStatus;
    }

    /**
     * Create ScriptID to pass to the webservice calls.
     *
     * @param appId
     * @param scriptId
     */
    private FullyQualifiedScriptIDType createScriptID(final String appId, final String scriptId) {
        final FullyQualifiedScriptIDType idType = new FullyQualifiedScriptIDType();
        idType.setApplicationID(appId);
        idType.setScriptID(scriptId);
        return idType;
    }

}

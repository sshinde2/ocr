/**
 *
 */
package au.com.target.tgttest.webservice;

import javax.ws.rs.core.MediaType;

import au.com.target.tgtcore.dto.TargetColourVariantProductDTO;
import au.com.target.tgtcore.dto.TargetProductDTO;

import com.sun.jersey.api.client.ClientResponse;


/**
 * Facade for accessing Hybris default web services
 *
 */
public class HybrisWebserviceFacade {

    private static final String ONLINE_PRODUCTS_URI = "catalogs/targetProductCatalog/catalogversions/Online/products/";
    private static final String STAGED_PRODUCTS_URI = "catalogs/targetProductCatalog/catalogversions/Staged/products/";

    public static TargetColourVariantProductDTO getStagedColourVariantProduct(final String code) {

        final ClientResponse result = getClientResponse(code, STAGED_PRODUCTS_URI);
        return result.getEntity(TargetColourVariantProductDTO.class);
    }

    public static TargetProductDTO getStagedTargetProduct(final String code) {

        final ClientResponse result = getClientResponse(code, STAGED_PRODUCTS_URI);
        return result.getEntity(TargetProductDTO.class);
    }

    private static ClientResponse getClientResponse(final String code, final String baseuri) {

        final ClientResponse result = WebserviceUtil.getHybrisWebResource()
                .path(baseuri + code)
                .accept(MediaType.APPLICATION_XML).get(ClientResponse.class);
        result.bufferEntity();
        WebserviceUtil.assertOk(result, false);

        return result;
    }

}

/**
 * 
 */
package au.com.target.tgttest.webservice;

import static org.fest.assertions.Assertions.assertThat;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.client.ClientResponse;


/**
 * Facade for accessing tgttest web service api
 * 
 */
public class TestWebServiceFacade {

    public static void testInitialisation() {

        final ClientResponse result = WebserviceUtil.getTestWebResource()
                .path("testinitialisation").path("init")
                .accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        result.bufferEntity();

        assertThat(result.getStatus()).as("HTTP status").isEqualTo(Response.Status.OK.getStatusCode());
    }


    public static void setPosProductClientMock() {

        final ClientResponse result = WebserviceUtil.getTestWebResource()
                .path("injectmocks").path("posProductClient")
                .accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        result.bufferEntity();

        assertThat(result.getStatus()).as("HTTP status").isEqualTo(Response.Status.OK.getStatusCode());
    }

    public static String createAnyOrder() {

        final ClientResponse result = WebserviceUtil.getTestWebResource()
                .path("createorder").path("anyorder")
                .accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        result.bufferEntity();

        assertThat(result.getStatus()).as("HTTP status").isEqualTo(Response.Status.OK.getStatusCode());

        final String orderNumber = result.getEntity(String.class);
        return orderNumber;
    }

}

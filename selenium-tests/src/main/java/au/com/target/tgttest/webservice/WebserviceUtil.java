/**
 * 
 */
package au.com.target.tgttest.webservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import au.com.target.tgttest.selenium.base.config.TestConfiguration;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;


/**
 * Util for accessing web service
 * 
 */
public class WebserviceUtil {

    private static final String HTTP_PROTOCOL_PREFIX = "http://";

    // hybris resources
    private static final String HYBRIS_WS_VERSION = "ws410";
    private static final String HYBRIS_WS_REST = "rest";

    // custom resources
    private static final String WS_VERSION = "v1";
    private static final String WS_TGTWS = "tgtws";
    private static final String WS_TARGET = "target";

    // tgttest resources
    private static final String TEST_TGTTEST = "tgttest";
    private static final String TEST_REST = "rest";


    /**
     * Get a WebResource for accessing Hybris default web services
     * 
     * @return WebResource
     */
    public static WebResource getHybrisWebResource() {

        final TestConfiguration testConfig = TestConfiguration.getInstance();

        final Client jerseyClient = getAuthorizedJerseyClient(testConfig.getWSUsername(),
                testConfig.getWSPassword());

        final WebResource webResource = jerseyClient.resource(getHostWebUri()
                .path(HYBRIS_WS_VERSION).path(HYBRIS_WS_REST).build());

        return webResource;
    }

    /**
     * Get a WebResource for accessing Target Custom web services
     * 
     * @return WebResource
     */
    public static WebResource getCustomWebResource() {

        final TestConfiguration testConfig = TestConfiguration.getInstance();

        final Client jerseyClient = getAuthorizedJerseyClient(testConfig.getESBUsername(), testConfig.getESBPassword());

        final WebResource webResource = jerseyClient.resource(getHostWebUri()
                .path(WS_TGTWS).path(WS_VERSION).path(WS_TARGET).build());

        return webResource;
    }

    public static WebResource getTestWebResource() {

        final ClientConfig config = new DefaultClientConfig();

        final Client jerseyClient = Client.create(config);
        jerseyClient.addFilter(new LoggingFilter());

        return jerseyClient.resource(getHostWebUri().path(TEST_TGTTEST).path(TEST_REST).build());
    }

    private static Client getAuthorizedJerseyClient(final String username, final String password) {

        final ClientConfig config = new DefaultClientConfig();

        final Client jerseyClient = Client.create(config);
        jerseyClient.addFilter(new LoggingFilter());
        jerseyClient.addFilter(new HTTPBasicAuthFilter(username, password));

        return jerseyClient;
    }

    private static UriBuilder getHostWebUri() {

        final TestConfiguration testConfig = TestConfiguration.getInstance();

        final String hostAddress = testConfig.getAdminHost();
        final int port = Integer.parseInt(testConfig.getAdminPort());

        String host;
        if (hostAddress.startsWith(HTTP_PROTOCOL_PREFIX)) {
            host = hostAddress;
        }
        else {
            host = HTTP_PROTOCOL_PREFIX + hostAddress;
        }

        return UriBuilder.fromUri(host).port(port);
    }


    /**
     * Check the response is ok
     * 
     * @param response
     * @param expectEmptyBody
     */
    public static void assertOk(final ClientResponse response, final boolean expectEmptyBody)
    {
        assertEquals("Wrong HTTP status at response: " + response, Response.Status.OK, response.getResponseStatus());
        assertTrue("Wrong content-type at response: " + response,
                MediaType.APPLICATION_XML_TYPE.isCompatible(response.getType()));
        if (expectEmptyBody)
        {
            assertTrue("Body should be empty at response: " + response, response.getEntity(String.class).isEmpty());
        }
    }


}

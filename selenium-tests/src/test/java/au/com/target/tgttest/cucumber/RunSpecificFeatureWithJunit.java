/**
 *
 */
package au.com.target.tgttest.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(snippets = SnippetType.CAMELCASE, features = { "classpath:au/com/target/tgttest/cucumber/webservice/Endeca.feature" },
        format = { "pretty", "html:target/cucumber-html-report" })
public class RunSpecificFeatureWithJunit {
    // Cucumber test runner class - specified feature

}

/**
 * 
 */
package au.com.target.tgttest.selenium.test.checkout;

import java.util.List;

import au.com.target.tgttest.selenium.base.BaseSeleniumTest;
import au.com.target.tgttest.selenium.datastore.bean.Product;


/**
 * A Base Selenium test class specific for checkout stories.
 * 
 * @author maesi
 * 
 */
public abstract class BaseCheckoutTest extends BaseSeleniumTest {

    private static final String TEST_USERS_IMPEX = "test-users.impex";

    private List<Product> products;


    @Override
    protected void setupTestData() {

        // Products to buy - reset the stock
        products = loadProducts();
        getStockLevelAdjuster().resetStock(products);

        // Test customer
        getImpexImporter().importImpexFromImpexFolder(TEST_USERS_IMPEX);
    }


    /**
     * Load the products to buy
     * 
     * @return list
     */
    protected abstract List<Product> loadProducts();


    /**
     * Get the product list.
     * 
     * @return list of all buy now products for the test.
     */
    protected List<Product> getProducts() {
        return products;
    }


}

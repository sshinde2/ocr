/**
 * 
 */
package au.com.target.tgttest.selenium.test.checkout;

import java.util.Collections;
import java.util.List;

import org.testng.annotations.Test;

import au.com.target.tgttest.selenium.datastore.DataStoreLookup;
import au.com.target.tgttest.selenium.datastore.bean.CreditCard;
import au.com.target.tgttest.selenium.datastore.bean.Customer;
import au.com.target.tgttest.selenium.datastore.bean.Product;
import au.com.target.tgttest.selenium.pageobjects.ShoppingBasketPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.DeliveryMethodPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.PaymentDetailsPage;
import au.com.target.tgttest.selenium.pageobjects.checkout.ThankyouPage;
import au.com.target.tgttest.selenium.process.ShoppingProcess;
import au.com.target.tgttest.selenium.verifier.CheckoutPageComponentVerifier;


/**
 * A simple selenium checkout test, which adds a buy now product to the shopping basket, starts the checkout, logs in as
 * an existing user, selects home delivery and pays off with credit card.
 * 
 * 
 * @author maesi
 * 
 */
public class BuyNowProductExistingUserHomeDeliveryCreditCardPayment extends BaseCheckoutTest {


    /**
     * Load a single colour variant product
     */
    @Override
    public List<Product> loadProducts() {

        return Collections.singletonList(getDataStore().getProductDataStore(DataStoreLookup.PRODUCTS_CV_HD)
                .getNextProduct());
    }


    /**
     * tests a simple checkout run with an existing user account, home delivery option and credit card payment.
     */
    @Test
    public void testCheckoutExistingUserHomeDeliveryCreditCardPayment() {

        final ShoppingProcess shop = getShoppingProcess();

        final ShoppingBasketPage basketPage = shop.addProductsIntoShoppingBasket(getProducts());

        final Customer customer = getDataStore().getStandardRegisteredCustomer();
        final CreditCard card = getDataStore().getStandardCreditCard();

        final DeliveryMethodPage deliveryMethodPage = shop.checkoutLoginAsRegisteredCustomer(basketPage, customer);

        final PaymentDetailsPage paymentDetailPage = shop.checkoutHomeDeliveryUseFirstAddress(deliveryMethodPage);

        final ThankyouPage thankyouPage = shop.checkoutPayWithCreditCard(paymentDetailPage, customer, card);

        CheckoutPageComponentVerifier.verifyTotalisationSummary(thankyouPage, getProducts(), true);

    }


}

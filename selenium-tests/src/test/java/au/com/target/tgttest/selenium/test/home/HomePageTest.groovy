/**
 *
 */
package au.com.target.tgttest.selenium.test.home

import static org.fest.assertions.Assertions.assertThat

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.testng.annotations.Test

import au.com.target.tgttest.selenium.base.BaseSeleniumTest
import au.com.target.tgttest.selenium.data.DepartmentType
import au.com.target.tgttest.selenium.data.FooterContentType
import au.com.target.tgttest.selenium.pageobjects.HomePage

/**
 * Groovy version of Home Page test
 *
 */
class HomePageTest extends BaseSeleniumTest {

    Logger LOG = LogManager.getLogger(this.getClass());

    @Test
    void givenHomePage() {
        def homePage = navigateHome();

        verifyHeader(homePage);
        verifyFooter(homePage);

        // Check target links back to home page
        homePage = homePage.getHeader().clickTargetLogo();
        assertThat (homePage.isCurrentPage()).as("expect home page").isTrue();
    }

    private void verifyHeader(HomePage homePage) {

        assertThat(homePage.getHeader().hasTargetLogo()).as("target logo").isTrue();
    }

    private void verifyFooter(HomePage homePage) {

        // Check dept level categories exist in the footer product nav
        for (DepartmentType dept : DepartmentType.values()) {

            assertThat(homePage.getFooter().existsDepartmentLink(dept.getName()))
                    .as("department link: " + dept.getCode()).isTrue();
        }

        for (FooterContentType footerContentType : FooterContentType.values()) {

            assertThat(homePage.getFooter().existsContentLink(footerContentType))
                    .as("content link: " + footerContentType.getTitleText()).isTrue();
        }

        // Check contact info contains the right info
        assertThat(homePage.getFooter().getContactInfoText()).contains("Online Shopping Support");
        assertThat(homePage.getFooter().getContactInfoText()).contains("1300 753 567");

        // Check legal info is present
        assertThat(homePage.getFooter().getLegalInfoText())
                .contains("2014 Target Australia Pty Ltd ABN 75 004 250 944");

    }

}

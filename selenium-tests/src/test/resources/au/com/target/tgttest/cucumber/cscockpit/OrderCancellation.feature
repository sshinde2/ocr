Feature: Orders can be cancelled via CS Cockpit

Scenario: Standard order is placed in state In Progress.
  Given any order is placed
  When find order in cscockpit
  Then function 'Partial Cancel Order' is available
  And function 'Cancel Order' is available
  And function 'Refund Order' is not available
  And function 'Partial Refund Order' is not available
  
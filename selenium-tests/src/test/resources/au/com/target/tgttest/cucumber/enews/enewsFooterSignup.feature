@enews
Feature: Would like customers to easily sign in for enews, so add a sign up in footer.
  See OCR-6230

  Scenario Outline: Sign in box appears in footer of some pages, but not checkout
    When visit page '<page>'
    Then enews sign up footer box appears is '<doesAppear>'

    Examples: 
      | page             | doesAppear |
      | homepage         | true       |
      | checkout pages   | false      |
      | my account pages | false      |

  Scenario: Disclaimer text and button appears on activation (receives focus, eg click in box)
    When visit homepage
    And activate enews sign in
    Then see enews disclaimer text and button

  Scenario Outline: enews signup via footer
    When visit homepage
    And activate enews sign in
    And do enews footer signup '<email>'
    Then sign up succsess is '<success>'
    And enews validation message '<validationMessage>'
    

    Examples: 
      | email                    | success | validationMessage                                     |
      | john.smith@target.com.au | true    |                                                       |
      | invalidemail             | false   | Email Address must be in a valid email Address format |

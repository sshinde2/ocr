Feature: eNews sign up popup on home page
  Popup encourages customers to sign up for enews
  OCR-6231
  
  Scenario: First visit sign up
    Given not already signed up for enews
    And no soft login
    When sign up for enews via popup
    Then sign up success is 'true'
    And remember signed up for enews is 'true'

  Scenario: No popup when soft logged in
    Given not already signed up for enews
    And has soft login
    And popup visit homepage
    Then popup form does not appear

  Scenario: No popup when already signed up
    Given already signed up for enews
    And no soft login
    When popup visit homepage
    Then popup form does not appear

  Scenario: First visit dismiss removes popup for 15 days
    Given not already signed up for enews
    And no soft login
    And popup visit homepage
    When dismiss enews popup
    Then remember signed up for enews is 'false'
    But last dismissed time is set for 15 days

  Scenario: Dismiss removes popup for 15 days, does not appear before
    Given not already signed up for enews
    And no soft login
    And last dismissed time is set to 1 day ago
    And popup visit homepage
    Then popup form does not appear

  Scenario: Dismiss removes popup for 15 days, does appear after
    Given not already signed up for enews
    And no soft login
    And last dismissed time is set to 15 days ago
    Then popup form appears


@enews
Feature: eNews sign up page.
  This landing page can be linked to from marketing material. The form allows customers to sign up for eNews.
  See OCR-6151

  # No need to check existing email address - handled by Salmat
  # Check 'Done' message on screen, Check the cookie is set, maybe check esb message using mock
  Scenario Outline: Sign up customer given various field combinations
    When enews sign up with:
      | title     | email     | first     |
      | '<title>' | '<email>' | '<first>' |
    Then enews sign up success is '<success>'
    And enews validation message is '<message>'

    Examples: 
      | notes         | title | email                    | first | message                                               | success |
      | all fields    | Mr    | john.smith@target.com.au | John  |                                                       | true    |
      | missing title |       | john.smith@target.com.au | John  |                                                       | true    |
      | missing name  | Mr    | john.smith@target.com.au |       |                                                       | true    |
      | just email    |       | john.smith@target.com.au |       |                                                       | true    |
      | no email      | Mr    |                          | John  | Please provide an Email Address                       | false   |
      | invalid email | Mr    | bademail                 | John  | Email Address must be in a valid Email Address format | false   |

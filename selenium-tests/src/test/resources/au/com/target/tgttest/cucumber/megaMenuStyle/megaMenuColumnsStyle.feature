@notAutomated
Feature: Mega Menu columns style
  
  Scenario : Column configured to have a grey background
  Given a Column on the Mega Menu has been configured to have a grey background
  When the Mega Menu is retrieved
  Then the Column is returned with a grey background
  
  Scenario : Column not configured to have a grey background
  Given a Column on the Mega Menu has not been configured to have a grey background
  When the Mega Menu is retrieved
  Then the Column is returned with the default background colour (white)
  
  Scenario : Consecutive Columns configured to have a grey background
  Given two Columns on the Mega Menu has been configured to have a grey background
  And those columns are consecutive (side-by-side)
  When the Mega Menu is retrieved
  Then the Columns are returned with a grey background
  And the visual display of the columns has a single grey background block

@notAutomated
Feature: Mega Menu item colours
  
  Scenario : Section has a green Item
  Given a Section on the Mega Menu has an Item that is configured to be displayed green
  And the Item has a link defined
  When the Mega Menu is retrieved
  Then the Item is returned as green
  And the Item has the associated link URL
  
  Scenario : Section has a red Item
  Given a Section on the Mega Menu has an Item that is configured to be displayed red
  And the Item has a link defined
  When the Mega Menu is retrieved
  Then the Item is returned as red
  And the Item has the associated link URL

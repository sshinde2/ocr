@notAutomated
Feature: Mega Menu section headings style

Scenario : Section has a red heading
Given a Section on the Mega Menu is configured to have a red heading
When the Mega Menu is retrieved
Then the Section heading is returned as red

Scenario : Section has the default colour heading
Given a Section on the Mega Menu is not configured to have a colour heading
When the Mega Menu is retrieved
Then the Section heading is returned as the default colour

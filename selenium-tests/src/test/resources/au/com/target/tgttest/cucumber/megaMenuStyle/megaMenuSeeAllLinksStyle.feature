@notAutomated
Feature: mega menu See All links for Sections
  
  Scenario : Section configured to have a See All Item
  Given a Section has been configured to have a See All Item
  When the Mega Menu is retrieved
  Then the Section is returned
  And a See All Item is returned in the Section
  And the Item has the label and link configured in the CMS
  
  Scenario : Section not configured to have a See All Item
  Given a Section has not been configured to have a See All Item
  When the Mega Menu is retrieved
  Then the Section is returned
  And no See All Item is returned in the Section

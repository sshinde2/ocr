Feature: Product details page

# TODO: Expand detail in the verification step
Scenario: Colour variant product in stock
Given given single colour variant in stock
When go to product page
Then verify product page for in stock

Scenario: Colour variant product out of stock
Given given single colour variant out of stock
When go to product page
Then verify product page for out of stock

Scenario: Size variant product in stock
Given given single size variant in stock
When go to product page
Then verify product page for in stock

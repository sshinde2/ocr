Feature: search results can be sorted, so the customer

  # Bike should have 8 results and various prices
  
  Scenario: Sort by default order
    Given go to homepage
    When search for 'bike'
    Then number search results on page is 8
    And search results message says found 8

  Scenario Outline: Sort by different options
    Given go to homepage
    When search for 'bike'
    And sort results '<sort>'
    Then verify results are sorted

    Examples: 
      | sort       |
      | name_asc   |
      | name_desc  |
      | price_asc  |
      | price_desc |

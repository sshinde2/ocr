Feature: Shopping basket.

  Scenario: Add products to basket, basket page shows the products.
  Given given 2 products
  When add all products to basket
  And minicart view basket
  Then basket num items is 2
  And basket total is the total product price
  And basket has all the products 
  

@nonweb
Feature: Product Import Web service allows import of products.

  # Two ways of testing product import web service - using XML or DTO as input
  Scenario: Update a product description using XML input.
    Given product code 'P32631302'
    And new product details with xml:
      """
      <integration-product>
      <productCode>P32631302</productCode>
      <variantCode>32631302</variantCode>
      <name>Baby shoes product</name>
      <approvalStatus>active</approvalStatus>
      <department>7000</department>
      <brand>target</brand>
      <availableLayby>false</availableLayby>
      <availableLongtermLayby>false</availableLongtermLayby>
      <availableHomeDelivery>true</availableHomeDelivery>
      <availableCnc>true</availableCnc>
      <primaryCategory>W93746</primaryCategory>
      <description>description</description>
      <isStylegroup>false</isStylegroup>
      <isAssortment>false</isAssortment>
      <isSizeOnly>false</isSizeOnly>
      </integration-product>	
      """
    When run product import via ws
    And retrieve product via ws 'P32631302'
    Then product description is 'description'

  Scenario Outline: Update a product description with two different values using DTO input.
    Given product code 'P32631302'
    And new product details with dto:
      | productCode | variantCode | name               | department | brand  | primaryCategory | description   |
      | P32631302   | 32631302    | Baby shoes product | 7000       | target | W93746          | <description> |
    When run product import via ws
    And retrieve product via ws 'P32631302'
    Then product description is '<description>'

    Examples: 
      | description  |
      | description1 |
      | description2 |

  Scenario Outline: Verify the assigned delivery modes for a product
    Given product code 'P32631302'
    And new product details with xml:
      """
      <integration-product>
      <productCode>P32631302</productCode>
      <variantCode>32631302</variantCode>
      <name>Lord Of the Rings - Two Towers - DVD</name>
      <approvalStatus>active</approvalStatus>
      <department><department_number></department>
      <brand>target</brand>
      <availableLayby>N</availableLayby>
      <availableLongtermLayby>N</availableLongtermLayby>
      <availableHomeDelivery><is_available_for_HD></availableHomeDelivery>
      <availableCnc><is_available_for_CNC></availableCnc>
      <primaryCategory>W405332</primaryCategory>
      <description>description</description>
      <isStylegroup>false</isStylegroup>
      <isAssortment>false</isAssortment>
      <isSizeOnly>false</isSizeOnly>
      </integration-product>
      """
    When run product import via ws
    And retrieve product via ws 'P32631302'
    Then assigned delivery modes are '<assigned_delivery_modes>'

    # dept# 3000 - not eligible for express delivery
    # dept# 1000 - eligible for express delivery
    Examples: 
      | department_number | is_available_for_HD | is_available_for_CNC | assigned_delivery_modes                          |
      | 3000              | Y                   | Y                    | home-delivery,click-and-collect                  |
      | 3000              | Y                   | N                    | home-delivery                                    |
      | 3000              | N                   | Y                    | click-and-collect                                |
      | 3000              | N                   | N                    |                                                  |
      | 1000              | Y                   | Y                    | home-delivery,click-and-collect,express-delivery |
      | 1000              | Y                   | N                    | home-delivery,express-delivery                   |
      | 1000              | N                   | Y                    | click-and-collect,express-delivery               |
      | 1000              | N                   | N                    | express-delivery                                 |

  Scenario Outline: Verify department assignment for Product
    Given product code 'P32631302'
    And new product details with xml:
      """
      <integration-product>
      <productCode>P32631302</productCode>
      <variantCode>32631302</variantCode>
      <name>Lord Of the Rings - Two Towers - DVD</name>
      <approvalStatus>active</approvalStatus>
      <department><department_number></department>
      <brand>target</brand>
      <availableLayby>N</availableLayby>
      <availableLongtermLayby>N</availableLongtermLayby>
      <availableHomeDelivery>Y</availableHomeDelivery>
      <availableCnc>Y</availableCnc>
      <primaryCategory>W405332</primaryCategory>
      <description>description</description>
      <isStylegroup>false</isStylegroup>
      <isAssortment>false</isAssortment>
      <isSizeOnly>false</isSizeOnly>
      </integration-product>
      """
    When run product import via ws
    And retrieve product via ws 'P32631302'
    Then assigned department is '<department_number>'

    Examples: 
      | department_number |
      | 1000              |
      | 3000              |

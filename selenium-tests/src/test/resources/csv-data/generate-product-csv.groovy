def csvGenerator = ctx.getBean("csvGenerator");

// Header is same for all product files
header = "BASE_NAME;BASE_CODE;BASE_DESCRIPTION;BRAND_CODE;BRAND_NAME;CV_CODE;CV_NAME;CLEARANCE;COLOUR_CODE;COLOUR_NAME;SWATCH_CODE;SWATCH_NAME;ESSENTIAL;HOTPRODUCT;PRICE;WASPRICE;SIZE";


out.println("Colour variants HD buynow")
queryString = """select 
 {tp.name} as base_name, {tp.code} as base_code, {tp.description} as base_description, {b.code} as brand_code, {b.name} as brand_name, 
   {tcvp.code} as cv_code, {tcvp.name} as cv_name, {tcvp.clearance} as clearance, {c.code} as colour_code, {c.name} as colour_name,
   {swatch.code} as swatch_code, {swatch.name} as swatch_name, 
   {tcvp.essential} as essential, {tcvp.hotProduct} as hotProduct, {tpr.price} as price, {tpr.wasPrice} as wasPrice, '' as size 
 from  
  {  
    TargetProduct as tp 
    join TargetColourVariantProduct as tcvp on {tcvp:baseProduct} = {tp.pk} 
    join ProductDeliveryModeRelation as pdm on {pdm.source} = {tp.pk} 
    join DeliveryMode as dm on {pdm.target} = {dm.pk} 
    join TargetProduct2PurchaseOption as ppo on {ppo.source} = {tp.pk} 
    join PurchaseOption as po on {ppo.target} = {po.pk} 
    join Brand as b on {tp.brand} = {b.pk} 
    join CatalogVersion as cv on {tp.catalogVersion} = {cv.pk} 
    join TargetProductCategory as tpc on {tp:primarysupercategory} = {tpc:pk} 
    join Colour as c on {tcvp.colour} = {c.pk} 
    join Colour as swatch on {tcvp.swatch} = {swatch.pk} 
    join TargetPriceRow as tpr on {tpr.product} = {tcvp.pk} and {tpr.startTime} is null 
  } 
  where {cv.version} = 'Online' 
  and {dm.code} = 'home-delivery'  
  and {po.code} = 'buynow' 
  and {tcvp.variantType} is null
  """;
 
out.println(csvGenerator.generateCsvFile(header, queryString, 10, "colour-variants-hd-buynow.csv"));


out.println("Colour variants CNC buynow")
queryString = """select 
 {tp.name} as base_name, {tp.code} as base_code, {tp.description} as base_description, {b.code} as brand_code, {b.name} as brand_name, 
   {tcvp.code} as cv_code, {tcvp.name} as cv_name, {tcvp.clearance} as clearance, {c.code} as colour_code, {c.name} as colour_name,
   {swatch.code} as swatch_code, {swatch.name} as swatch_name, 
   {tcvp.essential} as essential, {tcvp.hotProduct} as hotProduct, {tpr.price} as price, {tpr.wasPrice} as wasPrice, '' as size 
 from  
  {  
    TargetProduct as tp 
    join TargetColourVariantProduct as tcvp on {tcvp:baseProduct} = {tp.pk} 
    join ProductDeliveryModeRelation as pdm on {pdm.source} = {tp.pk} 
    join DeliveryMode as dm on {pdm.target} = {dm.pk} 
    join TargetProduct2PurchaseOption as ppo on {ppo.source} = {tp.pk} 
    join PurchaseOption as po on {ppo.target} = {po.pk} 
    join Brand as b on {tp.brand} = {b.pk} 
    join CatalogVersion as cv on {tp.catalogVersion} = {cv.pk} 
    join TargetProductCategory as tpc on {tp:primarysupercategory} = {tpc:pk} 
    join Colour as c on {tcvp.colour} = {c.pk} 
    join Colour as swatch on {tcvp.swatch} = {swatch.pk} 
    join TargetPriceRow as tpr on {tpr.product} = {tcvp.pk} and {tpr.startTime} is null 
  } 
  where {cv.version} = 'Online' 
  and {dm.code} = 'click-and-collect'  
  and {po.code} = 'buynow' 
  and {tcvp.variantType} is null
  """;
 
out.println(csvGenerator.generateCsvFile(header, queryString, 10, "colour-variants-cnc-buynow.csv"));


out.println("Colour variants HD layby")
queryString = """select 
 {tp.name} as base_name, {tp.code} as base_code, {tp.description} as base_description, {b.code} as brand_code, {b.name} as brand_name, 
   {tcvp.code} as cv_code, {tcvp.name} as cv_name, {tcvp.clearance} as clearance, {c.code} as colour_code, {c.name} as colour_name,
   {swatch.code} as swatch_code, {swatch.name} as swatch_name, 
   {tcvp.essential} as essential, {tcvp.hotProduct} as hotProduct, {tpr.price} as price, {tpr.wasPrice} as wasPrice, '' as size 
 from  
  {  
    TargetProduct as tp 
    join TargetColourVariantProduct as tcvp on {tcvp:baseProduct} = {tp.pk} 
    join ProductDeliveryModeRelation as pdm on {pdm.source} = {tp.pk} 
    join DeliveryMode as dm on {pdm.target} = {dm.pk} 
    join TargetProduct2PurchaseOption as ppo on {ppo.source} = {tp.pk} 
    join PurchaseOption as po on {ppo.target} = {po.pk} 
    join Brand as b on {tp.brand} = {b.pk} 
    join CatalogVersion as cv on {tp.catalogVersion} = {cv.pk} 
    join TargetProductCategory as tpc on {tp:primarysupercategory} = {tpc:pk} 
    join Colour as c on {tcvp.colour} = {c.pk} 
    join Colour as swatch on {tcvp.swatch} = {swatch.pk} 
    join TargetPriceRow as tpr on {tpr.product} = {tcvp.pk} and {tpr.startTime} is null 
  } 
  where {cv.version} = 'Online' 
  and {dm.code} = 'home-delivery'  
  and {po.code} = 'layby' 
  and {tcvp.variantType} is null
  """;
 
out.println(csvGenerator.generateCsvFile(header, queryString, 10, "colour-variants-hd-layby.csv"));


out.println("Colour variants CNC layby")
queryString = """select 
 {tp.name} as base_name, {tp.code} as base_code, {tp.description} as base_description, {b.code} as brand_code, {b.name} as brand_name, 
   {tcvp.code} as cv_code, {tcvp.name} as cv_name, {tcvp.clearance} as clearance, {c.code} as colour_code, {c.name} as colour_name,
   {swatch.code} as swatch_code, {swatch.name} as swatch_name, 
   {tcvp.essential} as essential, {tcvp.hotProduct} as hotProduct, {tpr.price} as price, {tpr.wasPrice} as wasPrice, '' as size 
 from  
  {  
    TargetProduct as tp 
    join TargetColourVariantProduct as tcvp on {tcvp:baseProduct} = {tp.pk} 
    join ProductDeliveryModeRelation as pdm on {pdm.source} = {tp.pk} 
    join DeliveryMode as dm on {pdm.target} = {dm.pk} 
    join TargetProduct2PurchaseOption as ppo on {ppo.source} = {tp.pk} 
    join PurchaseOption as po on {ppo.target} = {po.pk} 
    join Brand as b on {tp.brand} = {b.pk} 
    join CatalogVersion as cv on {tp.catalogVersion} = {cv.pk} 
    join TargetProductCategory as tpc on {tp:primarysupercategory} = {tpc:pk} 
    join Colour as c on {tcvp.colour} = {c.pk} 
    join Colour as swatch on {tcvp.swatch} = {swatch.pk} 
    join TargetPriceRow as tpr on {tpr.product} = {tcvp.pk} and {tpr.startTime} is null 
  } 
  where {cv.version} = 'Online' 
  and {dm.code} = 'click-and-collect'  
  and {po.code} = 'layby' 
  and {tcvp.variantType} is null
  """;
 
out.println(csvGenerator.generateCsvFile(header, queryString, 10, "colour-variants-cnc-layby.csv"));


out.println("Size variants HD buynow")
queryString = """select 
 {tp.name} as base_name, {tp.code} as base_code, {tp.description} as base_description, {b.code} as brand_code, {b.name} as brand_name,
 {tcvp.code} as cv_code, {tcvp.name} as cv_name, {tcvp.clearance} as clearance, {c.code} as colour_code, {c.name} as colour_name,
 {swatch.code} as swatch_code, {swatch.name} as swatch_name, 
 {tcvp.essential} as essential, {tcvp.hotProduct} as hotProduct, {tpr.price} as price, {tpr.wasPrice} as wasPrice, {tsvp.size} as size
 from  
  {  
    TargetProduct as tp
    join TargetColourVariantProduct as tcvp on {tcvp:baseProduct} = {tp.pk}
    join TargetSizeVariantProduct as tsvp on {tsvp.baseProduct} = {tcvp.pk}
    join ProductDeliveryModeRelation as pdm on {pdm.source} = {tp.pk}
    join DeliveryMode as dm on {pdm.target} = {dm.pk}
    join TargetProduct2PurchaseOption as ppo on {ppo.source} = {tp.pk}
    join PurchaseOption as po on {ppo.target} = {po.pk}
    join Brand as b on {tp.brand} = {b.pk}
    join CatalogVersion as cv on {tp.catalogVersion} = {cv.pk}
    join TargetProductCategory as tpc on {tp:primarysupercategory} = {tpc:pk}
    join Colour as c on {tcvp.colour} = {c.pk}
    join Colour as swatch on {tcvp.swatch} = {swatch.pk}
    join TargetPriceRow as tpr on {tpr.product} = {tsvp.pk} and {tpr.startTime} is null
  }
  where {cv.version} = 'Online'
  and {dm.code} = 'home-delivery' 
  and {po.code} = 'buynow'
  """;
 
out.println(csvGenerator.generateCsvFile(header, queryString, 10, "size-variants-hd-buynow.csv"));

